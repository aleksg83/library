<?php
/*
 * языковые константы панели управления
 */
$lng = array();
$lng['login']="Логин";
$lng['password']="Пароль";
$lng['enter']="Войти";
$lng['cmsName']="CMS PHARUS - Портал Математическое образование ";
$lng['auth']="Авторизация";
$lng['authDescription']="Введите логин и пароль";
$lng['welcome']="Добро пожаловать,";
$lng['profile']="Профиль";
$lng['exit']="Выход";
$lng['errorLoginForm']="Ошибка входа";
$lng['copy']='&copy 2010 - '.date("Y").' <a href="http://www.web-pharus.ru">PHARUS</a>. Все права защищены';
$lng['dashboard']="Рабочий стол";
$lng['closeBar']="Свернуть панель";
$lng['openBar']="Открыть панель";
$lng['layoutWidth']="Ширина экрана";
$lng['site']="Сайт"; 
$lng['goSite']="Перейти на сайт";
$lng['refreshCache']="Обновить кэш";
$lng['blockSite']="Отключить сайт";
$lng['blockSiteVk']="Включить сайт";
$lng['options']="Действия";
$lng['spname']="Справочники";
$lng['delete']="Удалить запись";
$lng['deleteAll']="Удалить";
$lng['edit']="Редактировать запись";
$lng['view_record']="Просмотр записи";
$lng['empty']="Очистить журнал";
$lng['empty_question']="Вы действительно хотите очистить журнал? В журнале сохранены все действия пользователей системы и эта статистика может понадобиться в дальнейшем!";
$lng['add']="Добавить запись";
$lng['addMass']="Массовая загрузка";
$lng['save']="Сохранить";
$lng['cancel']="Отменить";
$lng['continue']="Продолжить";
$lng['del_question']="Вы действительно хотите удалить запись?";
$lng['del_question_all']="Вы действительно хотите удалить выбранные записи?";
$lng['firstPage']="Первая страница";
$lng['lastPage']="Последняя страница";
$lng['previosPage']="Предыдющая страница";
$lng['nextPage']="Следующая страница";
$lng['records']="записей";
$lng['noRecord']="В таблице не найдено ни одной записи";
$lng['requiredField']="* Поле обязательно для заполнения";
$lng['requiredFieldPassword']="Длина пароля должна быть не менее 6 символов";
$lng['errorEmailRules']="Укажите корректный email адрес!";
$lng['deleteInfo']="Запись удалена";
$lng['deleteSinfo']="Отмеченные записи удалены";
$lng['addInfo']="Запись добавлена";
$lng['editInfo']="Редактирование записи выполнено успешно";
$lng['editInfoFalse']="При обновлении записи произошла ошибка, запись не обновлена";
$lng['editInfoFalsePass']="Запись не обновлена, старый пароль указан не верно";
$lng['menu']="Меню";
$lng['menu_name']="Пункт меню";
$lng['mainmenu']="Главное меню";
$lng['menu_url']="Адрес";
$lng['menu_site_map']="Обновить файл sitemap.xml";
$lng['root']="Родитель";
$lng['visible']="Доступность";
$lng['root_site']="Корень сайта";
$lng['root_node']="Корневой узел (0 уровень)";
$lng['rubric']="Рубрика";
$lng['rubrics']="Рубрики";
$lng['rubricSite']="Рубрики сайта";
$lng['importBook']="Импорт материалов";
$lng['infoImport']="Загрузите импортируемый архив в формате <b>*.zip</b> и нажмите кнопку <b>\"Отправить\"</b>:";
$lng['importOk']="Импрот записей выполнен успешно";
$lng['importActionOk']=" Операция импорта выполнена успешно, все файлы добавлены в базу библиотеки. ";
$lng['importActionNo']=" При импорте данных произошла ошибка. Проверьте наличие прав на запись в директорию портала, а также установленного расширения для работы с архивами *.zip и повторите операцию.";
$lng['importSend']="Отправить";
$lng['personname']="Персоны";
$lng['rubric_in_menu']="Рубрики меню";
$lng['rubric_in_menu_razdel']="Рубрики пункта меню";
$lng['menu_in_rubric']="Связанные пункты меню";
$lng['menu_in_rubric_razdel']="Связанные с рубрикой";
$lng['menu_in_rubric_razdel2']="пункты меню";
$lng['menu_position']="Позиция";
$lng['menu_text']="Описание раздела";
$lng['connection']="Привязка";
$lng['persons']="Персоны";
$lng['no_forwars']="Нет отсылки";
$lng['no_forwars_tooltip']=" Персона не имеет дублирования имени";
$lng['enciclopediya_yes']="является объектом энциклопедии";
$lng['enciclopediya_no']="не является объектом энциклопедии";
$lng['view_main_yes']="отображается на главной";
$lng['view_main_no']="не отображается на главной";
$lng['person_in_rubric']="Связанные рубрики";
$lng['person_citats']="Цитаты";
$lng['person_data']="Материалы, связанные с персоной";
$lng['person_fam_dp']="Материалы, связанные с персоной";
$lng['surname']="Фамилия";
$lng['patronymic']="Отчество";
$lng['fio']="ФИО";
$lng['men']="Мужчина";
$lng['women']="Женщина";
$lng['other_name']="Идентификатор";
$lng['person_years_life']="Годы жизни";
$lng['person_date_happy']="День рождения";
$lng['person_day']="День рождения";
$lng['person_month']="Месяц рождения";
$lng['person_year']="Год рождения";
$lng['person_is_bc']="Дата рождения до нашей эры?";
$lng['is_view_in_main']="Отображать на главной?";
$lng['person_forwars']="Отсылка";
$lng['person_text']="Текст страницы в энциклопедии";
$lng['person_definition']="Дефиниция";
$lng['person_fio_full_enc']="Полное ФИО";
$lng['person_date_full_enc']="Даты и места рождения и смерти";
$lng['org_definition']="Дефиниция";
$lng['short_text']="Короткий текст (вывод в правом блоке в календаре на главной)";
$lng['is_in_encyclopedia']="Объект энциклопедии?";
$lng['title']="Заголовок страницы (мета тег meta title)";
$lng['meta_description']="Описание страницы (мета тег meta decription)";
$lng['meta_keywords']="Ключевые слова (мета тег meta keywords)";
$lng['person_photo']="Фотография";
$lng['delete_photo']="Удалить фото? ";
$lng['person_url']="Адрес страницы";
$lng['person_in_rubric']="Рубрики, связанные с персоной";
$lng['citataname']="Цитаты";
$lng['citata_form_name']="Цитата";
$lng['citata_person']="Цитаты персоны";
$lng['person_in_data']="Материалы, связанные с персоной";
$lng['person_data']="Материал";
$lng['person_data_type']="Тип материала";
$lng['person_data_annotacia']="Аннотация";
$lng['person_data_type_person']="Тип авторства";
$lng['person_fio']="ФИО";
$lng['is_view_person_news']="Отобразить персону в разделе \"Новости\"";
$lng['person_is_portret']="Портрет";
$lng['newsname']="Новости";
$lng['newsrss']="RSS";
$lng['newssend']="Новостная рассылка";
$lng['news_redactor']="Добавил";
$lng['no_main_theme']="Нет главной темы";
$lng['news_text']="Текст новости";
$lng['news_is_main']="Отображать на главной? (Учитывается только для вида новостей \"Объявления\")";
$lng['news_annotaciya']="Аннотация новости";
$lng['news_zagolovok']="Заголовок новости";
$lng['date_add']="Дата создания";
$lng['date_from']="Дата начала";
$lng['date_to']="Дата окончания";
$lng['news_comment']="Комментарий редакторов";
$lng['news_url_page']="Адрес страницы";
$lng['news_main_theme']="Главная тема новости";
$lng['news_theme']="Тема новости";
$lng['news_vid']="Вид новости";
$lng['news_no_theme']="Нет темы";
$lng['news_no_vid']="Нет вида";
$lng['news_education']="Образование";
$lng['news_organisation']="Организация";
$lng['news_person']="Персона";
$lng['is_visible']="Видимость";
$lng['is_rss']="Включить новость в RSS-поток";
$lng['error_format_date']="Ошибка формата даты - дд.мм.год час:мин:сек ";
$lng['error_format_date_no_time']="Ошибка формата даты - дд.мм.год";
$lng['error_format_date_to_date']="Дата окончания меньше даты начала";
$lng['error_format_login_symbols']="Допустимые символы: числа от 0 до 9, буквы латинского алфавита и символ '_'";
$lng['error_format_login_count']="Такой логин уже существует, введите другой";
$lng['error_format_menu_url_count']="Запись с таким url уже существует, введите другой";
$lng['lib_book']="Книги";
$lng['lib_metabook']="Мета книги";
$lng['lib_article']="Статьи";
$lng['lib_filmstrip']="Диафильмы";
$lng['lib_news']="Новости";
$lng['lib_thesis']="Диссертационные материалы";
$lng['lib_thesis_short']="Дис. материалы";
$lng['lib_article_istochnik']="Источник";
$lng['lib_button_book']="Перейти к метакнигам";
$lng['lib_button_metabook']="Вернуться к книгам";
$lng['no_polnota_materials']="Полнота не установлена";
$lng['lib_polnota']="Полнота представления материала";
$lng['lib_in_book_rubric']="Привязка к рубрикам";
$lng['lib_in_book_person']="Привязка к персонам";
$lng['lib_mapping_full']="Полное содержание";
$lng['lib_mapping_small']="Краткое содержание";
$lng['lib_images_preview']="Изображения для просмоторщика";
$lng['lib_short_author']="Короткое имя автора (текстовое описание)";
$lng['lib_short_name']="Короткое заглавие";
$lng['lib_is_dot']="Ставить ли точку после заглавия?";
$lng['lib_annotaciya']="Аннотация";
$lng['lib_bo']="Библиографическое описание";
$lng['lib_bo_group']="Библиографическое описание статьи в продолжении";
$lng['lib_year']="Год издания";
$lng['lib_is_view_author']="Показывать ли в указателе авторов?";
$lng['lib_is_view_zaglavie']="Показывать ли в указателе заглавий?";
$lng['lib_is_search']="Искать ли по заглавию и содержанию?";
$lng['lib_is_add_year']="Добавлять ли в короткое имя год?";
$lng['lib_is_periodical']="Является ли периодическим?";
$lng['lib_is_recomended']="Рекомендованно к прочтению";
$lng['lib_url']="Адрес страницы";
$lng['lib_start_page']="Номер стартовой страницы в просмоторщике";
$lng['lib_materials']="Материалы, связанные с изданием";
$lng['lib_meta']="Прикрепить к метакниге";
$lng['lib_file']="Файл";
$lng['lib_delete_file']="Удалить файл";
$lng['lib_file_name']="Прикрепленный файл";
$lng['lib_author']="Автор(ы)";
$lng['lib_redactor']="Составитель(и), редактор(ы), переводчик(и)";
$lng['lib_ubilyar']="Персона (если материал посвящен кому-то)";
$lng['lib_is_stok']="В наличии(для раздела \"Ищу читателя\")?";
$lng['lib_metaname']="Короткое имя";
$lng['lib_meta_materials']="БО 1-го издания, история, знаки отличия, наличие в библиотеках, прочие материалы";
$lng['lib_meta_template']='
 <div class="dopmaterials">
 <p class="zag">Сведения об изданиях</p> 
 <p>Тект бибилиографического описания</p>
 <p class="history"><em>История</em>. Текст историческиго описания.</p>
 <p class="history"><em>Наличие в б-ках</em>: Текст о наличии в библиотеках.</p>
 <p class="history"><em>Знаки отличия</em>. Текст о знаках отличия.</p>
 </div>
 
<div class="dopmaterials">
  <p class="zag">Критические материалы</p>
  <p>Текст, ссылки и прочие критические материалы</p>
  </p>
</div>

<div class="dopmaterials">
<p class="zag">Решения, ответы</p>
<p>Текст, ссылки</p>
</div>
                        
<div class="dopmaterials">
<p class="zag">Методические материалы</p>
<p>Текст, ссылки</p>
</div>
';
 
$lng['lib_article_type']="Тип статьи";
$lng['lib_article_izdannie']="Сама по себе";
$lng['lib_article_chastj']="В составе издания";
$lng['lib_article_next']="Продолжение статьи";
$lng['lib_article_prev']="Предыдущая часть стаьи";
$lng['lib_book_art']="Источник";
$lng['lib_thesis_type1']="Кандидатская диссертация";
$lng['lib_thesis_type2']="Докторская диссертация";
$lng['lib_thesis_type3']="Автореферат";
$lng['lib_director']='Научный руководитель, консультанты';
$lng['lib_opponent']='Оппоненты';
$lng['lib_place_of_work']='Место выполнения работы';
$lng['lib_vid_dis']='Вид диссертации';
$lng['lib_thesis_dis']='Диссертация';
$lng['lib_thesis_referat']='Автореферат';
$lng['lib_dis_type']='Тип записи';
$lng['lib_list_dis']='Прикрепить к диссертации';
$lng['data_in_rubric']="Связанные рубрики";
$lng['data_in_images']="Изображения просмотрщика";
$lng['filter_reset']="Сбросить фильтр";
$lng['filter_reset_name']="Сбросить";
$lng['lib_zaglavie']="Заглавие";
$lng['img_open']="Открыть изображение";
$lng['uploadOk']="Загрузка файлов выполнена успешно";
$lng['lib_resurs_no_upload_file']="Для данного ресурса загруженных файлов не обнаружено";
$lng['data_in_mapping_1']="Полное содержание ресурса";
$lng['data_in_mapping_0']="Краткое содержание ресурса";
$lng['lib_static_page']="Статические разделы портала";
$lng['lib_static_page_short']="Стат. разделы";
$lng['static_no_punct_menu']="Нет";
$lng['yes']="Да";
$lng['menu_node']="Привязка к пунктам главного меню";
$lng['no_menu_node']="Нет привязки";
$lng['root_pege']="Родительский ресурс";
$lng['page_forwars']="Отсылка на страницу";
$lng['page_link_out']="Ссылка на внешний источник";
$lng['page_text']="Содержимое ресурса";
$lng['page_annotaciya']="Аннотация";
$lng['sp_org_day']="День создания";
$lng['sp_org_month']="Месяц создания";
$lng['sp_org_year']="Год создания";
$lng['sp_org']="Организации";
$lng['sp_org_in_rubric']="Рубрики, связанные с организацией";
$lng['is_view_sp_org_news']="Отобразить организацию в разделе \"Новости\"";
$lng['person_in_lib']="Персоны, связанные с ресурсом";
$lng['person_service_1']="Авторы";
$lng['person_service_1_hover']="Авторы";
$lng['person_service_2']="Редакторы";
$lng['person_service_2_hover']="Составитель(и), редактор(ы), переводчик(и)";
$lng['person_service_3']="Персоны";
$lng['person_service_3_hover']="Персона (если материал посвящен кому-то)";
$lng['person_service_4']="Научный руководитель";
$lng['person_service_4_hover']="Научный руководитель, консультанты";
$lng['person_service_5']="Оппонент";
$lng['person_service_5_hover']="Оппоненты";
$lng['person_no_link_type']="Для данного типа ресурса связи с персонами не предусмотрено";
$lng['lib_calendarj']="Календарь событий";
$lng['lib_calendarj_short']="Календарь";
$lng['calendar_date_from']="Начало";
$lng['calendar_date_to']="Окончание";
$lng['calendar_name']="Событие";
$lng['calendar_description']="Описание";
$lng['philter_calendar']="Фильтр";
$lng['philter_calendar_type1']="Cобытия календаря";
$lng['philter_calendar_type2']="Дни рождения персон";
$lng['philter_calendar_type3']="Даты создания организаций";
$lng['year_in_calendarj']="Отображение в календаре";
$lng['calendar_is_bc']="до нашей эры";
$lng['calendarj_organasation']="Организация";
$lng['calendarj_organasation_date_happy']="Дата создания";
$lng['calendar_is_bc']="Событие до нашей эры";
$lng['cal_repeat_no']="Никогда";
$lng['cal_repeat_day']="День";
$lng['cal_repeat_week']="Неделя";
$lng['cal_repeat_month']="Месяц";
$lng['cal_repeat_year']="Год";
$lng['cal_repeat_type']="Повторять";
$lng['cal_repeat_value']="Период повторения";
$lng['calendar_is_main']="Отображать на главной";
$lng['cal_philter_date']="Отобразить диапазон дат";
$lng['cal_philter_from']="от";
$lng['cal_philter_to']="до";
$lng['cal_url']="Ссылка на событие";
$lng['user_rolespage']="Группы пользователей";
$lng['user_page']="Пользователи";
$lng['user_name']="Имя";
$lng['user_fam']="Фамилия";
$lng['user_otch']="Отчество";
$lng['user_is_active']="Активность";
$lng['user_roles']="Группы безопасности";
$lng['user_in_roles']="Привязка пользователя к группам безопасности";
$lng['user_email']="E-mail";
$lng['user_phone']="Телефон";
$lng['user_prioritet']="Приоритет";
$lng['password_new']="Новый пароль";
$lng['password_old']="Старый пароль";
$lng['user_in_roles_page']="Привязка пользователя к группам безопасности";
$lng['user_in_roles_page_login']="Привязка пользователя к группам безопасности c логином";
$lng['user_roles_group']="Группа безопасности";
$lng['user_roles_code']="Код группы";
$lng['user_profile']="Профиль пользователя";
$lng['user_profile_update']="Обновить";
$lng['predprint']="Препринт";
$lng['predprint_user']="Препринт, на рассмотрении документов - ";
$lng['predprint_user']="Препринт, на рассмотрении документов - ";
$lng['predprint_resurs']="Ресурс на обработке в модуле Препринт";
$lng['predprint_this_user']="Ресурсы на утвеждении у Вас";
$lng['predprint_other_user']="Ресурсы на утвеждении другими пользователями";
$lng['predprint_other_user_obrabotka']="находится на обработке пользователем c логином";
$lng['predprint_remove']="Удалить ресурс из препринта";
$lng['lib_feedback']="Обратная связь";
$lng['predprint_name']="Ресурс";
$lng['predprint_type']="Тип ресурса";
$lng['predprint_data']="Раздел библиотеки";
$lng['predprint_news']="Новости";
$lng['print_is_done']="Утвердить ресурс";
$lng['print_comment']="Ваш комментарий к ресурсу";
$lng['print_act_type']="Действия";
$lng['print_act_type1']="Сохранить свой комментарий и продолжить работу с ресурсом без передачи ресурса другому редактору";
$lng['print_act_type2']="Отклонить ресурс и передать на дальнейшую обработку предыдущему редактору";
$lng['print_act_type3']="Утвердить ресурс и передать на дальнейшую обработку другому редактору";
$lng['print_act_type3_end']="Утвердить ресурс и удалить ресурс из модуля \"Препринт\"";
$lng['print_comment_redactor']="Комментарии редакторов";
$lng['print_comment_update']="комментарий обновлен";
$lng['text_comment']="Комментарий";
$lng['print_comment_update']="дата";
$lng['print_no_comment']="Нет комментариев к ресурсу";
$lng['fast_enter']="Быстрый доступ";
$lng['predprint_all_resurs']="Общее количество ресурсов, находящихся на утверждении редакторами в модуле \"Препринт\" равно ";

$lng['server_config']="Настройка сервера";
$lng['rss_name']="Управление RSS-потоком";
$lng['updateRSSSet']="Настройки канала";
$lng['updateRSS']="Обновить поток";
$lng['rss_pudDate']="Дата создания канала";
$lng['rss_zagolovok']="Заголовок ленты";
$lng['rss_link']="URL-сайта канала";
$lng['rss_description']="Описание канала";
$lng['rss_copyright']="Копирайт (авторские права на канал)";
$lng['rss_managingEditor']="Email ответственного за содержание RSS-канала";
$lng['rss_webMaster']="Email вебмастера";
$lng['rss_category']="Категория, к которой принадлежит канал";
$lng['rss_number']="Кол-во новостей, включаемых в канал";
$lng['updateSetRSSFalse']="Ошибка обновления настроек канала";
$lng['updateSetRSSTrue']="Настройки канала обновлены";
$lng['rss_last_update']="Времы последнего обновления канала";

$lng['news_send_user_page']="Управление подписчиками";
$lng['rassilka_send_page']="Рассылка";
$lng['rassilka_send_tema']="Рассылка сообщения на тему ";
$lng['rassilka_action']="Отправка сообщений";
$lng['rassilka_action2']="Начать отправку";
$lng['rassilka_action3']="Начаю отправку рассылки...";
$lng['rassilka_sender']="Отправить подписчикам";
$lng['rassilka_sender_update']="Повторить отправку подписчикам";
$lng['rassilka_new_status']="Рассылка подготовлена и готова к отправке";
$lng['rassilka_new1_status']="Готова к отправке";
$lng['rassilka_error_status']="При предыдущей отправке произошла ошибка. Повторите отправку";
$lng['rassilka_error1_status']="Ошибка отправки";
$lng['rassilka_send_status']="Рассылка отправлена подписчикам и переведена в архив";
$lng['rassilka_send1_status']="Отправлена и переведена в архив";
$lng['rassilka_sender_user']="Список пользователей, кому было отправленна данная рассылка";
$lng['rassilka_deactivate']="Для удаления своего адреса из списка рассылки активируйте ссылку:";
$lng['rassilka_status']="Отправить подписчикам";
$lng['rassilka_setting']="Настройки рассылки";
$lng['rassilka_setting_name_author']="Имя автора отправки сообщения";
$lng['rassilka_setting_adress_author']="Адрес автора отправки сообщения";
$lng['rassilka_setting_adress_server']="Адрес сервера отправки сообщения";
$lng['rassilka_setting_save']="Сохранить";
$lng['rassilka_setting_type']="Тип отправки сообщений";
$lng['rassilka_setting_type0']="Рассылка сообщений без ограничений";
$lng['rassilka_setting_type1']="Рассылка сообщений с ограничениями (пакетами)";
$lng['rassilka_setting_time_paket']="Время на рассылку одного пакета (мин)";
$lng['rassilka_setting_kolvo_pisem']="Кол-во писем в пакете";
$lng['rassilka_setting_kolvo_adress']="Кол-во адресатов";
$lng['rassilka_setting_text']="Тип отправки ";
$lng['rassilka_setting_kolvo_paket']="Кол-во пакетов";
$lng['rassilka_user_rass']="Подписчики, которым была отправлена рассылка";
$lng['rassilka_setting_html']="Отправлять в формате HTML";
$lng['rassilka_mess1']="отправка сообщения пользователю";
$lng['rassilka_mess_ok']="письмо отправленно";
$lng['rassilka_mess_error']="ошибка отправки письма";
$lng['rassilka_set_status_ok']='<p><b>Сообщения отправлены, рассылка переведена в архив!</b></p>';
$lng['rassilka_send_user_ok']="Успешно отправлено";
$lng['rassilka_send_user_error']="Ошибка  отправки";
$lng['rassilka_set_status_error']='<p><b>Сообщения отправлены, но рассылка не переведена в архив!</b></p>';
$lng['rassilka_setting_end_letter']="Окончание письма рассылки";
$lng['rassilka_setting_about']="О рассылке";
$lng['rassilka_setting_info_after_sender']="Информация для пользователя после подписки";
$lng['rassilka_setting_appeal_to_men']="Обращение к мужчине";
$lng['rassilka_setting_appeal_to_women']="Обращение к женщине";
$lng['setting_all_system']="Общесистемные настройки";
$lng['setting_lang_redact']="Управление словарями";
$lng['setting_lang_key']="Ключ";
$lng['setting_lang_value']="Значение";
$lng['setting_type_date']="Тип ресурса";
$lng['setting_type_name']="Имя ключа";
$lng['setting_type_value']="Значение";
$lng['portal_panel']="Портал";
$lng['admin_panel']="Административная панель";
$lng['setting_sistem']="Настройки системы";
$lng['setting_code_countOnPage']="Количество результатов, выводимое на портале";
$lng['setting_code_countOnPagePersonOrganisation']="Количество результатов, выводимое на портале в разделе Персоны и Организации";
$lng['setting_code_isPredPrint']="Модуль препринт (использовать в работе редакторов?)";
$lng['setting_code_countNewsMain']="Количество новостей, выводимое на главной странице";
$lng['setting_code_showCountMonthSystemGurnalDefault']="Количество месяцев, выводимое по умолчанию в журнале системы управления";
$lng['setting_code_CountVidNewsJournal']="Количество новостей, выводимое на главной странице в разделе \"Дневник проекта\"";
$lng['setting_code_CountVidNewsAdvert']="Количество новостей, выводимое на главной странице в разделе \"Объявления\"";
$lng['setting_code_CountDataBookReader']="Количество книг, отображаемое на главной странице в разделе \"Стоит прочесть\"";
$lng['setting_code_StaticMainPage']="Список статических разделов, для вывода на главной странице";
$lng['setting_code_emailAdmin']="Email администратора сайта (используется для приема сообщений через форму обратной связи)";         
$lng['setting_code_ListMenuMain']="Список пунктов меню для вывода на главной";
$lng['setting_code_IdVidNewsMain']="ID-вида новости, выводимого на главной странице в центральной части";
$lng['setting_code_IdVidNewsMainRight']="ID-вида новости, выводимого на главной странице в правой части";
$lng['setting_code_ListMenuMainLimit']="Ограничение количества пунктов меню для вывода на главной";
$lng['setting_code_EncyclopediaMenuType1']="Энциклопедия, тип меню 1 (аналог раздела Математика, вывод 2-го уровня как фильтр, на странице алфавитный указатель)";
$lng['setting_code_EncyclopediaMenuType2']="Энциклопедия, тип меню 2 (аналог раздела Педагогика, вывод 2-го уровня не предусмотрен, обработчик - статические разделы, на странице алфавитный указатель)";
$lng['setting_code_EncyclopediaMenuType3']="Энциклопедия, тип меню 3 (аналог раздела Хронология, вывод 2-го уровня не предусмотрен, вывод алфавитных фильтров на странице не предусмотрен)";
$lng['setting_code_EncyclopediaMenuType4']="Энциклопедия, тип меню 4 (аналог раздела События, вывод 2-го уровня предусмотрен, обработчик - статические разделы, вывод алфавитных фильтров на странице не предусмотрен)";
$lng['setting_code_EncyclopediaMenuCalendarj']="Энциклопедия, меню Календарь";
$lng['setting_code_EncyclopediaMenuPerson']="Энциклопедия, меню Персоны";
$lng['setting_code_EncyclopediaMenuOrg']="Энциклопедия, меню Организации";
$lng['setting_code_countOnListInEnciclopediaStaticRazdel']="Энциклопедия, количество элементов в столбце на статических страницах с алфавитными фильтрами";
$lng['setting_code_countOnPageAuthor']="Библиотека, Авторы, количество элементов в столбце";

$lng['setting_type_lib']="Библиотека";
$lng['dashboard_is_block_ok']="Сайт отключен для посетителей на регламентные работы.";
$lng['dashboard_is_block_no']="Сайт включен и доступен для посетителей.";
$lng['dashboard_is_open']="Конфигурационный файл портала открыт для записи!";
$lng['dashboard_is_open_db']="Конфигурационный файл подключения к базе данных портала открыт для записи!";
$lng['dashboard_is_open_recomend']="Исправьте права на запись этих фалов!";
$lng['dashboard_is_error']="В настройках системы присутствуют ошибки";
$lng['dashboard_is_no_error']="В настройках системы ошибок не обнаружено";
$lng['dashboard_in_correction']="Проверка конфигурации";
$lng['dashboard_in_download']="Информация о загрузке ресурсов";
$lng['dashboard_in_download_day']="За день";
$lng['dashboard_in_download_work']="За неделю";
$lng['dashboard_in_download_month']="За месяц";
$lng['dashboard_in_download_statistic']="Статистика загрузок";
$lng['dashboard_in_download_statistic_view']="Статистика загрузок материалов";
$lng['server_feedback']="Обратная связь";
$lng['server_feedback_act']="Управление сообщениями посетителей сайта, отправленными через форму обратной связи";
$lng['server_feedback_name_user']="Контактное лицо";
$lng['server_feedback_status_mess']="Статус сообщения";
$lng['server_feedback_redactor']="Кем обработано";
$lng['server_feedback_status_s1']="Новое сообщение";
$lng['server_feedback_status_s2']="Сообщение обработано";
$lng['server_feedback_block_email']="Заблокированные email-адреса";
$lng['server_feedback_message']="Текст сообщения";
$lng['server_ban_email']="Внутренняя база спам адресов";
$lng['server_arhivs']="Резервирование данных";
$lng['server_file_name']="Название файла";
$lng['server_file_type']="Тип файла";
$lng['server_file_type1']="Архив с файлами сайта";
$lng['server_file_type2']="Дамп базы данных";
$lng['server_file_size']="Размер файла";
$lng['download']="Скачать";
$lng['server_add_arhivs_zip']="Создать новый архив";
$lng['server_add_arhivs_db']="Создать новый дамп базы данных";
$lng['creatFile_ok']="Файл создан";
$lng['creatFile_no']="Ошибка создания файла, файл не создан";
$lng['system_gurnal_name']="Журнал системы управления";

$lng['system_gurnal_date']="Дата операции";
$lng['system_gurnal_id_user']="id пользователя";
$lng['system_gurnal_login']="Логин пользователя";
$lng['system_gurnal_action']="URL-действия";
$lng['system_gurnal_ip']="IP-компьютера";
$lng['system_gurnal_view']="Просмотр записи";
$lng['system_gurnal_del']="Очистить журнал";
$lng['emptyGurnal_ok']="Журнал очищен";
$lng['emptyGurnal_no']="Ошибка очистки журнала";
$lng['system_gurnal_db_old']="Состояние записи до действия";
$lng['system_gurnal_db_new']="Состояние записи после действия";
$lng['system_gurnal_range_date']="Отобразить системные события за период";


#--------------------------
$lng['month.jan']="Январь";
$lng['month.feb']="Февраль";
$lng['month.mar']="Март";
$lng['month.apl']="Апрель";
$lng['month.may']="Май";
$lng['month.iun']="Июнь";
$lng['month.iul']="Июль";
$lng['month.avg']="Август";
$lng['month.sen']="Сентябрь";
$lng['month.okt']="Октябрь";
$lng['month.nob']="Ноябрь";
$lng['month.dec']="Декабрь";

$lng['month.janr']="января";
$lng['month.febr']="Февраля";
$lng['month.marr']="марта";
$lng['month.aplr']="апреля";
$lng['month.mayr']="мая";
$lng['month.iunr']="июня";
$lng['month.iulr']="июля";
$lng['month.avgr']="августа";
$lng['month.senr']="сентября";
$lng['month.oktr']="октября";
$lng['month.nobr']="ноября";
$lng['month.decr']="декабря";
$lng['y_v']=" г. в";
#--------------------------


#----------------------------------
#table name column, form name

#----------------------------------
#main menu
$lng['main_menu_resurs']="Ресурсы";
$lng['main_fast_acceess']="Быстрый доступ к основным ресурсам портала";
$lng['main_menu_sp']="Справочники";
$lng['main_menu_sp_education']="Образование";
$lng['main_menu_sp_organasation']="Организации";
$lng['main_menu_sp_theme_news']="Темы новостей";
$lng['main_menu_sp_vid_news']="Виды новостей";


#---------------------------------

#person
$lng['form_thead_fam_dp']="Фамилия и инициалы";
$lng['form_thead_name']="Имя";
$lng['form_thead_fio']="ФИО";
$lng['form_thead_is_view_in_main']="Отображать на главной";
$lng['form_thead_url']="Адрес";
$lng['form_thead_is_in_encyclopedia']="Объект энциклопедии";
$lng['form_thead_forwars']="Отсылка";
$lng['form_thead_is_visible']="Видимость";
$lng['form_thead_position']="Позиция";
$lng['form_thead_date_add']="Дата создания";
$lng['form_thead_zagolovok']="Заголовок";
$lng['form_thead_annotacia']="Аннотация";
$lng['form_thead_is_main']="Главная тема";
$lng['form_thead_is_rss']="Включать в RSS-поток";
$lng['form_thead_main_theme']="Главная тема";
$lng['form_thead_sp_polnota']="Полнота";
$lng['form_thead_view_in_rubrik']="Доступность";
$lng['form_thead_view_author']="Отображать в разделе \"Авторы\"";
$lng['form_thead_vid_dis']="Вид диссертации";
$lng['form_thead_is_periodical']="Переодическое";
$lng['form_thead_is_recomended']="Стоит прочесть";
$lng['form_thead_is_in_stock']="Ищу читателя";
$lng['form_thead_name_punct']="Пункт меню";
$lng['form_thead_images']="Изображение";
$lng['form_thead_puthimage']="Полный путь к изображению";
$lng['form_thead_map_name']="Название раздела";
$lng['form_thead_map_number_viewer']="Номер страницы в просмотрщике";
$lng['form_thead_map_number_viewer_add']="Номер страницы в просмотрщике, либо диапазон страниц через знак \"-\" (например 4-11)";
$lng['form_thead_map_number_real']="Номер страницы в печатном издании";
$lng['form_thead_map_number_real_add']="Номер страницы в печатном издании, либо диапазон страниц через знак \"-\" (например 4-11)";
$lng['form_thead_map_file']="Файл изображения";
$lng['form_thead_name_page']="Название страницы";
$lng['form_thead_punct_menu']="Пункт главного меню?";
$lng['form_thead_punct_is_menu']="Отображать в меню?";
$lng['form_thead_sp_org_name']="Название";
$lng['form_thead_sp_org_is_main']="Отображать на главной";
$lng['form_thead_sp_org_name']="Название";
$lng['form_thead_id_record']="id-записи";
$lng['form_thead_cal_main']="На главной";

//lib----------------------------------------------
$lng['form_thead_lib_book_name']="Короткое имя ";
$lng['form_thead_lib_date_add']="Дата добавления ";
$lng['form_thead_user_roles_name']="Название группы";
$lng['form_thead_user_roles_code']="Код (на латинском)";
$lng['form_thead_user_send_name']="Имя пользователя";
$lng['form_thead_user_send_email']="Email";
$lng['form_thead_user_send_sex']="Пол";
$lng['form_thead_user_is_active']="Активность";
$lng['form_thead_is_black_list']="Черный список";
$lng['form_thead_ras_theme']="Тема";
$lng['form_thead_ras_body']="Текст рассылки";
$lng['form_thead_date_add']="Дата создания";
$lng['form_thead_date_send']="Дата отправки";
$lng['form_thead_rassilka_status']="Статус рассылки";
$lng['form_thead_']="";

#----------------------------------
?>