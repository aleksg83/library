<?php
/*
 * языковые константы внешние части портала
 */
$lng = array();
$lng['portal.name']="Портал : Математическое образование";
$lng['portal.logo_name']="Математическое образование";
$lng['portal.main_page']="Главная";
$lng['portal.logo_save']="сохранить";
$lng['portal.logo_multiply']="приумножить";
$lng['portal.logo_transmit']="передать";
$lng['portal.bottom_uptext']="наверх";
$lng['portal.bottom_text']="Математическое образование, 2006-".date("Y").".  Все права защищены.<br/>Портал зарегистрирован как СМИ. Свидетельство ЭЛ 12-4563.";
$lng['displayed_on_the_page']="Выводить на странице";
$lng['records_on_the_page']="записей";
$lng['a-ya']="A-Я";
$lng['sort']="Сортировать";
$lng['abs']="по алфавиту";
$lng['time']="по хронологии";
$lng['up']="по возрастанию";
$lng['view_list']="Показывать списки";
$lng['no_img']="без картинок";
$lng['img']="с картинками";
$lng['no_img_art']="без описаний";
$lng['img_art']="c описаниями";
$lng['portal.error_title']="Запрашиваемая страница не найдена";
$lng['portal.error_404']="Запрашиваемый ресурс недоступен или был удален!";
$lng['portal.block_site_title']="Портал закрыт для проведения технических работ, извините за предоставленные неудобства.";
$lng['portal.block_site_text']="Портал временно закрыт для проведения технических работ.<br/>
    Извините за предоставленные неудобства.";
$lng['portal.main_meta_title']="Главная страница портала";
$lng['portal.main_meta_description']="Портал Математическое образование";
$lng['portal.main_meta_keywords']="математическое образование, литература по математике";
$lng['portal.news']="Новости";
$lng['portal.main-title']="Главная страница";
$lng['portal.main-descrition']="портал о математическом образовании в России, истории математики";
$lng['portal.main-keywords']="история математики, математическое образование в России";
$lng['portal.reader']="Стоит прочесть";
$lng['portal.calendarj']="Календарь истории";
$lng['portal.year']="лет";
$lng['portal.in_year']="В этом году";
$lng['portal.news']="Новости";
$lng['portal.news_rss']="RSS-подписка";
$lng['portal.news_meta_description']="новости портала математическое образование";
$lng['portal.news_meta_keywords']="";
$lng['portal.news_theme_no_count']="По выбранной теме новостей не найденно.";
$lng['portal.go_back']="Назад";
$lng['portal.lib_meta_title']="Библиотека";
$lng['portal.lib_meta_description']="Портал Математическое образование, библиотека";
$lng['portal.lib_meta_keywords']="математическое образование, литература по математике, электронная библиотека";
$lng['portal.lib.donwload']="Загрузить издание";
$lng['portal.lib.bo']="Библиографическое описание";
$lng['portal.lib.bo_izd']="Библиографическое описание издания";
$lng['portal.lib.info']="Информация обо всех изданиях";
$lng['portal.lib.soderganie']="Содержание";
$lng['portal.lib.material']="Материалы, связанные с изданием";
$lng['portal.news_share']="Поделиться";
$lng['portal.news_comment']="Комментарий Mathedu";
$lng['portal.news_material_theme']="Материалы по теме";
$lng['portal.rassulka']="Подписка на новости";
$lng['portal.name_user']="Ваше имя";
$lng['portal.email_user']="Ваш e-mail";
$lng['portal.sender_theme']="Подписка на рассылку";
$lng['portal.sender_body_start']="Уважаемый(ая)";
$lng['portal.sender_body_next']="Вы подписались на новостную рассылку сайта <a href=\"".SITE_URL."\">".SITE_URL."</a>. </br> Для активации Вашей подписки пройдите по ссылке ";
$lng['portal.sender_body_end']="Если Вы не подписывались на данную рассылку не отвечайте на это письмо";
$lng['portal.user_activate']="Активация подписки";
$lng['portal.user_no_activate']="Ошибка активации / дезактивации подписки";
$lng['portal.user_yes_activate']="активирован";
$lng['portal.user_dez_activate']="дезактивирован";
$lng['portal.user_activate_error_parametr']="Ошибка передачи параметров";
$lng['portal.user_email_no_db_st']="Указанный email ";
$lng['portal.user_email_no_db']="в базе подписчиков не обнаружен";
$lng['portal.user_activate_sex']=" Для завершения активации подписки укажите Ваш пол:";
$lng['portal.user_sex_men']=" Мужской";
$lng['portal.user_sex_women']=" Женский";
$lng['portal.user_sex_next']=" Продолжить";

$lng['portal.feedback_admin']="Администратору";
$lng['portal.feedback_theme']="Сообщение";
$lng['portal.calendar_title']="Календарь истории";
$lng['portal.calendar_year']="год";
$lng['portal.calendar_organization_ubilei']=" co дня основания";
$lng['portal.calendar_person_ubilei']= " co дня рождения";
$lng['portal.calendar_date_no']="Точная дата неизвестна";
$lng['portal.calendar_dafault']="<p>Дефолтный текст для календаря. Содержит пояснительную записку к календарю.</p>";
$lng['portal.enciclopediya_ukazatelj']="Алфавитный указатель всех статей";
$lng['portal.enciclopediya_sokr_years']="гг.";
$lng['portal.enciclopediya_sokr_year']="г.";
$lng['portal.enciclopediya_sokr_rog']="г.";
$lng['portal.enciclopediya_sokr_do_n_eru']="до н.э.";
$lng['portal.lib_author_title']="Авторы";
$lng['portal.lib_author_name']="Авторы";
$lng['portal.lib_name']="Библиотека";
/*----------------------------------------------------*/
$lng['month.janr']="января";
$lng['month.febr']="февраля";
$lng['month.marr']="марта";
$lng['month.aplr']="апреля";
$lng['month.mayr']="мая";
$lng['month.iunr']="июня";
$lng['month.iulr']="июля";
$lng['month.avgr']="августа";
$lng['month.senr']="сентября";
$lng['month.oktr']="октября";
$lng['month.nobr']="ноября";
$lng['month.decr']="декабря";


$lng['month.jan']="Январь";
$lng['month.feb']="Февраль";
$lng['month.mar']="Март";
$lng['month.apl']="Апрель";
$lng['month.may']="Май";
$lng['month.iun']="Июнь";
$lng['month.iul']="Июль";
$lng['month.avg']="Август";
$lng['month.sen']="Сентябрь";
$lng['month.okt']="Октябрь";
$lng['month.nob']="Ноябрь";
$lng['month.dec']="Декабрь";



?>