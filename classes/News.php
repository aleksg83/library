<?php
class News {
    
    private $db;
    private $settings = array();
    private $id_sp_type_data = 5;
    private $params;
    private $dopParams;
    private $request;
    private $currentNews;
    private $auth;
    private $preprintSQL, $preprintSQL2, $preprintSQLSingle;
    
    public function __construct($params, $dopParams) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->params = $params;
        $this->dopParams = $dopParams;  
        $this->auth = auth::instance();
        $this->selectSettings();   
        $this->preparePreprintSQL();
    }
    
    public function getListNews(){
        if($this->dopParams['page']){
            $start = $this->settings['countOnPage']*($this->dopParams['page']-1);        
            $lim = ' LIMIT '.($start).', '.($start+$this->settings['countOnPage']);
        }elseif($this->settings['countOnPage']){
            $lim = ' LIMIT 0, '.$this->settings['countOnPage'];
        }
        if($this->dopParams['vid']){
            $q = " and v.id_sp_vid_news = {$this->dopParams['vid']} ";
        }
        
        if(count($this->params)){            
            $sth = $this->db->prepare("SELECT CONCAT('{$this->request}', '/', n.`url`) as url1, DATE_FORMAT(n.`date`, '%d.%m.%Y | %H:%i') as `date_n` , n.* FROM ".PREFIX."_news n
                                        left join ".PREFIX."_rel_news_sp_theme_news rn using(id_news)
                                        left join ".PREFIX."_sp_theme_news sn using(id_sp_theme_news)
                                        left join ".PREFIX."_rel_news_sp_vid_news v using(id_news)
                                    WHERE sn.url = ?
                                    {$q}
                                    {$this->preprintSQL}
                                    order by `date` desc {$lim}");                    
            $sth->bindParam(1, $this->params[0], PDO::PARAM_STR);
            $sth->execute();
        }else{
            $sth = $this->db->query("SELECT 
                                        CONCAT('{$this->request}', '/', n.`url`) as url1, 
                                        DATE_FORMAT(n.`date`, '%d.%m.%Y | %H:%i') as `date_n`, 
                                        n.* 
                                    FROM ".PREFIX."_news n    
                                        left join ".PREFIX."_rel_news_sp_vid_news v using(id_news)
                                    where 1=1
                                       {$this->preprintSQL2} {$q} 
                                    order by `date` desc 
                                    {$lim}");
        }    
        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $key=>$result){
            preg_match_all("#[\d{2}].(\d{2}).[\d{2}]#", $result['date_n'], $month);       
            $results[$key]['date_n'] = str_replace('.'.$month[1][0].'.', ' '.Common::getMonthName($month[1][0]).' ', $result['date_n']);
        }
        return $results;
    }
    
    public function getSingleNews($url){
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_news where url = ? {$this->preprintSQLSingle}");        
        $sth->bindParam(1, $url, PDO::PARAM_STR);
        $sth->execute();
        $news = $sth->fetch(PDO::FETCH_ASSOC);
        $this->currentNews = $news['id_news'];
        return $news;
    }
    
     public function getListNewsMainPage(){
        $count = $this->settings['countNewsMain'];
        $sth = $this->db->query("SELECT date, url, zagolovok FROM ".PREFIX."_news 
                                        WHERE 
                                            is_visible='1' 
                                            {$this->preprintSQLSingle}
                                            ORDER BY date desc LIMIT {$count}");        
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        $date=date("Y-m-d");
        for($i=0; $i<count($result);$i++){
            $day=new DateTime($result[$i]['date']);
            $dateDay=$day->format('Y-m-d');
            $dateTimeShablon=$day->format('H:i');
            $dateDayShablon=$day->format('d.m');
            $result[$i]['date'] = ($dateDay == $date)? $dateTimeShablon : $dateDayShablon ;
            $result[$i]['date-class'] = ($dateDay == $date)? "new" : "year" ;
            $result[$i]['class'] = (strlen($result[$i]['zagolovok'])>184) ? ' class="two"' : ""; 
        }
        return $result;
    }
    
    
    public function getRelatedNews($urlTheme){
        
        $sth = $this->db->prepare("SELECT CONCAT('{$this->request}', '/', n.`url`) as url1, DATE_FORMAT(n.`date`, '%d.%m.%y') as `date_n` , n.* FROM ".PREFIX."_news n 
                                      left join `pharus_rel_news_sp_theme_news` rn using(id_news)
                                      left join `pharus_sp_theme_news` t using(id_sp_theme_news)
                                   WHERE t.url = ? and n.id_news != {$this->currentNews} 
                                      {$this->preprintSQL}    
                                    order by n.`date` desc limit 0,3");
        $sth->bindParam(1, $urlTheme, PDO::PARAM_STR);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC); 
    }
    
    public function getVids(){
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_sp_vid_news WHERE is_visible = 1 ORDER by position");
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)){
            $result[$row['id_sp_vid_news']] = $row;
            $result[$row['id_sp_vid_news']]['relatedNews'] = $this->getVidNews($row['id_sp_vid_news']);
        }
        return  $result;
    }
    
    public function getCountData(){
        if(count($this->params)){
            $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_news n
                                        left join ".PREFIX."_rel_news_sp_theme_news rn using(id_news)
                                        left join ".PREFIX."_sp_theme_news sn using(id_sp_theme_news)
                                    WHERE sn.url = ?         
                                    {$this->preprintSQL}
                                    order by `date` desc");
            $sth->bindParam(1, $this->params[0], PDO::PARAM_STR);
            $sth->execute();
        }else{
            $sth = $this->db->query("SELECT 
                                        count(*) 
                                    FROM ".PREFIX."_news n where 1=1
                                    {$this->preprintSQL2} 
                                    order by `date` desc");
        }          
        return array_shift($sth->fetch(PDO::FETCH_ASSOC));
    }
    
    public function getSetting(){
        return $this->settings;
    }
    
    public function setRequest($request){
        $tmp = explode("/", $request);   
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            array_pop($tmp);
        }        
        $this->request = implode("/", $tmp);        
    }
    
    public function getOrganizaciya(){
        $sth = $this->db->query("select id_sp_organasation, name from ".PREFIX."_sp_organasation ORDER by position");
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getPerson(){
        $sth = $this->db->query("select id_person, fio from ".PREFIX."_person");
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getObrazovanie(){
        $sth = $this->db->query("select id_sp_education, name from ".PREFIX."_sp_education ORDER by position");
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getTheme(){
        $sth = $this->db->query("select id_sp_theme_news, name from ".PREFIX."_sp_theme_news ORDER by position");
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------     
    private function selectSettings(){
        $sth=$this->db->prepare("SELECT `name`, `value` FROM ".PREFIX."_main_setting WHERE id_sp_type_data = ?");
        $sth->bindParam(1, $this->id_sp_type_data, PDO::PARAM_INT);
        $sth->execute();
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->settings[$row['name']] = $row['value'];
        }
    }
    private function getVidNews($idVid){
        $sth = $this->db->prepare("SELECT 
                                       CONCAT('{$this->request}', '/', n.`url`) as url1,
                                       n.* 
                                   FROM ".PREFIX."_news n
                                       LEFT JOIN ".PREFIX."_rel_news_sp_vid_news vn using(id_news)
                                   WHERE  vn.id_sp_vid_news = ? 
                                        {$this->preprintSQL} 
                                   ORDER by n.`date` desc 
                                   LIMIT 0, 3");
        $sth->bindParam(1, $idVid, PDO::PARAM_STR);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    } 
    
    #---------------------------------------------------------------------------
    public function getListMainNewsDopSection($idVid, $typeSection){
        $count= ($typeSection=="journal")? $count = $this->settings['CountVidNewsJournal'] : $count = $this->settings['CountVidNewsAdvert'];
        $listNews=$this->getListVidNews($idVid, $count, $typeSection);
        for($i=0; $i<count($listNews); $i++) {
            $listNews[$i]['annotaciya']=  stripslashes($listNews[$i]['annotaciya']);
        }
        $return=(count($listNews) >0)? array("name"=>$this->getNameVidNews($idVid), "url" =>SITE_URL.URL_NEWS, "news"=> $listNews, "view"=>"yes") : array("view"=>"no") ; 
        return $return;
    }
    #---------------------------------------------------------------------------
    private function getNameVidNews($id){
        $sth = $this->db->prepare("select * from ".PREFIX."_sp_vid_news where is_visible='1' and id_sp_vid_news=?");
        $sth->bindParam(1, $id, PDO::PARAM_STR);
        $sth->execute();
        $result=$sth->fetch(PDO::FETCH_ASSOC);
        return $result['name'];
    }
    #---------------------------------------------------------------------------
    private function getListVidNews($idVid, $count, $type="journal"){
        if ($type=="notice") $dopSql=" and n.is_main=1 ";
        else {$dopSql="";}
        
        $sth = $this->db->prepare("SELECT 
                                          DATE_FORMAT(n.`date`, '%d.%m.%y') as `date_n` , n.`url`, n.`zagolovok`, n.`annotaciya` 
                                   FROM ".PREFIX."_news n 
                                         LEFT JOIN ".PREFIX."_rel_news_sp_vid_news vn using(id_news)
                                   WHERE  
                                          vn.id_sp_vid_news = ? {$dopSql} 
                                          {$this->preprintSQL}   
                                   ORDER by n.`date` desc 
                                   LIMIT 0, ".intval($count)." ");
                                 
                                          
        $sth->bindParam(1, $idVid, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    } 
    #---------------------------------------------------------------------------
     public function getMetaData($lang){
         $return=array();
         if ($this->params[0]==""){
             $return['metatitle'] = $this->lang['portal.name']." : ".$this->lang['portal.news'];
             $return['metadescription'] = $this->lang['portal.news_meta_description'];
             $return['metakeywords'] = $this->lang['portal.news_meta_keywords'];
         }else{
               $sth = $this->db->query("select name from ".PREFIX."_sp_theme_news WHERE url='".$this->params[0]."'");
               $inform = $sth->fetch(PDO::FETCH_ASSOC);
               $return['metatitle'] = $lang['portal.name']." : ".$lang['portal.news']." : ".$inform['name'];
         }
         
         return $return;
     }
     
     #--------------------------------------------------------------------------
     public function preparePreprintSQL(){
          if (!$this->auth->isLogged) {
              
              $this->preprintSQL=" and NOT EXISTS (
                                                        SELECT 
                                                            id_news 
                                                        FROM ".PREFIX."_rel_users_pred_print rup
                                                        WHERE 
                                                            rup.id_news=n.id_news and rup.is_done=0
                                                     )";
    
              $this->preprintSQL2=" and NOT EXISTS (
                                                        SELECT 
                                                            id_news 
                                                        FROM ".PREFIX."_rel_users_pred_print rup
                                                        WHERE 
                                                            rup.id_news=n.id_news and rup.is_done=0
                                                     )";
              
              $this->preprintSQLSingle='and id_news NOT IN (select id_news FROM '.PREFIX.'_rel_users_pred_print where is_done=0)';
              
          }
     }
     
     
     
     
}

?>
