<?php

class Sputnik {
    private $db;
    private $params;
    private $currentPage;
    
    public function __construct($params) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->params = $params;
    }
    
    public function setRequest($request){
        $tmp = explode("/", $request);   
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            array_pop($tmp);
        }        
        $this->request = implode("/", $tmp);   
    }
    
    
    public function getPage(){
       $tmp = explode("/", $this->request);
       $infoTopPage=$this->getInfoTopPage($tmp);
       $urlSection=$tmp[0]."/".$tmp[1]."/"; 
       $urlDop=$tmp[0]."/";
       if (count($this->params)==0){
         $return=$infoTopPage;
       }else{
            $id_top=$infoTopPage['id_static_page'];
            for ($i=0; $i<count($this->params); $i++){
                $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1' and id_static_page_top=? and url=?");
                $sth->bindParam(1, $id_top, PDO::PARAM_STR);
                $sth->bindParam(2, $this->params[$i], PDO::PARAM_STR);
                $sth->execute(); 
                $res_temp = $sth->fetch(PDO::FETCH_ASSOC); 
                $id_top=$res_temp['id_static_page'];
                
                if ($i==count($this->params)-1){
                $return = $res_temp;  
                $this->currentPage=$id_top;
                }
            }
       }     
     return $return;
  } 
  
   public function getUrlPage($id, $start){
       $id=intval($id);
       if ($start) $this->urlPage="";
             
       $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1'  and id_static_page=?");
       $sth->bindParam(1, $id, PDO::PARAM_STR);
       $sth->execute(); 
       $res_temp = $sth->fetch(PDO::FETCH_ASSOC); 
       $this->urlPage.=$res_temp['url']."*";
       $id_top=$res_temp['id_static_page_top'];
       
       if ($id_top!=0) {
           $this->getUrlPage($id_top, false, $top);   
       }else{
           $this->urlPage=trim($this->urlPage, "*");
           $arrUrl=explode("*", $this->urlPage);
           array_pop($arrUrl);
           $this->urlPage="/";
           for ($i=count($arrUrl)-1; $i>=0;$i--){
               $dop= ($i==0) ? "" : "/";
               $this->urlPage.=$arrUrl[$i].$dop;
           }
       }
        return $this->urlPage;
   } 
   
   public function parserPage($text){
       $text=stripcslashes($text);
       $text=$this->parserUrlPage($text);
       return $text;
   }
   
   public function parserUrlPage($text){
       $tmp=  explode("/", $this->request);
       $dopSection=$tmp[0]."/".$tmp[1]."/";
       $i=0; 
        while (strpos($text,"[[~",$i)!=false){
           $pos=strpos($text,"[[~",$i);
           $pos2=strpos($text,"~]]",$pos+2);
           if ($pos2){
                $id=substr($text,$pos+3,$pos2-$pos-3);
                $replaceTarget="[[~".$id."~]]";
                $replace = "/".trim($dopSection, "/").$this->getUrlPage($id, true);
                $text = str_replace($replaceTarget, $replace, $text);
                $i=$pos2;
           }
       }
      
     return $text;  
   }
  
   #----------------------------------------------------------------------------
    private function getInfoTopPage($tmp){
       $urlSection=$tmp[0]."/".$tmp[1]."/"; 
       $urlDop=$tmp[0]."/";
       $sth = $this->db->prepare('SELECT s.* FROM '.PREFIX.'_menu m 
                                             LEFT JOIN '.PREFIX.'_static_page s on s.`id_menu` = m.`id_menu`
                                             WHERE 
                                                       (m.`url` = ? or m.`url` = ? or m.`url` = ? )and 
                                                        m.`is_visible` = 1 and 
                                                        m.`is_rubricator` = 0 and
                                                        s.`is_visible` = 1 and 
                                                        s.`is_menu` = 1');
       $sth->bindParam(1, $tmp[0], PDO::PARAM_STR);
       $sth->bindParam(2, $urlSection, PDO::PARAM_STR);
       $sth->bindParam(3, $urlDop, PDO::PARAM_STR);
       $sth->execute();
       $result=$sth->fetch(PDO::FETCH_ASSOC);
       
       return $result;
     }  
   
   
}

?>
