<?php
class StaticPage {
    
    private $db;
    private $params;
    private $dopParams;
    private $request;
    private $currentPage;
    private $urlPage;
   
    
    public function __construct($params, $dopParams) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->params = $params;
        $this->dopParams = $dopParams;  
    }
    
    function getListMainPageRazdel($id){
      $arr = explode(",", $id);  
      $return=array();  
        for ($i=0; $i<count($arr); $i++){
            $sth = $this->db->prepare("SELECT name, url, is_folders FROM ".PREFIX."_static_page WHERE id_static_page=? and is_visible='1' and is_menu='1'"); 
            $sth->bindParam(1, $arr[$i], PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
            if ($sth->rowCount()>0){
                $sth1 = $this->db->prepare("SELECT name, url, is_folders FROM ".PREFIX."_static_page WHERE id_static_page_top=? and is_visible='1' and is_menu='1'"); 
                $sth1->bindParam(1, $arr[$i], PDO::PARAM_INT);
                $sth1->execute();
           
                $resultChild = $sth1->fetchAll(PDO::FETCH_ASSOC);
                    $child=array();
                    for($j=0; $j<count($resultChild); $j++){
                         $dp= ($resultChild[$j]['is_folders']=="1") ? "/" : "" ;
                        array_push($child, array("name" => $resultChild[$j]['name'], "url" => $resultChild[$j]['url'].$dp ));
                    }
           
            
            $dp= ($result['is_folders']=="1") ? "/" : "" ;
            array_push($return, array("name" => $result['name'], "url" =>URL_STATIC.$result['url'].$dp, "child" =>$child));
            } 
        
        }
        return $return;
        
    }
    
    public function setRequest($request){
        $tmp = explode("/", $request);   
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            array_pop($tmp);
        }        
        $this->request = implode("/", $tmp);        
    }
    
    public function getPage(){
        $id_top=0;
        for ($i=0; $i<count($this->params); $i++){
                $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1' and id_static_page_top=? and url=?");
                $sth->bindParam(1, $id_top, PDO::PARAM_STR);
                $sth->bindParam(2, $this->params[$i], PDO::PARAM_STR);
                $sth->execute(); 
                $res_temp = $sth->fetch(PDO::FETCH_ASSOC); 
                $id_top=$res_temp['id_static_page'];
         if ($i==count($this->params)-1){
            $return = $res_temp;  
            $this->currentPage=$id_top;
        }
    }
     return $return;
  } 
   
   public function getUrlPage($id, $start){
       $id=intval($id);
       if ($start) $this->urlPage="";
       
       $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1'  and id_static_page=?");
       $sth->bindParam(1, $id, PDO::PARAM_STR);
       $sth->execute(); 
       $res_temp = $sth->fetch(PDO::FETCH_ASSOC); 
       $this->urlPage.=$res_temp['url']."*";
       $id_top=$res_temp['id_static_page_top'];
       
       if ($id_top!="0") {
           $this->getUrlPage($id_top, false);   
       }else{
           $this->urlPage=trim($this->urlPage, "*");
           $arrUrl=  explode("*", $this->urlPage);
           $this->urlPage="/";
           for ($i=count($arrUrl)-1; $i>=0;$i--){
               $dop= ($i==0) ? "" : "/";
               $this->urlPage.=$arrUrl[$i].$dop;
           }
       }
        return $this->urlPage;
   } 
   
   public function parserPage($text){
       $text=stripcslashes($text);
       $text=$this->parserUrlPage($text);
       return $text;
   }
   
   public function parserUrlPage($text){
       $i=0; 
        while (strpos($text,"[[~",$i)!=false){
           $pos=strpos($text,"[[~",$i);
           $pos2=strpos($text,"~]]",$pos+2);
           if ($pos2){
                $id=substr($text,$pos+3,$pos2-$pos-3);
                $replaceTarget="[[~".$id."~]]";
                $replace = "/".trim(URL_STATIC, "/").$this->getUrlPage($id, true);
                $text = str_replace($replaceTarget, $replace, $text);
                $i=$pos2;
           }
       }
      
     return $text;  
   }
    
}

?>
