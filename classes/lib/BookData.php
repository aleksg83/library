<?php

class BookData extends Data {
    
    const listData = 'listData';
    const typeListData = 'listDataMain';
    const singleData = 'singleData';
    const mainData = 'index';
    const type = 1;
    
    public function __construct($params) {
        parent::__construct($params, self::type);
    }
    
    public function getCountData(){
        return parent::getCountData();
    }    

    public function getSetting(){
        return parent::getSetting();
    }
    
    public function getRubrName(){
        return parent::getRubrName();
    }
    
    public function getListData(){
        $books = parent::getListData();
        return $books;
    }
    
    public function getSingleData(){
        $book = parent::getSingleData();        
        #$relBook = $this->getRelatedData($book['id_data']);
        $book['mapping'] = $this->getShortContent($book['id_data']);
        $book['related'] = ($relBook)?$relBook:array();
        $book['fileInfo'] = $this->getFileInfo($book['file_name'], $book['folder_name'],  'BOOK');
        $book['mode'] = 'BOOK';
        
        return $book;
    }
    
    public function getListReaderData(){
        $setting=$this->getSetting();
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_data
                                        WHERE 
                                                is_recomended=1 and
                                                is_view_in_rubrik=1 and 
                                                id_sp_type_data = ? and
                                                id_data NOT IN ( SELECT id_data FROM ".PREFIX."_rel_users_pred_print WHERE is_done='0' )
                                                ORDER by  date_add desc
                                                LIMIT ".$setting['CountDataBookReader']);
        $sth->bindParam(1, $this->type, PDO::PARAM_INT);
        $sth->execute();
        $listData = $sth->fetchAll(PDO::FETCH_ASSOC);
        $return=array();
        for ($i=0; $i<count($listData); $i++){
             
            if (file_exists(BOOK.$listData[$i]['folder_name']."/preview_0001.jpg")){ 
                  $images=BOOK_BASE.$listData[$i]['folder_name']."/preview_0001.jpg"; 
            }elseif (file_exists(BOOK.$listData[$i]['folder_name']."/jpg/0001.jpg")){        
                  $images=BOOK_BASE.$listData[$i]['folder_name']."/jpg/0001.jpg"; 
            }else{
                  $images=MAIN_TEMPLATE."/images/book/default.jpg"; 
            }          
            $tochka=($listData[$i]['is_dot']=="1")? "." : "" ;
            $name=$listData[$i]['short_author'].' '.$listData[$i]['name'].$tochka.' &mdash; '.$listData[$i]['year'];
            $annotaciya= stripcslashes($listData[$i]['annotaciya']);
            $url="/lib/book/".$listData[$i]['url'];
            $class= ($listData[$i]['id_sp_polnota']=="1") ? "" : ' class="color2"';
            $returnTemp=array("images"=>$images, "name"=>$name, "annotaciya"=>$annotaciya, "url"=>$url, "class"=>$class);
            array_push($return, $returnTemp);
        }
       return $return;
   }
    
#----------------------------------------------------------------------------------------------------------------------------------------------------------    
    private function getRelatedData($idBook){
        $sth = $this->db->prepare("SELECT d.* FROM ".PREFIX."_data d 
                                        left join ".PREFIX."_rel_data_data rd on d.id_data = rd.id_data_used
                                   where rd.id_data = ?");
        $sth->bindParam(1, $idBook, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>