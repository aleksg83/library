<?php
class Data {
    protected $db; 
    protected $type;
    protected $rubr;
    protected $params;
    protected $url;
    protected $person;
    protected $lim;
    protected $filterString = '';
    protected $order;
    private $settings = array();
    private $countData;
    private $rubrName;        
    private $is_in_stock = false;
    private $relRubs = false;
    private $preprintSQL;

    protected function __construct($params, $type) {
        $this->type = $type;
        $this->params = $params;
        if($this->params['page'] == 'false') $this->params['page'] = 1;
        $this->person = new Person();
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->auth = auth::instance();
        $this->preparePreprintSQL();
        $this->selectSettings();        
    }
 #----------------------------------------------------------------------------------------------------------------------------------------------------------   
    public function collectSetting(){
        $this->selectSettings();
        $this->selectRubrName();
        $this->selectCountData();
        $this->filterString = '';
    }
    
    public function setRelRubs($rubs){
        $this->relRubs = $rubs;
    }
    
    public function setRubr($rubr){
        $this->rubr = $rubr;
    }
    
    public function setUrl($url){
        $this->url = $url;
    }
    
    public function setStock($stok){
        $this->is_in_stock = ($stok)?true:false;
    }
        
#----------------------------------------------------------------------------------------------------------------------------------------------------------            
    protected function getCountData(){
        return $this->countData;
    }
    
    protected function getSetting(){
        return $this->settings;
    }
    
    protected function getRubrName(){
        return $this->rubrName;
    }       
    
    protected function getAllContent($idData){
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_mapping where id_data = ?");
        $sth->bindParam(1, $idData, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    protected function getShortContent($idData){
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_mapping where id_data = ? and name is not null");
        $sth->bindParam(1, $idData, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    protected function getFileInfo($file_name, $folder_name, $type){
        $tmp = explode(".", $file_name);
        $fileInfo['size'] = $this->filesize(constant($type).$folder_name.'/'.$file_name); 
        $fileInfo['ext'] = $tmp[1];
        return $fileInfo;
    }
    
    protected function getListData(){ 
        $rubr = ($this->rubr)?" and m.url_name = '{$this->rubr}' ":'';
        $relRub = ($this->relRubs)?" and m.url_name in ({$this->relRubs}) ":"";
        $artSQL= ($this->type=="2")?  "and NOT EXISTS ( SELECT  id_data FROM ".PREFIX."_data d2 WHERE  d2.id_data=d.id_data and d.id_sp_type_data=2 and d.id_prev!=0)" : "";
        $disSQL= ($this->type=="3")?  "and d.vid_dis!=3" : "" ;
        
        $this->applyFilter(); 
        $sql = "SELECT distinct d.* FROM ".PREFIX."_data d
                                    left join ".PREFIX."_rel_data_menu rm using(id_data)
                                    left join ".PREFIX."_menu m using (id_menu)
                                    left join ".PREFIX."_rel_users_pred_print pp using (id_data)
                                   where d.id_sp_type_data = {$this->type} and
                                         d.is_view_in_rubrik = 1 
                                    {$disSQL}     
                                    {$this->preprintSQL}
                                    {$rubr} {$relRub} {$artSQL}
                                    {$this->filterString} {$this->order} {$this->lim}";
        #echo $sql;
        $sth = $this->db->prepare($sql);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
  
    }
    
    protected function getSingleData(){
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_data where url = ?");
        $sth->bindParam(1, $this->url, PDO::PARAM_STR);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
    
    protected function applyFilter(){      
        if(!$this->params['limit']) $this->params['limit'] = 15;            
        if(!$this->params['page']) $this->params['page'] = 1; 
        $limit = $this->params['limit'];
        $start = $limit*($this->params['page']-1);
        $this->lim = ' LIMIT '.($start).', '.($start+$limit);
        if($this->is_in_stock) $this->filterString .= " and d.is_in_stock = 1 ";
        if($_REQUEST){
            foreach($_REQUEST as $key=>$val){            
                switch($key){
                    case 'dateStart': if($val && $val != 'false') { $this->filterString .= " and d.date_add >= '".implode("-", array_reverse(explode(".", $val)))."'"; } break;
                    case 'dateEnd': if($val && $val != 'false') { $this->filterString .= " and d.date_add <= '".implode("-", array_reverse(explode(".", $val)))."'"; } break;
                }
            }
        }      
        if($this->params['sortType']){
            $sortField = ($this->params['sortType'] == 'A')?'d.`name`':'d.`year`';
            if($this->params['sort']){
                $this->order = "order by {$sortField} {$this->params['sort']}";
            }                    
        }
    }
    
    protected function selectCountData(){  
        $rubr = ($this->rubr)?" and m.url_name = '{$this->rubr}' ":'';
        $artSQL= ($this->type=="2")?  "and NOT EXISTS ( SELECT  id_data FROM ".PREFIX."_data d2 WHERE  d2.id_data=d.id_data and d.id_sp_type_data=2 and d.id_prev!=0)" : "";
        $disSQL= ($this->type=="3")?  "and d.vid_dis!=3" : "" ;
        $this->applyFilter();
        $sql = "SELECT count(distinct d.`id_data`) FROM ".PREFIX."_data d
                                    left join ".PREFIX."_rel_data_menu rm using(id_data)
                                    left join ".PREFIX."_menu m using (id_menu)
                                    left join ".PREFIX."_rel_users_pred_print pp using (id_data)
                                where d.id_sp_type_data = {$this->type} and
                                      d.is_view_in_rubrik = 1
                                    {$disSQL} 
                                    {$this->preprintSQL}
                                    {$rubr} 
                                    {$artSQL}    
                                    {$this->filterString}";
        #echo $sql;
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $this->countData = array_shift($sth->fetch(PDO::FETCH_ASSOC)); 
    }

#----------------------------------------------------------------------------------------------------------------------------------------------------------    
    private function selectSettings(){
        $sth=$this->db->prepare("SELECT `name`, `value` FROM ".PREFIX."_main_setting WHERE id_sp_type_data = ?");
        $sth->bindParam(1, $this->type, PDO::PARAM_INT);
        $sth->execute();
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->settings[$row['name']] = $row['value'];
        }
    }
     
    private function selectRubrName(){
        #echo "<pre>"; print_r($this); echo "</pre>";
        if($this->rubr){
            $sth=$this->db->prepare("SELECT `name` FROM ".PREFIX."_menu WHERE url_name = ?");            
            $sth->bindParam(1, $this->rubr, PDO::PARAM_STR);
            $sth->execute();
            $this->rubrName = @array_shift($sth->fetch(PDO::FETCH_ASSOC));
        }
    }
    
    private function filesize($file){
     if (file_exists($file)){
        $filesize = filesize($file);   
         if($filesize > 1024){
             $filesize = ($filesize/1024);
             if($filesize > 1024){
                 $filesize = ($filesize/1024);
                 if($filesize > 1024){
                     $filesize = ($filesize/1024);
                     $filesize = round($filesize, 2);
                     return $filesize." б";   
                 }else{
                     $filesize = round($filesize, 2);
                     return $filesize." Кб";   
                 }  
             }else{
                 $filesize = round($filesize, 2);
                 return $filesize." Мб";   
             }  
         }else{
             $filesize = round($filesize, 2);
             return $filesize." Гб";   
         }  
     }    
   }  
   #----------------------------------------------------------------------------
   public function preparePreprintSQL(){
          if (!$this->auth->isLogged) {
              
              $this->preprintSQL=" and (NOT EXISTS 
                                        (select 1 from ".PREFIX."_rel_users_pred_print pp 
                                            where pp.id_data = d.id_data and 
                                            pp.is_done = 0) or 
                                            pp.id_rel_users_pred_print is null)";
    
          } 
          
     }
     
   
   
}
?>