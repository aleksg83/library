<?php
class FilmstripData extends Data {
    
    const listData = 'listData';
    const typeListData = 'listDataMain';
    const singleData = 'singleData';
    const mainData = 'index';
    
    public function __construct($params) {
        parent::__construct($params, 4);
    }
    
    public function getCountData(){
        return parent::getCountData();
    }    

    public function getSetting(){
        return parent::getSetting();
    }
    
    public function getRubrName(){
        return parent::getRubrName();
    }
    
    public function getListData(){
        $filmstrips = parent::getListData();
        return $filmstrips;
    }
    
    public function getSingleData(){
        $filmstrip = parent::getSingleData();   
        $filmstrip['mapping'] = $this->getShortContent($filmstrip['id_data']);
        $filmstrip['fileInfo'] = $this->getFileInfo($filmstrip['file_name'],  $filmstrip['folder_name'], 'FILMSTRIP');
        $filmstrip['mode'] = 'FILMSTRIP';
        
        return $filmstrip;
    }
}

?>
