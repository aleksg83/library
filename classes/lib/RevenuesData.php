<?php

class RevenuesData extends Data { 
    
    const listData = 'listData';
    const typeListData = 'listDataRevenues';
    const singleData = 'singleData';
    const mainData = 'revenues';
    
    const periodicalId = '126';
    
    public function __construct($params) {
        parent::__construct($params, 1);
    }
    
    public function getCountData(){
        return $this->selectCountData();
    }    

    public function getSetting(){
        return parent::getSetting();
    }
    
    public function getRubrName(){
        return parent::getRubrName();
    }
    public function getListData() {    
        $type = ($this->params['type'] == 'false')?" and st.code = 'book' ":" and st.code = '{$this->params['type']}' ";
        $mode = ($this->params['mode'] == 'false' || $this->params['mode'] == 0)?"":" and d.id_sp_polnota = {$this->params['mode']} ";
        if($this->params['type'] == 'periodic'){ 
            $type = " and d.is_periodical = 1";
        }
        
        $this->applyFilter(); 
        $sth = $this->db->prepare("SELECT distinct d.*, st.code FROM ".PREFIX."_data d
                                    left join ".PREFIX."_rel_data_menu rm using(id_data)
                                    left join ".PREFIX."_menu m using (id_menu)
                                    left join ".PREFIX."_rel_users_pred_print pp using (id_data)
                                    left join ".PREFIX."_sp_type_data st using (id_sp_type_data)
                                   where (NOT EXISTS (select 1 from ".PREFIX."_rel_users_pred_print pp where pp.id_data = d.id_data and pp.is_done = 0) or pp.id_rel_users_pred_print is null)
                                    and d.`is_view_in_rubrik`=1
                                    {$type} {$mode} {$this->filterString} {$this->order} {$this->lim}");
        #echo "SELECT distinct d.*, st.code FROM ".PREFIX."_data d left join ".PREFIX."_rel_data_menu rm using(id_data) left join ".PREFIX."_menu m using (id_menu) left join ".PREFIX."_rel_users_pred_print pp using (id_data) left join ".PREFIX."_sp_type_data st using (id_sp_type_data) where (NOT EXISTS (select 1 from ".PREFIX."_rel_users_pred_print pp where pp.id_data = d.id_data and pp.is_done = 0) or pp.id_rel_users_pred_print is null) {$type} {$mode} {$this->filterString} {$this->order} {$this->lim}";
        #exit;
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function selectCountData(){    
        $type = ($this->params['type'] == 'false' || !$this->params['type'])?" and st.code='book' ":" and st.code = '{$this->params['type']}' ";
        $mode = ($this->params['mode'] == 'false' || !$this->params['mode'])?"":" and d.id_sp_polnota = {$this->params['mode']} ";
        
        if($this->params['type'] == 'periodic'){ 
            $type = " and d.is_periodical = 1";
        }
        
        $this->applyFilter();       
        $sth = $this->db->prepare("SELECT count(distinct d.`id_data`) FROM ".PREFIX."_data d
                                    left join ".PREFIX."_rel_data_menu rm using(id_data)
                                    left join ".PREFIX."_menu m using (id_menu)
                                    left join ".PREFIX."_rel_users_pred_print pp using (id_data)
                                    left join ".PREFIX."_sp_type_data st using (id_sp_type_data)
                                   where (NOT EXISTS (select 1 from ".PREFIX."_rel_users_pred_print pp where pp.id_data = d.id_data and pp.is_done = 0) or pp.id_rel_users_pred_print is null)
                                    and d.is_view_in_rubrik=1
                                    {$type} {$mode} {$this->filterString} {$this->order}");
        $sth->execute();
        return array_shift($sth->fetch(PDO::FETCH_ASSOC)); 
    }
    
}

?>