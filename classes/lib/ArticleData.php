<?php

class ArticleData extends Data {
    
    const listData = 'listData';
    const typeListData = 'listDataArt';
    const singleData = 'singleData';
    const mainData = 'index';
    
    public function __construct($params) {
        parent::__construct($params, 2);
    }
    
    public function getCountData(){
        return parent::getCountData();
    }    

    public function getSetting(){
        return parent::getSetting();
    }
    
    public function getRubrName(){
        return parent::getRubrName();
    }
    
    public function getListData(){
        $articles = parent::getListData();
        foreach($articles as $key=>$article){
            if ($article['id_next']!=0){
                $articles[$key]['nodes']="1";
                $articles[$key]['short_name'].=". Начало";
                $articles[$key]['bo']=$articles[$key]['article_group_bo'];
                $this->getNodeArticle($article['id_next'], "", 1, false);
                $articles[$key]['node_art']=$this->node;
                
            }
        }
        return $articles;
    }
    
    public function getSingleData(){
        $article = parent::getSingleData();   
        $article['mapping'] = $this->getShortContent($article['id_data']);
        $article['fileInfo'] = $this->getFileInfo($article['file_name'], $article['folder_name'], 'ARTICLE');
        $article['mode'] = 'ARTICLE';
        
        return $article;
    }
    
    #---------------------------------------------------------------------------
    private function getNodeArticle($id, $node, $numb, $npodolg){
        
        if ($id!=0 && $id!=""){
                $sth = $this->db->prepare('SELECT d.* FROM '.PREFIX.'_data d 
                                                      WHERE 
                                                     d.`id_data` = ? 
                                            ');
        
                $sth->bindParam(1, $id, PDO::PARAM_INT);
                $sth->execute();  
                $temp=$sth->fetch(PDO::FETCH_ASSOC);
       
                if (($numb==1) && ($temp['id_next']!=0) ) {//на первом шаге проверим сколько продолжений (1 или больше)
                     $sth1 = $this->db->prepare('SELECT d.* FROM '.PREFIX.'_data d 
                                                            WHERE 
                                                            d.`id_data` = ? 
                                                ');
        
                    $sth1->bindParam(1, $temp['id_next'], PDO::PARAM_INT);
                    $sth1->execute();      
                    $temp2=$sth->fetch(PDO::FETCH_ASSOC);
                    if ($temp2['id_next']!=0) $npodolg=true; //к продолжениям надо добавлять нумерацию
                }
       
              if ($temp['id_next']!=0) {
       
                    $numbProdolgenie=($npodolg)? ($numb+1) : "";
                    if ($temp['is_view_in_rubrik']=="1"){
                        $class=($temp['id_sp_polnota']=="1")?"color1":"color2";
                        $node.='<a href="'.SITE_URL.VIEWER.'article/'.$temp['url'].'#'.$temp['start_page'].'" class="'.$class.'" target="_blank">Продолжение'.$numbProdolgenie.'</a>&nbsp;('.$temp['year'].').&nbsp;';
                    }else{
                        $node.='Продолжение'.$numbProdolgenie.'&nbsp;('.$temp['year'].').&nbsp;'; 
                    }
                    $numb++;
                    $this->getNodeArticle($temp['id_next'], $node, $numb, $npodolg);
               
                    
               }else{
                       if ($temp['is_view_in_rubrik']=="1"){
                        $class=($temp['id_sp_polnota']=="1")?"color1":"color2";
                         $node.='<a href="'.SITE_URL.VIEWER.'article/'.$temp['url'].'#'.$temp['start_page'].'" class="'.$class.'" target="_blank">Окончание'.$numbProdolgenie.'</a>&nbsp;('.$temp['year'].').&nbsp;';
                       }else{
                         $node.=' Окончание'.'&nbsp;'.$temp['year']; 
                        }
                       $this->node=$node;
               }
       }         
        
    }
    
}

?>