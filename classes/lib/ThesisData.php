<?php

class ThesisData extends Data {
    
    const listData = 'listData';
    const typeListData = 'listDataDis';
    const singleData = 'singleData';
    const mainData = 'index';
    
    public function __construct($params) {
        parent::__construct($params, 3);
    }
    
    public function getCountData(){
        return parent::getCountData();
    }    

    public function getSetting(){
        return parent::getSetting();
    }
    
    public function getRubrName(){
        return parent::getRubrName();
    }
    
    public function getListData(){
        $Tmaterials = parent::getListData();
        foreach($Tmaterials as $key=>$Tmaterial){
            
          $Tmaterials[$key]['synopsis'] = $this->getSynopsis($Tmaterial['id_data']);
        }
        return $Tmaterials;
    }
    
    public function getSingleData(){
        $Tmaterial = parent::getSingleData();   
        $Tmaterial['mapping'] = $this->getShortContent($Tmaterial['id_data']);
        $Tmaterial['synopsis'] = ($Tmaterial['vid_dis'] == 1 || $Tmaterial['vid_dis'] == 2)?$this->getSynopsis($Tmaterial['id_data']):array();
        $Tmaterial['fileInfo'] = $this->getFileInfo($Tmaterial['file_name'],  $Tmaterial['folder_name'], 'THESIS_MATERIAL');
        $Tmaterial['mode'] = 'THESIS_MATERIAL';
        
        return $Tmaterial;
    }
    
#---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function getSynopsis($idDis){
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_data where article_source = ? and vid_dis=3");
        $sth->bindParam(1, $idDis, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
}

?>