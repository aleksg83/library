<?php

class Person {
    
    private $db;
    private $letter;
    private $menuRequest;
    private $personPage;
    private $params;
    private $dopparams;
    private $settings = array();
    
    public function __construct($menuRequest=null, $params=null, $dopparams=null) {
        $sql = new Sql();
        $this->db = $sql->connect();        
        $this->menuRequest = $menuRequest;
        $this->params=$params;
        $this->dopparams=$dopparams;
        $this->selectSettings();
    }
    
    public function getHTMLListPerson(){
        $result = $this->getListPerson();
        $cnt = ceil(count($result)/4);
        $countColumn = $this->settings['countOnPageAuthor'];
        if($cnt < $countColumn) $cnt = $countColumn;     
        $res = array_chunk($result, $cnt);
        $out = '';
        foreach($res as $div){
            $out .= '<ul class="authors authorsfour">';
            foreach($div as $person){
                if ($person['id_person_forwars']!=0){
                    $otsul=$this->getPersonid($person['id_person_forwars']);
                    $out .= '<li>'.$person['short_name'].' см &mdash; <a href="/lib/authors/'.$otsul['url'].'">'.$otsul['short_name'].'</a></li>';
                }else{
                    $out .= '<li><a href="/lib/authors/'.$person['url'].'">'.$person['short_name'].'</a></li>';
                }    
            }
            $out .= '</ul>';
        }
        return $out;
    }
   #----------------------------------------------------------------------------
   public function getSinglePerson(){        
        $person = $this->getSimplePersonUrl();
        $person['material'] = array();
        $person['material'] = $this->getRelatedMaterial($person['id_person']);
        return $person; 
    }
     public function getPersonalInfoPerson(){  
        return  $this->getSimplePersonUrl(); 
     }

    public function createLetters(){
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я');
        $eng = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P' ,'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $modeRus= (in_array($this->letter, $rus)|| ($this->letter=='all')) ? "view" : "none"; 
        $modeEng= (in_array($this->letter, $eng)) ? "view" : "none"; 
        $result = '<div class="abs" mode="'.$modeRus.'" rel="rus">';
        foreach ($rus as $value) {
            $class = ($this->letter == $value)?'class="act"':'';
            $result .= '<a href="/'.$this->menuRequest.'/letter/'.$value.'" '.$class.'>'.$value.'</a>';
        }
        $result .= '<a href="javascript:void(0);" class="alph" rel="eng">A-Z</a></div>';
        $result .= '<div class="abs" mode="'.$modeEng.'" rel="eng">';
        foreach ($eng as $value) {
            $class = ($this->letter == $value)?'class="act"':'';
            $result .= '<a href="/'.$this->menuRequest.'/letter/'.$value.'" '.$class.'>'.$value.'</a>';
        }
        $result .= '<a href="javascript:void(0);" class="alph" rel="rus">А-Я</a></div>';
        return $result;
    }
    
    public function setRequest($request){
        $this->personPage = array_pop(explode("/",$request));
    }
    
    public function setLetter($letter){
        $this->letter = $letter;
    }
#----------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function getListPerson(){
       $page=$this->getValueKeyDopparams('page');
       if($page){
            $start = 4*$this->settings['countOnPageAuthor']*($page-1);
            $lim = ' LIMIT '.($start).', '.($start+(4*$this->settings['countOnPageAuthor']));
       }elseif($this->settings['countOnPageAuthor']){
            $lim = ' LIMIT 0, '.(4*$this->settings['countOnPageAuthor']); 
       } 
       if ($this->letter=="all"){
            $sth = $this->db->query("SELECT p.*, p.`fam_dp` as 'short_name' FROM ".PREFIX."_person p
                                                                            WHERE EXISTS (
                                                                                           SELECT rdp.`id_person` FROM ".PREFIX."_rel_data_person rdp 
                                                                                           WHERE    
                                                                                                p.`id_person`=rdp.`id_person` and
                                                                                                (rdp.`id_sp_type_person`= 1 or 
                                                                                                 rdp.`id_sp_type_person`= 2 or 
                                                                                                 rdp.`id_sp_type_person`= 4 or 
                                                                                                 rdp.`id_sp_type_person`= 5)

                                                                                          )  
                                                                                
                                                                            order by p.fio {$lim}");   
            
            
        }else{
            $sth = $this->db->prepare("SELECT p.*, p.`fam_dp` as 'short_name' FROM ".PREFIX."_person p 
                                                                           WHERE 
                                                                            SUBSTRING(p.`fio`, 1, 1) = ?  and
                                                                            EXISTS (
                                                                                           SELECT rdp.`id_person` FROM ".PREFIX."_rel_data_person rdp 
                                                                                           WHERE    
                                                                                                p.`id_person`=rdp.`id_person` and
                                                                                                (rdp.`id_sp_type_person`= 1 or 
                                                                                                 rdp.`id_sp_type_person`= 2 or 
                                                                                                 rdp.`id_sp_type_person`= 4 or 
                                                                                                 rdp.`id_sp_type_person`= 5)

                                                                                          )  
                                                                            
                                                                            order by p.fio {$lim}");        
            $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
            $sth->execute();
        }    
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;         
    }
    #---------------------------------------------------------------------------
    private function getSimplePersonUrl(){
        $sth=$this->db->prepare("SELECT p.*, CONCAT(`fam_dp`) as 'short_name' FROM ".PREFIX."_person p WHERE p.url = ?"); 
        $sth->bindParam(1, $this->personPage, PDO::PARAM_STR);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
     #---------------------------------------------------------------------------
     private function getPersonid($id){
        $sth=$this->db->prepare("SELECT p.*, CONCAT(`fam_dp`) as 'short_name' FROM ".PREFIX."_person p WHERE p.id_person = ?"); 
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
    #---------------------------------------------------------------------------
    public function getDopUrlPage(){
       $ArrPerson =  explode(",",$this->settings['EncyclopediaMenuPerson']);
       $punctPerson=$this->getInfoIdMenu($ArrPerson[0]); //Персоны
       return "/".URL_ENCYCLOPEDIA.$punctPerson['url_name']."/";
    }
    #--------------------------------------------------------------------------
     private function getInfoIdMenu($id){
       $sth = $this->db->prepare('SELECT m.* FROM '.PREFIX.'_menu m 
                                    WHERE 
                                        m.`id_menu` = ? and
                                        m.`is_visible` = 1  and 
                                        m.`is_rubricator` = 0 
                                        ');
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->execute();  
        return $sth->fetch(PDO::FETCH_ASSOC);
     }
    #---------------------------------------------------------------------------
    private function getRelatedMaterial($id_person){
        $sth=$this->db->prepare("SELECT 
                                    d.*, 
                                    dp.id_sp_type_person,
                                    tp.name as 'material_name',
                                    tp.url as 'dop_url'
                                FROM  ".PREFIX."_rel_data_person  dp  
                                    LEFT JOIN ".PREFIX."_data d using(id_data)
                                    LEFT JOIN ".PREFIX."_sp_type_data tp using(id_sp_type_data)
                                WHERE dp.id_person = ? and
                                      d.is_view_author = 1 and
                                      NOT EXISTS (
                                                        SELECT 
                                                            id_data 
                                                        FROM ".PREFIX."_data d2
                                                        WHERE 
                                                           d2.id_data=d.id_data and d.id_sp_type_data=2 and d.id_prev!=0
                                                     ) and
                                     NOT EXISTS (select 1 from ".PREFIX."_rel_users_pred_print pp where pp.id_data = d.id_data and pp.is_done = 0)                 
                                 GROUP BY d.id_data
                                ");
        $sth->bindParam(1, $id_person, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        foreach($result as $key=>$row){ 
           #Common::pre($row);
            #link in viewer
            if ( ($row['id_sp_type_data']==2) || ( ($row['id_sp_type_data']==3) && ($row['vid_dis']==3)) ) {
                $type= ($row['id_sp_type_data']==2) ? "article" : "thesis";
                $row['dop_url']=VIEWER.$type;
                $row['viewer_url']="#".$row['start_page'];
                $row['target']=' target="_blank"';
            }
            
            $out[$row['material_name']][$key] = $row;
            $out[$row['material_name']][$key]['class'] = ($row['id_sp_polnota']=="1")?"color1":"color2";
            
            #get node article
            if ( ($row['id_sp_type_data']==2) && $row['id_next']!=0){
                #поиск всех продолжений
                $this->getNodeArticle($row['id_next'], $row['dop_url'], "", 1, false);
                $out[$row['material_name']][$key]['node_art']=$this->node;
            }
            
        }
        return $out;
    }
    #---------------------------------------------------------------------------
    private function getNodeArticle($id, $dop_url, $node, $numb, $npodolg){
        
        if ($id!=0 && $id!=""){
                $sth = $this->db->prepare('SELECT d.* FROM '.PREFIX.'_data d 
                                                      WHERE 
                                                     d.`id_data` = ? 
                                            ');
        
                $sth->bindParam(1, $id, PDO::PARAM_INT);
                $sth->execute();  
                $temp=$sth->fetch(PDO::FETCH_ASSOC);
       
                if (($numb==1) && ($temp['id_next']!=0) ) {//на первом шаге проверим сколько продолжений (1 или больше)
                     $sth1 = $this->db->prepare('SELECT d.* FROM '.PREFIX.'_data d 
                                                            WHERE 
                                                            d.`id_data` = ? 
                                                ');
        
                    $sth1->bindParam(1, $temp['id_next'], PDO::PARAM_INT);
                    $sth1->execute();      
                    $temp2=$sth->fetch(PDO::FETCH_ASSOC);
                    if ($temp2['id_next']!=0) $npodolg=true; //к продолжениям надо добавлять нумерацию
                }
       
              if ($temp['id_next']!=0) {
       
                    $numbProdolgenie=($npodolg)? ($numb+1) : "";
                    if ($temp['is_view_in_rubrik']=="1"){
                        $class=($temp['id_sp_polnota']=="1")?"color1":"color2";
                        $node.='<a href="/'.$dop_url.'/'.$temp['url'].'" class="'.$class.'">Продолжение'.$numbProdolgenie.'</a>&nbsp;('.$temp['year'].'). ';
                    }else{
                        $node.='Продолжение'.$numbProdolgenie.'&nbsp;('.$temp['year'].'). '; 
                    }
                    $numb++;
                    $this->getNodeArticle($temp['id_next'], $dop_url, $node, $numb, $npodolg);
               
                    
               }else{
                       if ($temp['is_view_in_rubrik']=="1"){
                        $class=($temp['id_sp_polnota']=="1")?"color1":"color2";
                         $node.='<a href="/'.$dop_url.'/'.$temp['url'].'" class="'.$class.'">Окончание'.$numbProdolgenie.'</a>&nbsp;('.$temp['year'].'). ';
                       }else{
                         $node.='Окончание'.'&nbsp;'.$temp['year']; 
                        }
                       $this->node=$node;
                       //return $node; 
               }
       }         
        
    }
    #---------------------------------------------------------------------------
     private function getValueKeyDopparams($k){
       $val="";
       if ($this->dopparams){
            foreach ($this->dopparams as $key => $value) {
                if ($key==$k)  $val=$value;
            }
       }
       return $val;
     }
     #--------------------------------------------------------------------------
      private function selectSettings(){
        $sth=$this->db->query("SELECT `name`, `value` FROM ".PREFIX."_main_setting");
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->settings[$row['name']] = $row['value'];
        }
    }
    #---------------------------------------------------------------------------
    public function countLimit(){
        return 4*$this->settings['countOnPageAuthor'];
    }
    #---------------------------------------------------------------------------
    public function getCountPerson(){    
     if ($this->letter=="all"){
              $sth = $this->db->query("SELECT count(*) FROM ".PREFIX."_person p 
                                                                    WHERE EXISTS (
                                                                                   SELECT rdp.`id_person` FROM ".PREFIX."_rel_data_person rdp 
                                                                                   WHERE    
                                                                                          p.`id_person`=rdp.`id_person` and
                                                                                         (rdp.`id_sp_type_person`= 1 or 
                                                                                          rdp.`id_sp_type_person`= 2 or 
                                                                                          rdp.`id_sp_type_person`= 4 or 
                                                                                          rdp.`id_sp_type_person`= 5)
                                                                                          )  
                                                                                 ");
     }else{
              $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_person p 
                                                                        WHERE 
                                                                        SUBSTRING(p.`fio`, 1, 1) = ? and
                                                                        EXISTS (
                                                                                   SELECT rdp.`id_person` FROM ".PREFIX."_rel_data_person rdp 
                                                                                   WHERE    
                                                                                   p.`id_person`=rdp.`id_person` and
                                                                                  (rdp.`id_sp_type_person`= 1 or 
                                                                                   rdp.`id_sp_type_person`= 2 or 
                                                                                   rdp.`id_sp_type_person`= 4 or 
                                                                                   rdp.`id_sp_type_person`= 5)
                                                                                   )  
                                                                        ");
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->execute();
     }                           
     
     return array_shift($sth->fetch(PDO::FETCH_ASSOC));
    } 
     
}

?>