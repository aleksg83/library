<?
class Sql  {
    private $db;
    #private $sql = array('login'=>'root', 'pass'=>'unix', 'db'=>'library', 'host'=>'localhost');
    private $sql = array('login'=>'librarybase', 'pass'=>'LibTestoSer2R!', 'db'=>'librarybase', 'host'=>'librarybase.db.9102037.hostedresource.com');
    
    public function connect(){                  
        $this->db = new PDO("mysql:host={$this->sql['host']};dbname={$this->sql['db']}", $this->sql['login'], $this->sql['pass']); #, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode = 'TRADITIONAL'"));     
        $this->db->exec("set names utf8");
        return $this->db;
    }
    
    public function getSP(){
        $sth = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = '{$this->sql['db']}' and TABLE_NAME like '".PREFIX."_sp_%' and table_comment != ''");
        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $key=>$result){
            $res = explode("_", $result['TABLE_NAME']);
            array_shift($res); array_shift($res);  
            $results[$key]['TABLE_NAME'] = implode("_",$res);
        }
        return $results;
    }
        
    public function getTableColumns($tableName){
        
        $sth = $this->db->query("SELECT * FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='{$this->sql['db']}' AND `TABLE_NAME`='".PREFIX."_{$tableName}'");
        $columns = $sth->fetchAll(PDO::FETCH_ASSOC);         
        foreach($columns as $column){            
	    $columnDetails = $this->getColumnDetails($column['COLUMN_NAME'], $tableName);
            $result[$column['COLUMN_NAME']] = $columnDetails['name'];
        }
        return $result;
    }    
    #----------------------------------------------------------------------------------------------------------------------------------------    
    private function getColumnDetails($columnName, $tableName){
		$table = " = '{$tableName}' ";
        $sth = $this->db->query("
                SELECT  tab.*
                FROM `INFORMATION_SCHEMA`.`COLUMNS` col
                        LEFT JOIN ".PREFIX."_sp_table_names tab on col.`COLUMN_NAME` = tab.`column`
                WHERE col.TABLE_SCHEMA='{$this->sql['db']}' 
                    AND col.TABLE_NAME='".PREFIX."_{$tableName}'
                    AND (tab.table {$table} or tab.table is null)
                    AND tab.name is not null
                    AND tab.column = '{$columnName}'");
        return $sth->fetch(PDO::FETCH_ASSOC);
    }
    
    
    public function getCommentTable($tableName){
        $sth = $this->db->query("SELECT TABLE_COMMENT FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = '{$this->sql['db']}' and TABLE_NAME='".PREFIX.$tableName."'");
        $results = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }
    
   public function getNameDB(){
     return $this->sql['db'];  
       
   }
}
?>