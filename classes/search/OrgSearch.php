<?php

class OrgSearch extends Search {
    
    private $db;
    
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect(); 
    }
    
    public function search($params){           
        #$this->setLimit($params, $params['limit']);    
        $sth = $this->db->prepare("SELECT 
                                     o.*, CONCAT('organization', '/', o.url) as `url`
                                   FROM ".PREFIX."_sp_organasation o
                                   where 
                                       UPPER(o.name) like '%{$params['e_text']}%' or 
                                       UPPER(o.text) like '%{$params['e_text']}%' or 
                                       UPPER(o.short_text) like '%{$params['e_text']}%'");        
        $sth->bindParam(1, $this->mode, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}

?>
