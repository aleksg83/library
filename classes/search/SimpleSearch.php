<?php

class SimpleSearch {
    private $db;
    #private $mode = array('sputnik'=>40, 'encyclopedia'=>46, 'other'=>0);
    private $searchWord;
    public $cnt = array(1=>0, 2=>0, 3=>0, 4=>0);    
    
    public function __construct($params) {
        $this->searchWord = $params['search']; 
        $sql = new Sql();
        $this->db = $sql->connect(); 
    }
    
    public function search(){
        $sth = $this->db->prepare("SELECT 
                                       d.short_name as short_name, d.short_author as short_author, d.year as `year`, d.url as url, td.url as td_url, td.code as code 
                                   FROM ".PREFIX."_data d
                                       left join ".PREFIX."_sp_type_data td using(id_sp_type_data)
                                   where 
                                       UPPER(d.name) like UPPER('%{$this->searchWord}%')                                           
                                   UNION ALL                                   
                                   SELECT 
                                       n.zagolovok as short_name, '' as short_author, '' as `year`, n.url as url, '' as td_url, 'news' as code
                                   FROM ".PREFIX."_news n
                                   where 
                                       UPPER(n.text) like UPPER('%{$this->searchWord}%')
                                   UNION ALL
                                   SELECT 
                                       sp.name as short_name, '' as short_author, '' as `year`, sp.url as url, '' as td_url, 'sputnik' as code
                                   FROM ".PREFIX."_static_page sp 
                                   where 
                                        sp.id_static_page_top = 40 and UPPER(sp.text) like UPPER('%{$this->searchWord}%')
                                   UNION ALL
                                   SELECT 
                                       sp.name as short_name, '' as short_author, '' as `year`, sp.url as url, '' as td_url, 'encyclopedia' as code
                                   FROM ".PREFIX."_static_page sp 
                                   where 
                                        sp.id_static_page_top = 46 and UPPER(sp.text) like UPPER('%{$this->searchWord}%')");
        $sth->execute();     
        while($row = $sth->fetch(PDO::FETCH_ASSOC)){      
            $result[$row['code']][] = $row;
        }
        
        return $result;
    }
}

?>
