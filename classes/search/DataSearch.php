<?php

class DataSearch extends Search {
    private $db;
    private $mode;
    private $join = '';
    private $where = '';
    private $order = '';    
    
    public function __construct($mode) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->mode = $mode;
    }
    
    public function search($params){               
        $sth = $this->db->query("select `name` from ".PREFIX."_sp_type_data where id_sp_type_data = {$this->mode}");        
        $res = array_shift($sth->fetch(PDO::FETCH_ASSOC));
        SearchLog::addParam("Область поиска:", $res);
        
        $this->createQuery($params);             
        /*echo "SELECT p1.*, d.*, td.url as td_url FROM ".PREFIX."_data d
                                       left join ".PREFIX."_rel_data_person rp1 using(id_data)
                                       left join ".PREFIX."_person p1 using(id_person)
                                       left join ".PREFIX."_sp_type_data td using(id_sp_type_data)
                                       {$this->join}
                                   where d.id_sp_type_data = {$this->mode} {$this->where} {$this->order} {$this->limit}";*/  
        $query = "SELECT p1.*, d.*, td.url as td_url FROM ".PREFIX."_data d left join ".PREFIX."_rel_data_person rp1 using(id_data) left join ".PREFIX."_person p1 using(id_person) left join ".PREFIX."_sp_type_data td using(id_sp_type_data) {$this->join} where d.id_sp_type_data = {$this->mode} {$this->where}";
        $sth = $this->db->prepare("SELECT 
                                     p1.*, d.*, td.url as td_url 
                                   FROM ".PREFIX."_data d
                                       left join ".PREFIX."_rel_data_person rp1 using(id_data)
                                       left join ".PREFIX."_person p1 using(id_person)
                                       left join ".PREFIX."_sp_type_data td using(id_sp_type_data)
                                       {$this->join}
                                   where 
                                       d.id_sp_type_data = ? 
                                       {$this->where} 
                                       {$this->order} 
                                       {$this->limit}");        
        $sth->bindParam(1, $this->mode, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        $this->getCNT($query, $this->db);
        SearchLog::addParam("Результат:", $this->cnt[4]);
        SearchLog::setLog();        
        return $result;
    }
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function createQuery($params){        
        foreach($params as $key=>$value){
            if(strpos($key, "_")!==false) list($dop, $name) = explode("_", $key); else $name = $key;     
            if($value){
                switch ($name) {
                    case 'author'    : $this->where .= " and rp1.id_sp_type_person = 1 and (UPPER(p1.fio) like UPPER('%{$value}%') or UPPER(p1.fam_dp) like UPPER('%{$value}%'))"; SearchLog::addParam("Автор:", $value); break;
                    case 'person'    : $this->setPerson($value, 3); SearchLog::addParam("Персона:", $value); break;
                    case 'zaglavie'  : $this->where .= " and (UPPER(d.name) like UPPER('%{$value}%') or UPPER(d.short_name) like UPPER('%{$value}%'))"; SearchLog::addParam("Заглавие:", $value); break; 
                    case 'datestart' : if(preg_match("#[0-9]+#", $value)) { $this->where .= " and d.year >= '{$value}-01-01'"; SearchLog::addParam("Дата с:", $value); } break; 
                    case 'dateend'   : if(preg_match("#[0-9]+#", $value)) { $this->where .= " and d.year >= '{$value}-01-01'"; SearchLog::addParam("Дата по:", $value); } break;
                    case 'oglavlenie': $this->setOglavlenie($value); SearchLog::addParam("Оглавление:", $value); break;
                    case 'type'      : $this->where .= " and d.vid_dis = {$value}"; SearchLog::addParam("Вид дис.:", $value); break;
                    case 'ruk'       : $this->setPerson($value, 4); SearchLog::addParam("Руководитель:", $value); break;
                    case 'all'       : break;
                    case 'source'    : $this->where .= " and d.article_source_text like '%{$value}%'"; SearchLog::addParam("Источник:", $value); break;
                    case 'limit'     : $this->setLimit($params, $value);  break;
                    case 'sort'      : $this->setOrder($value); break;
                }                
            }
        }        
    }
    
    private function setOrder($value){
        switch ($value) {
            case 'author': $this->order = " order by p1.surname desc"; break;
            case 'year'  : $this->order = " order by d.year desc"; break;
            case 'title' : $this->order = " order by d.name desc"; break;
        }
    }
    
    private function setPerson($value, $type){
        $this->join .= " left join ".PREFIX."_rel_data_person rp{$type} using(id_data)";
        $this->join .= " left join ".PREFIX."_person p{$type} using(id_person)";
        
        $this->where .= " and rp{$type}.id_sp_type_person = {$type} and (UPPER(p{$type}.fio) like UPPER('%{$value}%') or UPPER(p{$type}.fam_dp) like UPPER('%{$value}%'))";
    }
    
    private function setOglavlenie($value){
        $this->join .= " left join ".PREFIX."_mapping m using(id_data)";
        
        $this->where .= " and UPPER(m.name) like UPPER('%{$value}%')";
    }       
}

?>