<?php

class StaticSearch extends Search {
    private $db;
    private $type;
    private $url = '';
    private $where = '';
    private $id = array();
    private $mode = array('sputnik'=>40, 'encyclopedia'=>41, 'others'=>'42, 47, 50 ,56');
    private $modeRu = array('sputnik'=>"Спутник", 'encyclopedia'=>"Энциклопедия", 'static'=>"Другие разделы");
    private $vid;
    public $cnt = array(1=>0, 2=>0, 3=>0, 4=>0); 
    
    public function __construct($mode) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->vid = ($mode != 'others')?$mode:'static';
    }
    
    public function search($params){
        $this->createQuery($params);
        SearchLog::addParam("Область поиска:", $this->modeRu[$this->vid]);
        $this->getIdToSearch($this->type);
        $id = implode(",", $this->id);
        $query = "SELECT sp.id_static_page, sp.name, sp.id_menu, sp.url, sp.id_static_page_top FROM ".PREFIX."_static_page sp where (sp.id_static_page_top in ({$id}) or sp.id_static_page in ({$id})) {$this->where}";
        $sth = $this->db->prepare("SELECT 
                                    sp.id_static_page, 
                                    sp.name, 
                                    sp.id_menu, 
                                    sp.url, 
                                    sp.id_static_page_top 
                                FROM ".PREFIX."_static_page sp 
                                where (sp.id_static_page_top in ({$id}) or sp.id_static_page in ({$id})) {$this->where} {$this->limit}");
        #echo "SELECT sp.id_static_page, sp.name, sp.id_menu, sp.url, sp.id_static_page_top FROM ".PREFIX."_static_page sp where sp.id_menu in ({$this->type}) {$this->where} {$this->limit}";
        $sth->execute();        
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if($this->vid == 'encyclopedia'){
            $ps = new PersonSearch(); $psResults = array(); $psResults = $ps->search($params);
            $os = new OrgSearch();  $osResults = array(); $osResults = $os->search($params);
            $rows = array_merge($rows, $psResults, $osResults); 
        }
        #Common::pre($rows);
        foreach($rows as $row){
            $result[$row['id_static_page']] = $row;
            $this->createURL($row);
            $result[$row['id_static_page']]['url'] = trim(str_replace("//", "/", str_replace("//", "/", $_SERVER['SERVER_NAME'].'/'.$this->vid.'/index/'.$this->url)), "/");
            $result[$row['id_static_page']]['id_sp_polnota'] = 0;
            $result[$row['id_static_page']]['short_name'] = $row['name'];
            $this->url = '';
        }        
        $this->getCNT($query, $this->db);
        SearchLog::addParam("Результат:", $this->cnt[4]);
        SearchLog::setLog();
        return $result;
    }
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function getIdToSearch($id){
        $sth = $this->db->query("select id_static_page from ".PREFIX."_static_page where id_static_page_top in ({$id})");
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        if($rows){
            foreach($rows as $row){                
                $this->getIdToSearch($row['id_static_page']);
                $this->id[] = $row['id_static_page'];
            }
        }
    }
    
    private function createQuery($params){
        foreach($params as $key=>$value){
            if(strpos($key, "_")!==false) list($dop, $name) = explode("_", $key); else $name = $key;
            if($value){
                switch ($name) {
                    case 'mode'      : $this->type = $this->mode[$value];  break;
                    case 'text'      : $this->where .= " and (UPPER(sp.text) like UPPER('%{$value}%') or UPPER(sp.name) like UPPER('%{$value}%'))"; SearchLog::addParam("Ntrcn:", $value); break;                    
                    case 'limit'     : $this->setLimit($params, $value);  break;
                }
            }
        }
    }
    
    private function createURL($result){
        if(!$result['url']){
            $sth = $this->db->query("SELECT url FROM ".PREFIX."_menu where id_menu = {$result['id_menu']}");
            $this->url = array_shift($sth->fetch(PDO::FETCH_ASSOC));
        }elseif($result['id_static_page_top'] && $result['url']){             
            $this->url = $result['url'].'/'.$this->url.'/'; 
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_static_page where id_menu = {$result['id_menu']} and id_static_page = {$result['id_static_page_top']}");            
            $res = $sth->fetch(PDO::FETCH_ASSOC);
            if($res['id_static_page_top']){
                $this->createURL($res);
            }else{
                $this->url = $res['url'].'/'.$this->url.'/'; 
            }
        }else{
            $this->url = $result['url'].'/';
        }
    }
}

?>
