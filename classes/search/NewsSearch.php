<?php

class NewsSearch extends Search {
    private $db;
    private $join = '';
    private $where = '';
    
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect(); 
    }
    
    public function search($params){
        $this->createQuery($params); 
        SearchLog::addParam("Область поиска:", "Новости");
        $query = "SELECT n.* FROM ".PREFIX."_news n {$this->join} where 1=1 {$this->where}";
        $sth = $this->db->prepare("SELECT 
                                     n.*
                                   FROM ".PREFIX."_news n
                                       {$this->join}
                                   where
                                       1=1
                                       {$this->where}
                                       {$this->limit}");
        $sth->execute();
        while($row = $sth->fetch(PDO::FETCH_ASSOC)){
            $result[$row['id_news']] = $row;
            $result[$row['id_news']]['url'] = $_SERVER['SERVER_NAME'].'/news/index/'.$row['url'];
            $result[$row['id_news']]['id_sp_polnota'] = 1;
            $result[$row['id_news']]['short_name'] = $row['zagolovok'];
        }
        $this->getCNT($query, $this->db);
        SearchLog::addParam("Результат:", $this->cnt[4]);
        SearchLog::setLog();
        return $result;
    }
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function createQuery($params){        
        foreach($params as $key=>$value){
            if(strpos($key, "_")!==false) list($dop, $name) = explode("_", $key); else $name = $key;     
            if($value){                
                switch ($name) {
                    case 'person'        : $this->setPerson($value); SearchLog::addParam("Персона:", $value); break;
                    case 'newsDatestart' : if(preg_match("#[0-9]+#", $value)) { $this->where .= " and n.date >= '".implode("-", array_reverse(explode(".",$value)))."'"; SearchLog::addParam("Дата с:", $value); } break; 
                    case 'newsDateend'   : if(preg_match("#[0-9]+#", $value)) { $this->where .= " and n.date >= '".implode("-", array_reverse(explode(".",$value)))."'"; SearchLog::addParam("Дата по:", $value); } break;
                    case 'obrazovanie'   : $this->setObrazovanie($value); SearchLog::addParam("Образование:", $value); break;
                    case 'organizaciya'  : $this->setOrganizaciya($value); SearchLog::addParam("Организация:", $value); break;
                    case 'theme'         : $this->setTheme($value); SearchLog::addParam("Тема:", $value); break;
                    case 'limit'         : $this->setLimit($params, $value);  break;
                }                
            }
        }        
    }
    
    private function setPerson($value){
        $this->join .= " left join ".PREFIX."_rel_news_person rp using(id_news)";        
        $this->where .= " and rp.id_person = {$value}";
    }
    
    private function setObrazovanie($value){
        $this->join .= " left join ".PREFIX."_rel_news_sp_education e using(id_news)";        
        $this->where .= " and e.id_sp_education = {$value}";
    }
    
    private function setOrganizaciya($value){
        $this->join .= " left join ".PREFIX."_rel_news_sp_organasation o using(id_news)";        
        $this->where .= " and o.id_sp_organasation = {$value}";
    }
    
    private function setTheme($value){
        $this->join .= " left join ".PREFIX."_rel_news_sp_theme_news t using(id_news)";        
        $this->where .= " and t.id_sp_theme_news = {$value}";
    }
}    
?>