<?php

abstract class Search {
    public $cnt = array(1=>0, 2=>0, 3=>0, 4=>0);
    protected $limit;

    protected function getCNT($query, $db){
        $sth = $db->query($query);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        if($result){
            foreach($result as $value){
                if($value){ 
                    ++$this->cnt[$value['id_sp_polnota']];
                    ++$this->cnt[4];
                }
            }
        }
    }
    
    protected function setLimit($params, $value){
        if($params['page']){ 
            $start = $value*($params['page']-1);
            $this->limit = ' limit '.($start).', '.($start+$value);
        }else{
            $this->limit = " limit 0,{$value}";
        }
    } 
}

?>