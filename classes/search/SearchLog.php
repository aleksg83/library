<?php

class SearchLog {
    
    private static $log = array();
    
    public static function addParam($name, $value){
        self::$log['params'][$name] = $value;
    }
    
    public static function getLastQuery($count = 10){
        $result = array();        
        #for($i=count($_SESSION['query'])-1, $j=count($_SESSION['query'])-$count; $i>=$j; $i--){
        if($_SESSION['query']){
            foreach($_SESSION['query'] as $value){
                #if($_SESSION['query'][$i]) $result[] = $_SESSION['query'][$i];
                if($value) $result[] = $value;
            }      
        }
        return $result;
    }
    
    public static function setLog(){
        if(!$_SESSION['query']) $_SESSION['query']=array();
        self::$log['url'] = $_SERVER["REDIRECT_URL"].'?'.$_SERVER['QUERY_STRING'];
        #$_SESSION['query'][] = self::$log;
        array_unshift($_SESSION['query'], self::$log);
        if(count($_SESSION['query'])>3) unset($_SESSION['query'][3]);
        self::$log = array();
    }
}
?>