<?php

class PersonSearch extends Search {
    
    private $db;
    
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect(); 
    }
    
    public function search($params){   
        #$this->setLimit($params, $params['limit']);    
        $sth = $this->db->prepare("SELECT 
                                     p.*, p.fio as `name`, CONCAT('person', '/', p.url) as `url`
                                   FROM ".PREFIX."_person p
                                   where 
                                       UPPER(p.fio) like '%{$params['e_text']}%' or 
                                       UPPER(p.fam_dp) like '%{$params['e_text']}%' or 
                                       UPPER(p.name) like '%{$params['e_text']}%' or 
                                       UPPER(p.surname) like '%{$params['e_text']}%' or 
                                       UPPER(p.patronymic) like '%{$params['e_text']}%'");        
        $sth->bindParam(1, $this->mode, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}

?>
