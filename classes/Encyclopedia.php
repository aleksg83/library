<?php
class Encyclopedia {
    private $db;
    private $params;
    private $dopparams;
    private $letter;
    private $request;
    private $type;
    private $idRubric;
    private $settings = array();
    private $month_arr = array();
    
    public function __construct($params, $dopparams) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->params=$params;
        $this->dopparams=$dopparams;
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        $this->month_arr = array( 
                            1 => $this->lang['month.jan'], 
                            2 => $this->lang['month.feb'], 
                            3 => $this->lang['month.mar'], 
                            4 => $this->lang['month.apl'], 
                            5 => $this->lang['month.may'], 
                            6 => $this->lang['month.iun'],  
                            7 => $this->lang['month.iul'], 
                            8 => $this->lang['month.avg'], 
                            9 => $this->lang['month.sen'],  
                            10 => $this->lang['month.okt'],  
                            11 => $this->lang['month.nob'], 
                            12 => $this->lang['month.dec']);
        $this->selectSettings();
        
    }
    
    public function setRequest($request){
        $tmp = explode("/", $request);   
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            array_pop($tmp);
        }        
        $this->request = implode("/", $tmp);   
    }
    #---------------------------------------------------------------------------
    public function createLetters(){
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Э', 'Ю', 'Я');
        $eng = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P' ,'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $modeRus= ( in_array($this->letter, $rus) || ($this->letter=="all") ) ? "view" : "none"; 
        $modeEng= (in_array($this->letter, $eng)) ? "view" : "none"; 
        $result = '<div class="abs" mode="'.$modeRus.'" rel="rus">';
        foreach ($rus as $value) {
            $class = ($this->letter == $value)?'class="act"':'';
            $result .= '<a href="/'.$this->request.'/letter/'.$value.'" '.$class.'>'.$value.'</a>';
        }
        $result .= '<a href="javascript:void(0);" class="alph" rel="eng">A-Z</a></div>';
        $result .= '<div class="abs" mode="'.$modeEng.'" rel="eng">';
        foreach ($eng as $value) {
            $class = ($this->letter == $value)?'class="act"':'';
            $result .= '<a href="/'.$this->request.'/letter/'.$value.'" '.$class.'>'.$value.'</a>';
        }
        $result .= '<a href="javascript:void(0);" class="alph" rel="rus">А-Я</a></div>';
        return $result;
    }
    #---------------------------------------------------------------------------
    public function getPage(){  
      $tmp = explode("/", $this->request);
      $dopUrl = $tmp[0]."/".$tmp[1]."/";
      $top=$this->getInfoTopMenu($tmp[0], 0);//верхнее меню
      if ($tmp[2]!="") $punct=$this->getInfoTopMenu($tmp[2], $top['id_menu']); //текущий пункт 
      if ($this->params==""){ //стартовая страница энциклопедии
          $return=$this->getInfoTopPage($tmp);
      }else{
          //определяем принадлежность
          $ArrType1 =  explode(",",$this->settings['EncyclopediaMenuType1']);
          $ArrType2 =  explode(",",$this->settings['EncyclopediaMenuType2']);
          $ArrType3 =  explode(",",$this->settings['EncyclopediaMenuType3']);
          $ArrType4 =  explode(",",$this->settings['EncyclopediaMenuType4']);
          $ArrCalendarj =  explode(",",$this->settings['EncyclopediaMenuCalendarj']);
          $ArrPerson =  explode(",",$this->settings['EncyclopediaMenuPerson']);
          $ArrOrg =  explode(",",$this->settings['EncyclopediaMenuOrg']);
          
         
          #-статические страницы (м.б. 4-типов,  для обработки вывода важны 3) ----------
          if ( in_array($punct['id_menu'], $ArrType1) || in_array($punct['id_menu'], $ArrType2) || in_array($punct['id_menu'], $ArrType3) || in_array($punct['id_menu'], $ArrType4) ){
            
           if ( in_array($punct['id_menu'], $ArrType1) ) {
              $this->type="type1";
               $this->request=$dopUrl.$tmp[2];
               if ( (count($this->params)==1) || ($this->params[1]=="letter") ){
                     $this->request=$dopUrl.$tmp[2];
               }else if (!strpos($this->params[1], ".html") && !strpos($this->params[2], ".html")){//раздел
                    $this->request=$dopUrl.$tmp[2]."/".$tmp[3];
               } 
           }else if ( in_array($punct['id_menu'], $ArrType2) ){
               $this->type="type2";
               $this->request=$dopUrl.$tmp[2]; 
           }else {
               $this->type="type3"; 
           } 
               $return=$this->getStaticPage($tmp, $punct, $dopUrl);           
              
          #-Календарь ----------------------------------------------------------   
          }else if (in_array($punct['id_menu'], $ArrCalendarj) ){
              $return=$this->getCalendarjPageEvents($tmp, $dopUrl, $punct);
              
          #-Персоны и Организации ----------------------------------------------  
          }else if ( in_array($punct['id_menu'], $ArrPerson) || in_array($punct['id_menu'], $ArrOrg) ){
              $this->type= (in_array($punct['id_menu'], $ArrPerson)) ? "person" : "org" ;
               #main page-------------------------------------------------------
              if ( ($this->params['1'] == "") || ($this->params['1']=="letter") ){ 
                  $this->request=$dopUrl.$tmp[2];
                  $return=$this->getPersonOrOrgMainPageList($tmp, $punct);
               #конечная страница ------------------------------------------------------  
               } else if ( strrpos($this->params['1'], ".html") || strrpos($this->params['2'], ".html") ) {
                  $return=$this->getPersonOrOrgPersonalPage();
               #рубрика--------------------------------------------------------   
               }else{
                   $this->request=$dopUrl.$tmp[2]."/".$tmp[3];
                   $return=$this->getPersonOrOrgMainPageList($tmp, $punct, "rubric");
               }
           #-Персоны и Организации---------------------------------------------
          }
      }
       return $return;
    }
    #---------------------------------------------------------------------------
     private function getCalendarjPageEvents($tmp, $dopUrl, $punct){
              $yearActive = ($tmp[3]!="")? $tmp[3]:  date("Y"); 
              $urlCalendar = $dopUrl.$punct['url_name']."/";
              $text="";
              
              $namePage=$this->lang['portal.calendar_title'].": ".$yearActive." ".$this->lang['portal.calendar_year'];
              $event= new Calendarj($this->params, $this->lang);
              
              
              #событие за год---------------------------------------------------
              if ($tmp[4]=="") { 
                  $datestart=$yearActive."-01-01";
                  $listEvent=$event->getListEvents($datestart, "year");
                  $period="year";
                  if ($yearActive==date("Y")) $text=$this->lang['portal.calendar_dafault'];
                      
              #событие за месяц-------------------------------------------------      
              }else{
                  $monthNow= (strlen($tmp[4])==1)? "0".$tmp[4] : $tmp[4];
                  $datestart=$yearActive."-".$monthNow."-01";
                  $listEvent=$event->getListEvents($datestart, "month");
                  $period="month";
                  $namePage.=", ".$this->month_arr[$tmp[4]];
              }  
               //Common::pre($listEvent);
              return array("name" => $namePage, "title" =>$namePage, "calendarj"=>$listEvent, "period"=>$period, "text"=>$text, "template"=>"calendar.php");
     }
     #---------------------------------------------------------------------------
      private function getPersonOrOrgMainPageList($tmp, $punct, $type="main"){
             if ($type=="main") {   
                $sth0 = $this->db->prepare('SELECT s.*  FROM '.PREFIX.'_menu m 
                                    LEFT JOIN '.PREFIX.'_static_page s  on s.`id_menu` = m.`id_menu` 
                                    WHERE 
                                      m.`url_name` = ? and
                                      m.`is_visible` = 1 and
                                      s.`is_visible` = 1 and
                                      s.`is_menu` = 1 
                                     ');
                 $sth0->bindParam(1, $tmp[2], PDO::PARAM_STR);
             }else{   
                  $sth0 = $this->db->prepare('SELECT m1.*  FROM '.PREFIX.'_menu m 
                                    LEFT JOIN '.PREFIX.'_rel_menu_rub rm on rm.`id_menu` = m.`id_menu` 
                                    LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu` = rm.`id_rub`     
                                    WHERE 
                                      m.`id_menu` = ? and 
                                      m.`is_visible` = 1 and
                                      m1.`url_name` = ? and 
                                      m1.`is_visible` = 1 and
                                      m1.`is_rubricator` = 1'); 
                   $sth0->bindParam(1, $punct['id_menu'], PDO::PARAM_INT);
                   $sth0->bindParam(2, $this->params[1], PDO::PARAM_STR);
             }    
                 $sth0->execute(); 
                 $page= $sth0->fetch(PDO::FETCH_ASSOC);
                 if ($type=="rubric") $this->idRubric = $page['id_menu'];
                 $letter_view = ($type=="main") ? ( (!$this->params['2'])?'all':urldecode($this->params['2']) ) : ( (!$this->params['3'])?'all':urldecode($this->params['3']) );
                 $this->setLetter($letter_view);
                 $letter=$this->createLetters();
                 $listperson= ($type=="main") ? $this->getListPerson() : $this->getListPersonRubric();
                 
                 $listpersonReturn='';
                 if ($this->type=="person"){
                       foreach($listperson as $person){
                           
                           if ($person['id_person_forwars']=="0"){
                               
                                #-----------------------------------------
                                if (trim($person['definition'])!="") {
                                        $tmpStr=explode(" ", trim($person['definition']));
                                        $tmpStr[0]='<span class="lowercase">'.$tmpStr[0].'</span>';
                                
                                        $person['definition']=implode("&nbsp;", $tmpStr);
                                } 
                                unset($tmpStr); 
                                #----------------------------------------   
                               
                               
                              $dopYears = ($person['years']!="") ?  ' ('.$person['years'].')' : ""; 
                              $dopDef =  ($person['definition']!="") ?  ' — '.$person['definition'] : "" ;   
                              $dopInfo=  $dopYears.$dopDef;   
                              $listpersonReturn.= '<li><a href="/'.$this->request."/".$person['url'].'">'.$person['fio'].'</a> <span>'.$dopInfo.'</span> </li>';
                           }else{
                              $forwars=$this->getSinglePersonId($person['id_person_forwars']);
                              $dopInfo= ($forwars['years']!="") ? '<span>('.$forwars['years'].')</span>' : "";
                              $listpersonReturn.= '<li>'.$person['fio'].' &mdash; см.&nbsp;&nbsp;<a href="/'.$this->request.'/'.$forwars['url'].'">'.$forwars['fio'].'</a> '.$dopInfo.'</li>';
                           }    
                        }
                 }else if ($this->type=="org"){
                       foreach($listperson as $person){
                           $listpersonReturn.= '<li><a href="/'.$this->request."/".$person['url'].'">'.$person['name'].'</a></li>';
                        }
                 }
                 
                 $countResurs= ($type=="main") ? $this->getCountDataPersonOrOrganisation() : $this->getCountDataPersonOrOrganisationRubric();
                 $countAllResurs['countOnPage']= $this->settings['countOnPagePersonOrganisation'];
                 
                 $template= ($type=="main") ? "person/index.php" : "person/list.php";
                 
                 if ($type=="main") {
                     $return=array("name" => $page['name'], "title" =>$page['title'], "description"=>$page['description'], 
                               "keywords"=>$page['keywords'], "text"=>$page['text'], "template"=>$template, 
                               "letter"=>$letter, "listperson"=>$listpersonReturn,
                                "countResurs"=> $countResurs, "countAllResurs"=>$countAllResurs);
                 }else{
                     
                      $npages=$this->getValueKeyDopparams('page');
                      if($npages){
                          $start=' start="'.($countAllResurs['countOnPage']*$npages-1).'"';
                      }else{
                          $start='';
                      }
                      
                      $return=array("name" => $page['name'], "title" =>$page['name'], "text"=>$page['text'], "template"=>$template, 
                               "letter"=>$letter, "listperson"=>$listpersonReturn, "countResurs"=> $countResurs, "countAllResurs"=>$countAllResurs,
                                "start"=>$start);
                 }    
          return $return;
    }
    #---------------------------------------------------------------------------
    private function getPersonOrOrgPersonalPage(){
     $url = strrpos($this->params['1'], ".html") ? $this->params['1'] : $this->params['2'];
     if ($this->type=="person")
           $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_person p WHERE p.`url` = ? and is_in_encyclopedia='1'");         
     if ($this->type=="org") 
           $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_sp_organasation p WHERE p.`url` = ? and is_in_encyclopedia='1'");         
     
     $sth->bindParam(1, $url, PDO::PARAM_STR);
     $sth->execute();
     $row = $sth->fetch(PDO::FETCH_ASSOC); 
                   
     if ($this->type=="person") {
        $dyear = ($row['is_bc']=="1") ? " ".$this->lang['portal.enciclopediya_sokr_do_n_eru'] : ""; 
        $dopinfo=($row['years']!="") ? " (".$row['years']." ".$this->lang['portal.enciclopediya_sokr_years'].$dyear.")" : " (".$this->lang['portal.enciclopediya_sokr_rog']." ".$row['year']." ".$this->lang['portal.enciclopediya_sokr_year'].$dyear.")" ;
        //$name=$row['fio'].$dopinfo;
        #-----------------------------------------
          if (trim($row['definition'])!="") {
               $tmpStr=explode(" ", trim($row['definition']));
               $tmpStr[0]='<span class="capitalize">'.$tmpStr[0].'</span>';
                                
               $row['definition']=implode("&nbsp;", $tmpStr);
          } 
          unset($tmpStr); 
        #----------------------------------------   
        $row['fio_full_enc']=str_replace("[b]", "<b>", $row['fio_full_enc']);
        $row['fio_full_enc']=str_replace("[/b]", "</b>", $row['fio_full_enc']);
        
         $name='
                <p>'.$row['fio_full_enc'].'</p>
                <p>'.$row['date_full_enc'].'</p>
                <p>'.$row['definition'].'</p>
               ';
         $photo= (file_exists(DROOT."/".$row['photo']) && ($row['photo']!="") && ($row['is_portret']=="1") )?"/".$row['photo']:"";
     }else{    
         $name=$row['name'];
         $photo= (file_exists(DROOT."/".$row['photo']) && ($row['photo']!="") )?"/".$row['photo']:"";
     }    
                   
     
      
     $template= ($this->type=="person") ? 'person/pageperson.php' : 'person/org.php' ; 
     
     return array("name" => $name, "title" =>$row['title'], "description"=>$row['description'], "photo" =>$photo, 
                                 "keywords"=>$row['keywords'], "text"=>$row['text'], "template"=>$template);
                   
    }               
    #---------------------------------------------------------------------------
    private function getStaticPage($tmp, $punct, $dopUrl){
       //точка входа
       $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                              WHERE 
                                                    p.`is_visible` = '1' and 
                                                    p.`id_menu` = ?");  
       $sth->bindParam(1, $punct['id_menu'], PDO::PARAM_INT);
       $sth->execute();
       $row = $sth->fetch(PDO::FETCH_ASSOC); 
       #--------------------------- type1--------------------------------------
       if ($this->type=="type1"){ //на стартовой - фильтр, 2 страница - фильтр в рубриках
          
          //-главная  или фильтр на главной ------------------------------------
           if ( (count($this->params)==1) || ($this->params[1]=="letter") ){
              $letter_view = (!$this->params['2'])? 'all' :urldecode($this->params['2']);
              $this->setLetter($letter_view);
              $letter=$this->createLetters();
              $this->idPageTop=$row['id_static_page'];
              $listpage=$this->getListChildStaticPageTwoLevel();
              $countResurs= $this->getCountDataStaticChildTwoLevel();
              $countAllResurs['countOnPage']= 3*$this->settings['countOnListInEnciclopediaStaticRazdel'];
              $listpersonReturn = $this->getTplTreeColumns($listpage, $this->settings['countOnListInEnciclopediaStaticRazdel'], "main");
              $template="listpage.php"; 
           
           #-----------раздел и фильтрация по разделу   
           }else if (!strpos($this->params[1], ".html") && !strpos($this->params[2], ".html")){
             $row=$this->getSingleStaticUrl($row['id_static_page'], $this->params[1]);
             $letter_view = 'all';
             $this->setLetter($letter_view);
             $this->idPageTop=$row['id_static_page'];
             $listpage= $this->getListChildStaticPage();
             $countResurs= $this->getCountDataStaticChild();
             $countAllResurs['countOnPage']= 3*$this->settings['countOnListInEnciclopediaStaticRazdel'];
             $listpersonReturn = $this->getTplTreeColumns($listpage, $this->settings['countOnListInEnciclopediaStaticRazdel']);
             $template="listpage.php"; 
             $nolistukaz="1"; //отмена вывода "Алфавитный указатель"
           #-- //конечная страница  
           }else{ 
              if (strpos($this->params[1], ".html")) $row=$this->getSingleStaticUrl($row['id_static_page'], $this->params[0]); 
             $this->recursiveGetStaticPage($row['id_static_page'], 1);          
             $row=$this->returnChild;
             $template="person/page.php";
           }
           
           $row['text']=$this->parserPage($row['text'],$punct);
           $return=array("name" => $row['name'], "title" =>$row['title'], "description"=>$row['description'], 
                         "keywords"=>$row['keywords'], "text"=>$row['text'], 
                         "letter"=>$letter, "listperson"=>$listpersonReturn, "template"=>$template,
                         "countResurs"=> $countResurs, "countAllResurs"=>$countAllResurs, "nolistukaz"=>$nolistukaz);
           
       #------------------------------- type 2----------------------------------
       }else if ($this->type=="type2"){ //на стартовой - фильтр, дальше только статьи
           
           if ( (count($this->params)==1) || ($this->params[1]=="letter") ){
              $letter_view = (!$this->params['2'])? 'all' :urldecode($this->params['2']);
              $this->setLetter($letter_view);
              $letter=$this->createLetters();
              $this->idPageTop=$row['id_static_page'];
              $listpage= $this->getListChildStaticPage();
              $countResurs= $this->getCountDataStaticChild();
              $countAllResurs['countOnPage']= 3*$this->settings['countOnListInEnciclopediaStaticRazdel'];
              $listpersonReturn = $this->getTplTreeColumns($listpage, $this->settings['countOnListInEnciclopediaStaticRazdel']);
              $template="listpage.php"; 
           }else{
             $this->recursiveGetStaticPage($row['id_static_page'], 1);          
             $row=$this->returnChild;
             $template="person/page.php";
           }
           
           $row['text']=$this->parserPage($row['text'],$punct);
           $return=array("name" => $row['name'], "title" =>$row['title'], "description"=>$row['description'], 
                         "keywords"=>$row['keywords'], "text"=>$row['text'], 
                         "letter"=>$letter, "listperson"=>$listpersonReturn, "template"=>$template,
                         "countResurs"=> $countResurs, "countAllResurs"=>$countAllResurs);
       #------------------------------- type 3 ---------------------------------
       }else{ //только статьи
           $template="";
           if (count($this->params)>1){
             $this->recursiveGetStaticPage($row['id_static_page'], 1);          
             $row=$this->returnChild;
             $template="person/page.php";
           }
           
           $row['text']=$this->parserPage($row['text'],$punct);
           $return=array("name" => $row['name'], "title" =>$row['title'], "description"=>$row['description'], 
                         "keywords"=>$row['keywords'], "text"=>$row['text'], "template"=>$template); 
             
       }
       return $return;
     }
    #--------------------------------------------------------------------------
     private function recursiveGetStaticPage($id, $i){
         $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                               WHERE 
                                                    p.`url` = ? and
                                                    p.id_static_page_top = ?");
         $sth->bindParam(1, $this->params[$i], PDO::PARAM_STR);
         $sth->bindParam(2, $id, PDO::PARAM_INT);
         $sth->execute();
         $row = $sth->fetch(PDO::FETCH_ASSOC); 
         $i++;
         if ($i<count($this->params)){
            $this->recursiveGetStaticPage($row['id_static_page'], $i);
         }else{
           $this->returnChild=$row;
         } 
     }
    #---------------------------------------------------------------------------
     public function getUrlPage($id, $start){
       $id=intval($id);
       if ($start) $this->urlPage="";
       
       $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1' and id_static_page=?");
       $sth->bindParam(1, $id, PDO::PARAM_STR);
       $sth->execute(); 
       $res_temp = $sth->fetch(PDO::FETCH_ASSOC); 
       $this->urlPage.=$res_temp['url']."*";
       $id_top=$res_temp['id_static_page_top'];
       
       if ($id_top!="0") {
           $this->getUrlPage($id_top, false);   
       }else{
           $this->urlPage=trim($this->urlPage, "*");
           $arrUrl=  explode("*", $this->urlPage);
           $this->urlPage="/";
           for ($i=count($arrUrl)-3; $i>=0;$i--){
               $dop= ($i==0) ? "" : "/";
               $this->urlPage.=$arrUrl[$i].$dop;
           }
       }
        return $this->urlPage;
   } 
   
   public function parserPage($text, $punct){
       $text=stripcslashes($text);
       $text=$this->parserUrlPage($text, $punct);
       return $text;
   }
   
   public function parserUrlPage($text, $punct){
       $i=0; 
       while (strpos($text,"[[~",$i)!=false){
           $pos=strpos($text,"[[~",$i);
           $pos2=strpos($text,"~]]",$pos+2);
           if ($pos2){
                $id=substr($text,$pos+3,$pos2-$pos-3);
                $replaceTarget="[[~".$id."~]]";
                $replace = "/".trim(URL_ENCYCLOPEDIA, "/")."/".$punct['url_name'].$this->getUrlPage($id, true);
                $text = str_replace($replaceTarget, $replace, $text);
                $i=$pos2;
           }
       }
      
     return $text;  
   } 
    #---------------------------------------------------------------------------  
     
    public function setLetter($letter){
        $this->letter = $letter;
    }
    #---------------------------------------------------------------------------  
    private function getInfoTopPage($tmp){
       $urlSection=$tmp[0]."/".$tmp[1]."/"; 
       $urlDop=$tmp[0]."/";
       $sth = $this->db->prepare('SELECT s.* FROM '.PREFIX.'_menu m 
                                             LEFT JOIN '.PREFIX.'_static_page s on s.`id_menu` = m.`id_menu`
                                             WHERE 
                                                       (m.`url` = ? or m.`url` = ? or m.`url` = ? )and 
                                                        m.`is_visible` = 1 and 
                                                        m.`is_rubricator` = 0 and
                                                        s.`is_visible` = 1 and 
                                                        s.`is_menu` = 1');
       $sth->bindParam(1, $tmp[0], PDO::PARAM_STR);
       $sth->bindParam(2, $urlSection, PDO::PARAM_STR);
       $sth->bindParam(3, $urlDop, PDO::PARAM_STR);
       $sth->execute();
       $result=$sth->fetch(PDO::FETCH_ASSOC);
       
       return $result;
     }  
     #--------------------------------------------------------------------------
     private function getInfoTopMenu($request, $top){
        
       $url=$request;
       $url1=$request."/";
       $sth = $this->db->prepare('SELECT m.* FROM '.PREFIX.'_menu m 
                                    WHERE (m.`url_name` = ? or m.`url_name` = ?) and
                                    m.`id_menu_top` = ? and
                                    m.`is_visible` = 1  and 
                                    m.`is_rubricator` = 0 
                                    ');
        $sth->bindParam(1, $url, PDO::PARAM_STR);
        $sth->bindParam(2, $url1, PDO::PARAM_STR);
        $sth->bindParam(3, $top, PDO::PARAM_INT);
        $sth->execute();  
        return $sth->fetch(PDO::FETCH_ASSOC);
         
         
     }
     #--------------------------------------------------------------------------
     private function getListPerson(){
       $page=$this->getValueKeyDopparams('page');
       if($page){
            $start = $this->settings['countOnPagePersonOrganisation']*($page-1);        
            $lim = ' LIMIT '.($start).', '.($start+$this->settings['countOnPagePersonOrganisation']);
       }elseif($this->settings['countOnPagePersonOrganisation']){
            $lim = ' LIMIT 0, '.$this->settings['countOnPagePersonOrganisation']; 
       } 
         
       if ($this->type=="person"){ 
            if ($this->letter=="all"){
                $sth = $this->db->query("SELECT p.*, CONCAT(SUBSTRING(p.`fio`, 1, 1)) as 'short_name' FROM ".PREFIX."_person p WHERE p.`is_in_encyclopedia` = 1 order by p.fio {$lim}");        
            }else{ 
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`fio`, 1, 1)) as 'short_name' FROM ".PREFIX."_person p WHERE SUBSTRING(p.`fio`, 1, 1) = ? and p.`is_in_encyclopedia` = 1 order by p.fio {$lim}");        
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->execute();
            }     
       }else if ($this->type=="org"){ 
            if ($this->letter=="all"){
                $sth = $this->db->query("SELECT p.*, CONCAT(SUBSTRING(p.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_sp_organasation p WHERE p.`is_in_encyclopedia` = 1 order by p.name {$lim}");        
            }else{ 
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_sp_organasation p WHERE SUBSTRING(p.`name`, 1, 1) = ? and p.`is_in_encyclopedia` = 1 order by p.name {$lim}");        
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->execute();
            }     
        }
        
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;         
     }
     #--------------------------------------------------------------------------
     private function getListPersonRubric(){
       $page=$this->getValueKeyDopparams('page');
       if($page){
            $start = $this->settings['countOnPagePersonOrganisation']*($page-1);        
            $lim = ' LIMIT '.($start).', '.($start+$this->settings['countOnPagePersonOrganisation']);
       }elseif($this->settings['countOnPagePersonOrganisation']){
            $lim = ' LIMIT 0, '.$this->settings['countOnPagePersonOrganisation']; 
       } 
       if ($this->type=="person"){ 
            if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`fio`, 1, 1)) as 'short_name' FROM ".PREFIX."_person p
                                                                   LEFT JOIN ".PREFIX."_rel_person_menu rp on p.`id_person` = rp.`id_person`
                                                                   LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                   WHERE 
                                                                        p.`is_in_encyclopedia` = 1 and
                                                                        m.is_rubricator = 1 and
                                                                        m.id_menu = ?
                                                                        order by p.fio {$lim}");  
                $sth->bindParam(1, $this->idRubric, PDO::PARAM_INT);
            }else{ 
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`fio`, 1, 1)) as 'short_name' FROM ".PREFIX."_person p 
                                                                   LEFT JOIN ".PREFIX."_rel_person_menu rp on p.`id_person` = rp.`id_person`
                                                                   LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                   WHERE 
                                                                        SUBSTRING(p.`fio`, 1, 1) = ? and 
                                                                        p.`is_in_encyclopedia` = 1 and
                                                                        m.is_rubricator = 1 and
                                                                        m.id_menu = ?
                                                                        order by p.fio {$lim}");        
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idRubric, PDO::PARAM_INT);
            }     
       }else if ($this->type=="org"){ 
            if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_sp_organasation p 
                                                                     LEFT JOIN ".PREFIX."_rel_sp_organasation_menu rp on p.`id_sp_organasation` = rp.`id_sp_organasation`
                                                                     LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                     WHERE 
                                                                         p.`is_in_encyclopedia` = 1 and
                                                                         m.is_rubricator = 1 and
                                                                         m.id_menu = ?
                                                                         order by p.name {$lim}");
                 $sth->bindParam(1, $this->idRubric, PDO::PARAM_INT);                                                         
            }else{ 
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_sp_organasation p 
                                                                       LEFT JOIN ".PREFIX."_rel_sp_organasation_menu rp on p.`id_sp_organasation` = rp.`id_sp_organasation`
                                                                       LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                       WHERE 
                                                                       SUBSTRING(p.`name`, 1, 1) = ? and 
                                                                       p.`is_in_encyclopedia` = 1 and
                                                                       m.id_menu = ? 
                                                                       order by p.name {$lim}");        
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idRubric, PDO::PARAM_INT);  
            }     
        }
        $sth->execute();
        
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;         
     }
    #--------------------------------------------------------------------------
    public function getSinglePersonId($id){        
        $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`fio`, 1, 1)) as 'short_name' FROM ".PREFIX."_person p WHERE id_person = ? and p.`is_in_encyclopedia` = 1");        
        $sth->bindParam(1, $id, PDO::PARAM_STR);
        $sth->execute();
        $person = $sth->fetch(PDO::FETCH_ASSOC);
        return $person; 
    }
    #--------------------------------------------------------------------------
    public function getCountDataPersonOrOrganisation(){    
     if ($this->type=="person"){ 
         if ($this->letter=="all"){
                $sth = $this->db->query("SELECT count(*) FROM ".PREFIX."_person p WHERE p.`is_in_encyclopedia` = 1 ");
         }else{
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_person p WHERE SUBSTRING(p.`fio`, 1, 1) = ? and p.`is_in_encyclopedia` = 1 ");
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->execute();
         }                           
    }else if ($this->type=="org"){ 
         if ($this->letter=="all"){
                $sth = $this->db->query("SELECT count(*) FROM ".PREFIX."_sp_organasation p WHERE p.`is_in_encyclopedia` = 1 ");
         }else{
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_sp_organasation p WHERE SUBSTRING(p.`name`, 1, 1) = ? and p.`is_in_encyclopedia` = 1 ");
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->execute();
         } 
    }
     
     return array_shift($sth->fetch(PDO::FETCH_ASSOC));
    } 
    #--------------------------------------------------------------------------
    public function getCountDataPersonOrOrganisationRubric(){    
     if ($this->type=="person"){ 
         if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_person p 
                                                         LEFT JOIN ".PREFIX."_rel_person_menu rp on p.`id_person` = rp.`id_person`
                                                         LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                   WHERE 
                                                                        p.`is_in_encyclopedia` = 1 and
                                                                        m.is_rubricator = 1 and
                                                                        m.id_menu = ?");
                $sth->bindParam(1, $this->idRubric, PDO::PARAM_INT); 
         }else{
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_person p 
                                                                   LEFT JOIN ".PREFIX."_rel_person_menu rp on p.`id_person` = rp.`id_person`
                                                                   LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                   WHERE 
                                                                        SUBSTRING(p.`fio`, 1, 1) = ? and 
                                                                        p.`is_in_encyclopedia` = 1 and
                                                                        m.is_rubricator = 1 and
                                                                        m.id_menu = ?");
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idRubric, PDO::PARAM_INT);
              
         }                           
    }else if ($this->type=="org"){ 
         if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_sp_organasation p 
                                                                     LEFT JOIN ".PREFIX."_rel_sp_organasation_menu rp on p.`id_sp_organasation` = rp.`id_sp_organasation`
                                                                     LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                     WHERE 
                                                                         p.`is_in_encyclopedia` = 1 and
                                                                         m.is_rubricator = 1 and
                                                                         m.id_menu = ?");
                $sth->bindParam(1, $this->idRubric, PDO::PARAM_INT); 
                
         }else{
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_sp_organasation p 
                                                                     LEFT JOIN ".PREFIX."_rel_sp_organasation_menu rp on p.`id_sp_organasation` = rp.`id_sp_organasation`
                                                                     LEFT JOIN ".PREFIX."_menu m on m.`id_menu` = rp.`id_menu`
                                                                     WHERE 
                                                                         SUBSTRING(p.`name`, 1, 1) = ? and 
                                                                         p.`is_in_encyclopedia` = 1 and
                                                                         m.is_rubricator = 1 and
                                                                         m.id_menu = ?");
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idRubric, PDO::PARAM_INT); 
         } 
    }
       $sth->execute();
       return array_shift($sth->fetch(PDO::FETCH_ASSOC));
    }  
          
    #--------------------------------------------------------------------------
     private function getListChildStaticPage(){
       $page=$this->getValueKeyDopparams('page');
       if($page){
            $start = $this->settings['countOnListInEnciclopediaStaticRazdel']*3*($page-1);        
            $lim = ' LIMIT '.($start).', '.($start+3*$this->settings['countOnListInEnciclopediaStaticRazdel']);
       }elseif($this->settings['countOnPagePersonOrganisation']){
            $lim = ' LIMIT 0, '.(3*$this->settings['countOnListInEnciclopediaStaticRazdel']); 
       } 
       
       if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_static_page p WHERE p.`is_visible` = 1 and p.`id_static_page_top`= ? order by p.name {$lim}");        
                $sth->bindParam(1, $this->idPageTop, PDO::PARAM_INT);
       }else{ 
                $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_static_page p WHERE SUBSTRING(p.`name`, 1, 1) = ? and p.`is_visible` = 1 and p.`id_static_page_top`= ? order by p.name {$lim}");        
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idPageTop, PDO::PARAM_INT);
       }     
       $sth->execute();
       $result = $sth->fetchAll(PDO::FETCH_ASSOC);
       return $result;         
    }
    #--------------------------------------------------------------------------
     private function getListChildStaticPageTwoLevel(){
       $page=$this->getValueKeyDopparams('page');
       if($page){
            $start = $this->settings['countOnListInEnciclopediaStaticRazdel']*3*($page-1);        
            $lim = ' LIMIT '.($start).', '.($start+3*$this->settings['countOnListInEnciclopediaStaticRazdel']);
       }elseif($this->settings['countOnPagePersonOrganisation']){
            $lim = ' LIMIT 0, '.(3*$this->settings['countOnListInEnciclopediaStaticRazdel']); 
       } 
       if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT p2.*, CONCAT(p1.`url`, '/', p2.`url`) as url FROM ".PREFIX."_static_page p
                                                       LEFT JOIN ".PREFIX."_static_page p1 on p1.`id_static_page_top` = p.`id_static_page`                                
                                                       LEFT JOIN ".PREFIX."_static_page p2 on p2.`id_static_page_top` = p1.`id_static_page`                                    
                                                       WHERE 
                                                            p.`is_visible` = 1 and 
                                                            p1.`is_visible` = 1 and 
                                                            p2.`is_visible` = 1 and 
                                                            p.`id_static_page`= ? order by p2.name {$lim}");        
                $sth->bindParam(1, $this->idPageTop, PDO::PARAM_INT);
       }else{ 
                $sth = $this->db->prepare("SELECT p2.*, CONCAT(p1.`url`, '/', p2.`url`) as url, CONCAT(SUBSTRING(p2.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_static_page p
                                                       LEFT JOIN ".PREFIX."_static_page p1 on p1.`id_static_page_top` = p.`id_static_page`                                
                                                       LEFT JOIN ".PREFIX."_static_page p2 on p2.`id_static_page_top` = p1.`id_static_page`                                    
                                                       WHERE 
                                                            p.`is_visible` = 1 and 
                                                            p1.`is_visible` = 1 and 
                                                            p2.`is_visible` = 1 and 
                                                            SUBSTRING(p2.`name`, 1, 1) = ? and
                                                            p.`id_static_page`= ? order by p2.name {$lim}"); 
           
               // $sth = $this->db->prepare("SELECT p.*, CONCAT(SUBSTRING(p.`name`, 1, 1)) as 'short_name' FROM ".PREFIX."_static_page p WHERE SUBSTRING(p.`name`, 1, 1) = ? and p.`is_visible` = 1 and id_static_page_top= ? order by p.name {$lim}");        
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idPageTop, PDO::PARAM_INT);
       }     
       $sth->execute();
       $result = $sth->fetchAll(PDO::FETCH_ASSOC);
       return $result;         
    }
    #--------------------------------------------------------------------------
    public function getCountDataStaticChild(){    
    if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_static_page p WHERE p.`is_visible` = 1 and p.`id_static_page_top`= ?");
                $sth->bindParam(1, $this->idPageTop, PDO::PARAM_INT);
    }else{
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_static_page p  WHERE SUBSTRING(p.`name`, 1, 1) = ? and p.`is_visible` = 1 and p.`id_static_page_top`= ?");
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idPageTop, PDO::PARAM_INT);
     }                           
     $sth->execute();
     return array_shift($sth->fetch(PDO::FETCH_ASSOC));
    } 
    #--------------------------------------------------------------------------
    public function getCountDataStaticChildTwoLevel(){    
    if ($this->letter=="all"){
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_static_page p 
                                                           LEFT JOIN ".PREFIX."_static_page p1 on p1.`id_static_page_top` = p.`id_static_page`                                
                                                           LEFT JOIN ".PREFIX."_static_page p2 on p2.`id_static_page_top` = p1.`id_static_page`                                    
                                                           WHERE 
                                                            p.`is_visible` = 1 and 
                                                            p1.`is_visible` = 1 and 
                                                            p2.`is_visible` = 1 and                     
                                                            p.`id_static_page`= ?");
                $sth->bindParam(1, $this->idPageTop, PDO::PARAM_INT);
    }else{
                $sth = $this->db->prepare("SELECT count(*) FROM ".PREFIX."_static_page p  
                                                           LEFT JOIN ".PREFIX."_static_page p1 on p1.`id_static_page_top` = p.`id_static_page`                                
                                                           LEFT JOIN ".PREFIX."_static_page p2 on p2.`id_static_page_top` = p1.`id_static_page`                                    
                                                           WHERE 
                                                            p.`is_visible` = 1 and 
                                                            p1.`is_visible` = 1 and 
                                                            p2.`is_visible` = 1 and                    
                                                            SUBSTRING(p2.`name`, 1, 1) = ? and 
                                                            p.`id_static_page`= ?");
                $sth->bindParam(1, $this->letter, PDO::PARAM_STR);
                $sth->bindParam(2, $this->idPageTop, PDO::PARAM_INT);
     }                           
     $sth->execute();
     return array_shift($sth->fetch(PDO::FETCH_ASSOC));
    } 
    #--------------------------------------------------------------------------
    public function getSingleStaticId($id){  
        $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p WHERE p.`id_static_page` = ? and p.`is_visible` = 1");        
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->execute();
        $page = $sth->fetch(PDO::FETCH_ASSOC);
        return $page; 
    }
    #--------------------------------------------------------------------------
    public function getSingleStaticUrl($idTop, $url){  
        $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                    WHERE 
                                        p.`id_static_page_top` = ? and 
                                        p.`is_visible` = 1 and
                                        p.`url` = ?
                                        ");        
        $sth->bindParam(1, $idTop, PDO::PARAM_INT);
        $sth->bindParam(2, $url, PDO::PARAM_STR);
        $sth->execute();
        $page = $sth->fetch(PDO::FETCH_ASSOC);
        return $page; 
    }
    #--------------------------------------------------------------------------
      private function selectSettings(){
        $sth=$this->db->query("SELECT `name`, `value` FROM ".PREFIX."_main_setting");
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->settings[$row['name']] = $row['value'];
        }
    }
    #---------------------------------------------------------------------------
     private function getValueKeyDopparams($k){
       $val="";
       if ($this->dopparams){
            foreach ($this->dopparams as $key => $value) {
                if ($key==$k)  $val=$value;
            }
       }
       return $val;
     } 
     #--------------------------------------------------------------------------
      private function getTplTreeColumns($listpage, $cnt, $main="no"){
          $res = array_chunk($listpage, $cnt);
              $out='';
              foreach($res as $div){
                 $out .= '<ul class="authors">';
                    foreach($div as $person){
                        if ($person['link_out']!=""){
                            $out .= '<li><a href="'.$person['link_out'].'">'.$person['name'].'</a></li>';
                        }else if ( ($person['id_page_forwars']=="0") || ($person['id_page_forwars']=="") ){
                            $out .= '<li><a href="/'.$this->request.'/'.$person['url'].'">'.$person['name'].'</a></li>';
                        }else{
                            $page=$this->getSingleStaticId($person['id_page_forwars']);
                            
                            #---
                            if ($main=="main") {
                                $parentR=$this->getSingleStaticId($page['id_static_page_top']);
                                $page['url']=$parentR['url']."/".$page['url'];
                            }
                            #---
                            $url_forwars= ($page['link_out']!="")  ? $page['link_out']  : '/'.$this->request.'/'. $page['url'].'' ;
                            
                            $out .= '<li>'.$person['name'].' &mdash; см.&nbsp;&nbsp;<a href="'.$url_forwars.'">'. $page['name'].'</a></li>';
                        }    
                    }
                $out .= '</ul>';
              }
         return  $out;
      }
}