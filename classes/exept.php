<?php
class exept {
    
    private $fc, $view, $db;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();
        $sql = new Sql();
        $this->db = $sql->connect();  
        
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        
        $menu = new Menu("lib/");        
        $this->view->menuTop = $menu->getTopMenu(); 
        $this->view->menuLeft = $menu->getLeftMenu($this->params);
    }
    
    public function show404(){
        $this->view->metatitle=$this->lang['portal.error_title'];
        $this->view->content=$this->lang['portal.error_404'];
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/exept/404.php');
        $this->fc->setBody($result);
    }
    
    public function blockSite(){
        $this->view->metatitle=$this->lang['portal.block_site_title'];
        $this->view->content=$this->lang['portal.block_site_text'];
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/exept/block.php');
        $this->fc->setBody($result);
    }
    
    public function userActivate($email, $type){
      if ( ($email!="") && ($type!="")){
        $type=(intval($type)>1 || intval($type)<0) ? 1 : $type; 
        $sth0 = $this->db->prepare("SELECT email FROM ".PREFIX."_user_email WHERE email=?");          
        $sth0->bindParam(1, $email, PDO::PARAM_STR);
        $sth0->execute();
        if($sth0->rowCount()>0){
          if ($type==1){
              
              $form.="<div class='activate_end'> <p>".$this->lang['portal.user_activate_sex']."</p>";
              $form.='  <p class="informer"></p>
                        <p> <input type="radio" name="sex_activate" value="men" /> '.$this->lang['portal.user_sex_men'].' </p>
                        <p> <input type="radio" name="sex_activate" value="women" /> '.$this->lang['portal.user_sex_women'].' </p>
                        <p> <button class="end_activate_button form" title="'.$this->lang['portal.user_sex_next'].'">'.$this->lang['portal.user_sex_next'].'</button></p>
                      </div>';
              return $form;
              
          }else{  
              
                $sth = $this->db->prepare("UPDATE ".PREFIX."_user_email SET is_active=? WHERE email=?");        
                $sth->bindParam(1, $type, PDO::PARAM_INT);
                $sth->bindParam(2, $email, PDO::PARAM_STR);
                $sth->execute();        
                $err = $sth->errorInfo();
                     if ($err[0] != '00000'){
                         return "<p>".$this->lang['portal.user_no_activate']."</p>";
                     }else{
                         return ($type==1) ? "<p>Email <b>".$email."</b> ".$this->lang['portal.user_yes_activate'].".</p>" : "<p>Email <b>".$email."</b> ".$this->lang['portal.user_dez_activate'].".</p>";
                     }
          } 
        }else{
            return "<p>".$this->lang['portal.user_no_activate']." - ".$this->lang['portal.user_email_no_db_st']." ".$email." ".$this->lang['portal.user_email_no_db'].".</p>";
        }         
      }else{
          return"<p>".$this->lang['portal.user_activate_error_parametr']."</p>";
      } 
        
    }
    
    
    public function getRassulkaAboutAndSenderInfo($id){
        $sth = $this->db->prepare("SELECT about, info_after_sender FROM ".PREFIX."_rassilka_setting WHERE id_rassilka_setting=?");          
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->execute();
        $return = $sth->fetch(PDO::FETCH_ASSOC); 
        return  $return;
    }
    
}

?>
