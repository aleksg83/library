<?php
class Calendarj {
    
    private $db;
    private $settings = array();
    private $params;
    private $request;
    public  $lang;
    private $position;
    private $dateArray= array();

    public function __construct($params, $lang) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->params = $params;
        $this->lang= $lang;
        $this->selectSettings();        
    }
    
    
    public function getMainListEvents(){
        $now=date("Y-m-d");
        $date=new DateTime($now);
        $dateEnd=new DateTime($now);
        $dateEnd->modify('+1 month');
        $dEnd=$dateEnd->format("Y-m-d");
        
        $this->position=0;
        #----------------для адресации в энциклопедию
         $ArrPerson =  explode(",",$this->settings['EncyclopediaMenuPerson']);
         $ArrOrg =  explode(",",$this->settings['EncyclopediaMenuOrg']);
         $punctPerson=$this->getInfoIdMenu($ArrPerson[0]); //Персоны
         $punctOrg=$this->getInfoIdMenu($ArrOrg[0]); //Организации
                 
         do{
             $tempArray=array();
             $tempArray = $this->getMainListEventsCikl($date, $punctPerson, $punctOrg);
             $this->dateArray=array_merge($this->dateArray, $tempArray);
             $date->modify('+1 day');
             $dStr=$date->format("Y-m-d");
             
             
         } while ( (count($this->dateArray) < 6) && ($dStr < $dEnd) ); 
        
         return  array_slice($this->dateArray, 0, 6);
    }
    
    public function getMainListEventsCikl($date, $punctPerson, $punctOrg){
        //$now=date("Y-m-d");
        //$date=new DateTime($now);
        $dateEventFull=$date->format("Y-m-d");
        $dateDay=$date->format('d');
        $dateDay2=$date->format('j');
        $dateMonth=$date->format('m');
        $dateYear=$date->format('Y');
        $return=array();
        $returnAll=array();
        //$position=0;
        
       /* #----------------для адресации в энциклопедию
         $ArrPerson =  explode(",",$this->settings['EncyclopediaMenuPerson']);
         $ArrOrg =  explode(",",$this->settings['EncyclopediaMenuOrg']);
         $punctPerson=$this->getInfoIdMenu($ArrPerson[0]); //Персоны
         $punctOrg=$this->getInfoIdMenu($ArrOrg[0]); //Организации
        #---------------- 
        */
        #-----------------------------------------------------------------------
        #1. List Person and Organization 
        $sth = $this->db->query("SELECT id_person, day, month, year, short_text, is_bc, fio, url, photo FROM ".PREFIX."_person WHERE is_view_in_main='1' and is_in_encyclopedia='1' and id_person_forwars='0'");
        $result1 = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        $sth2= $this->db->query("SELECT id_sp_organasation, day, month, year, short_text, url, photo, name FROM ".PREFIX."_sp_organasation WHERE is_view_in_main='1' and is_in_encyclopedia='1'");
        $result2 = $sth2->fetchAll(PDO::FETCH_ASSOC);
        
        $result=array_merge($result1, $result2);
        
        for ($j=0; $j<count($result); $j++){
            $day = ( ($result[$j]['day']=="") || ($result[$j]['day']=="0") ) ? "" : intval($result[$j]['day']);
            $month = ( ($result[$j]['month']=="") || ($result[$j]['month']=="0") ) ? "" : $result[$j]['month'];
            $year = ( ($result[$j]['year']=="") || ($result[$j]['year']=="0") ) ? "" : $result[$j]['year'];
            $bc =  ($result[$j]['is_bc']!="") ? $result[$j]['is_bc'] : 0 ;
            $add=false;
            //если год рождения не нулевой, то проверяем - выводить ли в этом году
            if ($year!=""){
                $date_out=Common::multiplicityOfFive($year, $month, $day, $dateYear, $dateMonth, $dateDay, $bc);
                if ($date_out == $dateYear){
                    
                    if ($result[$j]['name']!="") $result[$j]['fio']=$result[$j]['name']; //для организаций
                    
                    $images= ( ($result[$j]['photo']!="") && file_exists(DROOT."/".$result[$j]['photo']) ) ? "/".$result[$j]['photo'] : MAIN_TEMPLATE."/images/calendarj/default.png";
                    $difference= ($bc == "0")? intval($date_out)-intval($year):  intval($date_out)+intval($year)-1;
                    $target=($this->position==0)? "act" : "none";
                    
                     if ( ($day!="") && ($month!="")){
                         if ( ($day==$dateDay) && ($month==$dateMonth) ){
                            $dateInfo=$day." ".$this->getMonthName(date('n'))." &mdash; ".$difference. " ".$this->lang['portal.year']; 
                            $add=true; 
                         }    
                         
                     } else if ($month!=""){
                          if ($month==$dateMonth) {
                            $dateInfo=$this->getMonthName(date('n'), "im")." &mdash; ".$difference. " ".$this->lang['portal.year']; 
                            $add=true; 
                          }    
                     } else {
                         $dateInfo=$this->lang['portal.in_year']." &mdash; ".$difference. " ".$this->lang['portal.year']; 
                         $add=true; 
                     }    
                         
                     if ($add) { 
                                    
                             //$urlEvents= ($result[$j]['id_person']!="") ? "#person" : "#orgenisation";
                             $urlEvents= ($result[$j]['id_person']!="") ? "/".URL_ENCYCLOPEDIA.$punctPerson['url_name']."/".$result[$j]['url'] : "/".URL_ENCYCLOPEDIA.$punctOrg['url_name']."/".$result[$j]['url'];
                             
                             $tempArray=array("date"=>$dateInfo, "title"=>$result[$j]['fio'], "text"=>$result[$j]['short_text'], "position"=>$this->position, "target"=>$target, "images"=>$images, "url"=>$urlEvents);
                             
                             if (!$this->compareArrayInEvents($this->dateArray, $tempArray)){
                                array_push($return, $tempArray);
                             }   
                             
                             $this->position++;
                     }     
                             
                 }    
             }
                    
                    
         }//for Person, Organization
        #-----------------------------------------------------------------------
        # 2. Record Calendarj
        $sth3 = $this->db->query("SELECT * FROM ".PREFIX."_calendar WHERE is_main='1' and is_visible='1' ORDER BY date_from desc");
        $events = $sth3->fetchAll(PDO::FETCH_ASSOC);
        $images=MAIN_TEMPLATE."/images/calendarj/default.png";
        
         for ($j=0; $j<count($events); $j++){
             
             $startEv = new DateTime($events[$j]['date_from']);
             $endEv = new DateTime($events[$j]['date_to']);
             #------------------------------------------
             do{ 
                    $dEv1=$startEv->format("Y-m-d"); //дата старта события
                    $dEv1day=$startEv->format("d");  //день начала события
                    $dEv1day2=$startEv->format("j");  //день начала события
                    $dEv1month=$startEv->format("m"); //месяц начала события
                    $dEv1year=$startEv->format("Y"); //год начала события
                    
                    $dEv2=$endEv->format("Y-m-d");  //дата окончания события
                    $dEv2day=$endEv->format("d"); //день окончания события
                    $dEv2day2=$endEv->format("j");  //день начала события
                    $dEv2month=$endEv->format("m"); //месяц окончания события
                    $dEv2year=$endEv->format("Y"); //год окончания события
                    $target=($this->position==0)? "act" : "none";
             
                      //если диапазон события входит в текущую дату, то включаем в календарь
                      if ( ($dEv1<=$dateEventFull) && ($dEv2>=$dateEventFull) ) {
                            
                          if ($dEv1year==$dEv2year){   //совпадают года события
                              if ($dEv1month==$dEv2month){ // совпадают месяцы событий
                                  
                                    if ($dEv1day==$dEv2day){ // совпадают дни событий
                                        $period=$dEv1day2." ".Common::getMonthName(trim($dEv1month))." ".$dateYear;
                                    }else{
                                        $period=$dEv1day2." &mdash; ".$dEv2day2." ".Common::getMonthName(trim($dEv1month))." ".$dateYear;
                                    }
                                    
                                  
                              }else{
                                  $period=$dEv1day2." ".Common::getMonthName(trim($dEv1month))." &mdash; ".$dEv2day2." ".Common::getMonthName(trim($dEv2month))." ".$dateYear;
                              }
                          } else{
                              $period=$dEv1day2." ".Common::getMonthName(trim($dEv1month))." ".$dEv1year." &mdash; ".$dEv2day2." ".Common::getMonthName(trim($dEv2month))." ".$dEv2year;
                          }
                          
                           $urlEvents=$events[$j]['url'];
                           
                           $resultEvents=array("date"=>$period." ".$dr, "title"=>$events[$j]['title'], "text"=>$events[$j]['text'], "position"=>$this->position, "target"=>$target, "images"=>$images, "url"=>$urlEvents);  
                          
                           if (!$this->compareArrayInEvents($this->dateArray, $resultEvents)){
                             array_push($return, $resultEvents);
                           }
                           
                           $this->position++;
                      }
                      
                      
                      
                     if ( intval($events[$j]['repeat_type'])==0 ) {
                         $flag=false; //событие без повторений, выполняется один раз
                     }else{ //далее смотрим тип  и значение повторения   
                                                                  
                         //шаг
                         $step=intval($events[$j]['repeat_value']);
                         $typeRecord=intval($events[$j]['repeat_type']);
                             
                          if ($typeRecord==4){ 
                                $startEv = $startEv->modify('+'.$step.' year');
                                $endEv =  $endEv->modify('+'.$step.' year'); 
                          }else if ($typeRecord==3){ 
                                $startEv = $startEv->modify('+'.$step.' month');
                                $endEv =  $endEv->modify('+'.$step.' month'); 
                          }else if ($typeRecord==2){ 
                                $steps=7*$step; 
                                $startEv = $startEv->modify('+'.$steps.' day');
                                $endEv =  $endEv->modify('+'.$steps.' day'); 
                           }else if($typeRecord==1){ 
                                $startEv = $startEv->modify('+'.$step.' day');
                                $endEv =  $endEv->modify('+'.$step.' day'); 
                           }
                             
                            $data_from_temp =$startEv->format('Y-m-d');
                            $data_to_temp = $endEv->format('Y-m-d');
                         
                            if  ( ($data_from_temp>$dateEventFull) && ($data_to_temp>$dateEventFull) ) {    
                                $flag=false;
                            }else{
                                $flag=true;
                            } 
                     }
                      
                     
                } while ($flag);
            }
            //for event Calendar
           #---------------------------------------------------------------------
         
    // Common::pre($events);  
      return $return;
     //return array_slice($return, 0, 6);
    }
    #---------------------------------------------------------------------------
    public function compareArrayInEvents($array, $temp){
        $result = false;
        for ($i=0; $i<count($array); $i++){
            if ( ($array[$i]['date'] === $temp['date']) && ($array[$i]['title'] == $temp['title']) && ($array[$i]['text'] == $temp['text'])  ) {
                $result=true;                
            }
        }
        
        return $result;
    }
    
    public function getListEvents($datego, $period){
        $datestart = new DateTime($datego);
        $dateStartEvent = $datestart->format("Y-m-d");
        $yearStart = $datestart->format("Y");
        $monthStart = $datestart->format("m");
        $monthIndexStart = $datestart->format("n");
        $dayStart = $datestart->format("d");
        
        $dateend = ($period=="month") ? $datestart->modify('+1 month') : $datestart->modify('+1 year');
        $dateend=$dateend->modify('-1 day');
        
        $dateEndEvent=$dateend->format("Y-m-d");
       
        $return=array();
        
         #----------------для адресации в энциклопедию
         $ArrPerson =  explode(",",$this->settings['EncyclopediaMenuPerson']);
         $ArrOrg =  explode(",",$this->settings['EncyclopediaMenuOrg']);
         $punctPerson=$this->getInfoIdMenu($ArrPerson[0]); //Персоны
         $punctOrg=$this->getInfoIdMenu($ArrOrg[0]); //Организации
         #----------------
        
        
        
        #-----------------------------------------------------------------------
        #1. List Person and Organization 
        $sth = $this->db->query("SELECT id_person, day, month, year, years, definition, is_bc, fio, url FROM ".PREFIX."_person WHERE is_in_encyclopedia='1' and id_person_forwars='0'");
        $result1 = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        $sth2= $this->db->query("SELECT id_sp_organasation, day, month, year, definition, name, url FROM ".PREFIX."_sp_organasation WHERE is_in_encyclopedia='1'");
        $result2 = $sth2->fetchAll(PDO::FETCH_ASSOC);
        
        $result=array_merge($result1, $result2);
        
        for ($j=0; $j<count($result); $j++){
            $day = ( ($result[$j]['day']=="") || ($result[$j]['day']=="0") ) ? "" : intval($result[$j]['day']);
            $month = ( ($result[$j]['month']=="") || ($result[$j]['month']=="0") ) ? "" : $result[$j]['month'];
            $year = ( ($result[$j]['year']=="") || ($result[$j]['year']=="0") ) ? "" : $result[$j]['year'];
            $bc =  ($result[$j]['is_bc']!="") ? $result[$j]['is_bc'] : 0 ;
            $add=false;
            //если год рождения не нулевой, то проверяем - выводить ли в этом году
            if ($year!=""){
              
                $date_out=Common::multiplicityOfFive($year, $month, $day, $yearStart, "", "", $bc);
                
                if ($date_out == $yearStart){
                   
                    if ($result[$j]['name']!="") $result[$j]['fio']=$result[$j]['name']; //для организаций
                    $sobutie = ($result[$j]['name']!="") ? $this->lang['portal.calendar_organization_ubilei'] : $this->lang['portal.calendar_person_ubilei']; 
                    
                    $difference= ($bc == "0")? intval($date_out)-intval($year):  intval($date_out)+intval($year)-1;
                    
                    #-----------------------------------------
                    if (trim($result[$j]['definition'])!="") {
                                $tmpStr=explode(" ", trim($result[$j]['definition']));
                                if ($tmpStr[(count($tmpStr)-1)]!=".") $tmpStr[(count($tmpStr)-1)].="."; 
                                $tmpStr[0]='<span class="capitalize">'.$tmpStr[0].'</span>';
                                
                                $result[$j]['definition']=implode("&nbsp;", $tmpStr);
                         } 
                    unset($tmpStr); 
                    #----------------------------------------      
                    
                    $datestart = new DateTime($datego);
                    $counterDay = 0;
                    $dateTarget = $dateStartEvent;
                    #-----------------------------------------------------------
                    while ($dateTarget < $dateEndEvent){
                        if ($counterDay > 0) {
                            $dateNew=$datestart->modify('+1 day');
                            $monthTarget = $dateNew->format("m");
                            $monthIndex = $dateNew->format("n");
                            $dayTarget = $dateNew->format("d");
                            $dateTarget = $dateNew->format("Y-m-d");  
                         }else{
                              $monthTarget  =   $monthStart;
                              $monthIndex = $monthIndexStart;
                              $dayTarget = $dayStart;
                         }
                         
                         $dayIndex = (substr($dayTarget, 0, 1)=="0") ? substr($dayTarget, 1) : $dayTarget ;
                         $urlEvents= ($result[$j]['id_person']!="") ? "/".URL_ENCYCLOPEDIA.$punctPerson['url_name']."/".$result[$j]['url'] : "/".URL_ENCYCLOPEDIA.$punctOrg['url_name']."/".$result[$j]['url'];
                         $dateYears = ($result[$j]['years']!="") ? " (".$result[$j]['years'].")": "";
                         $add=false;
                         
                         
                         if ( ($day!="") && ($month!="")){
                             
                             
                            if ( ($day==$dayTarget) && ($month==$monthTarget) && ($period=="month") ){
                               // $dateInfo=$day." ".$this->getMonthName(date('n'))." &mdash; ".$difference. " ".$this->lang['portal.year']." со дня рождения"; 
                                 $dateInfo=" &mdash; <b>".$difference. " ".$this->lang['portal.year']."</b> ".$sobutie; 
                                 $add=true; 
                            }    
                         } else if ( ($month!="") ){ 
                            //событие не имеет установленной даты, но имеет месяц
                            if ( ($month==$monthTarget) && ($period=="month")) {
                                $dateInfo=" &mdash; <b>".$difference. " ".$this->lang['portal.year']."</b>".$sobutie; 
                               
                                if ($dayIndex==1){ 
                                    if (!is_array($return[$monthIndex][0])) $return[$monthIndex][0]=array();
                                    $tempArray=array("date"=>$dateInfo, "title"=>$result[$j]['fio'], "text"=>$result[$j]['definition'],  "url"=>$urlEvents, "monthim"=>$this->getMonthName($monthIndex, "im"), "years"=>$dateYears);
                                    array_push($return[$monthIndex][0], $tempArray);
                                }
                                $add=false; 
                            }
                            
                         } else {
                             //событие не имеет установленной даты, и месяца, только год
                             $dateInfo=" &mdash; <b>".$difference. " ".$this->lang['portal.year']."</b>".$sobutie; 
                            
                            if ($period=="year"){
                                if ( ($monthIndex==1) && ($dayIndex==1) ){ 
                                        if (!is_array($return[0][0])) $return[0][0]=array();
                                        $tempArray=array("date"=>$dateInfo, "title"=>$result[$j]['fio'], "text"=>$result[$j]['definition'],  "url"=>$urlEvents, "years"=>$dateYears);
                                        array_push($return[0][0], $tempArray);
                                }
                            }
                            
                            $add=false; 
                         }  
                         
                         
                          if ($add) { 
                             $tempArray=array("date"=>$dateInfo, "title"=>$result[$j]['fio'], "text"=>$result[$j]['definition'],  "url"=>$urlEvents, "monthr"=>$this->getMonthName($monthIndex), "monthim"=>$this->getMonthName($monthIndex, "im"), "years"=>$dateYears);
                             if (!is_array($return[$monthIndex][$dayIndex])) $return[$monthIndex][$dayIndex]=array();
                             array_push($return[$monthIndex][$dayIndex], $tempArray);
                           
                         }     
                         
                        $counterDay++;
                    } 
                    #-----------------------------------------------------------
                             
                 }    
             }
                   
         }//for Person, Organization
         #----------------------------------------------------------------------
         
        # 2. Record Calendarj
        $sth3 = $this->db->query("SELECT * FROM ".PREFIX."_calendar WHERE is_visible='1' ORDER BY date_from desc");
        $events = $sth3->fetchAll(PDO::FETCH_ASSOC);
        
        if ($period=="month") $stP=0; else $stP=count($events);
        
        for ($j=$stP; $j<count($events); $j++){
             $startEv = new DateTime($events[$j]['date_from']);
             $endEv = new DateTime($events[$j]['date_to']);
             #------------------------------------------
             do{ 
                    $dEv1=$startEv->format("Y-m-d"); //дата старта события
                    $dEv1day=$startEv->format("d");  //день начала события
                    $dEv1day2=$startEv->format("j");  //день начала события
                    $dEv1month=$startEv->format("m"); //месяц начала события
                    $dEv1year=$startEv->format("Y"); //год начала события
                    
                    $dEv2=$endEv->format("Y-m-d");  //дата окончания события
                    $dEv2day=$endEv->format("d"); //день окончания события
                    $dEv2day2=$endEv->format("j");  //день начала события
                    $dEv2month=$endEv->format("m"); //месяц окончания события
                    $dEv2year=$endEv->format("Y"); //год окончания события
               
                     if ( (($dEv1>=$dateStartEvent) && ($dEv1<=$dateEndEvent)) || ($dEv2<=$dateEndEvent) && ($dateStartEvent<=$dEv2)  ) {
                     
                         #------------------------------------------------------ 
                         $datestart = new DateTime($datego);
                         $counterDay = 0;
                         $dateTarget = $dateStartEvent;
                     
                         while ($dateTarget < $dateEndEvent){
                            if ($counterDay > 0) {
                                  $dateNew=$datestart->modify('+1 day');
                                  $monthTarget = $dateNew->format("m");
                                  $monthIndex = $dateNew->format("n");
                                  $dayTarget = $dateNew->format("d");
                                  $dateTarget = $dateNew->format("Y-m-d");  
                            }else{
                                  $monthTarget  =   $monthStart;
                                  $monthIndex = $monthIndexStart;
                                  $dayTarget = $dayStart;
                            }
                             
                            $dayIndex = (substr($dayTarget, 0, 1)=="0") ? substr($dayTarget, 1) : $dayTarget ;
                            
                            $datename="";
                            $add=false;
                          
                           
                           if ( ($dEv1<=$dateTarget) && ($dEv2>=$dateTarget) ) {
                                $urlEvents=$events[$j]['url'];
                                $resultEvents=array("date"=>$datename, "title"=>$events[$j]['title'], "text"=>$events[$j]['text'],  "url"=>$urlEvents, "monthr"=>$this->getMonthName($monthIndex), "monthim"=>$this->getMonthName($monthIndex, "im"));  
                                if (!is_array($return[$monthIndex][$dayIndex])) $return[$monthIndex][$dayIndex]=array();
                                array_push($return[$monthIndex][$dayIndex], $resultEvents);
                                
                           }    
                           
                           $counterDay++;
                         }  
                         
                         #-----------------------------------------------  
                           
                 
                     } 
                      
                     if ( intval($events[$j]['repeat_type'])==0 ) {
                         $flag=false; //событие без повторений, выполняется один раз
                     }else{ //далее смотрим тип  и значение повторения   
                         //шаг
                         $step=intval($events[$j]['repeat_value']);
                         $typeRecord=intval($events[$j]['repeat_type']);
                             
                          if ($typeRecord==4){ 
                                $startEv = $startEv->modify('+'.$step.' year');
                                $endEv =  $endEv->modify('+'.$step.' year'); 
                          }else if ($typeRecord==3){ 
                                $startEv = $startEv->modify('+'.$step.' month');
                                $endEv =  $endEv->modify('+'.$step.' month'); 
                          }else if ($typeRecord==2){ 
                                $steps=7*$step; 
                                $startEv = $startEv->modify('+'.$steps.' day');
                                $endEv =  $endEv->modify('+'.$steps.' day'); 
                           }else if($typeRecord==1){ 
                                $startEv = $startEv->modify('+'.$step.' day');
                                $endEv =  $endEv->modify('+'.$step.' day'); 
                           }
                            
                            $data_from_temp =$startEv->format('Y-m-d');
                            $data_to_temp = $endEv->format('Y-m-d');
                         
                            if  ( ($data_from_temp>$dateEndEvent) && ($data_to_temp>$dateEndEvent) ) {  
                                $flag=false;
                            }else{
                                $flag=true;
                            }
                            
                     }
                      
                     
                } while ($flag);
            }
            //for event Calendar
           #---------------------------------------------------------------------
         
        
        ksort($return);
        
        foreach ($return as $key=>$val){
             ksort($return[$key]);
        }
        
        return $return;
    }
    #---------------------------------------------------------------------------
    
    public function getMonthName($n, $type="r"){
      if ($type=="r") { 
        $month_arr = array( 
            1 => $this->lang['month.janr'], 
            2 => $this->lang['month.febr'], 
            3 => $this->lang['month.marr'], 
            4 => $this->lang['month.aplr'], 
            5 => $this->lang['month.mayr'], 
            6 => $this->lang['month.iunr'],  
            7 => $this->lang['month.iulr'], 
            8 => $this->lang['month.avgr'], 
            9 => $this->lang['month.senr'],  
            10 => $this->lang['month.oktr'],  
            11 => $this->lang['month.nobr'], 
            12 => $this->lang['month.decr']);
      }else if ($type=="im"){
           $month_arr = array( 
            1 => $this->lang['month.jan'], 
            2 => $this->lang['month.feb'], 
            3 => $this->lang['month.mar'], 
            4 => $this->lang['month.apl'], 
            5 => $this->lang['month.may'], 
            6 => $this->lang['month.iun'],  
            7 => $this->lang['month.iul'], 
            8 => $this->lang['month.avg'], 
            9 => $this->lang['month.sen'],  
            10 => $this->lang['month.okt'],  
            11 => $this->lang['month.nob'], 
            12 => $this->lang['month.dec']);
          
      }  
        return $month_arr[$n];
    }
    
    public function getSetting(){
        return $this->settings;
    }
    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------     
   private function selectSettings(){
        $sth=$this->db->query("SELECT `name`, `value` FROM ".PREFIX."_main_setting");
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->settings[$row['name']] = $row['value'];
        }
    }
    
    #--------------------------------------------------------------------------
     private function getInfoIdMenu($id){
       $sth = $this->db->prepare('SELECT m.* FROM '.PREFIX.'_menu m 
                                    WHERE 
                                        m.`id_menu` = ? and
                                        m.`is_visible` = 1  and 
                                        m.`is_rubricator` = 0 
                                        ');
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->execute();  
        return $sth->fetch(PDO::FETCH_ASSOC);
     }
    
}

?>
