<?
class Menu {
    private $db;
    public $request;
    public $realRequest;
    public $menuRequest;
    public $info;
    private $rubText;
    private $patterns;
    
    public function __construct($request) {
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->request = $this->realRequest = $request;
        $this->getInfo();
        $this->createPattern();
    }
    
    public function clearUrlRub($url) {
        return preg_replace($this->patterns, '', $url);
    }
    
    public function getRelatedRubr(){
        #echo "select m.url_name from ".PREFIX."_menu m left join ".PREFIX."_rel_menu_rub rm on rm.id_rub = m.id_menu where rm.id_menu = {$this->info['id_menu']}";
        $sth = $this->db->prepare("select m.url_name
                                   from ".PREFIX."_menu m
                                    left join ".PREFIX."_rel_menu_rub rm on rm.id_rub = m.id_menu
                                   where rm.id_menu = ?");
        $sth->bindParam(1, $this->info['id_menu'], PDO::PARAM_INT);
        $sth->execute();    
        while($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $result .= "'{$row['url_name']}',";
        }
        return trim($result, ',');
    }
  
    public function getTopMenu(){        
        #$sth = $this->db->query('SELECT * FROM '.PREFIX.'_menu WHERE is_visible = 1 and id_menu_top = 0 and is_rubricator = 0 order by position');
        $sth = $this->db->query('SELECT * FROM '.PREFIX.'_menu WHERE id_menu in (3,4,16,146,14,17,46,145,143) order by position');
        $i=1;
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)){
            $result[$i] = $row;
            $result[$i]['class'] = (strpos($_SERVER['REQUEST_URI'], $row['url_name'])!= false)?" class='act' ":"";
            ++$i;
        }
        return $result;
    }
    
     public function getMenuMainPage($id, $lim){ 
        $arr=explode(",", $id); 
        $limit=explode(",", $lim); 
        $sth = $this->db->prepare("SELECT id_menu, name, url FROM ".PREFIX."_menu WHERE is_visible = '1' and id_menu_top = '0' and (id_menu = ? or id_menu = ? or id_menu= ? ) order by position");
        $sth->bindParam(1, $arr[0], PDO::PARAM_INT);
        $sth->bindParam(2, $arr[1], PDO::PARAM_INT);
        $sth->bindParam(3, $arr[2], PDO::PARAM_INT);
        $sth->execute();      
        $listMain=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        
        $return=array();
        for($i=0; $i<count($listMain);$i++){
             $limittemp=$limit[$i];  
             
             /*проверяем подключенные статические страницы, если есть - выводим их, - пункты меню*/
             $sth0 = $this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible = '1' and is_menu = '1' and id_menu = ? limit 1");
             $sth0->bindParam(1, $listMain[$i]['id_menu'], PDO::PARAM_INT);
             $sth0->execute(); 
             if ($sth0->rowCount()>0) {
                    $static=$sth0->fetch(PDO::FETCH_ASSOC);
                    $sth1 = $this->db->prepare("SELECT id_static_page as id,  name, url FROM ".PREFIX."_static_page WHERE is_visible = '1' and is_menu = '1' and id_static_page_top = ? order by position limit {$limittemp}");
                    $sth1->bindParam(1, $static['id_static_page'], PDO::PARAM_INT);
                    $dopUrl=trim($listMain[$i]['url'], "/")."/index/";
             } else {
                    $dopUrl="";
                    $sth1 = $this->db->prepare("SELECT id_menu as id, name, url FROM ".PREFIX."_menu WHERE is_visible = '1' and id_menu_top=? and level='2' order by position limit {$limittemp}");
                    $sth1->bindParam(1, $listMain[$i]['id_menu'], PDO::PARAM_INT);
             }
                $sth1->execute();
                $listChild="";
                //$listChild=$sth1->fetchAll(PDO::FETCH_ASSOC);
                while($row = $sth1->fetch(PDO::FETCH_ASSOC)) {
                  $row['url']= $dopUrl.$row['url']; 
                  $listChild[$row['id']]=$row;
                }
                
                switch($i) {
                  case 0: $class=" sputnik"; break;
                  case 1: $class=" bib"; break;
                  case 2: $class=" book"; break;

                 default: $class=" book"; break;
                }
            $arraytemp=array("name"=>$listMain[$i]['name'], "url"=>$listMain[$i]['url'], "class" => $class, "menu"=>$listChild);
            array_push($return, $arraytemp);
        }
        return $return;
    }
    
    public function getRubricators(){       
        $sth = $this->db->prepare('SELECT m.*, CONCAT("'.$this->menuRequest.'/", m.`url`) as url 
                                   FROM pharus_menu m
                                    left join pharus_rel_menu_rub rm on m.id_menu = rm.id_rub
                                   WHERE 
                                          m.is_visible = 1 and
                                          rm.id_menu = ? and 
                                          m.id_menu_top = (select me.id_menu from pharus_menu me 
                                                            left join pharus_rel_menu_rub rmr on me.id_menu = rmr.id_rub
                                                           where me.id_menu_top = 0 and me.is_rubricator = 1 and me.is_visible = 1 and rmr.id_menu = ?)'); 
        $sth->bindParam(1, $this->info['id_menu'], PDO::PARAM_INT);        
        $sth->bindParam(2, $this->info['id_menu'], PDO::PARAM_INT);
        $sth->execute();
        $rubricators = $sth->fetchAll(PDO::FETCH_ASSOC);
        foreach($rubricators as $key=>$rubricator){
            $rubricators[$key]['nodes'] = $this->getNextRubr($rubricator['id_menu']);
        }
        $this->createRubr($rubricators);        
        $this->rubText['out'] = '<ul class="rubrik">'.$this->rubText['out'].'</ul>';
        $this->createRubrInner($rubricators);
        if(count($rubricators)){
            $this->rubText['inner'] = '<div class="tree"><div class="foot"><div class="body"><ul>'.$this->rubText['inner'].'</ul></div></div></div>';
        }
        return $this->rubText;
    }
    
    public function getLeftMenu($url = null){  
        $req = explode("/", $this->request);
        $parser = $req[0].'UrlParser';
        if($req[0]){
            $urlParser = new $parser($this->db);
            return $urlParser->getLeftMenu($this, $url);
        }
    }
    
    public function getBreadCrumbs(){
        $req = explode("/", $this->request);        
        $parser = $req[0].'UrlParser';
        if($req[0]){
            $urlParser = new $parser($this->db);
            return $urlParser->getBreadCrumbs($this);
        }
    }
    
    public function getTextMenu(){
        return str_replace("\\r\\n", "", $this->info['text']);
    }
    
    public function getNameMenu(){
        return $this->info['name'];
    }
    
    public function getMenuRequest(){
        return $this->menuRequest;
    }
    
    public function getTextMenuRubr($params){
        $result = '';
        if(!$params) return '';
        $sth = $this->db->prepare("select `name` from ".PREFIX."_menu where `url_name` = ?");
        foreach($params as $rubr){
            $sth->bindParam(1, $rubr, PDO::PARAM_STR);
            $sth->execute();
            if($row = $sth->fetch(PDO::FETCH_ASSOC)){
                $result .= ', '.array_shift($row);
            }
        }
        
        return trim($result, ",");
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------    
    private function getInfo(){        
        $req = explode("/", $this->request);
        $parser = $req[0].'UrlParser';
        if($req[0]){
            $urlParser = new $parser($this->db);
            $urlParser->parse($this);
        }
    }
    
    private function getNextRubr($idMenuTop=null){        
        $sth = $this->db->prepare('SELECT m.*, CONCAT("'.$this->menuRequest.'/", m.`url`) as url FROM '.PREFIX.'_rel_menu_rub rm
                                        left join '.PREFIX.'_menu m on m.id_menu = rm.id_rub
                                   WHERE m.id_menu_top = ? and rm.id_menu=?  and m.is_visible = 1');
        $sth->bindParam(1, $idMenuTop, PDO::PARAM_INT);
        $sth->bindParam(2, $this->info['id_menu'], PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);        
        return $result;
    }
    
    private function createRubr($rubricators){
        foreach($rubricators as $rub){
            $this->rubText['out'] .= '<li><a href="/'.$this->clearUrlRub($rub['url']).'">'.$rub['name'].'</a>';
            if(is_array($rub['nodes'])){
                $this->rubText['out'] .= '<ul>';                
                $this->createRubr($rub['nodes']);
                $this->rubText['out'] .= '</ul></li>';
            }else{
                $this->rubText['out'] .= '</li>';
            }
        }
    }
    
    private function createRubrInner($rubricators){           
        foreach($rubricators as $rub){            
            $rub['url'] = $this->clearUrlRub($rub['url']);
            $type = (strpos($this->realRequest, $rub['url']) !== false)?'minus':'plus';
            $type1 = (strpos($this->realRequest, $rub['url']) !== false)?'style="display: block;"':'style="display: none;"';
            $act = ($this->realRequest == $rub['url'])?'class="act"':'';
            $plus = (is_array($rub['nodes']))?'<span class="'.$type.'"></span>':'';
            $this->rubText['inner'] .= '<li class="node">'.$plus.'<a href="/'.$rub['url'].'" '.$act.'>'.$rub['name'].'</a>';
            if(is_array($rub['nodes'])){
                $this->rubText['inner'] .= '<ul '.$type1.'>';                                
                $this->createRubrInner($rub['nodes']);
                $this->rubText['inner'] .= '</ul></li>';
            }else{
                $this->rubText['inner'] .= '</li>';
            }
        }
    }        
    
    private function createPattern(){
        $sth = $this->db->query("select url from ".PREFIX."_menu where id_menu_top = 0 and is_rubricator = 1");
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)){
            $this->patterns[] = "#{$row['url']}#";
        }
    }
}

?>