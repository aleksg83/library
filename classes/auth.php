<?php
class auth {
    
    static private $_instance;
    private $db;
    public $userId, $isLogged, $isActive, $lang, $isAdmin, $isRedactor;
    
    private function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->authorize();
    }
    
    static public function instance() {
         if(!isset(self::$_instance)){
             self::$_instance = new self();
         }
         return self::$_instance;
    }
    
    private function authorize() {
        if($this->checkLogin($_POST['login'], $_POST['password'])){
           #$_SESSION['alert_error']=($this->isLogged)?false:'Неверное имя пользователя или пароль';
        }elseif($_POST['login'] || $_POST['password']) {
            $_SESSION['alert_error']='Неверное имя пользователя или пароль';                
        }     
        #var_dump($this);
        if(isset($_POST['login']) && isset($_POST['password']) && $this->isLogged){
            header('Location: '.$_SERVER['REQUEST_URI']);
            die();
        }
    }
    
    private function checkLogin($username, $password) {
        $this->isLogged=false;
        $this->isAdmin=false;
        $this->isRedactor=false;
        if($username || $password){
            $sth=$this->db->prepare("SELECT * FROM `".PREFIX."_users` WHERE `login`=? AND `password`=MD5(?) AND is_active='1' AND is_blocked='0'");
            $sth->bindValue(1, strtolower($username),PDO::PARAM_STR);
            $sth->bindValue(2, $password,PDO::PARAM_STR);
        }elseif($_SESSION['user']['user_id']){
            $sth=$this->db->prepare("SELECT * FROM `".PREFIX."_users` WHERE id_users=?");
            $sth->bindValue(1, $_SESSION['user']['user_id'],PDO::PARAM_INT);
        }else{
            return false;
        }
        $sth->execute();
        $row=$sth->fetch(PDO::FETCH_ASSOC);     
        if($row) {
            $this->userId = $_SESSION['user']['user_id']=$row['id_users'];
            $_SESSION['user']['name']=$row['name'];
            $_SESSION['user']['fam']=$row['fam'];
            $_SESSION['user']['otch']=$row['otch'];
            $_SESSION['user']['login']=$row['login'];
            $this->getUserPrivilege();
	    $this->isLogged=true;
        }
        $this->userId=$_SESSION['user']['user_id'];
        return $this->isLogged;
    }
    
    public function checkLogout() {
        unset($_SESSION['user']);
        unset($_SESSION['alert_error']);
        session_destroy();
        $this->isLogged=false;
        $this->isAdmin=false;
        $this->isRedactor=false;
        return true;
    }
    
    public function checkActive(){
       $sth=$this->db->prepare("SELECT * FROM `".PREFIX."_users` WHERE `login`=? AND is_blocked='0'");
       $sth->bindValue(1, $_SESSION['user']['login'],PDO::PARAM_STR);
       $sth->execute();
       $row=$sth->fetch(PDO::FETCH_ASSOC); 
       $this->isActive=($row['is_active']=="0") ?false:true;
       return $this->isActive;
    }
    
    public function getUserPrivilege(){
        if($this->userId) {
            $sth=$this->db->prepare("select 
                                        rol.code
                                    from ".PREFIX."_roles rol
                                        left join ".PREFIX."_rel_users_roles rur using(id_roles)                                     
                                     where rur.id_users = ?");
            $sth->bindValue("1", $this->userId, PDO::PARAM_STR);						 
            $sth->execute();
            while($row = $sth->fetch(PDO::FETCH_ASSOC)){
                    $_SESSION['role'][] = $row['code'];
            }
            
       }
       
       
          $_SESSION['role'] = array_unique($_SESSION['role']);
        
          foreach ($_SESSION['role'] as $key=>$val){
                if ($val=="ADMIN") $this->isAdmin=true;
                if ($val=="REDACTOR") $this->isRedactor=true;
          }
        
        
       
    }
    
}
?>