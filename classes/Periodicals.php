<?php
class Periodicals {
    private $db;
    private $request;
    
    public function __construct($request) {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->request = $request;
    }
    
    public function getTextContent($params){  
        $tmp=explode("/", $this->request);
        $menu=$this->getInfoTopMenu($this->request);
        #top-page
        $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                                                    where 
                                                                        p.id_menu = ? and
                                                                        p.`is_menu` = 1 and
                                                                        p.`is_visible` = 1");
        $sth->bindParam(1, $menu['id_menu'], PDO::PARAM_STR);
        $sth->execute();
        $toppage = $sth->fetch(PDO::FETCH_ASSOC);
        $this->topId=$toppage['id_static_page'];  
        if (count($params)==0){
              $result=$toppage;
        }else{
          
          if (strpos($params[0], ".html")) $result=$this->getSingleStaticUrl($toppage['id_static_page'], $params[0]); 
          else{
             $this->recursiveGetStaticPage($toppage['id_static_page'], 0, $params);          
             $result=$this->returnChild;
              
          }
        }
        $result['text']=$this->parserPage($result['text']);
        return $result;
    }
     #---------------------------------------------------------------------------
     private function recursiveGetStaticPage($id, $i, $params){
         $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                               WHERE 
                                                    p.`url` = ? and
                                                    p.id_static_page_top = ?");
         $sth->bindParam(1, $params[$i], PDO::PARAM_STR);
         $sth->bindParam(2, $id, PDO::PARAM_INT);
         $sth->execute();
         $row = $sth->fetch(PDO::FETCH_ASSOC); 
         $i++;
         if ($i<count($params)){
            $this->recursiveGetStaticPage($row['id_static_page'], $i, $params);
         }else{
           $this->returnChild=$row;
         } 
    }
    #--------------------------------------------------------------------------
    public function getSingleStaticUrl($idTop, $url){  
        $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                    WHERE 
                                        p.`id_static_page_top` = ? and 
                                        p.`is_visible` = 1 and
                                        p.`url` = ?
                                        ");        
        $sth->bindParam(1, $idTop, PDO::PARAM_INT);
        $sth->bindParam(2, $url, PDO::PARAM_STR);
        $sth->execute();
        $page = $sth->fetch(PDO::FETCH_ASSOC);
        return $page; 
    } 
    #---------------------------------------------------------------------------
     public function getUrlPage($id, $start){
         
       $id=intval($id);
       if ($start) $this->urlPage="";
       
       $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1' and id_static_page=?");
       $sth->bindParam(1, $id, PDO::PARAM_STR);
       $sth->execute(); 
       if ($sth->rowCount()>0){
            $res_temp = $sth->fetch(PDO::FETCH_ASSOC);
            $this->urlPage.=$res_temp['url']."*";
            $id_top=$res_temp['id_static_page_top'];
       
            if ($id_top!=$this->topId) {
                $this->getUrlPage($id_top, false);   
            }else{
                $this->urlPage=trim($this->urlPage, "*");
           
                $arrUrl=  explode("*", $this->urlPage);
                $this->urlPage="";
                for ($i=count($arrUrl)-1; $i>=0;$i--){
                    $dop= ($i==0) ? "" : "/";
                    $this->urlPage.=$arrUrl[$i].$dop;
                }
            }
       }
        return $this->urlPage;
   } 
   #----------------------------------------------------------------------------
   public function parserPage($text){
       $text=stripcslashes($text);
       $text=$this->parserUrlPage($text);
       return $text;
   }
   #----------------------------------------------------------------------------
   public function parserUrlPage($text){
       $tmp=explode("/", $this->request);
       $i=0; 
       while (strpos($text,"[[~",$i)!=false){
           $pos=strpos($text,"[[~",$i);
           $pos2=strpos($text,"~]]",$pos+2);
           if ($pos2){
                $id=substr($text,$pos+3,$pos2-$pos-3);
                $replaceTarget="[[~".$id."~]]";
                $replace = "/".$tmp[0]."/".trim($tmp[1], "/")."/".$this->getUrlPage($id, true);
                $text = str_replace($replaceTarget, $replace, $text);
                $i=$pos2;
           }
       }
      
     return $text;  
   } 
    #---------------------------------------------------------------------------
    private function getInfoTopMenu($request){
       $tmp=explode("/", $this->request);
       $urlTop=$tmp[0];
       $url=$tmp[1];
       $url1=$tmp[1]."/";
       $sth = $this->db->prepare('SELECT m.* FROM '.PREFIX.'_menu m 
                                             LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu`=m.`id_menu_top`
                                    WHERE (m.`url_name` = ? or m.`url_name` = ?) and
                                    m1.`url_name` = ? and
                                    m.`is_visible` = 1  and 
                                    m.`is_rubricator` = 0 
                                    ');
        $sth->bindParam(1, $url, PDO::PARAM_STR);
        $sth->bindParam(2, $url1, PDO::PARAM_STR);
        $sth->bindParam(3, $urlTop, PDO::PARAM_INT);
        $sth->execute();  
        return $sth->fetch(PDO::FETCH_ASSOC);
         
         
     }
}


