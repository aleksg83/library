<?php

class LibUrlParser implements UrlParser  {
    
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function parse(Menu $menu){
        $req = explode("/", $menu->request);
        $sth = $this->db->prepare('SELECT * FROM '.PREFIX.'_menu WHERE `url_name` = ? and `is_visible` = 1');
        $sth->bindParam(1, array_pop($req), PDO::PARAM_STR);
        $sth->execute();
        $menu->info = $sth->fetch(PDO::FETCH_ASSOC);
        if($menu->info['url']){
            $data = explode($menu->info['url'], $menu->request);
            if($menu->info['url'] == $menu->request){
                $menu->menuRequest = $menu->request;
            }else{
                $menu->menuRequest = trim(array_shift($data),"/");
            }    
        }
        if($menu->info['is_rubricator']){
            $sth = $this->db->prepare('SELECT * FROM '.PREFIX.'_menu WHERE `url` = ? and `is_visible` = 1');
            $sth->bindParam(1, $menu->menuRequest, PDO::PARAM_STR);
            $sth->execute();
            $menu->info = $sth->fetch(PDO::FETCH_ASSOC);
        }
        if(!$menu->info){
            $menu->request = implode("/", $req);
            $this->parse($menu);            
        }               
        $menu->menuRequest = (!$menu->menuRequest)?$menu->realRequest:$menu->menuRequest;
    }
    
    public function getLeftMenu(Menu $menu, $url){
        $url2=$menu->menuRequest."/";
        $sth = $this->db->prepare('SELECT m1.* FROM '.PREFIX.'_menu m LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu_top` = m.`id_menu` WHERE (m.`url` = ? or m.`url` = ?) and m.`is_visible` = 1 and m1.`is_visible` = 1 and m1.`is_rubricator` = 0 ORDER BY m1.position');
        $sth->bindParam(1, $menu->menuRequest, PDO::PARAM_STR);
        $sth->bindParam(2, $url2, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        if($sth->rowCount() < 2){
            $sth = $this->db->prepare('SELECT m1.* FROM '.PREFIX.'_menu m LEFT JOIN '.PREFIX.'_menu m1 on m1.`level` = m.`level` WHERE m.`url` = ? and m.`is_visible` = 1 and m1.`is_rubricator` = 0 and m.`id_menu_top` = m1.`id_menu_top` ORDER BY m1.position');
            $sth->bindParam(1, $menu->menuRequest, PDO::PARAM_STR);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);                        
        }
        foreach($result as $k=>$v) if(strpos($menu->menuRequest, $v['url_name'])!==false) $result[$k]['class'] = " class='act' ";
        return $result;
       
    }
    
    public function getBreadCrumbs(Menu $menu){
        $parts = explode("/",$menu->realRequest);  
        foreach($parts as $key=>$part) {
            $url = implode("/", array_slice($parts, 0, count($parts)-$key));   
            $sth = $this->db->prepare("SELECT `name`, `url_name` FROM ".PREFIX."_menu WHERE `url_name` = '{$url}' and `is_visible` = 1");         
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            if(!$row){
                $sth = $this->db->prepare('SELECT `name`, `url_name` FROM '.PREFIX.'_menu WHERE `url_name` = ? and `is_visible` = 1');
                $sth->bindParam(1, $parts[count($parts)-$key-1], PDO::PARAM_STR);
                $sth->execute();
                $row = $sth->fetch(PDO::FETCH_ASSOC);
                $result[$key]['url'] = $parts[0].'/'.$row['url_name'];
            }else{
                $result[$key]['url'] = $row['url_name'];
            }
            $result[$key]['name'] = $row['name'];            
        }
        
        #----------------------------------------
        if ($menu->menuRequest=="lib/periodicals"){
            $tmp=explode("/", substr($_SERVER['REQUEST_URI'], 1) );
            $tmp = array_diff($tmp, array( '' )); 
            $menu=$this->getInfoTopMenu($tmp);
            #top-page
            $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                                                    where 
                                                                        p.id_menu = ? and
                                                                        p.`is_menu` = 1 and
                                                                        p.`is_visible` = 1");
            $sth->bindParam(1, $menu['id_menu'], PDO::PARAM_STR);
            $sth->execute();
            $toppage = $sth->fetch(PDO::FETCH_ASSOC);
            $this->topId=$toppage['id_static_page']; 
            if (count($tmp)>2){
              $this->returnChild=array();
              $this->dopChild=$tmp[0]."/".$tmp[1]."/";
              $this->recursiveGetStaticPageBredcrumb($this->topId, 2, $tmp);
              unset($result[0]);
              foreach ($this->returnChild as $keys=>$values){
                    array_unshift($result, $values);
              }    
            }
        }
        #----------------------------------------------
                
        krsort($result);
        return $result;
    }
    
    
    
    #---------------------------------------------------------------------------
    private function getInfoTopMenu($tmp){
       $urlTop=$tmp[0];
       $url=$tmp[1];
       $url1=$tmp[1]."/";
       $sth = $this->db->prepare('SELECT m.* FROM '.PREFIX.'_menu m 
                                             LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu`=m.`id_menu_top`
                                    WHERE (m.`url_name` = ? or m.`url_name` = ?) and
                                    m1.`url_name` = ? and
                                    m.`is_visible` = 1  and 
                                    m.`is_rubricator` = 0 
                                    ');
        $sth->bindParam(1, $url, PDO::PARAM_STR);
        $sth->bindParam(2, $url1, PDO::PARAM_STR);
        $sth->bindParam(3, $urlTop, PDO::PARAM_INT);
        $sth->execute();  
        return $sth->fetch(PDO::FETCH_ASSOC);
     }
     
     #---------------------------------------------------------------------------
     private function recursiveGetStaticPageBredcrumb($id, $i, $params){
         $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                               WHERE 
                                                    p.`url` = ? and
                                                    p.id_static_page_top = ?");
         $sth->bindParam(1, $params[$i], PDO::PARAM_STR);
         $sth->bindParam(2, $id, PDO::PARAM_INT);
         $sth->execute();
         $row = $sth->fetch(PDO::FETCH_ASSOC); 
         array_push($this->returnChild, array("name" => $row['name'], "url" => $this->dopChild.$row['url'])); 
         $this->dopChild.=$row['url']."/";
         $i++; 
        
         if ($i<count($params)-1){
            $this->recursiveGetStaticPage($row['id_static_page'], $i, $params);
         }else if ($i==count($params)-1){
             $sth = $this->db->prepare("SELECT p.* FROM ".PREFIX."_static_page p 
                                               WHERE 
                                                    p.`url` = ? and
                                                    p.id_static_page_top = ?");
            $sth->bindParam(1, $params[$i], PDO::PARAM_STR);
            $sth->bindParam(2, $row['id_static_page'], PDO::PARAM_INT);
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC); 
            array_push($this->returnChild, array("name" => $row['name'], "url" => $this->dopChild.$row['url'])); 
         }
    }
}

?>
