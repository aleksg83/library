<?php

class ReaderUrlParser implements UrlParser{
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function parse(Menu $menu){
        $req = explode("/", $menu->request);
        $sth = $this->db->prepare('SELECT * FROM '.PREFIX.'_menu WHERE `url_name` = ?');       
        #echo 'SELECT * FROM '.PREFIX.'_menu WHERE `url_name` = "reader_geometry"';
        $sth->bindParam(1, array_pop($req), PDO::PARAM_STR);
        $sth->execute();
        $menu->info = $sth->fetch(PDO::FETCH_ASSOC);        
        if($menu->info['url']){
            $data = explode($menu->info['url_name'], $menu->request);
            if($menu->info['url'] == $menu->request){
                $menu->menuRequest = $menu->request;
            }else{
                $menu->menuRequest = trim(array_shift($data),"/");
            }    
        }
        if($menu->info['is_rubricator']){
            $sth = $this->db->prepare('SELECT * FROM '.PREFIX.'_menu WHERE `url` = ?');
            $sth->bindParam(1, $menu->menuRequest, PDO::PARAM_STR);
            $sth->execute();
            $menu->info = $sth->fetch(PDO::FETCH_ASSOC);
        }        
        if(!$menu->info){
            $menu->request = implode("/", $req);
            $this->parse($menu);            
        }               
        $menu->menuRequest = (!$menu->menuRequest)?$menu->realRequest:$menu->menuRequest;
    }
    
    public function getLeftMenu(Menu $menu, $url){
        if($menu->menuRequest == 'reader') $menu->menuRequest = 'reader/';
        $sth = $this->db->prepare('SELECT m1.* FROM '.PREFIX.'_menu m LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu_top` = m.`id_menu` WHERE m.`url` = ? and m.`is_visible` = 1 and m1.`is_visible` = 1 and m1.`is_rubricator` = 0 ORDER BY m1.position');
        $sth->bindValue(1, $menu->menuRequest, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);        
        if($sth->rowCount() < 2){
            $sth = $this->db->prepare('SELECT m1.* FROM '.PREFIX.'_menu m LEFT JOIN '.PREFIX.'_menu m1 on m1.`level` = m.`level` WHERE m.`url_name` = ? and m.`is_visible` = 1 and m1.`is_visible` = 1 and m1.`is_rubricator` = 0 and m.`id_menu_top` = m1.`id_menu_top` ORDER BY m1.position');
            $sth->bindParam(1, array_pop(explode("/", $menu->menuRequest)), PDO::PARAM_STR);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);                        
        }
        foreach($result as $k=>$v) if(strpos($menu->menuRequest, $v['url_name'])!==false) $result[$k]['class'] = " class='act' ";
        return $result;
    }

    public function getBreadCrumbs(Menu $menu) {
        $parts = explode("/",  str_replace("/index", "", $menu->realRequest));  
        foreach($parts as $key=>$part) {
            $url = implode("/", array_slice($parts, 0, count($parts)-$key));   
            $sth = $this->db->prepare("SELECT `name`, `url` FROM ".PREFIX."_menu WHERE `url` = '{$url}'");         
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            if(!$row){
                $sth = $this->db->prepare('SELECT `is_rubricator`, `name`, `url` FROM '.PREFIX.'_menu WHERE `url_name` = ?');
                $sth->bindParam(1, $parts[count($parts)-$key-1], PDO::PARAM_STR);
                $sth->execute();
                $row = $sth->fetch(PDO::FETCH_ASSOC);
                if($row['is_rubricator']){
                    $result[$key]['url'] = $menu->menuRequest.'/'.$row['url'];
                }else{
                    $result[$key]['url'] = $row['url'];
                }
            }else{
                $result[$key]['url'] = $row['url'];
            }
            $result[$key]['name'] = $row['name'];            
        }
        krsort($result);
        return $result;
    }
}

?>
