<?php

class SputnikUrlParser implements UrlParser {
    private $db;
    private $urlSection;
    private $tmp;        
    
    public function __construct($db) {
        $this->db = $db;
   }
    
    public function parse(Menu $menu){
        $tmp = explode("/", $menu->request);
        $menu->menuRequest = $tmp[0];
        if(count($tmp) < 2){
            $menu->request = implode("/", array($tmp[0], 'index'));
        }
    }
    
    
    public function getLeftMenu(Menu $menu, $url){
       $tmp = explode("/", $menu->request);
       $urlSection=$tmp[0]."/".$tmp[1]."/"; 
       $urlDop=$tmp[0]."/";
       $page=$this->getInfoTopPage($tmp);
       $id_top=$page['id_static_page'];
       $sth0 = $this->db->prepare('SELECT s.* FROM '.PREFIX.'_static_page s
                                             WHERE 
                                                    s.`is_visible` = 1 and 
                                                    s.`is_menu` = 1 and
                                                    s.`id_static_page_top` = ? 
                                                    ORDER BY s.position');
                                                            
      
       $sth0->bindParam(1, $id_top, PDO::PARAM_INT);
       $sth0->execute();
       $result="";
       while($row = $sth0->fetch(PDO::FETCH_ASSOC)){
            $url_normal = $row['url'];
            $row['url'] = $urlSection.$row['url'];
            $row['class'] = ($url_normal == $tmp[2])?"class='act'":"";
            $row['style'] = ($url_normal == $tmp[2])?" block ":" none ";   
               #----------------------------------------------------------------
                $sth1 = $this->db->prepare('SELECT s.* FROM '.PREFIX.'_static_page s
                                             WHERE 
                                                    s.`is_visible` = 1 and 
                                                    s.`is_menu` = 1 and
                                                    s.`id_static_page_top` = ? 
                                                    ORDER BY s.position');
                                                            
      
                $sth1->bindParam(1, $row['id_static_page'], PDO::PARAM_INT);
                $sth1->execute();
                $result2="";
                 while($row2 = $sth1->fetch(PDO::FETCH_ASSOC)){
                        $url_normal2 = $row2['url'];
                        $row2['url'] = $urlSection.trim($url_normal)."/".$row2['url'];
                        $row2['class'] = ($url_normal2 == $tmp[3])?"class='act'":"";
                        $result2[$row2['id_static_page']] = $row2;
                 }       
               #----------------------------------------------------------------
             
            $result[$row['id_static_page']] = $row;
            if ($result2!="") $result[$row['id_static_page']]['child']=$result2;
            
       }
       $result = array_diff($result, array(''));
        return $result; 
    }

    public function getBreadCrumbs(Menu $menu) {
        $parts = explode("/", $menu->request);
        $page=$this->getInfoTopPage($parts );
       
        $dopSection=$parts[0]."/".$parts[1]."/";
        //$dopSection="";
        $return[$page['id_static_page']] = array("level"=> $page['level'], "url" => $parts[0]."/".$parts[1], "name"=>$page['name'], "id_static_page_top"=>"");
        $urlParent="";
        foreach($parts as $key=>$part){
            $sth = $this->db->prepare("SELECT p.`level`, p.`name`, p.`url`, p.`id_static_page_top` FROM ".PREFIX."_static_page p WHERE p.`url` = '{$part}'");         
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);                    
            if($row){
                 $tempUrl=$row['url'];
                 $row['url'] = $dopSection.$urlParent.$row['url'];
                 $return[$key] = $row;
                 $urlParent.=$tempUrl."/";
            }
        }
        if ($return) $return2 = array_reverse($return);
        if ($return2) krsort($return2);
        return $return2;
    }
    
    
    #---------------------------------------------------------------------------
     private function getInfoTopPage($tmp){
       $urlSection=$tmp[0]."/".$tmp[1]."/"; 
       $urlDop=$tmp[0]."/";
       $sth = $this->db->prepare('SELECT s.* FROM '.PREFIX.'_menu m 
                                             LEFT JOIN '.PREFIX.'_static_page s on s.`id_menu` = m.`id_menu`
                                             WHERE 
                                                       (m.`url` = ? or m.`url` = ? or m.`url` = ? )and 
                                                        m.`is_visible` = 1 and 
                                                        m.`is_rubricator` = 0 and
                                                        s.`is_visible` = 1 and 
                                                        s.`is_menu` = 1');
       $sth->bindParam(1, $tmp[0], PDO::PARAM_STR);
       $sth->bindParam(2, $urlSection, PDO::PARAM_STR);
       $sth->bindParam(3, $urlDop, PDO::PARAM_STR);
       $sth->execute();
       $result=$sth->fetch(PDO::FETCH_ASSOC);
       
       return $result;
     }  
    
    
}

?>
