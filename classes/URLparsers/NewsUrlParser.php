<?php

class NewsUrlParser implements UrlParser {
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function parse(Menu $menu){
        $tmp = explode("/", $menu->request);
        if(count($tmp) < 2){
            $menu->request = implode("/", array($tmp[0], 'text'));
        }
    }
    
    public function getLeftMenu(Menu $menu, $urlTheme){
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_sp_theme_news WHERE is_visible = 1 ORDER by position");
        while($row = $sth->fetch(PDO::FETCH_ASSOC)){
            $result[$row['id_sp_theme_news']] = $row;
            $result[$row['id_sp_theme_news']]['class'] = ($row['url'] == $urlTheme)?"class='act'":"";
        }
        return $result;
    }
    
    public function getBreadCrumbs(Menu $menu){                
        $URLstring = (explode("/", $menu->realRequest));
        $url1 = array_pop($URLstring);        
        $i=0;
        if(strstr($url1, ".html") !== false){
            $sth = $this->db->query("SELECT `zagolovok` as name FROM ".PREFIX."_news WHERE `url` = '{$url1}'");         
            $res[$i] = $sth->fetch(PDO::FETCH_ASSOC); $i++;
        }
        $url = array_pop($URLstring);
        $sth = $this->db->query("SELECT `name`, CONCAT('".URL_NEWS."', '', `url`) as url FROM ".PREFIX."_sp_theme_news WHERE `url` = '{$url}' or `url` = '{$url1}'");         
        $sth->execute();        
        if($row = $sth->fetch(PDO::FETCH_ASSOC)){
            $res[$i] = $row; $i++;
        }else{
            $sth = $this->db->query("SELECT th.`name`, CONCAT('".URL_NEWS."', '', th.`url`) as url
                                    FROM pharus_news n
                                            left join `pharus_rel_news_sp_theme_news` thn using(id_news)
                                        left join `pharus_sp_theme_news` th using(id_sp_theme_news)
                                    WHERE n.`url` = '{$url1}'
                                    limit 0,1");            
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            if($row['name']){
                $res[$i] = $row; $i++;
            }
        }        
        $res[$i] = array("name"=>"Новости", "url"=>"news");
        if($res) krsort($res);
        return $res;
    }
}

?>
