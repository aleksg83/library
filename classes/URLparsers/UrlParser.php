<?php

interface UrlParser {

    public function parse(Menu $menu);
    public function getBreadCrumbs(Menu $menu);
    public function getLeftMenu(Menu $menu, $url);
}

?>
