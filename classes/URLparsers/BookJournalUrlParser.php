<?php

class BookJournalUrlParser implements UrlParser {
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function parse(Menu $menu){
        $tmp = explode("/", $menu->request);
        $menu->menuRequest = $tmp[0];
        if(count($tmp) < 2){
            $menu->request = implode("/", array($tmp[0], 'index'));
        }
        $sth = $this->db->prepare('SELECT * FROM '.PREFIX.'_menu WHERE `url_name` = ?');
        $sth->bindParam(1, $tmp[0], PDO::PARAM_STR);
        $sth->execute();
        $menu->info = $sth->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getLeftMenu(Menu $menu, $url){        
        $sth = $this->db->prepare('SELECT m1.* FROM '.PREFIX.'_menu m LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu_top` = m.`id_menu` WHERE m.`url` = ? and m.`is_visible` = 1 and m1.`is_visible` = 1 and m1.`is_rubricator` = 0 ORDER BY m1.position');
        $sth->bindValue(1, $menu->menuRequest.'/', PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);        
        if($menu->request == 'bookJournal/index') $result[0]['class'] = " class='act' ";
        foreach($result as $k=>$v){                         
            if(strpos($menu->request, $v['url_name'])!==false) $result[$k]['class'] = " class='act' ";
            $result[$k]['nastedRubr'] = $this->getRubMenu($menu, $v['id_menu'], $v['url_name']);
        }
        #Common::pre($result);
        return $result;
    }

    public function getBreadCrumbs(Menu $menu) {
        $parts = explode("/",  str_replace("/index", "", $menu->realRequest)); 
        
        foreach($parts as $key=>$part) {
            $url = implode("/", array_slice($parts, 0, count($parts)-$key)); 
            $url2=$url."/";
            $sth = $this->db->prepare("SELECT `name`, `url` FROM ".PREFIX."_menu WHERE `url` = '{$url}' or `url` = '{$url2}'");         
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);
            if(!$row){
                $sth = $this->db->prepare('SELECT `is_rubricator`, `name`, `url` FROM '.PREFIX.'_menu WHERE `url_name` = ?');
                $sth->bindParam(1, $parts[count($parts)-$key-1], PDO::PARAM_STR);
                $sth->execute();
                $row = $sth->fetch(PDO::FETCH_ASSOC);
                if($row['is_rubricator']){
                    $result[$key]['url'] = $menu->menuRequest.'/'.$row['url'];
                }else{
                    $result[$key]['url'] = $row['url'];
                }
            }else{
                $result[$key]['url'] = $row['url'];
            }
            $result[$key]['name'] = $row['name'];            
        }
        krsort($result);
        return $result;
    }
    
#------------------------------------------------------------------------------------------------------------------------------------------------------
    private function getRubMenu(Menu $menu, $idMenu, $urlMenu){
        $sth = $this->db->prepare('SELECT m.*, CONCAT("'.$menu->menuRequest.'/", "index/", "'.$urlMenu.'/", m.`url`) as url FROM '.PREFIX.'_rel_menu_rub rm
                                        left join '.PREFIX.'_menu m on m.id_menu = rm.id_rub
                                   WHERE rm.id_menu = ? and m.is_visible = 1 and m.id_menu_top = (select me.id_menu from pharus_menu me 
                                                            left join pharus_rel_menu_rub rmr on me.id_menu = rmr.id_rub
                                                           where me.id_menu_top = 0 and me.is_rubricator = 1 and rmr.id_menu = ?)');  
        $sth->bindParam(1, $idMenu, PDO::PARAM_INT);        
        $sth->bindParam(2, $idMenu, PDO::PARAM_INT); 
        $sth->execute();       
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $rubricators[$row['id_menu']] = $row;
            if(strpos($menu->request, $row['url_name'])!==false) $rubricators[$row['id_menu']]['class'] = " class='act' ";
            $rubricators[$row['id_menu']]['url'] = $menu->clearUrlRub($row['url']);
        }
        return ($rubricators)?$rubricators:array();
    }
}

?>