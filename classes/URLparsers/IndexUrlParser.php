<?php

class IndexUrlParser implements UrlParser {
    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function parse(Menu $menu){
        $tmp = explode("/", $menu->request);
        if(count($tmp) < 2){
            $menu->request = implode("/", array($tmp[0], 'index'));
        }
    }
    
    public function getLeftMenu(Menu $menu, $url){
        $tmp = explode("/", $menu->request);
        $sth = $this->db->query("SELECT id_static_page FROM ".PREFIX."_static_page WHERE is_visible ='1' and level='1' and url ='{$url}' ");
        $parent=$sth->fetch(PDO::FETCH_ASSOC);
        $id=$parent['id_static_page'];
        $sth2 = $this->db->query("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1' and is_menu='1' and id_static_page_top='{$id}' ");
        
        while($row = $sth2->fetch(PDO::FETCH_ASSOC)){
            $url_normal = $row['url'];
            $row['url'] = URL_STATIC.$url."/".$row['url'];
            $row['style'] = ($url_normal == $tmp[3])?" block ":" none "; 
            $result[$row['id_static_page']] = $row;
            $result[$row['id_static_page']]['class'] = ($url_normal == $tmp[3])?"class='act'":"";
            #----------------------------------------------------------------
                $sth1 = $this->db->prepare('SELECT s.* FROM '.PREFIX.'_static_page s
                                             WHERE 
                                                    s.`is_visible` = 1 and 
                                                    s.`is_menu` = 1 and
                                                    s.`id_static_page_top` = ? 
                                                    ORDER BY s.position');
                                                            
      
                $sth1->bindParam(1, $row['id_static_page'], PDO::PARAM_INT);
                $sth1->execute();
                $result2="";
                 $urlSection=$tmp[0]."/".$tmp[1]."/".$tmp[2]."/"; 
                 while($row2 = $sth1->fetch(PDO::FETCH_ASSOC)){
                        $url_normal2 = $row2['url'];
                        $row2['url'] = $urlSection.trim($url_normal)."/".$row2['url'];
                        $row2['class'] = ($url_normal2 == $tmp[3])?"class='act'":"";
                        $result2[$row2['id_static_page']] = $row2;
                 }       
               #----------------------------------------------------------------
               if ($result2!="") $result[$row['id_static_page']]['child']=$result2;
              
        }
        return $result;
    }
    
    public function getBreadCrumbs(Menu $menu){                
        $URLstring = (explode("/", $menu->realRequest));
        $res=array();
        $urlParent="";
        for ($i=2; $i<count($URLstring); $i++){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE is_visible='1' and is_menu='1' and url=?");
            $sth->bindParam(1, $URLstring[$i], PDO::PARAM_STR);
            $sth->execute(); 
            $res_temp = $sth->fetch(PDO::FETCH_ASSOC); 
                    
            array_push($res, array("name" => $res_temp['name'], "url" => URL_STATIC.$urlParent.$res_temp['url']) );   
            $urlParent=$res_temp['url']."/";
        }
        if($res) $res2 = array_reverse($res);
        if($res2) krsort($res2);
        return $res2;
    }
}

?>
