<?php

class EncyclopediaUrlParser implements UrlParser {
    private $db;
    private $bred;
    private $settings = array();
    private $month_arr = array();
    
    public function __construct($db) {
        $this->db = $db;
        $this->selectSettings();
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        $this->month_arr = array( 
                            1 => $this->lang['month.jan'], 
                            2 => $this->lang['month.feb'], 
                            3 => $this->lang['month.mar'], 
                            4 => $this->lang['month.apl'], 
                            5 => $this->lang['month.may'], 
                            6 => $this->lang['month.iun'],  
                            7 => $this->lang['month.iul'], 
                            8 => $this->lang['month.avg'], 
                            9 => $this->lang['month.sen'],  
                            10 => $this->lang['month.okt'],  
                            11 => $this->lang['month.nob'], 
                            12 => $this->lang['month.dec']);
    }
    
    public function parse(Menu $menu){
        $tmp = explode("/", $menu->request);
        $menu->menuRequest = $tmp[0];
        if(count($tmp) < 2){
            $menu->request = implode("/", array($tmp[0], 'index'));
        }
        $sth = $this->db->prepare('SELECT * FROM '.PREFIX.'_menu WHERE `url_name` = ?');
        $sth->bindParam(1, $tmp[0], PDO::PARAM_STR);
        $sth->execute();
        $menu->info = $sth->fetch(PDO::FETCH_ASSOC);
    }
    #---------------------------------------------------------------------------
    public function getLeftMenu(Menu $menu, $url){
       $tmp = explode("/", $menu->request); 
       $tmp[1]= ($tmp[1]=="")? "index" : $tmp[1];
       $dopUrl = $tmp[0]."/".$tmp[1]."/";
       $ArrType1 =  explode(",",$this->settings['EncyclopediaMenuType1']);
       $ArrType4 =  explode(",",$this->settings['EncyclopediaMenuType4']);
       $ArrCalendarj =  explode(",",$this->settings['EncyclopediaMenuCalendarj']);
       $target=$this->getInfoTargetMenu($menu->menuRequest);
       $top=$this->getInfoTopMenu($tmp[0], 0);//верхнее меню
       if ($tmp[2]!="") $punct = $this->getInfoTopMenu($tmp[2], $top['id_menu']);
       
       if ( !in_array($punct['id_menu'], $ArrCalendarj)){
         #------------------------------------------------------------------------
         #если не календарь
          foreach ($target as $key=>$val)   {
              $result[$target[$key]['id_menu']] = $target[$key];
              $result[$target[$key]['id_menu']]['url'] = $dopUrl.$target[$key]['url_name'];
              $result[$target[$key]['id_menu']]['class'] = ($target[$key]['url_name'] == $tmp[2])?"class='act'":"";
              $result[$target[$key]['id_menu']]['style'] = ($target[$key]['url_name'] == $tmp[2])?" block ":" none ";   
             
              $result2="";
              //тип 1, выводим 2 уровень страниц
              #---------------------------------
              if ( in_array($target[$key]['id_menu'], $ArrType1) || in_array($target[$key]['id_menu'], $ArrType4)){
                    $sth0 = $this->db->prepare('SELECT s1.*  FROM '.PREFIX.'_static_page s 
                                    LEFT JOIN '.PREFIX.'_static_page s1 on s1.`id_static_page_top` = s.`id_static_page` 
                                    WHERE 
                                      s.`id_menu` = ? and 
                                      s.`is_visible` = 1 and
                                      s.`is_menu` = 1 and
                                      s1.`is_visible` = 1 and
                                      s1.`is_menu` = 1 
                                      ORDER BY s1.position');
                 $sth0->bindParam(1, $target[$key]['id_menu'], PDO::PARAM_STR);
                 $sth0->execute(); 
                 while($row2 = $sth0->fetch(PDO::FETCH_ASSOC)){
                        $result2[$row2['id_static_page']] = $row2;
                        $result2[$row2['id_static_page']]['url'] = $dopUrl.$target[$key]['url_name']."/".$row2['url'];
                        $result2[$row2['id_static_page']]['class'] = ($row2['url'] == $tmp[3])?"class='act'":"";
                       
                 }      
               
             #---------------------------------        
             }else{
                 $sth0 = $this->db->prepare('SELECT m1.*  FROM '.PREFIX.'_menu m 
                                    LEFT JOIN '.PREFIX.'_rel_menu_rub rm on rm.`id_menu` = m.`id_menu` 
                                    LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu` = rm.`id_rub`     
                                    WHERE 
                                      m.`id_menu` = ? and 
                                      m.`is_visible` = 1 and
                                      m1.`is_visible` = 1 and
                                      m1.`is_rubricator` = 1 
                                      ORDER BY m1.position');
                 $sth0->bindParam(1, $target[$key]['id_menu'], PDO::PARAM_STR);
                 $sth0->execute(); 
                 while($row2 = $sth0->fetch(PDO::FETCH_ASSOC)){
                        $result2[$row2['id_menu']] = $row2;
                        $result2[$row2['id_menu']]['url'] = $dopUrl.$target[$key]['url_name']."/".$row2['url_name'];
                        $result2[$row2['id_menu']]['class'] = ($row2['url_name'] == $tmp[3])?"class='act'":"";
                 }     
                 
             }
             #---------------------------------
            
             if ($result2!="") {
                $result2 = array_diff($result2, array(''));
                $result[$target[$key]['id_menu']]['child']=$result2;
             }    
            
        }  //если не календарь, end foreach
        #----------------------------------------------------------------------
        }else{ //если активный пункт календарь
            
            $year=date("Y");
            $yearActive = ($tmp[3]!="")? $tmp[3]:  $year; 
            $urlCalendar = $dopUrl.$punct['url_name']."/";
            $result=array();
           
            for ($i=$year; $i<$year+5; $i++){
                
                if ($i==$yearActive){
                    $dopSection='<div class="monthDiv">';
                    
                    for ($m=1;$m<13;$m++){
                        if ($m==1 || $m==7) $dopSection.='<ul class="monthcalendarj">';
                          
                         $dopClass = ( ($i==$yearActive) && ($m==$tmp[4])) ? ' class="act"' : '' ;   
                         $dopSection.='<li><a href="/'.$urlCalendar.$i.'/'.$m.'/" '.$dopClass.'>'.$this->month_arr[$m].'</a></li>';
                        if ($m==6 || $m==12) $dopSection.='</ul>';
                    }
                   
                     $dopSection.='</div>';
                     
                }else{
                   $dopSection=""; 
                }
                
                $class = ($i==$yearActive) ? ' class="act"' : '' ;  
                
                array_push($result, array("name" => $i, "url" => $urlCalendar.$i, "class"=>$class, "month" => $dopSection ));
            }
            
            
        }
        //print_r($result);
        $result = array_diff($result, array(''));
        return $result;
    }
    #---------------------------------------------------------------------------
    public function getBreadCrumbs(Menu $menu) {
        $parts = explode("/", $menu->request);
        
        #calendar
        if ( ($parts[2]=="almanac") && ($parts[3]=="") ){
            $parts[3]=date("Y");
        }
        
        #------------------------------------
        $top=$this->getInfoTopMenu($parts[0], 0);
        $this->bred = array(100 => array("name"=>$top['name'], "url" => $top['url_name'] ));
        if ($parts[2]!="") {
                $punct = $this->getInfoTopMenu($parts[2], $top['id_menu']);
                if ($parts[3]!="") $this->bred['99']=array("name"=>$punct['name'], "url" => $top['url_name']."/index/".$punct['url_name'] );
                else $this->bred['0']=array("name"=>$punct['name'], "url" => $top['url_name']."/index/".$punct['url_name'] );
        
                if ($parts[3]!="") {
                     $parts0=$parts;
                     $parts = array_reverse($parts);
                     $dopParent=$top['url_name']."/index/".$punct['url_name']."/";
        
                     $ArrType1 =  explode(",",$this->settings['EncyclopediaMenuType1']);
                     $ArrType2 =  explode(",",$this->settings['EncyclopediaMenuType2']);
                     $ArrType3 =  explode(",",$this->settings['EncyclopediaMenuType3']);
                     $ArrType4 =  explode(",",$this->settings['EncyclopediaMenuType4']);
                     $ArrCalendarj =  explode(",",$this->settings['EncyclopediaMenuCalendarj']);
                     $ArrPerson =  explode(",",$this->settings['EncyclopediaMenuPerson']);
                     $ArrOrg =  explode(",",$this->settings['EncyclopediaMenuOrg']);
                     //тип 1, выводим далее статику
                     #-----------------------------
                     if ( in_array($punct['id_menu'], $ArrType1) || in_array($punct['id_menu'], $ArrType2) || in_array($punct['id_menu'], $ArrType3) || in_array($punct['id_menu'], $ArrType4) ){
                       
                        // print_r($parts0);
                         
                         $parent="";
                         $arrayPage=array();
                         for ($l=3;$l<count($parts0);$l++){
                             $part=$parts0[$l];
                             $sth = $this->db->prepare("SELECT p.`level`, p.`name`, p.`url`, p.`id_static_page_top`,  p.`id_static_page` FROM ".PREFIX."_static_page p WHERE p.`url` = '{$part}'");         
                             $sth->execute();
                             $row = $sth->fetch(PDO::FETCH_ASSOC);  
                             if($row){
                                        $urlTemp = $row['url'];
                                        $row['url'] = $top['url_name']."/index/".$punct['url_name']."/".$parent."{$row['url']}";
                                        array_push($arrayPage, array("name" => $row['name'], "url"=> $row['url']));
                                        $parent.=$urlTemp."/";
                             }
                         }
                         
                         $arrayPage=array_reverse($arrayPage);
                         
                         foreach($arrayPage as $key => $val){
                             $this->bred[$key] = $val;
                         }
                         
                         if ( ($parts0[3]=="letter") ){
                             $this->bred[0] = array("name"=>urldecode($parts0[4]));
                         }
                         
                         if ( ($parts0[4]=="letter") ){
                             $this->bred[0] = array("name"=>urldecode($parts0[5]));
                         }
                         //print_r($parts0);
                         
                     #----------------------------- #тип 1 и тип 2, связанный со статическими страницами   
                    
                    }else if (in_array($punct['id_menu'], $ArrCalendarj) ){ //календарь 
                       $kl=($parts0[4]=="")? 0 : 1 ; 
                       $arrayPage[$kl] = array("name" => $parts0[3], "url"=> $dopParent.$parts0[3]);
                       if ($parts0[4]!=""){
                           $arrayPage[0]=array("name" => $this->month_arr[$parts0[4]], "url"=> $dopParent.$parts0[3]."/".$parts0[4]);
                           $arrayPage=array_reverse($arrayPage);
                       } 
                         
                       foreach($arrayPage as $key => $val){
                             $this->bred[$key] = $val;
                       }
                    //календарь end
                    
                    #--- person and organization   
                    }else if ( in_array($punct['id_menu'], $ArrPerson) || in_array($punct['id_menu'], $ArrOrg)){ 
                    
                       $type=(in_array($punct['id_menu'], $ArrPerson)) ? "person" : "org" ;
                       
                       if ($parts0[3]=="letter"){
                           $this->bred[0] = array("name"=>urldecode($parts0[4]));
                       }else if (strrpos($parts0[3], ".html")){
                           if ($type=="person") $sth = $this->db->prepare("SELECT p.`fio`, p.`url` FROM ".PREFIX."_person p WHERE p.`url` = ? and is_in_encyclopedia='1'");         
                           if ($type=="org") $sth = $this->db->prepare("SELECT p.`name`, p.`url` FROM ".PREFIX."_sp_organasation p WHERE p.`url` = ? and is_in_encyclopedia='1'");         
                           $sth->bindParam(1, $parts0[3], PDO::PARAM_STR);
                           $sth->execute();
                           $row = $sth->fetch(PDO::FETCH_ASSOC);  
                           $name = ($type=="person") ? $row['fio'] : $row['name'] ;
                           $this->bred[0] = array("name"=>$name, "url"=>$dopParent.$row['url']);
                       }else {
                           //рубрика-------------------------------------------- 
                            $sth0 = $this->db->prepare('SELECT m1.*  FROM '.PREFIX.'_menu m 
                                    LEFT JOIN '.PREFIX.'_rel_menu_rub rm on rm.`id_menu` = m.`id_menu` 
                                    LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu` = rm.`id_rub`     
                                    WHERE 
                                      m.`id_menu` = ? and 
                                      m.`is_visible` = 1 and
                                      m1.`url_name` = ? and 
                                      m1.`is_visible` = 1 and
                                      m1.`is_rubricator` = 1'); 
                    
                                $sth0->bindParam(1, $punct['id_menu'], PDO::PARAM_INT);
                                $sth0->bindParam(2, $parts0[3], PDO::PARAM_STR);
                                $sth0->execute();
                                $row = $sth0->fetch(PDO::FETCH_ASSOC);
                                
                                $urlRubric=$dopParent.$row['url_name'];
                           if ($parts0[4]==""){
                                $this->bred[0] = array("name"=>$row['name'], "url"=>$urlRubric);
                           }else  if ($parts0[4]=="letter"){
                                $this->bred[0] = array("name"=>urldecode($parts0[5]));
                                $this->bred[1] = array("name"=>$row['name'], "url"=>$urlRubric);
                           }else if (strrpos($parts0[4], ".html")){
                                if ($type=="person") $sth = $this->db->prepare("SELECT p.`fio`, p.`url` FROM ".PREFIX."_person p WHERE p.`url` = ? and is_in_encyclopedia='1'");         
                                if ($type=="org") $sth = $this->db->prepare("SELECT p.`name`, p.`url` FROM ".PREFIX."_sp_organasation p WHERE p.`url` = ? and is_in_encyclopedia='1'");         
                                $sth->bindParam(1, $parts0[4], PDO::PARAM_STR);
                                $sth->execute();
                                $row1 = $sth->fetch(PDO::FETCH_ASSOC);  
                                $name = ($type=="person") ? $row1['fio'] : $row1['name'] ;
                                $this->bred[0] = array("name"=>$name, "url"=>$dopParent.$row['url']."/".$row1['url']);
                                $this->bred[1] = array("name"=>$row['name'], "url"=>$dopParent.$row['url_name']);
                            }
                           
                       }
                       #-рубрика-----------------------------------------------
                    }
                     #--- person and organization   
                    
              }       
        }
        #------------------------------------
   
        krsort($this->bred);
       // print_r($this->bred);        
        return $this->bred;
    }
    
    #---------------------------------------------------------------------------
      private function selectSettings(){
        $sth=$this->db->query("SELECT `name`, `value` FROM ".PREFIX."_main_setting");
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->settings[$row['name']] = $row['value'];
        }
    }
    
    #---------------------------------------------------------------------------
     private function getInfoTopMenu($request, $top){
        
       $url=$request;
       $url1=$request."/";
       $sth = $this->db->prepare('SELECT m.* FROM '.PREFIX.'_menu m 
                                    WHERE (m.`url_name` = ? or m.`url_name` = ?) and
                                    m.`id_menu_top` = ? and
                                    m.`is_visible` = 1  and 
                                    m.`is_rubricator` = 0 
                                    ');
        $sth->bindParam(1, $url, PDO::PARAM_STR);
        $sth->bindParam(2, $url1, PDO::PARAM_STR);
        $sth->bindParam(3, $top, PDO::PARAM_INT);
        $sth->execute();  
        return $sth->fetch(PDO::FETCH_ASSOC);
         
         
     }
    #---------------------------------------------------------------------------
    private function getInfoTargetMenu($request){
        
       $url=$request;
       $url1=$request."/";
       $sth = $this->db->prepare('SELECT m1.* FROM '.PREFIX.'_menu m 
                                    LEFT JOIN '.PREFIX.'_menu m1 on m1.`id_menu_top` = m.`id_menu` 
                                    WHERE (m.`url` = ? or m.`url` = ?) and 
                                    m.`is_visible` = 1 and m1.`is_visible` = 1 
                                    and m1.`is_rubricator` = 0 
                                    ORDER BY m1.position');
        $sth->bindParam(1, $url, PDO::PARAM_STR);
        $sth->bindParam(2, $url1, PDO::PARAM_STR);
        $sth->execute();  
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
}

?>
