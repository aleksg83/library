<?php

class Paging {
    
    private $countItems, $itemsPerPage, $currentPage, $cntPage, $result, $url, $request, $queryString;
            
    public function __construct($countItems, $setting, $currentPage, $request = null) {        
        $this->countItems = (!$countItems)?1:$countItems;
        $this->itemsPerPage = (!$this->itemsPerPage)?($setting['countOnPage'])?$setting['countOnPage']:15:$this->itemsPerPage;
        $this->currentPage = $currentPage;
        $this->request = $request;     
    }
    
    public function createPagingNav(){
        $this->cntPage = ceil($this->countItems/$this->itemsPerPage);        
        if($this->cntPage <= 1) return;
        $this->createURL();
        
        $next = ($this->currentPage > 1)?"<a href=".$this->url."page=".($this->currentPage-1)." class='prev'></a>":'';
        $prev = ($this->currentPage < $this->cntPage)?"<a href=".$this->url."page=".($this->currentPage+1)." class='next'></a>":'';
        
        $this->result = '<div class="pages"><div class="innerpagin"><div class="innerpagin2"><span class="texts">Страница: </span>'.$next;
        
        if($this->cntPage <= 5){
            $this->simpleRow();
        }elseif($this->currentPage < 4){
            $this->endTochka();
        }elseif(($this->cntPage - $this->currentPage > 2) && ($this->currentPage >= 4)){            
            $this->betweenTochka();
        }else{
            $this->startTochka();
        }
        
        $this->result .= $prev.'</div></div></div>';
        return $this->result;
    }
    
    private function simpleRow(){
        for($i=1; $i<=$this->cntPage; $i++) 
            $this->result .= ($i == $this->currentPage)?'<a href="'.$this->url."page=".$i.'" class="act">'.$i.'</a>':'<a href="'.$this->url."page=".$i.'">'.$i.'</a>';
    }
    
    private function endTochka(){        
        for($i=1; $i<=4; $i++) $this->result .= ($i == $this->currentPage)?'<a href="'.$this->url."page=".$i.'" class="act">'.$i.'</a>':'<a href="'.$this->url."page=".$i.'">'.$i.'</a>';
        $this->result .= '<span class="tochka"></span><a href="'.$this->url.'"page="'.$this->cntPage.'">'.$this->cntPage.'</a>';
    }
    
    private function startTochka(){
        $this->result .= '<a href="'.$this->url.'"page="1">1</a><span class="tochka"></span>';
        for($i=$this->cntPage-3; $i<=$this->cntPage; $i++) $this->result .= ($i == $this->currentPage)?'<a href="'.$this->url."page=".$i.'" class="act">'.$i.'</a>':'<a href="'.$this->url."page=".$i.'">'.$i.'</a>';
    }
    
    private function betweenTochka(){
        $this->result .= '<a href="'.$this->url.'"page=1>1</a>';
        $this->result .= '<span class="tochka"></span>';
        $this->result .= '<a href="'.$this->url.'page='.($this->currentPage-1).'">'.($this->currentPage-1).'</a>';
        $this->result .= '<a href="'.$this->url.'page='.($this->currentPage).'"  class="act">'.($this->currentPage).'</a>';
        $this->result .= '<a href="'.$this->url.'page='.($this->currentPage+1).'">'.($this->currentPage+1).'</a>';
        $this->result .= '<span class="tochka"></span>';        
        $this->result .= '<a href="'.$this->url.'page='.$this->cntPage.'">'.$this->cntPage.'</a>';
    }
    
    private function createURL(){
        if($this->request){
            $this->prepareUrlQueryString();
            $query = ($this->queryString)?$this->queryString:'';                
            $this->url = SITE_URL.trim(urldecode($this->request['url']), '/').'?'.$query;
        }elseif($_SERVER['QUERY_STRING']){
            $this->url=str_replace($_SERVER['QUERY_STRING'],"",$_SERVER['REQUEST_URI']);
        }else{
             $this->url=SITE_URL.trim($_SERVER['REQUEST_URI'], "/")."/".'?';
        }      
    }
    
    private function prepareUrlQueryString(){
        $varToQueryString = array("mode", "type", "dateStart", "dateEnd");
        $vars = explode("&", $_SERVER['QUERY_STRING']);
        foreach($vars as $var){
            list($key, $value) = explode("=", $var);
            if(in_array($key, $varToQueryString)){
                $this->queryString .= "{$key}={$value}&";
            }
        }
    }
}
?>