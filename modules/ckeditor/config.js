﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';



config.uiColor = '#c1c1c1'; //цвет рамки
config.removePlugins = 'newpage,about,save, print'; 
//config.toolbar = 'Full'; //функциональность редактора, Basic-минимум, Full-максимум
/*
config.toolbar_Basic = //индивидуальная настройка режима Basic
[
['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink','-','Smiley']
];
config.width = 450; //ширина окна редактора

*/

/*
						toolbar :
		[
			['Source', '-', '-','Undo','Redo'],
			['Find','Replace','-','SelectAll','RemoveFormat'],
			['Link', 'Unlink', 'Image'],
			'/',
			['FontSize', 'Bold', 'Italic','Underline'],
			['NumberedList','BulletedList','-','Blockquote'],
			['TextColor', '-', 'Smiley','SpecialChar', '-', 'Maximize']
		],
		
		smiley_images :
		[
			'regular_smile.gif','sad_smile.gif','wink_smile.gif','teeth_smile.gif','tounge_smile.gif',
			'embaressed_smile.gif','omg_smile.gif','whatchutalkingabout_smile.gif','angel_smile.gif','shades_smile.gif',
			'cry_smile.gif','kiss.gif'
		],
		smiley_descriptions :
		[
			'smiley', 'sad', 'wink', 'laugh', 'cheeky', 'blush', 'surprise',
			'indecision', 'angel', 'cool', 'crying', 'kiss'
		]
*/

};