CKEDITOR.plugins.add('classes2',{
    init: function(editor){
        var cmd = editor.addCommand('addclasses', {
            exec:function(editor){
                 var element = editor.getSelection().getStartElement();
                     element.addClass("color2"); 
           }
        });
        cmd.modes = { wysiwyg : 1, source: 0 };
        editor.ui.addButton('Classes2',{
            label: 'Коричневый',
            command: 'addclasses',
            toolbar: 'toolbar2'
        });
        
    },
    icons:'classes2',
});