<?php
class ReaderController {
    private $fc, $view, $params;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();  
        $this->params = $this->fc->getParams();
        $this->preparePage();
    }
    
    public function IndexAction(){  
        $object = new BookData($this->dopParams);
        $this->renderPage($object, $rubrs);
    }

    public function ajaxIndexAction(){   
        $this->countPerPage = $this->dopParams['limit'];  
        $object = new BookData($this->dopParams);
        $rubrs = explode("/", trim(substr(urldecode($this->dopParams['url']), 7), "/"));
        $this->ajaxRenderPage($object, $rubrs);
    }
    
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------    
    private function ajaxRenderPage(Data $object, $rubrs){
        $this->prepareRenderPage($object, $rubrs); 
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/reader/listData.php');
        echo $result;
    }
    
    private function renderPage(Data $object, $rubrs){
        $this->prepareRenderPage($object, $rubrs); 
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/reader/index.php');           
        $this->fc->setBody($result);
    }  
    
    private function prepareRenderPage(Data $object, $rubrs){
        $rubr = (!is_array($rubrs))?array_pop(explode('/', $rubrs)):array_pop($rubrs);   
        $object->setRubr($rubr);
        $object->collectSetting(); 
        $object->setStock(true);
        #$this->view->title = $object->getRubrName().' ('.$object->getCountData().')';
        $this->view->objects = $object->getListData(); 
        $paging = new Paging($object->getCountData(), array("countOnPage"=>$this->countPerPage), ($_GET['page'])?$_GET['page']:1, $this->dopParams);
        $this->view->paging = $paging->createPagingNav(); 
    }
    
    private function preparePage(){
        $menu = new Menu($this->fc->request());        
        $this->view->menuTop = $menu->getTopMenu();  
        $this->dopParams = $this->fc->getDopParams();
        $this->view->menuLeft = $menu->getLeftMenu($this->params);
        $this->view->rubricators = $menu->getRubricators();
        $this->view->textMenuRubr = $menu->getTextMenuRubr($this->params);     
        $this->view->breadCrumbs = $menu->getBreadCrumbs();
    }
}
?>