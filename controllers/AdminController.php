<?php
class AdminController {
    private $fc, $view, $params, $dopparams, $auth, $shablon, $result, $lang, $urlPage, $system;
   
    public function __construct() { 
        $this->fc=FrontController::getInstance(); 
        $this->prepare();
        
        /*языковый файл с константами*/  
        lng::$file='admin'; 
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        
        $this->createTopMenu();
        
        $this->system=new systemGurnalEdit(); 
        
        if(!$this->auth->isLogged){
           $this->view->namePage = $this->lang['auth'];
           $this->result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/login.php");
        }
        if (!$this->auth->isActive) $this->logoutAction();
            
        include DROOT."/admin/forms/class.php";
      
        /*url*/
        $this->urlPage=str_replace ("?".$_SERVER['QUERY_STRING'], "", $_SERVER['REQUEST_URI']);
     }
    
    public function indexAction(){           
        if($this->auth->isLogged){                 
            $this->view->namePage = $this->lang['dashboard'];
            $this->view->shablon = $this->shablon;    
            $this->system->insert($this->urlPage);
            #echo "<pre>"; print_r($this->view->sp); echo "</pre>"; exit;
            $this->result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/dashboard.php");
            
        }
        $this->fc->setBody($this->result);
    }
    
    public function logoutAction(){
        $this->system->insert($this->urlPage);
        $this->auth->checkLogout();
        header('Location: /admin/');
        die();
    }
#-----------------------------------------------------------------------------------------------------------------------------------------------------------   
//sp
    public function showSpAction(){
        if($this->auth->isLogged){
            $sp = new spEdit($this->params[0]);
            $this->system->insert($this->urlPage);
           //$this->view->namePage=$sp->namePage[0]['TABLE_COMMENT'];
            $this->view->namePage=$this->lang['main_menu_sp_'.$this->params[0].''];
            $this->view->operation=$this->dopparams;
           
            $el = new ElementSp( new ConstructorSp($sp, $this->lang));
            $this->createObject($el);            
         
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);               
    }
    
    public function addSpAction(){
      if($this->auth->isLogged){   
         $sp = new spEdit($this->params[0]);
         $result = ($sp->add($_POST))?'ok':'false';
         #-------------------
         if ($result=='ok'){
            $db_new=serialize($sp->select($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
            }
         #-------------------
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
           $this->logoutAction();
      }  
    }
    public function updateSpAction(){
      if($this->auth->isLogged){     
        $sp = new spEdit($this->params[0]);
        $db_old=serialize($sp->select($_POST['id']));
        $result = ($sp->update($_POST))?'ok':'false';
        #-------------------
        if ($result=='ok'){
            $db_new=serialize($sp->select($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
        }
        #-------------------
        header('Location: '.$_POST['url'].'?edit='.$result);
      }else{
        $this->logoutAction();
      }  
    }
    public function deleteSpAction(){
      if($this->auth->isLogged){   
        $sp = new spEdit($this->params[0]);
        #-------------------
          $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($sp->select($val));
            $this->system->insert($this->urlPage, $db_old, "");
          }
        #------------------
        $result = ($sp->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
        $this->logoutAction();  
      }  
    }    
    public function columnUpSpAction(){
      if($this->auth->isLogged){     
        $sp = new spEdit($this->params[0]);
        $db_old=serialize($sp->select($_POST['id']));
        $result=$sp->columnUp($_POST['id'], $_POST['is_visible']);
        #-------------------
        if ($result){
            $db_new=serialize($sp->select($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
        }
        #-------------------
        return $result; 
      }else{
        $this->logoutAction();   
      }  
    }
    public function formShowSpAction(){
        #echo "<pre>"; var_dump($this->fc); echo "</pre>"; exit;
      if($this->auth->isLogged){   
        $sp = new spEdit($this->params[0]);
        $this->system->insert($this->urlPage);
        $form = $sp->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'],  $this->lang);
        print $form;
      }else{
        $this->logoutAction();   
      }    
    }  
    
    public function showSpOrgRubricAction(){
        if($this->auth->isLogged){
           $spId=$this->dopparams['id'];
            $name= '"'.urldecode($this->dopparams['name']).'"';
            $sp = new spEdit($this->params[0]);
            #-------------------
            $this->system->insert($this->urlPage);
            #-------------------
            $el = new ElementMenu( new ConstructorSpOrgInRubric($sp, $spId, $this->lang));
            $this->createObject($el);
            
            $this->view->namePage=$this->lang['sp_org_in_rubric']." ".$name;
            $this->view->operation=$this->dopparams; 
            $this->view->include="table2.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        
    }
    
//sp 
#-----------------------------------------------------------------------------------------------------------------------------------------------------------    
//menu
    public function showMenuAction(){
        Common::ChecksAccessRedactor();
        if($this->auth->isLogged){
           
            $isRubric=($this->dopparams['rubric']=="ok")? 1  : 0 ;
            $menu = new menuEdit($isRubric);
            #-------------------
            $this->system->insert($this->urlPage);
            #-------------------
            $el = new ElementMenu( new ConstructorMenu($menu, $this->lang));
            $this->createObject($el);
            
            $namePage=($isRubric)? $this->lang['rubricSite'] : $this->lang['mainmenu'];
            $this->view->namePage=$namePage;
            $this->view->operation=$this->dopparams; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        //echo "<pre>"; print_r($menu->showMenu()); echo "</pre>";
    }
    #---------------------------------------------------------------------------
    public function addMenuAction(){
      Common::ChecksAccessRedactor();
      if($this->auth->isLogged){  
        $menu = new menuEdit($_POST['is_rubricator']);
        $result = ($menu->addMenu($_POST['id_menu_top'], $_POST['name'], $_POST['is_visible'], $_POST['urlpunct'],$_POST['text'], $_POST['level'], $_POST['position'], $_POST['rubricators']))?'ok':'false';
        if ($_POST['is_rubricator']) $result.="&rubric=ok";
        #-------------------
         if ($result){
            $db_new=serialize($menu->showMenu($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
         }
        #-------------------
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
        $this->logoutAction(); 
      }  
    }
    #---------------------------------------------------------------------------
   public function updateMenuAction(){
     Common::ChecksAccessRedactor();  
     if($this->auth->isLogged){   
        $menu = new menuEdit($_POST['is_rubricator']);
        $db_old=serialize($menu->showMenu($_POST['id']));
        $result = ($menu->updateMenu($_POST['id_menu_top'], $_POST['name'], $_POST['is_visible'], $_POST['urlpunct'],$_POST['text'], $_POST['level'], $_POST['position'], $_POST['id'], $_POST['rubricators']))?'ok':'false';
        if ($_POST['is_rubricator']) $result.="&rubric=ok";
        #-------------------
         if ($result){
            $db_new=serialize($menu->showMenu($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        #-------------------
        header('Location: '.$_POST['url'].'?update='.$result);
     }else{
       $this->logoutAction();  
     }
     
    }
    public function deleteMenuAction(){
     Common::ChecksAccessRedactor();   
     if($this->auth->isLogged){     
        $isRubric=($this->dopparams['rubric']=="ok")? 1  : 0 ;
        $menu = new menuEdit($isRubric);
        #-------------------
          $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($menu->showMenu($val));
            $this->system->insert($this->urlPage, $db_old, "");
          }
        #-----------------
        $result = ($menu->deleteMenu($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
         if ($isRubric) $result.="&rubric=ok";
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
     }else{
        $this->logoutAction();   
     }   
    }
    
     public function formShowMenuAction(){
      Common::ChecksAccessRedactor();   
      if($this->auth->isLogged){      
        $isRubric=($this->dopparams['rubric']=="ok")? 1  : 0 ;
        $menu = new menuEdit($isRubric);
        $this->system->insert($this->urlPage);
        $form = $menu->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
        $this->logoutAction();   
      }     
    } 
    
    public function rubricMenuAction(){
        Common::ChecksAccessRedactor();
        if($this->auth->isLogged){
            $isRubric=($this->dopparams['rubric']=="ok")? 0  : 1 ;
            $punctId=$this->dopparams['menu'];
            $namePunct= '"'.urldecode($this->dopparams['name']).'"';
            $menu = new menuEdit($isRubric);
            #-------------------
            $this->system->insert($this->urlPage);
            #-------------------
            $el = new ElementMenu( new ConstructorMenuInRubric($menu, $punctId, $this->lang));
            $this->createObject($el);
            
            $namePage=($isRubric)? $this->lang['rubric_in_menu_razdel']." ".$namePunct : $this->lang['menu_in_rubric_razdel']." ".$namePunct." ".$this->lang['menu_in_rubric_razdel2'];
            $this->view->namePage=$namePage;
            $this->view->operation=$this->dopparams; 
            $this->view->include="table2.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        //echo "<pre>"; print_r($menu->showMenu()); echo "</pre>";
    }
    
    
     public function getUniqMenuAction(){
       Common::ChecksAccessRedactor();  
       //$isRubric=($this->dopparams['rubric']=="ok")? 1  : 0 ;
       $isRubric=$_POST['isrubric'];
       $punctId=$this->dopparams['menu'];
       $menu = new menuEdit($isRubric);
       #-------------------
       $this->system->insert($this->urlPage);
       #-------------------
       $result=$menu->getURLCount($_POST['action'], $_POST['url_name'], $_POST['id_menu_top'], $_POST['id_menu']);
       print $result;
    }   
    
    
//menu
#-----------------------------------------------------------------------------------------------------------------------------------------------------------   
//user
    public function showUserAction(){
        Common::ChecksAccessRedactor();
        if($this->auth->isLogged){
            $user = new userEdit();
            #-------------------
            $this->system->insert($this->urlPage);
            #-------------------
            $this->view->namePage=$this->lang['user_page'];
            $this->view->operation=$this->dopparams;
            $el = new ElementUser( new ConstructorUser($user, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getUser($this->params[0]);
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    public function addUserAction(){
        Common::ChecksAccessRedactor();
        $user = new userEdit();
        $result = ($user->add($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($user->getUser($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
            }
        #-------------------
        header('Location: '.$_POST['url'].'?add='.$result);
     }
    public function updateUserAction(){
        Common::ChecksAccessRedactor();
        $user = new userEdit();
        $db_old=serialize($user->getUser($_POST['id']));
        $result = ($user->update($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($user->getUser($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        #-------------------
        header('Location: '.$_POST['url'].'?update='.$result);
    }
    public function deleteUserAction(){
        Common::ChecksAccessRedactor();
        $user = new userEdit();
        #-------------------
          $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($user->getUser($val));
            $this->system->insert($this->urlPage, $db_old, "");
          }
        #-----------------
        $result = ($user->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
   }
    
    public function formShowUserAction(){
       Common::ChecksAccessRedactor();  
      if($this->auth->isLogged){  
         $user= new userEdit();
         $this->system->insert($this->urlPage);
         $form = $user->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
         print $form;
      }else{
         $this->logoutAction();
      }     
    } 
    
    public function getUniqLoginAction(){
       $user= new userEdit();
       #-------------------
       $this->system->insert($this->urlPage);
       #-------------------
       $result=$user->getLoginCount($_POST['action'], $_POST['login']);
       print $result;
    }   
    
    public function showUserInRolesAction(){
        Common::ChecksAccessRedactor();
        if($this->auth->isLogged){
            $user = new userEdit();
            #-------------------
             $this->system->insert($this->urlPage);
            #-------------------
            $userId=$this->dopparams['user'];
            $login= '"'.urldecode($this->dopparams['login']).'"';
            
            $this->view->namePage=$this->lang['user_in_roles_page_login']." - ".$login;
            $this->view->operation=$this->dopparams;
            $el = new ElementUser( new ConstructorUserInRoles($user, $userId, $login, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    } 
    
    public function profileAction(){
        if($this->auth->isLogged){
            $user = new userEdit();
            #-------------------
            $this->system->insert($this->urlPage);
            #-------------------
            $this->view->namePage=$this->lang['user_profile'];
            $this->view->operation=$this->dopparams;
            $el = new ElementUser( new ConstructorUserProfile($user, $this->lang));
            $this->createObject($el);   
            $this->view->include="profile.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getUser($this->params[0]);
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    
    public function updateProfileAction(){
     if($this->auth->isLogged){ 
        $user = new userEdit();
        $db_old=serialize($user->getUser($_POST['id_users']));
        $result = ($user->updateProfile($_POST))?'ok':'falsepass';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($user->getUser($_POST['id_users']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        #-------------------
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      } 
    }
    
//user
#-----------------------------------------------------------------------------------------------------------------------------------------------------------      
//roles
    public function showRolesAction(){
        if($this->auth->isLogged){
            $user = new userEdit();
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['user_rolespage'];
            $this->view->operation=$this->dopparams;
            $el = new ElementUser( new ConstructorUserRoles($user, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getRoles();
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);         
    }
   
     public function addRolesAction(){
      if($this->auth->isLogged){ 
        $user = new userEdit();
        $result = ($user->addRoles($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($user->getRoleInfo($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
          }
        #-------------------
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      }  
    }
     public function updateRolesAction(){
      if($this->auth->isLogged){    
        $user = new userEdit();
        $db_old=serialize($user->getRoleInfo($_POST['id']));
        $result = ($user->updateRoles($_POST))?'ok':'false';
        if ($result=='ok'){
            $db_new=serialize($user->getRoleInfo($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }   
    }
    public function deleteRolesAction(){
     if($this->auth->isLogged){   
        $user = new userEdit();
        #-------------------
         $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($user->getRoleInfo($val));
            $this->system->insert($this->urlPage, $db_old, "");
          }
        #-----------------
        $result = ($user->deleteRoles($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
     }else{
            $this->logoutAction();
     }    
    }
    
    public function formShowRolesAction(){
        $user= new userEdit();
        $this->system->insert($this->urlPage);
        $form = $user->showFormRoles($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
    } 
   
    
    public function setRolesAction(){
        $user = new userEdit();
        $this->system->insert($this->urlPage);
        $result = ($user->setUserRoles($_POST['idUser'],$_POST['idRoles']))?'ok':'false';
        header('Location: '.$_POST['url'].'?update='.$result);
    }
//roles
#-----------------------------------------------------------------------------------------------------------------------------------------------------------  
//import
    public function showFormImportAction(){
        if($this->auth->isLogged){
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['importBook'];
            $this->view->info=$this->lang['infoImport'];
            $this->view->action="/admin/importBook/";
            $this->view->operation=$this->dopparams; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/import.php");
            $this->fc->setBody($result);  
           
        }else{
            $this->logoutAction();
        }
    }
    
    public function importBookAction(){
      if($this->auth->isLogged){  
        $this->system->insert($this->urlPage);
        $import = new importData();
        $res = $import->importBook($_FILES['book'], 1);
        header('Location: /admin/showFormImport?add='.$res);
      }else{
            $this->logoutAction();
      }  
    }
//import
#-----------------------------------------------------------------------------------------------------------------------------------------------------------   
//person
    public function showPersonAction(){
        if($this->auth->isLogged){
            $person = new PersonEdit();
            //$tmp = $person->getPersons($this->params[0]);
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['personname'];
            $this->view->operation=$this->dopparams;
            
            $el = new ElementPerson( new ConstructorPerson($person, $this->lang));
            
            $this->createObject($el);            
           
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);               
       
       // echo "<pre>"; print_r($tmp); echo "</pre>";
    }
    public function addPersonAction(){
      if($this->auth->isLogged){  
        $person = new PersonEdit();
        $result = ($person->add($_POST))?'ok':'false';
         #-------------------
         if ($result=='ok'){
            $db_new=serialize($person->getPersons($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
         }
         #-------------------
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      }  
    }
    public function updatePersonAction(){
     if($this->auth->isLogged){    
        $person = new PersonEdit();
        $db_old=serialize($person->getPersons($_POST['id']));
        $result = ($person->update($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($person->getPersons($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
       #-------------------
        header('Location: '.$_POST['url'].'?update='.$result);
     }else{
            $this->logoutAction();
     }     
    }
    public function deletePersonAction(){
      if($this->auth->isLogged){   
        $person = new PersonEdit();
        #-------------------
          $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($person->getPersons($val));
            $this->system->insert($this->urlPage, $db_old, "");
          }
        #-----------------
        $result = ($person->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
            $this->logoutAction();
      }   
    }
    
    public function formShowPersonAction(){
        $person = new PersonEdit();
        $this->system->insert($this->urlPage);
        $form = $person->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
    } 
    
     public function showPersonRubricAction(){
        if($this->auth->isLogged){
           
            $personId=$this->dopparams['person'];
            $namePerson= urldecode($this->dopparams['name']);
            $person = new PersonEdit();
            $this->system->insert($this->urlPage);
            
            $el = new ElementMenu( new ConstructorPersonInRubric($person, $personId, $this->lang));
            $this->createObject($el);
            
            $this->view->namePage=$this->lang['person_in_rubric']." ".$namePerson;
            $this->view->operation=$this->dopparams; 
            $this->view->include="table2.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        //echo "<pre>"; print_r($menu->showMenu()); echo "</pre>";
    }
    
     public function showPersonDataAction(){
        if($this->auth->isLogged){
           
            $personId=$this->dopparams['person'];
            $namePerson= urldecode($this->dopparams['name']);
            $person = new PersonEdit();
            $this->system->insert($this->urlPage);
            $el = new ElementMenu( new ConstructorPersonInData($person, $personId, $this->lang));
            $this->createObject($el);
            
            $this->view->namePage=$this->lang['person_in_data']." ".$namePerson;
            $this->view->operation=$this->dopparams; 
            $this->view->include="table2.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        //echo "<pre>"; print_r($menu->showMenu()); echo "</pre>";
    }
    
    
//person
#----------------------------------------------------------------------------------------------------------------------------------------------------------- 
//citata
    public function showPersonCitataAction(){
        if($this->auth->isLogged){
            $personId=intval($this->dopparams['person']);
            
            if ($this->dopparams['name']==""){
              $person=new PersonEdit();
              $content=$person->getPersons($personId);
              $namePerson=$content['fio'];
            }else{
                $namePerson= '"'.urldecode($this->dopparams['name']).'"';
            }    
            
            $citata = new CitataEdit($personId);
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['citata_person']." ".$namePerson;
            $this->view->operation=$this->dopparams;
            
            $el = new ElementCitata( new ConstructorCitata($citata, $personId, $this->lang));
            
            $this->createObject($el);            
           
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);               
       
       // echo "<pre>"; print_r($tmp); echo "</pre>";
  }
  
   public function addCitataAction(){
     if($this->auth->isLogged){  
        $personId=$this->dopparams['person'];
        $citata = new CitataEdit($personId);
        $result = ($citata->add($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($citata->getCitats($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
          }
        #-------------------
        header('Location: '.$_POST['url'].'&add='.$result);
      }else{
            $this->logoutAction();
      }  
    }
    public function updateCitataAction(){
      if($this->auth->isLogged){  
        $personId=$this->dopparams['person'];
        $citata = new CitataEdit($personId);
        $db_old=serialize($citata->getCitats($_POST['id']));
        $result = ($citata->update($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($citata->getCitats($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        #-------------------
        header('Location: '.$_POST['url'].'&update='.$result);
      }else{
            $this->logoutAction();
      }    
    }
    public function deleteCitataAction(){
      if($this->auth->isLogged){  
        $personId=$this->dopparams['person'];
        $citata = new CitataEdit($personId);
        #-------------------
          $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($citata->getCitats($val));
            $this->system->insert($this->urlPage, $db_old, "");
          }
        #-----------------
        $result = ($citata->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
            $this->logoutAction();
      }   
    }
      
    public function formShowCitataAction(){
        $personId=$this->dopparams['person'];
        $citata = new CitataEdit($personId);
        $this->system->insert($this->urlPage);
        $form = $citata->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
    } 
    
//citata
#----------------------------------------------------------------------------------------------------------------------------------------------------------- 
//news
    public function showNewsAction(){
        if($this->auth->isLogged){
            $news = new newsEdit();
            $this->system->insert($this->urlPage);
            //$tmp = $news->getNews();
            $this->view->namePage=$this->lang['newsname'];
            $this->view->operation=$this->dopparams;
               
            $el = new ElementNews( new ConstructorNews($news, $this->lang));
            $this->createObject($el); 
            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
       
        $this->fc->setBody($result);      
        //echo "<pre>"; print_r($tmp); echo "</pre>";
    }
    public function addNewsAction(){
      if($this->auth->isLogged){  
        $news = new newsEdit();
        $result = ($news->add($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($news->getNews($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
            }
        #-------------------
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      }    
    }
    public function updateNewsAction(){
      if($this->auth->isLogged){  
        $news = new newsEdit();
        $db_old=serialize($news->getNews($_POST['id']));
        $result = ($news->update($_POST))?'ok':'false';
        if ($result=='ok'){
            $db_new=serialize($news->getNews($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }   
    }
    public function deleteNewsAction(){
      if($this->auth->isLogged){  
        $news = new newsEdit();
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($news->getNews($val));
            $this->system->insert($this->urlPage, $db_old, "");
         }
        $result = ($news->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
            $this->logoutAction();
      }   
    }
    
    public function formShowNewsAction(){
        $news = new newsEdit();
        $this->system->insert($this->urlPage);
        $form = $news->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
    } 
    
    
//news
#----------------------------------------------------------------------------------------------------------------------------------------------------------- 
//rss
 public function rssAction(){
       
      if($this->auth->isLogged){
            $rss = new rssEdit();
            $tmp = $rss->getNewsRSS();
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['rss_name'];
            $this->view->operation=$this->dopparams;
               
            $el = new ElementNews( new ConstructorRSS($rss, $this->lang));
            $this->createObject($el); 
            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
       
        $this->fc->setBody($result);      
        //echo "<pre>"; print_r($tmp); echo "</pre>";
   
 }
 
  public function updateSettingRSSAction(){
     if($this->auth->isLogged){ 
        $rss = new rssEdit();
        $db_old=serialize($rss->getRSSSetting());
        $result = ($rss->updateSetting($_POST))?'ok':'false';
        #--------------------
        if ($result=='ok'){
            $db_new=serialize($rss->getRSSSetting());
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        #------------------- 
        header('Location: '.$_POST['url'].'?updateSetRss='.$result);
      }else{
            $this->logoutAction();
      }
          
  }
  
  public function updateRSSAction(){
      if($this->auth->isLogged){  
        $rss = new rssEdit();
        $this->system->insert($this->urlPage);
        $result = ($rss->update())?'ok':'false';
        $_POST['url']='/admin/rss/';
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }  
  }
 
 public function formShowRSSAction(){
        $rss = new rssEdit();
        $this->system->insert($this->urlPage);
        $form = $rss->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
    } 
#----------------------------------------------------------------------------------------------------------------------------------------------------------- 
//Library    
public function showLibAction(){
    if($this->auth->isLogged){
            $lib = new libEdit($this->params[0]);
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['lib_'.$this->params[0]];
            $this->view->operation=$this->dopparams;
            
            $el = new ElementLib( new ConstructorLib($lib, $this->lang));
            $this->createObject($el);            
            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
}    

public function addLibAction(){
     if($this->auth->isLogged){
        $lib = new libEdit($this->params[0]);
        $result = ($lib->add($_POST))?'ok':'false';
        
        if ($result=='ok'){
            $db_new=serialize($lib->getData($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
        }
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      }  
}

public function updateLibAction(){
     if($this->auth->isLogged){
        $lib = new libEdit($this->params[0]);
        $db_old=serialize($lib->getData($_POST['id']));
        $result = ($lib->update($_POST))?'ok':'false';
        //------------------
        if ($result=='ok'){
          $db_new=serialize($lib->getData($_POST['id']));
          $this->system->insert($this->urlPage, $db_old, $db_new);
        }
        //------------------
        header('Location: '.$_POST['url'].'?update='.$result);
     }else{
            $this->logoutAction();
      }     
}

public function deleteLibAction(){
        $lib = new libEdit($this->params[0]);
        #-------------------
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($lib->getData($val));
            $this->system->insert($this->urlPage, $db_old, "");
        }
        #------------------
        $result = ($lib->delete($_POST['id']))?'ok':'false';
        header('Location: '.$_POST['url'].'?delete='.$result);
}

public function formShowLibAction(){
        $lib = new libEdit($this->params[0]);
        $this->system->insert($this->urlPage);
        $form = $lib->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
}

public function showLibInRubricAction(){
        if($this->auth->isLogged){
           
            $dataId=$this->dopparams['data'];
            $nameData= '"'.urldecode($this->dopparams['name']).'"';
            $lib = new libEdit($this->params[0]);
            $this->system->insert($this->urlPage);
            $el = new ElementLib( new ConstructorLibInRubric($lib, $dataId, $this->lang));
            $this->createObject($el);
            
            $this->view->namePage=$this->lang['data_in_rubric']." ".$nameData;
            $this->view->operation=$this->dopparams; 
            $this->view->include="table2.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        //echo "<pre>"; print_r($menu->showMenu()); echo "</pre>";
    }
    
   #--- file (images) library ------------------------------------------ 
    public function showLibImagesAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $nameData= '"'.urldecode($this->dopparams['name']).'"';
            $lib = new libEdit($this->params[0]);
            $this->system->insert($this->urlPage);
            $el = new ElementLib( new ConstructorLibImages($lib, $dataId, $this->lang));
            $this->createObject($el);
            
            $this->view->namePage=$this->lang['data_in_images']." ".$nameData;
            $this->view->operation=$this->dopparams; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result); 
    }    

    public function formShowLibImgAction(){
        $dataId=$this->dopparams['data'];
        $lib = new libEdit($this->params[0]);
        $this->system->insert($this->urlPage);
        $el = new libImgEdit($lib, $dataId, $this->lang);    
        $form = $el->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url']);
        print $form;
         
    }
      
    public function addLibImgAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $lib = new libEdit($this->params[0]);
            $el = new libImgEdit($lib, $dataId, $this->lang);
            $this->system->insert($this->urlPage);
            $result = ($el->add($_POST))?'ok':'false';
            header('Location: '.$_POST['url'].'&add='.$result);
        }else{
            $this->logoutAction();
        }
    }    
    
    public function updateLibImgAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $lib = new libEdit($this->params[0]);
            $this->system->insert($this->urlPage);            
            $el = new libImgEdit($lib, $dataId, $this->lang);
            $result = ($el->update($_POST))?'ok':'false';
            header('Location: '.$_POST['url'].'&update='.$result);
        }else{
            $this->logoutAction();
        }
    }    
    
    public function deleteLibImgAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $lib = new libEdit($this->params[0]);
            $this->system->insert($this->urlPage);
            $el = new libImgEdit($lib, $dataId, $this->lang);
            $result = ($el->delete($_POST['id']))?'ok':'false';
            $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
            header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
        }else{
            $this->logoutAction();
        }    
    }

   #--- file (images) library --------------------------------------------------
  
   #----- mapping --------------------------------------------------------------  
    public function showLibMappingAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $nameData= '"'.urldecode($this->dopparams['name']).'"';
            $type=$this->dopparams['type'];
            $this->system->insert($this->urlPage);
            $lib = new libEdit($this->params[0]);
            $map = new libMapEdit($lib, $dataId, $type, $this->lang);    
            
            $el = new ElementLib( new ConstructorMapping($map, $this->lang) );
            $this->createObject($el);
            
            $this->view->namePage=$this->lang['data_in_mapping_'.$map->type.'']." ".$nameData;
            $this->view->operation=$this->dopparams; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result); 
    } 
    
    public function addLibMapAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $type=$this->dopparams['type'];
            $lib = new libEdit($this->params[0]);
            $map = new libMapEdit($lib, $dataId, $type, $this->lang); 
            $this->system->insert($this->urlPage);
            $result = ($map->add($_POST))?'ok':'false';
            header('Location: '.$_POST['url'].'&add='.$result);
        }else{
            $this->logoutAction();
        }
    }    
    
    public function updateLibMapAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $type=$this->dopparams['type'];
            $lib = new libEdit($this->params[0]);
            $map = new libMapEdit($lib, $dataId, $type, $this->lang);             
            $result = ($map->update($_POST))?'ok':'false';
            $this->system->insert($this->urlPage);
            header('Location: '.$_POST['url'].'&update='.$result);
        }else{
            $this->logoutAction();
        }
    }    
    
     public function deleteLibMapAction(){
        if($this->auth->isLogged){
            $dataId=$this->dopparams['data'];
            $type=$this->dopparams['type'];
            $lib = new libEdit($this->params[0]);
            $map = new libMapEdit($lib, $dataId, $type, $this->lang);  
            #-------------------
                $arrayDel = explode(",", $_POST['id']);
                foreach($arrayDel as $key=>$val){
                    $db_old=serialize($map->getMap($val));
                    $this->system->insert($this->urlPage, $db_old, "");
                }
            #-----------------
            $result = ($map->delete($_POST['id']))?'ok':'false';
            $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
            header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
        }else{
            $this->logoutAction();
        }    
    }
    
    public function formShowLibMapAction(){
        $dataId=$this->dopparams['data'];
        $type=$this->dopparams['type'];
        $lib = new libEdit($this->params[0]);
        $this->system->insert($this->urlPage);
        $map = new libMapEdit($lib, $dataId, $type, $this->lang);  
        $form = $map->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url']);
        print $form;
         
    }
    #----- mapping --------------------------------------------------------------  
   
    
    public function showLibInPersonAction(){
        if($this->auth->isLogged){
           
            $dataId=$this->dopparams['data'];
            $nameData= urldecode($this->dopparams['name']);
            $type=$this->params[1];
            $lib = new libEdit($this->params[0]);
            $this->system->insert($this->urlPage);
            $el = new ElementLib( new ConstructorLibInPerson($lib, $dataId, $type, $this->lang));
            $this->createObject($el);
            
            $this->view->namePage=$this->lang['person_in_lib']." ".$nameData;
            $this->view->operation=$this->dopparams; 
            $this->view->include="table.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        //echo "<pre>"; print_r($menu->showMenu()); echo "</pre>";
    }
    
    
//Library     

    #----------------------------------------------------------------------------------------------------------------------------------------------------------- 
//Static Page
    public function showStaticPageAction(){
        if($this->auth->isLogged){
            $page = new staticEdit();
            $this->view->namePage=$this->lang['lib_static_page'];
            $this->view->operation=$this->dopparams;
            $this->system->insert($this->urlPage);
            //$this->view->include="table2.php"; 
            $el = new ElementStaticPage( new ConstructorStaticPage($page, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
    }
    public function addStaticPageAction(){
     if($this->auth->isLogged){  
        $page = new staticEdit();
        $result = ($page->add($_POST))?'ok':'false';
        
         if ($result=='ok'){
            $db_new=serialize($page->showStatic($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
         }
        
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      } 
        
    }
    public function updateStaticPageAction(){
      if($this->auth->isLogged){    
        $page = new staticEdit();
        $db_old=serialize($page->showStatic($_POST['id']));
        $result = ($page->update($_POST))?'ok':'false';
        
        if ($result=='ok'){
            $db_new=serialize($page->showStatic($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
        }
        
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }   
    }
    
    public function deleteStaticPageAction(){
      if($this->auth->isLogged){    
        $page = new staticEdit();
        
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($page->showStatic($val));
            $this->system->insert($this->urlPage, $db_old, "");
        }
        
        $result = ($page->delete($_POST['id']))?'ok':'false';
        header('Location: '.$_POST['url'].'?delete='.$result);
      }else{
            $this->logoutAction();
      }    
    }
    
    public function formShowStaticPageAction(){
      if($this->auth->isLogged){
        $page = new staticEdit();
        $this->system->insert($this->urlPage);
        $form = $page->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
        }
         
    }
    
    
//Static Page
#----------------------------------------------------------------------------------------------------------------------------------------------------------- 
    
#----------------------------------------------------------------------------------------------------------------------------------------------------------- 
//calendar
    public function showEventsAction(){
        if($this->auth->isLogged){
            $type= ($this->params[0] == "")? 1 : $this->params[0];
            $cal = new calendarEdit($type);
            $this->view->namePage=$this->lang['lib_calendarj'];
            $this->system->insert($this->urlPage);
            
            $el = new ElementCalendar( new ConstructorCalendar($cal, $this->lang));
            $this->createObject($el);            
            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        #echo "<pre>"; print_r($tmp); echo "</pre>";
    }
    public function addEventAction(){
      if($this->auth->isLogged){  
        $type= ($this->params[0] == "")? 1 : $this->params[0];
        $cal = new calendarEdit($type);
        $result = ($cal->addEvent($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($cal->getEvents($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
          }
        #-------------------
        
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      } 
    }
    public function updateEventAction(){
      if($this->auth->isLogged){   
        $type= ($this->params[0] == "")? 1 : $this->params[0];
        $cal = new calendarEdit($type);
        $db_old=serialize($cal->getEvents($_POST['id']));
        $result = ($cal->updateEvent($_POST))?'ok':'false';
        if ($result=='ok'){
            $db_new=serialize($cal->getEvents($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
        }
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }   
    }
    public function deleteEventAction(){
      if($this->auth->isLogged){      
        $type= ($this->params[0] == "")? 1 : $this->params[0];
        $cal = new calendarEdit($type);
        
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($cal->getEvents($val));
            $this->system->insert($this->urlPage, $db_old, "");
        }
         
        $result = ($cal->deleteEvent($_POST['id']))?'ok':'false';
        header('Location: '.$_POST['url'].'?delete='.$result);
        }else{
            $this->logoutAction();
      }     
    }
    
    public function formShowEventAction(){
      if($this->auth->isLogged){
        $type= ($this->params[0] == "")? 1 : $this->params[0];   
        $cal = new calendarEdit($type);
        $this->system->insert($this->urlPage);
        $form = $cal->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
        }
         
    }
    
//calendar
#----------------------------------------------------------------------------------------------------------------------------------------------------------- 
#predprint
 public function showPredPrintAction(){
        if($this->auth->isLogged){
            $print = new predPrint();
            $this->view->namePage=$this->lang['predprint'];
            $this->system->insert($this->urlPage);
             
            $el = new ElementPrint( new ConstructorPrint($print, $this->lang));
            $this->createObject($el);            
            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        #echo "<pre>"; print_r($tmp); echo "</pre>";
    }
    public function updatePredPrintAction(){
      if($this->auth->isLogged){ 
        $print= new predPrint();
        $db_old=serialize($print->getPrintResurs($_POST['id']));
        $result = ($print->update($_POST))?'ok':'false';
        if ($result=='ok'){
            $db_new=serialize($print->getPrintResurs($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      } 
    }
    
    public function formShowPredPrintAction(){
      if($this->auth->isLogged){
        $print= new predPrint();
        $this->system->insert($this->urlPage);
        $form = $print->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
        }
         
    }
    
     public function formShowPredPrintAdminAction(){
      if($this->auth->isLogged){
        $print= new predPrint();
        $this->system->insert($this->urlPage);
        $form = $print->showFormAdmin($_POST['id'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
        }
         
    }
    
    
    
     public function removePredPrintAdminAction(){
      if($this->auth->isLogged){      
        $print= new predPrint();
        $db_old=serialize($print->getPrintResurs($_POST['id']));  
        $result = ($print->emptyResursAdmin($_POST['id']))?'ok':'false';
            if ($result=='ok'){
                $db_new=serialize($print->getPrintResurs($_POST['id']));  
                $this->system->insert($this->urlPage, $db_old, $db_new);
            }
            header('Location: '.$_POST['url'].'?delete='.$result);
        }else{
            $this->logoutAction();
      }     
    }
    

  #-------------------------------------------------------------------------------------------------------------
  //news-send
  //user-send
    public function newsSendUserAction(){
        if($this->auth->isLogged){
            $user = new newsSendUserEdit();
            $this->view->namePage=$this->lang['news_send_user_page'];
            $this->view->operation=$this->dopparams;
            $this->system->insert($this->urlPage);
            $el = new ElementUser( new ConstructorNewsSendUser($user, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getUser($this->params[0]);
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    
    
     public function addSendUserAction(){
      if($this->auth->isLogged){ 
         $user = new newsSendUserEdit();  
         $result = ($user->add($_POST))?'ok':'false';
        #-------------------
         if ($result=='ok'){
            $db_new=serialize($user->getUsers($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
          }
        #-------------------
        
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      } 
    }
    
    
    public function updateSendUserAction(){
      if($this->auth->isLogged){  
        $user = new newsSendUserEdit();
        $db_old=serialize($user->getUsers($_POST['id']));
        $result = ($user->update($_POST))?'ok':'false';
        
        if ($result=='ok'){
            $db_new=serialize($user->getUsers($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
        }
        
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }   
    }
    
    public function deleteSendUserAction(){
       if($this->auth->isLogged){   
        $user = new newsSendUserEdit();
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($user->getUsers($val));
            $this->system->insert($this->urlPage, $db_old, "");
        }
        
        $result = ($user->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
          $this->logoutAction();
      }     
    }
   
    public function formShowSendUserAction(){
      if($this->auth->isLogged){
        $user = new newsSendUserEdit();
         $this->system->insert($this->urlPage);
        $form = $user->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
        }
         
    }
    //rassilka-send
    public function rassilkaSendAction(){
        if($this->auth->isLogged){
            $ras = new rassilkaSendEdit();
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['rassilka_send_page'];
            $this->view->operation=$this->dopparams;
            $el = new ElementUser( new ConstructorRassilkaSend($ras, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getUser($this->params[0]);
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    
    public function addRassilkaAction(){
       if($this->auth->isLogged){    
        $ras = new rassilkaSendEdit();
        $result = ($ras->add($_POST))?'ok':'false';
        
        if ($result=='ok'){
            $db_new=serialize($ras->getRassilka($_SESSION['setting_gurnal_last_insert_id']));
            $this->system->insert($this->urlPage, "", $db_new);
         }
        header('Location: '.$_POST['url'].'?add='.$result);
      }else{
            $this->logoutAction();
      }  
    }
    
    public function updateRassilkaAction(){
      if($this->auth->isLogged){    
        $ras = new rassilkaSendEdit();
        $db_old=serialize($ras->getRassilka($_POST['id']));
        $result = ($ras->update($_POST))?'ok':'false';
         if ($result=='ok'){
            $db_new=serialize($ras->getRassilka($_POST['id']));
            $this->system->insert($this->urlPage, $db_old, $db_new);
         }
        header('Location: '.$_POST['url'].'?update='.$result);
       }else{
            $this->logoutAction();
      }  
    }
    
    public function deleteRassilkaAction(){
      if($this->auth->isLogged){      
        $ras = new rassilkaSendEdit();
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($ras->getRassilka($val));
            $this->system->insert($this->urlPage, $db_old, "");
        }
        $result = ($ras->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
            $this->logoutAction();
      }    
    }
    
     public function formShowRassilkaAction(){
      if($this->auth->isLogged){
        $ras = new rassilkaSendEdit();
        $this->system->insert($this->urlPage);
        $form = $ras->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
        }
         
    }
    
    //setting
    public function updateSettingRassilkaAction(){
        if($this->auth->isLogged){
            $ras = new rassilkaSendEdit();
            $this->view->namePage=$this->lang['rassilka_setting'];
            $this->view->operation=$this->dopparams;
            $this->system->insert($this->urlPage);
            $el = new ElementUser( new ConstructorRassilkaSetiing($ras, $this->lang));
            $this->createObject($el);   
            $this->view->include="profile.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    
    public function updateSettingRassilkaSaveAction(){
      if($this->auth->isLogged){  
        $ras = new rassilkaSendEdit();
        $db_old = serialize($ras->getRassulkaSetting());
        $result = ($ras->updateSetting($_POST))?'ok':'false';
        if ($result=='ok'){
            $db_new=serialize($ras->getRassulkaSetting());
            $this->system->insert($this->urlPage, $db_old, $db_new);
        }
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }  
    }
    
    public function sendRassilkaUserAction(){
        if($this->auth->isLogged){
           
            $rasId=$this->params[0];
            $ras = new rassilkaSendEdit();
            $this->system->insert($this->urlPage);
            $el = new ElementMenu( new ConstructorUserInRas($ras, $rasId, $this->lang));
            $this->createObject($el);
            
            $name=$ras->getRassilka($rasId);
            
            $this->view->namePage=$this->lang['rassilka_send_page'].' "'.$name['tema'].'"';
            $this->view->include="table2.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        
    }
    
    
     public function sendRassilkaAction(){
        if($this->auth->isLogged){
           
            $rasId=$this->params[0];
            $ras = new rassilkaSendEdit();
            $this->system->insert($this->urlPage);
            $el = new ElementMenu( new ConstructorRassilkaSendAction($ras, $rasId, $this->lang));
            $this->createObject($el);
            
            $name=$ras->getRassilka($rasId);
            $this->view->namePage=$this->lang['rassilka_send_page'].' "'.$name['tema'].'"';
            $this->view->include="profile.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);  
        
    }
    
#-------------------------------------------------------------------------------------------------------------
#lang redact
    #---------------------------------------------------------------------------
    public function redactSlovarAction(){
        if($this->auth->isLogged){
            $setting = new settingSystemAndUserEdit($this->lang);
            $this->view->namePage=$this->lang['setting_lang_redact'];
            $this->view->operation=$this->dopparams;
            $this->system->insert($this->urlPage);
            $el = new ElementSetting( new ConstructorLangVariables($setting, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getUser($this->params[0]);
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }    
    
    
     public function addLangVariablesAction(){
      if($this->auth->isLogged){  
        $setting = new settingSystemAndUserEdit($this->lang);
        $result = ($setting->addLibVariables($_POST, $this->dopparams))?'ok':'false';
       
        if ($result=='ok'){
            $db_new=serialize($_POST['key']."=>".$_POST['value']);
            $this->system->insert($this->urlPage, "", $db_new);
        }
        
        header('Location: '.$_POST['url'].'?file='.$this->dopparams['file'].'&add='.$result);
      }else{
            $this->logoutAction();
      }  
    }
    
    public function updateLangVariablesAction(){
       if($this->auth->isLogged){ 
        $setting = new settingSystemAndUserEdit($this->lang);
        $result = ($setting->updateLibVariables($_POST, $this->dopparams))?'ok':'false';
        if ($result=='ok'){
            $db_new=serialize($_POST['keyedit']."=>".$_POST['value']);
            $this->system->insert($this->urlPage, "", $db_new);
        }
        header('Location: '.$_POST['url'].'?file='.$this->dopparams['file'].'&update='.$result);
      }else{
            $this->logoutAction();
      }    
    }
   
    public function deleteLangVariablesAction(){
       if($this->auth->isLogged){   
        $setting = new settingSystemAndUserEdit($this->lang);
        $this->system->insert($this->urlPage);
        $result = ($setting->deleteLibVariables($_POST['id'], $this->dopparams))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?file='.$this->dopparams['file'].'&'.$actions.'='.$result);
        }else{
            $this->logoutAction();
      }  
    }
     
    
    public function formShowLangVariablesAction(){
      if($this->auth->isLogged){
        $setting = new settingSystemAndUserEdit($this->lang);
        $this->system->insert($this->urlPage);
        $form = $setting->showFormLibVariables($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang, $this->dopparams);
        print $form;
      }else{
            $this->logoutAction();
      }
         
    }
    #---------------------------------------------------------------------------
    #configSystem
    public function configSystemAction(){
        if($this->auth->isLogged){
            $setting = new settingSystemAndUserEdit($this->lang);
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['setting_sistem'];
            $this->view->operation=$this->dopparams;
            $el = new ElementSetting( new ConstructorConfigSistem($setting, $this->lang));
            $this->createObject($el);            
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getUser($this->params[0]);
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }    
            
#-------------------------------------------------------------------------------------------------------------
#feedback
     public function feedbackAction(){
        if($this->auth->isLogged){
            $feed = new feedbackEdit(); 
            $this->view->namePage=$this->lang['server_feedback'];
            $this->view->operation=$this->dopparams;
            $this->system->insert($this->urlPage);
            $el = new ElementSetting( new ConstructorFeedbackForm($feed, $this->lang));
            $this->createObject($el);
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    
    
     public function updateFeedbackAction(){
        if($this->auth->isLogged){ 
            $feed = new feedbackEdit();
            $db_old=serialize($feed->getMessage($_POST['id']));
            $result = ($feed->update($_POST))?'ok':'false';
            if ($result=='ok'){
                $db_new=serialize($feed->getMessage($_POST['id']));
                $this->system->insert($this->urlPage, $db_old, $db_new);
            }
            header('Location: '.$_POST['url'].'?update='.$result);
        }else{
            $this->logoutAction();
        } 
    }
   
    public function deleteFeedbackAction(){
      if($this->auth->isLogged){ 
        $feed = new feedbackEdit(); 
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($feed->getMessage($val));
            $this->system->insert($this->urlPage, $db_old, "");
        }
        $result = ($feed->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
            $this->logoutAction();
        }   
    }
     
    
    public function formShowFeedbackAction(){
      if($this->auth->isLogged){
        $feed = new feedbackEdit(); 
        $this->system->insert($this->urlPage);
        $form = $feed->showForm($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
      }
    }
#--------------------------------------------------------------------------------------------------------
#ban_email (spam)
    public function showBanEmailAction(){
        if($this->auth->isLogged){
            $feed = new feedbackEdit();
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['server_ban_email'];
            $this->view->operation=$this->dopparams;
            $el = new ElementSetting( new ConstructorBanEmailForm($feed, $this->lang));
            $this->createObject($el);
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    
     public function addBanEmailAction(){
      if($this->auth->isLogged){  
            $feed = new feedbackEdit(); 
            $result = ($feed->addBan($_POST))?'ok':'false';
            
            if ($result=='ok'){
                $db_new=serialize($feed->getBanEmail($_SESSION['setting_gurnal_last_insert_id']));
                $this->system->insert($this->urlPage, "", $db_new);
            }
            
            header('Location: '.$_POST['url'].'?add='.$result);
       }else{
            $this->logoutAction();
       }  
    }
    
    public function updateBanEmailAction(){
      if($this->auth->isLogged){     
        $feed = new feedbackEdit(); 
        $db_old=serialize($feed->getBanEmail($_POST['id']));
        $result = ($feed->updateBan($_POST))?'ok':'false';
        if ($result=='ok'){
            $db_new=serialize($feed->getBanEmail($_POST['id']));
            $this->system->insert($this->urlPage,$db_old, $db_new);
         }
        header('Location: '.$_POST['url'].'?update='.$result);
      }else{
            $this->logoutAction();
      }    
    }
   
    public function deleteBanEmailAction(){
      if($this->auth->isLogged){   
        $feed = new feedbackEdit(); 
        $arrayDel = explode(",", $_POST['id']);
          foreach($arrayDel as $key=>$val){
            $db_old=serialize($feed->getBanEmail($val));
            $this->system->insert($this->urlPage, $db_old, "");
        }
        $result = ($feed->deleteBan($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
      }else{
            $this->logoutAction();
       }  
    }
     
    
    public function formShowBanEmailAction(){
      if($this->auth->isLogged){
        $feed = new feedbackEdit();
        $this->system->insert($this->urlPage);
        $form = $feed->showFormBan($_POST['id'], $_POST['act'], $_POST['action'], $_POST['url'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
      }
    }
 #--------------------------------------------------------------------------------------------------------
 #arhivs
    public function showArhivsAction(){
        if($this->auth->isLogged){
            $arh = new arhivEdit(); 
            $this->view->namePage=$this->lang['server_arhivs'];
            $this->view->operation=$this->dopparams;
             $this->system->insert($this->urlPage);
            $el = new ElementSetting( new ConstructorArhivsForm($arh, $this->lang));
            $this->createObject($el);
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }  
    
    public function deleteArhivsAction(){
        $arh = new arhivEdit(); 
        $this->system->insert($this->urlPage);
        $result = ($arh->delete($_POST['id']))?'ok':'false';
        $actions=(strpos($_POST['id'], ",")>-1)?'deletes':'delete';
        header('Location: '.$_POST['url'].'?'.$actions.'='.$result);
    }
#-------------------------------------------------------------------------------
  public function systemGurnalAction(){
        if($this->auth->isLogged){
            Common::ChecksAccessRedactor();
            $sistem = new systemGurnalEdit(); 
            $this->system->insert($this->urlPage);
            $this->view->namePage=$this->lang['system_gurnal_name'];
            $this->view->operation=$this->dopparams;
            $el = new ElementSetting( new ConstructorSystemGurnal($sistem, $this->lang));
            $this->createObject($el);
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
   
  public function systemGurnalEmptyAction(){
    if($this->auth->isLogged){  
        Common::ChecksAccessRedactor();
        $sistem = new systemGurnalEdit(); 
        $this->system->insert($this->urlPage);
        $result = ($sistem->emptyGurnal())?'ok':'false';
        header('Location: '.$_POST['url'].'?emptyGurnal='.$result); 
     }else{
        $this->logoutAction();
     } 
  }
  
  public function formShowSystemGurnalAction(){
      if($this->auth->isLogged){
        Common::ChecksAccessRedactor();
        $sistem = new systemGurnalEdit();
        $this->system->insert($this->urlPage);
        $form = $sistem->showForm($_POST['id'], $this->lang);
        print $form;
      }else{
            $this->logoutAction();
      }
    }

#-------------------------------------------------------------------------------------------------------------
#serverConfig    
     public function configServerAction(){
        if($this->auth->isLogged){
            Common::ChecksAccessRedactor();
            $this->view->namePage=$this->lang['server_config'];
            $this->system->insert($this->urlPage);
            $el = new ElementUser( new ConstructorConfigServer($this->lang));
            $this->createObject($el);
            $this->view->include="phpinfo.php"; 
            $result = $this->view->render(ADMIN_TEMPLATE."/{$this->shablon}/inner.php");
            //$tmp = $user->getUser($this->params[0]);
        }else{
            $this->logoutAction();
        }
        $this->fc->setBody($result);     
    }
    
#-----------------------------------------------------------------------------------------------------------------------------------------------------------  
    private function createTopMenu(){
        $mainMenu = new mainMenu($this->lang);
        //$this->view->sp = $mainMenu->createSp();
        $this->view->mainMenuList= $mainMenu->mainMenuList();
        $this->view->fastAccess= $mainMenu->fastAccess();
        $this->view->configContorol = $mainMenu->configControl();
    }
    
    private function prepare(){
        $this->shablon = 'default';
        $this->params = $this->fc->getParams();
        $this->dopparams = $this->fc->getDopParams();
        $this->view = new view();  
        $this->auth = auth::instance();
        $this->auth->checkActive();
        $this->view->user = $_SESSION['user'];
        $this->view->isLogged = $this->auth->isLogged;
   }
   
   private function createObject($el){
       $this->view->thead = $el->getTableHead();
       $this->view->tbody = $el->getTableTbody($this->urlPage);
       $this->view->validate = $el->getValidateForm();
       $this->view->breadcrumbs = $el->getBreadCrumbs();  
   }
}

?>