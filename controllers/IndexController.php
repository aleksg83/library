<?
class IndexController { 
    private $fc, $db, $view, $params, $dopParams, $settings = array();
    public $lang;
    
    public function __construct() {
        $this->fc=FrontController::getInstance();   
        $this->view = new view();  
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->params = $this->fc->getParams(); 
        $this->dopParams = $this->fc->getDopParams();
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        $this->selectSettings();
    }
	
    public function IndexAction(){
        $menu = new Menu($this->fc->request());
        $this->view->menuTop = $menu->getTopMenu();  
        $this->view->menuLeft = $menu->getLeftMenu($this->params);
        $this->view->metatitle = $this->lang['portal.name']." : ".$this->lang['portal.main-title'];
        $this->view->metadescription = $this->lang['portal.main-descrition'];
        $this->view->metakeywords = $this->lang['portal.main-keywords'];
        $news = new News($this->params, $this->dopparams);
        $this->view->listnews=$news->getListNewsMainPage();
        $this->view->listmenumain=$menu->getMenuMainPage($this->settings['ListMenuMain'], $this->settings['ListMenuMainLimit']);
        $this->view->journal=$news->getListMainNewsDopSection($this->settings['IdVidNewsMain'], "journal");
        $this->view->notice=$news->getListMainNewsDopSection($this->settings['IdVidNewsMainRight'], "notice");
        $data = new BookData($this->params);
        $this->view->reader = $data->getListReaderData($this->params);
        $event= new Calendarj($this->params, $this->lang);
        $this->view->event = $event->getMainListEvents();
        $endMenu=new StaticPage($this->params, $this->dopparams);
        $this->view->endmenu= $endMenu->getListMainPageRazdel(Common::getValueisNameMainSystem("StaticMainPage")); 
        $rassulka = new exept();
        $this->view->rassulka_info = $rassulka->getRassulkaAboutAndSenderInfo(1);
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/index.php');
        $this->fc->setBody($result);
       
    }
   
    public function userActivateAction(){
        $this->view->metatitle=$this->lang['portal.user_activate'];
        $menu = new Menu("lib/");        
        $this->view->menuTop = $menu->getTopMenu(); 
        $this->view->menuLeft = $menu->getLeftMenu($this->params);
        $email=$this->dopParams['email'];
        $type=$this->dopParams['type'];
        $action=new exept();
        $this->view->namepage=$this->lang['portal.user_activate'];
        $this->view->content=$action->userActivate($this->dopParams['email'], $this->dopParams['type']);
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/static/index.php');
        $this->fc->setBody($result);
    }
    
     private function selectSettings(){
        $sth=$this->db->query("SELECT `name`, `value` FROM ".PREFIX."_main_setting");
        while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->settings[$row['name']] = $row['value'];
        }
    }
    
    
    
    
    #--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /*
      public function infoAction(){
        $this->static = new StaticPage($this->params, $this->dopParams); 
        $this->preparePage();   
        
        $info=$this->static->getPage();
        $this->view->namepage=$info['name'];
        $this->view->content=$this->static->parserPage($info['text']);
        $this->view->metatitle=$info['title'];
        $this->view->metadescription=$info['description'];
        $this->view->metakeywords=$info['keywords'];
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/static/index.php'); 
        $this->fc->setBody($result);
     }
    
    
    private function preparePage(){
        $tmp = explode("/", $this->fc->request());
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            $this->singleView = true; 
            $this->urlView = $tmp[count($tmp)-1];
        }  
        $menu = new Menu($this->fc->request());        
        $this->static->setRequest($menu->request);
        $this->view->menuTop = $menu->getTopMenu(); 
        $this->view->menuLeft = $menu->getLeftMenu($this->params[0]);
        $this->view->breadCrumbs = $menu->getBreadCrumbs();                      
    }
    */
}
?>