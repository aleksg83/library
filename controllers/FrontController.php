<?
class FrontController {
	protected $_controller, $_action, $_body, $_params, $_dopParams, $_request, $auth, $isViewSite;
	static $_instance;
	
	public static function getInstance(){
		if(!self::$_instance) self::$_instance = new self();
		return 	self::$_instance;
	}
	
	private function __construct(){
               $request = $_SERVER['REQUEST_URI'];
                              
               $this->auth = auth::instance(); 
               
               if (!$this->auth->isLogged){
                $this->isViewSite=( !$this->auth->isLogged && !Common::isBlock() ) ? true : false;
               }else{
                $this->isViewSite=( !$this->auth->isAdmin && !Common::isBlock()) ? true : false;   
                if ($this->auth->isAdmin || $this->auth->isRedactor)$this->isViewSite=true;
               }
                  
                foreach($_POST as $key => $value) {
                  if ( ($key!="text") && ($key!="materials")  && ($key!="comment") && ($key!="body")  ) 
                    $_POST[$key] = $this->sanitize($value);
                }    
                
                
		if(preg_match('%\?%s', $request)) list($request, $query_string) = explode('?', $request);
                              
                $this->_request = trim($request,'/');
                $splits = explode('/', trim($request,'/'));  
                $controller = array_shift($splits);
                $action = array_shift($splits);
                
                $this->_controller = !empty($controller) ? ucfirst($controller).'Controller':'IndexController';
		$this->_action = !empty($action) ? ucfirst($action).'Action':'IndexAction';
                                
		if(!empty($splits)){
                    foreach($splits as $val) { $this->_params[] = $val;}
		}	                
                if(!empty($query_string)){
                    $query_string = explode("&", $query_string);                    
                    foreach($query_string as $val){
                        $tmp = explode("=", $val);                        
                        $this->_dopParams[$tmp[0]] = $tmp[1];
                    }
                }
                #echo "<pre>"; print_r($_SERVER); echo "<pre>";
                #echo "<pre>"; print_r($this); echo "<pre>";
                //$_SERVER['REQUEST_URI']
                #exit;                
              
	}
	
	public function route(){
            if ( $this->isViewSite || ($this->_controller=="AdminController") ) {
              
               if(class_exists($this->getController())){
			$rc = new ReflectionClass($this->getController());
			if($rc->hasMethod($this->getAction())){
				$controller = $rc->newInstance();
				$method = $rc->getMethod($this->getAction());
				$method->invoke($controller);
			}else{
                            //throw new Exception('Wrong Action');
                            $ex = new exept();
                            $ex->show404();
			}	
		}else{
                        //throw new Exception('Wrong Controller'); 
                        $ex = new exept();
                        $ex->show404();
		}
            }else{
                $ex = new exept();
                $ex->blockSite();
            }    
	} 
	
	public function getParams(){
		return $this->_params;
	}
        
        public function getDopParams(){
		return $this->_dopParams;
	}
        
        public function request(){
		return $this->_request;
	}
        
        public function setRequest($request){
		$this->_request = $request;
	}
	
	public function getController(){
		return $this->_controller;
	}
	
	public function getAction(){
		return $this->_action;
	}
	
	public function getBody(){
		return $this->_body;
	}
	
	public function setBody($body){
                $this->_body = $body;
	}
        
    private function cleanInput($input) {
        $search = array(
          '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
          '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
          '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
          '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
        );

        $output = preg_replace($search, '', $input);
        return $output;
    }

    private function sanitize($input) {
        if (is_array($input)) {
            foreach($input as $var=>$val) {
                $output[$var] = $this->sanitize($val);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $input = stripslashes($input);
            }
            $output  = $this->cleanInput($input);
            #$output = mysql_real_escape_string($input);            
        }
        return $output;
    }
}
?>