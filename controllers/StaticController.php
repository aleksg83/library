<?php
class StaticController {
    private $fc, $view, $params, $dopParams;
    private $static;
    private $urlView;
    private $singleView;
    public $lang;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();  
        $this->params = $this->fc->getParams();  
        $this->dopParams = $this->fc->getDopParams();
        $this->static = new StaticPage($this->params, $this->dopParams);   
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        $this->preparePage();
    }
    
    
    public function IndexAction(){
        $info=$this->static->getPage();
        $this->view->namepage=$info['name'];
        $this->view->content=$this->static->parserPage($info['text']);
        $this->view->metatitle=$info['title'];
        $this->view->metadescription=$info['description'];
        $this->view->metakeywords=$info['keywords'];
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/static/index.php');
        $this->fc->setBody($result);
        
    }    
       
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function preparePage(){
        $tmp = explode("/", $this->fc->request());
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            $this->singleView = true; 
            $this->urlView = $tmp[count($tmp)-1];
        }  
        $menu = new Menu($this->fc->request());        
        $this->static->setRequest($menu->request);
        $this->view->menuTop = $menu->getTopMenu(); 
        $this->view->menuLeft = $menu->getLeftMenu($this->params[0]);
        $this->view->breadCrumbs = $menu->getBreadCrumbs();                      
    }
}

?>