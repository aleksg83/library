<?php
class SputnikController {
    private $fc, $view, $params, $sputnik;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();  
        $this->params = $this->fc->getParams();
        $this->sputnik = new Sputnik($this->params);
        $this->preparePage();
    }
    
    public function IndexAction(){ 
        $info=$this->sputnik->getPage();
        $this->view->namepage=$info['name'];
        $this->view->content=$this->sputnik->parserPage($info['text']);
        $this->view->metatitle=$info['title'];
        $this->view->metadescription=$info['description'];
        $this->view->metakeywords=$info['keywords'];
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/sputnik/index.php');
        $this->fc->setBody($result);
    }
    
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function preparePage(){
        $tmp = explode("/", $this->fc->request());
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            $this->urlView = $tmp[count($tmp)-1];
        }  
        $menu = new Menu($this->fc->request());
        $this->sputnik->setRequest($menu->request);  
        $this->view->menuTop = $menu->getTopMenu();  
        $this->view->menuLeft = $menu->getLeftMenu($this->params[0]);
        $this->view->breadCrumbs = $menu->getBreadCrumbs();
        
    }
}

?>