<?php
class IndexAjaxController {
    private $fc, $params, $dopparams, $db, $lang;
    
    public function __construct() {   
        $this->fc=FrontController::getInstance(); 
        $this->params = $this->fc->getParams();
        $this->dopparams = $this->fc->getDopParams(); 
        $this->auth = auth::instance();
        $this->auth->checkActive();
        $sql = new Sql();
        $this->db = $sql->connect();
        
        /*языковый файл с константами*/  
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
       
        /*url*/
        $this->urlPage=str_replace ("?".$_SERVER['QUERY_STRING'], "", $_SERVER['REQUEST_URI']);
        
    }
    
    public function userSubscribeAction(){
        $name = $_POST['data'][0]['value'];
        $email = $_POST['data'][1]['value'];
        $act=0;
        $list=0;
        if(!preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/", $email)){
            print "error_format_email";
        }else if ($name==""){
            print "error_format_name";
        }else{
            //проверка наличия email в базе
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_user_email WHERE email=?");
            $sth->bindParam(1, $email, PDO::PARAM_INT);
            $sth->execute();
            $count=$sth->rowCount();
            
            if ($count>0){
                print "error_count_name";
            }else{
                $sth = $this->db->prepare("INSERT INTO ".PREFIX."_user_email (name, email, is_active, is_black_list) VALUES (:name, :email, :is_active, :is_black_list)");
                $sth->bindParam(":name", $name, PDO::PARAM_STR);
                $sth->bindParam(":email", $email, PDO::PARAM_STR);
                $sth->bindParam(":is_active", $act, PDO::PARAM_INT);
                $sth->bindParam(":is_black_list", $list, PDO::PARAM_INT);
                $sth->execute();
                $result=($err[0] != '00000')?true:false;
                $err = $sth->errorInfo();
                if ($result) {
                    //отправка email c активацией подписки
                    $sth = $this->db->query("SELECT * FROM ".PREFIX."_rassilka_setting WHERE id_rassilka_setting='1'");
                    $setting = $sth->fetch(PDO::FETCH_ASSOC); 
                    $nameAuthor=$setting['name_author'];  //адресат
                    $emailFrom=$setting['adress_author']; //email адресата
                    require_once(DROOT.'/classes/PHPMailer/class.phpmailer.php');
                    $mail = new PHPMailer();
           
                    $mail->SMTPAuth = FALSE;   
                    $mail->From     = $emailFrom; // from address (default root@localhost)
                    $mail->FromName = $nameAuthor;    // from name (default 'root user')
                    $mail->AddAddress($email, $name);
                    $mail->Subject = $this->lang['portal.sender_theme']; 
                    $mail->CharSet= "UTF-8";
                    $body="<p>".$this->lang['portal.sender_body_start']." ".$name.", <br/>";
                    $linkActivate='<a href="'.SITE_URL.'index/userActivate?email='.$email.'&type=1">'.SITE_URL.'index/userActivate?email='.$email.'&type=1</a>';
                    $body.=$this->lang['portal.sender_body_next']." ".$linkActivate.".</p>";
                    $body.='<p>'.$this->lang['portal.sender_body_end'].'.</p>';
                    $mail->Body=$body;
                    $mail->IsHTML(true);
                    
                    if(!$mail->Send())
                    {
                        print "no_send";
                    }else{    
                         print "ok_add";
                    }     
                    $mail->ClearAddresses(); 
                }else{
                    print "no_add";
                }    
            }
        }
     
    }
    
     public function userActivateWihtSexAction(){
        $email = $_POST['email'];
        $sex = ($_POST['sex'] == 'men') ? 1 : 0 ;
      
        $sth = $this->db->prepare("UPDATE ".PREFIX."_user_email SET is_active=1, sex=? WHERE email=?");        
                $sth->bindParam(1, $sex, PDO::PARAM_INT);
                $sth->bindParam(2, $email, PDO::PARAM_STR);
                $sth->execute();        
                $err = $sth->errorInfo();
                if ($err[0] != '00000'){
                    print "no";
                }else{
                    print "ok";
                }
     }   
    
    #---------------------------------------------------------------------------
     public function userFeedbackAction(){
        $name = $_POST['data'][0]['value'];
        $email = $_POST['data'][1]['value'];
        $message = $_POST['data'][2]['value'];
        if(!preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/", $email)){
            print "error_format_email";
        }else if ($name==""){
            print "error_format_name";
        }else{
           //проверка адреса в бан-базе адресов
           $sth = $this->db->query("SELECT * FROM ".PREFIX."_ban_email WHERE email='{$email}'");
           if ($sth->rowCount()>0){
                print "no_add"; //адрес забанен, не отправляем
           }else{
            $status=1;
            $sth = $this->db->prepare("INSERT INTO ".PREFIX."_feedback (name, email, message, date_add, status) VALUES (:name, :email, :message, :date_add, :status)");
                $sth->bindParam(":name", $name, PDO::PARAM_STR);
                $sth->bindParam(":email", $email, PDO::PARAM_STR);
                $sth->bindParam(":message", $message, PDO::PARAM_STR);
                $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
                $sth->bindParam(":status", $status, PDO::PARAM_INT);
                $sth->execute();
                $result=($err[0] != '00000')?true:false;
                if ($result) {
                    #-----------------------------------------------------------
                    $emailAdmin=  Common::getValueisNameMainSystem("emailAdmin");
                    if (preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/", $emailAdmin)){
                     
                        require_once(DROOT.'/classes/PHPMailer/class.phpmailer.php');
                        $mail = new PHPMailer();
           
                        $mail->SMTPAuth = FALSE;   
                        $mail->From     = $email; // from address (default root@localhost)
                        $mail->FromName = $name ;    // from name (default 'root user')
                        $mail->AddAddress($emailAdmin, $this->lang['portal.feedback_admin']);
                        $mail->Subject = $this->lang['portal.feedback_theme']; 
                        $mail->CharSet= "UTF-8";
                        $mail->Body=$message;
                        $mail->IsHTML(true);
                    
                        if(!$mail->Send()){
                            print "no_send";
                        }else{    
                            print "ok_add";
                        }     
                        $mail->ClearAddresses();
                        
                    }else{
                      print "ok_add";
                    }  
                    #-----------------------------------------------------------
                }else{
                    
                     print "no_add";
                }
            
           }       
        }
     }
  
}

?>