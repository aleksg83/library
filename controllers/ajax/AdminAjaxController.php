<?php
class AdminAjaxController {
    private $fc, $params, $dopparams, $auth, $db, $lang, $system;
    
    public function __construct() {   
        $this->fc=FrontController::getInstance(); 
        $this->params = $this->fc->getParams();
        $this->dopparams = $this->fc->getDopParams(); 
        $this->auth = auth::instance();
        $this->auth->checkActive();
        $sql = new Sql();
        $this->db = $sql->connect();
        
        /*языковый файл с константами*/  
        lng::$file='admin'; 
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        
        $this->system=new systemGurnalEdit();
        /*url*/
        $this->urlPage=str_replace ("?".$_SERVER['QUERY_STRING'], "", $_SERVER['REQUEST_URI']);
        
        if(!$this->auth->isLogged) exit;
        if (!$this->auth->isActive) exit;
    }
    
    public function setIsVisibleAction(){
        $sth = $this->db->prepare("UPDATE ".PREFIX."_".$_POST['table']." SET ".$_POST['nameColumn']."=? WHERE ".$_POST['id_rec_name']."=?");
        $sth->bindParam(1, $_POST['valueRec'], PDO::PARAM_INT);
        $sth->bindParam(2, $_POST['id'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        
        $serilArray=array("table" => $_POST['table'], "id" => $_POST['id'], "column" => $_POST['nameColumn'], "id_rec"=>$_POST['id_rec_name'], "value"=>$_POST['valueRec']);
        $db_new=serialize($serilArray);
        $this->system->insert($this->urlPage, "", $db_new);
        
        return ($err[0] != '00000')?false:true;
    }   
    
    #------------------------------------------------------------------------------------------------------
     public function updateMenuNodeAction(){
        //родитель и уровень не менялись
        //--------------------------------type=1------------------------------------------ 
        if ($_POST['typeoperation']=="1"){
            
         
            $strArr =  str_replace('\"', '"', $_POST['arrayPostId']);
            $arrayPostIdPos = json_decode($strArr);
            
            for ($i=0; $i<count($arrayPostIdPos); $i++){
                
                $id=$arrayPostIdPos[$i]->id;
                $position=$arrayPostIdPos[$i]->pos;
                
                $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET position=? WHERE id_menu=?");
                $sth->bindParam(1, $position, PDO::PARAM_INT);
                $sth->bindParam(2, $id, PDO::PARAM_INT);
                $sth->execute();
               // $err = $sth->errorInfo(); 
                
                $serilArray=array("table" => "menu", "id_menu" => $id, "column" => "position", "value"=>$position);
                $db_new=serialize($serilArray);
                $this->system->insert($this->urlPage, "", $db_new);
            }
          
         //--------------------------------type=1------------------------------------------   
         } else  if ($_POST['typeoperation']=="2"){
             
            //обновляем старую ветку и новую ветки, массив приходит объединенный
            $strArr =  str_replace('\"', '"', $_POST['arrayPostIdPos']);
            $arrayPostIdPos = json_decode($strArr);
           
            for ($i=0; $i<count($arrayPostIdPos); $i++){
                
                $id=$arrayPostIdPos[$i]->id;
                $position=$arrayPostIdPos[$i]->pos;
                
                $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET position=? WHERE id_menu=?");
                $sth->bindParam(1, $position, PDO::PARAM_INT);
                $sth->bindParam(2, $id, PDO::PARAM_INT);
                $sth->execute();
               // $err = $sth->errorInfo(); 
                
                 $serilArray=array("table" => "menu", "id_menu" => $id, "column" => "position", "value"=>$position);
                $db_new=serialize($serilArray);
                $this->system->insert($this->urlPage, "", $db_new);
               
            }
          
            //обновить сам узел и в зависимости от флага детей
            $strNode =  str_replace('\"', '"', $_POST['nodePost']);
            $node = json_decode($strNode);
            
            $idNode=$node[0]->id;
            $positionNode=$node[0]->pos;
            $parentNode=$node[0]->parent;
            $SparentNode=$node[0]->sparent;
            $level=$node[0]->level;
            $updateNodechild=$node[0]->flag;
           
            $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET id_menu_top=?, level=?, position=? WHERE id_menu=?");
            $sth->bindParam(1, $parentNode, PDO::PARAM_INT);
            $sth->bindParam(2, $level, PDO::PARAM_INT);
            $sth->bindParam(3, $positionNode, PDO::PARAM_INT);
            $sth->bindParam(4, $idNode, PDO::PARAM_INT);
            $sth->execute(); 
            
             $serilArray=array("table" => "menu", "id_menu_top"=>$parentNode, "level"=>$level, "id_menu" => $idNode, "column" => "position", "value"=>$positionNode);
             $db_new=serialize($serilArray);
             $this->system->insert($this->urlPage, "", $db_new);
            
            //обновим уровень у потомков
            if ($updateNodechild=="1"){
                //level и idNode
               $isRubric=($node[0]->rubric=="1")? 1  : 0 ;
               $menu = new menuEdit($isRubric);
               $menu->updateLevelChild($idNode, $level); 
            }
            
            /*******************************************************************/
            //обновим url-узла и потомков
            #------------------------------------------
             #сначало url самого узла 
             $isRubric=($node[0]->rubric=="1")? 1  : 0 ;
             $menu = new menuEdit($isRubric);
             $nodeInfo=$menu->showMenu($idNode);
          
             //проверка старого узла на наличие детей -------
             if ($parentNode!=$SparentNode){
                
                    $tmp2=$menu->showMenu($SparentNode);
                    //проверка детей у старого родителя - если детей нет, надо убрать "/"
                    $count=$menu->getCountChild($SparentNode); 
         
                    if ($count==0){ //переносимый узел был единственный ребенок
                         $length = strlen($tmp2['url']);
                          if (substr($tmp2['url'], $length-1)=="/"){
                            $newurl=substr($tmp2['url'], 0, $length-1);
                            $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET url=? WHERE id_menu=?");
                            $sth->bindParam(1, $newurl, PDO::PARAM_STR);
                            $sth->bindParam(2, $tmp2['id_menu'], PDO::PARAM_INT);
                            $sth->execute();
                          }
                          
                          
                    }
            }#------
            
            #проверка уникальности
            while ($menu->getURLCount("edit", $nodeInfo['url_name'], $nodeInfo['id_menu_top'], $idNode)!="no"){
               mt_srand(time());
               $r = mt_rand(0,10);
               $nodeInfo['url_name']=$nodeInfo['url_name'].$r; 
            }
            
             
             $count=$menu->getCountChild($idNode); 
             if ($parentNode!="0"){
                 $info=$menu->showMenu($parentNode); 
                 $length = strlen($info['url']);
                 
                       #------------------
                       if (substr($info['url'], $length-1)!="/"){
                           $dop= ($count>0)? "/": "";
                           $url=$info['url']."/".$nodeInfo['url_name'].$dop;
                     
                           //----------------------------update
                           $newurl=$info['url']."/";
                           $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET url=? WHERE id_menu=?");
                           $sth->bindParam(1, $newurl, PDO::PARAM_STR);
                           $sth->bindParam(2, $parentNode, PDO::PARAM_INT);
                           $sth->execute();
                            //---------------------------update
               
                        } else {
                            $dop= ($count>0)? "/": "";
                            $url=$info['url'].$nodeInfo['url_name'].$dop;;
                        }
                        #------------------
                 
                 
             }else{
                  $dop= ($count>0)? "/": "";  
                  $url=$nodeInfo['url_name'].$dop;  
                 
             }
                $sthUp = $this->db->prepare("UPDATE ".PREFIX."_menu SET url=? WHERE id_menu=?");
                $sthUp->bindParam(1, $url, PDO::PARAM_INT);
                $sthUp->bindParam(2, $idNode, PDO::PARAM_INT);
                $sthUp->execute(); 
                #затем всех потомков
                $menu->updateUrl($idNode);
                
                $arrayNode=array("id_menu"=>$idNode, "url"=>$url); 
            #-----------------------------------------
            
            //соберем массив для возврата и обновления DOM
            $sthR = $this->db->prepare("SELECT id_menu, url FROM ".PREFIX."_menu WHERE (id_menu_top=? or id_menu=? or id_menu=? or id_menu=?) and is_rubricator=?");
            $sthR->bindParam(1, $idNode, PDO::PARAM_INT);
            $sthR->bindParam(2, $idNode, PDO::PARAM_INT);
            $sthR->bindParam(3, $parentNode, PDO::PARAM_INT);
            $sthR->bindParam(4, $SparentNode, PDO::PARAM_INT);
            $sthR->bindParam(5, $isRubric, PDO::PARAM_INT);
            $sthR->execute();
            $result = $sthR->fetchAll(PDO::FETCH_ASSOC);     
             
           // array_push($result, $arrayNodeSNode);  
           // array_push($result, $arrayNode);  
                
        }//typeOperation = 2 end
           
            if ($_POST['typeoperation']=="2") 
            {
               print json_encode($result);
            }else { 
               return null; 
                
            }   
    }
    #------------------------------------------------------------------------------------------------------
    public function updateMenuPositionAction(){
           $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET position=? WHERE id_menu=?");
           $sth->bindParam(1, $_POST['position'], PDO::PARAM_INT);
           $sth->bindParam(2, $_POST['id'], PDO::PARAM_INT);
           $sth->execute();
           
           $serilArray=array("table" => "menu", "id_menu"=>$_POST['id'], "column" => "position", "value"=>$_POST['position']);
           $db_new=serialize($serilArray);
           $this->system->insert($this->urlPage, "", $db_new);
     }
     #------------------------------------------------------------------------------------------------------ 
     
    #------------------------------------------------------------------------------------------------------
     public function setIsRubricInMenuAction(){
          
        //  print "id=".$_POST['id']." main=".$_POST['id_main']." rubric=".$_POST['rubric']." val=".$_POST['value'];
          
          //удаление связи
          if ($_POST['value']=="0"){
              $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_menu_rub WHERE id_menu=? and id_rub=?");
          //установка связи    
          }else{
              $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_menu_rub (id_menu, id_rub) values (?, ?)");
          }
          
           //рубрика подчиненная, меню главная
            if ($_POST['rubric']=="1"){
                 $sth->bindParam(1, $_POST['id_main'], PDO::PARAM_INT);
                 $sth->bindParam(2, $_POST['id'], PDO::PARAM_INT);
                 
                 $serilArray=array("table" => "rel_menu_rub", "id_menu"=>$_POST['id_main'], "id_rub" => $_POST['id']);
                   
            }else{
                 $sth->bindParam(1, $_POST['id'], PDO::PARAM_INT); 
                 $sth->bindParam(2, $_POST['id_main'], PDO::PARAM_INT);
                 
                 $serilArray=array("table" => "rel_menu_rub", "id_menu"=>$_POST['id'], "id_rub" => $_POST['id_main']);
            }
            $sth->execute();     
            $err = $sth->errorInfo();
           
            $db_new=serialize($serilArray);
            
            $act=($_POST['value']=="0")? $this->system->insert($this->urlPage, $db_new, "") : $this->system->insert($this->urlPage, "", $db_new);
            
            print ($err[0] != '00000')?"no":"ok";
              
       }
     #------------------------------------------------------------------------------------------------------
    
       
       
    #------------------------------------------------------------------------------------------------------
    //setRubricInMenu   
     public function setIsRubricInPersonAction(){
          
         // print "id=".$_POST['id']." person=".$_POST['id_person']." val=".$_POST['value'];
        
         
          //удаление связи
          if ($_POST['value']=="0"){
              $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_person_menu WHERE id_menu=? and id_person=?");
          //установка связи    
          }else{
              $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_person_menu (id_menu, id_person) values (?, ?)");
          }
          
           
           $sth->bindParam(1, $_POST['id'], PDO::PARAM_INT); 
           $sth->bindParam(2, $_POST['id_person'], PDO::PARAM_INT);
           
           $sth->execute();     
           $err = $sth->errorInfo();
           
           $serilArray=array("table" => "rel_person_menu", "id_menu"=>$_POST['id'], "id_person" => $_POST['id_person']);
           $db_new=serialize($serilArray);
           $act=($_POST['value']=="0")? $this->system->insert($this->urlPage, $db_new, "") : $this->system->insert($this->urlPage, "", $db_new); 
           
           print ($err[0] != '00000')?"no":"ok";
             
       }
     //setRubricInMenu 
     #------------------------------------------------------------------------------------------------------
      
       
     #------------------------------------------------------------------------------------------------------
    //setRubricInLib   
     public function setIsRubricInLibAction(){
          
         // print "id=".$_POST['id']." person=".$_POST['id_person']." val=".$_POST['value'];
        
         
          //удаление связи
          if ($_POST['value']=="0"){
              $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_menu WHERE id_menu=? and id_data=?");
          //установка связи    
          }else{
              $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_data_menu (id_menu, id_data) values (?, ?)");
          }
          
           
           $sth->bindParam(1, $_POST['id'], PDO::PARAM_INT); 
           $sth->bindParam(2, $_POST['id_data'], PDO::PARAM_INT);
           
           $sth->execute();     
           $err = $sth->errorInfo();
           
           $serilArray=array("table" => "rel_data_menu", "id_menu"=>$_POST['id'], "id_data" => $_POST['id_data']);
           $db_new=serialize($serilArray);
           $act=($_POST['value']=="0")? $this->system->insert($this->urlPage, $db_new, "") : $this->system->insert($this->urlPage, "", $db_new); 
           
           
           print ($err[0] != '00000')?"no":"ok";
             
       }
     //setRubricInLib
     #------------------------------------------------------------------------------------------------------  
       
    #------------------------------------------------------------------------------------------------------
    //setRubricInSpOrg  
     public function setIsRubricInSpOrgAction(){
          
     //   print "id=".$_POST['id']." sp_org=".$_POST['id_sp_org']." val=".$_POST['value'];
        
        
          //удаление связи
          if ($_POST['value']=="0"){
              $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_sp_organasation_menu WHERE id_menu=? and id_sp_organasation=?");
          //установка связи    
          }else{
              $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_sp_organasation_menu (id_menu, id_sp_organasation) values (?, ?)");
          }
          
           
           $sth->bindParam(1, $_POST['id'], PDO::PARAM_INT); 
           $sth->bindParam(2, $_POST['id_sp_org'], PDO::PARAM_INT);
           
           $sth->execute();     
           $err = $sth->errorInfo();
           
           $serilArray=array("table" => "rel_sp_organasation_menu", "id_menu"=>$_POST['id'], "rel_sp_organasation_menu" => $_POST['id_sp_org']);
           $db_new=serialize($serilArray);
           $act=($_POST['value']=="0")? $this->system->insert($this->urlPage, $db_new, "") : $this->system->insert($this->urlPage, "", $db_new); 
           
           
           print ($err[0] != '00000')?"no":"ok";
            
       }
     //setRubricInSpOrg 
     #------------------------------------------------------------------------------------------------------
       
     #------------------------------------------------------------------------------------------------------
    //setIsPersonInLib  
     public function setIsPersonInLibAction(){
    
          //print "id=".$_POST['id']." id_sp_type_person=".$_POST['id_sp_type_person']." id_data=".$_POST['id_data']." val=".$_POST['value'];
        
         //удаление связи
          if ($_POST['value']=="0"){
              $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_person WHERE id_data=? and id_person=? and id_sp_type_person=?");
          //установка связи    
          }else{
              $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_data_person (id_data, id_person, id_sp_type_person) values (?, ?, ?)");
          }
          
           $sth->bindParam(1, $_POST['id_data'], PDO::PARAM_INT); 
           $sth->bindParam(2, $_POST['id'], PDO::PARAM_INT);
           $sth->bindParam(3, $_POST['id_sp_type_person'], PDO::PARAM_INT);
           
           $sth->execute();     
           $err = $sth->errorInfo();
           
           $serilArray=array("table" => "rel_data_person", "id_data"=>$_POST['id_data'], "id_person" => $_POST['id'], "id_sp_type_person" => $_POST['id_sp_type_person']);
           $db_new=serialize($serilArray);
           $act=($_POST['value']=="0")? $this->system->insert($this->urlPage, $db_new, "") : $this->system->insert($this->urlPage, "", $db_new); 
           
           
           print ($err[0] != '00000')?"no":"ok";
            
       }
     //setIsPersonInLib 
     #------------------------------------------------------------------------------------------------------
     
       
       
    #------------------------------------------------------------------------------------------------------
    //getLoginUniq  
     public function setIsUserInRolesAction(){
        
        //print "id=".$_POST['id']." id_sp_type_person=".$_POST['id_sp_type_person']." id_data=".$_POST['id_data']." val=".$_POST['value'];
        
         //удаление связи
          if ($_POST['value']=="0"){
              $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_users_roles WHERE id_users=? and id_roles=?");
          //установка связи    
          }else{
              $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_users_roles (id_users, id_roles) values (?, ?)");
          }
          
           $sth->bindParam(1, $_POST['id_users'], PDO::PARAM_INT); 
           $sth->bindParam(2, $_POST['id'], PDO::PARAM_INT);
           
           $sth->execute();     
           $err = $sth->errorInfo();
           
           $serilArray=array("table" => "rel_users_roles", "id_users"=>$_POST['id_users'], "id_roles" => $_POST['id']);
           $db_new=serialize($serilArray);
           $act=($_POST['value']=="0")? $this->system->insert($this->urlPage, $db_new, "") : $this->system->insert($this->urlPage, "", $db_new); 
           
           
           print ($err[0] != '00000')?"no":"ok";
     }   
     
     //-------------------------------------------------------------------------
    //sendRassulka 
     public function sendActionPacetsAction(){
         error_reporting(0);
             
         $nameSender=$_POST['name'];  //адресат
         $emailSender=$_POST['email']; //email адресата
         $sex=$_POST['sex']; //пол адресата
        
         //письмо
         $ras=new rassilkaSendEdit();
         
         $setting=$ras->getRassulkaSetting();
         $nameAuthor=$setting['name_author'];  //адресат
         $emailFrom=$setting['adress_author']; //email адресата
         if ( ($setting['appeal_to_men']=="") && ($setting['appeal_to_women']=="") ){
            $welcome="";
         }else{
            $welcome = ($sex == "1") ? '<p>'.$setting['appeal_to_men'].' '.$nameSender.'!</p>' : '<p>'.$setting['appeal_to_women'].' '.$nameSender.'!</p>'; 
         }   
         $is_html = intval($setting['is_HTML']);
         $endBody = $setting['end_letter'];
         
         $linkDezActivate='<p>'.$this->lang['rassilka_deactivate'].'<br/><a href="'.SITE_URL.'index/userActivate?email='.$emailSender.'&type=0">'.SITE_URL.'index/userActivate?email='.$emailSender.'&type=0</a></p>';
         $endBody.=$linkDezActivate;
                 
         $body = $ras->getRassilka($_POST['id']);
         
         $fileSend=false;
         if ( ($body['file'] != "") && file_exists(RASSILKA_FILE."/".$body['file']) ) {
            $file= RASSILKA_FILE.$body['file'];
            $fileSend=true;
         }
         
         require_once(DROOT.'/classes/PHPMailer/class.phpmailer.php');
         $mail = new PHPMailer();
           
         $mail->SMTPAuth = FALSE;   
         $mail->From     = $emailFrom; // from address (default root@localhost)
         $mail->FromName = $nameAuthor;    // from name (default 'root user')
         $mail->AddAddress($emailSender, $nameSender);
         $mail->Subject = $body['tema']; 
         $mail->CharSet= "UTF-8";
         
         if ($is_html==1) {
             $mail->IsHTML(true);
             $body['body']=str_replace("\n", "\r\n", $body['body']);
             $endBody=str_replace("\n", "\r\n", $endBody);
         }else{
             $body['body']=strip_tags($body['body']);
             $endBody=strip_tags($endBody);
         }
         
         $body['body'].=$endBody;
         
         $mail->Body = $welcome.$body['body'];
         
        if ($fileSend) $mail->AddAttachment($file, $body['file']); 
         
        //AddEmbeddedImage (string $path, string $cid, [string $name = ''], [string $encoding = 'base64'], [string $type = 'application/octet-stream']) 
        
        print "<p>- ".$this->lang['rassilka_mess1']." \"<b>".$nameSender."\" ".$emailSender."</b>";  
        if(!$mail->Send())
        {
            print ": ".$this->lang['rassilka_mess_error'].": " . $mail->ErrorInfo."</p>";
            
            $_SESSION['send_user_error']=$_SESSION['send_user_error']+1;
        }
        else 
        {
            print ": ".$this->lang['rassilka_mess_ok']."!</p>";
            $mail->ClearAddresses(); 
            if ($fileSend) $mail->ClearAttachments(); 
            
            //В случае повторной отправки, если запись уже есть, надо удалить.
            $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_rassilka_user_email WHERE id_rassilka=? and id_user_email=?");
            $sth->bindParam(1, $_POST['id'], PDO::PARAM_INT);
            $sth->bindParam(2, $_POST['idUser'], PDO::PARAM_INT);
            $sth->execute();
            
            //вставка записи в таблицу связи
            $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_rassilka_user_email (id_rassilka, id_user_email) VALUES ( :id_rassilka, :id_user_email)");
            $sth->bindParam(":id_rassilka", $_POST['id'], PDO::PARAM_INT);
            $sth->bindParam(":id_user_email", $_POST['idUser'], PDO::PARAM_INT);
            $sth->execute();  
            
            $_SESSION['send_user_ok']=$_SESSION['send_user_ok']+1;
        
        }
        
     }
     
     
     //getListUserSendAction  
     public function getListUserSendAction(){
        $user=new newsSendUserEdit();
        $listSender=$user->getUsersSender();
        print json_encode($listSender);
        
     }
     
     //setSendStatusAction
     public function setSendStatusAction(){
         $name=$_POST['status'];
         
         if ($name=="SEND"){
             $sqlDate=", date_send=:date_send";
         }
         
         $ras=new rassilkaSendEdit();
         $idStatus=$ras->getStatusId($name);
         
         $sth = $this->db->prepare("UPDATE ".PREFIX."_rassilka SET id_sp_status=:id_sp_status {$sqlDate} WHERE id_rassilka=:id_rassilka");        
         $sth->bindParam(":id_sp_status", $idStatus, PDO::PARAM_INT);
         if ($name=="SEND") {$sth->bindParam(":date_send", date("Y-m-d H:i:s"), PDO::PARAM_INT);} 
         $sth->bindParam(":id_rassilka", $_POST['id'], PDO::PARAM_INT);
         $sth->execute();        
         $err = $sth->errorInfo();
         
         $dop="<p>".$this->lang['rassilka_send_user_ok']." - <b>".$_SESSION['send_user_ok']."</b>, ".$this->lang['rassilka_send_user_error']." - <b>".$_SESSION['send_user_error']."</b></p>";
         
         if ($err[0] == '00000') print $this->lang['rassilka_set_status_ok'].$dop;
         else print $this->lang['rassilka_set_status_error'].$dop;
         
     }
     
     
     //-------------------------------------------------------------------------
       public function setBlockSiteAction(){
       
          $sth = $this->db->prepare("UPDATE ".PREFIX."_main_setting SET value=? WHERE name='BlockSaite'");
          $sth->bindParam(1, $_POST['value'], PDO::PARAM_INT);
          $sth->execute();
     
          $err = $sth->errorInfo();
          $result = ($err[0] != '00000')?0:1; 
          
          $serilArray=array("table" => "main_setting", "code"=>"BlockSaite", "value" => $_POST['value']);
          $db_new=serialize($serilArray);
          $this->system->insert($this->urlPage, $db_new, ""); 
           
          
          print $result; 
       }
      
    #------------------------------------------------------------------------------------------------------
    public function setSettingValueAction(){
           $sth = $this->db->prepare("UPDATE ".PREFIX."_main_setting SET value=? WHERE id_main_setting=?");
           $sth->bindParam(1, $_POST['value'], PDO::PARAM_INT);
           $sth->bindParam(2, $_POST['id'], PDO::PARAM_INT);
           $sth->execute();
           $err = $sth->errorInfo();
           $result = ($err[0] != '00000')?0:1; 
           
           $serilArray=array("table" => "main_setting", "id_main_setting"=>$_POST['id'], "value" => $_POST['value']);
           $db_new=serialize($serilArray);
           $this->system->insert($this->urlPage, $db_new, ""); 
           
           print $result; 
     }
    #------------------------------------------------------------------------------------------------------ 
     public function addArhivFileOrDBAction(){
         $arh = new arhivEdit(); 
         if ($_POST['type']=="zip"){
             $result=$arh->addFileArhiv();
         }else{
             $result=$arh->addFileDb();
         }
         
         $serilArray=array("action" => "arhiv", "type"=>$_POST['type']);
         $db_new=serialize($serilArray);
         $this->system->insert($this->urlPage, $db_new, ""); 
         
         print $result;
     } 
    #------------------------------------------------------------------------------------------------------ 
     public function updateSiteMapAction(){
         $menu = new menuEdit(0); 
         $result=$menu->updeteSiteMapXML();
         
         $serilArray=array("action" => "updateSiteMap");
         $db_new=serialize($serilArray);
         $this->system->insert($this->urlPage, $db_new, ""); 
         
         print $result;
     }  
    #------------------------------------------------------------------------------------------------------ 
}

?>