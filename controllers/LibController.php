<?
class LibController { 
    private $fc;
    private $view;
    private $params;
    private $menuRequest;
    private $dopParams;
    private $singleView;
    private $urlView; 
    private $menu;
    private $countPerPage;
    public $lang;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();          
        $this->view->tm = false;
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        $this->prepare();
    }
	
    public function IndexAction(){      
        $this->view->metatitle=$this->lang['portal.lib_meta_title'];
        $this->view->metadescription=$this->lang['portal.lib__meta_description'];
        $this->view->metakeywords=$this->lang['portal.lib_meta_keywords'];
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/index.php');
        $this->fc->setBody($result);
    }
    
    public function BookAction(){        
        $this->view->type = trim(urldecode($_SERVER['REQUEST_URI']), "/");
        $rubrs = array_pop(explode($this->menuRequest, $this->fc->request()));
        $object = new BookData($this->dopParams);    
        $this->renderPage($object, $rubrs);
    }
    
    public function ajaxListAction(){
        $this->countPerPage = $this->dopParams['limit'];
        $this->view->type = trim(urldecode($this->dopParams['url']), "/");  
        $rubrs = explode("/", trim(substr(urldecode($this->dopParams['url']), 5), "/"));
        $class = ucfirst(array_shift($rubrs)).'Data';
        $object = new $class($this->dopParams);
        $this->ajaxRenderPage($object, $rubrs);
    }
    
    public function RevenuesAction(){
        $object = new RevenuesData($this->dopParams);
        switch ($_GET['type']){
            case 'book': $name="Книги"; break;
            case 'article': $name="Статьи"; break;
            case 'filmstrip': $name="Диафильмы"; break;
            case 'thesis': $name="Диссертационные материалы"; break;
            case 'periodic': $name="Периодика"; break;
            default : $name="Книги"; break;
        }
        switch ($_GET['mode']){
                case '0': $name2="Все записи"; break;
                case '1': $name2="Полные тексты"; break;
                case '2': $name2="Фрагменты"; break;
                case '3': $name2="Описания"; break;
                default : $name2="Все записи"; break;
        }    
        
        $this->view->namePage=$name.", ".$name2;
        $this->view->metatitle= "Поступления: ".$this->view->namePage;
        if ($_GET['mode']=="false") $_GET['mode']=0;
        if ($_GET['type']=="false") $_GET['type']='book';
        
        $this->RenderPage($object, $rubrs);
    }
    
    public function ajaxRevenuesAction(){   
        $this->countPerPage = $this->dopParams['limit'];  
        $object = new RevenuesData($this->dopParams);
        $this->ajaxRenderPage($object, $rubrs);
    }
    
    public function FilmstripAction(){
        $rubrs = array_pop(explode($this->menuRequest, $this->fc->request()));
        $object = new FilmstripData($this->dopParams);        
        $this->renderPage($object, $rubrs);
    }        
    
    public function ArticleAction(){
        $rubrs = array_pop(explode($this->menuRequest, $this->fc->request()));
        $object = new ArticleData($this->dopParams);
        $this->renderPage($object, $rubrs);
    }
    
    public function ThesisAction(){
        $rubrs = array_pop(explode($this->menuRequest, $this->fc->request()));
        $object = new ThesisData($this->dopParams);    
        $this->view->tm = true;
        $this->renderPage($object, $rubrs);
    }    
    
    public function AuthorsAction(){        
        $person = new Person($this->menuRequest, $this->params, $this->dopParams);
        if($this->singleView){
            $person->setRequest($this->params[0]);
            $this->view->person = $person->getSinglePerson();
            $info=$person->getPersonalInfoPerson();
            $this->view->metatitle=$info['title'];
            $this->view->metadescription=$info['description'];
            $this->view->metakeywords=$info['keywords'];
            $url = ($info['is_in_encyclopedia']=="1") ? $person->getDopUrlPage().$info['url'] : "";
            $this->view->authorUrlPage=$url;
            $this->view->letter =  mb_substr($this->view->person['fio'], 0, 1, "UTF-8");
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/singleAuthor.php');
        }else{    
            $letter = (!$this->params['1'])?'all':urldecode($this->params['1']);
            $person->setLetter($letter);
            $this->view->letters = $person->createLetters();
            $this->view->persons = $person->getHTMLListPerson();
            $this->view->metatitle=$this->lang['portal.lib_author_title'];
            $countResurs = $person->getCountPerson();
            $setting['countOnPage'] = $person->countLimit(); 
            $paging = new Paging($countResurs, $setting, ($_GET['page'])?$_GET['page']:1);
            $this->view->paging = $paging->createPagingNav(); 
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/listAuthors.php');   
        }
        $this->fc->setBody($result);
    }  
    
    #---------------------------------------------------------------------------
    public function ViewerAction(){
         echo "Просмотрощик";
         
          $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/viewer/index.php');
          $this->fc->setBody($result); 
    }
    #---------------------------------------------------------------------------
    public function PeriodicalsAction(){
        $periodicals = new Periodicals($this->fc->request());
        $page= $periodicals->getTextContent($this->params);
        $this->view->content = ($page['text'])?$page['text']:'Нет статьи';
        $this->view->namePage = $page['name'];
        $this->view->metatitle=$page['title'];
        $this->view->metadescription=$page['description'];
        $this->view->metakeywords=$page['keywords'];
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/index.php');        
        $this->fc->setBody($result);        
    }
    
    public function DownloadAction(){
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($this->params[0]).'"');
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: '.filesize(constant($this->dopParams['mode']).basename($this->params[0]).'/'.$this->params[0]));
        ob_clean();
        flush();
        readfile(BOOK.basename($this->params[0]).'/'.$this->params[0]);
        exit;
    }
    
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function prepare(){
        $tmp = explode("/", $this->fc->request());
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            $this->singleView = true;
            $this->urlView = array_pop($tmp);
            $this->fc->setRequest(implode("/", $tmp));
        }
        $this->params = $this->fc->getParams();
        $this->dopParams = $this->fc->getDopParams();      
        $this->view->request = $this->fc->request();
       
        $this->menu = new Menu($this->fc->request());        
        $this->view->menuTop = $this->menu->getTopMenu();  
        $this->view->menuLeft = $this->menu->getLeftMenu($this->params);
        $this->view->rubricators = $this->menu->getRubricators();
        $this->view->content = $this->menu->getTextMenu();  
        $this->view->namePage = $this->menu->getNameMenu();  
        $this->view->breadCrumbs = $this->menu->getBreadCrumbs();        
        $this->view->menuRequest = $this->menuRequest = $this->menu->getMenuRequest();   
    }
    
    private function ajaxRenderPage(Data $object, $rubrs){        
        $this->prepareRenderPage($object, $rubrs); 
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/'.$object::typeListData.'.php');
        echo $result;
    }
    
    private function renderPage(Data $object, $rubrs){        
        if($this->singleView){
            $object->setUrl($this->urlView);
            $this->view->object = $object->getSingleData();        
            $this->view->metatitle=$this->view->object['title'];
            $this->view->metadescription=$this->view->object['description'];
            $this->view->metakeywords=$this->view->object['keywords'];
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/'.$object::singleData.'.php');
        }elseif($rubrs){  
            #$this->view->typeListData = $object::typeListData;
            $this->view->typeListData = $object::typeListData;
            $this->view->rubrType = (count($this->params))?'inner':'out';
            $this->prepareRenderPage($object, $rubrs); 
            $this->view->metatitle=$object->getRubrName();
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/'.$object::listData.'.php');
        }else{
            $this->view->typeListData = $object::typeListData;
            $this->view->rubrType = (count($this->params))?'inner':'out';
            $this->view->metatitle= ($this->view->metatitle!="")? $this->view->metatitle : $this->view->namePage;
            $this->prepareRenderPage($object, $rubrs); 
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/lib/'.$object::mainData.'.php');
        }                
        $this->fc->setBody($result);
    }
    
    private function prepareRenderPage(Data $object, $rubrs){        
        $rubr = (!is_array($rubrs))?array_pop(explode('/', $rubrs)):array_pop($rubrs);   
        $object->setRubr($rubr);
        $object->setRelRubs($this->menu->getRelatedRubr());
        $object->collectSetting();
        $this->view->title = $object->getRubrName().' ('.$object->getCountData().')';
        $this->view->objects = $object->getListData();
        #echo "<pre>"; print_r($this->view->objects); echo "</pre>";
        if ($this->dopParams['page']=='false') {$this->dopParams['page']=1; $_GET['page']=1;}
        $paging = new Paging($object->getCountData(), array("countOnPage"=>$this->countPerPage), ($_GET['page'])?$_GET['page']:1, $this->dopParams);
        $this->view->paging = $paging->createPagingNav(); 
    }
   
}    
?>