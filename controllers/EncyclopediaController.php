<?php
class EncyclopediaController {
    private $fc, $view, $params, $dopparams, $encyclopedia;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();  
        $this->params = $this->fc->getParams();
        $this->dopparams = $this->fc->getDopParams();
        $this->encyclopedia = new Encyclopedia($this->params, $this->dopparams);
        $this->preparePage();
    }
    
    public function IndexAction(){   
       $info=$this->encyclopedia->getPage();
       $this->view->namepage=$info['name'];
       $this->view->content=  stripcslashes($info['text']);
       
       #------------------------------------------
       $this->view->calendar=$info['calendarj'];
       $this->view->period=$info['period'];
       #------------------------------------------
       $this->view->letter=$info['letter'];
       $this->view->listperson=$info['listperson'];
       $this->view->start=$info['start'];
       $this->view->photo=$info['photo'];
       $this->view->nolistukazatel=$info['nolistukaz'];
       #------------------------------------------
       $paging = new Paging($info['countResurs'], $info['countAllResurs'], ($_GET['page'])?$_GET['page']:1);
       $this->view->paging = $paging->createPagingNav(); 
       
       $this->view->metatitle=$info['title'];
       $this->view->metadescription=$info['description'];
       $this->view->metakeywords=$info['keywords'];
        
       $template = ($info['template']=="") ? "index.php" : $info['template'] ;
       $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/encyclopedia/'.$template.'');
       $this->fc->setBody($result);
    }
    
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function preparePage(){
        $menu = new Menu($this->fc->request());   
        $this->view->menuTop = $menu->getTopMenu(); 
        $this->encyclopedia->setRequest($menu->request);  
        $this->view->menuLeft = $menu->getLeftMenu($this->params[0]);
        $this->view->breadCrumbs = $menu->getBreadCrumbs();
    }
}

?>