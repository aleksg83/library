<?
class SearchController { 
    private $fc; 
    private $view;
    
    public function __construct() {
        $this->fc=FrontController::getInstance();
        $this->view = new view();  
        $this->prepare();
    }
    
    public function DataAction(){
        if($_GET['mode']) $search = new DataSearch($_GET['mode']);
        $this->search($search);
    }
    
    public function NewsAction(){
        $search = new NewsSearch();            
        $this->search($search);
    }
    
    public function StaticAction(){
        if($_GET['mode']) $search = new StaticSearch($_GET['mode']);            
        $this->search($search);
    }
    
    public function simpleAction(){
        $search = new SimpleSearch($_GET);
        $res = $search->search();
        $this->view->result = $res;
        #Common::pre($res);
        $this->view->historyQuery = SearchLog::getLastQuery();
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/search/index.php');
        $this->fc->setBody($result);
    }
	
    public function IndexAction(){
        $this->view->result = array();
        #Common::pre($_SESSION['query']);
        $this->view->historyQuery = SearchLog::getLastQuery();
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/search/index.php');
        $this->fc->setBody($result);
    }
    
    public function SearchfullAction(){
        $this->view->result = array();
        #Common::pre($_SESSION['query']);
        $this->view->historyQuery = SearchLog::getLastQuery();
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/search/searchfull.php');
        $this->fc->setBody($result);
    }    
 #----------------------------------------------------------------------------------------------------------------------------------------
    private function search(Search $search = null){
        $res = array();        
        if($search){
            $res = $search->search($_GET);            
            $this->view->cnt = $search->cnt;
            $paging = new Paging(count($res), array('countOnPage'=>$_GET['limit']), ($_GET['page'])?$_GET['page']:1);
            $this->view->paging = $paging->createPagingNav();            
        }
        #unset($_SESSION['query']);
        #Common::pre($_SESSION['query']);
        $this->view->historyQuery = SearchLog::getLastQuery();
        $this->view->result = $res;
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/search/searchfull.php');
        $this->fc->setBody($result);
    }    
    
    private function prepare(){
        $menu = new Menu($this->fc->request());
        $params = $this->fc->getParams();
        $this->view->menuTop = $menu->getTopMenu();  
        $this->view->menuLeft = $menu->getLeftMenu($params);          
        $news = new News(null, null);
        $this->view->organizaciya = $news->getOrganizaciya();
        $this->view->person = $news->getPerson();
        $this->view->obrazovanie = $news->getObrazovanie();
        $this->view->theme = $news->getTheme();        
    }
    
    
}
?>