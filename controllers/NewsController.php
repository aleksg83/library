<?php
class NewsController {
    private $fc, $view, $params, $dopParams;
    private $news;
    private $urlView;
    private $singleView;
    public $lang;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();  
        $this->params = $this->fc->getParams();  
        $this->dopParams = $this->fc->getDopParams();
        $this->news = new News($this->params, $this->dopParams);   
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        $this->preparePage();
    }
    
    public function IndexAction(){
            $paging = new Paging($this->news->getCountData(), $this->news->getSetting(), ($_GET['page'])?$_GET['page']:1);
            $this->view->paging = $paging->createPagingNav();
            $listNews = $this->news->getListNews();
            $this->view->listNews = ($listNews)?$listNews:null;
            $metainfo=$this->news->getMetaData($this->lang);
            $this->view->metatitle = $metainfo['metatitle'];
            $this->view->metadescription = $metainfo['metatitle'];
            $this->view->metakeywords = $metainfo['metatitle'];
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/news/index.php');
      
        $this->fc->setBody($result);
    }    
    
    public function TextAction(){
        if(!$this->singleView){
            $paging = new Paging($this->news->getCountData(), $this->news->getSetting(), ($_GET['page'])?$_GET['page']:1);
            $this->view->paging = $paging->createPagingNav();
            $listNews = $this->news->getListNews();
            $this->view->listNews = ($listNews)?$listNews:null;
            $metainfo=$this->news->getMetaData($this->lang);
            $this->view->metatitle = $metainfo['metatitle'];
            $this->view->metadescription = $metainfo['metatitle'];
            $this->view->metakeywords = $metainfo['metatitle'];
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/news/index.php');
        }else{
            $this->view->news = $this->news->getSingleNews($this->urlView);  
            $this->view->metatitle=$this->view->news['title'];
            $this->view->metadescription=$this->view->news['description'];
            $this->view->metakeywords=$this->view->news['keywords'];
            $this->view->relNews = $this->news->getRelatedNews($this->params[0]);
            $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/news/newsfull.php');
        }
        $this->fc->setBody($result);
    }    
    
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function preparePage(){
        $tmp = explode("/", $this->fc->request());
        if(strstr($tmp[count($tmp)-1], ".html") !== false){
            $this->singleView = true; 
            $this->urlView = $tmp[count($tmp)-1];
        }        
        $menu = new Menu($this->fc->request());        
        $this->news->setRequest($menu->request);
        $this->view->menuTop = $menu->getTopMenu(); 
        $this->view->theme = $menu->getLeftMenu($this->params[0]);
        $this->view->vid = $this->news->getVids();
        $this->view->breadCrumbs = $menu->getBreadCrumbs();                      
    }
}

?>