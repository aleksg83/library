<?php

class BookJournalController {
    private $fc, $view, $params;
    
    public function __construct() {
        $this->fc=FrontController::getInstance(); 
        $this->view = new view();  
        $this->params = $this->fc->getParams();
        $this->preparePage();
    }
    
    
    public function IndexAction(){        
        $object = new BookData($this->dopParams);
        $rubrs = $this->getRubs(1);
        $this->view->lastRub = $rubrs[count($rubrs)-1];
        $this->renderPage($object, $rubrs);
    }
    
    public function ajaxIndexAction(){   
        $this->countPerPage = $this->dopParams['limit'];  
        $object = new BookData($this->dopParams);
        $rubrs = $this->getRubs(0);        
        $this->ajaxRenderPage($object, $rubrs);
    }
    
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private function ajaxRenderPage(Data $object, $rubrs){        
        $this->prepareRenderPage($object, $rubrs); 
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/bookJournal/listData.php');
        echo $result;
    }
    
    private function renderPage(Data $object, $rubrs){
        $this->prepareRenderPage($object, $rubrs); 
        $result = $this->view->render(DROOT.MAIN_TEMPLATE.'/bookJournal/index.php');           
        $this->fc->setBody($result);
    }  
    
    private function prepareRenderPage(Data $object, $rubrs){        
        $rubr = (!is_array($rubrs))?array_pop(explode('/', $rubrs)):array_pop($rubrs);   
        $object->setRubr($rubr);
        $object->collectSetting();        
        $this->view->objects = $object->getListData();
        $this->view->title = $object->getRubrName().' ('.$object->getCountData().')';         
        $this->view->metatitle= $this->view->textMenuRubr;
        #Common::pre($object);
        $paging = new Paging($object->getCountData(), array("countOnPage"=>$this->countPerPage), ($_GET['page'])?$_GET['page']:1, $this->dopParams);
        $this->view->paging = $paging->createPagingNav(); 
    }
    
    private function preparePage(){
        $menu = new Menu($this->fc->request());        
        $this->view->menuTop = $menu->getTopMenu();  
        $this->view->menuLeft = $menu->getLeftMenu($this->params);
        $this->dopParams = $this->fc->getDopParams();
        #$this->view->content = $menu->getTextMenu();        
        $this->view->textMenuRubr = $menu->getTextMenuRubr($this->params);
        $this->view->breadCrumbs = $menu->getBreadCrumbs();
    }
    
    private function getRubs($mode){
        $url = ($mode)?$_SERVER['REQUEST_URI']:urldecode($this->dopParams['url']);
        if(strpos($url, "?") !== false){
            $tmp = trim(substr($url, 12));
            $tmp1 = explode("?", $tmp);
            return explode ("/", $tmp1[0]);
        }else{
            return explode("/", trim(substr($url, 12), "/"));
        }
    }
}

?>