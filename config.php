<?
    define("DROOT", $_SERVER['DOCUMENT_ROOT']);    
    define("SITE_URL", 'http://'.$_SERVER['SERVER_NAME'].'/');   
    define("ADMIN_ROOT", $_SERVER['DOCUMENT_ROOT'].'/admin'); 
    
    define("ADMIN_TEMPLATE", $_SERVER['DOCUMENT_ROOT'].'/admin/templates');    
    //define("MAIN_TEMPLATE", $_SERVER['DOCUMENT_ROOT'].'/templates/default');    
    define("MAIN_TEMPLATE", '/templates/default');    
    
    define("CSSJS", '/admin/templates');
    define("version", '0.1');
    
    define("PREFIX", "pharus");
    
    define("TMP", $_SERVER['DOCUMENT_ROOT'].'/tmp/');
    
    define("BOOK", $_SERVER['DOCUMENT_ROOT'].'/data/book/');
    define("BOOK_BASE", '/data/book/');
    define("ARTICLE", $_SERVER['DOCUMENT_ROOT'].'/data/article/');
    define("ARTICLE_BASE", '/data/article/');
    define("FILMSTRIP", $_SERVER['DOCUMENT_ROOT'].'/data/filmstrip/');
    define("FILMSTRIP_BASE", '/data/filmstrip/');
    define("THESIS_MATERIAL", $_SERVER['DOCUMENT_ROOT'].'/data/thesis_material/');
    define("THESIS_MATERIAL_BASE", '/data/thesis_material/');
    define("URL_NEWS", 'news/text/');
    define("URL_STATIC", 'static/index/');
    //define("URL_STATIC", 'index/info/');
    define("VIEWER", 'lib/viewer/');
    define("URL_ENCYCLOPEDIA", 'encyclopedia/index/');
    define("RASSILKA_FILE", $_SERVER['DOCUMENT_ROOT'].'/data/rassilka/');
?>