<? if(count($this->breadCrumbs) > 0) : ?>                              
    <div class="bredcrumbs">
        <a href="/"><?=$this->lang['portal.main_page']?></a> 
        <span class="r2">&rarr;</span>
        <? foreach($this->breadCrumbs as $key=>$breadCrumb) : ?>
            <? if($key != 0) : ?>
                <a href="/<?=$breadCrumb['url']?>/"><?=$breadCrumb['name']?></a> 
                <span class="r2">&rarr;</span>
            <? else : ?>   
                <span class="end"><?=$breadCrumb['name']?></span>
            <? endif ?>                                 
       <? endforeach ?>
    </div> 
<? endif ?>