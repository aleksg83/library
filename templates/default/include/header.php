<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$this->metatitle?></title>
<meta content="<?=$this->metadescription?>" name="description">
<meta content="<?=$this->metakeywords?>" name="keywords">
<link href="<?=MAIN_TEMPLATE?>/css/ui-lightness/jquery-ui-1.9.2.custom.css" rel="stylesheet">
<link href="<?=MAIN_TEMPLATE?>/css/style.css" rel="stylesheet" type="text/css">
<link href="<?=MAIN_TEMPLATE?>/css/colorbox.css" rel="stylesheet" type="text/css">
<link href="#" rel="stylesheet" type="text/css" id="average">
<link href="#" rel="stylesheet" type="text/css" id="big">
<script src="<?=MAIN_TEMPLATE?>/js/jquery-1.8.3.js" type="text/javascript"></script>
<script src="<?=MAIN_TEMPLATE?>/js/jquery.cycle.all.min.js" type="text/javascript"></script>
<script src="<?=MAIN_TEMPLATE?>/js/jquery-ui-1.9.2.custom.js" type="text/javascript"></script>
<script src="<?=MAIN_TEMPLATE?>/js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<script src="<?=MAIN_TEMPLATE?>/js/jquery.jslatex.js" type="text/javascript"></script>
<script src="<?=MAIN_TEMPLATE?>/js/addclear.js" type="text/javascript"></script>
<!--<script src="<?=MAIN_TEMPLATE?>/js/jquery.colorbox.js" type="text/javascript"></script>-->
<script src="<?=MAIN_TEMPLATE?>/js/up.js" type="text/javascript"></script>
<script src="<?=MAIN_TEMPLATE?>/js/img.js" type="text/javascript"></script>

<!--
<script type="text/javascript" src="<?=MAIN_TEMPLATE?>/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?=MAIN_TEMPLATE?>/js/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?=MAIN_TEMPLATE?>/js/fancybox/jquery.fancybox.css" media="screen" />
-->

<link rel="stylesheet" type="text/css" href="<?=MAIN_TEMPLATE?>/js/lightbox/lightbox.css" media="screen,tv" />
<script type="text/javascript" charset="UTF-8" src="<?=MAIN_TEMPLATE?>/js/lightbox/lightbox_plus.js"></script>



<script src="<?=MAIN_TEMPLATE?>/js/userfunc.js" type="text/javascript"></script>
<link href="<?=SITE_URL?>favicon.ico" rel="shortcut icon">
<link href="<?=SITE_URL?>favicon.ico" rel="shortcut icon" type="image/x-icon">
<link href="<?=SITE_URL?>rss.xml" title="<?=$this->lang['portal.logo_name']?>" type="application/rss+xml" rel="alternate">
</head>
<body>
    
   