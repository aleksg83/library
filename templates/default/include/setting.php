<?
    $twoBut=($this->objects[0]['id_sp_type_data']==3) ? " twobutton" : "";
    if ( strpos($_SERVER['REQUEST_URI'], "revenues") > 0 ){
         $twoBut=" twobutton";
    }
?>

<div class="setting<?=$twoBut?>">
<ul>
 <li> 
   <a href="javascript:void(0);" class="numb" rel="number" title="">25</a> 
   <div class="number">
      <p><?=$this->lang['displayed_on_the_page']?>:</p>
      <ul class="npage">
       <li><span class="check act" mode="none" count="10">10 <?=$this->lang['records_on_the_page']?> <input type="radio" name="page" checked="checked" /> </span></li>
       <li><span class="check"  mode="none" count="25">25 <?=$this->lang['records_on_the_page']?> <input type="radio" name="page" />  </span></li>
       <li><span class="check"  mode="none" count="50">50  <?=$this->lang['records_on_the_page']?> <input type="radio" name="page"/>  </span></li>
      </ul>                                    
   </div>
 </li> 
  <li>
    <a  href="javascript:void(0);" class="numb" rel="sort" title="">A-Я</a>
    <div class="sort">
      <p><?=$this->lang['sort']?>:</p>
      <ul class="nsort">
       <li><span class="check act" mode="none" count="A"><?=$this->lang['abs']?> <input type="radio" name="sort" checked="checked" /></span></li>
       <li><span class="check"  mode="none" count="T"><?=$this->lang['time']?> <input type="radio" name="sort" /></span></li>
     </ul>    
      <div class="line"></div>
     <button mode="asc" class="bsort"><?=$this->lang['up']?></button>                                     
   </div>      
  </li> 
  <? if( ($this->objects[0]['id_sp_type_data']!=3) ): ?>
  <li>   
   <a  href="javascript:void(0);" class="numb"  rel="view" title=""><img src="/templates/default/images/setting/sp.png"/></a> 
   <div class="view">
       <p><?=$this->lang['view_list']?>:</p>
      <ul class="nview">
          
       <?
          $img=($this->objects[0]['id_sp_type_data']==2) ? $this->lang['no_img_art'] : $this->lang['no_img'];
          $no_img=($this->objects[0]['id_sp_type_data']==2) ?  $this->lang['img_art'] : $this->lang['img'];
       ?>   
          
       <li><span class="check act" mode="none" count="list"><?=$img?> <input type="radio" name="sort" checked="checked" /></span> </li>
       <li><span class="check"  mode="none" count="icon"><?=$no_img?> <input type="radio" name="sort" /></span> </li>
      </ul>  
   </div>
  </li>     
  <? endif ?>
</ul>                       
</div>