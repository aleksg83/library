<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="loader"></div>
<div class="wrapper">
            <div class="all">               
            <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>    
            <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs.php'; ?>
               
            <div id="containerL">
	            <div id="innerL">
		              
                       <?include DROOT.MAIN_TEMPLATE.'/include/leftMenuCalendar.php'; ?>
                       
                        <div id="centerL"> 
                              <p class="zagolovok"><?=$this->namepage?></p>
                              
                              <? if ($this->period=="year") :?>
                                  <? if ($this->content!="") :?>
                                        <div class="text">
                                            <?=$this->content?>
                                        </div>
                                 <?endif;?>
                                 
                                  <? if (count($this->calendar)>0):?>  
                                    <p class="zag"><?=$this->lang['portal.calendar_date_no']?></p>
                                  <? endif; ?>
                                  
                                 <div class="doing">
                                 <? foreach ($this->calendar as $key => $month): ?>
                                     <? foreach ($month as $key2 => $day): ?>
                                         <? foreach ($day as $key3 => $object): ?>
                                            <div class="doingArea">
                                                <p><span class="c"></span>
                                                      <a href="<?=$object['url']?>"><?=$object['title']?><?=$object['years']?></a> <?=$object['date']?>
                                                </p>
                                                <p class="desc"><?=$object['text']?></p>     
                                            </div>
                                           <? endforeach;?> 
                                     <? endforeach;?>  
                                  <? endforeach;?>  
                                 </div>  
                              <?endif;?>   
                                    
                              
                                    
                            <? if ($this->period=="month") :?>       
                               
                              <div class="doing">
                                 <? foreach ($this->calendar as $key => $month): ?>
                                     <? foreach ($month as $key2 => $day): ?>
                                        <?$i=0;?>
                                          <? foreach ($day as $key3 => $object): ?>
                                             <div class="doingArea">
                                                <?  if ($i==0) : ?>
                                                     <? if ($key2==0 ) {?> <p class="name"><?=$this->lang['portal.calendar_date_no']?></p> <?}?>
                                                     <? if ($key2!=0 ) {?> <p class="name"><?=$key2." ".$object['monthr']?></p><?}?>
                                                <?$i++;?>
                                                <? endif; ?> 
                                                      
                                                <div class="padd"> 
                                                    <p><span class="c"></span>
                                                    <?if ($object['url']!=""){?> 
                                                        <a href="<?=$object['url']?>"><?=$object['title']?><?=$object['years']?></a> <?=$object['date']?>
                                                    <?}else{?>
                                                        <span class="nolink"><?=$object['title']?><?=$object['years']?><?=$object['date']?></span>
                                                    <?}?>
                                                  </p>
                                                <p class="pad"><?=$object['text']?></p> 
                                                </div> 
                                            </div>
                                           <? endforeach;?> 
                                    
                                     <? endforeach;?>  
                                  <? endforeach;?>  
                                 </div>
                             <?endif;?>  
                          
                        </div>
                       

                        <div class="clear"></div>
	           </div>
           </div>
       </div>
     <div class="push"></div>
  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
