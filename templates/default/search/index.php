<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<script>


//---------------------------------------------------------------------------------------------------------------------------
$(function() {

$.datepicker.setDefaults($.extend($.datepicker.regional['ru']));
$(".date").datepicker({
 changeMonth: true,
 changeYear: true,
 yearRange: '-5:+5',
});

//---------------------------------------------------------------------------------------------------------------------------

});
</script>
<body>
<div class="wrapper">
            <div class="all">
              <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>        
            <div id="containerR">
	            <div id="innerR">
		             <div id="right"> 
                        <div class="searchHistory">
                           <p class="zag">История запросов</p>
                           <? foreach($this->historyQuery as $key=>$value) : ?>
                                <div class="<?=(count($this->historyQuery)-1 == $key)?"areaBlock end":"areaBlock"?>">
                                  <a href="<?=$value['url']?>">
                                      <? foreach($value['params'] as $key1=>$value1) :?>
                                         <p><span><?=$key1?></span>  <?=$value1?></p>                                  
                                     <? endforeach ?>
                                  </a> 
                                </div>                           
                               <? endforeach ?>
                        </div>
                     </div>
                    <div id="centerR"> 
                        <p class="zagolovok">Простой поиск</p>
                        <a href="/search/searchfull" class="searchBlank"><span>Расширенный поиск</span> &rarr;</a>
                        <div class="formSerach">
                          <form action="/search/" method="POST" class="all" name="simpleSearch">
                           <table>
                            <tr>
                               <td><input type="text" name="search_field" value="<?=$_POST['search_field']?>" /></td>
                              <td><input type="submit" name="submit" value="Искать"/></td>
                             </tr>
                           </table>  
                          </form>                          
                        </div>                        
                        <div class="result">                        
                           <p class="zag">Результы поиска</p>                           
                           <div class="group">
                               <a href="javascript:void(0)" rel="spt" class="start">Спутник учителя (<?=count($this->result['sputnik'])?>)</a>
                               <a href="javascript:void(0)" rel="book">Книги (<?=count($this->result['book'])?>)</a>
                               <a href="javascript:void(0)" rel="art">Статьи (<?=count($this->result['article'])?>)</a>
                               <a href="javascript:void(0)" rel="diafilm">Диафильмы (<?=count($this->result['filmstrip'])?>)</a> 
                               <a href="javascript:void(0)" rel="dismaterial">Дис. материалы (<?=count($this->result['thesis'])?>)</a>
                               <a href="javascript:void(0)" rel="news" class="end">Новости (<?=count($this->result['news'])?>)</a>
                               <a href="javascript:void(0)" rel="encyclop" class="start">Энциклопедия (<?=count($this->result['encyclopedia'])?>)</a>
                               <a href="javascript:void(0)" rel="r1">Система координат (0)</a>
                               <a href="javascript:void(0)" rel="r2">Человек и Текст (0)</a>
                               <a href="javascript:void(0)" rel="r3">Исследователям (0)</a>
                               <a href="javascript:void(0)" rel="r4" class="end">О проекте (0)</a>                               
                           </div>                            
                           <div class="groupResult">                           
                               <div mode="none" rel="book"><!--class="act"-->
                            	    <ul class="lists">
                                        <? if($this->result['book']) : ?>
                                        <? foreach($this->result['book'] as $data) : ?>
                                            <li><a href="/lib/book/<?=$data['url']?>"><?=$data['short_author']?> <?=$data['short_name']?>. — <?=$data['year']?>.</a></li>
                                        <? endforeach ?>
                                        <? endif ?>
                                    </ul>
                             </div>
                              <div mode="none" rel="news">
                            	    <ul class="lists">
                                        <? if($this->result['news']) : ?>
                                        <? foreach($this->result['news'] as $data) : ?>
                                            <li><a href="/news/<?=$data['url']?>"><?=$data['short_name']?>.</a></li>
                                        <? endforeach ?>
                                        <? endif ?>
                                    </ul>
                             </div>
                           
                             <div mode="none" rel="art">
                            	    <ul class="lists">
                                        <? if($this->result['article']) : ?>
                                        <? foreach($this->result['article'] as $data) : ?>
                                            <li><a href="/lib/article/<?=$data['url']?>"><?=$data['short_author']?> <?=$data['short_name']?>. — <?=$data['year']?>.</a></li>
                                        <? endforeach ?>
                                        <? endif ?>
                                    </ul>
                             </div>
                             
                             <div mode="none" rel="diafilm">
                            	   <ul class="lists">
                                        <? if($this->result['filmstrip']) : ?>
                                        <? foreach($this->result['filmstrip'] as $data) : ?>
                                            <li><a href="/lib/filmstrip/<?=$data['url']?>"><?=$data['short_author']?> <?=$data['short_name']?>. — <?=$data['year']?>.</a></li>
                                        <? endforeach ?>
                                        <? endif ?>
                                    </ul>
                             </div>
                             
                             
                              <div mode="none" rel="dismaterial">
                            	    <ul class="lists">
                                        <? if($this->result['thesis']) : ?>
                                        <? foreach($this->result['thesis'] as $data) : ?>
                                            <li><a href="/lib/thesis_material/<?=$data['url']?>"><?=$data['short_author']?> <?=$data['short_name']?>. — <?=$data['year']?>.</a></li>
                                        <? endforeach ?>
                                        <? endif ?>
                                    </ul>
                             </div>
                             
                              <div mode="none" rel="encyclop">
                            	    <ul class="lists">
                                        <? if($this->result['encyclopedia']) : ?>
                                        <? foreach($this->result['encyclopedia'] as $data) : ?>
                                            <li><a href="/encyclopedia/<?=$data['url']?>"><?=$data['short_name']?>.</a></li>
                                        <? endforeach ?>
                                        <? endif ?>
                                    </ul>
                             </div>
                           </div>
                        </div>
                       </div> 
                      </div>
		              <div class="clear"></div>
	           </div>
           </div>
            </div>
            <div class="push"></div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
