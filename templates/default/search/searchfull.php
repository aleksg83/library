<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<script>
$(function() {
$.datepicker.setDefaults($.extend($.datepicker.regional['ru']));
$(".date2").datepicker({
 changeMonth: true,
 changeYear: true,
 yearRange: '-10:+1',
 monthNamesShort: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']});
});
</script>
<div class="wrapper">
    <div class="all">             
               <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>            
            <div id="containerR">
	            <div id="innerR">
                        <div id="right">                         
                            <div class="searchHistory">
                               <p class="zag">История запросов</p> 
                               <? foreach($this->historyQuery as $key=>$value) : ?>
                                <div class="<?=(count($this->historyQuery)-1 == $key)?"areaBlock end":"areaBlock"?>">
                                  <a href="<?=$value['url']?>">
                                      <? foreach($value['params'] as $key1=>$value1) :?>
                                         <p><span><?=$key1?></span>  <?=$value1?></p>                                  
                                     <? endforeach ?>
                                  </a> 
                                </div>                           
                               <? endforeach ?>
                            </div>                     
                        </div>
                       
		              <div id="centerR">                
                        <p class="zagolovok">Расширенный поиск</p>
                        
                        <a href="/search/" class="searchBlank"><span>Простой поиск</span> &rarr;</a>
                       <div class="searchPhilter" id="searchPhilter">
                              <a href="javascript:void(0)" rel="spt" <?=($_GET['vid'] == 'spt' || !$_GET['vid'])?'class="act"':''?> >Спутник учителя</a>
                              <a href="javascript:void(0)" rel="book" <?=($_GET['vid'] == 'book')?'class="act"':''?> >Книги</a>
                              <a href="javascript:void(0)" rel="art" <?=($_GET['vid'] == 'art')?'class="act"':''?> >Статьи</a>
                              <a href="javascript:void(0)" rel="diafilm" <?=($_GET['vid'] == 'diafilm')?'class="act"':''?> >Диафильмы</a>
                              <a href="javascript:void(0)" rel="dismat" <?=($_GET['vid'] == 'dismat')?'class="act"':''?> >Дис. материалы</a>
                              <a href="javascript:void(0)" rel="encyclopedia" <?=($_GET['vid'] == 'encyclopedia')?'class="act"':''?> >Энциклопедия</a>
                              <a href="javascript:void(0)" rel="news" <?=($_GET['vid'] == 'news')?'class="act"':''?> >Новости</a>
                              <a href="javascript:void(0)" rel="others" <?=($_GET['vid'] == 'others')?'class="act"':''?> >Другие разделы</a>
                              <a href="javascript:void(0)" class="close open"></a>
                        </div>	
                        
                        
                       <!-- sputnik -->   
                      <div <?=($_GET['vid'] == 'spt' || !$_GET['vid'])?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="spt">                         
                         <div class="formSerach">
                          <form action="/search/static" method="get" class="all twos" name="sptSearch">      
                            <input type="hidden" name="mode" value="sputnik">
                            <input type="hidden" name="vid" value="spt">
                            <table>
                            	<tr>
                                    <td style="width:610px;"><input type="text" name="sp_text" value="<?=htmlspecialchars($_GET['sp_text'])?>" /></td>
                                    <td><input type="submit" name="submit" value="Искать"/></td>
                                </tr>                                   
                                <tr>                                    
                                    <td colspan="2">                                     
                                        <table style="margin-top:6px;">
                                         <tr>
                                           <td style="width:80px;">Выводить по</td>
                                           <td>                                      
                                             <ul class="npageSeach4">
                                               <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                               <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                               <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                             </ul>   
                                           </td>
                                         </tr>
                                       </table>                                          
                                    </td>                             	                                    
                                 </tr>
                             </table>  
                           </form>
                         </div>                           
                      </div>  

                      <!-- book -->   
                      <div <?=($_GET['vid'] == 'book')?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="book">                        
                        <div class="formSerach">
                          <form action="/search/data" method="get" class="book" name="bookSearch">      
                            <input type="hidden" name="mode" value="1">
                            <input type="hidden" name="vid" value="book">
                            <table>
                                <tr>
                                    <td class="name">Автор</td>
                                    <td class="two"><input type="text" name="b_author" value="<?=htmlspecialchars($_GET['b_author'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Выводить по</td>
                                    <td> 
                                      <ul class="npageSeach">
                                        <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                        <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                        <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                      </ul>   
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="name">Заглавие</td>
                                    <td class="two"><input type="text" name="b_zaglavie" value="<?=htmlspecialchars($_GET['b_zaglavie'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Сортировать по</td>
                                    <td> 
                                      <ul class="npageSort">
                                        <li><span <?=($_GET['sort']=='author' || !$_GET['sort'])?'class="check act"':'class="check"'?> mode="none" count="A"><input type="radio" name="sort" <?=($_GET['sort']=='author' || !$_GET['sort'])?'checked="checked"':''?> value="author"/></span> авторам</li>
                                        <li><span <?=($_GET['sort']=='title')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Z"><input type="radio" name="sort" <?=($_GET['sort']=='title')?'checked="checked"':''?> value="title"/></span>  заглавиям </li>   
                                        <li><span <?=($_GET['sort']=='year')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Y"><input type="radio" name="sort" <?=($_GET['sort']=='year')?'checked="checked"':''?> value="year"/></span>  годам </li>
                                      </ul>
                                    </td>                           
                                 </tr>
                                <tr>
                                    <td class="name">Персона</td>
                                    <td class="two"><input type="text" name="b_person" value="<?=htmlspecialchars($_GET['b_person'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                           
                                </tr>
                                <tr>
                                    <td class="name">Год</td>
                                    <td class="two">   <input type="text" name="b_datestart" value="<?=($_GET['b_datestart'])?htmlspecialchars($_GET['b_datestart']):'От:'?>" class="date one" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'От:'}" code="int"/><input type="text" name="b_dateend" value="<?=($_GET['b_dateend'])?htmlspecialchars($_GET['b_dateend']):'До:'?>" class="date" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'До:'}" code="int"/></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                               	                                           
                                </tr>                                
                                <tr>
                                    <td class="name">Оглавление</td>
                                    <td class="two"><input type="text" name="b_oglavlenie" value="<?=htmlspecialchars($_GET['b_oglavlenie'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                            
                                </tr>
                                 <tr>
                                    <td class="name">Все поля</td>
                                    <td class="two"><input type="text" name="b_all" value="<?=htmlspecialchars($_GET['b_all'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2" class="rgt"><input type="reset" name="reset" value="Очистить"/> |                                         <input type="submit" name="submit" value="Искать"/></td>                            
                                 </tr>
                           	 </table>  
                           </form>
                         </div> 
                      </div>  
                      
                       <!-- art -->   
                      <div <?=($_GET['vid'] == 'art')?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="art">                         
                         <div class="formSerach">
                          <form action="/search/data" method="get" class="book" name="artSearch">
                           <input type="hidden" name="mode" value="2">
                           <input type="hidden" name="vid" value="art">
                              <table>
                            	<tr>
                                    <td class="name">Автор</td>
                                    <td class="two"><input type="text" name="a_author" value="<?=htmlspecialchars($_GET['a_author'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Выводить по</td>
                                    <td> 
                                     <ul class="npageSeach1">
                                        <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                        <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                        <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                     </ul>   
                                    </td>                             
                                 </tr>
                                 <tr>
                                    <td class="name">Заглавие</td>
                                    <td class="two"><input type="text" name="a_zaglavie" value="<?=htmlspecialchars($_GET['a_zaglavie'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Сортировать по</td>
                                    <td> 
                                      <ul class="npageSort1">
                                        <li><span <?=($_GET['sort']=='author' || !$_GET['sort'])?'class="check act"':'class="check"'?> mode="none" count="A"><input type="radio" name="sort" <?=($_GET['sort']=='author' || !$_GET['sort'])?'checked="checked"':''?> value="author"/></span> авторам</li>
                                        <li><span <?=($_GET['sort']=='title')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Z"><input type="radio" name="sort" <?=($_GET['sort']=='title')?'checked="checked"':''?> value="title"/></span>  заглавиям </li>   
                                        <li><span <?=($_GET['sort']=='year')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Y"><input type="radio" name="sort" <?=($_GET['sort']=='year')?'checked="checked"':''?> value="year"/></span>  годам </li>
                                       </ul>
                                    </td>                          
                                 </tr>
                                <tr>
                                    <td class="name">Персона</td>
                                    <td class="two"><input type="text" name="a_person" value="<?=htmlspecialchars($_GET['a_person'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                            
                                </tr>
                                <tr>
                                    <td class="name">Источник</td>
                                    <td class="two"><input type="text" name="a_source" value="<?=htmlspecialchars($_GET['a_source'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                            
                                </tr>
                                <tr>
                                    <td class="name">Год</td>
                                    <td class="two">   <input type="text" name="a_datestart" value="<?=($_GET['a_datestart'])?htmlspecialchars($_GET['a_datestart']):'От:'?>" class="date one" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'От:'}" code="int"/><input type="text" name="b_dateend" value="<?=($_GET['a_dateend'])?htmlspecialchars($_GET['a_dateend']):'До:'?>" class="date" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'До:'}" code="int"/></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                           
                                </tr>
                                 <tr>
                                    <td class="name">Все поля</td>
                                    <td><input type="text" name="a_all" value="<?=htmlspecialchars($_GET['a_all'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2" class="rgt"><input type="reset" name="reset" value="Очистить"/> |                                         <input type="submit" name="submit" value="Искать"/></td>                             
                                 </tr>
                           	 </table>  
                           </form>
                         </div> 
                      </div>  
                      
                      
                        <!-- diafilm -->   
                      <div <?=($_GET['vid'] == 'diafilm')?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="diafilm"> 
                        <div class="formSerach">
                          <form action="/search/data" method="get" class="book" name="diafilmSearch">
                           <input type="hidden" name="mode" value="4">
                           <input type="hidden" name="vid" value="diafilm">
                            	<table>
                            	<tr>
                                    <td class="name">Автор</td>
                                    <td class="two"><input type="text" name="d_author" value="<?=htmlspecialchars($_GET['d_author'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Выводить по</td>
                                    <td> 
                                     <ul class="npageSeach2">
                                        <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                        <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                        <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                     </ul>                                                       
                                    </td>                             	                                    
                                 </tr>                                                                  
                                 <tr>
                                    <td class="name">Заглавие</td>
                                    <td class="two"><input type="text" name="d_zaglavie" value="<?=htmlspecialchars($_GET['d_zaglavie'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Сортировать по</td>
                                    <td> 
                                      <ul class="npageSort2">
                                        <li><span <?=($_GET['sort']=='author' || !$_GET['sort'])?'class="check act"':'class="check"'?> mode="none" count="A"><input type="radio" name="sort" <?=($_GET['sort']=='author' || !$_GET['sort'])?'checked="checked"':''?> value="author"/></span> авторам</li>
                                        <li><span <?=($_GET['sort']=='title')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Z"><input type="radio" name="sort" <?=($_GET['sort']=='title')?'checked="checked"':''?> value="title"/></span>  заглавиям </li>   
                                        <li><span <?=($_GET['sort']=='year')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Y"><input type="radio" name="sort" <?=($_GET['sort']=='year')?'checked="checked"':''?> value="year"/></span>  годам </li>
                                     </ul>
                                    </td>                          
                                </tr>
                                <tr>
                                    <td class="name">Персона</td>
                                    <td class="two"><input type="text" name="d_person" value="<?=htmlspecialchars($_GET['d_person'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                               
                                 </tr>
                                 <tr>
                                    <td class="name">Оглавление</td>
                                    <td class="two"><input type="text" name="d_oglavlrnie" value="<?=htmlspecialchars($_GET['d_oglavlrnie'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>             
                                 </tr>
                                  <tr>
                                    <td class="name">Год</td>
                                    <td class="two">   <input type="text" name="d_datestart" value="<?=($_GET['d_datestart'])?htmlspecialchars($_GET['d_datestart']):'От:'?>" class="date one" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'От:'}" code="int"/><input type="text" name="d_dateend" value="<?=($_GET['d_dateend'])?htmlspecialchars($_GET['d_dateend']):'До:'?>" class="date" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'До:'}" code="int"/></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>             
                                 </tr>
                                 <tr>
                                    <td class="name">Все поля</td>
                                    <td class="two"><input type="text" name="d_all" value="<?=htmlspecialchars($_GET['d_all'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2" class="rgt"> <input type="reset" name="reset" value="Очистить"/> |                                         <input type="submit" name="submit" value="Искать"/></td>                             	                                    
                                 </tr>
                                </table>  
                           </form>
                         </div> 
                      </div>  
                      
                      
                        <!-- dismat -->   
                      <div <?=($_GET['vid'] == 'dismat')?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="dismat"> 
                        <div class="formSerach">
                          <form action="/search/data" method="get" class="book" name="diafilmSearch">
                           <input type="hidden" name="mode" value="3">
                           <input type="hidden" name="vid" value="dismat">
                            	<table>
                            	<tr>
                                    <td class="name">Автор</td>
                                    <td class="two"><input type="text" name="dis_author" value="<?=htmlspecialchars($_GET['dis_author'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Выводить по</td>
                                    <td> 
                                     <ul class="npageSeach3">
                                        <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                        <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                        <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                     </ul>   
                                   </td>                             
                                 </tr>
                                 <tr>
                                    <td class="name">Заглавие</td>
                                    <td class="two"><input type="text" name="dis_zaglavie" value="<?=htmlspecialchars($_GET['dis_zaglavie'])?>" /></td>
                                    <td class="r"></td>
                                    <td class="sort">Сортировать по</td>
                                    <td> 
                                      <ul class="npageSort3">
                                        <li><span <?=($_GET['sort']=='author' || !$_GET['sort'])?'class="check act"':'class="check"'?> mode="none" count="A"><input type="radio" name="sort" <?=($_GET['sort']=='author' || !$_GET['sort'])?'checked="checked"':''?> value="author"/></span> авторам</li>
                                        <li><span <?=($_GET['sort']=='title')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Z"><input type="radio" name="sort" <?=($_GET['sort']=='title')?'checked="checked"':''?> value="title"/></span>  заглавиям </li>   
                                        <li><span <?=($_GET['sort']=='year')?'class="check act"':'class="check"'?> class="check"  mode="none" count="Y"><input type="radio" name="sort" <?=($_GET['sort']=='year')?'checked="checked"':''?> value="year"/></span>  годам </li>
                                     </ul>
                                    </td>                       
                                 </tr>
                                 <tr>
                                    <td class="name">Науч. рук.</td>
                                    <td class="two"><input type="text" name="dis_ruk" value="<?=htmlspecialchars($_GET['dis_ruk'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                       
                                </tr>
                                 <tr>
                                    <td class="name">Персона</td>
                                    <td class="two"><input type="text" name="dis_person" value="<?=htmlspecialchars($_GET['dis_person'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                             
                                </tr>
                                 <tr>
                                    <td class="name">Год</td>
                                    <td class="two">   <input type="text" name="dis_datestart" value="<?=($_GET['dis_datestart'])?htmlspecialchars($_GET['dis_datestart']):'От:'?>" class="date one" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'От:'}" code="int"/><input type="text" name="dis_dateend" value="<?=($_GET['dis_dateend'])?htmlspecialchars($_GET['dis_dateend']):'До:'?>" class="date" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'До:'}" code="int"/></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                             
                                </tr>
                                 <tr>
                                    <td class="name">Оглавление</td>
                                    <td class="two"><input type="text" name="dis_oglavlenie" value="<?=htmlspecialchars($_GET['dis_oglavlenie'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                               
                                </tr>
                                 <tr>
                                    <td class="name">Все поля</td>
                                    <td class="two"><input type="text" name="dis_all" value="<?=htmlspecialchars($_GET['dis_all'])?>" /></td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                           
                                </tr>
                                <tr>
                                    <td class="name">Вид</td>
                                    <td class="two">
                                        <select name="dis_type" >
                                            <option <?=($_GET['dis_type'] == 0)?'selected="selected"':''?> value="0">Любой</option>
                                            <option <?=($_GET['dis_type'] == 1)?'selected="selected"':''?>  value="1">Кандидатская</option>
                                            <option <?=($_GET['dis_type'] == 2)?'selected="selected"':''?>  value="2">Докторская</option>
                                        </select>
                                    </td>
                                    <td class="r"></td>
                                    <td colspan="2" class="rgt"><input type="reset" name="reset" value="Очистить"/> |                                         <input type="submit" name="submit" value="Искать"/></td>
                                </tr>
                             </table>  
                           </form>
                         </div> 
                      </div>  
                       
                      <!-- encyclopedia -->   
                      <div <?=($_GET['vid'] == 'encyclopedia')?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="encyclopedia"> 
                         <div class="formSerach">
                          <form action="/search/static" method="get" class="all twos" name="bookSearch">
                           <input type="hidden" name="mode" value="encyclopedia">
                           <input type="hidden" name="vid" value="encyclopedia">
                            	<table>
                                    <tr>
                                        <td style="width:610px;"><input type="text" name="e_text" value="<?=htmlspecialchars($_GET['e_text'])?>" /></td>
                                        <td><input type="submit" name="submit" value="Искать"/></td>
                                    </tr>
                                    <tr>
                                    <td colspan="2">
                                     <table style="margin-top:6px;">
                                      <tr>
                                        <td style="width:80px;">Выводить по</td>
                                        <td>  
                                            <ul class="npageSeach4">
                                             <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                             <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                             <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                           </ul>   
                                        </td>
                                      </tr>
                                    </table>      
                                    </td>                  
                                 </tr>
                              </table>  
                           </form>
                         </div> 
                      </div>  
                       
                      <!-- News -->   
                      <div <?=($_GET['vid'] == 'news')?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="news"> 
                           <div class="formSerach">
                             <form action="/search/news" method="get" class="book" name="diafilmSearch">
                                 <input type="hidden" name="vid" value="news">
                            	<table>
                                  <tr>
                                    <td class="name">Слова</td>
                                    <td class="two"><input type="text" name="n_text" value="<?=htmlspecialchars($_GET['n_text'])?>" /></td>
                                    <td class="r"></td>
                                    <td>Выводить по</td>
                                    <td> 
                                     <ul class="npageSeach5">
                                        <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                        <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                        <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                     </ul>   
                                    </td>                       
                                 </tr>
                                  <tr>
                                     <td class="name">Тема</td>
                                     <td class="two">
                                     <select name="n_theme" >
                                        <option value="0">Все темы</option>
                                        <? foreach($this->theme as $value) :?>
                                            <option <?=($_GET['n_theme']==$value['id_sp_theme_news'])?'selected="selected"':''?> value="<?=$value['id_sp_theme_news']?>"><?=$value['name']?></option>
                                        <? endforeach ?>
                                      </select>
                                    </td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                
                                 </tr>
                                 <tr>
                                     <td class="name">Образование</td>
                                     <td class="two">
                                     <select name="n_obrazovanie" >
                                        <option value="0">Все виды образований</option>
                                        <? foreach($this->obrazovanie as $value) :?>
                                            <option <?=($_GET['n_obrazovanie']==$value['id_sp_education'])?'selected="selected"':''?> value="<?=$value['id_sp_education']?>"><?=$value['name']?></option>
                                        <? endforeach ?>
                                      </select>
                                    </td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                
                                 </tr>
                                 <tr>
                                    <td class="name">Персона</td>
                                    <td class="two">
                                     <select name="n_person" >
                                        <option value="0">Все персоны</option>
                                        <? foreach($this->person as $value) :?>
                                            <option <?=($_GET['n_person']==$value['id_person'])?'selected="selected"':''?> value="<?=$value['id_person']?>"><?=$value['fio']?></option>
                                        <? endforeach ?>
                                      </select>
                                    </td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>
                                 </tr>                      
                                  <tr>
                                     <td class="name">Организация</td>
                                     <td class="two">
                                     <select name="n_organizaciya" >
                                        <option value="0">Все организации</option>
                                        <? foreach($this->organizaciya as $value) :?>
                                            <option <?=($_GET['n_organizaciya']==$value['id_sp_organasation'])?'selected="selected"':''?> value="<?=$value['id_sp_organasation']?>"><?=$value['name']?></option>
                                        <? endforeach ?>
                                      </select>
                                    </td>
                                    <td class="r"></td>
                                    <td colspan="2"></td>                 
                                 </tr>
                                <tr>
                                    <td class="name">Дата</td>
                                    <td class="two">   <input type="text" name="n_newsDatestart" value="<?=($_GET['n_newsDatestart'])?htmlspecialchars($_GET['n_newsDatestart']):'От:'?>" class="date one" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'От:'}" code="int"/><input type="text" name="n_newsDateend" value="<?=($_GET['n_newsDateend'])?htmlspecialchars($_GET['n_newsDateend']):'До:'?>" class="date" onfocus="{this.value = '';}" onblur="if (this.value == ''){this.value = 'До:'}" code="int"/></td>
                                    <td class="r"></td>
                                    <td colspan="2" class="rgt"><input type="reset" name="reset" value="Очистить"/> |                                         <input type="submit" name="submit" value="Искать"/></td>
                                </tr>
                             </table>  
                           </form>
                         </div>    
                   </div>
                      <div <?=($_GET['vid'] == 'others')?'class="searchDiv searchAct"':'class="searchDiv"'?> mode="none" rel="others"> 
                         <div class="formSerach">
                          <form action="/search/static" method="get" class="all twos" name="othersSearch">
                             <input type="hidden" name="mode" value="others">
                             <input type="hidden" name="vid" value="others">
                            	<table>
                                    <tr>
                                        <td style="width:610px;"><input type="text" name="o_text" value="<?=htmlspecialchars($_GET['o_text'])?>" /></td>
                                        <td><input type="submit" name="submit" value="Искать"/></td>
                                    </tr>
                                    <tr>
                                    <td colspan="2">
                                     <table style="margin-top:6px;">
                                      <tr>
                                        <td style="width:80px;">Выводить по</td>
                                        <td>  
                                            <ul class="npageSeach4">
                                             <li><span <?=($_GET['limit']==10 || !$_GET['limit'])?'class="check act"':'class="check"'?>  mode="none" count="10"><input type="radio" name="limit" <?=($_GET['limit']==10 || !$_GET['limit'])?'checked="checked"':''?> value="10" /></span> 10 </li>
                                             <li><span <?=($_GET['limit']==25)?'class="check act"':'class="check"'?>  mode="none" count="25"><input type="radio" name="limit" <?=($_GET['limit']==25)?'checked="checked"':''?>  value="25"/></span>  25 </li>
                                             <li><span <?=($_GET['limit']==50)?'class="check act"':'class="check"'?>  mode="none" count="50"><input type="radio" name="limit" <?=($_GET['limit']==50)?'checked="checked"':''?> value="50"/></span>  50  результатов</li>
                                           </ul>   
                                        </td>
                                      </tr>
                                      <tr>
                                       <td colspan="2" class="top">
                                         <p>Поиск производится в разделах &laquo;Система координат&raquo;, &laquo;Человек и Текст&raquo;, &laquo;Исследователям&raquo; и &laquo;О проекте&raquo;.</p>
                                         <p>Поиск по записям разделов &laquo;Книжный вестник&raquo; и &laquo;Ищу читателя&raquo; осуществляется через форму &laquo;Книги&raquo;.</p>
                                       </td>
                                      </tr>
                                    </table>      
                                    </td>                  
                                 </tr>
                           	 </table>  
                           </form>
                         </div> 
                      </div>     
                        <div class="result">
                           <div class="group"> 
                               <p class="zag resTitle">Результы поиска:</p>
                               <a href="javascript:void(0)" rel="book" class="start act">Все <?=($this->cnt[4])?$this->cnt[4]:0 ?></a>
                               <a href="javascript:void(0)" rel="art">Полные тексты <?=($this->cnt[1])?$this->cnt[1]:0 ?></a>
                               <a href="javascript:void(0)" rel="diafilm">Фрагменты <?=($this->cnt[2])?$this->cnt[2]:0 ?></a> 
                               <a href="javascript:void(0)" rel="dismaterial">Описания <?=($this->cnt[3])?$this->cnt[3]:0 ?></a>
                           </div> 
                           <div class="groupResult">
                             <div mode="none" rel="book" class="act">
                                 <?if($this->result) :?>
                                    <ol class="lists">
                                        <? foreach($this->result as $key=>$value ) : ?>    
                                         <li<?=($value['id_sp_polnota']==1)?'':' class="color2"'?>>
                                           <div class="iconmode art" mode="act"> 
                                            <p>
                                                <a href="<?='/'.$value['td_url'].'/'.$value['url']?>"><?=$value['short_author']?> <?=$value['short_name']?><?=($value['is_dot'])?'.':''?> <?=($value['short_author'])?'—':''?> <?=$value['year']?>.</a>
                                                <span><?=$value['bo']?></span>
                                            </p>
                                           </div>  
                                         </li>
                                         <? endforeach ?>    
                                    </ol>
                                 <? endif ?>
                             </div>                      
                             <div mode="none" rel="art">
                                 <?if($this->result) :?>
                            	    <ol class="lists">
                                        <? foreach($this->result as $key=>$value ) : ?>
                                            <? if($value['id_sp_polnota'] == 1) : ?> <li><a href="<?='/'.$value['td_url'].'/'.$value['url']?>"><?=$value['short_author']?> <?=$value['short_name']?><?=($value['is_dot'])?'.':''?> — <?=$value['year']?>.</a></li> <? endif ?>
                                        <? endforeach ?> 
                                    </ol>
                                 <? endif ?>
                             </div>
                             <div mode="none" rel="diafilm">
                                 <?if($this->result) :?>
                            	    <ol class="lists">
                                        <? foreach($this->result as $key=>$value ) : ?>
                                            <? if($value['id_sp_polnota'] == 2) : ?> <li><a href="<?='/'.$value['td_url'].'/'.$value['url']?>"><?=$value['short_author']?> <?=$value['short_name']?><?=($value['is_dot'])?'.':''?> — <?=$value['year']?>.</a></li> <? endif ?>
                                        <? endforeach ?> 
                                    </ol>
                                 <? endif ?>
                             </div>
                              <div mode="none" rel="dismaterial">
                                  <?if($this->result) :?>
                            	    <ol class="lists">
                                        <? foreach($this->result as $key=>$value ) : ?>
                                            <? if($value['id_sp_polnota'] == 3) : ?> <li><a href="<?='/'.$value['td_url'].'/'.$value['url']?>"><?=$value['short_author']?> <?=$value['short_name']?><?=($value['is_dot'])?'.':''?> — <?=$value['year']?>.</a></li> <? endif ?>
                                        <? endforeach ?> 
                                    </ol>
                                  <? endif ?>
                             </div>
                           </div>
                          <?=$this->paging?>  
                        </div>
                       </div>
                      </div>
		   <div class="clear"></div>
	           </div>
           </div>
            </div>
            <div class="push"></div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
