<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="wrapper">
            <div class="all">               
            <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>   
                <!--<?if($this->breadCrumbs) :?>
                    <div class="bredcrumbs">
                        <a href="/news/"><?=$this->lang['portal.news']?></a>                      
                            <span class="r"></span> 
                            <span class="end"><?=$this->breadCrumbs?></span>                    
                    </div>
                <?endif?>-->
                <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs.php'; ?>
            <div id="containerLR">
	            <div id="innerLR">                    
                      <div id="right"> 
                        <?foreach($this->vid as $vid) :?>
                          <? $i=0; $dop=($i==0) ? " topAr" : ""; $i++; ?>
                          <div id="area" class="ar1<?=$dop?>">
                             <div class="headR"><span><a href="?vid=<?=$vid['id_sp_vid_news']?>"><?=$vid['name']?></a></span></div>                           
                             <div id="info" class="news">                            
                               <ul>
                                   <?foreach($vid['relatedNews'] as $rel) :?>
                                      <li><a href="/<?=$rel['url1']?>"><?=stripslashes($rel['zagolovok'])?></a></li>
                                   <? endforeach ?>   
                               </ul>
                              </div>
                          </div>  
                       <?endforeach ?>                                                             
                      </div>
		              
                      <div id="left">
                        <ul class="left news">
                            <?foreach($this->theme as $key=>$theme) :?>
                                <li<?=(count($this->theme) == $key)?' class="end"':''?>><a <?=$theme['class']?> href="/<?=URL_NEWS.$theme['url']?>"><?=$theme['name']?></a></li>                       
                           <? endforeach ?>  
                        </ul>
                      </div>
                        <div id="centerLR"> 
                            <div class="news">                       
                                    <?=(count($this->breadCrumbs) == 1)?'<a href="/rss.xml" class="rss">'.$this->lang['portal.news_rss'].'</a>':''?>
                                    <p class="zagolovok">Новости<?=(count($this->breadCrumbs) > 1)?' по теме: '.$this->breadCrumbs[0]['name']:''?></p>
                                    <ul class="news">
                                        <? if(is_array($this->listNews)) : ?>
                                            <?foreach($this->listNews as $news) :?>
                                                <li>
                                                    <span class="time"><?=$news['date_n']?></span> <a href="/<?=$news['url1']?>"><?=stripslashes($news['zagolovok'])?></a>
                                                    <p><?=stripslashes($news['annotaciya'])?></p>
                                                </li>
                                            <? endforeach ?>
                                        <? else : ?> 
                                                <li><?=$this->lang['portal.news_theme_no_count']?></li>
                                        <? endif ?>
                                    </ul>
                                <?=$this->paging?>                      
                          </div>
                      </div>
		   <div class="clear"></div>
	           </div>
           </div>
            </div> 
            <div class="push"></div>
  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
