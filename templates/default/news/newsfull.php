<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="wrapper">
            <div class="all">               
            <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>  
            <div class="setting soneBut">
              <ul>
                 <li><a href="javascript:history.back();" class="icon back" title="<?=$this->lang['portal.go_back']?>"></a></li> 
              </ul>
            </div>       
                                
                <!--<div class="bredcrumbs">
                    <a href="/news/"><?=$this->lang['portal.news']?></a>  
                        <? #foreach($this->breadCrumbs as $brcr) : ?>
                            <span class="r"></span> 
                            <span class="end"><?=$this->breadCrumbs?></span>
                        <? #endforeach ?>
                </div>  -->
                <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs.php'; ?>
            <div id="containerL">
	            <div id="innerL">
                      <div id="left">
                        <ul class="left">
                           <?foreach($this->theme as $key=>$theme) :?>
                                <li<?=(count($this->theme) == $key)?' class="end"':''?>><a <?=$theme['class']?> href="/news/index/<?=$theme['url']?>"><?=$theme['name']?></a></li>                       
                           <? endforeach ?>
                        </ul>
                      </div>
		              <div id="centerL">                         
                        <div class="text">                           
                           <div class="social">
                            <!--<span><?=$this->lang['portal.news_share']?>:</span>-->							
                            <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                            <div>
			    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter"></div> 
                             </div>                               
                           </div>                           
                           <p class="zag zag-news"><?=$this->news['zagolovok']?></p>
                           <p class="decript"><?=stripslashes($this->news['annotaciya'])?></p>                           
                           <p><?=stripslashes($this->news['text'])?></p>
                           <? if($this->news['comment']) : ?>
                            <div class="comment">
                              <span><?=$this->lang['portal.news_comment']?></span>
                              <?=stripslashes($this->news['comment'])?>
                            </div>
                           <? endif ?>
                           <? if(count($this->relNews)) :?>
                           <p class="zag aril"><?=$this->lang['portal.news_material_theme']?></p>
                           <div class="alltheme">
                               <? foreach($this->relNews as $news) :?>
                                <p><?=$news['date_n']?>  <a href="/<?=$news['url1']?>"><?=$news['zagolovok']?></a></p>
                               <? endforeach ?> 
                           </div>
                           <? endif ?>
                        </div>
                      </div>
		              <div class="clear"></div>
	           </div>
           </div>
           </div>
           <div class="push"></div>
  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
