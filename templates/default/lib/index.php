<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="loader"></div>
<div class="wrapper">
            <div class="all">  
              <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>      
              <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs.php'; ?>               
            <div id="containerL">
	            <div id="innerL">		              
                        <? include DROOT.MAIN_TEMPLATE.'/include/leftMenu.php'; ?>                       
                        <div id="centerL"> 
                            <? $title = ($this->title!="")? " ".$this->title."" : "" ;?>
                            <p class="zagolovok"><?=$this->namePage.$title?></p>                            
                            <div class="text">
                              <?=$this->content?>  
                              <? if(!in_array($this->namePage, array('Книги', 'Статьи', 'Диафильмы')) && $this->rubricators[$this->rubrType]) echo $this->rubricators[$this->rubrType]; ?>
                              <? if(!in_array($this->namePage, array('Книги', 'Статьи', 'Диафильмы')) && $this->rubricators[$this->rubrType]) include('listDataMain.php'); ?>
                            </div>     
                       </div>
                       <div class="clear"></div>
	           </div>
               </div>
           </div>
           <div class="push"></div>
  </div>
<?=$this->paging?>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
