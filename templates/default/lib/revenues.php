<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<script src="<?=MAIN_TEMPLATE?>/js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<script>


//---------------------------------------------------------------------------------------------------------------------------
$(function() {

$.datepicker.setDefaults($.extend($.datepicker.regional['ru']));
$(".date").datepicker({
 changeMonth: true,
 changeYear: true,
 yearRange: '-10:+1',
 monthNamesShort: ['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
 
});

//---------------------------------------------------------------------------------------------------------------------------

});
</script>
<div class="loader"></div>
<div class="wrapper">
            <div class="all">               
             <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>  
             <? include DROOT.MAIN_TEMPLATE.'/include/setting.php'; ?>
             <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs.php'; ?>
            <div id="containerL">
	            <div id="innerL">
                      <div id="left">
                        <ul class="left vestnik">
                           <?
                              $datePhilter= ( ($_GET['dateStart']!="") || ($_GET['dateEnd']!="") ) ? '&dateStart='.$_GET['dateStart'].'&dateEnd='.$_GET['dateEnd'] : "";
                           ?>
                            
                          <li><a href="javascript:void(0)">Книги&nbsp;&nbsp;</a>
                                 <ul class="vest" <?=($_GET['type']=='book' || !$_GET['type'])?'style="display:block;"':''?>>
                                   <li><a href="revenues?type=book&mode=0<?=$datePhilter?>" <?=(($_GET['type']=='book' && $_GET['mode']=='0') || !$_GET['type'])?'class="act"':''?>>Все записи</a></li>
                                   <li><a href="revenues?type=book&mode=1<?=$datePhilter?>" <?=($_GET['type']=='book' && $_GET['mode']=='1')?'class="act"':''?>>Полные тексты</a></li>
                                   <li><a href="revenues?type=book&mode=2<?=$datePhilter?>" <?=($_GET['type']=='book' && $_GET['mode']=='2')?'class="act"':''?>>Фрагменты</a></li>
                                   <li><a href="revenues?type=book&mode=3<?=$datePhilter?>" <?=($_GET['type']=='book' && $_GET['mode']=='3')?'class="act"':''?>>Описания</a></li>
                                 </ul>
                           </li>
                           <li><a href="javascript:void(0)">Статьи</a>
                                <ul class="vest" <?=($_GET['type']=='article')?'style="display:block;"':''?>>
                                   <li><a href="revenues?type=article&mode=0<?=$datePhilter?>" <?=($_GET['type']=='article' && $_GET['mode']=='0')?'class="act"':''?>>Все записи</a></li>
                                   <li><a href="revenues?type=article&mode=1<?=$datePhilter?>" <?=($_GET['type']=='article' && $_GET['mode']=='1')?'class="act"':''?>>Полные тексты</a></li>
                                   <li><a href="revenues?type=article&mode=3<?=$datePhilter?>" <?=($_GET['type']=='article' && $_GET['mode']=='3')?'class="act"':''?>>Описания</a></li>
                                 </ul>
                           </li>
                           <li><a href="javascript:void(0)">Диафильмы</a>
                                <ul class="vest" <?=($_GET['type']=='filmstrip')?'style="display:block;"':''?>>
                                   <li><a href="revenues?type=filmstrip&mode=0<?=$datePhilter?>" <?=($_GET['type']=='filmstrip' && $_GET['mode']=='0')?'class="act"':''?>>Все записи</a></li>
                                   <li><a href="revenues?type=filmstrip&mode=1<?=$datePhilter?>" <?=($_GET['type']=='filmstrip' && $_GET['mode']=='1')?'class="act"':''?>>Полные тексты</a></li>
                                   <li><a href="revenues?type=filmstrip&mode=2<?=$datePhilter?>" <?=($_GET['type']=='filmstrip' && $_GET['mode']=='2')?'class="act"':''?>>Фрагменты</a></li>
                                   <li><a href="revenues?type=filmstrip&mode=3<?=$datePhilter?>" <?=($_GET['type']=='filmstrip' && $_GET['mode']=='3')?'class="act"':''?>>Описания</a></li>
                                 </ul>
                           </li>
                           <li><a href="javascript:void(0)">Диссертационные материалы</a>
                                 <ul class="vest" <?=($_GET['type']=='thesis')?'style="display:block;"':''?>>
                                   <li><a href="revenues?type=thesis&mode=0" <?=($_GET['type']=='thesis' && $_GET['mode']=='0')?'class="act"':''?>>Все записи</a></li>
                                   <li><a href="revenues?type=thesis&mode=1" <?=($_GET['type']=='thesis' && $_GET['mode']=='1')?'class="act"':''?>>Полные тексты</a></li>
                                   <li><a href="revenues?type=thesis&mode=2" <?=($_GET['type']=='thesis' && $_GET['mode']=='2')?'class="act"':''?>>Фрагменты</a></li>
                                   <li><a href="revenues?type=thesis&mode=3" <?=($_GET['type']=='thesis' && $_GET['mode']=='3')?'class="act"':''?>>Описания</a></li>
                                 </ul>
                           </li>
                           
                           <li class="end"><a href="javascript:void(0)">Периодика</a>
                                 <ul class="vest" <?=($_GET['type']=='periodic')?'style="display:block;"':''?>>
                                   <li><a href="revenues?type=periodic&mode=0" <?=($_GET['type']=='periodic' && $_GET['mode']=='0')?'class="act"':''?>>Все записи</a></li>
                                   <li><a href="revenues?type=periodic&mode=1" <?=($_GET['type']=='periodic' && $_GET['mode']=='1')?'class="act"':''?>>Полные тексты</a></li>
                                   <li><a href="revenues?type=periodic&mode=2" <?=($_GET['type']=='periodic' && $_GET['mode']=='2')?'class="act"':''?>>Фрагменты</a></li>
                                   <li><a href="revenues?type=periodic&mode=3" <?=($_GET['type']=='periodic' && $_GET['mode']=='3')?'class="act"':''?>>Описания</a></li>
                                 </ul>
                           </li>
                        </ul>
                      </div>
                        <div id="centerL"> 
                         <div class="philterDate">                             
                           <form action="" method="get" name="phtdate">
                            <input type="hidden" name="type" value="<?=$_GET['type']?>">
                            <input type="hidden" name="mode" value="<?=$_GET['mode']?>">
                           <span>Дата поступления: </span> с &nbsp;<input type="text" name="dateStart" value="<?=$_GET['dateStart']?>" star="<?=$_GET['dateStart']?>" class="date"/>&nbsp;&nbsp;по&nbsp;&nbsp;<input type="text" name="dateEnd" value="<?=$_GET['dateEnd']?>" star="<?=$_GET['dateEnd']?>" class="date"/>
                            <input type="submit" name="send" value="OK" title="Применить"/>
                           </form>
                        </div>
                        
                        <p class="zagolovok"> <?=$this->namePage?> <?=$this->title?></p>                        
                    	<div class="text">                                     
                        </div>              
                      </div>
                      <div class="clear"></div>
	           </div>
           </div>
            </div>
            <div class="push"></div>
  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>