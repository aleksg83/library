<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<script>
//---------------------------------------------------------------------------------------------------------------------------
$(function() {

$( "#dialog, #dialog2" ).dialog({
 	autoOpen: false,
	width: 350,
	resizable: false,
	dialogClass: 'bdescription'
	//show: 'slide'
});
	

$("a.bo").click(function(){ 
	$( "#dialog" ).dialog( "open" );
	//event.preventDefault();
});	

/*
$("a.save").click(function(){
						   
	$( "#dialog2" ).dialog({
	    width: 150
	});					   
	
	$( "#dialog2" ).dialog( "open" );
	//event.preventDefault();
});	
*/


$("div, .bdescription a").removeClass("ui-corner-all");
$(".ui-dialog-titlebar-close span").removeClass("ui-icon");
$(".ui-dialog-titlebar-close span").text("");
//---------------------------------------------------------------------------------------------------------------------------

});
</script>
<body>
<div class="loader"></div>
<div class="wrapper">
    <div class="all">               
          <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>  
              <? 
                    #-type-------------------------------------
                    if ($this->object['id_sp_type_data']=="1"){
                        $puth=BOOK;$puthbase=BOOK_BASE; $typedata="book";
                    }else if ($this->object['id_sp_type_data']=="2"){
                        $puth=ARTICLE;$puthbase=ARTICLE_BASE; $typedata="article";
                    }else if ($this->object['id_sp_type_data']=="3"){
                        $puth=THESIS_MATERIAL;$puthbase=THESIS_MATERIAL_BASE; $typedata="thesis";
                    }else if ($this->object['id_sp_type_data']=="4"){
                        $puth=FILMSTRIP;$puthbase=FILMSTRIP_BASE; $typedata="filmstrip";
                    }
                    
                    #-fileexist--------------------------------
                    if (file_exists($puth.$this->object['folder_name']."/".$this->object['file_name'])){
                         $fileshow=true;
                    }
                    
                    #-classbut---------------------------------
                    $classBut=($this->object['is_meta_book']) ? "" : " but" ;
                    if ( $this->object['is_meta_book'] && $fileshow){
                       $classBut=""; 
                    }else if ( (!$this->object['is_meta_book'] && $fileshow) || (($this->object['is_meta_book'] && !$fileshow)) ){
                        $classBut=" but"; 
                    }else if (!$this->object['is_meta_book'] && !$fileshow){
                        $classBut=" twobutton-data"; 
                    }
                    
              ?>
             <div class="setting sbook<?=$classBut?>">
                  <ul>
                     <li><a href="javascript:history.back();" class="icon back" title="<?=$this->lang['portal.go_back']?>"></a></li> 
                    <? if ($fileshow) :?> 
                     <li><a href="/lib/download/<?=$this->object['file_name']?>?mode=<?=$this->object['mode']?>" class="icon save" title="<?=$this->lang['portal.lib.donwload']?> (<?=$this->object['fileInfo']['ext']?>, <?=$this->object['fileInfo']['size']?>)"></a></li>
                    <? endif;?> 
                     <li><a href="javascript:void(0);" class="icon bo" title="<?=$this->lang['portal.lib.bo']?>"></a></li>  
                    <? if($this->object['is_meta_book']) : ?> 
                     <li><a href="/lib/meta.html" class="icon meta" title="<?=$this->lang['portal.lib.info']?>"></a></li>  
                    <? endif ?>    
                  </ul>                       
             </div>
             <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs.php'; ?>
             <div id="containerL">
	            <div id="innerL">       
                      <div id="left">
                       <ul class="left">
                           <? include DROOT.MAIN_TEMPLATE.'/include/leftMenu.php'; ?>
                        </ul>
                      </div>
		      <div id="centerL">
                      <div class="text">
                        <? if (file_exists($puth.$this->object['folder_name']."/preview_0001.jpg")) {
                              $file=$puthbase.$this->object['folder_name']."/preview_0001.jpg";
                        }else  if (file_exists($puth.$this->object['folder_name']."/jpg/0001.jpg")){
                              $file=$puthbase.$this->object['folder_name']."/jpg/0001.jpg";
                        }else{
                             $file=MAIN_TEMPLATE.'/images/book/default.jpg'; 
                        }  
                        ?>  
                        <img src="<?=$file?>" class="fleft" />
                        <? if ($this->object['year']!="") $years="(".$this->object['year'].")";?>
                        <p class="zag"><?=$this->object['short_author']?> <?=$this->object['name']?><?=($this->object['is_dot'])?'.':''?> <?=$years?></p>
                        <p class="annotaciya"><?=stripslashes($this->object['annotaciya'])?></p>
                      </div>	
                       <? if ($this->object['mapping']): ?>
                           <div class="sd">
                               <p class="zag"><?=$this->lang['portal.lib.soderganie']?></p>
                               <? foreach($this->object['mapping'] as $map) : ?>
                                    <? if (strlen($map['name']) > 180 )  $heightMapping = ' style="height:36px;"'; else $heightMapping="";  ?>
                                    <?if ($map['type']=="0"){?>
                                        <p class="sdl sdl2"<?=$heightMapping?>>
                                            <!--<span class="list fl color2" style="padding-left:<?=($map['level']-1)*20?>px;"><?=$map['name']?></span> -->
                                            <a href="/<?=VIEWER.$typedata?>/<?=$this->object['url']?>#<?=$map['num_page']?>" target="_blank" class="name color2"  style="padding-left:<?=($map['level']-1)*20?>px;"><?=$map['name']?></a> 
                                            <span class="list color2"><?=$map['real_page']?></a>
                                        </p>
                                    <?}else{?>
                                        <p class="sdl"<?=$heightMapping?>>
                                            <a href="/<?=VIEWER.$typedata?>/<?=$this->object['url']?>#<?=$map['num_page']?>" target="_blank" class="name"  style="padding-left:<?=($map['level']-1)*20?>px;"><?=$map['name']?></a> 
                                            <span class="list"><?=$map['real_page']?></a>
                                        </p>
                                    <?}?>
                                <? endforeach?> 
                           <div>
                        <? endif ?>
                        <? if ($this->object['materials']!="") : ?>    
                        <div class="text">
                            <div class="dopmaterials">
                                <p class="zag"><?=$this->lang['portal.lib.material']?></p>
                                <?=$this->object['materials']?>
                            </div> 
                        </div>  
                        <? endif;?>    
                        <div id="dialog" title="<?=$this->object['short_name']?>">
                           <p><b><?=$this->lang['portal.lib.bo_izd']?></b></p>
                           <p><?=$this->object['bo']?></p>
                        </div>                            
                      </div>
                    <div class="clear"></div>
                </div>
           </div>
        </div>
        <div class="push"></div>
  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>