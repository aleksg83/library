<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="wrapper">
    <div class="all">
        <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>    
        <div id="containerL">
            <div id="innerL">
                <div id="left">
                    <ul class="left">
                    <? include DROOT.MAIN_TEMPLATE.'/include/leftMenu.php'; ?>
                </div>
                <div id="centerL"> 
                    <p class="zagolovok">Авторы</p>
                    <?=$this->letters?>                                                
                    <div class="text">
                        <?=$this->persons?> 
                        <?=$this->paging?> 
                    </div>            
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="push"></div>
</div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>