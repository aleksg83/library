<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="wrapper">
            <div class="all">
              <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>  
               <div class="setting soneBut"><ul><li><a href="javascript:history.back();" class="icon back" title="Назад"></a></li></ul></div>                      
              
               <div class="bredcrumbs">
                <a href="/"><?=$this->lang['portal.main_page']?></a> 
                <span class="r"></span> 
                <a href="/lib/"><?=$this->lang['portal.lib_name']?></a> 
                <span class="r"></span>  
                <a href="/lib/authors"><?=$this->lang['portal.lib_author_name']?>
                </a><span class="r"></span>
                <a href="/lib/authors/letter/<?=$this->letter?>"><?=$this->letter?></a> 
               </div>     
               
               <div id="containerL">
	            <div id="innerL">		              
                      <div id="left">
                        <ul class="left">
                         <? include DROOT.MAIN_TEMPLATE.'/include/leftMenu.php'; ?>
                        </ul>
                      </div>                       
                      <div id="centerL">                        
                          <div class="text">                          
                           <? 
                                if ($this->person['years']!=""){
                                    $dopUrl="(".$this->person['years'].")";
                                }else{
                                    $dopUrl="";
                                }
                           
                           ?> 
                            <?if ($this->authorUrlPage=="") :?> 
                                <?
                                    $tmp=explode(" ", $this->person['fio']);
                                    $this->person['fio']= "<span>".$tmp[0]."</span> ".$tmp[1]." ".$tmp[2];
                                ?>
                                <p class="zagolovok author"><?=$this->person['fio']?> <span><?=$dopUrl?></span></p>
                            <?else:?>
                                <?
                                    $tmp=explode(" ", $this->person['fio']);
                                    $this->person['fio']= "<span class='color1'>".$tmp[0]."</span> ".$tmp[1]." ".$tmp[2];
                                ?>
                                <p class="zagolovok author"><a href="<?=$this->authorUrlPage?>"><?=$this->person['fio']?> <?=$dopUrl?></a></p>
                            <?  endif;?>
                            <p><?=$this->person['short_definition']?></p>                   
                            <?foreach($this->person['material'] as $name=>$data) :?>
                         	 <div class="dopmaterials">
                                    <p class="zag"><?=$name?></p>                               	 
                                    <ul class="book authorBot">
                                        <?foreach($data as $material):?>
                                         
                                         <? $nameAuthor = ($material['id_sp_type_person']!=1) ? $material['short_author']." " : "" ;?>
                                         
                                         <?if ( ($material['id_sp_type_data']!=2) || ( ($material['id_sp_type_data']==2) && ($material['id_next']==0) ) ){ ?>
                                             
                                             <?if ($material['is_view_in_rubrik']=="1"){?>
                                                <li class="<?=$material['class']?>"><a href="<?='/'.$material['dop_url'].'/'.$material['url'].$material['viewer_url']?>"<?=$material['target']?>><?=$nameAuthor.$material['short_name']?><?=($material['is_dot'])?'.':''?> &mdash; <?=$material['year']?>.</a></li>
                                             <?}else{?>
                                                <li><?=$nameAuthor.$material['short_name']?><?=($material['is_dot'])?'.':''?> — <?=$material['year']?>.</li>
                                             <?}?>  
                                                
                                         <? }else{ ?>
                                                
                                              <? $startText=$nameAuthor.$material['short_name'].". Начало";
                                                 $statYear="(".$material['year'].") ".$material['node_art'];  
                                                 if ($material['is_view_in_rubrik']=="1"){
                                                          $textArt='<a href="/'.$material['dop_url'].'/'.$material['url'].$material['viewer_url'].'" class="'.$material['class'].'">'.$startText.'</a>&nbsp;'.$statYear;  
                                                  }else{
                                                           $textArt=$startText.' '.$statYear;  
                                                  }
                                               ?>
                                                  <li><?=$textArt?></li>
                                                
                                         <?}?>       
                                                
                                       <?endforeach?>
                                    </ul>
                                </div>  
                         <?endforeach?>   
                      </div>
                   </div>      
                 <div class="clear"></div>
	       </div>
           </div>
       </div> 
     <div class="push"></div>
  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
