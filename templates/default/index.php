<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="loader"></div>
<div class="wrapper">
            <div class="all">               
               <div id="head">                
                    <? include DROOT.MAIN_TEMPLATE.'/include/search.php'; ?>
                   <span class="logo not">
                       <span class="main"><?=$this->lang['portal.logo_name']?></span>
                       <span class="text"><span class="one"><?=$this->lang['portal.logo_save']?>,</span><span class="two"><?=$this->lang['portal.logo_multiply']?>,</span><span class="three"><?=$this->lang['portal.logo_transmit']?></span></span>
                    </span>
                   
                  <? include DROOT.MAIN_TEMPLATE.'/include/topMenu.php'; ?>
               </div>
            
            
            <div id="containerR">
	            <div id="innerR">
		             <div id="right"> 
                       
                         <? 
                            $countPosition=count($this->event); $w=$countPosition*10; $style=' style="width:'.$w.'px"';
                            if ($countPosition>0){
                         ?>         
                        <div id="calendarj">
                           <div class="headR"><a href="<?=SITE_URL."encyclopedia/index/almanac"?>"><?=$this->lang['portal.calendarj']?></a></div>
                           <div class="sladeCalendarj">
                            <? foreach($this->event as $key=>$object) : ?>  
                                
                                <div class="date" target="<?=$object['target']?>" position="<?=$object['position']?>">
                                    <a href="<?=$object['url']?>">
                                        <div class="person">
                                             <img src="<?=$object['images']?>"/>
                                        </div>
                                        <div class="time"><span><?=$object['date']?></span></div>
                                        <div class="text">
                                            <span><?=$object['title']?></span>
                                            <?=$object['text']?> 
                                        </div>
                                    </a>
                                    <?if ($countPosition>1){?>
                                    <div class="button" <?=$style?>>
                                        <?for ($i=0;$i<$countPosition; $i++){?>
                                            <? $class=($i==$object['position']) ? ' class="act"' : '';?>
                                            <button <?=$class?> numb="<?=$i?>"></button>
                                        <?}?>
                                   </div>
                                    <?}?> 
                                </div>
                         
                              <? endforeach?>    
                              </div> 
                        </div><!-- calendarj-->
                            <?}?>
                      
                     
                      <?if ($this->notice['view']=="yes"){?>
                        <div id="area">
                             <div class="headR"><a href="<?=$this->notice['url']?>?vid=2" class="cnt"><span><?=$this->notice['name']?></span></a></div>
                              <div id="info">
                               <ul>
                                  <? foreach($this->notice['news'] as $value) : ?>  
                                        <li>
                                           <span class="time"><?=$value['date_n']?></span> <a href="<?=SITE_URL.URL_NEWS.$value['url']?>"><?=$value['zagolovok']?></a>
                                        </li>
                                 <? endforeach?>   
                               </ul> 
                              </div>    
                        </div>  
                      <?}?>
                        
                      <div class="newsform">
                           <p class="info"></p>
                           <p class="info-load"><img src="<?=MAIN_TEMPLATE?>/images/loading-big.gif"</p>
                           <p><?=$this->lang['portal.rassulka']?></p>
                           <form action="#" method="post" id="rassilka">
                            <table>
                              <tr>
                                 <td class="one"><?=$this->lang['portal.name_user']?></td>
                                 <td><input type="text" name="name" value=""/></td>
                              </tr>
                              
                              <tr>
                                 <td class="one"><?=$this->lang['portal.email_user']?></td>
                                 <td><input type="text" name="email" value=""/></td>
                              </tr>
                              
                              <tr>
                                 <td colspan="2">
                                     <div class="info-sender">
                                         <div class="head">
                                             <span class="title">О рассылке</span>
                                             <span class="close"></span>
                                         </div>
                                         <div class="info">
                                             <?=$this->rassulka_info['about']?>
                                         </div>
                                         <div class="info-user-sender"></div>
                                         <div class="info-user-sender-default" style="display:none;"><?=$this->rassulka_info['info_after_sender']?></div>
                                     </div>
                                     
                                     <a href="javascript:void(0)" class="send"></a>
                                     <a href="javascript:void(0)" class="about-sender">О рассылке</a>
                                    
                                 </td>
                              </tr>
                            </table>
                           </form>
                        </div>
                     </div> <!-- right -->

		     <div id="centerR"> 
                      
                       <div class="news">
                            <p class="zagolovok"><a href="/news/" class="cnt"><?=$this->lang['portal.news']?></a></p>
                            <ul class="news">
                             <? foreach($this->listnews as $key=>$object) : ?>  
                                  <li<?=$object['class']?>>
                                      <span class="<?=$object['date-class']?>"><?=$object['date']?></span> <a href="<?=SITE_URL.URL_NEWS.$object['url']?>"><?=$object['zagolovok']?></a>
                                  </li>
                              <? endforeach?>    
                            </ul>
                      </div>
                      <div class="line"></div> 
                        <div class="mainarea">
                            <? foreach($this->listmenumain as $value) : ?>
                                <ul class="main">
                                  <li><a href="<?=SITE_URL.$value['url']?>" class="bold<?=$value['class']?>"><?=$value['name']?></a></li>
                                  <li class="text">
                                      <? foreach($value['menu'] as $value2) : ?>
                                         <a href="/<?=$value2['url']?>"><?=$value2['name']?></a>
                                      <? endforeach?>  
                                         <a href="/<?=$value['url']?>">...</a>
                                  </li>
                                </ul>         
                            <? endforeach?>  
                        </div>
                      <div class="line"></div> 
                        <div class="blogCenter">
                            <? if ($this->journal['view']=="yes") {?>
                            <p class="zagolovok"><a href="<?=$this->journal['url']?>?vid=4" class="cnt"><span><?=$this->journal['name']?></span></a></p>
                             <div class="info">
                                 <ul>
                                      <? foreach($this->journal['news'] as $value) : ?>  
                                        <li>
                                           <span class="time"><?=$value['date_n']?></span> <a href="<?=SITE_URL.URL_NEWS.$value['url']?>"><?=$value['zagolovok']?></a>. <?=$value['annotaciya']?>
                                        </li>
                                      <? endforeach?>    
                                 </ul>   
                             </div>
                            <?}?>
                        </div>
                       <div class="read">
                        <p class="zagolovok"><a href="#" class="cnt"><span><?=$this->lang['portal.reader']?></span></a></p>
                        <ul class="read">
                           <ul class="read"> 
                                <? foreach($this->reader as $value) : ?>
                                    <li>
                                        <a href="<?=$value['url']?>"><img src="<?=$value['images']?>" /></a>
                                        <p class="name"><a href="<?=$value['url']?>"<?=$value['class']?>><?=$value['name']?></a></p>
                                        <p><?=$value['annotaciya']?></p>
                                    </li>
                               
                                <? endforeach?>   
                           </ul>   
                        </div>
                      </div>
                      <!-- centerR -->
		              <div class="clear"></div>
	           </div>
           </div>
              <!--  
                <div class="endmenu">
                   <? foreach($this->endmenu as $value) :?>
                      <ul>
                        <li><a href="<?=SITE_URL.$value['url']?>" class="bold"><?=$value['name']?></a></li>
                             <? foreach($value['child'] as $child) :?>
                                 <li><a href="<?=SITE_URL.$value['url'].$child['url']?>"><?=$child['name']?></a></li>
                             <? endforeach?>  
                      </ul>  
                   <? endforeach?> 
                </div>
              -->  
            </div> <!-- all -->
               
           
            <div class="push"></div>

  </div>       
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
