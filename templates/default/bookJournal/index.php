<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<script>
$(function() {
    $.datepicker.setDefaults($.extend($.datepicker.regional['ru']));
    $(".date").datepicker({
     changeMonth: true,
     changeYear: true,
     yearRange: '-10:+1',
     monthNamesShort: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']
    });
});
</script>
<div class="loader"></div>
<div class="wrapper">
            <div class="all">               
               <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>
               <? include DROOT.MAIN_TEMPLATE.'/include/setting.php'; ?>          
               <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs0.php'; ?>            
            <div id="containerL">
	            <div id="innerL">		              
                      <div id="left">                            
                        <ul class="left vestnik">
                           <? foreach($this->menuLeft as $key=>$menu) : ?>  
                                    <? $end = ($key==(count($this->menuLeft)-1)) ? ' class="end"' : '' ; ?>  
                                    <li<?=$end?>>
                                        <a href="<?=($menu['url_name']=='kv_all_book')?'/bookJournal':'#'; ?>" <?=$menu['class']?>><?=$menu['name']?></a>
                                        <?if(count($menu['nastedRubr'])): ?>
                                            <ul class="vest" <?=($menu['class'])?'style="display:block;"':''?>>
                                                <?foreach($menu['nastedRubr'] as $rub):?>
                                                    <? $start= ($_GET['dateStart']!="") ? "?dateStart=".$_GET['dateStart'] : "" ;
                                                       $end= ($_GET['dateEnd']!="") ? "dateEnd=".$_GET['dateEnd'] : "" ; 
                                                       $end = ($_GET['dateStart']!="") ? "&".$end : "?".$end;
                                                    ?>
                                                
                                                    <li><a href="/<?=$rub['url'].$start.$end?>" <?=$rub['class']; ?>><?=$rub['name']?></a></li>
                                                <? endforeach ?>
                                            </ul>
                                        <?endif?>
                                    </li>
                            <? endforeach; ?>
                        </ul>
                      </div>                       
                      <div id="centerL">                        
                        <div class="philterDate">
                           <form action="" method="get" name="filter">
                           <input type="image" src="<?=MAIN_TEMPLATE?>/images/ok.png" name="send" value="OK" title="Выбрать записи"/>
                           <span>Дата поступления: </span> от &nbsp;<input type="text" name="dateStart" value="<?=$_GET['dateStart']?>" star="<?=$_GET['dateStart']?>"  class="date"/>&nbsp;&nbsp;до&nbsp;&nbsp;<input type="text" name="dateEnd" value="<?=$_GET['dateEnd']?>" star="<?=$_GET['dateEnd']?>" class="date"/>
                          </form>
                        </div>                         
                        <p class="zagolovok"><?=(!$this->textMenuRubr)?'Все книги':''?> <?=$this->title?></p>                                               
                        <div class="text">
                                                             
                         </div>               
                      </div>
                    <div class="clear"></div>
	           </div>
           </div>
            </div>
            <div class="push"></div>
  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
