// JavaScript Document
$(function(){
var urlServer = window.location.protocol+ '//' +window.location.hostname+"/";

//$("#containerL a[href^='http://']").attr("target","_blank");
/*открытие всех внешних ссылок в новом окне*/
$('a').each(function() {
   var a = new RegExp('/' + window.location.host + '/');
   if ( (!a.test(this.href)) && (this.href!='javascript:history.back();') && (this.href!='javascript:void(0)') && (this.href!='javascript:void(0);') ) {
       $(this).click(function(event) {
           event.preventDefault();
           event.stopPropagation();
           window.open(this.href, '_blank');
       });
   }
});
    

if ($(".math").size()>0){
$(".math").latex({
    format: 'png'				 
});
}


$('input.date').change(function(){
   $(this).next().show();
   if ($(this).val()=="") $(this).next().hide();
});

$('input.date').focus(function(){
    if ($(this).val()!="") $(this).next().show();
});


if (  $("input.date").size()>0 ) {
 $("input.date").addClear({
  name: $(this),
  showOnLoad:true,
  closeImage : "/templates/default/images/clear.png",
  onClear: function(name, value){
     var nameTemp = name.attr("name");
     if ( (nameTemp == "dateStart") || (nameTemp == "dateEnd") ){
         var collection = $(".vest li a");
         for ( var i=0; i<collection.size(); i++ ){
             var obj = $(".vest li a:eq(" + i + ")");
             var str = obj.attr("href");
             obj.attr("href", str.replace("&"+nameTemp+"="+name.attr("star"), ""));
         }
         
     }
  }   
 });
}

//$("a.openimg").colorbox({});
//$("a.opengroup").colorbox({rel:'group', transition:"fade"});

/*
if ($("a.openimg").size()>0){
$('a.openimg').fancybox({
   padding:0
});
}
if ($("a.opengroup").size()>0){
    $("a.opengroup").attr("rel", "gallery1");  
    $("a.opengroup").fancybox({rel:'group', padding:0});
}
*/
if ($("a.openimg").size()>0){
$('a.openimg').addClass("horizontal").attr("rel", "lightbox");
}

if ($("a.opengroup").size()>0){
   $('a.opengroup').addClass("horizontal").attr("rel", "lightbox1");
}



if ($("p.code").size()>0){
 var vopros= new Array("3+7", "8-3", "4+3", "7+2"); 
 var otvet= new Array("10", "5", "7", "9"); 
 var indexOtvet = getRandomInt(0, 3);
$("p.code").html('<span class="math">'+ vopros[indexOtvet] +'</span> = <input type="text" name="code" class="codes" value="" />');
  $(".math").latex({format: 'png'});
}         
function getRandomInt(min, max)
{
	  return Math.floor(Math.random() * (max - min + 1)) + min;
}


/*
if(!$.browser.msie)
{ $("body").css("overflow-y", "scroll");}
*/
//alert($.browser.version);

$("#toTop").scrollToTop();  

//получаем переменную куки
myVarStyle = getCookie("matheduStyle"); //текущая стилевая таблица
myViewStyle = getCookie("matheduViewMode"); //вид отображения списка книг
myViewCount = getCookie("matheduViewCount"); //кол-ва отобраемых книг
myViewSortMode = getCookie("matheduViewSortMode"); //режим сортировки - по возрастанию или убыванию
myViewSortType = getCookie("matheduViewSortType"); //тип сортировки - по алфавиту или хронологии


//установка соответствующего стиля
setStyleCSS(myVarStyle, urlServer);

if (myVarStyle == null) {
  myVarStyle = setCookie("matheduStyle", "small");
  myVarStyle = "small";
}

//установка режима  отображения списков
if (myViewStyle == null) {
  myViewStyle = setCookie("matheduViewMode", "list");
  myViewStyle = "list";
}

//установка кол-ва страниц вывода
if (myViewCount == null) {
  myViewCount = setCookie("matheduViewCount", "25");
  myViewCount = "25";
}

//направление режима сортировки
if (myViewSortMode == null) {
  myViewSortMode = setCookie("matheduViewSortMode", "asc");
  myViewSortMode = "asc";
}

//тип сортировки (по алфавиту или хронологии)
if (myViewSortType == null) {
  myViewSortType = setCookie("matheduViewSortType", "A");
  myViewSortType = "A";
}

if ( $("ul").is(".nsort") ) {
	  setViewSortModeAndType(myViewSortType, myViewSortMode); //функция, вызывающая все действия по выводу результатов в зависимости от настроек - выбор режимов сортировки, кол-ва выводимого на странице результатов запроса и последующего применения вида отображения списка
}

$(".size button[class=" + myVarStyle +"]").attr("action", "act");

/*
*********
********** обработчик загрузки шрифта ******
**********
*/
		   
$(".size button").click(function(){
	
	var act = $(this).attr("action");
	var cls = $(this).attr("class");
	
	$(".size button").attr("action", "none");
	$(this).attr("action", "act");
	myVarStyle = setCookie("matheduStyle", cls); 
	
	setStyleCSS(cls, urlServer);
	
	
});


function setStyleCSS(cls, urlServer){
    
	//маленький
	if (cls == "small"){
	  
	 $("#big").attr("href", "#");
	 $("#average").attr("href", "#");
		
	//средний
	} else if (cls == "average") { 
	 
	    $("#big").attr("href", "#");
		$("#average").attr("href", urlServer+"css/average.css");
	
	//большой
	} else if (cls == "big") { 
	
	   $("#average").attr("href", "#");
	   $("#big").attr("href", urlServer+"css/big.css");
	
				
	}	
}

/*
*********
********** обработчик календаря на главной ******
**********
*/
//календарь
$(".button button").click(function(){
	
	//текущая область
	var $parent = $(this).parent().parent();
	//позиция
	var position = $(this).attr("numb");
	
	//режим кнопки
	var clasM=$(this).attr("class");
	
	//режим
	//var act = $parent.attr("target");
	//class кнопки
	//var cls = $(this).attr("class");
	//кол-во событий
	var count= parseInt($parent.parent().children().length, 10);
	//текущее место положение
	//var position = parseInt($parent.attr("position"), 10);
	
	//снимаем автоматическое перелистывание календаря
	$('.sladeCalendarj').cycle('stop');
	
	//очищаем области от действия плагина cycle
	for (var i=0; i<count; i++)
	{		
		$parent.parent().children().eq(i).attr("style", "");
		
	}

	//отключаем активную область
	 //$parent.parent().children().attr("target", "none");
	 
	 if (clasM != "act") {
	 
	 	//отключаем все области
	 	$(".sladeCalendarj .date").attr("target", "none");
 		 //включаем нужную
		 $(".sladeCalendarj .date[position="+ position +"]").attr("target", "act");
	 
	 }
	
	/*
	if ( (position>0) && (position< count-1 )){
		
		if (cls == "next") {
		   
		 $parent.parent().children().eq(position+1).attr("target", "act");
	
	  }
	   
	   if (cls == "prev") {
	    
		 $parent.parent().children().eq(position-1).attr("target", "act");
		
	   }
	  
	}
	
	
	
	else if ( (position==0) && (cls == "next") ){
			
		$parent.parent().children().eq(position+1).attr("target", "act");
	}
	
	else if ( (position==(count-1)) && (cls == "prev") ){
		
		$parent.parent().children().eq(position-1).attr("target", "act");
	
	} else { 
	  
	   //активной оставляем текущую область
	   $parent.attr("target","act");
	  
	}
	*/
		
});

$('.sladeCalendarj').cycle({ 
    fx:      'fade', 
    speed:    1000, 
    timeout:  5000
});

 //---------------------------------------------------------------------------------------------------------------------------
 
 
 
 //---------------------------------------------------------------------------------------------------------------------------
 /*
  дерево рубрик
 */
 $(".tree span").click(function(){ 
      var cls = $(this).attr("class");								
      var $ul = $(this).next().next();
	 
	 if (cls == "plus") {
	    
		$(this).attr("class", "minus");
		$ul.show();
		
	 } else if (cls == "minus") {
		 
		$(this).attr("class", "plus");
		$ul.hide();
	
	 }

 });
 
  
//---------------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------------

/*
*********
********** обработчик кол-ва результатов, режимов сортировки и вида списка ******
**********
*/
 $(".setting span.check").click(function(){ 
	
	//var mode = $(this).attr("mode");
	var count = $(this).attr("count");
	var parent = $(this).closest("ul").attr("class");
	
	
//	if (mode == "none"){
	
		if ( !$(this).hasClass("act") ){
	
	
		/*	$("ul."+parent+" li .check").attr("mode", "none");
			$("ul."+parent+" li .check input").attr("checked", "");
			$(this).attr("mode", "act");
			$(this).children().attr("checked", "checked");
		*/	
			//кол-во результатов на странице
			if (parent == "npage"){
			 
			  $("a[rel=number]").text(count);
			  
			  /* действие по изменению кол-ва страниц */
			  myViewCount = count;
			  setCookie("matheduViewCount", count);
			  setViewCount(count);
			
			} else if (parent == "nsort"){
		  	
			  //$("a[rel=sort]").text(count);
			  
			  var modeSort = $("button.bsort").attr("mode");
			 		  
			  /* действие по изменению ражима сортировки */
			  setCookie("matheduViewSortType", count);
			  setViewSortModeAndType(count, "");

			
			} else if (parent =="nview"){
				
				if (count == "list") var src = "/templates/default/images/setting/sp.png";
				else if (count == "icon") var src = "/templates/default/images/setting/img.png";
				
				$("a[rel=view] img").attr("src", src);
				
				/* действие по изменению режима отображения */
								
					myViewStyle = count;
					setCookie("matheduViewMode", count);
					setViewStyle(count, "act");
				
			}
	
	}
	
	
});

 $("button.bsort").click(function(){ 
  
    var mode=$(this).attr("mode");
	
	//var sortType = $(".nsort li span.check[mode=act]").attr("count");
	var sortType = $(".nsort li span.act").attr("count");
	
	//A - по алфавиту
	//T - по хронологии
	
		
	setCookie("matheduViewSortMode", mode);
	setViewSortModeAndType(sortType, mode);

 });
//---------------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------------
$(".year a").click(function(){
	var year = $(this).attr("rel");
	$(".year a").removeClass("act");
	$(this).addClass("act");
	$(".month").attr("mode", "none");
	$(".month[rel="+ year +"]").attr("mode", "view");
							
});
//---------------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------------
/*
обработка вид списка книг
*/
function setViewStyle(mode,  checkAction){
    
   	//найдем состояние кнопки переключения режима отображения
   // var checkAction = $("ul.nview li span[count="+mode+"]").attr("mode");
   

   
  if (checkAction == "act") {
	   
	   //лист
		if (mode == "list"){
	 
		    var src = "/templates/default/images/setting/sp.png";
			
			/*
			$("div.listmode").attr("mode", "act");
			$("div.iconmode").attr("mode", "none");
		    */
			
			$("div.listmode").addClass("act");
			$("div.iconmode").removeClass("act");
		    
			
		    $("ul.list[mode!=art]").removeClass("listNone");
			
		//с иконками и расширенным описанием
		} else if (mode == "icon") { 
	
			var src = "/templates/default/images/setting/img.png";
			
			/*
			$("div.listmode").attr("mode", "none");
			$("div.iconmode").attr("mode", "act");
			*/
			
			$("div.listmode").removeClass("act");
			$("div.iconmode").addClass("act");
				
			$("ul.list[mode!=art]").addClass("listNone");
			
	  
		}
		
		$("a[rel=view] img").attr("src", src);
			
		/*
		$("ul.nview li .check").attr("mode", "none");
		$("ul.nview li .check input").attr("checked", "");
		$("ul.nview li .check[count="+mode+"]").attr("mode", "act");
		$("ul.nview li .check[count="+mode+"]").children().attr("checked", "checked");
		*/
		
		$("ul.nview li .check").removeClass("act");
		$("ul.nview li .check input").attr("checked", "");
		$("ul.nview li .check[count="+mode+"]").addClass("act");
		$("ul.nview li .check[count="+mode+"]").children().attr("checked", "checked");
		
   }

}
//------------------------------------------------------------------------------
function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
//------------------------------------------------------------------------------
 function startDialog(){
  
    $("#dialog" ).dialog({
            autoOpen: false,
            width: 350,
            resizable: false,
            dialogClass: 'bdescription'
    });	

    $("a.bo").click(function(){         
        var rel=$(this).attr("rel");	            
            var $div=$( ".dialog[rel="+rel+"]" );	            
            var title=$div.attr("title");
            if (title.length>100) {
                var letter=title.split(" ");
                var temp='';
                for (var i=0; i<letter.length; i++){
                    if (temp.length < 90){
                        temp=temp + letter[i]+ " "; 
                    }
                }
                    
               //title=title.slice(0, 100) + "...";
               title=temp + " ...";
            }
            $( ".ui-dialog-title" ).attr("title", title);
            $( ".ui-dialog-title" ).html(title);
            $( "#dialog" ).html($div.html());	
            $( "#dialog" ).dialog( "open" );		
            //event.preventDefault();
    });	

    $("div, .bdescription a").removeClass("ui-corner-all");
    $(".ui-dialog-titlebar-close span").removeClass("ui-icon");
    $(".ui-dialog-titlebar-close span").text("");
  }   
//---------------------------------------------------------------------------------------------------------------------------


function setViewCount(count){
$("a[rel=number]").text(count);
/*
$("ul.npage li .check").attr("mode", "none");
$("ul.npage li .check input").attr("checked", "");
$('ul.npage li .check[count="'+ count +'"]').attr("mode", "act");
$('ul.npage li .check[count="'+ count +'"]').children().attr("checked", "checked");
 */


$("ul.npage li .check").removeClass("act");
$("ul.npage li .check input").attr("checked", "");
$('ul.npage li .check[count="'+ count +'"]').addClass("act");
$('ul.npage li .check[count="'+ count +'"]').children().attr("checked", "checked");
 
 
//находим параметры сортировки результатов
var sortMode = $("button.bsort").attr("mode"); //направление сортировки
var sortType = $('ul.nsort span.act').attr("count"); //тип - по алфавиту или хронологии

//sortType A = по алфавиту
//sortType T = по хронологии

var page=getQueryVariable("page");
 //действие по выводу соответсвующего кол-ва страниц (Ajax - запрос)
 
 var urlServer = window.location.protocol+ '//' +window.location.hostname+"/";
 var urlQuery=window.location.pathname;
 /**************************************************************************************************************/
 if (urlQuery.indexOf("/lib/revenues")==0){
    var adress='lib/ajaxRevenues';
    var dataObject={limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page, type: getQueryVariable("type"), mode: getQueryVariable("mode"), dateStart:getQueryVariable("dateStart"), dateEnd:getQueryVariable("dateEnd")};
    var showDialog=false;
 }else if (urlQuery.indexOf("/bookJournal")==0){
    var adress='bookJournal/ajaxIndex';
    var dataObject={limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page,  dateStart:getQueryVariable("dateStart"), dateEnd:getQueryVariable("dateEnd")};
    var showDialog=false;
 }else if (urlQuery.indexOf("/reader")==0){
    var adress='reader/ajaxIndex';
    var dataObject={limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page};
    var showDialog=false;
 }else{
    var adress='lib/ajaxList';
    var dataObject={limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page};
    var showDialog=true; 
 }      
  
 $.ajax({
               url: urlServer+adress,
               //async : false,
               type: "GET",
               data:dataObject,
               timeout: 6000000,
               beforeSend: function(){
                             	
               },
               success: function(data){
                     $("#centerL .text").html(data);  
                     
                     if ( $("ul").is(".nview") ) {
                        var actionM = "act";
                            if (myViewStyle == "list")	actionM = "none";
                            setViewStyle(myViewStyle, actionM); 
                        }
                  screenshotPreview();   
                  if (showDialog) startDialog();   
               }, 
               error: function(xhr, status){
                                           
               }	
     });
 
 
 /*--------------------------------------
 if (urlQuery.indexOf("/lib/revenues")==0){
     
      $.ajax({
               url: urlServer+'lib/ajaxRevenues',
               //async : false,
               type: "GET",
               data:{limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page, type: getQueryVariable("type"), mode: getQueryVariable("mode"), dateStart:getQueryVariable("dateStart"), dateEnd:getQueryVariable("dateEnd")},
               timeout: 6000000,
               beforeSend: function(){
                             	
               },
               success: function(data){
                     $("#centerL .text").html(data);  
                     
                     if ( $("ul").is(".nview") ) {
                        var actionM = "act";
                            if (myViewStyle == "list")	actionM = "none";
                            setViewStyle(myViewStyle, actionM); 
                        }
                     
                     screenshotPreview();
                     
               }, 
               error: function(xhr, status){
                                           
               }	
     });
     
 } else if (urlQuery.indexOf("/bookJournal")==0){
          
        $.ajax({
               url: urlServer+'bookJournal/ajaxIndex',
               //async : false,
               type: "GET",
               data:{limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page,  dateStart:getQueryVariable("dateStart"), dateEnd:getQueryVariable("dateEnd")},
               timeout: 6000000,
               beforeSend: function(){
                             	
               },
               success: function(data){
                     $("#centerL .text").html(data);  
                     
                     if ( $("ul").is(".nview") ) {
                        var actionM = "act";
                            if (myViewStyle == "list")	actionM = "none";
                            setViewStyle(myViewStyle, actionM); 
                        }
                     
                     screenshotPreview();
                     
               }, 
               error: function(xhr, status){
                                           
               }	
     });
     
 } else if (urlQuery.indexOf("/reader")==0){
          
        $.ajax({
               url: urlServer+'reader/ajaxIndexAction',
               //async : false,
               type: "GET",
               data:{limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page},
               timeout: 6000000,
               beforeSend: function(){
                             	
               },
               success: function(data){
                     $("#centerL .text").html(data);  
                     
                     if ( $("ul").is(".nview") ) {
                        var actionM = "act";
                            if (myViewStyle == "list")	actionM = "none";
                            setViewStyle(myViewStyle, actionM); 
                        }
                     
                     screenshotPreview();
                     
               }, 
               error: function(xhr, status){
                                           
               }	
     });
     
 }else{
      $.ajax({
               url: urlServer+'lib/ajaxList',
               //async : false,
               type: "GET",
               data:{limit: count, sort: sortMode, sortType:sortType, url:window.location.pathname, page: page},
               timeout: 6000000,
               beforeSend: function(){
                             	
               },
               success: function(data){
                     $("#centerL .text").html(data);  
                     
                     if ( $("ul").is(".nview") ) {
                        var actionM = "act";
                            if (myViewStyle == "list")	actionM = "none";
                            setViewStyle(myViewStyle, actionM); 
                        }
                  screenshotPreview();   
                  startDialog();   
               }, 
               error: function(xhr, status){
                                           
               }	
     });
    
 }    
 ----------------------------------------*/
 /**************************************************************************************************************/
 

 
 	//установка вида отображения
	//эту функцию надо будет выполнить после выполнения ajaх запроса по выводу страниц в зависимости от кол-ва на странице и режимов сортировки
	if ( $("ul").is(".nview") ) {
           	var actionM = "act";
		if (myViewStyle == "list")	actionM = "none";
		setViewStyle(myViewStyle, actionM); 
	}
 
}



function setViewSortModeAndType(type, mode){
	
  var text = "";
	
  if (mode != "")
  {
	//настройки направления  сортировки
	if (mode == "asc"){
		
		$("button.bsort").attr("mode", "desc");
		$("button.bsort").addClass("desc");
		$("button.bsort").text("по убыванию");
		
		if (type== "A")  text = "Я-А";
		if (type== "T")  text = '<img src="/templates/default/images/setting/tdesc.png" />';
		    
	} else if (mode == "desc"){
	   
	    $("button.bsort").attr("mode", "asc");
		$("button.bsort").removeClass("desc");
		$("button.bsort").text("по возрастанию");
		
		if (type== "A")  text = "А-Я";
		if (type== "T")  text = '<img src="/templates/default/images/setting/tasc.png" />';
	
	}
	
  } else if (mode == ""){
	   mode = $("button.bsort").attr("mode");
	
	   if (mode == "asc"){
		   
		   if (type== "A")  text = "А-Я";
		   if (type== "T")  text = '<img src="/templates/default/images/setting/tasc.png" />';  
		   
	   } else if (mode == "desc"){
		   
		   if (type== "A")  text = "Я-А";
		   if (type== "T")  text = '<img src="/templates/default/images/setting/tdesc.png" />';
	   
	   }
	   
  }
	/*
	//настройки типа сортировки
	 $("ul.nsort li .check").attr("mode", "none");
	 $("ul.nsort li .check input").attr("checked", "");
	 $("ul.nsort li .check[count="+type+"]").attr("mode", "act");
	 $("ul.nsort li .check[count="+type+"]").children().attr("checked", "checked");
    */
	
	//настройки типа сортировки
	 $("ul.nsort li .check").removeClass("act");
	 $("ul.nsort li .check input").attr("checked", "");
	 $("ul.nsort li .check[count="+type+"]").addClass("act");
	 $("ul.nsort li .check[count="+type+"]").children().attr("checked", "checked");
	
	
 	 $("a[rel=sort]").html(text);
		
	 //вызов функции для вывода результатов запроса, Ajax функция настройки вывода там
	 if ( $("ul").is(".npage") ) {
	     setViewCount(myViewCount);
	 }

}

//---------------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------------
$("a.alph").click(function(){
	var rel = $(this).attr("rel");	   
	var $parent = $(this).parent().parent();
	
	$parent.children("div").attr("mode", "none");
	$parent.children("div[rel="+rel+"]").attr("mode", "view");
	
 });
//---------------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------------
/*$(".abs2 a").mouseover(function(){
	
	var rel = $(this).attr("rel");
	
	$(".abs2 div[rel="+ rel +"]").attr("mode", "act");

 });
*/
//---------------------------------------------------------------------------------------------------------------------------



//------------------------------------------SEARCH--------------------------------------------------------------------
 $(".searchDiv span.check").click(function(){ 
	
	var mode = $(this).attr("mode");
	var count = $(this).attr("count");
	var parent = $(this).closest("ul").attr("class");
	
	//if (mode == "none"){
	if ( !$(this).hasClass("act") ) {	
		   /*
		    $("ul."+parent+" li .check").attr("mode", "none");
			$("ul."+parent+" li .check input").attr("checked", "");
			$(this).attr("mode", "act");
			$(this).children().attr("checked", "checked");
         */
		 
		    $("ul."+parent+" li .check").removeClass("act");
			$("ul."+parent+" li .check input").attr("checked", "");
			$(this).addClass("act");
			$(this).children().attr("checked", "checked");
	}
	
});

//----------------------- All Search --------------------------------
$(".group a").click(function(){ 
   
	var rel = $(this).attr("rel");
	
	$(".group a").removeClass("act");
	$(this).addClass("act");
	
	/*
	$(".groupResult div").attr("mode", "none");
	$(".groupResult div[rel="+ rel +"]").attr("mode", "act");
	$(".groupResult div.iconmode").attr("mode", "act");
	*/
	
	
	$(".groupResult div").removeClass("act");
	$(".groupResult div[rel="+ rel +"]").addClass("act");
	
	
	//alert("s");
	//setInterval(alert("s"), 1000);
	
	// if($.browser.msie) {$(".groupResult div ol").addClass("listsIE");}
		
	//$(".groupResult div.iconmode").attr("mode", "act");
	
});
//-------------------------------------------------------------------

$("#searchPhilter a").click(function(){ 
	
	var rel = $(this).attr("rel");
	
	//setTimeout("", 5000);

	
  //********************************************************** 
   if ( $(this).hasClass("close") ) {
	   
	  	   
		 if ( $(this).hasClass("open") ) {
			 
			 $(this).removeClass("open");
			 
			 //actArea
			 $act= $("#searchPhilter a[class=act]");
			 $act.addClass("actClose");
			 
			 /*
			// $(".searchPhilter a").removeClass("act");
			// $(this).addClass("act");
			 $(".searchDiv").attr("mode", "none");
			 //$(".searchDiv[rel="+ rel +"]").attr("mode", "act");
			 */
			 
			
			 $(".searchDiv").removeClass("searchAct");
			 
			 $(".result").addClass("resultTop");
		 
		 } else {
			 
			  
			  $(".searchPhilter a").each(function(){
				 
				 if ( $(this).hasClass("act") ) {
					 $act=$(this);
				 }
			 
			  }); 					   
			  
			 $act.removeClass("actClose");
			 relAct=$act.attr("rel");
			  
			  	  
			  //$(".searchDiv[rel="+ relAct +"]").attr("mode", "act");
			  
			  $(".searchDiv[rel="+ relAct +"]").addClass("searchAct");
			 
			 $(this).addClass("open"); 
			 $(".result").removeClass("resultTop");
			 
		 }
	
	//*********************************************************************	 
    
   } else {
    
	$("#searchPhilter a").removeClass("act");
	$(this).addClass("act");
	//$(".searchDiv").attr("mode", "none");
	//$(".searchDiv[rel="+ rel +"]").attr("mode", "act");
  
     $(".searchDiv").removeClass("searchAct");
	 $(".searchDiv[rel="+ rel +"]").addClass("searchAct");
  
  
   
     if ( !$("#searchPhilter a.close").hasClass("open") )  $("#searchPhilter a.close").addClass("open");
     $(".result").removeClass("resultTop");
		 
   }

});

//---------------------------------------------------------------------------------------------------------------------------
//jQuery.preLoadImages("images/endmenu.png");

//---------------------------------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------------------------------

 $("ul.vestnik li a").click(function(){ 
    
	 $(this).next().toggle();
 }); 
//


 $("p.sdl").click(function(){
	
	var act = urlServer + $(this).children().attr("href");
	
	 //window.location.href=act;
	 window.open( act );
	
});





$("input[code=int]").live("keypress",function(e) {


 if($.browser.msie)
        return isNum(e.keyCode)
  else
        return (e.keyCode) ? true : isNum(e.charCode)

});


function isNum(cCode){
    //return /[0-9\(\)\ \+\-\.]/.test(String.fromCharCode(cCode))
	return /[0-9]/.test(String.fromCharCode(cCode))
}
//----------------------------------------------------


$('.not, .not span, a, img, button, #searh input[type="submit"]').attr('unselectable','on').css('MozUserSelect','none');
$('.not, .not span, a, img, button, #searh input[type="submit"]').mousedown(function(){ return false; });

 //-----------------------------------------------------------------------------
   var $area = $(".info-sender"); 
   
    $(".about-sender").click(function(){
      //var $area = $(".info-sender"); 
      $area.find('.info-user-sender').hide();
      $area.find('.title').text('О рассылке');
      $area.find('.info').show();
      $area.css("margin-top", '-'+( parseInt($area.height()) + 10 )+'px');
     
      $area.toggle();
      
  });
 
  $(".info-sender .close").click(function(){
      $(".info-sender").hide();
  });
//------------------------------------------------------------------------------
$("form#rassilka a.send").click(function(){
   
   $area.hide();
   var email = $(".newsform input[name=email]").val();
   var name = $(".newsform input[name=name]").val();  
  
  
    var form=$(".newsform form").serializeArray();
    
    $.ajax({
               url: urlServer+'indexAjax/userSubscribe',
               //async : false,
               type: "POST",
               //dataType: "json",
               data:{data: form},
               timeout: 6000000,
               beforeSend: function(){
                       $(".newsform p.info-load").show();     	
                       $(".newsform p.info").html("");
               },
               success: function(data){
                         $(".newsform input").removeClass("error");
                        if (data=="error_format_email"){
                          $(".newsform p.info").html("Неверный формат email");  
                          $(".newsform input[name=email]").addClass("error");
                        }else if (data=="error_format_name"){
                          $(".newsform p.info").html("Укажите свое имя");  
                          $(".newsform input[name=name]").addClass("error");
                        }else if (data=="error_count_name"){
                          $(".newsform p.info").html("Указанный email уже подписан");  
                          $(".newsform input[name=email]").addClass("error");
                        }else if (data=="no_add"){
                          $(".newsform p.info").html("Ошибка записи, email не добавлен");  
                        }else if (data=="no_send"){
                          $(".newsform input[name=email]").val("");
                          $(".newsform input[name=name]").val("");  
                          $(".newsform p.info").html("Вы подписаны, но при отправке сообщения об активации произошла ошибка, свяжитесь с администратором");  
                        }else if (data=="ok_add"){
                          $(".newsform input[name=email]").val("");
                          $(".newsform input[name=name]").val("");
                          
                           var textDefault = $area.find(".info-user-sender-default").text();
                           var text = textDefault.replace(/{%user_name%}/g, name);
                               text = text.replace(/{%email%}/g, '<b>'+email+'</b>');
                           $area.find('.info-user-sender').html(text);
   
                           $area.find('.info-user-sender').show();
                           $area.find('.info').hide();
                           $area.find('.title').text('Информация');
                           $area.css("margin-top", '-'+( parseInt($area.height()) + 10 )+'px');
                           $area.show();
      
                           //$(".newsform p.info").html("<span>Вы подписаны, активируйте подписку</span>");  
                        }
                       
                       $(".newsform p.info-load").hide();
                       $("a#send").focus(); 
                       
               }, 
               error: function(xhr, status){
                      $(".newsform p.info-load").hide();
               }	
       });
       
    });
    //--------------------------------------------------------
    $(".form-feedback a.sender").click(function(){
         var form=$(".form-feedback form").serializeArray();
      
        var value = $("p.code input").val();
        if (value==otvet[indexOtvet]) { 
         
         $.ajax({
               url: urlServer+'indexAjax/userFeedback',
               //async : false,
               type: "POST",
               //dataType: "json",
               data:{data: form},
               timeout: 6000000,
               beforeSend: function(){
                       $(".form-feedback p.info").html("");
               },
               success: function(data){
                         $(".form-feedback input").removeClass("error");
                          if (data=="error_format_email"){
                          $(".form-feedback p.info").html("Неверный формат email");  
                          $(".form-feedback input[name=email]").addClass("error");
                        }else if (data=="error_format_name"){
                          $(".form-feedback p.info").html("Укажите свое имя");  
                          $(".form-feedback input[name=name]").addClass("error");
                        }else if (data=="no_add"){
                          $(".form-feedback p.info").html("Ошибка записи, сообщение не отправлено"); 
                          $(".form-feedback form").hide();
                        }else if (data=="ok_add"){
                          $(".form-feedback input[name=email]").val("");
                          $(".form-feedback input[name=name]").val("");
                          $(".form-feedback textarea").val("");
                          $(".form-feedback p.info").html("<span>Ваше сообщение отправлено, мы свяжемся с Вами в ближайшее время.</span>");  
                          $(".form-feedback form").hide();
                        }
                     
               }, 
               error: function(xhr, status){
                   $(".form-feedback p.info").hide();
               }	
       });
       
      }else{
          $(".form-feedback p.info").html("Код проверки введен не правильно!");  
                       
      }
         
    });
    //-------------------------------------------------------
    function preloadImages()
    {
        for(var i = 0; i<arguments.length; i++)
            $("<img />").attr("src", arguments[i]);
    }
    
    preloadImages("/templates/default/images/right/input_error.png");
    
    //-------------------------------------------------------
    $("button.end_activate_button").click(function(){
        var count = $(".activate_end input[type=radio]:checked").size();
        var email = getQueryVariable("email");
        var type = getQueryVariable("type");
        $(".activate_end p.informer").text("");
                
        if ( (parseInt(count) > 0) && (parseInt(type) == 1) ) {
            var sex = $(".activate_end input[type=radio]:checked").val();
            
            $.ajax({
               url: urlServer+'indexAjax/userActivateWihtSex',
               //async : false,
               type: "POST",
               data:{email: email, sex: sex},
               timeout: 6000000,
               beforeSend: function(){
               },
               success: function(data){
                      if (data === "ok"){
                          $(".activate_end").html('Рассылка новостей на email <b>'+ email +'</b> успешно активирована! Спасибо за использование ресурса.'); 
                      }else{
                          $(".activate_end").html('Email '+ email +' не был активирован! Повторите активацию или свяжитесь с администратором сайта');
                      }
                       
               }, 
               error: function(xhr, status){
                   $(".activate_end p.informer").text("При активации учетной записи произошла ошибка, повторите активацию или свяжитесь с администратором сайта! ");
               }	
       });
            
            
        }else{
           $(".activate_end p.informer").text("Укажите Ваш пол!");
        }
      
     });  
    
});
//-----------------------------------------------------------------------------
//func

function setCookie (name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}


function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset)
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}
