<? include DROOT.MAIN_TEMPLATE.'/include/header.php'; ?>
<div class="loader"></div>

<script type="text/javascript">
$(document).ready(function() {    
  // найдем элемент с изображением загрузки и уберем невидимость:
  var textSet = $(".blockDivInfo");
  // вычислим в какие координаты нужно поместить текст с изображением,
  // чтобы он оказалось в серидине страницы:
  var centerY = $(window).scrollTop() + ($(window).height() + textSet.height())/3;
  var winHeight=$(window).scrollTop() + $(window).height();
  //var centerX = $(window).scrollLeft() + ($(window).width() + textSet.width())/2;
  var winWidth = $(window).scrollLeft() + $(window).width();
  // поменяем координаты изображения на нужные:
  //textSet.offset({top:centerY, left:centerX});
  textSet.offset({top:centerY});
  $("#blockDiv").css({"height":winHeight+"px", "width":winWidth+"px"});
 });
</script>

<div id="blockDiv">
   
    <div class="blockDivInfo"><img src="<?=MAIN_TEMPLATE?>/images/block.png"/><br/><?=$this->content?></div>
    
</div>
<div class="wrapper">
            <div class="all">               
            <? include DROOT.MAIN_TEMPLATE.'/include/head_inner.php'; ?>    
            <? include DROOT.MAIN_TEMPLATE.'/include/breadCrumbs.php'; ?>
               
            <div id="containerL">
	            <div id="innerL">
		              
                        <? //include DROOT.MAIN_TEMPLATE.'/include/leftMenu.php'; ?>
                       
                        <div id="centerL"> 
                             
                        </div>
                     

                        <div class="clear"></div>
	           </div>
           </div>
           </div>
           <div class="push"></div>

  </div>
<? include DROOT.MAIN_TEMPLATE.'/include/bottom.php'; ?>
