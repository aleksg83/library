<?php
  ////////////////////////////////////////////////////////////
  // Класс HTML-формы
  ////////////////////////////////////////////////////////////

  class form
  {
    // Массив элементов управления
    public $fields;
    // Название кнопки HTML-формы
    protected $button_name;
    
    protected $action;

    // Класс CSS ячейки таблицы
    protected $css_td_class;
    // Стиль CSS ячейки таблицы
    protected $css_td_style;
    // Класс CSS элемента управления
    protected $css_fld_class;
    // Стиль CSS элемента управления
    protected $css_fld_style;

    // Конструктор класса
    public function __construct($flds, 
                         $button_name, 
                         $action,
                         $css_td_class = "", 
                         $css_td_style = "",
                         $css_fld_class = "",
                         $css_fld_style = "")
    {
      $this->fields       = $flds;
      $this->button_name  = $button_name;
      $this->action  = $action;
      
      $this->css_td_class = $css_td_class;
      $this->css_td_style = $css_td_style;
      $this->css_fld_class = $css_fld_class;
      $this->css_fld_style = $css_fld_style;

      // Проверяем, является ли элементы массива $flds
      // производными класса field
      foreach($flds as $key => $obj)
      {
        if(!is_subclass_of($obj, "field"))
        {
          throw new ExceptionObject($key, 
                "\"$key\" не является элементом управления");
        }
      }
    }

    // Вывод HTML-формы в окно браузера
    public function print_form()
    {
      $enctype = "";
      $twoColumns = false;
      if(!empty($this->fields))
      {
        foreach($this->fields as $obj)
        {
          // Назначаем всем элементам управления единый стиль
          if(!empty($this->css_fld_class))
          {
            $obj->css_class = $this->css_fld_class;
          }
          if(!empty($this->css_fld_class))
          {
            $obj->css_style = $this->css_fld_style;
          }
          // Проверяем нет ли среди элементов управления
          // поля file, если имеется, включаем строку
          // enctype='multipart/form-data'
          if($obj->type == "file")
          {
            $enctype = "enctype='multipart/form-data'";
          }
          
          //проверка, сколько колонок будет у формы - 2 ли 1
          if($obj->column == "columns-2")
          {
            $twoColumns = true;
          }
          
        }
      }

      // Если элементы оформления не пусты - учитываем их
      if(!empty($this->css_td_style))
      {
        $style = "style=\"".$this->css_td_style."\"";
      }
      else $style = "";
      if(!empty($this->css_td_class))
      {
        $class = "class=\"".$this->css_td_class."\"";
      }
      else $class = "";
      
      // Выводим HTML-форму
      $form = '<form name="form"  action="'.$this->action.'" class="editAction" id="validateForm" '.$enctype.' method="POST">';
      
      $form.= "<fieldset><ul>";
      
      if (!$twoColumns) {
          $form.= "<fieldset><ul>";
      }else{
           $form1= '<div class="leftColumn2"><fieldset><ul>';
           $form2= '<div class="rightColumn2"><fieldset><ul>';
      }
      
      if(!empty($this->fields))
      {
        foreach($this->fields as $obj)
        {
          // Получаем название поля, и его HTML-представление
          list($caption, $tag, $help, $alternative) = $obj->get_html();
          if(is_array($tag)) $tag = implode("<br>",$tag);
          
         // if (!empty($obj->style_label)) {$styleLabel = $obj->style_label;}
         // else {$styleLabel=' style="float:left; width:200px;"';}
         
          if($obj->column == "columns-2")
          {
            $twoColumnsField = true; //определяем в правую колонку
          }
          
          #---------------------------------------------------------------------
          if(!empty($obj->css_style_label))
          {
               $style_label = " style=\"".$obj->css_style_label."\"";
              
          }
          else $style_label = "";
          
          if(!empty($obj->css_class_label))
          {
               $class_label = " class=\"".$obj->css_class_label."\"";
              
          }
          else $class_label = "";
         #----------------------------------------------------------------------
          
          switch($obj->type)
          {
            case "hidden":
              // Скрытое поле
              if (!$twoColumns) $form.= $tag;
              else {$form2.= $tag;}
              break;
            case "paragraph":
            case "title":
              $form.="<tr>
                      <td $style $class colspan=2 valign=top>$tag</td>
                     </tr>\n";
              break;

            default:
              // Элементы управления по умолчанию
             if (!$twoColumns) {
                 $form.= "<li{$style_label}{$class_label}>
                         <label class='desc' for='$caption'>$caption</label>
                          $tag
                         </li>";
             }else{
                 //определяем поле в левую колонку
                 if (!$twoColumnsField){
                     $form1.= "<li{$style_label}{$class_label}>
                       <label class='desc' for='$caption'>$caption</label>
                       $tag
                      </li>";
                 //а это в правое    
                 } else {
                     $form2.= "<li{$style_label}{$class_label}>
                       <label class='desc' for='$caption'>$caption</label>
                       $tag
                      </li>";
                 }   
                 
                 
             }   
              if(!empty($help))
              {
                $form.="<tr>
                        <td>&nbsp;</td>
                        <td $style $class valign=top>$help</td>
                      </tr>";
              }
              break;
          }
        }
      }

      
     if (!$twoColumns) {$form.="</ul></fieldset>";}
     else {
         $form1.="</ul></fieldset></div>";
         $form2.="</ul></fieldset></div>";
         $form.=$form1.$form2;
         
         
     }
     $form.="</form>";
     
     return $form;
    }

  // Перегрузка специального метода __toString()
    public function __toString()
    {
      $this->print_form();
    }

    // Метод, проверяющий корректность ввода данных в форму
    public function check()
    {
      // Последовательно вызываем метод check для каждого
      // объекта field, принадлежащих классу
      $arr = array();
      if(!empty($this->fields))
      {
        foreach($this->fields as $obj)
        {
          $str = $obj->check();
          if(!empty($str)) $arr[] = $str;
        }
      }
      return $arr;
    }
  }
?>