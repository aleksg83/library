<?php
class ConstructorArhivsForm extends ConstructorElements {
    
    private $arh;
    private $lang;
        
    public function __construct(arhivEdit $arh, $lang) {
        $this->arh = $arh;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['server_arhivs'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['server_arhivs'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->column = Array (
            "id" => "",
            'name' => $this->lang['server_file_name'],
            'date_add' => $this->lang['form_thead_date_add'],
            'type' => $this->lang['server_file_type'],
            'size' => $this->lang['server_file_size']);
        
        $columns = Array();                
        for($i=0; $i<=count($this->column); $i++){
           if ( ($i==0) || ($i=count($this->column))) $setting.=$i.":{ sorter: false }, ";
        }        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->column as $key => $val) { 
            
             if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
             else $class='';
             
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        
        return  $columns;
        
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $array = $this->arh->getFileArhivs();
        
        $addAct='/adminajax/addArhivFileOrDB/';
        $deleteAct='/admin/deleteArhivs/';
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddArhivsButton($addAct, $this->lang['server_add_arhivs_zip'], $urlPage, " arhiv", "zip");
        $tbody['addMass'] = $panel->getAddArhivsButton($addAct, $this->lang['server_add_arhivs_db'], $urlPage, " arhiv", "db", "ui-icon-script");
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        
        $tr="";
        for ($j=0; $j<count($array); $j++){
         
          $i=0; 
            $tr.='<tr>';
                foreach($array[$j] as $key => $val ){
                   if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                        $tr.="<td class='cnt'>$val</td>";
                   }else if ($key=='type'){
                       if ($val=="zip") $str=$this->lang['server_file_type1'];
                       else if ($val=="sql" || $val=="txt" || $val=="gz") $str=$this->lang['server_file_type2'];
                       
                       $tr.="<td class='cnt'>$str</td>";
                       
                   }else{                    
                        $tr.="<td class='cnt'>$val</td>";                                          
                   }                                                    
                   $i++; 
                }            
            $tr.='<td style="padding-left: 40px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['download'].'" href="'.SITE_URL.'arhivs/'.$id_record.'" target="_blank">
              <span class="ui-icon ui-icon-disk"></span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        }  
        $tbody['tr']=$tr;
       
     return $tbody;
    }   
}

?>