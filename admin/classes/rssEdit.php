<?php
class rssEdit {
    private $db, $error;
    //public $column;
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();   
        Common::ChecksAccessRedactor();
    }
    public function getNewsRSS(){
        $set=$this->getRSSSetting();
        $limit=$set['number'];
        
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_news WHERE is_rss='1' ORDER BY date desc LIMIT ".$limit." ");
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function getRSSSetting(){
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_rss WHERE id_rss='1'");
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    
    public function updateSetting($data){
        
        $data['date'] = Common::getTimeConversionInsertDB($data['date']);
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_rss SET title=?, link=?, description=?, copyright=?, managingEditor=?, webMaster=?, pubDate=?, category=?, number=? WHERE id_rss=?");        
        $sth->bindParam(1, $data['title'], PDO::PARAM_STR);
        $sth->bindParam(2, $data['link'], PDO::PARAM_STR);
        $sth->bindParam(3, $data['description'], PDO::PARAM_STR);
        $sth->bindParam(4, $data['copyright'], PDO::PARAM_STR);
        $sth->bindParam(5, $data['managingEditor'], PDO::PARAM_STR);    
        $sth->bindParam(6, $data['webMaster'], PDO::PARAM_STR);   
        $sth->bindParam(7, $data['date'], PDO::PARAM_STR); 
        $sth->bindParam(8, $data['category'], PDO::PARAM_STR); 
        $sth->bindParam(9, $data['number'], PDO::PARAM_INT); 
        $sth->bindParam(10, $data['id'], PDO::PARAM_INT);
        $sth->execute();        
        $err = $sth->errorInfo();
     
        return ($err[0] != '00000')?false:true;
     
    }
    
     public function update(){
        
        $page = $this->getRSSSetting(); 
        
        if ($page['title']!="") $page['title']='<title>'.$page['title'].'</title>';
        if ($page['link']!="") $page['link']='<link>'.$page['link'].'</link>';
        if ($page['description']!="") $page['description']='<description>'.$page['description'].'</description>';
        if ($page['language']!="") $page['language']='<language>'.$page['language'].'</language>';
        if ($page['copyright']!="") $page['copyright']='<copyright>'.$page['copyright'].'</copyright>';
        if ($page['category']!="") $page['category']='<category>'.$page['category'].'</category>';
        
        
        //setlocale(LC_ALL, 'ru_RU.UTF-8'); 
               
        $page['pubDate']=strftime("%a, %d %b %Y %H:%M:%S %z", strtotime($page['pubDate']));
        $page['lastBuildDate']=strftime("%a, %d %b %Y %H:%M:%S %z", strtotime($page['lastBuildDate']));
        
        $dateStart='<pubDate>'.$page['pubDate'].'</pubDate>';
        $dateUpdate='<lastBuildDate>'.$page['lastBuildDate'].'</lastBuildDate>';
        
        $rss='<?xml version="1.0" encoding="utf-8"?>
               <rss version="2.0">
                    <channel>
                    '.$page['title'].'
                    '.$page['link'].'
                    '.$page['description'].'
                    '.$page['language'].'
                    '.$page['copyright'].'
                    '.$page['category'].'
                    '.$dateStart.'
                    '.$dateUpdate.' 
                        
                    <image>
                        <url>'.SITE_URL.'logorss.gif</url>
                        '.$page['title'].'
                        '.$page['link'].'
                    </image>
            ';
     
      
       $arrayNews = $this->getNewsRSS(); 
      
       for ($j=0; $j<count($arrayNews); $j++){
        
        //проверка на отсутствие в предпринте    
        //если предпринт отключен, не проверяем новости на наличие в предпринте
          $include=false;    
          if ( !Common::is_predprint("news")) $include=true;    
          else if ( Common::is_predprint("news") && !Common::is_predprintResurs($arrayNews[$j]['id_news'], "news") ){
               $include=true;
           }
            
            if ($include){
                 $rss.='<item>
                          <title>'.$arrayNews[$j]['zagolovok'].'</title>
                          <pubDate>'.strftime("%a, %d %b %Y %H:%M:%S %z", strtotime($arrayNews[$j]['date'])).'</pubDate>		
                          <link>'.SITE_URL.URL_NEWS.$arrayNews[$j]['url'].'</link>
                          <description>'.$arrayNews[$j]['annotaciya'].'</description>
                        </item>';
            }
            
            
            
        }
        
      
        $rss.='</channel>
             </rss>'; 
        
        
        $file="rss.xml";
 	$filedirec=DROOT."/".$file;
	$fp = fopen(''.$filedirec.'', 'w');
        $test = fwrite($fp, $rss); // Запись в файл
        $flag=fclose($fp); //Закрытие файла

       if ($flag){
              $id=1;
              $sth = $this->db->prepare("UPDATE ".PREFIX."_rss SET lastBuildDate=? WHERE id_rss=?");        
              $sth->bindParam(1, date("Y-m-d H:i:s"), PDO::PARAM_STR);
              $sth->bindParam(2, $id, PDO::PARAM_INT);
              $sth->execute();        
              $err = $sth->errorInfo();
       }
        
        
        return $flag? true : false ;
         
         
     }
    
    
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    
    public function showForm($id, $act, $action, $url, $lang){
        $rss = $this->getRSSSetting();
        $size=41; 
      
        $parametersDate['column']="columns-2";
        $parametersDate['id']="datepicker";        
        $parametersTextDefinition['column']="columns-2";
          
        $dateAdd =  Common::getTimeConversionForm($rss['pubDate']);
         
        
        $title =     new field_text("title", $lang['rss_zagolovok'], true, $rss['title'], "", $size);         
        $link =      new field_text("link", $lang['rss_link'], true, $rss['link'], "", $size);         
        $description =  new field_text("description", $lang['rss_description'], true, $rss['description'], "", $size);          
        $copyright = new field_text("copyright", $lang['rss_copyright'], false, $rss['copyright'], parametersTextDefinition, $size);          
        $managingEditor = new field_text("managingEditor", $lang['rss_managingEditor'], false, $rss['managingEditor'], "", $size);          
        $webMaster = new field_text("webMaster", $lang['rss_webMaster'], false, $rss['webMaster'], "", $size);          
         
        $date =      new field_text("date", $lang['rss_pudDate'], true, $dateAdd, "", 39, $parametersDate);         
        $category =  new field_text("category", $lang['rss_category'], false, $rss['category'], "", $size, $parametersTextDefinition);          
        $number =    new field_text("number", $lang['rss_number'], false, $rss['number'], "", $size, $parametersTextDefinition);          
         
        
        $id_rec =        new field_hidden_int("id", false, $id);  
        $url =           new field_hidden_int("url", false, $url);
          
        $form = new form(array("title"=>$title,
                               "link" => $link, 
                               "description" => $description,
                               "managingEditor" => $managingEditor,
                               "webMaster" => $webMaster,
                               "date" => $date,
                               "category" => $category,
                               "copyright" => $copyright, 
                               "number" => $number,
                               "id" => $id_rec,
                               "url" => $url,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
       
     }
     //form
     #---------------------------------------------------------------------------------------------
    
}

?>