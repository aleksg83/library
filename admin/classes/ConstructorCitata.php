<?php
class ConstructorCitata extends ConstructorElements {
    
    private $citata;
    private $lang;
    private $idPerson;
        
    public function __construct(CitataEdit $citata, $personId, $lang) {
        $this->citata = $citata;
        $this->lang = $lang;
        $this->idPerson=$personId; 
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            value: "required",
                        },
                        messages: {
                            value: "'.$this->lang['requiredField'].'",
                        }
                      });';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['citataname'];
        $path= '<a href="/admin/showPerson/" title="'.$this->lang['person'].'">'.$this->lang['persons'].'</a>';    
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' >'.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->citata->column = Array(
            "id_sp_citata" => "",
            "value" => ""
            
        );
         
        $columns = Array();                
        for($i=0; $i<=count($this->citata->column); $i++){
           if ( ($i==0) || ($i=count($this->citata->column))) $setting.=$i.":{ sorter: false }, ";
        }        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->citata->column as $key => $val) { 
          //  if ($i==1) $th.="<th>".$this->person->column['fio']."</th>";
           // else if ($i==2) $th.="<th>".$this->person->column['fam_dp']."</th>";
            //else if ($i>0) 
          //  echo $val;
            if ($i==1) $th.="<th>".$this->lang['citataname']."</th>";
            else if ($i>0) 
            $th.="<th>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        $columns['numb'] = count($this->menu->column);
        
        return  $columns;
             
    }
    
    public function getTableTbody($urlPage){
        
        $tbody = Array();
        $arrayCitata = $this->citata->getCitats();
        
        
        $dopParam="?person=".$this->idPerson;
        
        $addAct='/admin/addCitata/'.$dopParam;
        $updateAct='/admin/updateCitata/'.$dopParam;
        $formShow='/admin/formShowCitata/'.$dopParam;
        $deleteAct='/admin/deleteCitata/'.$dopParam;
               
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage.$dopParam, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage.$dopParam);
           
               
        $tbody['fheight']= "220";
        $tbody['fwidth']= "358";
        
        $tr="";$tbody['tr']="";
        for ($j=0; $j<count($arrayCitata); $j++){
            
            unset($arrayCitata[$j]['id_person']);
            
            $i=0; 
            $tr.='<tr>';
                foreach($arrayCitata[$j] as $key => $val ){
                   if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                    }else{                    
                        $tr.="<td>$val</td>";                                          
                    }                                                    
                    $i++; 
                }            
            $tr.='<td style="padding-left: 40px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.$dopParam.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.$dopParam.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        }  
        $tbody['tr']=$tr;
      
     return $tbody;
    }   
}

?>