<?php
class ConstructorMenuInRubric extends ConstructorElements {
    private $menu;
    private $lang;
    private $id;
    private $db;
    
    public function __construct(menuEdit $menu, $id, $lang) {
        $this->menu = $menu;
        $this->lang = $lang;
        $this->id = $id;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = ($this->menu->isRubr)?$this->lang['rubric_in_menu']:$this->lang['menu_in_rubric_razdel']." ".$this->lang['menu_in_rubric_razdel2'];        
        $path =  ($this->menu->isRubr)?'<a href="/admin/showMenu/" title="'.$this->lang['menu'].'">'.$this->lang['menu'].'</a>': '<a href="/admin/showMenu/?rubric=ok" title="'.$this->lang['rubrics'].'">'.$this->lang['rubrics'].'</a>';   
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' >'.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        
        $columns = Array();          
        for($i=0; $i<=2; $i++){
            $setting.=$i.":{ sorter: false }, ";
        }
        $columns['setting'] = $setting;
         
        $this->menu->column = Array ( "name" => $this->lang['form_thead_name_punct'], 
                                       "url" => $this->lang['form_thead_url']
                                     );
        
        $th=$this->menu->isRubr? "<th>".$this->lang['rubric']."</th>" : "<th>".$this->menu->column['name']."</th>";
        $th.="<th>".$this->menu->column['url']."</th>";
        $th.="<th>".$this->lang['connection']."</th>";

        $columns['th'] = $th;   
        
       //$columns['style']='style="width:600px;"';
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();        
        $arraySp = $this->menu->showMenu(); 
                
        #var_dump($this->menu); exit;
        
        $dopParam=($this->menu->isRubr)? "?rubric=ok" : "";
        
        //список связанных с меню рубрик  
        if ($this->menu->isRubr){
            $sth=$this->db->prepare("SELECT id_rub FROM ".PREFIX."_rel_menu_rub WHERE id_menu=?");
            
        }else{
            //echo "меню";
            $sth=$this->db->prepare("SELECT id_menu FROM ".PREFIX."_rel_menu_rub WHERE id_rub=?");
        }
        
        $sth->bindParam(1, $this->id, PDO::PARAM_INT);
        $sth->execute();
        $collection=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        $collectionIdRub=array();
        
        foreach($collection as $key => $val){
           if ($this->menu->isRubr)  {
               array_push($collectionIdRub, $val['id_rub']);
           }else{
               array_push($collectionIdRub, $val['id_menu']);
           }     
        }
       
        
        $tr="";
        for ($j=0; $j<count($arraySp); $j++){
            $level= $arraySp[$j]['level'];
          //  $id_menu_top= $arraySp[$j]['id_menu_top'];
            $id_record=$arraySp[$j]['id_menu'];
            
            //показать рубрики или меню
            $rubric=$arraySp[$j]['is_rubricator'];
           
            
             $left=( ($level-1)*20)."px";
             if ($level>1) {
                 $class='class="tree"'; 
                 $classTr=' class="close"';
             } else{
                 $class="";
                 $classTr='';
             }
             
             //проверка вложенности
             #---------------------------
              $sth1=$this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=?");
              $sth1->bindParam(1, $id_record, PDO::PARAM_INT);
              $sth1->execute();
              //$collection=$sth->fetchAll(PDO::FETCH_ASSOC);
              $nChild=$sth1->rowCount();
              $plus=($nChild>0) ? '<span class="ui-icon treerubric ui-icon-plusthick"></span>' : '' ;
             #--------------------------
             
             $tr.='<tr'.$classTr.'>';
             
             $tr.="<td class=\"rubrics\"><p $class style=\"margin-left:$left;\" idrec=\"".$id_record."\" idtop=\"".$arraySp[$j]['id_menu_top']."\" >".$plus." ".$arraySp[$j]['name']."</p></td>";
             
             $tr.="<td>".$arraySp[$j]['url']."</td>";
             
             if ( in_array($id_record, $collectionIdRub) ){
                  $val=1;
             } else {
                  $val=0;
             }
          
            if ($val == 1) {
              $checked = 'checked="cheked"';
             } else {
              $checked="";
             }       
            $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="is_rubric_menu_'.$j.'" class="checkInRubric" '.$checked.' id_record="'.$id_record.'" id_main="'.$this->id.'" rubric="'.$rubric.'" action="/adminajax/setIsRubricInMenu/" /></td>                      
            </tr>';
        }  
       
        $tbody['tr']=$tr;
        return $tbody;
    }        
}

?>