<?php
class ConstructorBanEmailForm extends ConstructorElements {
    
    private $feed;
    private $lang;
        
    public function __construct(feedbackEdit $feed, $lang) {
        $this->feed = $feed;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '
                $("#validateForm").validate({
                        rules: {
                            email: {
                                required: true,
                                email: true
                            },    
                           
                        },
                        messages: {
                            email: {
                                   required: "'.$this->lang['requiredField'].'",
                                   email: "'.$this->lang['errorEmailRules'].'",    
                           }             
                        }
                      });
                ';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['server_ban_email'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['server_ban_email'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->column = Array (
            "id" => "",
            'email' => $this->lang['form_thead_user_send_email']
       );
        
        $columns = Array();                
        for($i=0; $i<=count($this->column); $i++){
           if ( ($i==0) || ($i=count($this->column))) $setting.=$i.":{ sorter: false }, ";
        }        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->column as $key => $val) { 
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
             else $class='';
             
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        
        return  $columns;
        
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $array = $this->feed->getBanEmail();
        
        $addAct='/admin/addBanEmail/';
        $updateAct='/admin/updateBanEmail/'.$dopParam;
        $formShow='/admin/formShowBanEmail/'.$dopParam;
        $deleteAct='/admin/deleteBanEmail/'.$dopParam;
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        $tbody['right_but'] = $panel->getRightButton($this->lang['server_feedback'], '/admin/feedback/', "ui-icon-comment");
        $tbody['fheight']= "180";
        $tbody['fwidth']= "310";
        
        
        $tr="";
        for ($j=0; $j<count($array); $j++){
          $i=0; 
            $tr.='<tr>';
                foreach($array[$j] as $key => $val ){
                   if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                    }else{                    
                        $tr.="<td>$val</td>";                                          
                    }                                                    
                    $i++; 
                }            
            $tr.='<td style="padding-left: 40px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        }  
        $tbody['tr']=$tr;
       
     return $tbody;
    }   
}

?>