<?php
class arhivEdit {
    private $db,  $error;
    public $column, $folder, $direction, $zip, $dbName;
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        $this->dbName=$sql->getNameDB();
        $this->folder='arhivs';
        $this->direction=DROOT."/".$this->folder."/";
        $this->zip = new ZipArchive();
        Common::ChecksAccessRedactor();
        
    }
   
    public function getFileArhivs(){
       
        $linkDirectory=opendir($this->direction);
        
        $extArray=array ("zip", "sql", "txt", "gz");
        $result=array();
         while ( $d=readdir($linkDirectory)){
          
          if(is_file($this->direction.$d)){
                 
                $ext = substr(strrchr($this->direction.$d, "."), 1);
                
                 if (in_array($ext,$extArray)) {
                     
                     $resultTemp=array( "name"=>$d, 
                                        "date_add"=>date('d.m.Y H:i:s', filemtime($this->direction.$d)), 
                                        "type"=>$ext, 
                                        "size"=>Common::getFileSizeFormat(filesize($this->direction.$d)), 
                                        );
                       
                     array_push($result, $resultTemp);
                 }
              
           }   
        }
        #----------------------------
        function cmp($a, $b) 
        {
            return strnatcmp($b["date_add"], $a["date_add"]);
        }
            usort($result, "cmp"); 
        
        
        return $result;
    }
    #---------------------------------------------------------------------------
    public function addFileArhiv(){
        
        $filename = $this->direction."backup_".date("Y_m_d_H_i").".zip";
        if ($this->zip->open($filename, ZIPARCHIVE::CREATE) !== true) {
             fwrite(STDERR, "Error while creating archive file");
             return false;
        }else{
           ini_set('max_execution_time', 0); 
           
           $dirHandle = opendir(DROOT."/");
           while (false !== ($file = readdir($dirHandle))) {
            if( ($file!='.') &&  ($file!='..') && ($file!=$this->folder) && ($file!='.git') && ($file!=".gitignore") ){
           
                 if (is_dir($file)) {
                     $this->zip->addEmptyDir($file);
                    $this->zip_add_foolder($file."/");     
                 }else if (is_file($file)) {
                   $this->zip->addFile($file, $file);
                 }  
                    
            }    
           }
         
           //закрываем архив
           $this->zip->close();
           return true;
        }
        //-----------------------------------
       
    }
    #---------------------------------------------------------------------------
    public function zip_add_foolder($directory) {
       $dirHandle = opendir($directory);
       while (false !== ($file = readdir($dirHandle))) {
          if( ($file!='.') &&  ($file!='..') && ($file!=$this->folder) && ($file!='.git') && ($file!=".gitignore") ){
              
             if (is_dir($directory.$file)) {
                  $this->zip->addEmptyDir($directory.$file);
                  $this->zip_add_foolder($directory.$file."/");     
             }else if (is_file($directory.$file)){
                 $this->zip->addFile($directory.$file, $directory.$file);
             } 
                    
           }    
     }
    
        
        
    }
    
    #---------------------------------------------------------------------------
    public function addFileDb(){
        $filename=$this->direction."backup_dump_db_".date("Y_m_d_H_i").".sql";
        $insert_records=50; //кол-во записей в одном insert
        $res = $this->db->query("SHOW TABLES");
        $table = $res->fetchAll(PDO::FETCH_ASSOC);
        $fp=fopen($filename, "w"); 
        //------------------------
        foreach ($table as $key=>$val){
            
             $tab=$val['Tables_in_'.$this->dbName];
             
             $res1 = $this->db->query("SHOW CREATE TABLE ".$tab);
             $row1 = $res1->fetchAll(PDO::FETCH_ASSOC);
             $query="\nDROP TABLE IF EXISTS `".$tab."`;\n".$row1[0]['Create Table'].";\n";
             fwrite($fp, $query); $query="";
            
              $r_ins = $this->db->query('SELECT * FROM `'.$tab.'`');
              
              if($r_ins->rowCount()>0){
                        $query_ins = "\nINSERT INTO `".$tab."` VALUES ";
                        fwrite($fp, $query_ins);
                        $i=1;
                    
                        $row=$r_ins->fetchAll(PDO::FETCH_ASSOC); 
                        for ($l=0;$l<count($row); $l++){
                           $query=""; 
                            foreach ( $row[$l] as $field )
                            {
                                    if ( is_null($field) )$field = "NULL";
                                    else $field = "'".mysql_escape_string( $field )."'";
                                    if ( $query == "" ) $query = $field;
                                    else $query = $query.', '.$field;
                            }
                            
                            if($i>$insert_records){
                                $query_ins = ";\nINSERT INTO `".$tab."` VALUES ";
                                fwrite($fp, $query_ins);
                                $i=1;
                            }
                            
                            
                            if($i==1){$q="(".$query.")";}else $q=",(".$query.")";
                            fwrite($fp, $q); 
                            $i++;
                        }
                        fwrite($fp, ";\n"); 
               }          
        }
        
        
        //------------------------  
        fclose ($fp);
        
        /***gzip***/
        $data=file_get_contents($filename);
        $data = gzencode($data, 9);
        $fp = fopen($filename.".gz", "w");
        fwrite($fp, $data);
        fclose($fp);
	unlink($filename);
        
        $return=(file_exists($filename.".gz"))?true:false;
        
        return $return;
         
    }
   #----------------------------------------------------------------------------
    public function delete($id){
       ini_set('max_execution_time', 0);  
       $arrayDel = explode(",", $id);
       for ($i=0; $i<count($arrayDel); $i++){
           if (file_exists($this->direction.$arrayDel[$i])){
                $flag = (@unlink($this->direction.$arrayDel[$i]))?true:false; 
           }
       }
       
       return $flag;
        
    }
    #---------------------------------------------------------------------------
    
}

?>