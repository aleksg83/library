<?php
class settingSystemAndUserEdit {
    private $db;
    public $lang;
    
    public function __construct($lang) {
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->lang = $lang;
        Common::ChecksAccessRedactor();
    }
    
    public function showSettingValue(){
         $sth = $this->db->query("SELECT * FROM ".PREFIX."_main_setting WHERE name!='BlockSaite' ORDER BY name");
         $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    #---------------------------------------------------------------------------
    public function addLibVariables($data, $dopparams){
        $key=Common::encodestring($data['key']);
        $value=$data['value'];
        return $this->prepareLibVariables($key, $value, $dopparams);
    }
    #---------------------------------------------------------------------------

    public function updateLibVariables($data, $dopparams){
         //$key=$data['keyedit'];
         return $this->prepareLibVariables($data['keyedit'], $data['value'], $dopparams, $type="edit");
    }
    
    public function deleteLibVariables($id, $dopparams){
        return $this->prepareLibVariables("", $id, $dopparams, $type="del");
    }
    
    public function prepareLibVariables($key, $value, $dopparams, $type="add"){
        
        $fileTemp=DROOT."/lang/".$dopparams['dir']."/".$dopparams['file'];
        include($fileTemp);
        $array=$lng;
        $value=  Common::removingCharacter($value);
        
        if ($type=="add"){
            if (array_key_exists($key, $array)) {
                 mt_srand(time());
                 $r = mt_rand(0,1000);
                 $key=$key.r;
            }
        }
        
        if ($type=="del"){
             $arrayDel = explode(",", $value);
             foreach ($arrayDel as $key=>$val)
             {
                unset($array[$val]);
             }
        }else{
           $array[$key]=$value; //добавляем новый элемент или обновляем существующий
        }   
        
        $str = '<? ';
        foreach($array as $key=>$value){
                $str .= '$lng[\''.$key.'\']="'.$value.'";';
        }
        $str.= ' ?>';
        if (!file_put_contents($fileTemp, $str, FILE_USE_INCLUDE_PATH)) return FALSE;
        else return TRUE;
        
    }
    #---------------------------------------------------------------------------
    public function showFormLibVariables($id, $act, $action, $url, $lang, $dopparam){
        
         if ($act=="edit"){
             $fileTemp=DROOT."/lang/".$dopparam['dir']."/".$dopparam['file'];
             include($fileTemp);
             $array=$lng;
             $value=$array[$id];
         }
        
         $size=55;
         
        if ($act=="edit") $parametr['disable']="1";
        else $parametr['disable']="0";
         
         $key =     new field_text("key", $lang['setting_lang_key'], true, $id, "", $size, $parametr);         
         $value =   new field_textarea("value", $lang['setting_lang_value'], true, $value, 43, 2, "", "", "", ""); 
         
         $id_rec =  new field_hidden_int("id", false, $id);        
         $url =     new field_hidden_int("url", false, $url);
         $keyedit = new field_hidden_int("keyedit", false, $id);
       
        $form = new form(array( "key"   => $key,
                                "value"   => $value,
                                "id" => $id_rec,
                                "url" => $url,
                                "keyedit" => $keyedit,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
    
     }

    #---------------------------------------------------------------------------
    public function getCodeType($id){
        $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_type_data WHERE id_sp_type_data=?");
        $sth->bindParam(1, $id, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return $result['code'];
    } 
    #---------------------------------------------------------------------------
    
}

?>