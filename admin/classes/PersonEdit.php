<?php
class PersonEdit {   
    private $db;
    
    public function __construct() {
       $sql = new Sql();
       $this->db = $sql->connect();
    }
    
    public function getPersons($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_person WHERE id_person=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_person ORDER by fio");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }        
        return $result;
    }
    
    public function getPersonsNews(){
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_person WHERE is_view_news='1'");
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    public function getPersonsMain(){
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_person WHERE is_view_in_main='1'");
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    public function add($dataPerson){
        //if ($dataPerson['other_name']=="") $dataPerson['other_name']=  Common::encodestring($dataPerson['fio']);
        $dataPerson['short_definition']=$dataPerson['definition'];
        //$url_person = ($dataPerson['url_person']=="")? Common::createURL($dataPerson['fio']) : $dataPerson['url_person'];
        //$url_person = Common::createURL($dataPerson['other_name']);
        $url_person = $this->getUniqueURL( Common::createURL($dataPerson['other_name']) );
        $dataPerson['other_name'] = Common::encodestring($dataPerson['other_name']);
        
        $dataPerson['text']=  Common::removingCharacter($dataPerson['text']);
        $dataPerson['definition']=  Common::removingCharacter($dataPerson['definition']);
        $dataPerson['title'] = ($dataPerson['title']=="") ? $dataPerson['fam_dp'] : $dataPerson['title'] ;
        
        if ($dataPerson['is_view_pam']=="") $dataPerson['is_view_pam']=0;
        if ($dataPerson['is_view_izb']=="") $dataPerson['is_view_izb']=0;
        if ($dataPerson['is_bc']=="") $dataPerson['is_bc']=0;
        if ($dataPerson['is_view_news']=="") $dataPerson['is_view_news']=0;
        if ($dataPerson['is_portret']=="") $dataPerson['is_portret']=0;
        $img='data/person/'.$dataPerson['other_name'].'.jpg';
        
        
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_person (id_users, fam_dp, name, surname, patronymic, fio, other_name, years, day, month, year, is_bc, is_view_pam, is_view_izb, is_view_in_main, is_view_news, definition, short_definition, text, short_text, fio_full_enc, date_full_enc, url, is_in_encyclopedia, is_portret, id_person_forwars, keywords, description, title, photo, add_data) VALUES (:id_users, :fam_dp, :name, :surname, :patronymic,  :fio, :other_name, :years, :day, :month, :year, :is_bc, :is_view_pam, :is_view_izb, :is_view_in_main, :is_view_news, :definition, :short_definition, :text, :short_text, :fio_full_enc, :date_full_enc, :url, :is_in_encyclopedia, :is_portret, :id_person_forwars, :keywords, :description, :title, :photo, :add_data)");
        $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
        $sth->bindParam(":fam_dp", $dataPerson['fam_dp'], PDO::PARAM_STR);
        $sth->bindParam(":name", $dataPerson['name'], PDO::PARAM_STR);
        $sth->bindParam(":surname", $dataPerson['surname'], PDO::PARAM_STR);
        $sth->bindParam(":patronymic", $dataPerson['patronymic'], PDO::PARAM_STR);
        $sth->bindParam(":fio", $dataPerson['fio'], PDO::PARAM_STR);
        $sth->bindParam(":other_name", $dataPerson['other_name'], PDO::PARAM_STR);
        $sth->bindParam(":years", $dataPerson['years'], PDO::PARAM_STR);
        $sth->bindParam(":day", $dataPerson['day'], PDO::PARAM_INT);
        $sth->bindParam(":month", $dataPerson['month'], PDO::PARAM_INT);
        $sth->bindParam(":year", $dataPerson['year'], PDO::PARAM_INT);
        $sth->bindParam(":is_bc", $dataPerson['is_bc'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_pam", $dataPerson['is_view_pam'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_izb", $dataPerson['is_view_izb'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_in_main", $dataPerson['is_view_in_main'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_news", $dataPerson['is_view_news'], PDO::PARAM_INT);
        $sth->bindParam(":definition", $dataPerson['definition'], PDO::PARAM_STR);
        $sth->bindParam(":short_definition", $dataPerson['short_definition'], PDO::PARAM_STR);
        $sth->bindParam(":text", trim($dataPerson['text']), PDO::PARAM_STR);
        $sth->bindParam(":short_text", $dataPerson['short_text'], PDO::PARAM_STR);
        $sth->bindParam(":fio_full_enc", $dataPerson['fio_full_enc'], PDO::PARAM_STR);
        $sth->bindParam(":date_full_enc", $dataPerson['date_full_enc'], PDO::PARAM_STR);
        $sth->bindParam(":url", $url_person, PDO::PARAM_STR);
        $sth->bindParam(":is_in_encyclopedia", $dataPerson['is_in_encyclopedia'], PDO::PARAM_INT);
        $sth->bindParam(":is_portret", $dataPerson['is_portret'], PDO::PARAM_INT);
        $sth->bindParam(":id_person_forwars", $dataPerson['id_person_forwars'], PDO::PARAM_INT);
        $sth->bindParam(":keywords", $dataPerson['keywords'], PDO::PARAM_STR);
        $sth->bindParam(":description", $dataPerson['description'], PDO::PARAM_STR);
        $sth->bindParam(":title", $dataPerson['title'], PDO::PARAM_STR);
        //$sth->bindParam(":photo", Common::downloadFile('data/person/'), PDO::PARAM_STR);
        $sth->bindParam(":photo", $img, PDO::PARAM_STR);
        $sth->bindParam(":add_data", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $sth->execute();
        $err = $sth->errorInfo();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        return ($err[0] != '00000')?false:true;
    }
      
    public function update($dataPerson){
       
        if ($dataPerson['is_view_pam']=="") $dataPerson['is_view_pam']=0;
        if ($dataPerson['is_view_izb']=="") $dataPerson['is_view_izb']=0;
        if ($dataPerson['is_bc']=="") $dataPerson['is_bc']=0;
        if ($dataPerson['is_view_news']=="") $dataPerson['is_view_news']=0;
        if ($dataPerson['is_portret']=="") $dataPerson['is_portret']=0;
        $dataPerson['short_definition']=$dataPerson['definition'];
        //$url_person= ($dataPerson['url_person']=="")? Common::createURL($dataPerson['fio']) : $dataPerson['url_person'];
        //$url_person = $this->getUniqueURL($url_person, "edit", $dataPerson['id']);
        $url_person = $this->getUniqueURL( Common::createURL($dataPerson['other_name']),  "edit", $dataPerson['id'] );
        $dataPerson['title'] = ($dataPerson['title']=="") ? $dataPerson['fam_dp'] : $dataPerson['title'] ;
        $dataPerson['other_name'] = Common::encodestring($dataPerson['other_name']);
        #------------------------------------------------------------------------------
        #File
        if ( ($dataPerson['delImages']=="1") || !empty($_FILES['file']['tmp_name']) ) {
                $person = $this->getPersons($dataPerson['id'] );
                
                 if ( ($person['photo'] != "") && file_exists($person['photo']) ) {
                     unlink($person['photo']);
                 }
        }
       // if ( !empty($_FILES['file']['tmp_name']) )  {
          $newPhoto = true;
          $sql='photo=:photo,';
          $img='data/person/'.$dataPerson['other_name'].'.jpg';
       // }
        
        $dataPerson['text']=  Common::removingCharacter($dataPerson['text']);
        $dataPerson['definition']=  Common::removingCharacter($dataPerson['definition']);
        
        #File
        #------------------------------------------------------------------------------
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_person SET id_users=:id_users, fam_dp=:fam_dp, name=:name, surname=:surname, patronymic=:patronymic, fio=:fio, other_name=:other_name, years=:years, day=:day, month=:month, year=:year, is_bc=:is_bc, is_view_pam=:is_view_pam, is_view_izb=:is_view_izb, is_view_in_main=:is_view_in_main, is_view_news=:is_view_news, definition=:definition, short_definition=:short_definition, text=:text, short_text=:short_text, fio_full_enc=:fio_full_enc, date_full_enc=:date_full_enc, url=:url, is_in_encyclopedia=:is_in_encyclopedia, is_portret=:is_portret, id_person_forwars=:id_person_forwars, keywords=:keywords, description=:description, title=:title, {$sql} update_data=:update_data   WHERE id_person=:id_person");
        $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
        $sth->bindParam(":fam_dp", $dataPerson['fam_dp'], PDO::PARAM_STR);
        $sth->bindParam(":name", $dataPerson['name'], PDO::PARAM_STR);
        $sth->bindParam(":surname", $dataPerson['surname'], PDO::PARAM_STR);
        $sth->bindParam(":patronymic", $dataPerson['patronymic'], PDO::PARAM_STR);
        $sth->bindParam(":fio", $dataPerson['fio'], PDO::PARAM_STR);
        $sth->bindParam(":other_name", $dataPerson['other_name'], PDO::PARAM_STR);
        $sth->bindParam(":years", $dataPerson['years'], PDO::PARAM_STR);
        $sth->bindParam(":day", $dataPerson['day'], PDO::PARAM_INT);
        $sth->bindParam(":month", $dataPerson['month'], PDO::PARAM_INT);
        $sth->bindParam(":year", $dataPerson['year'], PDO::PARAM_INT);
        $sth->bindParam(":is_bc", $dataPerson['is_bc'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_pam", $dataPerson['is_view_pam'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_izb", $dataPerson['is_view_izb'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_in_main", $dataPerson['is_view_in_main'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_news", $dataPerson['is_view_news'], PDO::PARAM_INT);
        $sth->bindParam(":definition", $dataPerson['definition'], PDO::PARAM_STR);
        $sth->bindParam(":short_definition", $dataPerson['short_definition'], PDO::PARAM_STR);
        $sth->bindParam(":text",  trim($dataPerson['text']), PDO::PARAM_STR);
        $sth->bindParam(":short_text", $dataPerson['short_text'], PDO::PARAM_STR);
        $sth->bindParam(":fio_full_enc", $dataPerson['fio_full_enc'], PDO::PARAM_STR);
        $sth->bindParam(":date_full_enc", $dataPerson['date_full_enc'], PDO::PARAM_STR);
        $sth->bindParam(":url", $url_person, PDO::PARAM_STR);
        $sth->bindParam(":is_in_encyclopedia", $dataPerson['is_in_encyclopedia'], PDO::PARAM_INT);
        $sth->bindParam(":is_portret", $dataPerson['is_portret'], PDO::PARAM_INT);
        $sth->bindParam(":id_person_forwars", $dataPerson['id_person_forwars'], PDO::PARAM_INT);
        $sth->bindParam(":keywords", $dataPerson['keywords'], PDO::PARAM_STR);
        $sth->bindParam(":description", $dataPerson['description'], PDO::PARAM_STR);
        $sth->bindParam(":title", $dataPerson['title'], PDO::PARAM_STR);
        //if ($newPhoto) $sth->bindParam(":photo", Common::downloadFile('data/person/'), PDO::PARAM_STR);
        if ($newPhoto) $sth->bindParam(":photo", $img, PDO::PARAM_STR);
        $sth->bindParam(":update_data", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $sth->bindParam(":id_person", $dataPerson['id'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        
        return ($err[0] != '00000')?false:true;
    }
    
    public function delete($idPerson){
        $arrayDel = explode(",", $idPerson);
       #-----------------------------------------------------------------------
        $arrayDelPhoto = array();
        for($i=0; $i<count($arrayDel); $i++){
             $person = $this->getPersons($arrayDel[$i]);
             if ($person['photo'] != "") {
                 array_push($arrayDelPhoto, $person['photo']);
             }
        }
       #-----------------------------------------------------------------------
                
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_person WHERE id_person IN ($place_holders)");
        $sth->execute($arrayDel);        
        $err = $sth->errorInfo();
        
        #-----------------------------------------------------------------------
        if ($err[0] == '00000'){
             for($i=0; $i<count($arrayDelPhoto); $i++){
                if ( ($arrayDelPhoto[$i] != "") && file_exists($arrayDelPhoto[$i]) ) {
                      unlink($arrayDelPhoto[$i]);
                }
             }
           
        }
        #----------------------------------------------------------------------- 
        
        return ($err[0] != '00000')?false:true;
    }
    
     #----------------------------------------------------------------------------------------------  
     public function getUniqueURL($url, $type="add", $id=0){
             //уникальность url 
        if ($type=="add"){
            $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_person  WHERE url=?");
            $sth->bindParam(1, $url, PDO::PARAM_STR);
        }else if ($type=="edit"){    
             $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_person WHERE id_person!=? and url=?");
             $sth->bindParam(1, $id, PDO::PARAM_STR);
             $sth->bindParam(2, $url, PDO::PARAM_STR); 
        }    
            $sth->execute();
            //$err = $sth->errorInfo();
            $count = $sth->rowCount();
           
            if ( $count > 0){
              $i=$count+1;   
              if (strrpos($url, ".html")){
                  $length =  strlen($url)-5;
                  $temp   =  substr($url, 0, $length);
                  $url=$temp."-".$i.".html";
                  
              }else{
                  $url=$url."-".$i;
              }
            }
         #-----------------------------------------------------------------------  
      
        return $url;
    }
    
    
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id, $act, $action, $url, $lang){
         $personAllList = $this->getPersons();
         $person = $this->getPersons($id);
         $person = Common::removeStipsSlashes($person);
         
         $id_user = $_SESSION['user']['user_id'];
        
         $options = array();
         $options[0]=$lang['no_forwars'];
         $parametr[]="";
         for($j=0; $j<count($personAllList); $j++) {
            $value=$personAllList[$j]['fio'];
            $keigen=$personAllList[$j]['id_person'];
            $options[$keigen]=$value;   
         }
         
         $optionsDay = array();
         for($j=0; $j<32; $j++) {
            $optionsDay[$j]=$j;   
         }
         
         $optionsMonth=Common::GetMonth($lang);
         
         $size=41; 
         $size2=143; 
         $class='sm0'; //класс списка
         $classForwars='sm0 comboboxComplite'; //класс списка
         $paramTextField['class']="creditor";
         $paramTextField['id']="creditor_id";
         $paramTextField['column']="columns-2"; //определяем поле в правую колонку формы, форма получиться двух-колоночная
         
         $paramTextFieldDefinition['column']="columns-2";
                 
       
         if ( ($person['photo'] != "") && file_exists($person['photo']) ) {
            $paramImgFieldDefinition['file']='<a href="/'.$person['photo'].'" class="group" target="_blank"><img src="/'.$person['photo'].'" class="photoPerson"/></a>
                                              <span class="photoPerson photoDel">'.$lang['delete_photo'].' <input type="checkbox" name="delImages" value="0"/></span>';
         }else{
            $paramImgFieldDefinition['file']="";
         }
         
         
         $flg  = ($person['is_view_in_main']=="1")?true:false;   
         $flg2 = ($person['is_in_encyclopedia']=="1")?true:false;   
         $flg3 = ($person['is_bc']=="1")?true:false;   
         $flg4 = ($person['is_view_news']=="1")?true:false;   
         $flg5 = ($person['is_portret']=="1")?true:false;   
         
         $fam_dp =        new field_text("fam_dp", $lang['form_thead_fam_dp'], false, $person['fam_dp'], "", $size);         
         //------------------ reserv ------------------
         //$name =          new field_text("name", $lang['form_thead_name'], true, $person['name'], "", $size);         
         $name =          new field_hidden_int("name", false, "");  
         //$surname =       new field_text("surname", $lang['surname'], true, $person['surname'], "", $size);         
         $surname =       new field_hidden_int("surname", false, "");   
         //$patronymic =    new field_text("patronymic", $lang['patronymic'], true, $person['patronymic'], "", $size);         
         $patronymic =    new field_hidden_int("patronymic", false, "");   
         //------------------ reserv ------------------
         
         $fio =           new field_text("fio", $lang['person_fio'], false, $person['fio'], "", $size);         
         $other_name =    new field_text("other_name", $lang['other_name'], true, $person['other_name'], "", $size);         
         $years =         new field_text("years", $lang['person_years_life'], false, $person['years'], "", $size);         
         $day =           new field_select("day", $lang['person_day'], $optionsDay, $person['day'], false, "", "", "", "", $class); 
         $month =         new field_select("month", $lang['person_month'], $optionsMonth, $person['month'], false, "", "", "", "", $class); 
         $year =          new field_text("year", $lang['person_year'], false, $person['year'], "", $size);         
         $is_bc =         new field_checkbox("is_bc", $lang['person_is_bc'], $flg3, "");   
         $is_view_in_main = new field_checkbox("is_view_in_main", $lang['is_view_in_main'], $flg, "");   
         $is_view_news =  new field_checkbox("is_view_news", $lang['is_view_person_news'], $flg4, "");   
         $is_portret =    new field_checkbox("is_portret", $lang['person_is_portret'], $flg5, "");   
         //$urlPerson =     new field_text("url_person", $lang['person_url'], false, $person['url'], "", $size);         
         $urlPerson =     new field_hidden("url_person", false, "");
         
         $text =          new field_textarea("text", $lang['person_text'], false, $person['text'], 38, 10, "", "", "", $paramTextField); 
         $fio_full_enc =    new field_text("fio_full_enc", $lang['person_fio_full_enc'], false, $person['fio_full_enc'], "", $size2, $paramTextFieldDefinition); 
         $date_full_enc =    new field_text("date_full_enc", $lang['person_date_full_enc'], false, $person['date_full_enc'], "", $size2, $paramTextFieldDefinition); 
         $definition =    new field_text("definition", $lang['person_definition'], false, $person['definition'], "", $size2, $paramTextFieldDefinition); 
         $is_in_encyclopedia = new field_checkbox("is_in_encyclopedia", $lang['is_in_encyclopedia'], $flg2, "");   
         $short_text =    new field_textarea("short_text", $lang['short_text'], false, $person['short_text'], 109, 6, "", "", "", $paramTextFieldDefinition); 
         $forwars =       new field_select( "id_person_forwars", $lang['person_forwars'], $options, $person['id_person_forwars'], false, "", "", $parametr, "", $classForwars); 
         
         $title =         new field_text("title", $lang['title'], false, $person['title'], "", $size2, $paramTextFieldDefinition);         
         //$description =   new field_text("description", $lang['meta_description'], false, $person['description'], "", $size, $paramTextFieldDefinition);         
         $description =   new field_textarea("description", $lang['meta_description'], false, $person['description'], 109, 2, "", "", "", $paramTextFieldDefinition); 
         $keywords =      new field_text("keywords", $lang['meta_keywords'], false, $person['keywords'], "", $size2, $paramTextFieldDefinition);         
         
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
         
         //$photo =         new field_file("file", $lang['person_photo'], false, "", "", $paramImgFieldDefinition);         
         $photo =           new field_hidden_int("file", false, "");
         
        $form = new form(array("other_name" => $other_name,
                               "fam_dp"   => $fam_dp,
                               "surname" => $surname,
                               "name"   => $name,
                               "patronymic" => $patronymic,
                               "fio" =>$fio,
                               "years" => $years,
                               "day" => $day,
                               "month" =>$month, 
                               "year" => $year,
                               "is_bc" => $is_bc,
                               "id_person_forwars" => $forwars,
                               "url_person" => $urlPerson,
                               "is_in_encyclopedia" => $is_in_encyclopedia,
                               "is_portret" => $is_portret,
                               "is_view_in_main" => $is_view_in_main, 
                               "is_view_news" => $is_view_news,
                               "file" => $photo,
                                           
                               "fio_full_enc" => $fio_full_enc,
                               "date_full_enc" => $date_full_enc,
                               "definition" => $definition, 
                               "text"   => $text, 
                               "short_text" =>$short_text,
                               "title" => $title,
                               "description" => $description,
                               "keywords" => $keywords,
                               "id" => $id_rec,
                               "url" => $url,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
     }
     //form
     #---------------------------------------------------------------------------------------------
}

?>