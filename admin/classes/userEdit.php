<?php

class userEdit {
    private $db;
    
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getUser($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_users WHERE id_users=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_users");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }        
        return $result;
    }
    
    public function delete($id){
        $arrayDel = explode(",", $id);
        
        //нельзя удалить админа, его id=1 
        if(($key = array_search('1', $arrayDel)) !== FALSE){
	     unset($arrayDel[$key]);
	}
        if (count($arrayDel)>0) {
            
        
            foreach($arrayDel as $key=>$val){
                $this->clearUserRoles($val);
            } 
            $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
            $sth = $this->db->prepare("DELETE FROM ".PREFIX."_users WHERE id_users IN ($place_holders)");
            $sth->execute($arrayDel);
            $err = $sth->errorInfo();
            return ($err[0] != '00000')?false:true;
            
        }else{
            return false;
        }
        
    }
    
    public function add($userData){
        $userData=$this->prepareInsertData($userData);
        
        $this->db->beginTransaction();
        
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_users (login, password, name, fam, otch, email, is_active, is_blocked, phone, prioritet) VALUES (?, MD5(?), ?, ?, ?, ?, ?, ?, ?, ?)");
        $sth->bindParam(1, strtolower($userData['login']), PDO::PARAM_STR);
        $sth->bindParam(2, $userData['password'], PDO::PARAM_STR);
        $sth->bindParam(3, $userData['name'], PDO::PARAM_STR);
        $sth->bindParam(4, $userData['fam'], PDO::PARAM_STR);
        $sth->bindParam(5, $userData['otch'], PDO::PARAM_STR);
        $sth->bindParam(6, $userData['email'], PDO::PARAM_STR);
        $sth->bindParam(7, $userData['is_active'], PDO::PARAM_STR);
        $sth->bindParam(8, $userData['is_blocked'], PDO::PARAM_STR);
        $sth->bindParam(9, $userData['phone'], PDO::PARAM_STR);
        $sth->bindParam(10, $userData['prioritet'], PDO::PARAM_STR);
        $sth->execute();
        $idUsers=$this->db->lastInsertId();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        #---------
           $sthR = $this->db->query("SELECT * FROM ".PREFIX."_roles WHERE code='REDACTOR'");  
           $rolesRedactor=$sthR->fetchAll(PDO::FETCH_ASSOC);
           $this->setUserRol($idUsers, $rolesRedactor[0]['id_roles']);
           
        #---------
        $err = $sth->errorInfo();
        $this->error = ($err[0] != '00000')?0:1;
        
        if ($this->error == 1){
            $this->db->commit();
            return true;
        }else{
            $this->db->rollBack();
            return false;
        }
        //return ($err[0] != '00000')?false:true;
    }
    
    public function update($userData){
        $userData=$this->prepareInsertData($userData);
        
        if ($userData['password_redact']!=""){
            $sqlPassword=' password=MD5(:password), ';
            $flagPassword=true;
        }else{
            $sqlPassword='';
            $flagPassword=false;
        }
        if ($userData['prioritet']!=""){
            $sqlPrioritet=', prioritet=:prioritet  ';
            $flagPrioritet=true;
        }else{
            $sqlPrioritet='';
            $flagPrioritet=false;
        }
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_users SET {$sqlPassword} name=:name, fam=:fam, otch=:otch, email=:email, is_active=:is_active, phone=:phone {$sqlPrioritet} WHERE id_users=:id_users");
       
        if ($flagPassword) $sth->bindParam(":password", $userData['password_redact'], PDO::PARAM_STR);
        $sth->bindParam(":name", $userData['name'], PDO::PARAM_STR);
        $sth->bindParam(":fam", $userData['fam'], PDO::PARAM_STR);
        $sth->bindParam(":otch", $userData['otch'], PDO::PARAM_STR);
        $sth->bindParam(":email", $userData['email'], PDO::PARAM_STR);
        $sth->bindParam(":is_active", $userData['is_active'], PDO::PARAM_INT);
        $sth->bindParam(":phone", $userData['phone'], PDO::PARAM_STR);
        if ($flagPrioritet) $sth->bindParam(":prioritet", $userData['prioritet'], PDO::PARAM_INT);
        $sth->bindParam(":id_users", $userData['id'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    
     public function updateProfile($userData){
      #-- старый пароль -----   
      if ($userData['password_new'] !="") {
          $infoUs=$this->getUser($userData['id_users']);
          $passOldmd5=$infoUs['password'];
      }
      
      
      if ( ($userData['password_new']=="") || ( ($userData['password_new'] !="") && (md5($userData['password_old'])==$passOldmd5) ) ) {    
        
         if ($userData['password_new']!=""){
            $sqlPassword=' password=MD5(:password), ';
            $flagPassword=true;
        }else{
            $sqlPassword='';
            $flagPassword=false;
        }
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_users SET {$sqlPassword} name=:name, fam=:fam, otch=:otch, email=:email, phone=:phone WHERE id_users=:id_users");
       
        if ($flagPassword) $sth->bindParam(":password", $userData['password_new'], PDO::PARAM_STR);
        $sth->bindParam(":name", $userData['name'], PDO::PARAM_STR);
        $sth->bindParam(":fam", $userData['fam'], PDO::PARAM_STR);
        $sth->bindParam(":otch", $userData['otch'], PDO::PARAM_STR);
        $sth->bindParam(":email", $userData['email'], PDO::PARAM_STR);
        $sth->bindParam(":phone", $userData['phone'], PDO::PARAM_STR);
        $sth->bindParam(":id_users", $userData['id_users'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        
        return ($err[0] != '00000')?false:true;
        
      }else{
        return  false;
      }  
      
    }
    
    
    #--------------------------------------------------------------------
    public function prepareInsertData($data, $type="add"){
         if ($data['is_active']=="") $data['is_active']=0;
         if ($data['is_blocked']=="") $data['is_blocked']=0;
        
        return $data;
    }
    #---------------------------------------------------------------------
    
    
    public function getRoles($userId=null){
        if($userId){
            $sth=$this->db->prepare("select 
                                        rol.*
                                    from ".PREFIX."_roles rol
                                        left join ".PREFIX."_rel_users_roles rur using(id_roles)                                     
                                     where rur.id_users = ?");
            $sth->bindValue("1", $userId, PDO::PARAM_STR);						 
            $sth->execute();
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_roles");            
        }
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getRoleInfo($id){
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_roles WHERE id_roles=?");  
        $sth->bindParam(1, $id, PDO::PARAM_INT);						 
        $sth->execute();
        $result=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    
    public function setUserRol($idUser, $idRoles){
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_users_roles (id_users, id_roles) VALUES (?, ?)");
        $sth->bindParam(1, $idUser, PDO::PARAM_INT);
        $sth->bindParam(2, $idRoles, PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    
    
    public function setUserRoles($idUser, $idRoles){
        $this->clearUserRoles($idUser);
        $this->db->beginTransaction();
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_users_roles (id_users, id_roles) VALUES (?, ?)");
        foreach($idRoles as $idRole){
            $sth->bindParam(1, $idUser, PDO::PARAM_INT);
            $sth->bindParam(2, $idRole, PDO::PARAM_INT);
            $sth->execute();
            $err = $sth->errorInfo();
            $result[] = ($err[0] != '00000')?0:1;
        }
        if(in_array(0, $result)){
            $this->db->rollBack();
            return false;
        }else{
            $this->db->commit();
            return true;
        }
    }
    
    private function clearUserRoles($idUser){
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_users_roles WHERE id_users = ?");
        $sth->bindParam(1, $idUser, PDO::PARAM_STR);
        $sth->execute();
        $err = $sth->errorInfo();       
        return ($err[0] != '00000')?false:true;
    }
    #--------------------------------------------------------------------------------------------
    public function showForm($id, $act, $action, $url, $lang){
        $result = $this->getUser($id);
        
        //----- print ? -----------
        $printGroup=false;
        if ($act=="edit"){
            $roles=$this->getRoles($id);
        
            for ($i=0; $i<count($roles); $i++ ){
              if  ( ($roles[$i]['code']=="PRINTRESURS") || ($roles[$i]['code']=="PRINTNEWS") )  {
                  $printGroup=true;   
              }    
            }
        }    
        //-------------------------------------------
        
        $size=41; 
        $size2=10; 
        $paramCheck['column']="columns-2";
        if ($act=="edit") $parametr['disable']="1";
        else $parametr['disable']="0";
        
        $flg = (($act=="add") or ($result['is_active']=="1"))?true:false; 
        
        $login =  new field_text("login", $lang['login'], true, $result['login'], "", $size, $parametr);
        
        if ($act=="add") {
            $password =     new field_password("password", $lang['password'], true, "", "", $size);
            $password_redact  = new field_hidden_int("password_redact", false, "password");
        }else{
            $password_redact =   new field_password("password_redact", $lang['password'], false, "", "", $size);
            $password  = new field_hidden_int("password", false, "password");
        }
        
        $fam =          new field_text("fam", $lang['user_fam'], false, $result['fam'], "", $size, $paramCheck);
        $name =         new field_text("name", $lang['user_name'], false, $result['name'], "", $size, $paramCheck);
        $otch =         new field_text("otch", $lang['user_otch'], false, $result['otch'], "", $size, $paramCheck);
        $email =        new field_text("email", $lang['user_email'], false, $result['email'], "", $size, $paramCheck);
        $phone =        new field_text("phone", $lang['user_phone'], false, $result['phone'], "", $size, $paramCheck);
        if ($result['login']!="admin"){
            $is_active =    new field_checkbox("is_active", $lang['user_is_active'], $flg, $paramCheck);
        }else{
            $is_active =    new field_hidden_int("is_active", false, "1");
        }
        
        if ($printGroup){
            $prioritet =    new field_text("prioritet", $lang['user_prioritet'], false, $result['prioritet'], "", $size2);
        }else{
            $prioritet =    new field_hidden_int("prioritet", false, "");
        }
        
        $type_act   = new field_hidden_int("type_act", false, $act);
        $id   = new field_hidden_int("id", false, $id);
        $url  = new field_hidden_int("url", false, $url);
         
        
        $form = new form(array("login" => $login,
                               "password" => $password,
                               "password_redact" => $password_redact,
                               "prioritet" =>$prioritet,
            
                               "fam" => $fam,
                               "name"   => $name,
                               "otch" => $otch,
                               "email" => $email,
                               "phone" => $phone,
                                
                               "is_active" => $is_active,
                               "id" => $id,
                               "url" => $url,
                               "type_act" => $type_act, 
                                ), 
                                "", 
                                $action);
         
       return  $form->print_form();
       
     }
        
    
    
    #---------------------------------------------------------------------------------------------
      public function addRoles($Data){
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_roles (name, code) VALUES (?, ?)");
        $sth->bindParam(1, $Data['name'], PDO::PARAM_STR);
        $sth->bindParam(2, $Data['code'], PDO::PARAM_STR);
        $sth->execute();
        $err = $sth->errorInfo();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        return ($err[0] != '00000')?false:true;
    }
    
     public function updateRoles($Data){
         
         if ($Data['code'] != ""){
             $codeSql=', code=:code';
             $flagCode=true;
         }else{
             $codeSql='';
             $flagCode=false;
         } 
         
        $sth = $this->db->prepare("UPDATE ".PREFIX."_roles SET name=:name{$codeSql} WHERE id_roles=:id_roles");
        $sth->bindParam(":name", $Data['name'], PDO::PARAM_STR);
        if ($flagCode) $sth->bindParam(":code", $Data['code'], PDO::PARAM_STR);
        $sth->bindParam("id_roles", $Data['id'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    
    
      public function deleteRoles($id){
        $arrayDel = explode(",", $id);
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_roles WHERE id_roles IN ($place_holders)");
        $sth->execute($arrayDel);
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    
    public function showFormRoles($id, $act, $action, $url, $lang){
        $result = $this->getRoleInfo($id);
        $size=41; 
        
        
        if ( ($act=="edit") && ($result[0]['code']=='ADMIN') ) $parametr['disable']="1";
        else if ( ($act=="edit") && ($result[0]['code']=='REDACTOR') ) $parametr['disable']="1";
        else $parametr['disable']="0";
        
        $name = new field_text("name", $lang['form_thead_user_roles_name'], true, $result[0]['name'], "", $size);
        $code = new field_text("code", $lang['form_thead_user_roles_code'], true, $result[0]['code'], "", $size, $parametr);
        
        $id   = new field_hidden_int("id", false, $id);
        $url  = new field_hidden_int("url", false, $url);
         
        
         $form = new form(array("name"   => $name,
                                "code" => $code,
                                "id" => $id,
                                "url" => $url,
                                ), 
                                "", 
                                $action);
         
       return  $form->print_form();
     }
     
     public function getLoginCount($act, $login){
       $count= ($act=="add") ? 0: 1;
        
       $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_users WHERE login=?");
       $sth->bindParam(1, $login, PDO::PARAM_STR);
       $sth->execute();
       $countRecord = $sth->rowCount();
       //$result = $sth->fetch(PDO::FETCH_ASSOC);  
       $result = ($countRecord>$count) ? "yes" : "no" ;
       return $result;     
     }
    
}

?>