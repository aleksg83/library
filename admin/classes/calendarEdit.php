<?php
class calendarEdit {
    
    private $db;
    public $type;
    
            
    public function __construct($type) {
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->type=$type; 
    }
    
    public function getEvents($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_calendar WHERE id_calendar=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_calendar ORDER BY date_from");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }        
        return $result;
    }
    
    public function addEvent($eventData){
        $eventData=$this->prepareInsertData($eventData);
        
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_calendar (date_from, date_to, title, text, repeat_type, repeat_value, url, is_bc, is_visible, is_main, id_users) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $sth->bindParam(1, $eventData['date_from'], PDO::PARAM_STR);
        $sth->bindParam(2, $eventData['date_to'], PDO::PARAM_STR);
        $sth->bindParam(3, $eventData['title'], PDO::PARAM_STR);
        $sth->bindParam(4, $eventData['text'], PDO::PARAM_STR);
        $sth->bindParam(5, $eventData['repeat_type'], PDO::PARAM_INT);
        $sth->bindParam(6, $eventData['repeat_value'], PDO::PARAM_INT);
        $sth->bindParam(7, $eventData['url_ev'], PDO::PARAM_STR);
        $sth->bindParam(8, $eventData['is_bc'], PDO::PARAM_INT);
        $sth->bindParam(9, $eventData['is_visible'], PDO::PARAM_INT);
        $sth->bindParam(10, $eventData['is_main'], PDO::PARAM_INT);
        $sth->bindParam(11, $_SESSION['user']['user_id'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        return ($err[0] != '00000')?false:true;
    }
    public function updateEvent($eventData){
         $eventData=$this->prepareInsertData($eventData);
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_calendar SET  date_from=?, date_to=?, title=?, text=?, repeat_type=?, repeat_value=?, url=?, is_bc=?, is_visible=?, is_main=? WHERE id_calendar=?");
        $sth->bindParam(1, $eventData['date_from'], PDO::PARAM_STR);
        $sth->bindParam(2, $eventData['date_to'], PDO::PARAM_STR);
        $sth->bindParam(3, $eventData['title'], PDO::PARAM_STR);
        $sth->bindParam(4, $eventData['text'], PDO::PARAM_STR);
        $sth->bindParam(5, $eventData['repeat_type'], PDO::PARAM_INT);
        $sth->bindParam(6, $eventData['repeat_value'], PDO::PARAM_INT);
        $sth->bindParam(7, $eventData['url_ev'], PDO::PARAM_STR);
        $sth->bindParam(8, $eventData['is_bc'], PDO::PARAM_INT);
        $sth->bindParam(9, $eventData['is_visible'], PDO::PARAM_INT);
        $sth->bindParam(10, $eventData['is_main'], PDO::PARAM_INT);
        $sth->bindParam(11, $eventData['id'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    public function deleteEvent($id){
        $arrayDel = explode(",", $id);
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_calendar WHERE id_calendar IN ($place_holders)");
        $sth->execute($arrayDel);
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    
    public function prepareInsertData($eventData){
        if ($eventData['is_bc']=="") $eventData['is_bc']=0;
        if ($eventData['is_visible']=="") $eventData['is_visible']=0;
        if ($eventData['is_main']=="") $eventData['is_main']=0;
        
        $eventData['date_from'] = Common::getTimeConversionInsertDB($eventData['date_from']);
        $eventData['date_to'] =($eventData['date_to']=="")? $eventData['date_from'] : Common::getTimeConversionInsertDB($eventData['date_to']);
        
        $eventData['repeat_value']=intval($eventData['repeat_value']);
        
        $eventData['text']=  Common::removingCharacter($eventData['text']);
        
        return $eventData;
    }
    
    #---------------------------------------------------------------------------
    //showForm
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id=null, $act, $action, $url, $lang){
       
        $result = Common::removeStipsSlashes($this->getEvents($id));
        
        $optionsRepeat = array();
        $optionsRepeat[0]=$lang['cal_repeat_no'];
        $optionsRepeat[1]=$lang['cal_repeat_day'];
        $optionsRepeat[2]=$lang['cal_repeat_week'];
        $optionsRepeat[3]=$lang['cal_repeat_month'];
        $optionsRepeat[4]=$lang['cal_repeat_year'];
        $class='level sm0'; //класс списка
        
        
        $size=41; 
        $parametersDate['id']="datepicker_1";     
        $parametersDate2['id']="datepicker_2";  
        $paramTextFieldDefinition['column']="columns-2";
        
        $flg = ( ($act=="add") || ($result['is_visible']=="1") ) ? true:false;  
        $flg2 = ($result['is_bc']=="1")?true:false;   
        $flg3 = ($result['is_main']=="1")?true:false;   
        
        if ($act=="add") $dateFrom=  date ("d.m.Y");
        
        if ($act=="edit") $dateFrom = Common::getTimeConversionForm($result['date_from']);
        if ($act=="edit") $dateTo =  Common::getTimeConversionForm($result['date_to']);
        
        $type= $result['repeat_type'];
        
        $date =          new field_text("date_from", $lang['date_from'], false, $dateFrom, "", 39, $parametersDate);         
        $date2 =         new field_text("date_to", $lang['date_to'], false, $dateTo, "", 39, $parametersDate2);         
        $repeat_type =   new field_select("repeat_type", $lang['cal_repeat_type'], $optionsRepeat, $type, false, "", "level", "", "", $class); 
        $repeat_value =  new field_text("repeat_value", $lang['cal_repeat_value'], false, $result['repeat_value'], "", $size,"");         
        $url_ev =           new field_text("url_ev", $lang['cal_url'], false, $result['url'], "", $size,"");         
        $title =         new field_text("title", $lang['calendar_name'], true, $result['title'], "", $size, $paramTextFieldDefinition);         
        $text =          new field_textarea("text", $lang['calendar_description'], false, $result['text'], 32, 15, "", "", "", $paramTextFieldDefinition); 
        $is_visible =    new field_checkbox("is_visible", $lang['is_visible'], $flg);  
        //$is_bc =         new field_checkbox("is_bc", $lang['calendar_is_bc'], $flg2, ""); 
        $is_bc =           new field_hidden_int("is_bc", false, 0);
        $is_main =       new field_checkbox("is_main", $lang['calendar_is_main'], $flg3, "");   
        
        $id_rec =        new field_hidden_int("id", false, $id);        
        $url =           new field_hidden_int("url", false, $url);
         
        $form = new form(array("date_from" => $date,
                               "date_to" => $date2,
                               "repeat_type"=> $repeat_type,
                               "repeat_value" => $repeat_value,
                               "url_ev" => $url_ev,
                               "is_bc" => $is_bc,
                               "is_visible"   => $is_visible,
                               "is_main"   => $is_main,
                               "title"   => $title,
                               "text" => $text,
                               "id" => $id_rec,
                               "url" => $url,
                                ),
                             "",
                             $action);        
        return  $form->print_form(); 
    }
    #---------------------------------------------------------------------------
    
}

?>