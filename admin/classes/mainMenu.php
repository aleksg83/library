<?php
class mainMenu {
    private $db, $sql;
    public $lang, $auth;
    
    public function __construct($lang) {
        $this->sql = new Sql();
        $this->db = $this->sql->connect();
        $this->lang=$lang;
        
        $this->auth = auth::instance(); 
    }
    
    public function createSp(){
        return $this->sql->getSP();
    }
    
    public function mainMenuList(){
       
       $spList='';
       foreach($this->createSp() as $sp) :
          if ($this->lang['main_menu_sp_'.$sp['TABLE_NAME'].'']!="") {
            $spList.='<li><a href="/admin/showSp/'.$sp['TABLE_NAME'].'/">'.$this->lang['main_menu_sp_'.$sp['TABLE_NAME'].''].'</a></li>';
          }
       endforeach;
       //--------------- Admin --------------------------
      if ($this->auth->isAdmin) { 
       if (Common::isBlock()) { 
            $blockSite='<li><a href="javascript:void(0)" class="BlockSite" mode="0">'.$this->lang['blockSiteVk'].'</a></li>';
        }else{
            $blockSite='<li><a href="javascript:void(0)" class="BlockSite" mode="1">'.$this->lang['blockSite'].'</a></li>';
        }
        
        $menuInRubric='
                   <li><a href="/admin/showMenu/?rubric=ok">Рубрики</a></li>        
                   <li><a href="/admin/showMenu/">Меню</a></li>';
                  
        
        $service='
         <li>
                <a href="#" class="sf-with-ul">Сервисы</a>
                 <ul>
                    <li><a href="/admin/feedback/">Обратная связь</a></li>
                    <li><a href="#">Новостная рассылка</a>
                         <ul>
                            <li><a href="/admin/rassilkaSend/">Рассылка</a></li>
                            <li><a href="/admin/newsSendUser/">Подписчики</a></li>
                        </ul>
                     </li>
                     <li><a href="/admin/rss/">RSS</a></li>
                     <li><a href="/admin/showArhivs/">Резервирование данных</a></li>
                </ul>
            </li>   

         ';
        
        $security='
            <li>
                <a href="#" class="sf-with-ul">Безопасность</a>
                <ul>
                  <li><a href="/admin/showUser/">Управление пользователями</a></li>
                  <li><a href="/admin/showRoles/">Управления ролями</a></li>
                </ul>
            </li>
            ';
        
        $systemSetting='
            <li>
                <a href="#" class="sf-with-ul">Система</a>
                <ul>
                  <li><a href="/admin/configSystem/">Настройки системы</a></li>
                  <li><a href="/admin/configServer/">Настройки сервера</a></li>
                  <li><a href="/admin/redactSlovar/">Управление словарями</a></li>
                  <li><a href="/admin/systemGurnal/">Журнал системы управления</a></li>
                  <!--<li><a href="/admin/about/">О системе</a></li>-->
                </ul>
            </li>
        ';
      }
      //--------------- Admin --------------------------
      
        $menu='
          <ul id="navigation">
            <li>
               <a href="#" class="sf-with-ul">'.$this->lang['main_menu_resurs'].'</a>
                 <ul>
          
                   <li>
                      <a href="#">'.$this->lang['main_menu_sp'].'</a>
                      <ul>'.$spList.'</ul>    
                  </li>
                  '.$menuInRubric.'
                  <li>
                      <a href="tables.php">Библиотека</a>
                      <ul>
                          <li><a href="/admin/showLib/book/">Книги</a></li>
                          <li><a href="/admin/showLib/article/">Статьи</a></li>
                          <li><a href="/admin/showLib/filmstrip/">Диафильмы</a></li>
                          <li><a href="/admin/showLib/thesis/">Диссертационные материалы</a></li>
                       </ul>
                    </li>
                                                       
                    <li><a href="/admin/showNews/">'.$this->lang['newsname'].'</a></li>
                    <li><a href="/admin/showPerson/">'.$this->lang['personname'].'</a></li>
                    <li><a href="/admin/showStaticPage/">Статические разделы</a></li>
                    <li><a href="/admin/showEvents/1/">Календарь</a></li> 
            
                 </ul>
             </li>     
              '.$service.'
             <li>
                <a href="#" class="sf-with-ul">Сайт</a>
                <ul>
                    <li><a href="'.SITE_URL.'" target="_blank">Перейти на сайт</a></li>
                    '.$blockSite.'
                </ul>
            </li>
            '.$security.'  
            <li>
                <a href="#" class="sf-with-ul">Импорт ресурсов</a>
                <ul>
                    <li><a href="/admin/showFormImport/">Импорт материалов</a></li>
                </ul>
            </li>
            '.$systemSetting.'
          </ul>';
          
      return  $menu;  
         
     }
     
     //-------------------------------------------------------------------------
     public function fastAccess(){
         
         
       $dashboard='
         <div id="dashboard-buttons">
					<ul>';
                                        
                                         
        if ($this->auth->isLogged){
                                        
                if ( in_array("PRINTNEWS", $_SESSION['role']) || in_array("PRINTRESURS", $_SESSION['role']) || in_array("ADMIN", $_SESSION['role'])){   
                      $dashboard.='<li>
                                      <a href="/admin/showPredPrint/" class="prprint tooltip" title="'.$this->lang['predprint'].'">
                                     '.$this->lang['predprint'].' ('.Common::CountPredPrint($_SESSION['user']['user_id']).')
                                      </a>
                                    </li>';
                 }    
         }                                         
         
         $dashboard.='<li>
			<a href="/admin/showLib/book/" class="books-icon tooltip" title="'.$this->lang['lib_book'].'">
                            '.$this->lang['lib_book'].' 
			</a>
	            </li>
                    <li>
			<a href="/admin/showLib/article/" class="article tooltip" title="'.$this->lang['lib_article'].'">
			'.$this->lang['lib_article'].' 
			</a>
		    </li>
                    
                    <li>
			<a href="/admin/showLib/thesis/" class="dis-mat tooltip" title="'.$this->lang['lib_thesis'].'">
			'.$this->lang['lib_thesis_short'].' 
			</a>
	            </li>
                     <li>
			<a href="/admin/showLib/filmstrip/" class="diaphilms-icon tooltip" title="'.$this->lang['lib_filmstrip'].'">
			 '.$this->lang['lib_filmstrip'].'
			</a>
		    </li>';
		
          
                     if ($this->auth->isAdmin) { 
                         $dashboard.='
                                      <li>
                                        <a href="/admin/showMenu/" class="menu-icon tooltip" title="'.$this->lang['menu'].'">
                                         '.$this->lang['menu'].' 
                                        </a>
                                      </li>  
                                      <li>
                                        <a href="/admin/showMenu/?rubric=ok" class="rubric tooltip" title="'.$this->lang['rubrics'].'">
					 '.$this->lang['rubrics'].' 
				       </a>
                                       </li>';
                         
                     }    
			
                     $dashboard.='
				    <li>
					<a href="/admin/showNews/" class="news-icon tooltip" title="'.$this->lang['newsname'].'">
					 '.$this->lang['newsname'].' 
					</a>
				    </li>';
                    
                    if ($this->auth->isAdmin) { 
                        
                        $dashboard.= ' <li>
					<a href="/admin/showNewsRss/" class="news-rss tooltip" title="'.$this->lang['newsrss'].'">
					'.$this->lang['newsrss'].' 
					</a>
                                        </li>     
                                         <li>
					     <a href="/admin/showNewsSend/" class="news-send tooltip" title="'.$this->lang['newssend'].'">
					     '.$this->lang['newssend'].'
					     </a>
					</li>
					
					<li>
                                            <a href="/admin/showEvents/1/" class="calendar-icon tooltip" title="'.$this->lang['lib_calendarj'].'">
                                            '.$this->lang['lib_calendarj_short'].' 
					   </a>
                                        </li>';
                    }   
                     
                    $dashboard.= '   <li>
					<a href="/admin/showPerson/" class="person-icon tooltip" title="'.$this->lang['personname'].'">
					'.$this->lang['personname'].' 
					</a>
				     </li>
                                                
                                      <li>
					<a href="/admin/showSp/organasation/" class="org-icon tooltip" title="'.$this->lang['sp_org'].'">
					'.$this->lang['sp_org'].' 
					</a>
				      </li>
                                                
                                      <li>
                                            <a href="/admin/showStaticPage/" class="static tooltip" title="'.$this->lang['lib_static_page'].'">
					    '.$this->lang['lib_static_page_short'].'
					    </a>
                                      </li>';
                    
                    if ($this->auth->isAdmin) { 
                            
                            $dashboard.='<li>
					 <a href="/admin/feedback/" class="feedback tooltip" title="'.$this->lang['lib_feedback'].'">
					 '.$this->lang['lib_feedback'].' ('.Common::CountNewFeedBackMessage().') 
					 </a>
					  </li>';
                    }        
						
						
		$dashboard.='</ul>
				<div class="clear"></div>
				</div>';
        
       
       return $dashboard;
       
     }
     
     #--------------------------------------------------------------------------
     public function configControl(){
           
          $fileConf1=DROOT."/config.php";
          $fileConf2=DROOT."/classes/sql.php";
          $showError=false;
          if (is_writable($fileConf1)) {
              $errorbody='<p>'.$this->lang['dashboard_is_open'].'</p>';
              $showError=true;
              
          }
          
          if (is_writable($fileConf2)) {
              $errorbody.='<p>'.$this->lang['dashboard_is_open_db'].'</p>';
              $showError=true;
          }
          
          if ($showError) $errorbody.='<p><b>'.$this->lang['dashboard_is_open_recomend'].'</b></p>';
          
          if ($showError){
              $errorTitle='<b>'.$this->lang['dashboard_is_error'].'</b>:';
          }else{
              $errorTitle='<b>'.$this->lang['dashboard_is_no_error'].'</b>.';
          }
          
          if (Common::isBlock()) {
               $blockinfo=$this->lang['dashboard_is_block_ok'];
          }else{    
               $blockinfo=$this->lang['dashboard_is_block_no'];
          }
          
          $date_from=date("Y-m-d H:i:s");
          $day=new DateTime($date_from);
          $day->modify('-7 days'); 
          $date_to=$day->format('Y-m-d H:i:s');
          
          $dayD=new DateTime($date_from);
          $dayD->modify('-1 day'); 
          $date_toD=$dayD->format('Y-m-d H:i:s');
          
          $dayM=new DateTime($date_from);
          $dayM->modify('-1 month'); 
          $date_toM=$dayM->format('Y-m-d H:i:s');
          
          //books
          $book=new libEdit("book");
          $dayBookDay= $book->getCountDataFromTo($date_toD, $date_from);
          $weekBookDay=$book->getCountDataFromTo($date_to, $date_from);
          $monthBookDay=$book->getCountDataFromTo($date_toM, $date_from);
           
          //article
          $art=new libEdit("article");
          $dayArtDay= $art->getCountDataFromTo($date_toD, $date_from);
          $weekArtDay=$art->getCountDataFromTo($date_to, $date_from);
          $monthArtDay=$art->getCountDataFromTo($date_toM, $date_from);
                   
          //filmstrip
          $film=new libEdit("filmstrip");
          $dayFilmDay= $film->getCountDataFromTo($date_toD, $date_from);
          $weekFilmDay=$film->getCountDataFromTo($date_to, $date_from);
          $monthFilmDay=$film->getCountDataFromTo($date_toM, $date_from);
          
           //thesis
          $thes=new libEdit("thesis");
          $dayThDay= $thes->getCountDataFromTo($date_toD, $date_from);
          $weekThDay=$thes->getCountDataFromTo($date_to, $date_from);
          $monthThDay=$thes->getCountDataFromTo($date_toM, $date_from);
          
           //news
          $news=new newsEdit();
          $dayNewDay= $news->getCountNewsFromTo($date_toD, $date_from);
          $weekNewDay=$news->getCountNewsFromTo($date_to, $date_from);
          $monthNewDay=$news->getCountNewsFromTo($date_toM, $date_from);
          
          $table='<table id="table1" class="infoStatistic">
						<thead>
							<tr>
								<td></td>
								<th>День</th>
                                                                <th>Неделя</th>
                                                                <th>Месяц</th>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<th class="l">Книги</th>
								<td>'.$dayBookDay.'</td>
                                                                <td>'.$weekBookDay.'</td>
                                                                <td>'.$monthBookDay.'</td>    
							</tr>
							<tr>
								<th class="l">Статьи</th>
								<td>'.$dayArtDay.'</td>
                                                                <td>'.$weekArtDay.'</td>
                                                                <td>'.$monthArtDay.'</td>    
			
							</tr>
                                                                <th class="l">Диафильмы</th>
								<td>'.$dayFilmDay.'</td>
                                                                <td>'.$weekFilmDay.'</td>
                                                                <td>'.$monthFilmDay.'</td>    
			
							</tr>
                                                        </tr>
                                                                <th class="l">Дис материал</th>
								<td>'.$dayThDay.'</td>
                                                                <td>'.$weekThDay.'</td>
                                                                <td>'.$monthThDay.'</td>    
			
							</tr>
                                                        </tr>
                                                                <th class="l">Новости</th>
								<td>'.$dayNewDay.'</td>
                                                                <td>'.$weekNewDay.'</td>
                                                                <td>'.$monthNewDay.'</td>     
			
							</tr>
							
						</tbody>
					</table>
               ';
          
             
           
           
           return '
                  <div class="content-box">
					<div class="two-column">
						
                                                    <div class="column">
							<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div class="portlet-header ui-widget-header">'.$this->lang['dashboard_in_correction'].'<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
								<div class="portlet-content">
									<p>
										<p>'.$errorTitle.'</p>
										'.$errorbody.'
                                                                                <p><b>'.$blockinfo.'</b></p>    
									</p>
								</div>
							</div>
						</div>
                                        </div>
                                      
                                       <div class="column column-right" style="width:49%;">
							<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div class="portlet-header ui-widget-header">'.$this->lang['dashboard_in_download_statistic_view'].'<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
                                                                <div class="portlet-content">
                                                                             '.$table.'
                                                                									
                                                                </div>
                                                      </div>
						
                                        </div>
                                      
				      <div class="clear"></div>
		</div>';
           
       }
     
    
}

?>