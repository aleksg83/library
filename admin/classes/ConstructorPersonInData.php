<?php
class ConstructorPersonInData extends ConstructorElements {
    private $person;
    private $lang;
    private $id;
    
    public function __construct(PersonEdit $person, $id, $lang) {
        $this->person = $person;
        $this->lang = $lang;
        $this->id = $id;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = $this->lang['person_in_data'];        
        $path =  '<a href="/admin/showPerson/" title="'.$this->lang['person'].'">'.$this->lang['persons'].'</a>';   
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' >'.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        
        $columns = Array();          
        for($i=0; $i<=2; $i++){
            //$setting.=$i.":{ sorter: false }, ";
        }
        $columns['setting'] = $setting;
              
        $th="<th>".$this->lang['person_data']."</th>";
        $th.="<th>".$this->lang['person_data_type']."</th>";
        $th.="<th>".$this->lang['person_data_annotacia']."</th>";
        $th.="<th>".$this->lang['person_data_type_person']."</th>";
        //$th.="<th>".$this->lang['connection']."</th>";
      
        $columns['th'] = $th;   
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();        
        $sth = $this->db->prepare("SELECT d.name, d.short_author, d.short_name, d.year, d.annotaciya, tp.name as typename, td.name as dataname FROM ".PREFIX."_data as d 
                                    inner join ".PREFIX."_sp_type_person as tp 
                                    inner join ".PREFIX."_sp_type_data  as td 
                                    inner join ".PREFIX."_rel_data_person as rdp      
                                where d.id_data = rdp.id_data and d.id_sp_type_data=td.id_sp_type_data and rdp.id_sp_type_person = tp.id_sp_type_person and rdp.id_person=? ");
        
        $sth->bindParam(1, $this->id, PDO::PARAM_INT);
        $sth->execute();
        $collection=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        $tr="";
        for ($j=0; $j<count($collection); $j++){
            $collection[$j]['name']=$collection[$j]['short_author']." \"".$collection[$j]['short_name']."\" &mdash; ".$collection[$j]['year']; 
            $tr.='<tr>';
            $tr.="<td>".$collection[$j]['name']."</td>";
            $tr.="<td class='cnt'>".$collection[$j]['dataname']."</td>";
            $tr.="<td>".$collection[$j]['annotaciya']."</td>";
            $tr.="<td class='cnt'>".$collection[$j]['typename']."</td>";
            $tr.='</tr>';
        }  
        
        $tbody['tr']=$tr;
        return $tbody;    
    }    
}
?>