<?php
class ConstructorRSS extends ConstructorElements {
    
    private $rss;
    private $lang;
        
    public function __construct( rssEdit $rss, $lang) {
        $this->rss = $rss;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                           // zagolovok: "required",
                           // annotaciya: "required",
                            //text: "required",
                           // date: {
                           //          formRuDate : true
                           // },
                           
                        },
                        messages: {
                           // zagolovok: "'.$this->lang['requiredField'].'",
                           // annotaciya: "'.$this->lang['requiredField'].'",
                           // text: "'.$this->lang['requiredField'].'", 
                            //date: "'.$this->lang['requiredField'].'",
                              
                        }
                      });';
        
       
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['rss_name'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['rss_name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->rss->column = Array(
            "id" => "",
            "date" => $this->lang['form_thead_date_add'],
            "zagolovok" => $this->lang['form_thead_zagolovok'], 
            "annotaciya" => $this->lang['form_thead_annotacia'], 
            "url" => $this->lang['form_thead_url']
        );
        
        $columns = Array();                
      
        $th="";
        $i=0;
        
        foreach($this->rss->column as $key => $val) { 
            
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            
            if ($i==4) {$th.="<th$class>$val</th><th>".$this->lang['news_redactor']."</th>";}
            else if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        
        
        $columns['hidden_check']=true;
        $columns['hidden_action']=true;
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arrayNews = $this->rss->getNewsRSS();
        
        $updateActSet='/admin/updateSettingRSS/';
        $updateAct='/admin/updateRSS/';
        $formShow='/admin/formShowRSS/';
        
        /***********************************/
        $setting = $this->rss->getRSSSetting();
        $time=  Common::getTimeConversion($setting['lastBuildDate'], $this->lang);
        /***********************************/
        
        $panel = new servicePanel();
        $tbody['updateRSSSettting'] = $panel->getUpdateRSSSetting($updateActSet, $this->lang['updateRSSSet'], $urlPage, $formShow);
        $tbody['updateRSS'] = $panel->getUpdateRSS($updateAct, $this->lang['updateRSS'], $this->lang, $time);
        
        $tbody['fheight']= "400";
        $tbody['fwidth']= "880";
        
        
        $tbody['date_area']="1"; //применяем datapicker
               
        $tr="";
        for ($j=0; $j<count($arrayNews); $j++){
        
        //проверка на отсутствие в предпринте    
        //если предпринт отключен, не проверяем новости на наличие в предпринте
        $include=false;    
        if ( !Common::is_predprint("news")) $include=true;    
        else if ( Common::is_predprint("news") && !Common::is_predprintResurs($arrayNews[$j]['id_news'], "news") ){
             $include=true;
        }
            
        if ($include){
          $arrayNews[$j] = Common::removeStipsSlashes($arrayNews[$j]);    
          $id_author=$arrayNews[$j]['id_users'];
        
          unset($arrayNews[$j]['text'], $arrayNews[$j]['is_main'], $arrayNews[$j]['comment'], 
              $arrayNews[$j]['details'], $arrayNews[$j]['id_users'], $arrayNews[$j]['title'], 
              $arrayNews[$j]['keywords'], $arrayNews[$j]['description'], $arrayNews[$j]['id_news'],
              $arrayNews[$j]['is_rss'], $arrayNews[$j]['is_visible'], $arrayNews[$j]['main_theme']);
        //print_r($arrayNews[$j]);
        
         $user = new userEdit();
         $authorInfo = $user->getUser($id_author);
         $rol = $user->getRoles($id_author);
         $author=$authorInfo['login']; //." (".$authorInfo['name']." ".$authorInfo['otch'].")";
       
            
            $i=0; 
            $tr.='<tr>';
                foreach($arrayNews[$j] as $key => $val ){
                   if (strrpos($key, "is_") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }                     
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="news" nameidrec="id_news"/></td>';                      
                  
                   }else if ($key=="date"){
                       $tr.="<td>".Common::getTimeConversion($arrayNews[$j]['date'], $this->lang)."</td>"; 
                   }else{                    
                        $tr.="<td>$val</td>";                                          
                    }                                                    
                    $i++; 
                }            
            $tr.='
             <td class="cnt">'.$author.'</td>                
        </tr>';
       }   
     }//for 
        
         $tbody['tr']=$tr;
       
    return $tbody;
    }   
}

?>