<?php
class importData {
    private $db, $fileName, $error, $objPHPExcel, $idBook;
    
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->objPHPExcel = new PHPExcel();
    }
    
    public function importBook($file, $isSingle=false){
        if(preg_match('/.\.zip/',$file['name'])){            
            $zip = new ZipArchive;
            if ($zip->open($file['tmp_name']) === TRUE) {
                $zip->extractTo(TMP);
                $zip->close();                
                $this->parseDir();
                return (!in_array(0, $this->error))?1:$this->error;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    
    private function parseDir(){
        foreach (new DirectoryIterator(TMP) as $fileInfo){
            if($fileInfo->isDot()) continue;
            $this->fileName = $fileInfo->getFilename();
            #echo BOOK.$this->fileName.'<br>'; exit;
            if(!is_dir(BOOK.$this->fileName)) mkdir(BOOK.$this->fileName, 0777);
            $this->copyJPG();
            $this->copyMainFile();            
            if(!in_array(0, $this->error)){                
                $this->parseBookData();
            }else{
                return false;
            }
            $this->removeDir(TMP.$this->fileName);
            #echo "<pre>"; print_r($this->error); echo "</pre>";
        }
    }
    
    private function copyJPG(){
        if(!is_dir(BOOK.$this->fileName.'/jpg')) mkdir(BOOK.$this->fileName.'/jpg', 0777);
        foreach (new DirectoryIterator(TMP.$this->fileName.'/jpg') as $fileInfo){
            if($fileInfo->isDot()) continue;
            $this->error[] = (int)copy(TMP.$this->fileName.'/jpg/'.$fileInfo->getFilename(), BOOK.$this->fileName.'/jpg/'.$fileInfo->getFilename());
        }
        $this->imageresize(BOOK.$this->fileName.'/preview_0001.jpg',BOOK.$this->fileName.'/jpg/0001.jpg',30,75);
    }
    
    private function copyMainFile(){
        foreach (new DirectoryIterator(TMP.$this->fileName) as $fileInfo){
            if($fileInfo->isDot()) continue;
            if(preg_match("#\.[pdf|djvu|xls]+$#", $fileInfo->getFilename())){
                $this->error[] = (int)copy(TMP.$this->fileName.'/'.$fileInfo->getFilename(), BOOK.$this->fileName.'/'.$fileInfo->getFilename());
            }
        }
    }
    
    private function insertBook($data){   
        
        $title=$data['5']['E']." : ".$data['6']['E'].", ".$data['11']['E'];
        
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_data (`short_author`,`name`,`short_name`,`is_dot`,`annotaciya`,`bo`,`year`,`id_sp_polnota`,`is_view_in_rubrik`,`is_view_author`,`is_view_zaglavie`,`is_search`,`is_add_year`,`id_users`,`id_sp_type_data`,`is_in_stock`,`url`,`file_name`,`start_page`,`vid_dis`,`is_periodical`,`is_recomended`,`article_source`,`article_source_text`,`place_of_work`, `title`,`materials`,`id_next`,`id_prev`, `folder_name`) VALUES 
                                                               (:short_author, :name, :short_name, :is_dot, :annotaciya, :bo, :year, :id_sp_polnota, :is_view_in_rubrik, :is_view_author, :is_view_zaglavie, :is_search, :is_add_year, :id_users, :id_sp_type_data, :is_in_stock, :url, :file_name, :start_page, :vid_dis, :is_periodical, :is_recomended, :article_source, :article_source_text, :place_of_work, :title, :materials, :id_next, :id_prev, :folder_name)");        
        #echo "INSERT INTO ".PREFIX."_data (`short_author`     ,`name`              ,`short_name`        ,`is_dot`            ,`annotaciya`       ,`bo`                ,`year`              ,`id_sp_polnota`     ,`is_view_in_rubrik` ,`is_view_author`    ,`is_view_zaglavie`  ,`is_search`         ,`is_add_year`       ,`id_users`                      ,`id_sp_type_data`   ,`is_in_stock`        ,`url`                                   ,`file_name`         ,`start_page`        ,`vid_dis`           ,`is_periodical`     ,`is_recomended`     ,`article_source`    ,`article_source_text` ,`place_of_work`     ,`materials`         ,`id_next`           ,`id_prev`) VALUES ('{$data['5']['E']}', '{$data['6']['E']}', '{$data['7']['E']}', '{$data['8']['E']}','{$data['9']['E']}','{$data['10']['E']}','{$data['11']['E']}','{$data['12']['E']}','{$data['13']['E']}','{$data['14']['E']}','{$data['15']['E']}','{$data['17']['E']}','{$data['18']['E']}','{$_SESSION['user']['user_id']}','{$data['19']['E']}','{$data['20']['E']}','".Common::createURL($data['6']['E'])."','{$data['21']['E']}','{$data['22']['E']}','{$data['23']['E']}','{$data['26']['E']}','{$data['27']['E']}','{$data['24']['E']}','{$data['25']['E']}'  ,'{$data['30']['E']}','{$data['31']['E']}','{$data['32']['E']}','{$data['33']['E']}')<br>";  
        $sth->bindParam(":short_author",        $data['5']['E'], PDO::PARAM_STR);
        $sth->bindParam(":name",                $data['6']['E'], PDO::PARAM_STR);
        $sth->bindParam(":short_name",          $data['7']['E'], PDO::PARAM_STR);
        $sth->bindParam(":is_dot",              $data['8']['E'], PDO::PARAM_INT);
        $sth->bindParam(":annotaciya",          $data['9']['E'], PDO::PARAM_STR);
        $sth->bindParam(":bo",                  $data['10']['E'], PDO::PARAM_STR);
        $sth->bindParam(":year",                $data['11']['E'], PDO::PARAM_INT);
        $sth->bindParam(":id_sp_polnota",       $data['12']['E'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_in_rubrik",   $data['13']['E'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_author",      $data['14']['E'], PDO::PARAM_INT);
        $sth->bindParam(":is_view_zaglavie",    $data['15']['E'], PDO::PARAM_INT);
        $sth->bindParam(":is_search",           $data['17']['E'], PDO::PARAM_INT);
        $sth->bindParam(":is_add_year",         $data['18']['E'], PDO::PARAM_INT);
        $sth->bindParam(":id_sp_type_data",     $data['19']['E'], PDO::PARAM_INT);
        $sth->bindParam(":is_in_stock",         $data['20']['E'], PDO::PARAM_INT);
        $sth->bindParam(":file_name",           $data['21']['E'], PDO::PARAM_STR);
        $sth->bindParam(":start_page",          $data['22']['E'], PDO::PARAM_STR);
        $sth->bindParam(":vid_dis",             $data['23']['E'], PDO::PARAM_STR);
        $sth->bindParam(":article_source",      $data['24']['E'], PDO::PARAM_STR);
        $sth->bindParam(":article_source_text", $data['25']['E'], PDO::PARAM_STR);
        $sth->bindParam(":is_periodical",       $data['26']['E'], PDO::PARAM_INT);
        $sth->bindParam(":is_recomended",       $data['27']['E'], PDO::PARAM_INT);
        $sth->bindParam(":place_of_work",       $data['30']['E'], PDO::PARAM_STR);
        $sth->bindParam(":title",               $title, PDO::PARAM_STR);
        $sth->bindParam(":materials",           $data['31']['E'], PDO::PARAM_STR);
        $sth->bindParam(":id_next",             $data['32']['E'], PDO::PARAM_INT);
        $sth->bindParam(":id_prev",             $data['33']['E'], PDO::PARAM_INT);
        $sth->bindParam(":folder_name",         $this->fileName, PDO::PARAM_STR);
        $sth->bindParam(":url",                 Common::createURL($data['6']['E']), PDO::PARAM_STR);       
        $sth->bindParam(":id_users",            $_SESSION['user']['user_id'], PDO::PARAM_INT);
        
        $sth->execute();
        $this->idBook = $this->db->lastInsertId(); 
                
        if($data['16']['E']) $this->bindRubrik($data['16']['E']); /*Добавить рубрики*/
        if($data['4']['E'])  $this->addRelPerson($data['4']['E'], 3); /*Добавить юбиляров*/        
        if($data['28']['E']) $this->addRelPerson($data['28']['E'], 4); /*Добавить научных руководитклей*/
        if($data['29']['E']) $this->addRelPerson($data['29']['E'], 5); /*Добавить оппонентов*/
        if($data['2']['E'])  $this->addRelPerson($data['2']['E'], 1); /*Добавить авторов*/
        if($data['3']['E'])  $this->addRelPerson($data['3']['E'], 2); /*Добавить редакторов*/
        $err = $sth->errorInfo();
        $this->error[] = ($err[0] != '00000')?0:1;        
    }
    
    private function insertContent($data){
        $this->db->beginTransaction();
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_mapping (id_data, num_page, real_page, name, level, type, file) VALUES (?, ?, ?, ?, ?, 1, ?)");
        array_shift($data);
        foreach($data as $value){
            $sth->bindParam(1, $this->idBook, PDO::PARAM_INT);
            $sth->bindParam(2, $value['C'], PDO::PARAM_INT);
            $sth->bindParam(3, $value['D'], PDO::PARAM_INT);
            $sth->bindParam(4, $value['B'], PDO::PARAM_INT);
            $sth->bindParam(5, $value['A'], PDO::PARAM_INT);
            $sth->bindParam(6, $value['E'], PDO::PARAM_INT);
            $sth->execute();
            $err = $sth->errorInfo();
            $result[] = ($err[0] != '00000')?0:1;
        }
        if(in_array(0, $result)){
            $this->db->rollBack();
            $this->error[] = 0;
        }else{
            $this->db->commit();
            $this->error[] = 1;
        }
    }
    
    private function parseBookData(){
        $objPHPExcel = PHPExcel_IOFactory::load(BOOK.$this->fileName.'/'.$this->fileName.'.xls');
        $loadedSheetNames = $objPHPExcel->getSheetNames();        
        foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
            $objPHPExcel->setActiveSheetIndexByName($loadedSheetName);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            if($sheetIndex == 0){
                $this->insertBook($sheetData);
            }elseif($sheetIndex == 1 && $this->idBook){
                $this->insertContent($sheetData);
            }
        }
        
    }
    
    private function removeDir($directory) {
        $dir = opendir($directory);
        while(($file = readdir($dir))) {
            if(is_file($directory."/".$file)) {
              unlink ($directory."/".$file);
            }
            elseif(is_dir($directory."/".$file) && ($file != ".") && ($file != "..")) {
              $this->removeDir($directory."/".$file);  
            }
        }
        closedir($dir);
        rmdir($directory);
    } 
    
    private function bindRubrik($ids){
        $ids = explode(",", $ids);
        $sth = $this->db->prepare("INSERT IGNORE INTO ".PREFIX."_rel_data_menu (id_data, id_menu) VALUES (?, ?)");        
        $sth->bindParam(1, $this->idBook, PDO::PARAM_INT);
        foreach($ids as $id){ 
            $id = trim($id);
            #echo "INSERT IGNORE INTO ".PREFIX."_rel_data_menu (id_data, id_menu) VALUES ({$this->idBook}, {$id})<br>";
            $sth->bindParam(2, $id, PDO::PARAM_INT);
            $sth->execute();
        }
    }
    
    private function addRelPerson($cellValue, $type){
        $ids = explode(",", $cellValue);
        $sth = $this->db->prepare("insert into ".PREFIX."_rel_data_person (id_data, id_person, id_sp_type_person) values (?, ?, ?)");        
        $sth->bindParam(1, $this->idBook, PDO::PARAM_INT);
        $sth->bindParam(3, $type, PDO::PARAM_INT);
        foreach($ids as $id){                             
                $id = trim($id);
                #echo "insert into ".PREFIX."_rel_data_person (id_data, id_person, id_sp_type_person) values ({$this->idBook}, {$id}, {$type})<br>";
                $sth->bindParam(2, $id, PDO::PARAM_INT);               
                $sth->execute();
        }                
    }
    
    private function imageresize($outfile,$infile,$percents,$quality) {
        $im=imagecreatefromjpeg($infile);
        $w=imagesx($im)*$percents/100;
        $h=imagesy($im)*$percents/100;
        $im1=imagecreatetruecolor($w,$h);
        imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));

        imagejpeg($im1,$outfile,$quality);
        imagedestroy($im);
        imagedestroy($im1);
    }
    
}
?>