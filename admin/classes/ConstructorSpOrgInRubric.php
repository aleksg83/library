<?php
class ConstructorSpOrgInRubric extends ConstructorElements {
    private $sp;
    private $lang;
    private $id;
    
    public function __construct(spEdit $sp, $id, $lang) {
        $this->sp = $sp;
        $this->lang = $lang;
        $this->id = $id;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = $this->lang['sp_org_in_rubric'];        
        $path =  '<a href="/admin/showSp/'.$this->sp->name.'/" title="'.$this->lang['sp_org'].'">'.$this->lang['sp_org'].'</a>';   
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' >'.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        
        $columns = Array();          
        for($i=0; $i<=2; $i++){
            $setting.=$i.":{ sorter: false }, ";
        }
        $columns['setting'] = $setting;
              
        $th="<th>".$this->lang['rubric']."</th>";
        $th.="<th>".$this->lang['form_thead_url']."</th>";
        $th.="<th>".$this->lang['connection']."</th>";

        $columns['th'] = $th;   
        
       //$columns['style']='style="width:600px;"';
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();        
        
        $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_rel_sp_organasation_menu WHERE id_sp_organasation=?");
        $sth->bindParam(1, $this->id, PDO::PARAM_INT);
        $sth->execute();
        $collection=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        $collectionIdRub=array();
        
        foreach($collection as $key => $val){
               array_push($collectionIdRub, $val['id_menu']);
        }
        
        $menu = new menuEdit(1);
        $arraySp = $menu->showMenu(); 
        
        $tr="";
        for ($j=0; $j<count($arraySp); $j++){
            $level= $arraySp[$j]['level'];
            $id_record=$arraySp[$j]['id_menu'];
                   
            
             $left=( ($level-1)*20)."px";
             if ($level>1) {
                 $class='class="tree"';
                 $classTr=' class="close"';
             } else {
                 $class="";
                 $classTr='';
             }  
            
             //проверка вложенности
             #---------------------------
              $sth1=$this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=?");
              $sth1->bindParam(1, $id_record, PDO::PARAM_INT);
              $sth1->execute();
              //$collection=$sth->fetchAll(PDO::FETCH_ASSOC);
              $nChild=$sth1->rowCount();
              $plus=($nChild>0) ? '<span class="ui-icon treerubric ui-icon-plusthick"></span>' : '' ;
             #--------------------------
             
              $tr.='<tr'.$classTr.'>'; 
              
             $tr.="<td class=\"rubrics\"><p $class style=\"margin-left:$left;\" idrec=\"".$id_record."\" idtop=\"".$arraySp[$j]['id_menu_top']."\" >".$plus." ".$arraySp[$j]['name']."</p></td>";
             
             $tr.="<td>".$arraySp[$j]['url']."</td>";
             
             if ( in_array($id_record, $collectionIdRub) ){
                  $val=1;
             } else {
                  $val=0;
             }
          
            if ($val == 1) {
              $checked = 'checked="cheked"';
             } else {
              $checked="";
             }     
             
            $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="is_rubric_sp_org_'.$j.'" class="checkInRubricSpOrg" '.$checked.' id_record="'.$id_record.'" id_sp_org="'.$this->id.'" action="/adminajax/setIsRubricInSpOrg/" /></td>                      
            </tr>';
        }  
       
        $tbody['tr']=$tr;
        return $tbody;    
    }    
}

?>