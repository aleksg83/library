<?php
class ConstructorLib extends ConstructorElements {
    private $lib;
    private $lang;
        
    public function __construct( libEdit $lib, $lang) {
        $this->lib = $lib;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        if ($this->lib->type=='metabook'){
            return  '$("#validateForm").validate({
                        rules: {
                            name: "required",
                        },
                        messages: {
                            name: "'.$this->lang['requiredField'].'",
                        }
                      });';
            
        } else {
            return  '$("#validateForm").validate({
                        rules: {
                            short_author: "required",
                            name: "required",
                        },
                        messages: {
                            short_author: "'.$this->lang['requiredField'].'",
                            name: "'.$this->lang['requiredField'].'",
                        }
                      });';
        } 
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['lib_'.$this->lib->type];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
     
     $this->lib->column=Array ( 
                            'id_data' =>"",
                            'id_name' =>"ID",
                            'name'=> $this->lang['form_thead_lib_book_name'],
                            'id_sp_polnota' => $this->lang['form_thead_sp_polnota'], 
                            'is_view_in_rubrik' => $this->lang['form_thead_view_in_rubrik'],
                            //'is_view_author' => $this->lang['form_thead_view_author'],
                            'vid_dis' => $this->lang['form_thead_vid_dis'],
                            //'is_periodical' => $this->lang['form_thead_is_periodical'],
                            'is_recomended' => $this->lang['form_thead_is_recomended'],
                            'is_in_stock' => $this->lang['form_thead_is_in_stock'],
            
                          );
     
        if ($this->lib->type != 'thesis') {
           unset($this->lib->column['vid_dis']);
        }
       
        if ($this->lib->type != 'book') {
            unset($this->lib->column['is_periodical'], $this->lib->column['is_recomended'], $this->lib->column['is_in_stock']);
        } 
        
        if ($this->lib->type == 'metabook') {
            unset($this->lib->column['id_name'], $this->lib->column['id_sp_polnota'], $this->lib->column['is_view_author'], $this->lib->column['is_in_stock'], $this->lib->column['is_view_in_rubrik']);
        } 
        
        if ($this->lib->type == 'article') {
            unset($this->lib->column['id_sp_polnota']);
        }        
        
        $columns = Array();  
        
        
        for($i=0; $i<=count($this->lib->column); $i++){
           
            if ($this->lib->type != 'thesis') {
                if ( ($i==0) || ($i=count($this->lib->column))) $setting.=$i.":{ sorter: false }, ";
            }else{
                if ($i==0) $setting.=$i.":{ sorter: false }, ";
            }
        }
        
         //----------------------------------------------------------------------
        
        if ($this->lib->type=='book'){
            $setting.="5:{ sorter: 'checkbox' }, 
                       6:{ sorter: 'checkbox' }, 
                       7:{ sorter: 'checkbox' }, 
                       9:{ sorter: false },";
            $columns['numb'] = 10;
        }
        //----------------------------------------------------------------------
        
        //----------------------------------------------------------------------
        if ($this->lib->type=='metabook'){
            $setting="0:{ sorter: false }, 
                      4:{ sorter: false }, ";
             
        }
        //----------------------------------------------------------------------
        if ($this->lib->type=='article'){
            $setting="0:{ sorter: false },
                      8:{ sorter: false }, ";
            
        }
        //----------------------------------------------------------------------
        if ($this->lib->type=='filmstrip'){
            $setting.="5:{ sorter: 'checkbox' },
                       7:{ sorter: false },";
        }
        //----------------------------------------------------------------------
        
        if ($this->lib->type=='thesis'){
            $setting.="5:{ sorter: 'checkbox' },
                       8:{ sorter: false },";
        }
        //----------------------------------------------------------------------
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->lib->column as $key => $val) { 
           if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
           else $class='';
            
           if ($i==3) $th.="<th$class>".$this->lang['form_thead_lib_date_add']."</th>";
         
        #-----------------------------------------------------------------------
        #book   
         if ($this->lib->type=="book"){
            if ($i>0) $th.="<th$class>$val</th>";
            if ($i==6) $th.="<th>".$this->lang['news_redactor']."</th>";
         } 
         
        #-----------------------------------------------------------------------
        #metabook   
         if ($this->lib->type=="metabook"){
            if ($i>0) $th.="<th$class>$val</th>";
            if ($i==1) {$th.="<th$class>".$this->lang['date_add']."</th>";
                        $th.="<th>".$this->lang['news_redactor']."</th>";}
         } 
         
         
         #-----------------------------------------------------------------------
         #article
         if ($this->lib->type=="article"){
            if ($i>0) $th.="<th$class>$val</th>";
            if ($i==3) {
                $th.="<th$class>".$this->lang['lib_article_istochnik']."</th>";
                $th.="<th$class>".$this->lang['lib_article_type']."</th>";
                $th.="<th>".$this->lang['news_redactor']."</th>";
                 
            }    
         } 
         
         #-----------------------------------------------------------------------
         #diafilm
         if ($this->lib->type=="filmstrip"){
            if ($i>0) $th.="<th$class>$val</th>";
            if ($i==4) $th.="<th>".$this->lang['news_redactor']."</th>";
         }
         
         #-----------------------------------------------------------------------
         #
         if ($this->lib->type=="thesis"){
            if ($i>0) $th.="<th$class>$val</th>";
            if ($i==5) $th.="<th>".$this->lang['news_redactor']."</th>";
         }
         
         
            $i++;
        }        
        $columns['th'] = $th;  
        
       // $columns['numb'] = count($this->menu->column);
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arrayLib = $this->lib->getData();
        //print_r($arrayLib);
       
        $addAct='/admin/addLib/'.$this->lib->type.'/'.$dopParam;
        $updateAct='/admin/updateLib/'.$this->lib->type.'/'.$dopParam;
        $formShow='/admin/formShowLib/'.$this->lib->type.'/'.$dopParam;
        $deleteAct='/admin/deleteLib/'.$this->lib->type.'/'.$dopParam;
        $table="data";
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        #-------------------------------------------------------------------------------------------------------
        if ($this->lib->type=='book'){
            $tbody['right_but'] = $panel->getRightButton($this->lang['lib_button_book'], '/admin/showLib/metabook/');
        }
        
        #------------------------------------------------------------------------------------------------------
        if ($this->lib->type=='metabook'){
            $tbody['right_but'] = $panel->getRightButton($this->lang['lib_button_metabook'], '/admin/showLib/book/', 'ui-icon-bookmark');
        }
        
        
        //$tbody['fheight']= "594";
        //$tbody['fwidth']= "1110";
        
        $tbody['creditor_width']= "760";
        $tbody['creditor_height']= "220";   
        $tbody['creditor_big']= "1";   
         
        $tr="";
                       
        for ($j=0; $j<count($arrayLib); $j++){
            
             $arrayLib[$j] = Common::removeStipsSlashes($arrayLib[$j]);
            
            $user = new userEdit();
            $authorInfo = $user->getUser($arrayLib[$j]['id_users']);
            $author=$authorInfo['login'];
            
            //if ($this->lib->type == 'book') {
                $arrayLib[$j]['name']=$arrayLib[$j]['short_author']." &quot;".$arrayLib[$j]['short_name']."&quot; &mdash; ".$arrayLib[$j]['year']; 
            //}
            
            
             
        unset($arrayLib[$j]['id_sp_type_data'], $arrayLib[$j]['short_author'], $arrayLib[$j]['short_name'],
              $arrayLib[$j]['is_dot'], $arrayLib[$j]['bo'], $arrayLib[$j]['article_group_bo'], $arrayLib[$j]['soderzhanie'],
              $arrayLib[$j]['soderzhanie_full'], $arrayLib[$j]['is_view_zaglavie'], $arrayLib[$j]['is_search'], 
              $arrayLib[$j]['is_add_year'], $arrayLib[$j]['file_name'], $arrayLib[$j]['folder_name'], $arrayLib[$j]['start_page'],
              $arrayLib[$j]['date_update'],$arrayLib[$j]['opponent'], $arrayLib[$j]['place_of_work'],
              $arrayLib[$j]['is_meta_book'], $arrayLib[$j]['title'], 
              $arrayLib[$j]['description'], $arrayLib[$j]['keywords'], $arrayLib[$j]['materials'],$arrayLib[$j]['annotaciya'], $arrayLib[$j]['id_users'],
              $arrayLib[$j]['year'], $arrayLib[$j]['url'], $arrayLib[$j]['id_next'], $arrayLib[$j]['id_prev'], $arrayLib[$j]['id_scientific_director'],
              $arrayLib[$j]['is_view_author'], $arrayLib[$j]['is_periodical']);
        
            
            if ($this->lib->type != 'article') {
               unset($arrayLib[$j]['article_source'], $arrayLib[$j]['article_source_text']);
            } 
                
            if ($this->lib->type != 'thesis') {
               unset($arrayLib[$j]['vid_dis']);
            }
            
            if ($this->lib->type == 'metabook') {
                unset($arrayLib[$j]['id_sp_polnota'], $arrayLib[$j]['is_view_in_rubrik'], $arrayLib[$j]['is_view_author'], $arrayLib[$j]['is_periodical'], $arrayLib[$j]['is_recomended'], $arrayLib[$j]['is_in_stock']);
                
            }
            
            if ($this->lib->type == 'article') {
                unset($arrayLib[$j]['is_periodical'], $arrayLib[$j]['is_recomended'], $arrayLib[$j]['is_in_stock'], $arrayLib[$j]['id_sp_polnota']);
            }

             if ($this->lib->type == 'filmstrip') {
                unset($arrayLib[$j]['is_periodical'], $arrayLib[$j]['is_recomended'], $arrayLib[$j]['is_in_stock']);
            }
            
             if ($this->lib->type == 'thesis') {
                unset($arrayLib[$j]['is_periodical'], $arrayLib[$j]['is_recomended'], $arrayLib[$j]['is_in_stock']);
            }
            
            $classTr = ( Common::is_predprintResurs($arrayLib[$j]['id_data']) ) ? ' class="preprint" title="'.$this->lang['predprint_resurs'].'"' : '' ;
                       
            $i=0; 
            $tr.="<tr{$classTr}>";
            
            foreach($arrayLib[$j] as $key => $val ){
                   
                  if ($i==2){
                       $tr.='<td class="cnt" style=\"min-width:150px;\">'.Common::getTimeConversion($arrayLib[$j]['date_add'], $this->lang, true).'</td>';
                  }
                 
                  if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                        if ($this->lib->type!='metabook'){
                            $tr.='<td class="center">'.$val.'</td>'; 
                        }
                        
                  }else if($key=='name'){
                       $tr.="<td style=\"min-width:220px;\">$val</td>";   
                        
                  }else if($key=='id_sp_polnota'){
                      
                      #----------------------------------------------------------  
                      if ( ($val==0) || ($val=="")) {
                           $strTooltip=$this->lang['no_polnota_materials'];
                           $tr.="<td class=\"tooltips cnt\" title=\"".$strTooltip."\" style='min-width:80px;'>".$this->lang['no_polnota_materials']."</td>";
                       }else{
                            $polnota=new spEdit('polnota');
                            $pl=$polnota->select();
                            for ($k=0;$k<count($pl); $k++){
                                if ($arrayLib[$j]['id_sp_polnota'] == $pl[$k]['id_sp_polnota']) {
                                    $tr.="<td class=\"tooltips cnt\" title=\"".$pl[$k]['name']."\">".$pl[$k]['name']."</td>";
                                }
                            }
                        }    
                       #----------------------------------------------------------  
                        
                   }elseif(strrpos($key, "is_") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }  
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="'.$table.'" nameidrec="id_data"/></td>';                      
                    
                    }else if( ($key=='article_source') || ($key=='article_source_text') ){
                        
                    }else if( ($key=='vid_dis')){
                         if ($val=="1") $tr.="<td class='cnt'>".$this->lang['lib_thesis_type1']."</td>";
                         if ($val=="2") $tr.="<td class='cnt'>".$this->lang['lib_thesis_type2']."</td>";
                         if ($val=="3") $tr.="<td class='cnt'>".$this->lang['lib_thesis_type3']."</td>";
                         
                    }else{
                        if ($key!='date_add') $tr.="<td>$val</td>";   
                         
                    } 
                    
                    if ( ($this->lib->type == "book") && ($i==6) ) $tr.="<td class='cnt'>$author</td>"; 
                    else if ( ($this->lib->type == "metabook") && ($i==2) ) $tr.="<td class='cnt'>$author</td>"; 
                    else if ( $this->lib->type == "article" ) {
                        $typeArt = ( ($arrayLib[$j]['article_source']=="") ||  $arrayLib[$j]['article_source']==0)? 0 : 1;
                        //0=самостоятельная статья
                        //1=часть книги
                       
                        $typeText= ($typeArt==0)? $this->lang['lib_article_izdannie'] : $this->lang['lib_article_chastj'];
                       
                        if ($i==4) $tr.="<td class='cnt'>".$arrayLib[$j]['article_source_text']."</td>";
                        if ($i==5) $tr.="<td class='cnt'>$typeText</td>"; 
                        if ($i==5) $tr.="<td class='cnt'>$author</td>";
                        
                    }   
                    else if ( $this->lib->type == "filmstrip" ) {
                      if ($i==4) $tr.="<td class='cnt'>$author</td>";   
                    }
                    
                    else if ( $this->lib->type == "thesis" ) {
                      if ($i==5) $tr.="<td class='cnt'>$author</td>";   
                    }
                    
                    
                    $i++; 
                }            
           
           //------------------------------------------------------
           $arrayLib[$j]['name'] = urlencode($arrayLib[$j]['name']);
           #--------------------------------------------------------------------------- 
           #book
           if ( ($this->lib->type == "book") || ($this->lib->type == "filmstrip") || ($this->lib->type == "thesis") ) {     
            $but_dop ='
              <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_in_book_rubric'].'" href="/admin/showLibInRubric/'.$this->lib->type.'/?data='.$id_record.'&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-shuffle"></span>
             </a>   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_mapping_full'].'" href="/admin/showLibMapping/'.$this->lib->type.'/?data='.$id_record.'&type=1&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-clipboard"></span>
             </a>  
              <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_mapping_small'].'" href="/admin/showLibMapping/'.$this->lib->type.'/?data='.$id_record.'&type=0&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-copy"></span>
             </a>  
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_images_preview'].'" href="/admin/showLibImages/'.$this->lib->type.'/?data='.$id_record.'&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-image"></span>
             </a> ';     
            $style = ' style="padding-left: 10px; width:220px !important;"';
           }else if ($this->lib->type == "article") {
            
                if ($typeArt==0){   
            
                  $lib_but='
                      <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_mapping_full'].'" href="/admin/showLibMapping/'.$this->lib->type.'/?data='.$id_record.'&type=1&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
                       <span class="ui-icon ui-icon-clipboard"></span>
                      </a>  
                      <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_mapping_small'].'" href="/admin/showLibMapping/'.$this->lib->type.'/?data='.$id_record.'&type=0&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
                         <span class="ui-icon ui-icon-copy"></span>
                      </a>  
                     <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_images_preview'].'" href="/admin/showLibImages/'.$this->lib->type.'/?data='.$id_record.'&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
                         <span class="ui-icon ui-icon-image"></span>
                     </a>     
                    ';   
            
                    $style = ' style="padding-left: 8px; width:200px !important;"'; 
                 } else { 
                    
                    $lib_but="";
                    $style = ' style="padding-left: 40px;"';    
                }   
            
            $but_dop ='
              <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_in_book_rubric'].'" href="/admin/showLibInRubric/book/?data='.$id_record.'&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-shuffle"></span>
             </a>
             '.$lib_but.'
             ';
               
           }else if ($this->lib->type == "metabook") {    
              $style = ' style="padding-left: 40px;"'; 
           }
           
           #---------------- link person ---------------------------------------
           $person_but='<a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['lib_in_book_person'].'" href="/admin/showLibInPerson/'.$this->lib->type.'/1/?data='.$id_record.'&name='.$arrayLib[$j]['name'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-person"></span>
             </a>';
           if ($this->lib->type == "metabook") $person_but='';
           #---------------- link person ---------------------------------------
           
             $tr.='<td'.$style.'> 
             '.$but_dop.'
             '.$person_but.' 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil"></span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        }  
        $tbody['tr']=$tr;
               
     return $tbody;
      
    }   
}

?>