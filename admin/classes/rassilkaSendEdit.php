<?php
class rassilkaSendEdit {
    private $db, $error;
    public $column;
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();  
        Common::ChecksAccessRedactor();
    }
    
    public function getRassilka($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_rassilka WHERE id_rassilka=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_rassilka ORDER BY date_add desc");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        
        return $result;
    }
    
     public function getStatusName($id){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_status WHERE id_sp_status=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }
        
        return $result['name'];
    }
    
    public function getRassulkaSetting(){
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_rassilka_setting WHERE id_rassilka_setting='1'");
        $result = $sth->fetch(PDO::FETCH_ASSOC);   
        return $result;
    }
    
    public function getStatusId($name){
        if($name){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_status WHERE name=?");
            $sth->bindParam(1, $name, PDO::PARAM_STR);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }
        
        return $result['id_sp_status'];
    }
    
     
    public function add($data){
       
        $data=$this->prepareInsertData($data);
        
       if ($data['add_file']) {
              $file =" file, ";
              $sqlfile  =" :file, ";
       }else{
               $file = "";
               $sqlfile = "";
       }
        
        
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rassilka (tema, body, date_add, {$file} id_sp_status) VALUES ( :tema, :body, :date_add, {$sqlfile} :id_sp_status)");
        $sth->bindParam(":tema", $data['tema'], PDO::PARAM_STR);
        $sth->bindParam(":body", $data['body'], PDO::PARAM_STR);
        $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        if ($data['add_file']) $sth->bindParam(":file", $data['file_name'], PDO::PARAM_STR);
        $sth->bindParam(":id_sp_status", $this->getStatusId("NEW"), PDO::PARAM_INT);
        $sth->execute();        
                
        $err = $sth->errorInfo();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();

        return ($err[0] != '00000')?false:true;
    }
    
    public function update($data){
       
        $data=$this->prepareInsertData($data, "edit"); 
         
         if ($data['add_file']) {
              $file =", file=:file ";
       }else{
              $file = "";
       }
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_rassilka SET tema=:tema, body=:body {$file}, date_add=:date_add  WHERE id_rassilka=:id_rassilka");        
        $sth->bindParam(":tema", $data['tema'], PDO::PARAM_STR);
        $sth->bindParam(":body", $data['body'], PDO::PARAM_STR);
        if ($data['add_file']) $sth->bindParam(":file", $data['file_name'], PDO::PARAM_STR);
        $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $sth->bindParam(":id_rassilka", $data['id'], PDO::PARAM_INT);
        $sth->execute();        
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
         
    }
    
    public function delete($id){
       $arrayDel = explode(",", $id);
        
        foreach($arrayDel as $key=>$val){
           
            $tempData=$this->getRassilka($val);
            
            if ( ($tempData['file']!="") && file_exists(RASSILKA_FILE.$tempData['file'])){
                unlink(RASSILKA_FILE.$tempData['file']); 
            }
            
            //-------------------------------------------------------------------
             $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_rassilka_user_email WHERE id_rassilka=?");
             $sth->bindParam(1, $val, PDO::PARAM_INT);
             $sth->execute();
                
        } 
        
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rassilka WHERE id_rassilka IN ($place_holders)");
        $sth->execute($arrayDel);
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
        
    }
    #---------------------------------------------------------------------------------------------------------------------------------------------------    
     public function prepareInsertData($data, $type="add"){
       
        $data['tema']=  Common::removingCharacter($data['tema']);
        $data['body']=  Common::removingCharacter($data['body']);
        
        
        if ( ($type=="edit") && ( ($data['delFile']=="1") || (!empty($_FILES['file']['tmp_name'])) ) ){
             
             $data_resurs=$this->getRassilka($data['id']);
             if ( ($data_resurs['file'] != "") && file_exists(RASSILKA_FILE.$data_resurs['file']) ) {
                     unlink(RASSILKA_FILE.$data_resurs['file']);
             }
             
         }
        
        
        $data['add_file']=false;
        if (!empty($_FILES['file']['tmp_name'])){
             Common::downloadFile(RASSILKA_FILE."/"); 
             $file_name=Common::encodestring($_FILES['file']['name'], true);
             
             $data['add_file']=true;
             $data['file_name']=$file_name;
         }
        
        return $data;
       
    }
    
    public function updateSetting($data){
        
        if (!isset($data['is_html'])) $data['is_html']=0;
        else $data['is_html']=1;
        
        $data['text']= Common::removingCharacter($data['text']);
        
        if ($data['type']=="0"){
            $type=1;
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_rassilka_setting SET type=:type, name_author=:name_author, adress_author=:adress_author, adress_server=:adress_server, is_HTML=:is_HTML, end_letter=:end_letter, about=:about, info_after_sender=:info_after_sender, appeal_to_men=:appeal_to_men, appeal_to_women=:appeal_to_women  WHERE id_rassilka_setting=:id_rassilka_setting");        
            $sth->bindParam(":type", $type, PDO::PARAM_INT);
            $sth->bindParam(":name_author", $data['name'], PDO::PARAM_STR);
            $sth->bindParam(":adress_author", $data['email_a'], PDO::PARAM_STR);
            $sth->bindParam(":adress_server", $data['email_s'], PDO::PARAM_STR);     
            
        }elseif ($data['type']=="1"){
            $type=2;
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_rassilka_setting SET type=:type, time_paket=:time_paket, kolvo_pisem=:kolvo_pisem, kolvo_addr=:kolvo_addr,  name_author=:name_author, adress_author=:adress_author, adress_server=:adress_server, is_HTML=:is_HTML, end_letter=:end_letter, about=:about, info_after_sender=:info_after_sender, appeal_to_men=:appeal_to_men, appeal_to_women=:appeal_to_women  WHERE id_rassilka_setting=:id_rassilka_setting");        
            $sth->bindParam(":type", $type, PDO::PARAM_INT);
            $sth->bindParam(":time_paket", $data['time'], PDO::PARAM_STR);
            $sth->bindParam(":kolvo_pisem", $data['k_p'], PDO::PARAM_STR);
            $sth->bindParam(":kolvo_addr", $data['k_a'], PDO::PARAM_STR);
            $sth->bindParam(":name_author", $data['name'], PDO::PARAM_STR);
            $sth->bindParam(":adress_author", $data['email_a'], PDO::PARAM_STR);
            $sth->bindParam(":adress_server", $data['email_s'], PDO::PARAM_STR);    
        }
        
        $sth->bindParam(":is_HTML", $data['is_html'], PDO::PARAM_INT);
        $sth->bindParam(":end_letter", $data['text'], PDO::PARAM_STR);  
        $sth->bindParam(":about", $data['about'], PDO::PARAM_STR);  
        $sth->bindParam(":info_after_sender", $data['info_after_sender'], PDO::PARAM_STR);  
        $sth->bindParam(":appeal_to_men", $data['appeal_to_men'], PDO::PARAM_STR);  
        $sth->bindParam(":appeal_to_women", $data['appeal_to_women'], PDO::PARAM_STR);  
        $sth->bindParam(":id_rassilka_setting", $data['id'], PDO::PARAM_INT);
        $sth->execute();        
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    
    }
    
    
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id, $act, $action, $url, $lang){
         if ($act=="edit")  {
             $ras = $this->getRassilka($id);
             $ras = Common::removeStipsSlashes($ras);
         }    
        
         //print_r($news[0]['details']);
        
         
         $size2=118; 
         
         $paramTextField['class']="creditor";
         $paramTextField['id']="creditor_id3";
         //$paramTextField['column']="columns-2"; //определяем поле в правую колонку формы, форма получиться двух-колоночная
         
         
         
         if ($act=="add"){
             $ras['tema']="";
             $ras['body'] = "";
             $ras['file'] = "";
         }
       
         if ( ($ras['file'] != "") && file_exists(RASSILKA_FILE."/".$ras['file']) ) {
            $paramFileFieldDefinition['file']='<a href="/'.RASSILKA_FILE."/".$ras['file'].'" target="_blank" class="tooltips" title="'.$ras['file'].'">'.$ras['file'].'</a>
                                              <span class="photoPerson fileDel">'.$lang['lib_delete_file'].' <input type="checkbox" name="delFile" value="0"/></span>';
         }else{
            $paramFileFieldDefinition['file']="";
         }
         
         $tema =         new field_text("tema", $lang['form_thead_ras_theme'], true, $ras['tema'], "", $size2);         
         $body =         new field_textarea("body", $lang['form_thead_ras_body'], false, $ras['body'], 33, 10, "", "", "", $paramTextField); 
         
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
         
         $file =         new field_file("file", $lang['lib_file'], false, "", "", $paramFileFieldDefinition);         
         
         $form = new form(array("tema" => $tema,
                                "body" => $body, 
                                "file" => $file, 
                                "id" => $id_rec,
                                "url" => $url,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
       
     }
     //form
     #---------------------------------------------------------------------------------------------
    
}

?>