<?php
class ConstructorPerson extends ConstructorElements {
    
    private $person;
    private $lang;
        
    public function __construct( personEdit $person, $lang) {
        $this->person = $person;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            other_name: "required",
                            //name: "required",
                            //surname: "required",
                            //patronymic: "required",
                            //fio: "required",
                        },
                        messages: {
                            other_name: "'.$this->lang['requiredField'].'",
                            //name: "'.$this->lang['requiredField'].'",
                            //surname: "'.$this->lang['requiredField'].'", 
                            //patronymic: "'.$this->lang['requiredField'].'",
                            //fio: "'.$this->lang['requiredField'].'",     
                        }
                      });';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['personname'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['personname'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->person->column = Array (
            "id" => "",
            "id-show" => "ID",
            //'fam_dp' => $this->lang['form_thead_fam_dp'], 
            // 'name' => $this->lang['form_thead_name'],
            'fio' => $this->lang['form_thead_fio'],
            'is_view_in_main' => $this->lang['form_thead_is_view_in_main'],
            //'url' => $this->lang['form_thead_url'],
            //'is_in_encyclopedia' => $this->lang['form_thead_is_in_encyclopedia'],
            'id_person_forwars' => $this->lang['form_thead_forwars'] );
        
        $columns = Array();                
        for($i=0; $i<=count($this->person->column); $i++){
           if ( ($i==0) || ($i=count($this->person->column))) $setting.=$i.":{ sorter: false }, ";
        }        
        
        $setting.="3:{ sorter: 'checkbox' } ";
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->person->column as $key => $val) { 
            
             if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            
            if ($i==2) $th.="<th$class>".$this->person->column['fio']."</th>";
            //else if ($i==2) $th.="<th$class>".$this->person->column['fam_dp']."</th>";
            else if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        
        return  $columns;
        
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arrayPerson = $this->person->getPersons();
        
        $addAct='/admin/addPerson/'.$dopParam;
        $updateAct='/admin/updatePerson/'.$dopParam;
        $formShow='/admin/formShowPerson/'.$dopParam;
        $deleteAct='/admin/deletePerson/'.$dopParam;
        $table="person";
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        //$tbody['fheight']= "600";
        //$tbody['fwidth']= "1240";
        
        $tbody['creditor_width']= "890";
        $tbody['creditor_height']= "500";   
        $tbody['creditor_big']= "1";   
                
        $tr="";
        for ($j=0; $j<count($arrayPerson); $j++){
        
        $other_name=$arrayPerson[$j]['other_name'];    
        unset($arrayPerson[$j]['id_users'],$arrayPerson[$j]['is_view_pam'], $arrayPerson[$j]['is_view_izb'], 
              $arrayPerson[$j]['is_view_pam'], $arrayPerson[$j]['years'], $arrayPerson[$j]['day'], 
              $arrayPerson[$j]['month'], $arrayPerson[$j]['year'], $arrayPerson[$j]['definition'],
              $arrayPerson[$j]['short_definition'],  $arrayPerson[$j]['text'], $arrayPerson[$j]['short_text'],
              $arrayPerson[$j]['keywords'], $arrayPerson[$j]['description'], $arrayPerson[$j]['title'],
              $arrayPerson[$j]['other_name'],$arrayPerson[$j]['photo'], $arrayPerson[$j]['add_data'], $arrayPerson[$j]['update_data'],
              $arrayPerson[$j]['name'], $arrayPerson[$j]['surname'],
              $arrayPerson[$j]['patronymic'], $arrayPerson[$j]['is_bc'], $arrayPerson[$j]['url'], $arrayPerson[$j]['fam_dp'],
              $arrayPerson[$j]['is_in_encyclopedia'], $arrayPerson[$j]['is_view_news'], $arrayPerson[$j]['fio_full_enc'],
              $arrayPerson[$j]['date_full_enc'], $arrayPerson[$j]['is_portret']);
        
          $i=0; 
            $tr.='<tr>';
                foreach($arrayPerson[$j] as $key => $val ){
                   if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                        $tr.='<td class="center cnt">'.$other_name.'</td>'; 
                        
                   /*}elseif(strrpos($key, "other_") === 0){
                        $tr.="<td>".$arrayPerson[$j]['other_name']."</td>";
                   }else if ($i==2){
                        $tr.="<td>".$arrayPerson[$j]['fam_dp']."</td>"; 
                   */     
                   }elseif(strrpos($key, "is_") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }  
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="'.$table.'" nameidrec="id_person"/></td>';                      
                          
                    }elseif($key === "id_person_forwars"){      
                        if ( ($val==0) || ($val=="")) {
                            $strTooltip=$this->lang['no_forwars_tooltip'];
                            $tr.="<td class=\"tooltips\" title=\"".$strTooltip."\" style='min-width:80px;'>".$this->lang['no_forwars']."</td>";
                        }else{
                            $personForwars=$this->person->getPersons($val);
                            if ($personForwars['is_view_in_main']==1) $dop1=$this->lang['view_main_yes'];
                            else $dop1=$this->lang['view_main_no'];
                            
                            if ($personForwars['is_in_encyclopedia']==1) $dop2.=$this->lang['enciclopediya_yes'];
                            else $dop2=$this->lang['enciclopediya_no'];
                            
                            $strTooltip=$personForwars['fio'].", id=".$personForwars['id_person']."<br/> ".$dop1."<br/> ".$dop2;
                            
                            $tr.="<td class=\"tooltips\" title=\"".$strTooltip."\">".$personForwars['fio']."</td>";
                        }    
                        
                    }else{                    
                        $tr.="<td>$val</td>";                                          
                    }                                                    
                    $i++; 
                }  
            $arrayPerson[$j]['fio']= urlencode($arrayPerson[$j]['fio']);    
            $tr.='<td style="padding-left: 6px; width:160px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['person_in_rubric'].'" href="/admin/showPersonRubric/?person='.$id_record.'&name='.$arrayPerson[$j]['fio'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-shuffle"></span>
             </a>   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['person_citats'].'" href="/admin/showPersonCitata/?person='.$id_record.'&name='.$arrayPerson[$j]['fio'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-comment"></span>
             </a>   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['person_data'].'" href="/admin/showPersonData/?person='.$id_record.'&name='.$arrayPerson[$j]['fio'].'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-contact"></span>
             </a>   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        }  
        $tbody['tr']=$tr;
       
     return $tbody;
    }   
}

?>