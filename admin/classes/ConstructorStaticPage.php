<?php
class ConstructorStaticPage extends ConstructorElements {
    private $page;
    private $lang;
    private $db;
    
    public function __construct(staticEdit $page, $lang) {
        $this->page = $page;
        $this->lang = $lang;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return ' $("#validateForm").validate({
                        rules: {
                            name: "required",
                            date: {
                                     formRuDate : true
                            },
                        },
                        messages: {
                            name: "'.$this->lang['requiredField'].'",
                        }
                      });';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = $this->lang['lib_static_page'];        
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        $this->page->column = Array ( 
                    "id"=>"",
                    "id-view"=>"ID",
                    "name" => $this->lang['form_thead_name_page'],          //имя страницы
                    "url" => $this->lang['form_thead_url'],                 //адрес
                    "is_visible" => $this->lang['form_thead_is_visible'],   //видимость
                    "position"  => $this->lang['form_thead_position'],      //позиция
                    "id_menu" => $this->lang['form_thead_punct_menu'],      //пункт меню?
                    "is_menu" => $this->lang['form_thead_punct_is_menu'],   //отображать в меню?
                    "date_add" =>  $this->lang['form_thead_date_add'],      //дата создания
                    "id_users"  => $this->lang['news_redactor'],            //автор ресурса
                    );
        
        $columns = Array();          
        for($i=0; $i<=count($this->page->column); $i++){
            $setting.=$i.":{ sorter: false }, ";
        }
        $columns['setting'] = $setting;
        
        $th="";
        $i=0;
        foreach($this->page->column as $key => $val)  { 
            if (strrpos($key, "is_") === 0) $class = ' class="filter-false"';
            else $class = '';
            
            if ($i>0) { $th.="<th$class>$val</th>";}
            $i++;
        }
       
        $columns['th'] = $th; 
        
        $columns['var_sort_old'] = true;
        
        $columns['numb'] = count($this->page->column);
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();        
        $arrayResurs = $this->page->showStatic(); 
       /* 
        echo "<pre>";
        print_r($arrayResurs);
        echo "</pre>";
        */
        
       $idOpenPage = explode(",", $_COOKIE['pharus_tree_showStaticPage']);
       $tbody['tree'] = true;
       
       # var_dump($this->page); exit;
        $dopParam='';
        $addAct='/admin/addStaticPage/'.$dopParam;
        $updateAct='/admin/updateStaticPage/'.$dopParam;
        $formShow='/admin/formShowStaticPage/'.$dopParam;
        $deleteAct='/admin/deleteStaticPage/'.$dopParam;
        
        $panel = new servicePanel();
        
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        //$tbody['fheight']= "594";
        //$tbody['fwidth']= "1110";
        
        $tbody['creditor_width']= "760";
        $tbody['creditor_height']= "220"; 
        $tbody['creditor_big']= "1";  //применяем текстовый редактор
        $tbody['date_area']="1"; //применяем datapicker
        
        $tr="";
        for ($j=0; $j<count($arrayResurs); $j++){
            $i=0; 
            $arrayResurs[$j] = Common::removeStipsSlashes($arrayResurs[$j]); 
             
            $id_page_top= $arrayResurs[$j]['id_static_page_top'];
            $level= $arrayResurs[$j]['level'];
            $id_record=$arrayResurs[$j]['id_static_page'];  
           
            
            $left=( ($level-1)*20)."px";
            if ($level>1) {
                $class='class="tree"'; 
                if ( in_array($id_page_top, $idOpenPage)) $classTr='';
                else $classTr=' class="close"';
            } else {
                $class="";
                $classTr='';
            }
            
            $tr.='<tr'.$classTr.'>';
            
            $tr.='<td class="center"><input type="checkbox" value="'.$id_record.'" name="list" class="checkbox"/></td>';                
            $tr.='<td class="center">'.$id_record.'</td>';                
            //проверка вложенности
             #---------------------------
              $sth1=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE id_static_page_top=?");
              $sth1->bindParam(1, $id_record, PDO::PARAM_INT);
              $sth1->execute();
              //$collection=$sth->fetchAll(PDO::FETCH_ASSOC);
              $nChild=$sth1->rowCount();
              
              $dopClassOpenClose = ( in_array($id_record, $idOpenPage)) ?'ui-icon-minusthick':'ui-icon-plusthick';
              $plus=($nChild>0) ? '<span class="ui-icon treerubric '.$dopClassOpenClose.'"></span>' : '' ;
             #--------------------------
            
            
            
            $tr.="<td class=\"rubrics\"><p $class style='margin-left:$left;' idrec=\"".$id_record."\" idtop=\"".$id_page_top."\" >".$plus." ".$arrayResurs[$j]['name']."</p></td>"; 
            $tr.='<td class="cnt">'.$arrayResurs[$j]['url'].'</td>'; 
             
            $checked=($arrayResurs[$j]['is_visible']== 1)?  'checked="cheked"' : "";
            $tr.='<td class="cnt"><input type="checkbox" value="'.$arrayResurs[$j]['is_visible'].'" name="is_visible" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="static_page" nameidrec="id_static_page"/></td>';                      
            $tr.='<td class="cnt">'.$arrayResurs[$j]['position'].'</td>'; 
           
            if ( (intval($arrayResurs[$j]['id_menu'])!="") || (intval($arrayResurs[$j]['id_menu'])!=0) ){
                $menu = new menuEdit(0);   
                $punct = $menu->showMenu($arrayResurs[$j]['id_menu']);
                
                if ($punct['id_menu_top'] != "0")  $punct['name']=$menu->getParent($punct['id_menu_top']).$punct['name'];
                
                $tr.='<td class="cnt">'.$punct['name'].'"</td>'; 
                        
            }else{
                $tr.='<td class="cnt">-</td>'; 
            }    
            
            $checked =($arrayResurs[$j]['is_menu']== 1)?  'checked="cheked"' : "";
            $tr.='<td class="cnt"><input type="checkbox" value="'.$arrayResurs[$j]['is_menu'].'" name="is_menu" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="static_page" nameidrec="id_static_page"/></td>';                      
            
            
            
            $tr.='<td class="cnt">'.Common::getTimeConversion($arrayResurs[$j]['date_add'], $this->lang, true).'</td>'; 
            
            $user = new userEdit();
            $authorInfo = $user->getUser($arrayResurs[$j]['id_users']);
            //$rol = $user->getRoles($id_author);
            $author=$authorInfo['login']; //." (".$authorInfo['name']." ".$authorInfo['otch'].")";
            
            $tr.='<td class="cnt">'.$author.'</td>'; 
            
           
            $tr.='<td style="padding-left: 40px;">   
            
            <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'" root="'.$id_page_top.'" level="'.$level.'">
              <span class="ui-icon ui-icon-pencil" ></span>
             </a>                   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
               <span class="ui-icon ui-icon-circle-close"></span>
            </a>   
            </td>
        </tr>';
        }  
       
        $tbody['tr']=$tr;
        return $tbody; 
           
    }     
             
}

?>