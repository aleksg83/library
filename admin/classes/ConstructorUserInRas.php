<?php
class ConstructorUserInRas extends ConstructorElements {
    private $ras;
    private $lang;
    private $id;
    
    public function __construct(rassilkaSendEdit $ras, $id, $lang) {
        $this->ras = $ras;
        $this->lang = $lang;
        $this->id = $id;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = $this->lang['rassilka_user_rass'];        
        $path =  '<a href="/admin/rassilkaSend/" title="'.$this->lang['rassilka_send_page'].'">'.$this->lang['rassilka_send_page'].'</a>';   
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' >'.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        
        $columns = Array();          
        for($i=0; $i<=2; $i++){
            //$setting.=$i.":{ sorter: false }, ";
        }
        $columns['setting'] = $setting;
        
        $th="<th>".$this->lang['form_thead_user_send_name']."</th>";
        $th.="<th>".$this->lang['form_thead_user_send_email']."</th>";
       
        $columns['th'] = $th;   
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();        
        $sth = $this->db->prepare("SELECT u.name, u.email FROM ".PREFIX."_user_email as u 
                                    inner join ".PREFIX."_rel_rassilka_user_email as rel 
                                    inner join ".PREFIX."_rassilka  as ras 
                                where u.id_user_email = rel.id_user_email and ras.id_rassilka=rel.id_rassilka and ras.id_rassilka=? ");
        
        $sth->bindParam(1, $this->id, PDO::PARAM_INT);
        $sth->execute();
        $collection=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        $tr="";
        for ($j=0; $j<count($collection); $j++){
            $tr.='<tr>';
            $tr.="<td>".$collection[$j]['name']."</td>";
            $tr.="<td class='cnt'>".$collection[$j]['email']."</td>";
            $tr.='</tr>';
        }  
        
        $tbody['tr']=$tr;
        return $tbody;    
    }    
}
?>