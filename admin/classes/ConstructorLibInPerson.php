<?php
class ConstructorLibInPerson extends ConstructorElements {
    private $lib;
    private $lang;
    private $id;
    public  $type;
    
    public function __construct(libEdit $lib, $id, $type, $lang) {
        $this->lib = $lib;
        $this->lang = $lang;
        $this->id = $id;
        $this->type=$type;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = $this->lang['person_in_lib'];        
        $path =  '<a href="/admin/showLib/'.$this->lib->type.'/" title="'.$this->lang['lib_'.$this->lib->type.''].'">'.$this->lang['lib_'.$this->lib->type.''].'</a>';   
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' >'.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        
        $columns = Array();          
        for($i=0; $i<=2; $i++){
            //$setting.=$i.":{ sorter: false }, ";
        }
        
        $th="<th>".$this->lang['personname']."</th>";
        $th.="<th>".$this->lang['person_forwars']."</th>";
        $th.="<th class=\"filter-false\">".$this->lang['connection']."</th>";

        $columns['th'] = $th;   
        $columns['hidden_check_var2'] = true;
        $columns['hidden_action'] = true;
        
        $setting.="2:{ sorter: 'checkbox' }";
        $columns['setting'] = $setting; 
        $columns['numb'] = 3;
        
        
       //$columns['style']='style="width:600px;"';
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
      
     if ( ($this->lib->type=="book") || ($this->lib->type=="article") || ($this->lib->type=="filmstrip") || ($this->lib->type=="thesis") ){
         
        $tbody = Array();        
        
        $panel = new servicePanel();
        $dopParam='?data='.$_GET['data'].'&name='.urlencode($_GET['name']);
        
        if ( ($this->lib->type == "book") || ($this->lib->type == "article") || ($this->lib->type =="filmstrip") ){
              $countTypePerson = 3;
        }else if ($this->lib->type == "thesis"){
              $countTypePerson = 5;
        }else{
            $countTypePerson = 0;
        }    
        
       
        for ($i=1; $i<=$countTypePerson; $i++){
         $act='/admin/showLibInPerson/'.$this->lib->type.'/'.$i.'/'.$dopParam;
         if ($this->type==$i) {
             $class="";
         }else{
              $class="open";
         }
             $tbody['person_'.$i] = $panel->getPersonButton($act, $this->lang['person_service_'.$i], $this->lang['person_service_'.$i.'_hover'], $class); 
        }
       
       
        
        $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_rel_data_person WHERE id_data=? and id_sp_type_person=?");
        $sth->bindParam(1, $this->id, PDO::PARAM_INT);
        $sth->bindParam(2, $this->type, PDO::PARAM_INT);
        $sth->execute();
        $collection=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        $collectionIdRub=array();
        
        foreach($collection as $key => $val){
               array_push($collectionIdRub, $val['id_person']);
        }
        
        $person = new PersonEdit();
        $arraySp = $person->getPersons(); 
        
        $tr="";
        for ($j=0; $j<count($arraySp); $j++){
            $id_record=$arraySp[$j]['id_person'];
                   
             $tr.='<tr>';
             $tr.="<td><p>".$arraySp[$j]['fio']."</p></td>";
             
             
             if ( ($arraySp[$j]['id_person_forwars']==0) || ($arraySp[$j]['id_person_forwars']=="")) {
                  $strTooltip=$this->lang['no_forwars_tooltip'];
                  $tr.="<td class=\"tooltips\" title=\"".$strTooltip."\" style='min-width:80px;'>".$this->lang['no_forwars']."</td>";
             }else{
                 
                 $personForwars=$person->getPersons($arraySp[$j]['id_person_forwars']);
                 if ($personForwars['is_view_in_main']==1) $dop1=$this->lang['view_main_yes'];
                 else $dop1=$this->lang['view_main_no'];
                            
                  if ($personForwars['is_in_encyclopedia']==1) $dop2.=$this->lang['enciclopediya_yes'];
                  else $dop2=$this->lang['enciclopediya_no'];
                            
                  $strTooltip=$personForwars['fio'].", id=".$personForwars['id_person']."<br/> ".$dop1."<br/> ".$dop2;
                            
                  $tr.="<td class=\"tooltips\" title=\"".$strTooltip."\">".$personForwars['fio']."</td>";
                 
             }          
            
             if ( in_array($id_record, $collectionIdRub) ){
                  $val=1;
             } else {
                  $val=0;
             }
          
            if ($val == 1) {
              $checked = 'checked="cheked"';
             } else {
              $checked="";
             }     
             
            $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="is_person_lib_'.$j.'" class="checkInPersonLib" '.$checked.' id_record="'.$id_record.'" id_data="'.$this->id.'" id_sp_type_person="'.$this->type.'" action="/adminajax/setIsPersonInLib/" /></td>                      
            </tr>';
        }  
       
        $tbody['tr']=$tr;
     }else{
         $tbody['tr']='<tr><td>'.$this->lang['person_no_link_type'].'</td><td></td><td></td></tr>'; 
     }  
        return $tbody;    
    }    
}

?>