<?php

class Common{
     
    static public function createURL($name){
        $converter = array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'zh','з'=>'z','и'=>'i','й'=>'j','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'sch','ь'=>'','ы'=>'y','ъ'=>'','э'=>'e','ю'=>'yu','я'=>'ya','А'=>'A','Б'=>'B','В'=>'V','Г'=>'G','Д'=>'D','Е'=>'E','Ё'=>'E','Ж'=>'Zh','З'=>'Z','И'=>'I','Й'=>'Y','К'=>'K','Л'=>'L','М'=>'M','Н'=>'N','О'=>'O','П'=>'P','Р'=>'R','С'=>'S','Т'=>'T','У'=>'U','Ф'=>'F','Х'=>'H','Ц'=>'C','Ч'=>'Ch','Ш'=>'Sh','Щ'=>'Sch','Ь'=>'\'','Ы'=>'Y','Ъ'=>'\'','Э'=>'E','Ю'=>'Yu','Я'=>'Ya');
        return trim(preg_replace('~[^-a-z0-9_]+~u', '-', strtolower(strtr($name, $converter))), "-").'.html';        
    }
    
    static function pre($text){
        echo "<pre>";
        print_r($text);
        echo "</pre>"; 
    }
   
   #----------------------------------------------------------------------------
   static function GetMonth($lang){
        $options = array();
        $options[0]="---"; 
        $options[1]=$lang["month.jan"]; 
        $options[2]=$lang["month.feb"]; 
        $options[3]=$lang["month.mar"]; 
        $options[4]=$lang["month.apl"]; 
        $options[5]=$lang["month.may"]; 
        $options[6]=$lang["month.iun"]; 
        $options[7]=$lang["month.iul"]; 
        $options[8]=$lang["month.avg"]; 
        $options[9]=$lang["month.sen"]; 
        $options[10]=$lang["month.okt"]; 
        $options[11]=$lang["month.nob"]; 
        $options[12]=$lang["month.dec"]; 
        
        return $options;       
    }
    
    public function getMonthName($n){
        $lngConst = new lng();
        $this->lang=$lngConst->getArrayLng();
        $month_arr = array( 
            '01' => $this->lang['month.janr'], 
            '02' => $this->lang['month.febr'], 
            '03' => $this->lang['month.marr'], 
            '04' => $this->lang['month.aplr'], 
            '05' => $this->lang['month.mayr'], 
            '06' => $this->lang['month.iunr'],  
            '07' => $this->lang['month.iulr'], 
            '08' => $this->lang['month.avgr'], 
            '09' => $this->lang['month.senr'],  
            '10' => $this->lang['month.oktr'],  
            '11' => $this->lang['month.nobr'], 
            '12' => $this->lang['month.decr']);
        return $month_arr[$n];
    }
   #----------------------------------------------------------------------------
    
   #----------------------------------------------------------------------------
   # функция превода текста с русского языка в траслит
   static function encodestring($st, $flag=false){
      $converter = array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'zh','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'sch','ь'=>'\'','ы'=>'y','ъ'=>'\'','э'=>'e','ю'=>'yu','я'=>'ya','А'=>'A','Б'=>'B','В'=>'V','Г'=>'G','Д'=>'D','Е'=>'E','Ё'=>'E','Ж'=>'Zh','З'=>'Z','И'=>'I','Й'=>'Y','К'=>'K','Л'=>'L','М'=>'M','Н'=>'N','О'=>'O','П'=>'P','Р'=>'R','С'=>'S','Т'=>'T','У'=>'U','Ф'=>'F','Х'=>'H','Ц'=>'C','Ч'=>'Ch','Ш'=>'Sh','Щ'=>'Sch','Ь'=>'\'','Ы'=>'Y','Ъ'=>'\'','Э'=>'E','Ю'=>'Yu','Я'=>'Ya');
      if (!$flag) $str_result=trim(preg_replace('~[^-a-z0-9_]+~u', '_', strtolower(strtr($st, $converter))), "_");        
      else $str_result=trim(preg_replace('~[^-a-z0-9_]+~u', '.', strtolower(strtr($st, $converter))), "_");        
      // Возвращаем результат.
      return $str_result;
    }
   #----------------------------------------------------------------------------
    
   #---------------------------------------------------------------------------- 
   static function GetExtUploadFiles() { 
        return "txt,html,htm,xml,js,css,zip,gz,rar,z,tgz,tar,htaccess,mp3,mp4,aac,wav,au,wmv,avi,mpg,mpeg,pdf,djvu,doc,docx,xls,xlsx,ppt,pptx,jpg,jpeg,png,gif,psd,ico,bmp,odt,ods,odp,odb,odg,odf";
   }
   #----------------------------------------------------------------------------
   
   #----------------------------------------------------------------------------
   static function GetUploadMaxSize() { 
        return intval(ini_get('upload_max_filesize'));
   }
   #----------------------------------------------------------------------------
   
   #----------------------------------------------------------------------------
   #downloadFile
   static function downloadFile($dirFileDownload) {
	   
        if (!empty($_FILES['file']['tmp_name'])){
	
            
              //преобразование имени файла
              $_FILES['file']['name']=  self::encodestring($_FILES['file']['name'], true);
              
                        
              $extArray=explode(",",  self::GetExtUploadFiles());
	      $ext = substr(strrchr($_FILES['file']['name'], "."), 1); 
			  
	      if (in_array($ext,$extArray)) {
	  		   
                if ( intval(self::getFileSizeFormat(filesize($_FILES['file']['tmp_name']))) <= self::GetUploadMaxSize() ) {
				   
                    $DirPuth=$dirFileDownload;
                    $dir = @opendir($DirPuth);
                   
                    if (!$dir) {
                     mkdir ($DirPuth, 0777);
                     $dir = opendir($DirPuth);
                    }
					  
                    $files = $DirPuth.$_FILES['file']['name'];
				       
                    if (file_exists($files)){
                        mt_srand(time());
                        $r = mt_rand(0,1000);  //случайное число
                       $files=$DirPuth.$r.basename($files); 
                    }
					   
					   
                    if (copy($_FILES['file']['tmp_name'], $files)){
                        unlink($_FILES['file']['tmp_name']);
                        return $files;				   
                    }else{
		     return '';
                     }
		} 
               
                return '';
             }
	 }else{
	    return '';
			  
	 } 
    } 
    #downloadFile
    #---------------------------------------------------------------------------
    
    #---------------------------------------------------------------------------
    static function getFileSizeFormat($filesize){
        $formats = array('Б','КБ','МБ','ГБ','ТБ');// варианты размера файла
        $format = 0;// формат размера по-умолчанию
    
        while ($filesize > 1024 && count($formats) != ++$format){
        $filesize = round($filesize / 1024, 2);
        }
        $formats[] = 'ТБ';
    
     return $filesize." ".$formats[$format];
    }
    #---------------------------------------------------------------------------
    static function getTimeConversion($datetime, $lang, $timeno=false)
    {
        $year=substr($datetime, 0, 4);			     		  
        $mes_c=substr($datetime, 5, 2);

        
        if ($mes_c=='01') $str=$lang['month.janr'];
        if ($mes_c=='02') $str=$lang['month.febr'];
        if ($mes_c=='03') $str=$lang['month.marr'];
        if ($mes_c=='04') $str=$lang['month.aplr'];
        if ($mes_c=='05') $str=$lang['month.mayr'];
        if ($mes_c=='06') $str=$lang['month.iunr'];
        if ($mes_c=='07') $str=$lang['month.iulr'];
        if ($mes_c=='08') $str=$lang['month.avgr'];
        if ($mes_c=='09') $str=$lang['month.senr'];
        if ($mes_c=='10') $str=$lang['month.oktr'];
        if ($mes_c=='11') $str=$lang['month.nobr'];
        if ($mes_c=='12') $str=$lang['month.decr'];

        
        $day=substr($datetime, 8, 2);
        $minsec=substr($datetime, 11, 5);

        $times_ret=$day." ".$str." ".$year." ".$lang['y_v']." ".$minsec;
       
        //возвращаем без времени
        if ($timeno==true)  $times_ret=$day." ".$str." ".$year;
        
        
        return trim($times_ret);
    }
    #---------------------------------------------------------------------------
    static function getTimeConversionForm($datetime)
    {
        $year=substr($datetime, 0, 4);			     		  
        $mes_c=substr($datetime, 5, 2);
        $day=substr($datetime, 8, 2);
        $minsec=substr($datetime, 11, 8);

        $times_ret=$day.".".$mes_c.".".$year." ".$minsec;
        return $times_ret;
    }
    #---------------------------------------------------------------------------
    static function getTimeConversionInsertDB($datetime)
    {
        
        $day=substr($datetime, 0, 2);
        $mes=substr($datetime, 3, 2);
        $year=substr($datetime, 6, 4);	
       
        $minsec=substr($datetime, 11, 8);
        
        $times_ret=$year."-".$mes."-".$day." ".$minsec;
       
        return trim($times_ret);
    }
   
   #---------------------------------------------------------------------------
    
    /// rmdir_recursive
    /** Delete all files in directory
    * @param $path directory to clean
    * @param $recursive delete files in subdirs
    * @param $delDirs delete subdirs
    * @param $delRoot delete root directory
    * @access public
    * @return success
    */
    static function rmdir_recursive($path,$recursive=true,$delDirs=true, $delRoot=true)
    {
         $result=true;
         if($delRoot===null) $delRoot=$delDirs;
         if(!$dir=@dir($path)) return false;
         while($file=$dir->read())
         {
            if($file==='.' || $file==='..') continue;

            $full=$dir->path.DIRECTORY_SEPARATOR.$file;
            if(is_dir($full) && $recursive){
                
                $result&=self::rmdir_recursive($full,$recursive,$delDirs,$delDirs);
                
            }else if(is_file($full)){
                
                $result&=unlink($full);
                
            }
            
          }
          $dir->close();
          if($delRoot)
          {
            $result&=rmdir($dir->path);
           }
        return $result;
     }
    
   #---------------------------------------------------------------------------
    static function rmdir_recursive_del($dir) {
        $files = scandir($dir);
        array_shift($files); // remove "." from array
        array_shift($files); // remove ".." from array

        foreach ($files as $file) {
            $file = $dir."/".$file;
            if (is_dir($file)) {
               self::rmdir_recursive($file);
            if (is_dir($file))
                chmod($file, 0777);
                rmdir($file);
            } else {
                chmod($file, 0777);
                unlink($file);
            }
        }
            chmod($dir, 0777);
            rmdir($dir);
}

#------------------------------------------------------------------------------  
 static function removingCharacter ($text) {
        return str_replace("\\r\\n", "", $text);
 }
 
#------------------------------------------------------------------------------  
 static function removeStipsSlashes($parametr) {
      if (is_array($parametr)) {
          foreach ($parametr as $key => $val){
                
               if (is_array($parametr[$key])){
                    self::removeStipsSlashes($parametr[$key]);
                }else{
                    $parametr[$key]=trim(stripslashes($val));
                }    
          }
      }    
      
      return $parametr;
 }
 #------------------------------------------------------------------------------  
 
 #------------------------------------------------------------------------------
 #------------------------------------------------------------------------------  
 static function multiplicityOfFive($year_start, $month_start, $day_start, $year_now, $month_now, $day_now, $is_bc) {
     
     $date_out=$year_now;
     
     if ($is_bc == "0"){ //our era
         
         $difference= intval($date_out)-intval($year_start);
         
     } else { //bc
         $difference= intval($date_out)+intval($year_start)-1;
         
     }
     
      $stop=false;
           for ($i=1; $i<=5; $i++){
              if (!$stop){  
                 if ($difference % 5 == 0) {
                      $stop=true; 
                 }else{
                     $difference++;
                     $date_out++;
                 }
             }   
          }
     
     
      if ($date_out == $year_now){  //если год текущий, надо проверить и месяц и день
           if ($month_start < $month){
             $date_out=$year_now+5;
           } else if ($day_start < $day){   
             $date_out=$year_now+5; 
           }
      }
     
      return $date_out;
     
 }
 #------------------------------------------------------------------------------
 
 #------------------------------------------------------------------------------  
 static function InsertPredPrint($id, $type, $db) {
     
     #находим пользователей предпринта по типу ресурса
     if ($type=="news"){
         $userGroup="PRINTNEWS";
         $id_data=0;
         $id_news=$id;
         
     }else if ($type=="data"){
         $userGroup="PRINTRESURS";
          $id_data=$id;
          $id_news=0;
     }
     //кол-во пользователей предпринта
     $sth=$db->prepare("SELECT 
                                us.id_users, us.login, us.prioritet 
                                    FROM ".PREFIX."_users as us 
                                         LEFT JOIN ".PREFIX."_rel_users_roles  as rel using(id_users)
                                         LEFT JOIN ".PREFIX."_roles  as rol  using(id_roles)
                                    WHERE rol.code=? ORDER BY us.prioritet");
      
     $sth->bindParam(1, $userGroup, PDO::PARAM_STR);
     $sth->execute();
     $results = $sth->fetchAll(PDO::FETCH_ASSOC);
     $count = $sth->rowCount();
    
     //echo "<pre>"; print_r($results); echo "</pre>";
          
     $is_done=0;//по умолчанию для всех пользователей материал не утвержден
     for ($j=0; $j<$count; $j++){
        $pr = $j+1; 
      
        $sth = $db->prepare("INSERT INTO ".PREFIX."_rel_users_pred_print (id_users, id_data, id_news, prioritet, is_done) VALUES (?, ?, ?, ?, ?)");
        $sth->bindParam(1, $results[$j]['id_users'], PDO::PARAM_INT);
        $sth->bindParam(2, $id_data, PDO::PARAM_INT);
        $sth->bindParam(3, $id_news, PDO::PARAM_INT);
        $sth->bindParam(4, $pr, PDO::PARAM_INT);
        $sth->bindParam(5, $is_done, PDO::PARAM_INT);
        $sth->execute();        
     }
     
 }
 #------------------------------------------------------------------------------  
 
 
 #------------------------------------------------------------------------------  
 static function CountPredPrint($id_user) {
     $sql = new Sql();
     $db = $sql->connect();
     
     $sth=$db->prepare("SELECT 
                                *
                                    FROM ".PREFIX."_rel_users_pred_print
                                    WHERE id_users=? and is_done='0'");
      
     $sth->bindParam(1, $id_user, PDO::PARAM_INT);
     $sth->execute();
     $results = $sth->fetchAll(PDO::FETCH_ASSOC);
     $count = $sth->rowCount();
     $i=0;
     
     for ($j=0; $j<$count; $j++){
         $pr=$results[$j]['prioritet']+1;
         
         $sth1=$db->prepare("SELECT 
                                id_rel_users_pred_print
                                    FROM ".PREFIX."_rel_users_pred_print
                                    WHERE id_data=? and id_news=? and prioritet=? and is_done='0'");
         $sth1->bindParam(1, $results[$j]['id_data'], PDO::PARAM_INT);
         $sth1->bindParam(2, $results[$j]['id_news'], PDO::PARAM_INT);
         $sth1->bindParam(3, $pr, PDO::PARAM_INT);
         $sth1->execute();
         $rowCountTemp = $sth1->rowCount();
         if ($rowCountTemp > 0) $count--; 
         
     }
      
     return $count;
 }
 #------------------------------------------------------------------------------ 
 
 #---------------------------------------------------------------------------
 static function is_predprint($type="lib") {
     $sql = new Sql();
     $db = $sql->connect();
     
     $flag = true;
     $id=($type=="lib")? 1 : 5 ;
     
     $sth = $db->prepare("SELECT value FROM ".PREFIX."_main_setting WHERE id_sp_type_data='".$id."' and name='isPredPrint'");
     $sth->bindParam(1, $id, PDO::PARAM_INT);
     $sth->execute();
     $result = $sth->fetch(PDO::FETCH_ASSOC);
          
     return ($result['value']=="1")? true : false ;
 }
 
 
 #---------------------------------------------------------------------------
 static function is_predprintResurs($id, $type="lib") {
     $sql = new Sql();
     $db = $sql->connect();
     
     if ($type=="lib"){
         $sth = $db->prepare("SELECT * FROM ".PREFIX."_rel_users_pred_print WHERE id_data=? and is_done='0'");
     }elseif ($type=="news"){
         $sth = $db->prepare("SELECT * FROM ".PREFIX."_rel_users_pred_print WHERE id_news=? and is_done='0'");
     }
     
     $sth->bindParam(1, $id, PDO::PARAM_INT);
     $sth->execute();
     $result = $sth->fetch(PDO::FETCH_ASSOC);
     $count = $sth->rowCount();
     
     
     return ($count>0)? true : false;
     
 }
 
 //-----------------------------------------------------------------------------
 static function isBlock() {
     $sql = new Sql();
     $db = $sql->connect();
     $sth = $db->query("SELECT value FROM ".PREFIX."_main_setting WHERE name='BlockSaite'");
     $result = $sth->fetch(PDO::FETCH_ASSOC);
     
     return ($result['value']=="1")? true : false;
     
 }
 //-----------------------------------------------------------------------------
  static function getRealIp(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
  }
 //-----------------------------------------------------------------------------
 static function getValueisNameMainSystem($name){
         $sql = new Sql();
         $db = $sql->connect();
         $sth = $db->prepare("SELECT * FROM ".PREFIX."_main_setting WHERE name=?");
         $sth->bindParam(1, $name, PDO::PARAM_INT);
         $sth->execute();
         $result = $sth->fetch(PDO::FETCH_ASSOC);
        
        return $result['value'];
    }
  //----------------------------------------------------------------------------
  static function CountNewFeedBackMessage() {
     $sql = new Sql();
     $db = $sql->connect();
     
     $sth=$db->query("SELECT 
                                id_feedback
                                    FROM ".PREFIX."_feedback
                                    WHERE status='1'");
     $count = $sth->rowCount();
     return $count; 
  }  
  //---------------------------------------------------------------------------- 
   static function ChecksAccessRedactor(){
     $auth = auth::instance();    
     if ($auth->isLogged & !$auth->isAdmin){ 
            if ($auth->isRedactor) {
                  header('Location: /admin/');
            }
     }
   }
  
  //---------------------------------------------------------------------------- 
   static function getLoginUser($id) {
     $sql = new Sql();
     $db = $sql->connect();
     
     $sth = $db->prepare("SELECT * FROM ".PREFIX."_users WHERE id_users=?");
     $sth->bindParam(1, $id, PDO::PARAM_INT);
     $sth->execute();
     $result = $sth->fetch(PDO::FETCH_ASSOC);
        
     return $result['login'];
  }  
}

?>