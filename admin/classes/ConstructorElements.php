<?php
abstract class ConstructorElements {
    abstract public function getValidateForm();
    abstract public function getBreadCrumbs();
    abstract public function getTableHead();
    abstract public function getTableTbody($urlPage);   
}

?>