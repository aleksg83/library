<?php
class ConstructorRassilkaSend extends ConstructorElements {
    
    private $ras;
    private $lang;
        
    public function __construct(rassilkaSendEdit $ras, $lang) {
        $this->ras = $ras;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            tema: "required",
                            
                        },
                        messages: {
                            tema: "'.$this->lang['requiredField'].'",
                                                                  
                        }
                      });';
       
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['rassilka_send_page'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['rassilka_send_page'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        $this->ras->column = Array(
            "id" => "",
            "theme" => $this->lang['form_thead_ras_theme'],
            "date_add" => $this->lang['form_thead_date_add'], 
            "date_send" => $this->lang['form_thead_date_send'], 
            "status" => $this->lang['form_thead_rassilka_status'], 
        );
        
        $columns = Array();                
        for($i=0; $i<=count($this->ras->column)+1; $i++){
           if ( ($i==0) || ($i==count($this->ras->column)+1)) $setting.=$i.":{ sorter: false }, ";
        }    
        
       //$setting.="3:{ sorter: 'checkbox' }, 4:{ sorter: 'checkbox' }, ";
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        
        foreach($this->ras->column as $key => $val) { 
            
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
       
        $tbody = Array();
        $array = $this->ras->getRassilka();
           
        $addAct='/admin/addRassilka/';
        $updateAct='/admin/updateRassilka/';
        $updateActSet='/admin/updateSettingRassilka/';
        $formShow='/admin/formShowRassilka/';
        $deleteAct='/admin/deleteRassilka/';
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        $tbody['right_but'] = $panel->getRightButton($this->lang['rassilka_setting'], $updateActSet, "ui-icon-wrench");
        
        
        $tbody['creditor_width']= "740";
        $tbody['creditor_height']= "220"; 
        //$tbody['creditor_big']= "1";  //применяем текстовый редактор
        $tbody['creditor_no_resize']= "1";
        //$tbody['fheight']= "590";
        $tbody['fwidth']= "790";
       
        $tr="";
        for ($j=0; $j<count($array); $j++){
         
        $array[$j] = Common::removeStipsSlashes($array[$j]);    
        
        $nameSt=$this->ras->getStatusName($array[$j]['id_sp_status']);
        $senderURL='/admin/sendRassilka/'.$array[$j]['id_rassilka'].'/';
        $senderUser='/admin/sendRassilkaUser/'.$array[$j]['id_rassilka'].'/';
        #-----------------------------------------------------------------------
          if ($nameSt=="NEW") {
              $class="ui-icon-lightbulb";
              $title=' title="'.$this->lang['rassilka_new_status'].'"';
              $nameStatus=$this->lang['rassilka_new1_status'];
            $dopButton='
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['rassilka_sender'].'" href="'.$senderURL.'">
              <span class="ui-icon ui-icon-transferthick-e-w"></span>
             </a>';
                      
          }else if($nameSt=="ERROR") {
              $class="ui-icon-alert";
              $title=' title="'.$this->lang['rassilka_error_status'].'"';
              $nameStatus=$this->lang['rassilka_error1_status'];
              $dopButton='
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['rassilka_sender_update'].'" href="'.$senderURL.'">
              <span class="ui-icon ui-icon-transferthick-e-w"></span>
             </a>';
                
          }else if($nameSt=="SEND") {
              $title=' title="'.$this->lang['rassilka_send_status'].'"';
              $class="ui-icon-key";
              $nameStatus=$this->lang['rassilka_send1_status'];
              $dopButton='
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['rassilka_sender_user'].'" href="'.$senderUser.'">
              <span class="ui-icon ui-icon-person"></span>
             </a>';
                
          }    
        #-----------------------------------------------------------------------
        
        
        unset($array[$j]['body'], $array[$j]['file']);
        
         $i=0; 
         $tr.='<tr>';
         foreach($array[$j] as $key => $val ){
            
           if ($i==0){
                $id_record=$val;  
                $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
           
           }elseif ($key=="tema"){  
              
               $tr.="<td> <span class=\"ui-icon {$class} fleft tooltips\" {$title}></span> {$val}</td>"; 
               
           }elseif ($key=="id_sp_status"){  
              
               $tr.="<td class=\"cnt\">{$nameStatus}</td>"; 
               
           }elseif (strrpos($key, "is_") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }                     
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="rassilka" nameidrec="id_rassilka"/></td>';                      
                        
           }elseif (strrpos($key, "date_") === 0){  
               
               if ( ($val != "0000-00-00 00:00:00") && ($val != "") ){
                   $tr.='<td class="cnt">'.Common::getTimeConversion($val, $this->lang).'</td>';
               }else{
                   $tr.='<td class="cnt"></td>';
               }
               
               
           }else{                    
                 $tr.="<td>$val</td>";                                          
           }                                                    
            $i++; 
    }//foreach  
          
            $tr.='
             <td style="padding-left: 30px;"> 
             '.$dopButton.'     
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            
            </td>
        </tr>';
      
  }//for 
         $tbody['tr']=$tr;
       
         return $tbody;
        
    }  
}

?>