<?php
abstract class Elements {
    protected $constructor;
    
    public function __construct(ConstructorElements $constructor) {
        $this->constructor = $constructor;
    }

    public function getValidateForm(){
        return $this->constructor->getValidateForm();
    }
    public function getBreadCrumbs(){
        return $this->constructor->getBreadCrumbs();
    }
    public function getTableHead(){
        return $this->constructor->getTableHead();
    }
    public function getTableTbody($urlPage){
        return $this->constructor->getTableTbody($urlPage);
    }
}

?>