<?php

class menuEdit {
    private $db, $menu=array(), $idNextNode, $level = 0, $err;
    public $column, $isRubr, $parent;
    
    public function __construct($isRubr) {
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->isRubr = $isRubr;
        $this->nextNode=true;
        $this->parent="";
    }
    
    public function showMenu($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE is_rubricator=? and id_menu=? ORDER BY position");
            $sth->bindParam(1, $this->isRubr, PDO::PARAM_INT);
            $sth->bindParam(2, $id, PDO::PARAM_INT);
            $sth->execute();
            $this->menu = $sth->fetch(PDO::FETCH_ASSOC);
            $this->menu['rubr'] = $this->getMenuRubr($id);
            #echo "<pre>"; print_r($this->menu); echo "</pre>"; exit;
        }else{
              $this->getNextMenu();
        }
        #echo "<pre>"; print_r($this->menu); echo "</pre>"; exit;
        return $this->menu;
    }
     
   
    public function addMenu($id_menu_top, $name, $is_visible, $url, $text, $level, $position, $rubs ){
        //$url_name = $this->getUrlName($url); 
        $url_name = $url; //Common::encodestring($url);
        
        $url=$this->prepareInsert("add", $url_name, $id_menu_top, "");
               
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_menu (id_menu_top, name, is_visible, url, text, is_rubricator, url_name, level, position) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $sth->bindParam(1, $id_menu_top, PDO::PARAM_INT);
        $sth->bindParam(2, $name, PDO::PARAM_STR);
        $sth->bindParam(3, $is_visible, PDO::PARAM_INT);
        $sth->bindParam(4, $url, PDO::PARAM_STR);
        $sth->bindParam(5, $text, PDO::PARAM_STR);
        $sth->bindParam(6, $this->isRubr, PDO::PARAM_INT);
        $sth->bindParam(7, $url_name, PDO::PARAM_STR);
        $sth->bindParam(8, $level, PDO::PARAM_INT);
        $sth->bindParam(9, $position, PDO::PARAM_INT);
        $sth->execute();
        
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        
        $err = $sth->errorInfo();
        if($err[0] != '00000' && !$this->isRubr) $this->addRub($this->db->lastInsertId(), $rubs);
        
        return ($err[0] != '00000')?false:true;
    }
    
    public function updateMenu($id_menu_top, $name, $is_visible, $url, $text, $level, $position, $id, $rubs){
        //$url_name = $this->getUrlName($url); 
        
        $url_name = $url; //Common::encodestring($url);
        
        
        //проверка изменился ли родитель
        $tmp=$this->showMenu($id);
        if ($tmp['id_menu_top']!=$id_menu_top){
         //у всех потомкков надо будет изменить уровень level и меню url
         $change=true;
         
          $tmp2=$this->showMenu($tmp['id_menu_top']);
         //проверка детей у старого родителя - если детей нет, надо убрать "/"
          $count=$this->getCountChild($tmp2['id_menu']); 
          
          if ($count==1){ //переносимый узел единственный ребенок
              $length = strlen($tmp2['url']);
              
              if (substr($tmp2['url'], $length-1)=="/"){
                  $newurl=substr($tmp2['url'], 0, $length-1);
                  $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET url=? WHERE id_menu=?");
                  $sth->bindParam(1, $newurl, PDO::PARAM_STR);
                  $sth->bindParam(2, $tmp2['id_menu'], PDO::PARAM_INT);
                  $sth->execute();
              }
          }
        }
        //изменился ли url
        if ($tmp['url_name']!=$id_menu_top){
         //у всех потомкков надо будет изменить  меню url
         $change=true;
        } 
        #-------------------------------------------
       
        $url=$this->prepareInsert("edit", $url_name, $id_menu_top, $id); 
                  
        $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET id_menu_top=?, name=?, is_visible=?, url=?, text=?, is_rubricator=?, url_name=?, level=?, position=? WHERE id_menu=?");
        $sth->bindParam(1, $id_menu_top, PDO::PARAM_INT);
        $sth->bindParam(2, $name, PDO::PARAM_STR);
        $sth->bindParam(3, $is_visible, PDO::PARAM_INT);
        $sth->bindParam(4, $url, PDO::PARAM_STR);
        $sth->bindParam(5, $text, PDO::PARAM_STR);
        $sth->bindParam(6, $this->isRubr, PDO::PARAM_INT);
        $sth->bindParam(7, $url_name, PDO::PARAM_STR);
        $sth->bindParam(8, $level, PDO::PARAM_INT);
        $sth->bindParam(9, $position, PDO::PARAM_INT);
        $sth->bindParam(10, $id, PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        
        if ( ($err[0] == '00000') && $change) {
              $this->updateLevel($id, $level);
              $this->updateUrl($id);
             // $this->delRub($id);
             // $this->addRub($id, $rubs);
        }
        
        return ($err[0] != '00000')?false:true;
        
    }
    
    public function deleteMenu($id){
        $this->delRub($id);
        $arrayDel = explode(",", $id);
        
        for ($i=0; $i<count($arrayDel); $i++){
            $this->clearAllRel($id);
            $this->deleteNode($arrayDel[$i]);    
        }
        
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_menu WHERE id_menu IN ($place_holders)");
        $sth->execute($arrayDel);
        
        $err = $sth->errorInfo();
        //print_r($err);
        //exit();
        return ($err[0] != '00000')?false:true;
    }
    #---------------------------------------------------------------------------
    public function prepareInsert($act, $url_name, $id_menu_top, $id){
        $count=($act=="edit")? $this->getCountChild($id): 0; 
          if ($id_menu_top!="0"){ 
           $info=$this->showMenu($id_menu_top);
           $length = strlen($info['url']);
                 if (substr($info['url'], $length-1)!="/"){
                     $dop= ($count>0)? "/": "";
                     $url=$info['url']."/".$url_name.$dop;
                     
                    //----------------------------update
                    $newurl=$info['url']."/";
                    $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET url=? WHERE id_menu=?");
                    $sth->bindParam(1, $newurl, PDO::PARAM_STR);
                    $sth->bindParam(2, $id_menu_top, PDO::PARAM_INT);
                    $sth->execute();
                    //---------------------------update
               
                 } else {
                     $dop= ($count>0)? "/": "";
                     $url=$info['url'].$url_name.$dop;;
                 }
           
          }else{
              $dop= ($count>0)? "/": "";  
              $url=$url_name.$dop;
          }
        
          return $url;
    }
    #---------------------------------------------------------------------------
    public function clearAllRel($id){
        $rel=Array("rel_person_menu", "rel_data_menu", "rel_menu_rub", "rel_sp_organasation_menu", "static_page");
        for($i=0; $i<count($rel); $i++){
            if ($rel[$i]=="static_page"){
                $n=0; 
                $sth = $this->db->prepare("UPDATE ".PREFIX."_".$rel[$i]." SET id_menu=? WHERE id_menu=?");
                $sth->bindParam(1, $n, PDO::PARAM_INT);
                $sth->bindParam(2, $id, PDO::PARAM_INT);
                $sth->execute();
            }else if ($rel[$i]=="rel_menu_rub"){
                 $sth = $this->db->prepare("DELETE FROM ".PREFIX."_".$rel[$i]." WHERE (id_menu=? or id_rub=?)");
                 $sth->bindParam(1, $id, PDO::PARAM_INT);
                 $sth->bindParam(2, $id, PDO::PARAM_INT);
                 $sth->execute();   
                
            }else{
                 $sth = $this->db->prepare("DELETE FROM ".PREFIX."_".$rel[$i]." WHERE id_menu=?");
                 $sth->bindParam(1, $id, PDO::PARAM_INT);
                 $sth->execute();
            }
        }
    }
    public function showForm($id, $act, $action, $url, $lang){
         $this->idNextNode=$id; //при выводе списка прерываем поиск потомков текущего элемента
         $resultAll = $this->showMenu();
         $result = $this->showMenu($id);
         $level = $result['level'];
         $id_top= $result['id_menu_top'];;
         $options = array();
                 
         $options[0]=$lang['root_node'];
         $parametr[]="";
         $levels[]=" level=\"0\"";
         for($j=0; $j<count($resultAll); $j++) {
            $value="";
            $keigen="";
            $flag=false;            
            if ($resultAll[$j]['id_menu'] != $id){            
                foreach($resultAll[$j]  as $key => $val ){
                    if ($key=="id_menu") $keigen = $val;                     
                    if ($key=="name") $value = $val; 
                    if ($key=="level") $level = $val; 
                 }                    
                $tire = ($level>0)?"-":"";  
                $options[$keigen]=$tire.$value; 
                $parametr[]=' style="padding-left:'.($level*10).'px;"';
                $levels[]=" level=\"$level\"";
            }    
         }
                 
         $flg = (($act=="add") or ($result['is_visible']=="1"))?true:false;         
         if ($act=="add") {
             $result['position']=20;
             $result['level']=1;
         }    
         
         $namePunct = ($this->isRubr)? $lang['rubric'] : $lang['menu_name'];
         $size= ($this->isRubr)? 41 : 58;       
         $class=($this->isRubr)? 'level sm' : 'level big'; //класс списка
         
         
         
         $name =          new field_text("name", $namePunct, true, $result['name'], "", $size);         
         $urlpunct =      new field_text("urlpunct", $lang['menu_url'], true, $result['url_name'], "", $size);         
         //if (!$this->isRubr) $text =  new field_text("text", $this->namefiled['text'], false, $result['text']);            
         if (!$this->isRubr) $text =  new field_textarea("text", $lang['menu_text'], false, $result['text'], 44, 10); 
         else $text =     new field_hidden_int("text", false, "");
         $root =          new field_select( "id_menu_top", $lang['root'], $options, $id_top, false, "", "level", $parametr, $levels, $class); 
         $position =      new field_text_int("position", $lang['menu_position'], true, $result['position'],"","","", 10);
         $is_visible =    new field_checkbox("is_visible", $lang['is_visible'], $flg);        
         $id_rec =        new field_hidden_int("id", false, $id);        
         $url =           new field_hidden_int("url", false, $url);
         $level =         new field_hidden_int("level", false, $result['level']);
         $is_rubricator = new field_hidden_int("is_rubricator", false, $this->isRubr);
          $type_act   = new field_hidden_int("type_act", false, $act);
        
        $form = new form(array("name"   => $name,
                                "urlpunct"   => $urlpunct,
                                "root" => $root,
                                "text"   => $text, 
                                "position"   => $position, 
                                "is_visible"   => $is_visible,
                                "id" => $id_rec,
                                "url" => $url,
                                "level" => $level,
                                "is_rubricator" => $is_rubricator,  
                                "type_act" => $type_act, 
                                ),
                             "",
                             $action);        
        return  $form->print_form();
     }

#----------------------------------------------------------------------------------------------------------------------------------------------------------    
    private function getNextMenu($idMenuTop=0){           
        if($this->isRubr){
            ++$this->level; 
            $dop = ($this->level > 1)?"and id_menu_top = {$this->menu[count($this->menu)-1]['id_menu']}":"";
            $sth = $this->db->prepare("SELECT * FROM pharus_menu WHERE level = ? and is_rubricator = ? {$dop} order by `position`");
            $sth->bindParam(1, $this->level, PDO::PARAM_INT);
            $sth->bindParam(2, $this->isRubr, PDO::PARAM_INT);
        }else{
            $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=? and is_rubricator=? order by position");
            $sth->bindParam(1, $idMenuTop, PDO::PARAM_INT);
            $sth->bindParam(2, $this->isRubr, PDO::PARAM_INT);
        }            
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        if(is_array($result)){  
            $i=0;
            foreach($result as $val){
                 $result[$i]['rubs'] = $this->getMenuRubr($result[$i]['id_menu']);
                 array_push( $this->menu, $result[$i]);
                
                 if ($this->idNextNode!=$val['id_menu']) {
                      $this->getNextMenu($val['id_menu']);
                      --$this->level;
                 }else{
                      $this->idNextNode="";
                 }     
                 $i++;
            }    
        }
    }
     #-------------------------------------------------------------------------- 
     public function updateLevelChild($id, $level){
         if ($id && $level) $this->updateLevel ($id, $level);
     }
     #--------------------------------------------------------------------------
     private function updateLevel($id, $level){
        if (!$id)  $id=0;  
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=? and is_rubricator=?");
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->bindParam(2, $this->isRubr, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        
        if(is_array($result)){  
             $level++;
            
             foreach($result as $val){
                 //echo $val['id_menu']."v".$level; exit();
                 $sthUp = $this->db->prepare("UPDATE ".PREFIX."_menu SET level=? WHERE id_menu=?");
                 $sthUp->bindParam(1, $level, PDO::PARAM_INT);
                 $sthUp->bindParam(2, $val['id_menu'], PDO::PARAM_INT);
                 $sthUp->execute();
                 //$result = $sthUp->fetchAll(PDO::FETCH_ASSOC); 
                 
                 $this->updateLevel($val['id_menu'], $level);
            } 
        }
         
     }   
    #---------------------------------------------------------------------------
     public function updateUrl($id){
        if (!$id)  $id=0;  
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=? and is_rubricator=?");
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->bindParam(2, $this->isRubr, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        
        if(is_array($result)){  
             foreach($result as $val){
                 $count=$this->getCountChild($val['id_menu']); 
                 #-----------------------------------------
                 if ($val['id_menu_top']!="0"){ 
                   $info=$this->showMenu($val['id_menu_top']); 
                   $length = strlen($info['url']);
                        #------------------
                        if (substr($info['url'], $length-1)!="/"){
                            $dop= ($count>0)? "/": "";
                            $url=$info['url']."/".$val['url_name'].$dop;
                     
                            //----------------------------update
                            $newurl=$info['url']."/";
                            $sth = $this->db->prepare("UPDATE ".PREFIX."_menu SET url=? WHERE id_menu=?");
                            $sth->bindParam(1, $newurl, PDO::PARAM_STR);
                            $sth->bindParam(2, $val['id_menu_top'], PDO::PARAM_INT);
                            $sth->execute();
                            //---------------------------update
               
                        } else {
                            $dop= ($count>0)? "/": "";
                            $url=$info['url'].$val['url_name'].$dop;;
                        }
                        #------------------
                 } else {
                   $dop= ($count>0)? "/": "";  
                   $url=$val['url_name'].$dop;  
                 }
                 
                 $sthUp = $this->db->prepare("UPDATE ".PREFIX."_menu SET url=? WHERE id_menu=?");
                 $sthUp->bindParam(1, $url, PDO::PARAM_INT);
                 $sthUp->bindParam(2, $val['id_menu'], PDO::PARAM_INT);
                 $sthUp->execute(); 
                 #---------
                 $this->updateUrl($val['id_menu']);
             }
         
        }
          
    }
     
     
    #--------------------------------------------------------------------------- 
     private function deleteNode($id){        
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=? and is_rubricator=?");
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->bindParam(2, $this->isRubr, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        
        if(is_array($result)){
             foreach($result as $val){
                 $this->deleteNode($val['id_menu']);                 
             }
        }
        $sthDel = $this->db->prepare("DELETE FROM ".PREFIX."_menu WHERE id_menu=?");
        $sthDel->bindParam(1, $id, PDO::PARAM_INT);
        $sthDel->execute();
        $err = $sthDel->errorInfo();
    }   
       
       private function getUrlName($url){
           $splits = explode('/', trim($url,'/')); 
           return array_pop($splits); 
       }
       
       private function getMenuRubr($idMenu){
           $sth = $this->db->prepare("SELECT m.* FROM ".PREFIX."_rel_menu_rub rm
                                        left join ".PREFIX."_menu m on m.id_menu = rm.id_rub
                                      WHERE rm.id_menu=?");
           $sth->bindParam(1, $idMenu, PDO::PARAM_INT);
           $sth->execute();
           return $sth->fetchAll(PDO::FETCH_ASSOC);
       }
       
       private function addRub($idMenu, $rubs){
           if($idMenu){
                $sth = $this->db->prepare("insert into ".PREFIX."_rel_menu_rub (id_menu, id_rub) values (?, ?)");
                foreach($rubs as $rub){
                    $sth->bindParam(1, $idMenu, PDO::PARAM_INT);
                    $sth->bindParam(2, $rub, PDO::PARAM_INT);
                    $sth->execute();
                }
           }
       }
       
       private function delRub($idMenu){
           $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_menu_rub where id_menu = ?");
           $sth->bindParam(1, $idMenu, PDO::PARAM_INT);
           $sth->execute();
       }
       
       public function getParent($idMenu){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu=?");
            $sth->bindParam(1, $idMenu, PDO::PARAM_INT);
            $sth->execute();    
            $menuTemp= $sth->fetch(PDO::FETCH_ASSOC);
            $this->parent.= $menuTemp['name']."->";
            
            if ($menuTemp['id_menu_top'] != "0") {
                $this->getParent($menuTemp['id_menu_top']);
            }
            return $this->parent;    
         
       }
      //------------------------------------------------------------------------
       public function getURLCount($act, $url_name, $id_menu_top, $id){
       $count= ($act=="add") ? 0: 1;
       $dopSelect=false;
       if ($act=="edit"){
           //проверим, изменился ли родитель
           $info=$this->showMenu($id);
           if ($id_menu_top!=$info['id_menu_top']) $count=0;
           if ($id_menu_top==$info['id_menu_top']) {$dopSelect=true;};
       }
       
       $url_name = Common::encodestring($url_name);
       
       //уникальность url
       if (strrpos($url_name, ".html")){
            $length =  strlen($url_name)-5;
            $url_page2 =  substr($url_name, 0, $length);
        }else{
            $url_page2 = $url_name['url_page'].".html";
        }
       
        $sth=$this->db->prepare("SELECT `url_name` FROM ".PREFIX."_menu WHERE id_menu_top=? and (url_name=? or url_name=?) and is_rubricator = ?");
        $sth->bindParam(1, $id_menu_top, PDO::PARAM_STR);
        $sth->bindParam(2, $url_name, PDO::PARAM_STR);
        $sth->bindParam(3, $url_page2, PDO::PARAM_STR);
        $sth->bindParam(4, $this->isRubr, PDO::PARAM_INT);
        $sth->execute();
        $countRecord = $sth->rowCount();
        
        
        if ( ($countRecord>0) && $dopSelect){
             //если уровень не менялся, то надо проверить url на этом уровне, кроме самого узла. И если такой UrL будет найден - вернуть yes
             $sth=$this->db->prepare("SELECT `url_name` FROM ".PREFIX."_menu WHERE id_menu_top=? and (url_name=? or url_name=?) and is_rubricator = ? and id_menu!=?");
             $sth->bindParam(1, $id_menu_top, PDO::PARAM_STR);
             $sth->bindParam(2, $url_name, PDO::PARAM_STR);
             $sth->bindParam(3, $url_page2, PDO::PARAM_STR);
             $sth->bindParam(4, $this->isRubr, PDO::PARAM_INT);
             $sth->bindParam(5, $id, PDO::PARAM_INT);
             $sth->execute();
             $countRecordDop = $sth->rowCount();
            
             $result=($countRecordDop>0) ? "yes" : "no" ;
        } else {
             $result = ($countRecord>$count) ? "yes" : "no" ;
        }     
        return $result;     
       
     } 
     
     
     public function getCountChild($id){
        if (!$id)  $id=0;  
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=? and is_rubricator=?");
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->bindParam(2, $this->isRubr, PDO::PARAM_INT);
        $sth->execute();
        //$result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        return $sth->rowCount();
     }   
     #--------------------------------------------------------------------------
     public function updeteSiteMapXML(){
         ini_set('max_execution_time', 0); 
         $xml='<?xml version="1.0" encoding="UTF-8"?>';
         $xml.='<urlset 
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
             xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" 
             xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 
             http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
             ';
         
         $resurs=$this->showMenu();
         for ($i=0; $i<count($resurs); $i++){
             $xml.='
                  <url>
                      <loc>'.SITE_URL.$resurs[$i]['url'].'</loc>
                   </url>
                   ';
              
              $length = strlen($resurs[$i]['url']); 
              $dop=(substr($resurs[$i]['url'], $length-1)!="/") ? '/': ""; 
            
             //проверим связанные рубрики
              $sth=$this->db->prepare("SELECT id_rub FROM ".PREFIX."_rel_menu_rub WHERE id_menu=?");
              $sth->bindParam(1, $resurs[$i]['id_menu'], PDO::PARAM_INT);
              $sth->execute();
              $collectionRub=$sth->fetchAll(PDO::FETCH_ASSOC);
              for ($j=0;$j<count($collectionRub); $j++){
                  foreach($collectionRub[$j] as $key=>$val){ 
                   $resursTempRub=$this->showRubricsInMenu($val);
                     $xml.='
                      <url>
                          <loc>'.SITE_URL.$resurs[$i]['url'].$dop.$resursTempRub['url'].'</loc>
                     </url>';
                 }  
              }
         }
       
         $xml.='
             </urlset>';
         
         $fp=fopen(DROOT."/sitemap.xml", "w");
         fwrite($fp, $xml);
         fclose ($fp);
         return null;
     }
     
     
     public function showRubricsInMenu($id){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE is_rubricator='1' and id_menu=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $return = $sth->fetch(PDO::FETCH_ASSOC);
          
            return $return;
        } 
    }
     
}

?>