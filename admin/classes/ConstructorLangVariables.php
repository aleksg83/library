<?php
class ConstructorLangVariables extends ConstructorElements {
    
    private $setting, $lang, $column;
        
    public function __construct( settingSystemAndUserEdit $setting, $lang) {
        $this->setting = $setting;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            key: "required",
                            value: "required",
                        },
                        messages: {
                            value: "'.$this->lang['requiredField'].'",
                            key: "'.$this->lang['requiredField'].'",    
                        }
                      });';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['setting_lang_redact'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['setting_lang_redact'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->column = Array(
            "id" => "",
            "key" => $this->lang['setting_lang_key'],
            "value" => $this->lang['setting_lang_value'], 
        );
        
        $columns = Array();                
        for($i=0; $i<=count($this->column)+1; $i++){
           if ( ($i==0) || ($i==count($this->column)) ) $setting.=$i.":{ sorter: false }, ";
        }    
        //$setting.="5:{ sorter: 'checkbox' }, 6:{ sorter: 'checkbox' }, ";
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        
        foreach($this->column as $key => $val) { 
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
         
       // $columns['hidden_check_var'] = true;
       // $columns['hidden_action'] = true;
        $columns['numb'] = count($this->column)+1;
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $lngD="ru";
        
        if (!isset($_GET['file'])) $_GET['file']="portal.php";
        
        $dopParam="?file=".$_GET['file']."&dir=".$lngD;
        
        $addAct='/admin/addLangVariables/'.$dopParam;
        $updateAct='/admin/updateLangVariables/'.$dopParam;
        $formShow='/admin/formShowLangVariables/'.$dopParam;
        $deleteAct='/admin/deleteLangVariables/'.$dopParam;
        
        
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        $tbody['philterLang'] = $panel->filterLibVariables($lngD, $_GET['file'] ,$this->lang);
        
        $tbody['fheight']= "270";
        $tbody['fwidth']= "400";
        
        $file=$directory=DROOT."/lang/{$lngD}/".$_GET['file'];
        include($file);
        $array=$lng;
        
        $tr="";
        foreach($array as $key => $val ){
          $tr.='<tr>';
          $tr.='<td class="center"><input type="checkbox" value="'.$key.'" name="list" class="checkbox"/></td>'; 
          $tr.='<td>'.$key.'</td>'; 
          $tr.='<td>'.$val.'</td>'; 
          $tr.='<td style="padding-left: 40px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$key.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$key.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>';
          $tr.='</tr>';
        }
       $tbody['tr']=$tr;
       
    return $tbody;
    }   
}

?>