<?php

class CitataEdit {   
    private $db;
    public $idPerson;
    
    public function __construct($personId) {
       $sql = new Sql();
       $this->db = $sql->connect();
       $this->idPerson=$personId;
    }
    
    public function getCitats($id=null){
        $id_person = $this->idPerson;
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_citata WHERE id_sp_citata=? and id_person=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->bindParam(2, $id_person, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_citata WHERE id_person=?");
            $sth->bindParam(1, $id_person, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
           }        
        return $result;
    }
    
    public function add($dataCitats){
        
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_sp_citata (id_person, value) VALUES (:id_person, :value)");
        $sth->bindParam(":id_person", $dataCitats['id_person'], PDO::PARAM_INT); 
        $sth->bindParam(":value", trim(Common::removingCharacter($dataCitats['value'])), PDO::PARAM_STR);
        $sth->execute();
        $err = $sth->errorInfo();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        return ($err[0] != '00000')?false:true;
    }
      
    public function update($dataCitats){
              
        $sth = $this->db->prepare("UPDATE ".PREFIX."_sp_citata SET id_person=:id_person, value=:value   WHERE id_sp_citata=:id_sp_citata");
        $sth->bindParam(":id_person", $dataCitats['id_person'], PDO::PARAM_INT);
        $sth->bindParam(":value", Common::removingCharacter($dataCitats['value']), PDO::PARAM_STR);
        $sth->bindParam(":id_sp_citata", $dataCitats['id'], PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        
        return ($err[0] != '00000')?false:true;
    }
    
    public function delete($idCitata){
        $arrayDel = explode(",", $idCitata);
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_sp_citata WHERE id_sp_citata IN ($place_holders)");
        $sth->execute($arrayDel);        
        $err = $sth->errorInfo();
        
        return ($err[0] != '00000')?false:true;
    }
    
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id, $act, $action, $url, $lang){
       //  $citataAllList = $this->getCitats();
         $citata = $this->getCitats($id);
     //  $size=41; 
        
         $value =         new field_textarea("value", $lang['citata_form_name'], false, $citata['value'], 38, 3, "", "", "", ""); 
         
         $id_person =     new field_hidden_int("id_person", false, $this->idPerson);  
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
         
        $form = new form(array("value"   => $value,
                               "id" => $id_rec,
                               "url" => $url,
                               "id_person"=>$id_person
                                ),
                             "",
                             $action);        
        return  $form->print_form();
     }
     //form
     #---------------------------------------------------------------------------------------------
}

?>