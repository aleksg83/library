<?
class spEdit {
    
    private $db, $namefiled;
    public $namePage, $column, $name;
    
    public function __construct($name) {
        $this->name = $name;
        $sql = new Sql();
        $this->db = $sql->connect();
       //$this->namefiled = $sql->getTableColumns("sp_".$this->name);
       //$this->namePage=$sql->getCommentTable("_sp_".$this->name);
       //$this->column = $sql->getTableColumns("sp_".$this->name);
    }
    
    public function add($data){
        if ($this->name == "organasation"){
           $data = $this->prepareInsertData($data);
       
            $sth = $this->db->prepare("INSERT INTO ".PREFIX."_sp_organasation (id_users, is_visible, name, day, month, year, is_view_in_main, is_view_news, text, short_text, definition, url, is_in_encyclopedia, keywords, description, title, photo, add_data, position) VALUES (:id_users, :is_visible, :name, :day, :month, :year, :is_view_in_main, :is_view_news, :text, :short_text, :definition, :url, :is_in_encyclopedia, :keywords, :description, :title, :photo, :add_data, :position)");
            $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
            $sth->bindParam(":is_visible", $data['is_visible'], PDO::PARAM_INT);
            $sth->bindParam(":name", $data['name'], PDO::PARAM_STR);
            $sth->bindParam(":day", $data['day'], PDO::PARAM_INT);
            $sth->bindParam(":month", $data['month'], PDO::PARAM_INT);
            $sth->bindParam(":year", $data['year'], PDO::PARAM_INT);
            $sth->bindParam(":is_view_in_main", $data['is_view_in_main'], PDO::PARAM_INT);
            $sth->bindParam(":is_view_news", $data['is_view_news'], PDO::PARAM_INT);
            $sth->bindParam(":text", trim($data['text']), PDO::PARAM_STR);
            $sth->bindParam(":short_text", $data['short_text'], PDO::PARAM_STR);
            $sth->bindParam(":definition", $data['definition'], PDO::PARAM_STR);
            $sth->bindParam(":url", $data['url_sp_org'], PDO::PARAM_STR);
            $sth->bindParam(":is_in_encyclopedia", $data['is_in_encyclopedia'], PDO::PARAM_INT);
            $sth->bindParam(":keywords", $data['keywords'], PDO::PARAM_STR);
            $sth->bindParam(":description", $data['description'], PDO::PARAM_STR);
            $sth->bindParam(":title", $data['title'], PDO::PARAM_STR);
            $sth->bindParam(":photo", Common::downloadFile('data/organisation/'), PDO::PARAM_STR);
            $sth->bindParam(":add_data", date("Y-m-d H:i:s"), PDO::PARAM_STR);
            $sth->bindParam(":position", $data['position'], PDO::PARAM_INT);
            
        }else if ( ($this->name == "vid_news") || ($this->name == "theme_news") ){
            if ($data['is_visible']=="") $data['is_visible']=0;
            $data['url_name']= ($data['url_name']=="") ? Common::encodestring($data['name']) : Common::encodestring($data['url_name']);
            
            $sth = $this->db->prepare("INSERT INTO ".PREFIX."_sp_{$this->name} (name, is_visible, url, position) VALUES (?, ?, ?, ?)");
            $sth->bindParam(1, $data['name'], PDO::PARAM_STR);
            $sth->bindParam(2, $data['is_visible'], PDO::PARAM_INT);
            $sth->bindParam(3, $data['url_name'], PDO::PARAM_STR);
            $sth->bindParam(4, $data['position'], PDO::PARAM_INT);
            
        }else{
            if ($data['is_visible']=="") $data['is_visible']=0;
            
            $sth = $this->db->prepare("INSERT INTO ".PREFIX."_sp_{$this->name} (name, is_visible, position) VALUES (?, ?, ?)");
            $sth->bindParam(1, $data['name'], PDO::PARAM_STR);
            $sth->bindParam(2, $data['is_visible'], PDO::PARAM_INT);
            $sth->bindParam(3, $data['position'], PDO::PARAM_INT);
        }
        
        $sth->execute();
        $err = $sth->errorInfo();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        return ($err[0] != '00000')?false:true;
    }
    
    public function update($data){
        
        if ($this->name == "organasation"){
             $data=$this->prepareInsertData($data, "edit"); 
             
            #------------------------------------------------------------------------------
            #File
            if ( ($data['delImages']=="1") || !empty($_FILES['file']['tmp_name']) ) {
                     $spTemp = $this->select($data['id']);
                
                     if ( ($spTemp['photo'] != "") && file_exists($spTemp['photo']) ) {
                     unlink($spTemp['photo']);
                     }
             }
             if ( !empty($_FILES['file']['tmp_name']) )  {
                     $newPhoto = true;
                     $sql='photo=:photo,';
             }
             #File
             #------------------------------------------------------------------------------
         
             $sth = $this->db->prepare("UPDATE ".PREFIX."_sp_organasation SET  is_visible=:is_visible, name=:name, day=:day, month=:month, year=:year, is_view_in_main=:is_view_in_main, is_view_news=:is_view_news, text=:text, short_text=:short_text, definition=:definition, url=:url, is_in_encyclopedia=:is_in_encyclopedia, keywords=:keywords, description=:description, title=:title, {$sql} update_data=:update_data, position=:position  WHERE id_sp_organasation=:id_sp_organasation");
             $sth->bindParam(":is_visible", $data['is_visible'], PDO::PARAM_STR);
             $sth->bindParam(":name", $data['name'], PDO::PARAM_STR);
             $sth->bindParam(":day", $data['day'], PDO::PARAM_INT);
             $sth->bindParam(":month", $data['month'], PDO::PARAM_INT);
             $sth->bindParam(":year", $data['year'], PDO::PARAM_INT);
             $sth->bindParam(":is_view_in_main", $data['is_view_in_main'], PDO::PARAM_INT);
             $sth->bindParam(":is_view_news", $data['is_view_news'], PDO::PARAM_INT);
             $sth->bindParam(":text",  trim($data['text']), PDO::PARAM_STR);
             $sth->bindParam(":short_text", $data['short_text'], PDO::PARAM_STR);
             $sth->bindParam(":definition", $data['definition'], PDO::PARAM_STR);
             $sth->bindParam(":url", $data['url_sp_org'], PDO::PARAM_STR);
             $sth->bindParam(":is_in_encyclopedia", $data['is_in_encyclopedia'], PDO::PARAM_INT);
             $sth->bindParam(":keywords", $data['keywords'], PDO::PARAM_STR);
             $sth->bindParam(":description", $data['description'], PDO::PARAM_STR);
             $sth->bindParam(":title", $data['title'], PDO::PARAM_STR);
             if ($newPhoto) $sth->bindParam(":photo", Common::downloadFile('data/organisation/'), PDO::PARAM_STR);
             $sth->bindParam(":update_data", date("Y-m-d H:i:s"), PDO::PARAM_STR);
             $sth->bindParam(":position", $data['position'], PDO::PARAM_INT);
             $sth->bindParam(":id_sp_organasation", $data['id'], PDO::PARAM_INT);
             
        
        
        }else if ( ($this->name == "vid_news") || ($this->name == "theme_news") ){
            
            if ($data['is_visible']=="") $data['is_visible']=0;
            $data['url_name']= ($data['url_name']=="") ? Common::encodestring($data['name']) : Common::encodestring($data['url_name']);
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_sp_{$this->name} SET name=?, is_visible=?, url=?, position=? WHERE id_sp_{$this->name}=?");
            $sth->bindParam(1, $data['name'], PDO::PARAM_STR);
            $sth->bindParam(2, $data['is_visible'], PDO::PARAM_INT);
            $sth->bindParam(3, $data['url_name'], PDO::PARAM_STR);
            $sth->bindParam(4, $data['position'], PDO::PARAM_STR);
            $sth->bindParam(5, $data['id'], PDO::PARAM_INT);
            
        }else{
            if ($data['is_visible']=="") $data['is_visible']=0;
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_sp_{$this->name} SET name=?, is_visible=?, position=? WHERE id_sp_{$this->name}=?");
            $sth->bindParam(1, $data['name'], PDO::PARAM_STR);
            $sth->bindParam(2, $data['is_visible'], PDO::PARAM_INT);
            $sth->bindParam(3, $data['position'], PDO::PARAM_STR);
            $sth->bindParam(4, $data['id'], PDO::PARAM_INT);
        }
        
        $sth->execute();
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    
    public function columnUp($id, $isVisible){
        $sth = $this->db->prepare("UPDATE ".PREFIX."_sp_{$this->name} SET is_visible=? WHERE id_sp_{$this->name}=?");
        $sth->bindParam(1, $isVisible, PDO::PARAM_INT);
        $sth->bindParam(2, $id, PDO::PARAM_INT);
        $sth->execute();
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
        
    }
    
    public function delete($id){
        $arrayDel = explode(",", $id);
        
        if ($this->name == "organasation"){
            #-----------------------------------------------------------------------
            $arrayDelPhoto = array();
                for($i=0; $i<count($arrayDel); $i++){
                 $spTemp = $this->select($arrayDel[$i]);
                 if ($spTemp['photo'] != "") {
                    array_push($arrayDelPhoto, $spTemp['photo']);
                 }
            }
            #-----------------------------------------------------------------------
        }
        
        
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_sp_{$this->name} WHERE id_sp_{$this->name} IN ($place_holders)");
        $sth->execute($arrayDel);
        
        $err = $sth->errorInfo();
        
        
        if ($this->name == "organasation"){
            if ($err[0] == '00000'){
                 for($i=0; $i<count($arrayDelPhoto); $i++){
                     if ( ($arrayDelPhoto[$i] != "") && file_exists($arrayDelPhoto[$i]) ) {
                      unlink($arrayDelPhoto[$i]);
                    }
                }
            }
        }
        
        
        return ($err[0] != '00000')?false:true;
    }
    
    public function select($id=null){        
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_{$this->name} WHERE id_sp_{$this->name}=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_sp_{$this->name}");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }        
        return $result;
    }
    
    public function getMainOrg(){        
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_sp_{$this->name} WHERE is_view_in_main='1'");
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
    
    #-----------------------------------------------------------
    public function selectNews(){        
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_sp_{$this->name} WHERE is_view_news='1' ORDER by position");
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    
    #----------------------------------------------------------------------------------------------  
     public function prepareInsertData($data, $type="add"){
          $data['url_sp_org'] = ($data['url_sp_org']=="")? Common::createURL($data['name']) : $data['url_sp_org'];
          $data['url_sp_org'] = $this->getUniqueURL($data['url_sp_org'], $type, $data['id']);
         
          if ($type=="add") $data['title']=($data['title']!="")? $data['title']:$data['name'];
          
          $data['text']=  Common::removingCharacter($data['text']);
          $data['short_text']=  Common::removingCharacter($data['short_text']);
        
        if ($data['is_visible']=="") $data['is_visible']=0;  
        if ($data['is_view_in_main']=="") $data['is_view_in_main']=0;
        if ($data['is_in_encyclopedia']=="") $data['is_in_encyclopedia']=0;
        if ($data['is_view_news']=="") $data['is_view_news']=0;
        
        return $data;
     }
    #----------------------------------------------------------------------------------------------  
     public function getUniqueURL($url, $type="add", $id=0){
        //уникальность url 
        if ($type=="add"){
            $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_sp_organasation  WHERE url=?");
            $sth->bindParam(1, $url, PDO::PARAM_STR);
        }else if ($type=="edit"){    
             $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_sp_organasation WHERE id_sp_organasation!=? and url=?");
             $sth->bindParam(1, $id, PDO::PARAM_STR);
             $sth->bindParam(2, $url, PDO::PARAM_STR); 
        }    
            $sth->execute();
            //$err = $sth->errorInfo();
            $count = $sth->rowCount();
           
            if ( $count > 0){
              $i=$count+1;   
              if (strrpos($url, ".html")){
                  $length =  strlen($url)-5;
                  $temp   =  substr($url, 0, $length);
                  $url=$temp."-".$i.".html";
                  
              }else{
                  $url=$url."-".$i;
              }
            }
         #-----------------------------------------------------------------------  
      
        return $url;
    }
    
    public function showForm($id=null, $act, $action, $url, $lng){
         #var_dump($id); exit;
         $result = $this->select($id);
         $result = Common::removeStipsSlashes($result);
         
         $flg = (($act=="add") or ($result['is_visible']=="1"))?true:false;
         $is_visible = new field_checkbox("is_visible", $lng['visible'], $flg);
         $id = new field_hidden_int("id", false, $id);
         $url = new field_hidden_int("url", false, $url);
         
         #----------------------------------------------------------------------
         if ($this->name == "organasation"){
              
              $flg2 = ($result['is_in_encyclopedia']=="1")?true:false;   
              $flg3 = ($result['is_view_in_main']=="1")?true:false;   
              $flg4 = ($result['is_view_news']=="1")?true:false;   
              
              $size=41; 
              $size2=123; 
              $class='sm0'; //класс списка
              $classForwars='sm0 comboboxComplite'; //класс списка
              $paramTextField['class']="creditor";
              $paramTextField['id']="creditor_id";
              $paramTextField['column']="columns-2"; //определяем поле в правую колонку формы, форма получиться двух-колоночная
         
              $paramTextFieldDefinition['column']="columns-2";
              
              if ( ($result['photo'] != "") && file_exists($result['photo']) ) {
                    $paramImgFieldDefinition['file']='<a href="/'.$result['photo'].'" class="group" target="_blank"><img src="/'.$result['photo'].'" class="photoPerson"/></a>
                                              <span class="photoPerson photoDel">'.$lng['delete_photo'].' <input type="checkbox" name="delImages" value="0"/></span>';
                 }else{
                    $paramImgFieldDefinition['file']="";
              }
             
              $optionsDay = array();
              for($j=0; $j<32; $j++) {
                 $optionsDay[$j]=$j;   
              }
         
             $optionsMonth=Common::GetMonth($lng);
              
              $name = new field_text("name", $lng['form_thead_sp_org_name'], true, $result['name'], "", $size);
              
              $text =          new field_textarea("text", $lng['person_text'], false, $result['text'], 38, 10, "", "", "", $paramTextField); 
              $short_text =    new field_textarea("short_text", $lng['short_text'], false, $result['short_text'], 93, 6, "", "", "", $paramTextFieldDefinition); 
              $definition =    new field_textarea("definition", $lng['org_definition'], false, $result['definition'], 93, 6, "", "", "", $paramTextFieldDefinition); 
              $is_in_encyclopedia = new field_checkbox("is_in_encyclopedia", $lng['is_in_encyclopedia'], $flg2, $paramTextFieldDefinition);   
              $is_view_in_main = new field_checkbox("is_view_in_main", $lng['is_view_in_main'], $flg3, $paramTextFieldDefinition);   
              $is_view_news = new field_checkbox("is_view_news", $lng['is_view_sp_org_news'], $flg4, $paramTextFieldDefinition);   
              $day =           new field_select("day", $lng['sp_org_day'], $optionsDay, $result['day'], false, "", "", "", "", $class); 
              $month =         new field_select("month", $lng['sp_org_month'], $optionsMonth, $result['month'], false, "", "", "", "", $class); 
              $year =          new field_text("year", $lng['sp_org_year'], false, $result['year'], "", $size);         
              $urlSpOrg =     new field_text("url_sp_org", $lng['person_url'], false, $result['url'], "", $size);         
              $position =      new field_text("position", $lng['menu_position'], false, $result['position'], "", $size);         
              $photo =         new field_file("file", $lng['person_photo'], false, "", "", $paramImgFieldDefinition);         
              
              $title =         new field_text("title", $lng['title'], false, $result['title'], "", $size2, $paramTextFieldDefinition);         
              $description =   new field_textarea("description", $lng['meta_description'], false, $result['description'], 93, 2, "", "", "", $paramTextFieldDefinition); 
              $keywords =      new field_text("keywords", $lng['meta_keywords'], false, $result['keywords'], "", $size2, $paramTextFieldDefinition);         
         
              
             $form = new form(array("name"   => $name,
                               "is_visible"   => $is_visible,
                               "day" => $day,
                               "month" =>$month, 
                               "year" => $year,
                               "url_sp_org" => $urlSpOrg,
                               "position" => $position,
                               "file" => $photo,
                               "text"   => $text,
                               "is_in_encyclopedia" => $is_in_encyclopedia,
                               "short_text" => $short_text,
                               "definition" => $definition,
                               "is_view_in_main" => $is_view_in_main,
                               "is_view_news" => $is_view_news,
                               "title" => $title, 
                               "description" => $description,
                               "keywords" => $keywords,
                               "id" => $id,
                               "url" => $url,
                                ), "", $action);
             
         
         #----------------------------------------------------------------------    
         }else{
             
            if ( ($this->name == "vid_news") || ($this->name == "theme_news") ){
                    $name = new field_text("name", $lng['form_thead_name'], true, $result['name']);
                    $url_name = new field_text("url_name", $lng['form_thead_url'], false, $result['url']);
                    $position = new field_text("position", $lng['menu_position'], false, $result['position']);
                    $form = new form(array("name"   => $name,
                                           "url_name" => $url_name, 
                                           "is_visible"   => $is_visible,
                                           "id" => $id,
                                           "url" => $url,
                                           "position" => $position,
                                ), "", $action);
              
            }else{
                    $name = new field_text("name", $lng['form_thead_name'], true, $result['name']);
                    $position = new field_text("position", $lng['menu_position'], false, $result['position']);
                    $form = new form(array("name"   => $name,
                                           "is_visible"   => $is_visible,
                                           "id" => $id,
                                           "url" => $url,
                                           "position" => $position,
                                           
                                ), "", $action);
            }        
         }    
        return  $form->print_form();
    }   
}
?>