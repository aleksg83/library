<?php
class newsSendUserEdit{
    private $db,  $error;
    public $column;
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect(); 
        Common::ChecksAccessRedactor();
    }
    
    public function getUsers($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_user_email WHERE id_user_email=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_user_email");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        
        return $result;
    }
    
    public function getUsersSender(){
       
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_user_email WHERE is_active='1' and is_black_list='0'");
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
    
    public function add($data){
        
        if (!isset($data['is_active'])) $data['is_active']=0; 
        if (!isset($data['is_black_list'])) $data['is_black_list']=0; 
        
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_user_email (name, email, is_active, is_black_list, sex) VALUES (?, ?, ?, ?, ?) ");        
        $sth->bindParam(1, $data['name'], PDO::PARAM_STR);
        $sth->bindParam(2, $data['email'], PDO::PARAM_STR);
        $sth->bindParam(3, $data['is_active'], PDO::PARAM_INT);
        $sth->bindParam(4, $data['is_black_list'], PDO::PARAM_INT);
        $sth->bindParam(5, $data['sex'], PDO::PARAM_INT);
        $sth->execute();        
        $err = $sth->errorInfo();
        $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        return ($err[0] != '00000')?false:true;
       
    }
    
         
    public function update($data){
        
        if (!isset($data['is_active'])) $data['is_active']=0; 
        if (!isset($data['is_black_list'])) $data['is_black_list']=0; 
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_user_email SET name=?, email=?, is_active=?, is_black_list=?, sex=? WHERE id_user_email=?");        
        $sth->bindParam(1, $data['name'], PDO::PARAM_STR);
        $sth->bindParam(2, $data['email'], PDO::PARAM_STR);
        $sth->bindParam(3, $data['is_active'], PDO::PARAM_INT);
        $sth->bindParam(4, $data['is_black_list'], PDO::PARAM_INT);
        $sth->bindParam(5, $data['sex'], PDO::PARAM_INT);
        $sth->bindParam(6, $data['id'], PDO::PARAM_INT);
        $sth->execute();        
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
       
    }
    
    public function delete($id){
       
        $arrayDel = explode(",", $id);
        
        foreach($arrayDel as $key=>$val){
            //-------------------------------------------------------------------
             $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_rassilka_user_email WHERE id_user_email=?");
             $sth->bindParam(1, $val, PDO::PARAM_INT);
             $sth->execute();
                
        } 
       
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_user_email WHERE id_user_email IN ($place_holders)");
        $sth->execute($arrayDel);
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
       
    }
  #---------------------------------------------------------------------------------------------------------------------------------------------------    
  #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id, $act, $action, $url, $lang){
        $users = $this->getUsers($id);
        //$users = Common::removeStipsSlashes($users);
        
        $size=41; 
       
        $options[0]=$lang['women'];
        $options[1]=$lang['men'];
        
         
        $flg = ( ($users['is_active']=="1") || ($act=="add") ) ?true:false;     
        $flg2 = ($users['is_black_list']=="1")?true:false; 
        
        $name =          new field_text("name", $lang['form_thead_user_send_name'], true, $users['name'], "",$size);         
        $email =         new field_text("email", $lang['form_thead_user_send_email'], false, $users['email'], "", $size);         
        $is_active =     new field_checkbox("is_active", $lang['form_thead_user_is_active'], $flg, "");   
        $is_black_list =    new field_checkbox("is_black_list", $lang['form_thead_is_black_list'], $flg2, "");
        
        $sex =          new field_select( "sex", $lang['form_thead_user_send_sex'], $options, $users['sex'], false, "", "","", "", ""); 
        
        $id_rec =        new field_hidden_int("id", false, $id);  
        $url =           new field_hidden_int("url", false, $url);
          
        $form = new form(array("name" => $name,
                               "email" => $email,
                               "sex" => $sex,
                               "is_active" => $is_active,
                               "is_black_list" => $is_black_list,
                               "id" => $id_rec,
                               "url" => $url,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
       
     }
     //form
     #---------------------------------------------------------------------------------------------
    
}

?>