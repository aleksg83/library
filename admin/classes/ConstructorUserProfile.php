<?php
class ConstructorUserProfile extends ConstructorElements {
    
    private $user;
    private $lang;
  
    public function __construct( userEdit $user, $lang) {
        $this->user = $user;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
       
        return '';
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['user_profile'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        return  '';
     
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $user = $this->user->getUser($_SESSION['user']['user_id']);
            
        
        $dopParam='';
        $updateAct='/admin/updateProfile/'.$dopParam;
        $urlPage=$urlPage.$dopParam;
        
       
        $tbody['text-div']='
            
	<script type="text/javascript">
        $().ready(function() {
        
	    $("#validateForm").validate({
            	rules: {
			 password_new:{
                                minlength: 6,
                         } ,
                },    
                 messages: {
                              password_new: "'.$this->lang['requiredFieldPassword'].'",   
			
                 }
                    
                   
            });
            
          });  
	</script>

        <form class="forms" id="validateForm" method="post" action="'.$updateAct.'"> 
            <fieldset>
                <ul>
                    <li>
			<label class="desc" for="login">'.$this->lang['login'].'</label>
			<div><input id="login" class="field text full" name="login" disabled="disabled" value="'.$user['login'].'" /></div>
                    </li>
                    <li>
                        <label class="desc" for="fam">'.$this->lang['user_fam'].'</label>
                        <div><input id="fam" class="field text full" name="fam" value="'.$user['fam'].'" /></div>
                    </li>
                    <li>
                        <label class="desc" for="name">'.$this->lang['user_name'].'</label>
                        <div><input id="name" class="field text full" name="name" value="'.$user['name'].'" /></div>
                    </li> 
                     <li>
                        <label class="desc" for="otch">'.$this->lang['user_otch'].'</label>
                        <div><input id="otch" class="field text full" name="otch" value="'.$user['otch'].'" /></div>
                    </li> 
                    <li>
                        <label class="desc" for="email">'.$this->lang['user_email'].'</label>
                        <div><input id="email" class="field text full" name="email" value="'.$user['email'].'" /></div>
                    </li> 
                    <li>
                        <label class="desc" for="phone">'.$this->lang['user_phone'].'</label>
                        <div><input id="phone" class="field text full" name="phone" value="'.$user['phone'].'" autocomplete="off" /></div>
                    </li> 


		   <li>
                        <label class="desc" for="password_new">'.$this->lang['password_new'].'</label>
			<div><input id="password_new" class="field text full" name="password_new" type="password" value="" autocomplete="off"/></div>
                   </li>
                   <li>
			<label class="desc" for="confirm_password">'.$this->lang['password_old'].'</label>
			<div><input id="password_old" class="field text full" name="password_old" type="password" value=""/></div>
                   </li>
                  
		<li>
                        <input  type="hidden" name="url" value="'.$urlPage.'"/>
                        <input  type="hidden" name="id_users" value="'.$_SESSION['user']['user_id'].'"/>    
			<input class="update" type="submit" value="'.$this->lang['user_profile_update'].'"/>
                </li>
		</ul>
		</fieldset>
            </form>    
        ';
               
     return $tbody;
    
    }  
            
}

?>