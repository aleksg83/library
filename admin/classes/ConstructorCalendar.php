<?php
class ConstructorCalendar extends ConstructorElements {
    private $cal;
    private $lang;
    
    public function __construct( calendarEdit $cal, $lang) {
        $this->cal = $cal;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return ' $("#validateForm").validate({
                        rules: {
                            title: "required",
                            date_to: {
                                     formRuDateToDate : true
                            },
                        },
                        messages: {
                            title: "'.$this->lang['requiredField'].'",
                        }
                      });';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = $this->lang['lib_calendarj'];        
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
    #---------------------------------------------------------------------------   
    if ($this->cal->type == "1") { 
        $this->page->column = Array ( 
                    "id"=>"",
                    "date_from" => $this->lang['calendar_date_from'],       //дата старта
                    "date_to" => $this->lang['calendar_date_to'],           //дата окончания
                    "title" => $this->lang['calendar_name'],                //название
                    "period" => $this->lang['cal_repeat_type'],            //переодичность
                    "period_value" => $this->lang['cal_repeat_value'],      //переодичность
                    "is_visible" => $this->lang['form_thead_is_visible'],   //видимость
                    "is_main" => $this->lang['form_thead_cal_main'],  //выводить на главной
                    "id_users"  => $this->lang['news_redactor'],            //автор ресурса
                   );
        
        $columns = Array();          
        for($i=0; $i<=count($this->page->column); $i++){
            if ( ($i==0) || ($i=count($this->page->column))) $setting.=$i.":{ sorter: false }, ";
        }
        
        $setting.="6:{ sorter: 'checkbox' }, 7:{ sorter: 'checkbox' }";
        
        $columns['setting'] = $setting;
        
      #---------------------------------Person----------------------------------------  
      } else if ($this->cal->type == "2"){
         
           $this->page->column = Array ( 
                    "id"=>"",
                    "name" => $this->lang['news_person'],                      //персона
                    "date" => $this->lang['person_date_happy'],                 //дата рождения
                    "year_in_calendarj" => $this->lang['year_in_calendarj'],    //год показа в календаре событий
                             
                   );
           
        $columns['hidden_check_var2'] = true;
        $columns['hidden_action'] = true;
        $columns['setting'] = "";
        $columns['numb'] = 3;
      
      #--------------------------------Organization---------------------------------------     
      } else if ($this->cal->type == "3"){
          
           $this->page->column = Array ( 
                    "id"=>"",
                    "name" => $this->lang['calendarj_organasation'],            //организация
                    "date" => $this->lang['calendarj_organasation_date_happy'], //дата создания
                    "year_in_calendarj" => $this->lang['year_in_calendarj'],    //год показа в календаре событий
                             
           );
           
        $columns['hidden_check_var2'] = true;
        $columns['hidden_action'] = true;
        $columns['setting'] = "";
        $columns['numb'] = 3;
          
      }
      
        $th="";
        $i=0;
        foreach($this->page->column as $key => $val)  { 
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            
            if ($i>0) { $th.="<th$class>$val</th>";}
            $i++;
        }
      
        $columns['th'] = $th; 
        
       // $columns['hidden_check'] = true;
        
        $columns['numb'] = count($this->page->column);
        
        return  $columns;
    
    }
    
    public function getTableTbody($urlPage){
     $tbody = Array();        
     
     $dopParam=$this->cal->type.'/';
     $addAct='/admin/addEvent/'.$dopParam;
     $updateAct='/admin/updateEvent/'.$dopParam;
     $formShow='/admin/formShowEvent/'.$dopParam;
     $deleteAct='/admin/deleteEvent/'.$dopParam;
     $urlCalendar='admin/showEvents/';
       
     $panel = new servicePanel();
     $tbody['philter_calendar'] = $panel->getPhilterCalButton($this->lang, $urlCalendar, $this->cal->type);
     
     
     if ($this->cal->type == "1"){
           
       $arrayResurs = $this->cal->getEvents(); 
       
       /* echo "<pre>";
        print_r($arrayResurs);
        echo "</pre>";
        */
       
       if (!isset($_POST['date_from'])){
            $_POST['date_from'] = $_SESSION["PharusAdminPhilterDateStart"];
            $_POST['date_to'] = $_SESSION["PharusAdminPhilterDateEnd"];
       }     
       
       if ( ($_POST['date_from'] != "") || ($_POST['date_to'] != "")){ //если в фильтре установлены данные
       
             $_POST['date_from']=($_POST['date_from']=="")? date("d.m.Y"): $_POST['date_from']; 
             if ($_POST['date_to']==""){
                $day=new DateTime($_POST['date_from']);
                $day->modify('+1 year'); 
                $_POST['date_to']=$day->format('d.m.Y');
             }
             
            //формируем 
            $date_from=Common::getTimeConversionInsertDB($_POST['date_from']);
            $date_to=Common::getTimeConversionInsertDB($_POST['date_to']);
            
            $start = new DateTime($date_from);
            $d1=$start->format("Y-m-d");
            $end = new DateTime($date_to); 
            $d2=$end->format("Y-m-d");
            //$date1 = explode("-", $date_from);
            //$date2 = explode("-", $date_to);
            //$result = mktime(0, 0, 0, $date2[1], $date2[2], $date2[0]) - mktime(0, 0, 0, $date1[1], $date1[2], $date1[0]);
            //if ($result>=0){
         
         if ($d2 >= $d1){ 
             
              $arrayShowResurs = Array(); 
              $k=0;
                
              for ($j=0; $j<count($arrayResurs); $j++){  
            
                
                  //если тип повторения равен "Никогда", то это событие не может образовывать цепочек
                 //проверяем только это событие
                 if ( intval($arrayResurs[$j]['repeat_type'])==0 ){
                     
                     $startEv = new DateTime($arrayResurs[$j]['date_from']);
                     $dEv1=$startEv->format("Y-m-d");
                     $endEv = new DateTime($arrayResurs[$j]['date_to']); 
                     $dEv2=$endEv->format("Y-m-d");
                     
                     if ( (($dEv1>=$d1) && ($dEv1 <= $d2)) || ($d2<=$dEv2) && ($dEv1<=$d2)  ) {
                     
                       $arrayShowResurs[$k]=array(
                             'id_calendar' => $arrayResurs[$j]['id_calendar'], 
                             'date_from' => $arrayResurs[$j]['date_from'], 
                             'date_to' => $arrayResurs[$j]['date_to'], 
                             'title' => $arrayResurs[$j]['title'], 
                             'text' => $arrayResurs[$j]['text'], 
                             'repeat_type' => $arrayResurs[$j]['repeat_type'], 
                             'repeat_value' => $arrayResurs[$j]['repeat_value'], 
                             'is_bc' => $arrayResurs[$j]['is_bc'], 
                             'is_visible' => $arrayResurs[$j]['is_visible'], 
                             'is_main' => $arrayResurs[$j]['is_main'], 
                             'id_users' => $arrayResurs[$j]['id_users'], 
                             );
                         $k++;
                     }
              #-------------------------------------------------------------------------------------
              #-------------------------------------------------------------------------------------    
                //если тип повторения равен "Год", то это событие  может образовывать серию цепочек с периодом value
                //}else if ( intval($arrayResurs[$j]['repeat_type'])==4){
                 }else{   
                        $data_from_temp=$arrayResurs[$j]['date_from'];
                        $data_to_temp=$arrayResurs[$j]['date_to'];
                         
                        do{
                            
                            $startEv = new DateTime($data_from_temp);
                            $dEv1=$startEv->format("Y-m-d");
                            $endEv = new DateTime($data_to_temp); 
                            $dEv2=$endEv->format("Y-m-d");
                            
                            if ( (($dEv1>=$d1) && ($dEv1 <= $d2)) || ($d2<=$dEv2) && ($dEv1<=$d2)  ) {
                                $arrayShowResurs[$k]=array(
                                   'id_calendar' => $arrayResurs[$j]['id_calendar'], 
                                   'date_from' => $data_from_temp, 
                                   'date_to' => $data_to_temp, 
                                   'title' => $arrayResurs[$j]['title'], 
                                   'text' => $arrayResurs[$j]['text'], 
                                   'repeat_type' => $arrayResurs[$j]['repeat_type'], 
                                   'repeat_value' => $arrayResurs[$j]['repeat_value'], 
                                   'is_bc' => $arrayResurs[$j]['is_bc'], 
                                   'is_visible' => $arrayResurs[$j]['is_visible'], 
                                   'is_main' => $arrayResurs[$j]['is_main'], 
                                   'id_users' => $arrayResurs[$j]['id_users'], 
                                );
                                 $k++;
                             }
                             //шаг
                             $step=intval($arrayResurs[$j]['repeat_value']);
                             $typeRecord=intval($arrayResurs[$j]['repeat_type']);
                             
                             if ($typeRecord==4){ 
                                $data_from_temp = $startEv->modify('+'.$step.' year');
                                $data_to_temp =  $endEv->modify('+'.$step.' year'); 
                             }
                             if ($typeRecord==3){ 
                                $data_from_temp = $startEv->modify('+'.$step.' month');
                                $data_to_temp =  $endEv->modify('+'.$step.' month'); 
                               
                             }
                             if ($typeRecord==2){ 
                                $steps=7*$step; 
                                $data_from_temp = $startEv->modify('+'.$steps.' day');
                                $data_to_temp =  $endEv->modify('+'.$steps.' day'); 
                             }
                             if ($typeRecord==1){ 
                                $data_from_temp = $startEv->modify('+'.$step.' day');
                                $data_to_temp =  $endEv->modify('+'.$step.' day'); 
                             }
                             
                             $data_from_temp = $data_from_temp->format('Y-m-d');
                             $data_to_temp = $data_to_temp->format('Y-m-d');
                          
                            if  ( ($data_from_temp>$d2) && ($data_to_temp>$d2) ) {
                                $flag=false;
                            }else{
                                $flag=true;
                            } 
                             
                        } while ($flag);
                   
                 }                    
                 #------------------------------------------------------------------------------------- 
             
              
             }//for 
               
            #--------------- SORT BY DATE_FROM --------------------------------    
            function cmp($a, $b) {
                $date1 = new DateTime($a['date_from']);
                $d1=$date1->format('Y-m-d');
                
                $date2 = new DateTime($b['date_from']);
                $d2=$date2->format('Y-m-d');
                
                
                if ($d1 == $d2) {
                    return 0;
                }
                return ($d1 < $d2) ? -1 : 1;
            }
            
            uasort($arrayShowResurs, 'cmp');
           // echo "<pre>"; var_dump($arrayShowResurs); echo "</pre>";
            #------------------------------------------------------------------- 
             
          } // if result > 0;
       }
       
        
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        $tbody['philter_date'] = $panel->getPhilterDateButton($this->lang, $urlCalendar, $this->cal->type);
     
        
        $tbody['fheight']= "500";
        $tbody['fwidth']= "780";

        //$tbody['creditor_width']= "760";
        //$tbody['creditor_height']= "220"; 
        //$tbody['creditor_big']= "1";  //применяем текстовый редактор
        $tbody['date_area2']="1"; //применяем datapicker для двух полей
        
        //$arrayShowResurs
        #----------------------------------------------------------
        if (count($arrayShowResurs)>0){
           //обнуляем массив и присвоем ему вновь сформированный 
           $arrayResurs=Array();
           $i=0;
           foreach ($arrayShowResurs as $value){
               foreach ($value as $key=>$val){
                  $arrayResurs[$i][$key]=$val; 
               }
               $i++;
           }
        }
        #----------------------------------------------------------
        
        $tr="";
        for ($j=0; $j<count($arrayResurs); $j++){
            $arrayResurs[$j] = Common::removeStipsSlashes($arrayResurs[$j]); 
          
            $id_record=$arrayResurs[$j]['id_calendar'];  
            
            $tr.='<td class="center"><input type="checkbox" value="'.$id_record.'" name="list" class="checkbox"/></td>';                
           
            $tr.="<td class=\"cnt\">".Common::getTimeConversion($arrayResurs[$j]['date_from'], $this->lang, true)."</td>"; 
            $tr.="<td class=\"cnt\">".Common::getTimeConversion($arrayResurs[$j]['date_to'], $this->lang, true)."</td>"; 
            $tr.='<td class="cnt">'.$arrayResurs[$j]['title'].'</td>'; 
            
            switch (intval($arrayResurs[$j]['repeat_type']) ){
                case 0: 
                    $nameType=$this->lang['cal_repeat_no'];
                    break;
                case 1: 
                    $nameType=$this->lang['cal_repeat_day'];
                    break;
                case 2: 
                    $nameType=$this->lang['cal_repeat_week'];
                    break;
                case 3: 
                    $nameType=$this->lang['cal_repeat_month'];
                    break;
                case 4: 
                    $nameType=$this->lang['cal_repeat_year'];
                    break;
            }
            
            $tr.='<td class="cnt">'.$nameType.'</td>'; 
            $tr.='<td class="cnt">'.$arrayResurs[$j]['repeat_value'].'</td>'; 
             
            $checked=($arrayResurs[$j]['is_visible']== 1)?  'checked="cheked"' : "";
            $tr.='<td class="cnt"><input type="checkbox" value="'.$arrayResurs[$j]['is_visible'].'" name="is_visible" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="calendar" nameidrec="id_calendar"/></td>';                      
            
            $checked2=($arrayResurs[$j]['is_main']== 1)?  'checked="cheked"' : "";
            $tr.='<td class="cnt"><input type="checkbox" value="'.$arrayResurs[$j]['is_main'].'" name="is_main" class="check" '.$checked2.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="calendar" nameidrec="id_calendar"/></td>';                      
              
            
            
            $user = new userEdit();
            $authorInfo = $user->getUser($arrayResurs[$j]['id_users']);
            //$rol = $user->getRoles($id_author);
            $author=$authorInfo['login']; //." (".$authorInfo['name']." ".$authorInfo['otch'].")";
            
            $tr.='<td class="cnt">'.$author.'</td>'; 
            
           
            $tr.='<td style="padding-left: 40px;">   
            
            <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil" ></span>
             </a>                   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
               <span class="ui-icon ui-icon-circle-close"></span>
            </a>   
            </td>
        </tr>';
        }  
      
     } else if ($this->cal->type =="2") {
         $person=new PersonEdit();
         $arrayResurs = $person->getPersonsMain(); 
         
          $tr="";
          for ($j=0; $j<count($arrayResurs); $j++){
              $tr.="<tr>";
              $tr.='<td>'.$arrayResurs[$j]['fio'].'</td>';
              
              $arrayResurs[$j]['day']= ( ($arrayResurs[$j]['day']=="") || ($arrayResurs[$j]['day']=="0") ) ? "-" : $arrayResurs[$j]['day'];
              $arrayResurs[$j]['month']= ( ($arrayResurs[$j]['month']=="") || ($arrayResurs[$j]['month']=="0") ) ? "-" : $arrayResurs[$j]['month'];
              $arrayResurs[$j]['year']= ( ($arrayResurs[$j]['year']=="") || ($arrayResurs[$j]['year']=="0") ) ? "-" : $arrayResurs[$j]['year'];
              
              if ( (strlen($arrayResurs[$j]['day'])==1) &&($arrayResurs[$j]['day']!="-") ) $arrayResurs[$j]['day']="0".$arrayResurs[$j]['day'];
              if ( (strlen($arrayResurs[$j]['month'])==1) && ($arrayResurs[$j]['month']!="-")) $arrayResurs[$j]['month']="0".$arrayResurs[$j]['month'];
              
              $bc = ($arrayResurs[$j]['is_bc'] == "1") ? " ".$this->lang['calendar_is_bc'] : "";
              
              $tr.='<td class="cnt">'.$arrayResurs[$j]['day'].'.'.$arrayResurs[$j]['month'].'.'.$arrayResurs[$j]['year'].''.$bc.'</td>'; 
              
              $year=date("Y");
              $month=date("m");
              $day=date("d");
              
              if ($arrayResurs[$j]['year']!="-"){
                $date_out=Common::multiplicityOfFive($arrayResurs[$j]['year'], $arrayResurs[$j]['month'], $arrayResurs[$j]['day'], $year, $month, $day, $arrayResurs[$j]['is_bc']);
              }else{
                $date_out=""; 
              }
              $tr.='<td class="cnt">'.$date_out.'</td>'; 
              $tr.="</tr>";
          }
         
         
     } else if ($this->cal->type =="3") {
         
         $org=new spEdit("organasation");
         $arrayResurs = $org->getMainOrg(); 
         
         $tr="";
         for ($j=0; $j<count($arrayResurs); $j++){
              $tr.="<tr>";
              $tr.='<td>'.$arrayResurs[$j]['name'].'</td>';
              
              
              $arrayResurs[$j]['day']= ( ($arrayResurs[$j]['day']=="") || ($arrayResurs[$j]['day']=="0") ) ? "-" : $arrayResurs[$j]['day'];
              $arrayResurs[$j]['month']= ( ($arrayResurs[$j]['month']=="") || ($arrayResurs[$j]['month']=="0") ) ? "-" : $arrayResurs[$j]['month'];
              $arrayResurs[$j]['year']= ( ($arrayResurs[$j]['year']=="") || ($arrayResurs[$j]['year']=="0") ) ? "-" : $arrayResurs[$j]['year'];
              
              if ( (strlen($arrayResurs[$j]['day'])==1) && ($arrayResurs[$j]['day']!="-")) $arrayResurs[$j]['day']="0".$arrayResurs[$j]['day'];
              if ( (strlen($arrayResurs[$j]['month'])==1) && ($arrayResurs[$j]['month']!="-")) $arrayResurs[$j]['month']="0".$arrayResurs[$j]['month'];
              
              $tr.='<td class="cnt">'.$arrayResurs[$j]['day'].'.'.$arrayResurs[$j]['month'].'.'.$arrayResurs[$j]['year'].'</td>'; 
              
              $year=date("Y");
              $month=date("m");
              $day=date("d");
              
              if ($arrayResurs[$j]['year']!="-"){
                $date_out = Common::multiplicityOfFive($arrayResurs[$j]['year'], $arrayResurs[$j]['month'], $arrayResurs[$j]['day'], $year, $month, $day, "0");
              }else{
                $date_out="";  
              }
              $tr.='<td class="cnt">'.$date_out.'</td>'; 
                           
              $tr.="</tr>";
         }     
         
     }
        
      
        $tbody['tr']=$tr;
        return $tbody; 
         
    }     
             
}

?>