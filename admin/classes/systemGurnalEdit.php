<?php
class systemGurnalEdit{
    private $db;
    public $unserialize, $countSpace;
    
   public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();
   }
   
   public function showSystemGurnalValue($id=null){
       if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_system_gurnal WHERE id_system_gurnal=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
       }else{
            //настройка по умолчанию
            $month=Common::getValueisNameMainSystem('showCountMonthSystemGurnalDefault');
            
            $date_end=date("Y-m-d H:i:s");
            $day=new DateTime($date_end);
            $day->modify('-'.$month.' month'); 
            $date_start=$day->format('Y-m-d H:i:s');
            
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_system_gurnal WHERE date >='{$date_start}' ORDER BY date desc");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }          
        
        return $result;
    }
    #---------------------------------------------------------------------------
    public function showSystemGurnalRangeValue($dateStart, $dateEnd){
      if ($dateStart & $dateEnd){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_system_gurnal WHERE date>=? and date<=?");
            $sth->bindParam(1, $dateStart, PDO::PARAM_STR);
            $sth->bindParam(2, $dateEnd, PDO::PARAM_STR);
            $sth->execute();
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
      }
      
      return $result;
    }
    #---------------------------------------------------------------------------
    
    public function emptyGurnal(){
        $sth = $this->db->query("DELETE FROM ".PREFIX."_system_gurnal");
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
    }
    #---------------------------------------------------------------------------
    public function printSpace($count){
        $return="";
        for($i=0;$i< $count; $i++){
           $return.=" "; 
        }
        return $return;
    }
    #---------------------------------------------------------------------------
    public function getUnserializeArray($value){
        
        foreach ($value as $key=>$val){
            if (is_array($val) ) {
                $n=$this->countSpace*3;
                $this->countSpace=$this->countSpace*3;
                $this->unserialize.="----------------\n";
                $this->unserialize.=$this->printSpace($n)."---".$key." (start) ---\n";
                $this->getUnserializeArray($val);
                $this->unserialize.=$this->printSpace($n)."---".$key." (end) ---\n";
                $this->unserialize.="----------------\n";
            }else{
                $this->unserialize.= $this->printSpace(($this->countSpace+4)).$key." => ".$val."\n";
            }
        } 
        return $this->unserialize;
    }
    #---------------------------------------------------------------------------
    public function showForm($id, $lang){
         $result = $this->showSystemGurnalValue($id);
         
         $this->unserialize="";$this->countSpace=1;
         $res_db_old= ($result['db_old']!="")? $this->getUnserializeArray(unserialize($result['db_old'])) : "" ;
         $this->unserialize=""; $this->countSpace=1;
         $res_db_new= ($result['db_new']!="")? $this->getUnserializeArray(unserialize($result['db_new'])) : "" ;
         
         
         $size=89;
         $action="";
         $date = new field_text("date", $lang['system_gurnal_date'], false, $result['date'], "", $size);
         $id = new field_text("id", $lang['system_gurnal_id_user'], false, $result['id_users'], "", $size);
         $login = new field_text("login", $lang['system_gurnal_login'], false, $result['login'], "", $size);
         $control = new field_text("control", $lang['system_gurnal_action'], false, $result['action'], "", $size);
         $ip = new field_text("ip", $lang['system_gurnal_ip'], false, $result['ip_address'], "", $size);
         
         $dbold = new field_textarea("db_old", $lang['system_gurnal_db_old'], false, $res_db_old, 68, 7, "", "", "", $paramCheck); 
         $dbnew = new field_textarea("db_new", $lang['system_gurnal_db_new'], false, $res_db_new, 68, 7, "", "", "", $paramCheck); 
         
          $form = new form(array("date" => $date,
                                 "id" => $id,
                                 "login"   => $login,
                                 "conrol" => $control,
                                 "ip" => $ip,
                                 "db_old" => $dbold,
                                 "db_new" => $dbnew,
                                 ), 
                                 "", 
                                 $action);
          
         return  $form->print_form();
     }
    #---------------------------------------------------------------------------
    #запись в журнал
       public function insert($action, $db_old=null, $db_new=null){
     
       if ($db_old!="") 
       { 
           $db_old_pr="db_old, "; $db_old_pr_ins=":db_old, "; 
       }else{
           $db_old_pr=""; $db_old_pr_ins=""; 
       } 
       if ($db_new!="") 
       { 
           $db_new_pr="db_new, "; $db_new_pr_ins=":db_new, "; 
       }else{
           $db_new_pr=""; $db_new_pr_ins=""; 
       } 
   
       $sth = $this->db->prepare("INSERT INTO ".PREFIX."_system_gurnal (id_users, login, action, date, {$db_old_pr} {$db_new_pr} ip_address) VALUES (:id_users, :login, :action, :date, {$db_old_pr_ins} {$db_new_pr_ins} :ip_address )");
       $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
       $sth->bindParam(":login", $_SESSION['user']['login'], PDO::PARAM_STR);
       $sth->bindParam(":action", $action, PDO::PARAM_STR);
       $sth->bindParam(":date", date("Y-m-d H:i:s"), PDO::PARAM_STR);
       if ($db_old!="") { $sth->bindParam(":db_old", $db_old, PDO::PARAM_STR);}
       if ($db_new!="") { $sth->bindParam(":db_new", $db_new, PDO::PARAM_STR);}
       $sth->bindParam(":ip_address", Common::getRealIp(), PDO::PARAM_STR);
       $sth->execute();
       $err = $sth->errorInfo();
       return ($err[0] != '00000')?false:true;
          
    } 
     
}

?>