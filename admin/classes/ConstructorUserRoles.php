<?php
class ConstructorUserRoles extends ConstructorElements {
    
    private $user;
    private $lang;
  
    public function __construct( userEdit $user, $lang) {
        $this->user = $user;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        
            return  '$("#validateForm").validate({
                        rules: {
                             name: "required",
                             code: "required",
                             
                        },
                        messages: {
                           name: "'.$this->lang['requiredField'].'",
                           code: "'.$this->lang['requiredField'].'",    
                        }
                      });';
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['user_rolespage'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
      
          $this->lib->column = Array(
             'id' => "",
             'name' => $this->lang['form_thead_user_roles_name'],
             'code' => $this->lang['form_thead_user_roles_code'], 
             
          );
    
        $columns = Array();  
        
        for($i=0; $i<=count($this->lib->column); $i++){
           if ( ($i==0) || ($i=count($this->lib->column))) $setting.=$i.":{ sorter: false }, ";
        }
      
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->lib->column as $key => $val) { 
          if ($i>0) $th.="<th>$val</th>";
          $i++;
        }        
        $columns['th'] = $th;    
        
        $columns['numb'] = count($this->lib->column);
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arrayResurs= $this->user->getRoles();
            
        
        $dopParam='';
        $addAct='/admin/addRoles/'.$dopParam;
        $updateAct='/admin/updateRoles/'.$dopParam;
        $formShow='/admin/formShowRoles/'.$dopParam;
        $deleteAct='/admin/deleteRoles/'.$dopParam;
        
        $urlPage=$urlPage.$dopParam;
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
            
        $tbody['fheight']= "240";
        $tbody['fwidth']= "320";
        
         
        $tr="";
                       
        for ($j=0; $j<count($arrayResurs); $j++){
           
            $tr.='<tr>';
            
            $tr.='<td class="center"><input type="checkbox" value="'.$arrayResurs[$j]['id_roles'].'" name="list" class="checkbox"/></td>'; 
            $tr.='<td>'.$arrayResurs[$j]['name'].'</td>'; 
            $tr.='<td>'.$arrayResurs[$j]['code'].'</td>'; 
              
            
            $id_record=$arrayResurs[$j]['id_roles'];
            
            $del='<a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
                        <span class="ui-icon ui-icon-circle-close"></span>
                    </a> ';
            
            if ($arrayResurs[$j]['code']=="ADMIN") $del='';
            if ($arrayResurs[$j]['code']=="REDACTOR") $del='';
            
            
            $tr.='<td style="padding-left:50px;"> 
                    <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
                      <span class="ui-icon ui-icon-pencil"></span>
                    </a>                                      
                    '.$del.' 
                 </td>
                </tr>';
         
            
        }
       
       
       $tbody['tr']=$tr;
               
     return $tbody;
      
    }  
            
}

?>