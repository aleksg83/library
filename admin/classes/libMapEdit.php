<?php

class libMapEdit {   
    private $db;
    public $lib;
    public $libimg;
    private $lang;
    private $resurs;
    public $id_resurs;
    public $type;
    
    
    public function __construct(libEdit $lib, $dataId, $type, $lang) {
       $sql = new Sql();
       $this->db = $sql->connect();
       $this->lib = $lib;
       $this->lang = $lang;
       
       $this->resurs=$this->lib->getData($dataId);
       $this->id_resurs=$dataId;
       $this->type=$type;
       
    }
    
     public function getMap($id=null){        
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_mapping WHERE id_mapping=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_mapping WHERE id_data='{$this->id_resurs}' and type='{$this->type}' ORDER BY num_page");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }        
        return $result;
    }
    
    public function getFileName($num){ 
        $lenth = strlen($num);
        switch ($lenth) {
            case 1:
                $num="000".$num;
                break;
            case 2:
                $num="00".$num;
                break;
            case 3:
                $num="0".$num;
                break;
            default:
                break;
        }
        
        $num.=".jpg";
        return $num;  
    }
  
    public function add($data){
      $pos = strpos($data['num_page'], "-"); //определяем - передан диапазон или одна страница
       
      if ($pos === false) {  //одно число
        
        if ($this->type == "1") {
             $level = ($data['name'] == "")? "" : $data['level'];
            
             $sth = $this->db->prepare("INSERT INTO ".PREFIX."_mapping (id_data, num_page, real_page, name, level, type, file) VALUES (:id_data, :num_page, :real_page, :name, :level, :type, :file)");
             $sth->bindParam(":id_data", $this->id_resurs, PDO::PARAM_INT);
             $sth->bindParam(":num_page", intval($data['num_page']), PDO::PARAM_INT);
             $sth->bindParam(":real_page",$data['real_page'], PDO::PARAM_STR);
             $sth->bindParam(":name",$data['name'], PDO::PARAM_STR);
             $sth->bindParam(":level",$level, PDO::PARAM_STR);
             $sth->bindParam(":type",$data['type'], PDO::PARAM_INT);
             $sth->bindParam(":file",$this->getFileName($data['num_page']), PDO::PARAM_STR);
        } else { //Для краткого содержания диапазонов нет, значение обрабатываем только здесь
            
             $sth = $this->db->prepare("INSERT INTO ".PREFIX."_mapping (id_data, num_page, real_page, name, level, type,  file) VALUES (:id_data, :num_page, :real_page, :name, :level, :type, :file)");
             $sth->bindParam(":id_data", $this->id_resurs, PDO::PARAM_INT);
             $sth->bindParam(":num_page", intval($data['num_page']), PDO::PARAM_INT);
             $sth->bindParam(":real_page",$data['real_page'], PDO::PARAM_STR);
             $sth->bindParam(":name",$data['name'], PDO::PARAM_STR);
             $sth->bindParam(":level",$data['level'], PDO::PARAM_STR);
             $sth->bindParam(":type",$data['type'], PDO::PARAM_INT);
              $sth->bindParam(":file",$this->getFileName($data['num_page']), PDO::PARAM_STR);
        }     
        
        $sth->execute();
        $err = $sth->errorInfo();
         
      }else{    //передан диапазон чисел
         
          $startNum = intval(substr($data['num_page'], 0, $pos));
          $endNum = substr($data['num_page'], $pos+1);
          $startRealPage=intval($data['real_page']);
          
          if ($startRealPage==="") $startRealPage=$startNum;
         
          if ($endNum<$startNum){ //если диапазон передан неверно и последнее число меньше первого, то записываем только первую запись 
              $endNum=$startNum;
          }
        
          for ($i=$startNum; $i<($endNum+1); $i++) {
                  
                  if ($i == $startNum ) {
                      $name = $data['name'];
                      $level = ($data['name'] == "")? "" : $data['level'];
                  }else{
                      $name="";
                      $level="";
                  }    
                  
                  $sth = $this->db->prepare("INSERT INTO ".PREFIX."_mapping (id_data, num_page, real_page, name, level, type, file) VALUES (:id_data, :num_page, :real_page, :name, :level, :type, :file)");
                  $sth->bindParam(":id_data", $this->id_resurs, PDO::PARAM_INT);
                  $sth->bindParam(":num_page", $i, PDO::PARAM_INT);
                  $sth->bindParam(":real_page", intval($startRealPage), PDO::PARAM_STR);
                  $sth->bindParam(":name",$name, PDO::PARAM_STR);
                  $sth->bindParam(":level",$level, PDO::PARAM_STR);
                  $sth->bindParam(":type",$data['type'], PDO::PARAM_INT);
                  $sth->bindParam(":file",$this->getFileName($i), PDO::PARAM_STR);
        
                  $sth->execute();
                  if ($i == $startNum ) $err = $sth->errorInfo();
                  
                  $startRealPage++;
             
          }
   
      }
        return ($err[0] != '00000')?false:true;
    }
      
    public function update($data){
        
        if ($data['name']=="{none}") {
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_mapping SET num_page=:num_page, real_page=:real_page, file=:file WHERE id_mapping=:id_mapping");
            $sth->bindParam(":num_page",$data['num_page'], PDO::PARAM_STR);
            $sth->bindParam(":real_page",$data['real_page'], PDO::PARAM_STR);
            $sth->bindParam(":file",$data['file'], PDO::PARAM_STR);
        
            
        }else{
            
            $resurs=$this->getMap($data['id']);
            $resursAll=$this->getMap();
            $stopNext=false;
            
            for ($i=$resurs['num_page']; $i<count($resursAll); $i++){
                
                if ( !$stopNext && $resursAll[$i]['name']!=""){
                    if ($resursAll[$i]['level'] > $resurs['level']){
                        $levelNew = $resursAll[$i]['level'] - 1;
                        $sth = $this->db->prepare("UPDATE ".PREFIX."_mapping SET level=:level WHERE id_mapping=:id_mapping");
                        
                        $sth->bindParam(":level",$levelNew, PDO::PARAM_STR);
                        $sth->bindParam(":id_mapping", $resursAll[$i]['id_mapping'], PDO::PARAM_INT);
                        $sth->execute(); 
                        
                    }else{
                       $stopNext=true; 
                    }
                        
                }
            }
           
            if ($data['name'] == "") $data['name'] = "-";
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_mapping SET name=:name, num_page=:num_page, real_page=:real_page, level=:level, file=:file WHERE id_mapping=:id_mapping");
            $sth->bindParam(":name",$data['name'], PDO::PARAM_STR);
            $sth->bindParam(":num_page",$data['num_page'], PDO::PARAM_STR);
            $sth->bindParam(":real_page",$data['real_page'], PDO::PARAM_STR);
            $sth->bindParam(":level",$data['level'], PDO::PARAM_STR);
            $sth->bindParam(":file",$data['file'], PDO::PARAM_STR);
        }
          $sth->bindParam(":id_mapping", $data['id'], PDO::PARAM_INT);
          $sth->execute(); 
          $err = $sth->errorInfo();
       
         return ($err[0] != '00000')?false:true;
           
    }
    
    public function delete($id){
        $arrayDel = explode(",", $id);
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_mapping WHERE id_mapping IN ($place_holders)");
        $sth->execute($arrayDel);
        
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
                
    }
    
    #-----------------------------------------------------------------------------------------------
    //form
    
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     */
    public function showForm($id=null, $act, $action, $url){
       
        $result = $this->getMap($id);
        $arrayMap = $this->getMap();
        
        $level = $result['level'];
        $levelUp = $level-1;
        $num_page = $result['num_page'];
        
        if ($result['name'] != ""){
          //$id_top= $result['id_menu_top'];
           $id_top="0";
           $stopUp=($level>1) ? false : true;
           
           for ($n=$num_page; $n>0; $n--){
                if (!$stopUp){
                    
                    if ( ($arrayMap[$n]['name'] != "") && ($arrayMap[$n]['level']==$levelUp) ) {
                        $id_top = $arrayMap[$n]['id_mapping'];
                        $stopUp=true;
                    }
                }
           }
                       
        }
        
        $options = array();
               
        $options[0]=$this->lang['root_node'];
        $parametr[]="";
        $levels[]=" level=\"0\"";
        for($j=0; $j<count($arrayMap); $j++) {
            $value="";
            $keigen="";
            $flag=false;            
           if ( $arrayMap[$j]['name'] != "") { 
                if ($arrayMap[$j]['id_mapping'] != $id){            
               
                    foreach($arrayMap[$j]  as $key => $val ){
                        if ($key=="id_mapping") $keigen = $val;                     
                        if ($key=="name") $value = $val; 
                        if ($key=="level") $level = $val; 
                    }                    
                    $tire = ($level>0)?"-":"";  
                    $options[$keigen]=$tire.$value; 
                    $parametr[]=' style="padding-left:'.($level*10).'px;"';
                    $levels[]=" level=\"$level\"";
                }
           }      
         }
        
         if ($act=="add") {
             $num = count($arrayMap)-1;
             $result['num_page'] = $arrayMap[$num]['num_page'] + 1;
             $result['file'] = $this->getFileName($result['num_page']);
             $result['level']=1;
         }  
         
       
         $size=41; 
         if ($this->type == "1") {
           if ( ( ($act == "edit") && ($result['name']!="") ) || ($act== "add")  ){
                $name =      new field_text("name", $this->lang['form_thead_map_name'], false, $result['name'], "", $size);   
           }else{
                $name =      new field_hidden_int("name", false, "{none}");  
           }
            
            $numPageForm = ($act== "add") ? $this->lang['form_thead_map_number_viewer_add'] : $this->lang['form_thead_map_number_viewer'];
            $realPageForm = ($act== "add") ? $this->lang['form_thead_map_number_real_add'] : $this->lang['form_thead_map_number_real'];
          
            $num_page =      new field_text("num_page",  $numPageForm, true, $result['num_page'], "", $size);   
            $real_page =     new field_text("real_page", $realPageForm, true, $result['real_page'], "", $size);   
            
            $file =          new field_text("file", $this->lang['form_thead_map_file'], true, $result['file'], "", $size);   
            if ($act== "add")  {
                $file =    new field_hidden_int("file", false, "_");
            }    
            
         } else {
           
           if ( ( ($act == "edit") && ($result['name']!="") ) || ($act== "add")  ){
                $name =      new field_text("name", $this->lang['form_thead_map_name'], false, $result['name'], "", $size);   
           }else{
                $name =      new field_hidden_int("name", false, "{none}");  
           }
            //$name =          new field_text("name", $this->lang['form_thead_map_name'], true, $result['name'], "", $size);      
           
            
            $numPageForm = ($act== "add") ? $this->lang['form_thead_map_number_viewer_add'] : $this->lang['form_thead_map_number_viewer'];
            $realPageForm = ($act== "add") ? $this->lang['form_thead_map_number_real_add'] : $this->lang['form_thead_map_number_real'];
          
            $num_page =      new field_text("num_page",  $numPageForm, true, $result['num_page'], "", $size);   
            $real_page =     new field_text("real_page", $realPageForm, true, $result['real_page'], "", $size);   
            
            $file =          new field_text("file", $this->lang['form_thead_map_file'], true, $result['file'], "", $size);   
            if ($act== "add")  {
                $file =    new field_hidden_int("file", false, "_");
            }  
            
            //$num_page =      new field_hidden_int("num_page", false, ""); 
            //$real_page =     new field_text("real_page", $this->lang['form_thead_map_number_real'], true, $result['real_page'], "", $size);   
            
            //$real_page =     new field_hidden_int("real_page", false, "");  
            //$file =          new field_hidden_int("file", false, "");  
         }
         
         $class='level sm0'; //класс списка
         
         if ( ( ($act == "edit") && ($result['name']!="") ) || ($act== "add")  ){
             $root =         new field_select( "top", $this->lang['root'], $options, $id_top, false, "", "level", $parametr, $levels, $class); 
         }else{
             $root =         new field_hidden_int("top", false, "");
         }
         
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
         $type =          new field_hidden_int("type", false, $this->type);
         $level =         new field_hidden_int("level", false, $result['level']);
         
        $form = new form(array(
                               "name" => $name,
                               "num_page" => $num_page,
                               "real_page" => $real_page,
                               "file" => $file,
                               "top" => $root,
                               "id" => $id_rec,
                               "url" => $url,
                               "type" => $type,
                               "level" => $level 
                                ),
                             "",
                             $action);        
        
        return  $form->print_form();
        
      
     }
     //form
     #---------------------------------------------------------------------------------------------
    
     
}

?>