<?php
class ConstructorPrint extends ConstructorElements {
    
    private $print;
    private $lang;
  
    public function __construct( predPrint $print, $lang) {
        $this->print = $print;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        
            return  '$("#validateForm").validate({
                        rules: {
                             comment: {
                                    required: true,
                             
                             },
                            
                        },
                        messages: {
                             comment: "'.$this->lang['requiredField'].'",
                                   
                        }
                      });';
       
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['predprint'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
      
          $this->lib->column = Array(
             'id' => "",
             'name' => $this->lang['predprint_name'],
             'type' => $this->lang['predprint_type'],  
             'ID_name' => "ID", 
          );
    
        $columns = Array();  
        
        for($i=0; $i<=count($this->lib->column); $i++){
           if ($i==count($this->lib->column)-1) $setting.=$i.":{ sorter: false }, ";
        }
        
       // $setting.="4:{ sorter: 'checkbox' },";
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->lib->column as $key => $val) { 
         if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
           else $class='';   
          if ($i>0) $th.="<th$class>$val</th>";
          $i++;
        }        
        $columns['th'] = $th;    
        
        $columns['numb'] = count($this->lib->column);
        $columns['hidden_check_var2'] = true;
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
        
        //Common::pre($this->print->getPrintResurs());
        
        $tbody = Array();
        $arrayResurs= $this->print->getPrintResurs();
        $nRec=1;//по умолчанию просматриватеся только свои ресурсы предпринта, если это администратор, то ему доступны и другие ресурсы 
        if (in_array("ADMIN", $_SESSION['role'])) {
               $admin=true;     
               $tbody['text-div']='<div class="divInfo">'.$this->lang['predprint_all_resurs'].' <b>'.$this->print->getCountPredPrintAdmin().'</b></div>';
               $arrayResursAdmin= $this->print->getPrintResursAdmin();
               $nRec=2;
               $tbody['view']="view";
        }
        
        
        $dopParam='';
        $updateAct='/admin/updatePredPrint/'.$dopParam;
        $formShow='/admin/formShowPredPrint/'.$dopParam;
        $deleteAct='/admin/removePredPrintAdmin/'.$dopParam;
        $formShowAdmin='/admin/formShowPredPrintAdmin/'.$dopParam;
        
        $urlPage=$urlPage.$dopParam;
             
            
        //$tbody['fheight']= "430";
        $tbody['fwidth']= "620";
        
         
        $tr="";
        //----------------------------------------------------------------------
     for ($n=1; $n<($nRec+1); $n++){
        if ( ($n==1) & $admin){
            $tr.="<tr><td><b>".$this->lang['predprint_this_user']."</b></td><td></td><td></td><td></td></tr>";
            if (count($arrayResurs)==0) $tr.="<tr><td>".$this->lang['noRecord']."</td><td></td><td></td><td></td></tr>";
        }
        
        if ($n==2){  
            $arrayResurs=$arrayResursAdmin;
            $tr.="<tr><td><b>".$this->lang['predprint_other_user']."</b></td><td></td><td></td><td></td></tr>";
            if (count($arrayResurs)==0) $tr.="<tr><td>".$this->lang['noRecord']."</td><td></td><td></td><td></td></tr>";
        }    
        for ($j=0; $j<count($arrayResurs); $j++){
            
            $id_record=$arrayResurs[$j]['id_rel_users_pred_print'];
            $id_news=$arrayResurs[$j]['id_news'];
            $id_data=$arrayResurs[$j]['id_data'];
            
            //--------------------------------------
            if ($n==2) {
                $id_user=$arrayResurs[$j]['id_users'];
                $login= Common::getLoginUser($id_user);
                $dopInfo=' ('.$this->lang['predprint_other_user_obrabotka'].' '.$login.'(id='.$id_user.'))';
                $redAction='';
            }else{
                $dopInfo='';
                $redAction=' 
                    <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
                     <span class="ui-icon ui-icon-pencil"></span>
                    </a>';
            }
            //---------------------------------------
             $action='
                    <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['print_comment_redactor'].'" href="javascript:void(0);" action="#" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShowAdmin.'">
                     <span class="ui-icon ui-icon-comment"></span>
                    </a>
                    
                    <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action removeprint" title="'.$this->lang['predprint_remove'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
                     <span class="ui-icon ui-icon-trash"></span>
                    </a>';
             $adminAction= ($admin) ? $action : "" ;
             $stylePadding = ($admin) ? 'style="padding-left:40px;"' : 'style="padding-left:60px;"' ;
            //---------------------------------------
            
            if ($id_news == "0") {
               
                $typeResurs=$this->lang['predprint_data'];
                $data = new libEdit("book"); //по умолчанию - книга, далее определим истинный тип ресурса
                $resursData = $data->getData($id_data);
                $codeResurs = $data->getCodeType($resursData['id_sp_type_data']);
                switch ($codeResurs){
                    case "book":
                            $typeResurs.=" - ".$this->lang['lib_book'];
                        break;
                     case "article":
                            $typeResurs.=" - ".$this->lang['lib_article'];
                        break;
                    case "thesis":
                            $typeResurs.=" - ".$this->lang['lib_thesis'];
                        break;
                     case "filmstrip":
                            $typeResurs.=" - ".$this->lang['lib_filmstrip'];
                        break;
                    
                }
                $name=$resursData['name'];
                $id=$id_data;
                //Common::pre($resursData);
                
            } else if ($id_data == "0") { 
                $typeResurs=$this->lang['predprint_news'];
                $data = new newsEdit(); //по умолчанию - книга, далее определим истинный тип ресурса
                $resursData = $data->getNews($id_news);
                $name=$resursData['zagolovok'];
                $id=$id_news;
               
            }           
           
            $tr.='<tr>';
            
           // $tr.='<td class="center"><input type="checkbox" value="'.$id_record.'" name="list" class="checkbox" '.$disable.'/></td>'; 
            
            
            $tr.='<td class="cnt">'.$name.''.$dopInfo.'</td>'; 
            $tr.='<td class="cnt">'.$typeResurs.'</td>'; 
            $tr.='<td class="cnt">'.$id.'</td>'; 
           
           $tr.='<td '.$stylePadding.'> 
                   '.$redAction.'
                   '.$adminAction.'    
                 </td>
                </tr>';
         
            
        }
     
     }     
               
       $tbody['tr']=$tr;
               
     return $tbody;
      
    }  
            
}

?>