<?php
class ConstructorFeedbackForm extends ConstructorElements {
    
    private $feed;
    private $lang;
        
    public function __construct(feedbackEdit $feed, $lang) {
        $this->feed = $feed;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['server_feedback'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['server_feedback_act'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->column = Array (
            "id" => "",
            'name' => $this->lang['server_feedback_name_user'],
            'email' => $this->lang['form_thead_user_send_email'],
            'date_add' => $this->lang['form_thead_date_add'],
            'status' => $this->lang['server_feedback_status_mess'],
            'id_users' => $this->lang['server_feedback_redactor'] );
        
        $columns = Array();                
        for($i=0; $i<=count($this->column); $i++){
           if ( ($i==0) || ($i=count($this->column))) $setting.=$i.":{ sorter: false }, ";
        }        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->column as $key => $val) { 
            
             if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
             else $class='';
             
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        
        return  $columns;
        
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $array = $this->feed->getMessage();

        $updateAct='/admin/updateFeedback/'.$dopParam;
        $formShow='/admin/formShowFeedback/'.$dopParam;
        $deleteAct='/admin/deleteFeedback/'.$dopParam;
        
        $panel = new servicePanel();
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        $tbody['right_but'] = $panel->getRightButton($this->lang['server_feedback_block_email'], '/admin/showBanEmail/', "ui-icon-locked");
        $tbody['fheight']= "500";
        $tbody['fwidth']= "480";
        
        
        $tr="";
        for ($j=0; $j<count($array); $j++){
            
         unset($array[$j]['message']);
        
        if ($array[$j]['id_users']!="") { 
                $user = new userEdit();
                $authorInfo = $user->getUser($array[$j]['id_users']);
                $author=$authorInfo['login'];
        }else{
            $author="";
        }         
         
         
          $i=0; 
            $tr.='<tr>';
                foreach($array[$j] as $key => $val ){
                   if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                    }elseif($key === "id_users"){      
                       $tr.="<td class='cnt'>$author</td>"; 
                    }elseif($key === "status"){
                       $str=""; 
                       if ($val=="1") $str=$this->lang['server_feedback_status_s1'];
                       if ($val=="2") $str=$this->lang['server_feedback_status_s2'];
                       $tr.="<td class='cnt'>$str</td>";    
                    }else if ($key=="date_add"){
                       $tr.="<td>".Common::getTimeConversion($array[$j]['date_add'], $this->lang)."</td>"; 
                       
                    }else{                    
                        $tr.="<td>$val</td>";                                          
                    }                                                    
                    $i++; 
                }            
            $tr.='<td style="padding-left: 40px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        }  
        $tbody['tr']=$tr;
       
     return $tbody;
    }   
}

?>