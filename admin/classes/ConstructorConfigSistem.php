<?php
class ConstructorConfigSistem extends ConstructorElements {
    
    private $setting, $lang, $column;
        
    public function __construct( settingSystemAndUserEdit $setting, $lang) {
        $this->setting = $setting;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            key: "required",
                            value: "required",
                        },
                        messages: {
                            value: "'.$this->lang['requiredField'].'",
                            key: "'.$this->lang['requiredField'].'",    
                        }
                      });';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['setting_sistem'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['setting_sistem'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->column = Array(
            "id" => "",
            "type" => $this->lang['setting_type_date'],
            "name" => $this->lang['setting_type_name'],
            "value" => $this->lang['setting_type_value'], 
        );
        
        $columns = Array();                
        /*
        for($i=0; $i<=count($this->column)+1; $i++){
           if ( ($i==0) || ($i==count($this->column)) ) $setting.=$i.":{ sorter: false }, ";
        }
        */    
        //$setting.="5:{ sorter: 'checkbox' }, 6:{ sorter: 'checkbox' }, ";
        $columns['setting'] = "";//$setting;        
        $th="";
        $i=0;
        
        foreach($this->column as $key => $val) { 
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
         
        $columns['hidden_check_var2'] = true;
        $columns['hidden_action'] = true;
        //$columns['numb'] = count($this->column)+1;
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $array = $this->setting->showSettingValue();
        //Common::pre($array);
       
        for ($j=0; $j<count($array); $j++){ 
            
             $i=0; 
            $tr.='<tr>';
            
            foreach($array[$j] as $key => $val ){
                
                 if ($i==0){
                        $id_record=$val;  
                       // $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                       // $tr.='<td class="center">'.$val.'</td>'; 
                    
                 }else if ($key=="id_sp_type_data"){
                     if ($val!=""){  
                        $code=$this->setting->getCodeType($val);
                        if ( ($code=="book") && ($array[$j]['name']=="isPredPrint") ){
                               $tr.='<td class="center">'.$this->lang['setting_type_lib'].'</td>';
                        }else{
                               $tr.='<td class="center">'.$this->lang['lib_'.$code].'</td>';
                        }   
                     }else{
                          $tr.='<td class="center">'.$this->lang['setting_all_system'].'</td>';      
                     }
                 
                 }else if ($key=="name"){
                     //$tr.="<td class=\"cnt\">$val</td>"; 
                     
                      if (($val=="isPredPrint")){
                     
                           if ($array[$j]['value'] == 1) {
                                  $checked = 'checked="cheked"';
                           } else {
                                  $checked="";
                           }
                          $tr.="<td>".$this->lang['setting_code_'.$val]." ($val)</td>";  
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$array[$j]['value'].'" code="'.$key.'" class="settingSetCheck" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setSettingValue/"/></td>';                      
                      }else{  
                          $tr.="<td>".$this->lang['setting_code_'.$val]." ($val)</td>"; 
                          $tr.='<td class="cnt"><input type="text" value="'.$array[$j]['value'].'" code="'.$key.'"  class="settingSetInp" id_record="'.$id_record.'" action="/adminajax/setSettingValue/" size="16"/></td>';                                          
                      }    
                      
                 }                                                    
                 $i++; 
                   
                    
           }            
           $tr.='</tr>';  
       }
            
       $tbody['tr']=$tr;
       
    return $tbody;
    }   
}

?>