<?php
class ConstructorMapping extends ConstructorElements {
    
    private $map;
    private $lang;
  
    public function __construct( libMapEdit $map, $lang) {
        $this->map = $map;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        
        if ($this->map->type == "1"){
            return  '$("#validateForm").validate({
                        rules: {
                             num_page: "required",
                             real_page: "required",
                             file: "required",
                        },
                        messages: {
                           num_page: "'.$this->lang['requiredField'].'",
                           real_page: "'.$this->lang['requiredField'].'",    
                           file: "'.$this->lang['requiredField'].'",    
                        }
                      });';
        
        } else {
            
            return  '$("#validateForm").validate({
                        rules: {
                             name: "required",
                             real_page: "required",
                             file: "required",
                        },
                        messages: {
                           name: "'.$this->lang['requiredField'].'",
                           real_page: "'.$this->lang['requiredField'].'", 
                           file: "'.$this->lang['requiredField'].'",    
                        }
                      });';
        
            
        }     
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        
        $breadcrumbs['name']= $this->lang['data_in_mapping_'.$this->map->type.''];
        $path =  '<a href="/admin/showLib/'.$this->map->lib->type.'/" title="'.$this->lang['lib_'.$this->map->lib->type.''].'">'.$this->lang['lib_'.$this->map->lib->type.''].'</a>';   
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
       
      if ($this->map->type == "1") {  
        
          $this->lib->column = Array(
             'id' => "",
             'name' => $this->lang['form_thead_map_name'],
             'number_page' => $this->lang['form_thead_map_number_viewer'],
             'real_page' => $this->lang['form_thead_map_number_real'],
             'file' => $this->lang['form_thead_map_file'],
          );
      }else{
            $this->lib->column = Array(
             'id' => "",
             'name' => $this->lang['form_thead_map_name'],
             'number_page' => $this->lang['form_thead_map_number_viewer'],
             'real_page' => $this->lang['form_thead_map_number_real'], 
             'file' => $this->lang['form_thead_map_file'],   
            );
      }   
               
        $columns = Array();  
        
        for($i=0; $i<=count($this->lib->column); $i++){
           if ( ($i==0) || ($i=count($this->lib->column))) $setting.=$i.":{ sorter: false }, ";
        }
      
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->lib->column as $key => $val) { 
          if ($i>0) $th.="<th>$val</th>";
          $i++;
        }        
        $columns['th'] = $th;    
        
        $columns['numb'] = count($this->lib->column);
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arrayMap= $this->map->getMap();
            
        
        $dopParam='?data='.$_GET['data'].'&name='.urlencode($_GET['name']).'&type='.$_GET['type'];
        $addAct='/admin/addLibMap/'.$this->map->lib->type.'/'.$dopParam;
        $updateAct='/admin/updateLibMap/'.$this->map->lib->type.'/'.$dopParam;
        $formShow='/admin/formShowLibMap/'.$this->map->lib->type.'/'.$dopParam;
        $deleteAct='/admin/deleteLibMap/'.$this->map->lib->type.'/'.$dopParam;
        
        $urlPage=  $urlPage.$dopParam;
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        if ($this->map->type == "1"){      
             $tbody['fheight']= "400";
             $tbody['fwidth']= "320";
        } else {
            
            $tbody['fheight']= "400";
            $tbody['fwidth']= "320";
            
        }    
        
        /**images**/
        #----------------------------------------------------------------------
        //if ($this->map->type == "1") {  
        $resurs=$this->map->lib->getData($this->map->id_resurs);
        $folders=$resurs['folder_name'].'/';
        $puthArray=$this->map->lib->getPuthResurs($folders);
        
        $puth=$puthArray['puth'];
        $puthFull=$puthArray['puthFull'];
        //}
        #----------------------------------------------------------------------
        
        
        
        $tr="";
                       
        for ($j=0; $j<count($arrayMap); $j++){
           
            $tr.='<tr>';
            
            #-------- link img ------
              $file = $puthFull."jpg/".$arrayMap[$j]['file'];
        
              if ( file_exists($file) && ($arrayMap[$j]['file']!="") /*&& ($this->map->type == "1")*/ ) {
                    
                   $imgLink=' <a class="btn_no_text btn ui-state-default ui-corner-all tooltips group" title="'.$this->lang['img_open'].'" href="'.$puth.'jpg/'.$arrayMap[$j]['file'].'">
                         <span class="ui-icon  ui-icon-extlink"></span>
                      </a>'; 
                   $style='style="padding-left: 30px;"';
              }else{
                  $imgLink='';
                  //if ($this->map->type == "1") {
                       $style='style="padding-left: 62px;"';
                   /*}else{
                        $style='style="padding-left: 50px;"';
                   } */   
              }     
            
            #-------- link img ------
            
            
            $tr.='<td class="center"><input type="checkbox" value="'.$arrayMap[$j]['id_mapping'].'" name="list" class="checkbox"/></td>'; 
            
            $level= $arrayMap[$j]['level'];
            $left=( ($level-1)*20)."px";
            if ($level>1) $class='class="tree"';  else $class="";
            $tr.="<td><p $class style=\"margin-left:$left;\">".$arrayMap[$j]['name']."</p></td>";
           
          //  $tr.='<td>'.$arrayMap[$j]['name'].'</td>'; 
            //if ($this->map->type == "1") {  
            $tr.='<td class="cnt">'.$arrayMap[$j]['num_page'].'</td>'; 
            $tr.='<td class="cnt">'.$arrayMap[$j]['real_page'].'</td>'; 
            $tr.='<td class="cnt">'.$arrayMap[$j]['file'].'</td>'; 
           // }
            
            $id_record=$arrayMap[$j]['id_mapping'];
            
            $tr.='<td '.$style.'> 
                    '.$imgLink.'
                    <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
                      <span class="ui-icon ui-icon-pencil"></span>
                    </a>                                      
                     <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
                        <span class="ui-icon ui-icon-circle-close"></span>
                    </a> 
                 </td>
                </tr>';
         
            
        }
        
       if (count($arrayMap)>0){  
       
         $tr.='<script type="text/javascript">
             $(function() { 
                  $(".group").colorbox({rel:\'group\', transition:"fade"}); 
             });
          </script>';
       }
       
        $tbody['tr']=$tr;
               
     return $tbody;
      
      
    }  
            
}

?>