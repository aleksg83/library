<?php
class ConstructorUserInRoles extends ConstructorElements {
    
    private $user;
    private $lang;
    private $idUsers, $login;
    private $db;
  
    public function __construct( userEdit $user, $idUsers, $login, $lang) {
        $this->user = $user;
        $this->lang = $lang;
        $this->idUsers= $idUsers;
        $this->login = $login;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        
            return  '';
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['user_in_roles_page'];
        $path =  '<a href="/admin/showUser/" title="'.$this->lang['user_page'].'">'.$this->lang['user_page'].'</a>';   
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
         
        $columns = Array();          
        for($i=0; $i<=2; $i++){
            $setting.=$i.":{ sorter: false }, ";
        }
        
        $columns['hidden_check_var2'] = true;
        $columns['hidden_action'] = true;
        
        $setting.="2:{ sorter: 'checkbox' }";
        
        
        $columns['setting'] = $setting;
              
        $th="<th>".$this->lang['user_roles_group']."</th>";
        $th.="<th>".$this->lang['user_roles_code']."</th>";
        $th.="<th>".$this->lang['connection']."</th>";

        $columns['th'] = $th;   
        
       //$columns['style']='style="width:600px;"';
        
        return  $columns;
        
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $info = $this->user->getUser($this->idUsers);
        $login = $info['login'];
        
        $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_rel_users_roles WHERE id_users=?");
        $sth->bindParam(1, $this->idUsers, PDO::PARAM_INT);
        $sth->execute();
        $collection=$sth->fetchAll(PDO::FETCH_ASSOC);
        
        $collectionIdRol=array();
        
        foreach($collection as $key => $val){
               array_push($collectionIdRol, $val['id_roles']);
        }
        
        
        $roles=$this->user->getRoles();
        
        for ($j=0; $j<count($roles); $j++){
            
            $id_record = $roles[$j]['id_roles'];
            
           $tr.='<tr>';
             
             $tr.="<td>".$roles[$j]['name']."</td>";
             $tr.="<td>".$roles[$j]['code']."</td>";
             
             if ( in_array($id_record, $collectionIdRol) ){
                  $val=1;
             } else {
                  $val=0;
             }
          
            if ($val == 1) {
              $checked = 'checked="cheked"';
             } else {
              $checked="";
             }     
             
             $disable='';
             if ( ($roles[$j]['code']=="ADMIN") && ($login == "admin")  ) $disable='disabled="disabled"';
             
            $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="is_rel_users_roles_'.$j.'" class="checkUserInRoles" '.$checked.' id_record="'.$id_record.'" id_users="'.$this->idUsers.'" action="/adminajax/setIsUserInRoles/" '.$disable.'/></td>                      
            </tr>'; 
            
            
        }
      
       $tbody['tr']=$tr;
               
     return $tbody;
      
    }  
            
}

?>