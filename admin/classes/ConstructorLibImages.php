<?php
class ConstructorLibImages extends ConstructorElements {
    
    private $lib;
    private $lang;
    private $id;


    public function __construct( libEdit $lib, $dataId, $lang) {
        $this->lib = $lib;
        $this->lang = $lang;
        $this->id = $dataId;
    }
    
    public function getValidateForm(){
        
            return  '$("#validateForm").validate({
                        rules: {
                             name: "required",
                        },
                        messages: {
                           name: "'.$this->lang['requiredField'].'",
                        }
                      });';
         
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['data_in_images'];
        $path =  '<a href="/admin/showLib/'.$this->lib->type.'/" title="'.$this->lang['lib_'.$this->lib->type.''].'">'.$this->lang['lib_'.$this->lib->type.''].'</a>';   
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->lib->column = Array(
             'id' => "",
             'file' => $this->lang['form_thead_images'],
             'puth' => $this->lang['form_thead_puthimage']
        );
               
        $columns = Array();  
        
        for($i=0; $i<=count($this->lib->column); $i++){
           if ( ($i==0) || ($i=count($this->lib->column))) $setting.=$i.":{ sorter: false }, ";
        }
      
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->lib->column as $key => $val) { 
          if ($i>0) $th.="<th>$val</th>";
          $i++;
        }        
        $columns['th'] = $th;    
        
        $columns['numb'] = count($this->menu->column);
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $resurs=$this->lib->getData($this->id);
        $folders=$resurs['folder_name']."/";
        $puthArray=$this->lib->getPuthResurs($folders);
        
        $puth=$puthArray['puth'];
        $puthFull=$puthArray['puthFull'];
        
        
        $dopParam='?data='.$_GET['data'].'&name='.urlencode($_GET['name']).'';
        $addAct='/admin/addLibImg/'.$this->lib->type.'/'.$dopParam;
        //$addActMass='/admin/addLibImgMass/'.$this->lib->type.'/'.$dopParam;
        
        $updateAct='/admin/updateLibImg/'.$this->lib->type.'/'.$dopParam;
        $formShow='/admin/formShowLibImg/'.$this->lib->type.'/'.$dopParam;
        $formShowMass='/admin/formShowLibImgMass/'.$this->lib->type.'/'.$dopParam;
        $deleteAct='/admin/deleteLibImg/'.$this->lib->type.'/'.$dopParam;
        
        $urlPage=$urlPage.$dopParam;
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['addMass'] = $panel->getAddMassButton($this->lang['addMass'], $urlPage, $formShowMass);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
              
        $tbody['fheight']= "450";
        $tbody['fwidth']= "400";
        
        /**multy upload file***/
        $tbody['text-div']='
         <div class="container-upload">
                <div id="fileUpload2" class="butload">You have a problem with your javascript</div>
		<!--<a href="javascript:$(\'#fileUpload2\').fileUploadStart()">Загрузить</a> |  <a href="javascript:$(\'#fileUpload2\').fileUploadClearQueue()">Удалить</a>-->
            <p></p>
        </div>    
        ';
        $tbody['folders']=$puth.'jpg/';
        $tbody['url-upload']=$urlPage."&upload=ok";
        /********/
        
        
        $diretoriya = $puthFull."jpg/";
        
        if (!file_exists($diretoriya)) {
            mkdir ($diretoriya, 0755);
        }
        
        $dir = opendir($diretoriya);
        chdir($diretoriya);
        
        $j=0;
        while( $d=readdir($dir) ){
            
            if (is_file($d)) {
              $tr.='<tr>';    
                
                $tr.='<td class="center"><input type="checkbox" value="'.$d.'" name="list" class="checkbox"/></td>'; 
                $tr.='<td><a href="'.$puth.'jpg/'.$d.'" class="group1">'.$d.'</a></td>';   
                $tr.='<td>'.$puth.'jpg/'.$d.'</td>';
               
               
                $tr.='<td style="padding-left: 40px;"> 
                     
                      <a class="btn_no_text btn ui-state-default ui-corner-all tooltips group" title="'.$this->lang['img_open'].'" href="'.$puth.'jpg/'.$d.'">
                         <span class="ui-icon  ui-icon-extlink"></span>
                      </a>      


                     <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$d.'" form="'.$formShow.'">
                         <span class="ui-icon ui-icon-pencil"></span>
                     </a>                                      
                    <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$d.'">
                      <span class="ui-icon ui-icon-circle-close"></span>
                    </a> 
                    

                     </td>
             </tr>';
              $j++;  
            }
            
        }
      
      if ($j==0) {
          $tr='<tr><td></td><td></td><td></td><td></td></tr>
               <tr><td colspan="4" class="cnt">'.$this->lang['lib_resurs_no_upload_file'].'</td></tr>';
      }  
        
     chdir(DROOT);
     
     $tr.='<script type="text/javascript">
             $(function() { 
                  $(".group").colorbox({rel:\'group\', transition:"fade"}); 
                  $(".group1").colorbox({rel:\'group1\', transition:"fade"}); 
             });
          </script>';
     $tbody['tr']=$tr;
               
     return $tbody;
      
    }  
            
}

?>