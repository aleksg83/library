<?php

class libImgEdit {   
    private $db;
    private $lib;
    private $lang;
    private $resurs;
    private $folders;
    private $puthArray, $puth, $puthFull;
    
    public function __construct(libEdit $lib, $dataId, $lang) {
       $sql = new Sql();
       $this->db = $sql->connect();
       $this->lib = $lib;
       $this->lang = $lang;
       
       
       $this->resurs=$this->lib->getData($dataId);
       $this->folders=$this->resurs['folder_name']."/";
       
       $this->puthArray=$this->lib->getPuthResurs($this->folders);
         
       $this->puth=$this->puthArray['puth'];
       $this->puthFull=$this->puthArray['puthFull'];
       
       
    }
    
  
    public function add($data){
       if ( Common::downloadFile( $this->puthFull.'jpg/' ) != "" ) return true;
       else return false;
    }
      
    public function update($data){
         if (!empty($_FILES['file']['tmp_name'])){
             unlink($this->puthFull.'jpg/'.$data['id']);
             if ( Common::downloadFile( $this->puthFull.'jpg/' ) != "" ) return true;
             else return false;
            
         } else {
             return false;
         }
       
    }
    
    public function delete($data){
        $arrayDel = explode(",", $data);
        $return=false;
        for ($i=0; $i<count($arrayDel); $i++){
            
           if (file_exists($this->puthFull.'jpg/'.$arrayDel[$i])) {
              
               if ( unlink($this->puthFull.'jpg/'.$arrayDel[$i]) ) $return=true;
               
           }    
        }
       
       return  $return;
        
    }
    
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - photo
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     */
    public function showForm($id, $act, $action, $url){
                    
        $paramImgFieldDefinition['file']="";
        
        if ($act=="edit") {
          
            if ( file_exists($this->puthFull.'jpg/'.$id) ) {
               
            $paramImgFieldDefinition['file']='<a href="'.$this->puth.'jpg/'.$id.'" class="group" target="_blank"><img src="'.$this->puth.'jpg/'.$id.'" class="photoPerson"/></a>';
                                              
            }     
         
            
        }
        
        
         $photo =         new field_file("file", $this->lang['person_photo'], false, "", "", $paramImgFieldDefinition);         
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
         
        $form = new form(array("file" => $photo,
                               "id" => $id_rec,
                               "url" => $url,                               
                                ),
                             "",
                             $action);        
        return  $form->print_form();
     }
     //form
     #---------------------------------------------------------------------------------------------
    
     
}

?>