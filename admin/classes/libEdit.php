<?
class libEdit {
    
    private $db, $idData;
    public $namePage, $column, $type, $id_type;
    
    public function __construct($type) {
        $this->type = $type;
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->id_type=$this->getIdType($this->type);
    }
    
    public function getData($id=null){        
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_data WHERE id_data=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_data WHERE id_sp_type_data='{$this->id_type}' ORDER BY date_add desc");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }        
        return $result;
    }
    
    /*************************************************************************************************/
    public function add($dataLib){
        
       $dataLib=$this->prepareInsertData($dataLib);  
       if ($dataLib['add_file']==true) {
              $file =" file_name, ";
              $sqlfile  =" :file_name, ";
       }else{
               $file = "";
               $sqlfile = "";
       }
       
        $this->db->beginTransaction();
       
       #------------------------------------------------------
       if ($this->type=='book'){
           
           $sth = $this->db->prepare("INSERT INTO ".PREFIX."_data (id_sp_type_data, short_author, name, short_name, is_dot, annotaciya, bo, year, id_sp_polnota, is_view_in_rubrik, is_view_author, is_view_zaglavie, is_search, is_add_year, id_users, url, {$file} folder_name, start_page, date_add, is_periodical, is_recomended, is_in_stock, title, description, keywords, materials) VALUES (:id_sp_type_data, :short_author, :name, :short_name, :is_dot, :annotaciya, :bo, :year, :id_sp_polnota, :is_view_in_rubrik, :is_view_author, :is_view_zaglavie, :is_search, :is_add_year, :id_users, :url, {$sqlfile} :folder_name, :start_page, :date_add, :is_periodical, :is_recomended, :is_in_stock, :title, :description, :keywords, :materials)");
           $sth->bindParam(":id_sp_type_data", $this->id_type, PDO::PARAM_INT);
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           $sth->bindParam(":is_dot",$dataLib['is_dot'], PDO::PARAM_INT);
           $sth->bindParam(":annotaciya",$dataLib['annotaciya'], PDO::PARAM_STR);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           $sth->bindParam(":folder_name", $dataLib['folder_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":is_periodical",$dataLib['is_periodical'], PDO::PARAM_INT);
           $sth->bindParam(":is_recomended",$dataLib['is_recomended'], PDO::PARAM_INT);
           $sth->bindParam(":is_in_stock",$dataLib['is_in_stock'], PDO::PARAM_INT);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":materials", $dataLib['materials'], PDO::PARAM_STR);
           
       }
       #------------------------------------------------------
       if ($this->type=='metabook'){
            
           $sth = $this->db->prepare("INSERT INTO ".PREFIX."_data (id_sp_type_data, name, is_view_in_rubrik, id_users, url, date_add, is_meta_book, title, description, keywords, materials) VALUES (:id_sp_type_data, :name, :is_view_in_rubrik, :id_users, :url, :date_add, :is_meta_book, :title, :description, :keywords, :materials)");
           $sth->bindParam(":id_sp_type_data", $this->id_type, PDO::PARAM_INT);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":is_meta_book", $dataLib['meta'], PDO::PARAM_INT);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":materials", $dataLib['materials'], PDO::PARAM_STR);
       }
       #------------------------------------------------------
       if ($this->type=='article'){
          
         
          if ($dataLib['article_type'][0] == 0) { //часть книги
              $artSourse=$dataLib['book'];
              $book=$this->getData($artSourse);
              $artText=$book['name'];
              $dataLib['add_file']=false;
             
          }    
          if ($dataLib['article_type'][0] == 1) { //самостоятельная книга
              $artSourse=0;
              $artText=$dataLib['book_text'];
              $folder="folder_name, ";
              $folder_sql=":folder_name, ";
          }    
           
           $sth = $this->db->prepare("INSERT INTO ".PREFIX."_data (id_sp_type_data, short_author, name, short_name, is_dot, annotaciya, bo, article_group_bo, year, id_sp_polnota, is_view_in_rubrik, is_view_author, is_view_zaglavie, is_search, is_add_year, id_users, url, {$file} {$folder} start_page, date_add, article_source, article_source_text, title, description, keywords, materials, id_next, id_prev) VALUES (:id_sp_type_data, :short_author, :name, :short_name, :is_dot, :annotaciya, :bo, :article_group_bo, :year, :id_sp_polnota, :is_view_in_rubrik, :is_view_author, :is_view_zaglavie, :is_search, :is_add_year, :id_users, :url, {$sqlfile} {$folder_sql} :start_page, :date_add, :article_source, :article_source_text, :title, :description, :keywords, :materials, :id_next, :id_prev)");
           $sth->bindParam(":id_sp_type_data", $this->id_type, PDO::PARAM_INT);
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           $sth->bindParam(":is_dot",$dataLib['is_dot'], PDO::PARAM_INT);
           $sth->bindParam(":annotaciya",$dataLib['annotaciya'], PDO::PARAM_STR);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":article_group_bo",$dataLib['article_group_bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           if ($dataLib['article_type'][0] == 1) $sth->bindParam(":folder_name", $dataLib['folder_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":article_source", $artSourse, PDO::PARAM_INT);
           $sth->bindParam(":article_source_text", $artText, PDO::PARAM_STR);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":materials", $dataLib['materials'], PDO::PARAM_STR);
           $sth->bindParam(":id_next", $dataLib['article_next'], PDO::PARAM_INT);
           $sth->bindParam(":id_prev", $dataLib['article_prev'], PDO::PARAM_INT);
       }
       #------------------------------------------------------
       if ($this->type=='filmstrip'){
        
           $sth = $this->db->prepare("INSERT INTO ".PREFIX."_data (id_sp_type_data, short_author, name, short_name, is_dot, annotaciya, bo, year, id_sp_polnota, is_view_in_rubrik, is_view_author, is_view_zaglavie, is_search, is_add_year, id_users, url, {$file} folder_name, start_page, date_add, title, description, keywords) VALUES (:id_sp_type_data, :short_author, :name, :short_name, :is_dot, :annotaciya, :bo, :year, :id_sp_polnota, :is_view_in_rubrik, :is_view_author, :is_view_zaglavie, :is_search, :is_add_year, :id_users, :url, {$sqlfile} :folder_name, :start_page, :date_add, :title, :description, :keywords)");
           $sth->bindParam(":id_sp_type_data", $this->id_type, PDO::PARAM_INT);
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           $sth->bindParam(":is_dot",$dataLib['is_dot'], PDO::PARAM_INT);
           $sth->bindParam(":annotaciya",$dataLib['annotaciya'], PDO::PARAM_STR);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           $sth->bindParam(":folder_name", $dataLib['folder_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           
       }
       #------------------------------------------------------
       if ($this->type=='thesis'){
           
           $vid_type = ($dataLib['thesis_type'][0]=="0")? $dataLib['vid_dis']: 3;
           $list_type = ($dataLib['thesis_type'][0]=="0")? 0: $dataLib['list_dis'];
      
           $sth = $this->db->prepare("INSERT INTO ".PREFIX."_data (id_sp_type_data, short_author, name, short_name, bo, year, id_sp_polnota, is_view_in_rubrik, is_view_author, is_view_zaglavie, is_search, is_add_year, id_users, url, {$file} folder_name, start_page, vid_dis, date_add, place_of_work, article_source, title, description, keywords) VALUES (:id_sp_type_data, :short_author, :name, :short_name, :bo, :year, :id_sp_polnota, :is_view_in_rubrik, :is_view_author, :is_view_zaglavie, :is_search, :is_add_year, :id_users, :url, {$sqlfile} :folder_name, :start_page, :vid_dis, :date_add, :place_of_work, :article_source, :title, :description, :keywords)");
           $sth->bindParam(":id_sp_type_data", $this->id_type, PDO::PARAM_INT);
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           //$sth->bindParam(":is_dot", "0", PDO::PARAM_INT);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           $sth->bindParam(":folder_name", $dataLib['folder_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":vid_dis", $vid_type, PDO::PARAM_INT);
           $sth->bindParam(":date_add", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":place_of_work", $dataLib['place_of_work'], PDO::PARAM_STR);
           $sth->bindParam(":article_source", $list_type, PDO::PARAM_INT);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           
       }
       #------------------------------------------------------ 
        
       $sth->execute(); 
           $this->idData = $this->db->lastInsertId();
           $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
           $err = $sth->errorInfo();
                     
           $this->error = ($err[0] != '00000')?0:1;
           
           $this->addLibDetails($dataLib); 
           
           if ($this->error == 1){
                  $this->db->commit();
                  
                  if ($this->type!='metabook'){                  
                    #-----------------
                    #predprint
                    if (Common::is_predprint()) Common::InsertPredPrint($this->idData, "data", $this->db);
                  }
                  
            return true;
            }else{
                $this->db->rollBack();
            return false;
           }        
    }
    
    /*************************************************************************************************/
    public function update($dataLib){
        $this->idData=$dataLib['id'];
        $dataLib=$this->prepareInsertData($dataLib, "edit"); 
       
       if ($dataLib['add_file']==true) {
              $file =" file_name=:file_name, ";
       }else{
              $file = "";
       }
        $this->db->beginTransaction();
        
       #------------------------------------------------------
       if ($this->type=='book'){
        
           $sth = $this->db->prepare("UPDATE ".PREFIX."_data SET short_author=:short_author, name=:name, short_name=:short_name, is_dot=:is_dot, annotaciya=:annotaciya, bo=:bo, year=:year, id_sp_polnota=:id_sp_polnota, is_view_in_rubrik=:is_view_in_rubrik, is_view_author=:is_view_author, is_view_zaglavie=:is_view_zaglavie, is_search=:is_search, is_add_year=:is_add_year, url=:url, {$file} start_page=:start_page, date_update=:date_update, is_periodical=:is_periodical, is_recomended=:is_recomended, is_in_stock=:is_in_stock, title=:title, description=:description, keywords=:keywords, materials=:materials WHERE id_data=:id_data");
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           $sth->bindParam(":is_dot",$dataLib['is_dot'], PDO::PARAM_INT);
           $sth->bindParam(":annotaciya",$dataLib['annotaciya'], PDO::PARAM_STR);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":date_update", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":is_periodical",$dataLib['is_periodical'], PDO::PARAM_INT);
           $sth->bindParam(":is_recomended",$dataLib['is_recomended'], PDO::PARAM_INT);
           $sth->bindParam(":is_in_stock",$dataLib['is_in_stock'], PDO::PARAM_INT);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":materials", $dataLib['materials'], PDO::PARAM_STR);
           $sth->bindParam(":id_data", $dataLib['id'], PDO::PARAM_INT);
       } 
       #------------------------------------------------------
       if ($this->type=='metabook'){
           $sth = $this->db->prepare("UPDATE ".PREFIX."_data SET  name=:name, is_view_in_rubrik=:is_view_in_rubrik, url=:url, date_update=:date_update, title=:title, description=:description, keywords=:keywords, materials=:materials WHERE id_data=:id_data");
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           $sth->bindParam(":date_update", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":materials", $dataLib['materials'], PDO::PARAM_STR);
           $sth->bindParam(":id_data", $dataLib['id'], PDO::PARAM_INT);
       }
       #------------------------------------------------------ 
       if ($this->type=='article'){
           
          if ($dataLib['article_type'][0] == 0) { //амостоятельная книга 
              $artSourse=0;
              $artText=$dataLib['book_text'];   
             
          }    
          if ($dataLib['article_type'][0] == 1) { //часть книги
              $artSourse=$dataLib['book'];
              $book=$this->getData($artSourse);
              $artText=$book['name'];
          }     
          
           $sth = $this->db->prepare("UPDATE ".PREFIX."_data SET short_author=:short_author, name=:name, short_name=:short_name, is_dot=:is_dot, annotaciya=:annotaciya, bo=:bo, article_group_bo=:article_group_bo, year=:year, id_sp_polnota=:id_sp_polnota, is_view_in_rubrik=:is_view_in_rubrik, is_view_author=:is_view_author, is_view_zaglavie=:is_view_zaglavie, is_search=:is_search, is_add_year=:is_add_year, url=:url, {$file} start_page=:start_page, date_update=:date_update, article_source=:article_source, article_source_text=:article_source_text, title=:title, description=:description, keywords=:keywords, materials=:materials, id_next=:id_next, id_prev=:id_prev  WHERE id_data=:id_data");
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           $sth->bindParam(":is_dot",$dataLib['is_dot'], PDO::PARAM_INT);
           $sth->bindParam(":annotaciya",$dataLib['annotaciya'], PDO::PARAM_STR);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":article_group_bo",$dataLib['article_group_bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":date_update", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":article_source", $artSourse, PDO::PARAM_INT);
           $sth->bindParam(":article_source_text", $artText, PDO::PARAM_STR);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":materials", $dataLib['materials'], PDO::PARAM_STR);
           $sth->bindParam(":id_next", $dataLib['article_next'], PDO::PARAM_INT);
           $sth->bindParam(":id_prev", $dataLib['article_prev'], PDO::PARAM_INT);
           $sth->bindParam(":id_data", $dataLib['id'], PDO::PARAM_INT);
       }
       #------------------------------------------------------
       if ($this->type=='filmstrip'){
           
            $sth = $this->db->prepare("UPDATE ".PREFIX."_data SET short_author=:short_author, name=:name, short_name=:short_name, is_dot=:is_dot, annotaciya=:annotaciya, bo=:bo, year=:year, id_sp_polnota=:id_sp_polnota, is_view_in_rubrik=:is_view_in_rubrik, is_view_author=:is_view_author, is_view_zaglavie=:is_view_zaglavie, is_search=:is_search, is_add_year=:is_add_year, url=:url, {$file} start_page=:start_page, date_update=:date_update, title=:title, description=:description, keywords=:keywords WHERE id_data=:id_data");
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           $sth->bindParam(":is_dot",$dataLib['is_dot'], PDO::PARAM_INT);
           $sth->bindParam(":annotaciya",$dataLib['annotaciya'], PDO::PARAM_STR);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":date_update", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":id_data", $dataLib['id'], PDO::PARAM_INT);
           
           
           
       }
       #------------------------------------------------------
       if ($this->type=='thesis'){
           
           $vid_type = ($dataLib['thesis_type'][0]=="0")? $dataLib['vid_dis']: 3;
           $list_type = ($dataLib['thesis_type'][0]=="0")? 0: $dataLib['list_dis'];
      
           $sth = $this->db->prepare("UPDATE ".PREFIX."_data SET short_author=:short_author, name=:name, short_name=:short_name, bo=:bo, year=:year, id_sp_polnota=:id_sp_polnota, is_view_in_rubrik=:is_view_in_rubrik, is_view_author=:is_view_author, is_view_zaglavie=:is_view_zaglavie, is_search=:is_search, is_add_year=:is_add_year, url=:url, {$file} start_page=:start_page, vid_dis=:vid_dis, date_update=:date_update, place_of_work=:place_of_work, article_source=:article_source, title=:title, description=:description, keywords=:keywords WHERE id_data=:id_data");
           $sth->bindParam(":short_author",$dataLib['short_author'], PDO::PARAM_STR);
           $sth->bindParam(":name",$dataLib['name'], PDO::PARAM_STR);
           $sth->bindParam(":short_name",$dataLib['short_name'], PDO::PARAM_STR);
           $sth->bindParam(":bo",$dataLib['bo'], PDO::PARAM_STR);
           $sth->bindParam(":year",$dataLib['year'], PDO::PARAM_STR);
           $sth->bindParam(":id_sp_polnota",$dataLib['id_sp_polnota'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_in_rubrik",$dataLib['is_view_in_rubrik'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_author",$dataLib['is_view_author'], PDO::PARAM_INT);
           $sth->bindParam(":is_view_zaglavie",$dataLib['is_view_zaglavie'], PDO::PARAM_INT);
           $sth->bindParam(":is_search",$dataLib['is_search'], PDO::PARAM_INT);
           $sth->bindParam(":is_add_year",$dataLib['is_add_year'], PDO::PARAM_INT);
           $sth->bindParam(":url", $dataLib['url_page'], PDO::PARAM_STR);
           if ($dataLib['add_file']) $sth->bindParam(":file_name", $dataLib['file_name'], PDO::PARAM_STR);
           $sth->bindParam(":start_page", $dataLib['start_page'], PDO::PARAM_INT);
           $sth->bindParam(":vid_dis", $vid_type, PDO::PARAM_INT);
           $sth->bindParam(":date_update", date("Y-m-d H:i:s"), PDO::PARAM_STR);
           $sth->bindParam(":place_of_work", $dataLib['place_of_work'], PDO::PARAM_STR);
           $sth->bindParam(":article_source", $list_type, PDO::PARAM_INT);
           $sth->bindParam(":title", $dataLib['title'], PDO::PARAM_STR);
           $sth->bindParam(":description", $dataLib['description'], PDO::PARAM_STR);
           $sth->bindParam(":keywords", $dataLib['keywords'], PDO::PARAM_STR);
           $sth->bindParam(":id_data", $dataLib['id'], PDO::PARAM_INT);
       }
       #------------------------------------------------------ 
       
       $sth->execute(); 
       $this->idData = $dataLib['id'];
       $err = $sth->errorInfo();
       
       $this->error = ($err[0] != '00000')?0:1;
           
       $this->addLibDetails($dataLib); 
           
       if ($this->error == 1){
           $this->db->commit();
           return true;
        }else{
           $this->db->rollBack();
           return false;
        }        
       
       
   }
        
   public function delete($id){
       $arrayDel = explode(",", $id);
            
        foreach($arrayDel as $key=>$val){
            $this->idData=$val;
            $this->clearRelAllData();
            $tempData=$this->getData($this->idData);
            $dirFiles=$tempData['folder_name']."/";
            $dir="";
            //-------------------------
            if ($this->type=="book"){
                $dir=BOOK.$dirFiles;
            }
            //-------------------------
            if ($this->type=="article"){
                $dir=ARTICLE.$dirFiles;
            }
            //-------------------------
            if ($this->type=="filmstrip"){
                $dir=FILMSTRIP.$dirFiles;
            }
            //-------------------------
            if ($this->type=="thesis"){
                $dir=THESIS_MATERIAL.$dirFiles;
            }
            
            
            if ( ($dir) && file_exists($dir) && ($this->type!="metabook") && ($tempData['folder_name']!="")){
                Common::rmdir_recursive($dir);
               // rmdir($dir);
            }
        } 
        
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_data WHERE id_data IN ($place_holders)");
        $sth->execute($arrayDel);
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
        
    }
    
    #----------------------------------------------------------------------------------------------
    public function getIdType($type){
        
        $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_type_data WHERE code=?");
        $sth->bindParam(1, $type, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return $result['id_sp_type_data'];
    }
    public function getCodeType($id){
        
        $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_sp_type_data WHERE id_sp_type_data=?");
        $sth->bindParam(1, $id, PDO::PARAM_STR);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return $result['code'];
    }
    #---------------------------------------------------------------------------------------------
     public function prepareInsertData($data, $type="add"){
          
            if (!isset($data['is_view_in_rubrik'])) $data['is_view_in_rubrik']=0;
            if (!isset($data['is_dot']))            $data['is_dot']=0;
            if (!isset($data['is_view_author']))    $data['is_view_author']=0; 
            if (!isset($data['is_view_zaglavie']))  $data['is_view_zaglavie']=0;
            if (!isset($data['is_search']))         $data['is_search']=0;
            if (!isset($data['is_add_year']))       $data['is_add_year']=0;
            if (!isset($data['is_periodical']))     $data['is_periodical']=0; 
            if (!isset($data['is_recomended']))     $data['is_recomended']=0; 
            if (!isset($data['is_in_stock']))       $data['is_in_stock']=0; 
          
          if ($type=="add") {
               $data['folder_name']=  Common::encodestring($data['short_author']."_".$data['name']."_".$data['year']); 
          }else{
               $data_resurs=$this->getData($data['id']);
               $data['folder_name']=$data_resurs['folder_name'];
          }     
       
           #-------------------------
           if ($this->type == "book"){
               $dir = BOOK.$data['folder_name'];
           }
           if ($this->type == "article"){
               $dir = ARTICLE.$data['folder_name'];
           }
           if ($this->type == "filmstrip"){
               $dir = FILMSTRIP.$data['folder_name'];
           }
           if ($this->type == "thesis"){
               $dir = THESIS_MATERIAL.$data['folder_name'];
           }
           
            if ( ($type=="add") && ($this->type!="metabook") ){ 
                if ( ($this->type != "article") || ( ($this->type == "article") && ($data['article_type'][0] == 1) ) ) {
               
                    if (file_exists($dir)){
                            mt_srand(time());
                            $r = mt_rand(0,1000);  //случайное число
                            $dir=$dir."_".$r; 
                            $data['folder_name']=$data['folder_name']."_".$r;
                    }
                    mkdir ($dir, 0755);
                } 
            }
           #-------------------------
         
         #-------------------------------------------------------------------------------------------   
         //File
            
         if ( ($type=="edit") && ( ($data['delFile']=="1") || (!empty($_FILES['file']['tmp_name'])) ) ){
             
             $data_resurs=$this->getData($data['id']);
             if ( ($data_resurs['file_name'] != "") && file_exists($dir."/".$data_resurs['file_name']) ) {
                     unlink($dir."/".$data_resurs['file_name']);
             }
             
         }
           
         if (!empty($_FILES['file']['tmp_name'])){
             Common::downloadFile($dir."/"); 
             $file_name=Common::encodestring($_FILES['file']['name'], true);
             
             $data['add_file']=true;
             $data['file_name']=$file_name;
         }
         #------------------------------------------------------------------------------------------- 
         $title_temp = ($this->type=="metabook") ? $data['name']: $data['short_author']." : ".$data['name'].", ".$data['year'];
         $data['title'] = ($data['title']=="")? $title_temp : $data['title']; 
        
        #------ url ------------------    
        $temp_url = ($this->type=="metabook") ? $data['name'] : $data['short_author']."_".$data['name']."_".$data['year'];
        $data['url_page'] = ($data['url_page']=="")? Common::createURL($temp_url) : $data['url_page'];
        
        #------------------------------------------------------------------------
        //уникальность url 
        if ($type=="add"){
            $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_data  WHERE url=? and id_sp_type_data=?");
            $sth->bindParam(1, $data['url_page'], PDO::PARAM_STR);
            $sth->bindParam(2, $this->id_type, PDO::PARAM_INT);
        }else if ($type=="edit"){    
             $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_data  WHERE id_data!=? and url=? and id_sp_type_data=?");
             $sth->bindParam(1, $data['id'], PDO::PARAM_STR);
             $sth->bindParam(2, $data['url_page'], PDO::PARAM_STR);
             $sth->bindParam(3, $this->id_type, PDO::PARAM_INT);
        }    
            $sth->execute();
            //$err = $sth->errorInfo();
            $count = $sth->rowCount();
           
            if ( $count > 0){
              $i=$count+1;   
              if (strrpos($data['url_page'], ".html")){
                  $length =  strlen($data['url_page'])-5;
                  $temp   =  substr($data['url_page'], 0, $length);
                  $data['url_page']=$temp."-".$i.".html";
                  
              }else{
                  $data['url_page']=$data['url_page']."-".$i;
              }
            }
         #-----------------------------------------------------------------------  
       
       $data['materials']=  Common::removingCharacter($data['materials']);     
            
        return $data;
    }
    #---------------------------------------------------------------------------------------------
    public function addLibDetails($data){
      $this->clearRelData();
      #------------------------------------------------------------------------- 
      $nCountPersonType=4;

      //book  
      if ($this->type == 'book') {
            
            #------
            #id-data - metabook, id_data_used - book
            if ( ($data['meta']!="") && ($data['meta']!="0") ){
                $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_data_data (id_data, id_data_used) VALUES (?, ?)");
                $sth->bindParam(1, $data['meta'], PDO::PARAM_INT);
                $sth->bindParam(2, $this->idData, PDO::PARAM_INT);
                $sth->execute();
                $err = $sth->errorInfo();
                $this->error = ($err[0] != '00000')?0:1;
            }
          
      }  
      #-------------------------------------------------------------------------
      //metabook
      if ($this->type == 'metabook') {
          
         foreach($data['book'] as $key=>$val){
                    
                    //надо удалить добавляемую книгу из всех других метакниг
                    $sth1 = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_data WHERE id_data_used=?");
                    $sth1->bindParam(1, $val, PDO::PARAM_INT);
                    $sth1->execute();       
             
                    //далле уже добавляем к этой метакниге
                    $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_data_data (id_data, id_data_used) VALUES (?, ?)");
                    $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
                    $sth->bindParam(2, $val, PDO::PARAM_INT);
                    $sth->execute();
                    $err = $sth->errorInfo();
                    $this->error = ($err[0] != '00000')?0:1;
                }
      } 
      #-------------------------------------------------------------------------
      if ($this->type == 'article') {
          
      } 
      #-------------------------------------------------------------------------
      if ($this->type == 'filmstrip') {
          
      } 
      #-------------------------------------------------------------------------
      if ($this->type == 'thesis') {
          $nCountPersonType=6;
      } 
      #-------------------------------------------------------------------------
      
      /*
      #------- авторы, редакторы, юбиляры --------------------
      if ($this->type != 'metabook') { 
         for($i=1; $i<$nCountPersonType; $i++){ 
             if ($i==1) $temp=$data['author'];
             if ($i==2) $temp=$data['redactor'];
             if ($i==3) $temp=$data['ubilyar'];
             if ($i==4) $temp=$data['director'];
             if ($i==5) $temp=$data['opponent'];
               
             if ( count($temp)>0 ) {
                if ( in_array("0", $temp) ){
                  unset($temp[0]);
                }
                foreach($temp as $key=>$val){
                    $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_data_person (id_data, id_person, id_sp_type_person) VALUES (?, ?, ?)");
                    $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
                    $sth->bindParam(2, $val, PDO::PARAM_INT);
                    $sth->bindParam(3, $i, PDO::PARAM_INT);
                    $sth->execute();
                    $err = $sth->errorInfo();
                    $this->error = ($err[0] != '00000')?0:1;
                }
               
            }
          }
      }   
      #----------------------------------------------------
      */  
    }
    #----------------------------------------------------------------------------------------------
    private function clearRelData(){
        
        for($i=0; $i<1; $i++){
           // if ($i==0) $table="person";
            if ($i==0) $table="data";
             
           if($table!="data") {
               $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_{$table} WHERE id_data=?");
               $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
            
           $sth->execute();
           } else {
               if ($this->type=="book"){
                $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_{$table} WHERE id_data_used=?");
                $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
                $sth->execute();
               }
               
               if ($this->type=="metabook"){
                $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_{$table} WHERE id_data=?");
                $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
                $sth->execute();
               }
           }
           
        } 
    }
    #----------------------------------------------------------------------------------------------
    private function clearRelAllData(){
        for($i=0; $i<3; $i++){
            if ($i==0) $table="person";
            if ($i==1) $table="data";
            if ($i==2) $table="menu";
            
            if($table!="data") {
                $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_{$table} WHERE id_data=?");
            }else {
               if ($this->type=="book"){
                $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_{$table} WHERE id_data_used=?");
               }
              
               if ($this->type=="metabook"){
                $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_data_{$table} WHERE id_data=?");
               }
           }
            
            $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
            $sth->execute();
        } 
        
        //-------------------------------------------------------------------
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_users_pred_print WHERE id_data=?");
        $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
        $sth->execute();
        //-------------------------------------------------------------------
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_mapping WHERE id_data=?");
        $sth->bindParam(1, $this->idData, PDO::PARAM_INT);
        $sth->execute();
    }
    
    public function getPuthResurs($folders){
        $puthReturn = Array();
        
         switch ($this->type) {
            case 'book':
                $puth=BOOK_BASE.$folders;
                $puthFull=BOOK.$folders;
                break;
            case 'article':
                $puth=ARTICLE_BASE.$folders;
                $puthFull=ARTICLE.$folders;
               break; 
           case 'filmstrip':
               $puth=FILMSTRIP_BASE.$folders;
               $puthFull=FILMSTRIP.$folders;
               break; 
           case 'thesis':
               $puth=THESIS_MATERIAL_BASE.$folders;
               $puthFull=THESIS_MATERIAL.$folders;
               break; 
        }
        
        $puthReturn['puth']=$puth;
        $puthReturn['puthFull']=$puthFull;
        return $puthReturn;
    }
    
    
    //showForm
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id=null, $act, $action, $url, $lang){
       
        $result = $this->getData($id);
        $result = Common::removeStipsSlashes($result);
        
        $flg = (($act=="add") or ($result['is_view_in_rubrik']=="1"))?true:false;   //is_view_in_rubrik
        $flg2 = ($result['is_dot']=="1")?true:false;                                //is_dot
        $flg3 = (($act=="add") or ($result['is_view_author']=="1"))?true:false;     //is_view_author
        $flg4 = ($result['is_view_zaglavie']=="1")?true:false;   //is_view_zaglavie
        $flg5 = (($act=="add") or ($result['is_search']=="1"))?true:false;          //is_search
        $flg6 = (($act=="add") or ($result['is_add_year']=="1"))?true:false;        //is_add_year
        $flg7 = ($result['is_periodical']=="1")?true:false;      //is_periodical
        $flg8 = ($result['is_recomended']=="1")?true:false;      //is_recomended
        $flg9 = ($result['is_in_stock']=="1")?true:false;                           //is_in_stock
        
        $options = array();
        $options[0]="---";
        $options_art[0]="---";
        $polnota = new spEdit('polnota');
        $pl = $polnota->select();
        for($j=0; $j<count($pl); $j++) {
            $value=$pl[$j]['name'];
            $keigen=$pl[$j]['id_sp_polnota'];
            $options[$keigen]=$value;
            if ($keigen!=2)$options_art[$keigen]=$value;
         }
        
        $result['start_page'] = ($result['start_page']=="")? 1: $result['start_page'];
        
        
        //получить список метакниг
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_data WHERE is_meta_book='1'");
        $metaList = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        $options_meta= array();
        $options_meta[0]="";
        $id_meta = "";
        for($j=0; $j<count($metaList); $j++) {
            $value=$metaList[$j]['name'];
            $key=$metaList[$j]['id_data'];
               if ($act=="edit") {
                 //связи с метакнигой
                 $sth = $this->db->query("
                        SELECT id_rel_data_data FROM ".PREFIX."_rel_data_data a
                                      WHERE id_data='".$key."' and  
                                            id_data_used='".$result['id_data']."' ");     
                         
                 $tempList = $sth->fetchAll(PDO::FETCH_ASSOC);
                 if ( count($tempList)>0 ) $id_meta=$key; 
                 //if ( in_array($value, $news['details']['sp_education']) ) array_push($id_ed, $key);
           }   
            $options_meta[$key]=$value;  
         }
         #--------------------------------------------------------------------------------------------------------
        //получить список книг
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_data WHERE id_sp_type_data='1'");
        $bookList = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        $options_book= array();
        $id_book = array();
        for($j=0; $j<count($bookList); $j++) {
            $value=$bookList[$j]['name'];
            $key=$bookList[$j]['id_data'];
               if ($act=="edit") {
                 //связь книги с метакнигой
                 $sth = $this->db->query("
                        SELECT id_rel_data_data FROM ".PREFIX."_rel_data_data 
                        WHERE  id_data_used='".$key."' and
                               id_data='".$result['id_data']."' ");
                 $tempList = $sth->fetchAll(PDO::FETCH_ASSOC);
                 
                 if ( count($tempList)>0 )  array_push($id_book, $key);
                 
                 //связь статьи с источником
                 if ($key==$result['article_source']) $id_book_art=$key;
                
                //if ( in_array($value, $news['details']['sp_education']) ) array_push($id_ed, $key);
           } 
          
            $options_book[$key]=$value;  
         }
         #--------------------------------------------------------------------------------------------------------
        //получить список статей для создания связи продолжения и предыдущих частей
        $sth = $this->db->query("SELECT * FROM ".PREFIX."_data WHERE id_sp_type_data='2'");
        $artList = $sth->fetchAll(PDO::FETCH_ASSOC);
        
        $options_artNext= array();
        $options_artNext[0]="---";
        $options_artPrev= array();
        $options_artPrev[0]="---";
        
        $id_art_next = "";
        $id_art_prev = "";
        for($j=0; $j<count($artList); $j++) {
            $value=$artList[$j]['name'];
            $key=$artList[$j]['id_data'];
               if ( ($act=="edit") && ($key!=$result['id_data']) ) {
                 if ( $result['id_next'] == $key )   $id_art_next=$key;
                 if ( $result['id_prev'] == $key )   $id_art_prev=$key;
               } 
          
            if ($key!=$result['id_data']) $options_artPrev[$key]=$value; 
            if ($key!=$result['id_data']) $options_artNext[$key]=$value;
         }
         #--------------------------------------------------------------------------------------------------------
         
         $nameFile= (strlen($result['file_name'])>=50)? substr($result['file_name'], 0, 50)."...":$result['file_name'];
         
         //----------------------------------------------------------------------------------------------------------
         if ($this->type=="book"){
             $urlResus=BOOK;
             $urlResusBase=BOOK_BASE;
         }
         if ($this->type=="article"){
             $urlResus=ARTICLE;
             $urlResusBase=ARTICLE_BASE;
         }
         if ($this->type=="filmstrip"){
             $urlResus=FILMSTRIP;
             $urlResusBase=FILMSTRIP_BASE;
         }
         if ($this->type=="thesis"){
             $urlResus=THESIS_MATERIAL;
             $urlResusBase=THESIS_MATERIAL_BASE;
         }
         //----------------------------------------------------------------------------------------------------------
         
         if ( ($result['file_name'] != "") && file_exists($urlResus.$result['folder_name']."/".$result['file_name']) ) {
            $paramFileFieldDefinition['file']='<a href="'.$urlResusBase.$result['folder_name']."/".$result['file_name'].'" target="_blank" class="tooltip" title="'.$result['file_name'].'">'.$nameFile.'</a>
                                               <span class="photoPerson fileDel">'.$lang['lib_delete_file'].' <input type="checkbox" name="delFile" value="0"/></span>';
         }else{
            $paramFileFieldDefinition['file']="";
         }
        
        /* 
        //---------------------------------------------------------------------
        //связанные авторы
        $id_author = array(); 
        $id_redactor = array(); 
        $id_ub = array(); 
        $id_director = array(); 
        $id_opponent = array(); 
        if ($act=="edit") { 
           for ($j=1; $j<6; $j++){
            
            $sth = $this->db->query("
                        SELECT id_person FROM ".PREFIX."_rel_data_person as rel
                                      inner join  ".PREFIX."_data as d
                                      WHERE d.id_data='".$result['id_data']."' and  
                                            rel.id_data=d.id_data and 
                                            rel.id_sp_type_person='{$j}'");
            $tempAuthor = $sth->fetchAll(PDO::FETCH_ASSOC);
        
            for ($i=0; $i<count($tempAuthor); $i++){ 
                 foreach($tempAuthor[$i] as $key=>$val){
                  if ($key=='id_person'){  
                     if ($j==1) array_push($id_author, $val);
                     else if ($j==2) array_push($id_redactor, $val);
                     else if ($j==3) array_push($id_ub, $val);
                     else if ($j==4) array_push($id_director, $val);
                     else if ($j==5) array_push($id_opponent, $val);
                  }
                }
            } 
          }//for  
        }   
        //---------------------------------------------------------------------
        
         $persona=new PersonEdit();
         $personList=$persona->getPersons();
         $options_aut = array();
         $options_aut[0] = "";
         for($j=0; $j<count($personList); $j++) {
            $value=$personList[$j]['fio'];
            $key=$personList[$j]['id_person'];
            $options_aut[$key]=$value;   
         }
         //---------------------------------------------------------------------
         */
         
         //---------------------------------------------------------------------
         $options_vid_dis = array();
         $options_vid_dis[0] = "";
         $options_vid_dis[1] = $lang['lib_thesis_type1'];
         $options_vid_dis[2] = $lang['lib_thesis_type2'];
         if ($act=="add") $parametrSelect1Art['css_class_label']="type0";
         //---------------------------------------------------------------------
        //получить список диссертаций для создания связи автореферата и диссертации
         $sth = $this->db->query("SELECT * FROM ".PREFIX."_data WHERE id_sp_type_data='3' and vid_dis!='3'");
         $disList = $sth->fetchAll(PDO::FETCH_ASSOC);
         $options_dis_materials = array();
         $options_dis_materials[0] = "";
         
        
         
        for($j=0; $j<count($disList); $j++) {
            $value=$disList[$j]['name'];
            $key=$disList[$j]['id_data'];
            $options_dis_materials[$key]=$value; 
           
        }
         
          if ($act=="add") $parametrSelect2Art['css_class_label']="type1";
         //---------------------------------------------------------------------
         
        $size=41; 
        $size2=123; 
        $class='sm0'; //класс списка
        $paramCheck['column']="columns-2";
        $paramCheck1="";
        $paramTextField['class']="creditor";
        $paramTextField['id']="creditor_id";
        $paramTextField['column']="columns-2"; //определяем поле в правую колонку формы, форма получиться двух-колоночная
        
        #--------------------------------------------
        $typeArticle= array();
        $typeArticle[0]=$lang['lib_article_chastj'];
        $typeArticle[1]=$lang['lib_article_izdannie'];
        $typeArtParametr = array();
        $typeArtParametr['class']="art_type";
        //$parametrTextArt['css_style']="display:none;";
        if ($act=="add") $parametrTextArt['css_class_label']="type1";
        if ($act=="add") $parametrSelectArt['css_class_label']="type0";
        #--------------------------------------------
        $typeThesis= array();
        $typeThesis[0]=$lang['lib_thesis_dis'];
        $typeThesis[1]=$lang['lib_thesis_referat'];
        $typeThesisParametr = array();
        $typeThesisParametr['class']="dis_type";
        //$parametrTextArt['css_style']="display:none;";
      //  if ($act=="add") $parametrTextArt['css_class_label']="type1";
       // if ($act=="add") $parametrSelectArt['css_class_label']="type0";

        #------------------------------------- поля ----------------------------------------------------------
        $short_author=      new field_text("short_author", $lang['lib_short_author'], true, $result['short_author'], "", $size);
        $name =             new field_text("name", $lang['lib_zaglavie'], true, $result['name'], "", $size);
        if ($this->type=='metabook') {
            $name =  new field_text("name", $lang['lib_metaname'], true, $result['name'], "", $size);      
        }    
        $short_name=        new field_text("short_name", $lang['lib_short_name'], false, $result['short_name'], "", $size);
        $year =             new field_text("year", $lang['lib_year'], false, $result['year'], "", $size);
        $id_sp_polnota =    new field_select("id_sp_polnota", $lang['lib_polnota'], $options, $result['id_sp_polnota'], false, "", "", "", "", $class); 
        $id_sp_polnota_art = new field_select("id_sp_polnota", $lang['lib_polnota'], $options_art, $result['id_sp_polnota'], false, "", "", "", "", $class); 
        $url_page=          new field_text("url_page", $lang['lib_url'], false, $result['url'], "", $size);
        $start_page=        new field_text("start_page", $lang['lib_start_page'], false, $result['start_page'], "", $size);
        $meta =             new field_select("meta", $lang['lib_meta'], $options_meta, $id_meta, false, "", "", "", "", $class); 
        $file =             new field_file("file", $lang['lib_file'], false, "", "", $paramFileFieldDefinition);         
        
        //$author =           new field_select("author", $lang['lib_author'], $options_aut, $id_author, true, "5", "", "", "", $class); 
          $author  =        new field_hidden_int("author", false, "");  
        //$redactor =         new field_select("redactor", $lang['lib_redactor'], $options_aut, $id_redactor, true, "5", "", "", "", $class); 
          $redactor  =      new field_hidden_int("redactor", false, ""); 
        //$director =         new field_select("director", $lang['lib_director'], $options_aut, $id_director, true, "5", "", "", "", $class); 
          $director  =      new field_hidden_int("director", false, ""); 
        //$opponent =         new field_select("opponent", $lang['lib_opponent'], $options_aut, $id_opponent, true, "5", "", "", "", $class); 
          $opponent  =      new field_hidden_int("opponentr", false, ""); 
        //$ubilyar =          new field_select("ubilyar", $lang['lib_ubilyar'], $options_aut, $id_ub, true, "5", "", "", "", $class); 
          $ubilyar  =       new field_hidden_int("ubilyar", false, ""); 
          
        $book =             new field_select("book", $lang['lib_book'], $options_book, $id_book, true, "26", "", "", "", $class); 
        $book_art =         new field_select("book", $lang['lib_book_art'], $options_book, $result['article_source'], false, "", $parametrSelectArt, "", "", $class); 
        $book_art_text=     new field_text("book_text", $lang['lib_book_art'], false, $result['article_source_text'], "", $size, $parametrTextArt);
        $articleNext =      new field_select("article_next", $lang['lib_article_next'], $options_artNext,  $id_art_next, false, "", "", "", "", $class); 
        $articlePrev =      new field_select("article_prev", $lang['lib_article_prev'], $options_artPrev,  $id_art_prev, false, "", "", "", "", $class); 
        #----------------------------------------------------------------------------------------------------------
        if ( ($act == "add") && ($this->type=="article") ) {
            $articleType =  new field_radio("article_type", $lang['lib_article_type'], $typeArticle,  "0", $typeArtParametr); 
            
            $paramFileFieldDefinition['css_class_label']="type1";
            $file =         new field_file("file", $lang['lib_file'], false, "", "", $paramFileFieldDefinition);         
        
            
        } else if ( ($act == "edit") && ($this->type=="article") ){
            $typeArt = ( ($result['article_source']=="") ||  $result['article_source']==0)? 0 : 1;
            $articleType = new field_hidden_int("article_type", false, $typeArt);
            
            if ($typeArt == 0){
                $book_art = new field_hidden_int("book", false, 0);
                
            } else {
                $book_art_text= new field_hidden_int("book_text", false, 0);
                $file = new field_hidden_int("file", false, "");
            }
        }    
        #------------------------------------------------ Диссертация ------------------------------------------------------
       
        
        if ( ($act == "add") && ($this->type=="thesis") ) {
           
            $thesis_type =  new field_radio("thesis_type", $lang['lib_dis_type'], $typeThesis,  "0", $typeThesisParametr); 
            
            $vid_dis     =  new field_select("vid_dis", $lang['lib_vid_dis'], $options_vid_dis, $result['vid_dis'], false, "", $parametrSelect1Art, "", "", $class); 
            $list_dis    =  new field_select("list_dis", $lang['lib_list_dis'], $options_dis_materials, $result['article_source'], false, "", $parametrSelect2Art, "", "", $class); 
             
        } else if ( ($act == "edit") && ($this->type=="thesis") ){
            
            $typedis = ( ($result['vid_dis']=="1") ||  $result['vid_dis']=="2")? 0 : 1;
            
           //0 - диссертация, 1- автореферат
            $thesis_type = new field_hidden_int("thesis_type", false, $typedis);
            
            if ($typedis == 0){
                $list_dis =     new field_hidden_int("list_dis", false, 0);
                $vid_dis  =     new field_select("vid_dis", $lang['lib_vid_dis'], $options_vid_dis, $result['vid_dis'], false, "", $parametrSelect1Art, "", "", $class); 
            }else{
                $vid_dis    =  new field_hidden_int("vid_dis", false, 3);
                $list_dis   =  new field_select("list_dis", $lang['lib_list_dis'], $options_dis_materials, $result['article_source'], false, "", $parametrSelect2Art, "", "", $class); 
           }
            
        }    
        
        #---------------------------------------------------------------------------------------------------------- 
        
        
        $annotaciya =       new field_textarea("annotaciya", $lang['lib_annotaciya'], false, $result['annotaciya'], 93, 4, "", "", "", $paramCheck); 
        $bo =               new field_textarea("bo", $lang['lib_bo'], false, $result['bo'], 93, 5, "", "", "", $paramCheck); 
        $article_group_bo =   new field_textarea("article_group_bo", $lang['lib_bo_group'], false, $result['article_group_bo'], 93, 5, "", "", "", $paramCheck); 
        $materials =        new field_textarea("materials", $lang['lib_materials'], false, $result['materials'], 38, 10, "", "", "", $paramTextField); 
        if ($this->type=='metabook') {
            if ($act=="add") $result['materials']=$lang['lib_meta_template'];
            $materials = new field_textarea("materials", $lang['lib_meta_materials'], false, $result['materials'], 38, 10, "", "", "", $paramTextField); 
        }
        
        //$place_of_work    = new field_textarea("place_of_work", $lang['lib_place_of_work'], false, $result['place_of_work'], 93, 4, "", "", "", $paramCheck); 
        $place_of_work    = new field_hidden_int("place_of_work", false, "");
        $is_visible       = new field_checkbox("is_view_in_rubrik", $lang['visible'], $flg, $paramCheck1);
        $is_dot           = new field_checkbox("is_dot", $lang['lib_is_dot'], $flg2, $paramCheck1);
        $is_view_author   = new field_checkbox("is_view_author", $lang['lib_is_view_author'], $flg3, $paramCheck1);
        $is_view_zaglavie = new field_checkbox("is_view_zaglavie", $lang['lib_is_view_zaglavie'], $flg4, $paramCheck1);
        $is_search        = new field_checkbox("is_search", $lang['lib_is_search'], $flg5, $paramCheck1);
        $is_add_year      = new field_checkbox("is_add_year", $lang['lib_is_add_year'], $flg6, $paramCheck1);
        $is_add_year_art  = new field_checkbox("is_add_year", $lang['lib_is_add_year'], $flg6, $paramCheck1);
        $is_periodical    = new field_checkbox("is_periodical", $lang['lib_is_periodical'], $flg7, $paramCheck1);
        $is_recomended    = new field_checkbox("is_recomended", $lang['lib_is_recomended'], $flg8, $paramCheck1);
        $is_in_stock      = new field_checkbox("is_in_stock", $lang['lib_is_stok'], $flg9, $paramCheck1);
        
        $title            = new field_text("title", $lang['title'], false, $result['title'], "", $size2, $paramCheck);         
        $description      = new field_textarea("description", $lang['meta_description'], false, $result['description'], 93, 2, "", "", "", $paramCheck); 
        $keywords         = new field_text("keywords", $lang['meta_keywords'], false, $result['keywords'], "", $size2, $paramCheck);         
         
        
        $type             = new field_hidden_int("type", false, $this->type);
        $id_type          = new field_hidden_int("id_type", false, $this->id_type);
        $id               = new field_hidden_int("id", false, $id);
        $url              = new field_hidden_int("url", false, $url);
        #------------------------------------- поля ----------------------------------------------------------
        
        
     //-------------------------------------------------------------------------------------------
     //form book   
     if ($this->type == 'book') { 
         
        $form = new form(array( "short_author" => $short_author,
                                "name"   => $name,
                                "short_name" => $short_name,
                                "year" => $year,
                                "id_sp_polnota" => $id_sp_polnota,
                                "url_page" => $url_page,
                                "start_page" => $start_page,
                                "meta" => $meta,  
                                "file" => $file,
                                "author" => $author,
                                "redactor" => $redactor,
                                "ubilyar" => $ubilyar,
                                "is_visible"   => $is_visible,
                                "is_dot" => $is_dot,
                                "is_view_author" => $is_view_author,
                                "is_view_zaglavie" => $is_view_zaglavie,
                                "is_search" => $is_search,
                                "is_add_year" => $is_add_year,
                                "is_periodical" => $is_periodical,
                                "is_recomended" => $is_recomended,
                                "is_in_stock" => $is_in_stock,
                               
                                "annotaciya" => $annotaciya,
                                "bo" => $bo, 
                                "materials" => $materials,
                                "title" => $title,
                                "description" => $description,
                                "keywords" => $keywords,
                                
            
                                "type" => $type,
                                "id_type" => $id_type,
                                "id" => $id,
                                "url" => $url,
                                ), 
                                "", 
                                $action);
     }
     //-------------------------------------------------------------------------------------------------
     //metabook
     else if ($this->type == 'metabook'){
         $meta = new field_hidden_int("meta", false, "1");
         $form = new form(array( 
                                "name"   => $name,
                                "url_page" => $url_page,
                                "book" => $book,             
                                "is_visible"   => $is_visible,
                                "materials" => $materials,
                                "title" => $title,
                                "description" => $description,
                                "keywords" => $keywords,          
                                "meta" => $meta,
                                "type" => $type,
                                "id_type" => $id_type,
                                "id" => $id,
                                "url" => $url,
                                ), 
                                "", 
                                $action);
         
         
     }
     //-------------------------------------------------------------------------------------------------
     //form article 
     else if ($this->type == 'article') { 
        $form = new form(array( "articleType" => $articleType,
                                "book_art" => $book_art,
                                "book_art_text" => $book_art_text,
                                "short_author" => $short_author,
                                "name"   => $name,
                                "short_name" => $short_name,
                                "year" => $year,
                                "id_sp_polnota" => $id_sp_polnota_art,
                                "url_page" => $url_page,
                                "start_page" => $start_page,
                                "file" => $file,
                                "author" => $author,
                                "redactor" => $redactor,
                                "ubilyar" => $ubilyar,
                                "articleNext" => $articleNext,
                                "articlePrev" => $articlePrev,
                                "is_visible"   => $is_visible,
                                "is_dot" => $is_dot,
                                "is_view_author" => $is_view_author,
                                "is_view_zaglavie" => $is_view_zaglavie,
                                "is_search" => $is_search,
                                "is_add_year" => $is_add_year_art,
            
                                "annotaciya" => $annotaciya,
                                "bo" => $bo,  
                                "article_group_bo" => $article_group_bo,
                                "title" => $title,
                                "description" => $description,
                                "keywords" => $keywords,
            
                                "type" => $type,
                                "id_type" => $id_type,
                                "id" => $id,
                                "url" => $url,
                                ), 
                                "", 
                                $action);
     }
     //-------------------------------------------------------------------------------------------------
     #filmstrip
     else if ($this->type == 'filmstrip') { 
       
        $form = new form(array( "short_author" => $short_author,
                                "name"   => $name,
                                "short_name" => $short_name,
                                "year" => $year,
                                "id_sp_polnota" => $id_sp_polnota,
                                "url_page" => $url_page,
                                "start_page" => $start_page,
                                "file" => $file,
                                "author" => $author,
                                "redactor" => $redactor,
                                "ubilyar" => $ubilyar,
                                "is_visible"   => $is_visible,
                                "is_dot" => $is_dot,
                                "is_view_author" => $is_view_author,
                                "is_view_zaglavie" => $is_view_zaglavie,
                                "is_search" => $is_search,
                                "is_add_year" => $is_add_year_art, 
            
                                "annotaciya" => $annotaciya,
                                "bo" => $bo, 
                                "title" => $title,
                                "description" => $description,
                                "keywords" => $keywords,
                                
                                "type" => $type,
                                "id_type" => $id_type,
                                "id" => $id,
                                "url" => $url,
                                ), 
                                "", 
                                $action);
     }
     //-------------------------------------------------------------------------------------------------
     
     //-------------------------------------------------------------------------------------------------
     #thesis
     else if ($this->type == 'thesis') { 
       
        $form = new form(array( "thesis_type" => $thesis_type,
                                "vid_dis" => $vid_dis,
                                "list_dis" => $list_dis,
                                "short_author" => $short_author,
                                "name"   => $name,
                                "short_name" => $short_name,
                                "year" => $year,
                                "id_sp_polnota" => $id_sp_polnota,
                                "url_page" => $url_page,
                                "start_page" => $start_page,
                                "file" => $file,
                                "author" => $author,
                                "director" => $director,
                                "opponent" => $opponent,
                                "ubilyar" => $ubilyar,
                                "is_visible"   => $is_visible,
                                "is_view_author" => $is_view_author,
                                "is_view_zaglavie" => $is_view_zaglavie,
                                "is_search" => $is_search,
                                "is_add_year" => $is_add_year_art,
            
            
                                
                                "bo" => $bo, 
                                "place_of_work" => $place_of_work,
                                "title" => $title,
                                "description" => $description,
                                "keywords" => $keywords,
                                
                                "type" => $type,
                                "id_type" => $id_type,
                                "id" => $id,
                                "url" => $url,
                                ), 
                                "", 
                                $action);
     }
     //-------------------------------------------------------------------------------------------------
     
     
        return  $form->print_form();
       
    }
    #---------------------------------------------------------------------------
    public function getCountDataFromTo($from, $to){        
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_data WHERE date_add > ? and  date_add < ? and id_sp_type_data= ?");
            $sth->bindParam(1, $from, PDO::PARAM_STR);
            $sth->bindParam(2, $to, PDO::PARAM_STR);
            $sth->bindParam(3, $this->id_type, PDO::PARAM_INT);
            
            $sth->execute();
            
        return $sth->rowCount();
    }
    
    
}
?>