<?php
class ConstructorNews extends ConstructorElements {
    
    private $news;
    private $lang;
        
    public function __construct( newsEdit $news, $lang) {
        $this->news = $news;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            zagolovok: "required",
                            annotaciya: "required",
                            //text: "required",
                            date: {
                                     formRuDate : true
                            },
                           
                        },
                        messages: {
                            zagolovok: "'.$this->lang['requiredField'].'",
                            annotaciya: "'.$this->lang['requiredField'].'",
                           // text: "'.$this->lang['requiredField'].'", 
                            //date: "'.$this->lang['requiredField'].'",
                              
                        }
                      });';
        
       
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['newsname'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['newsname'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->news->column = Array(
            "id" => "",
            "id_name" => "ID",
            "date" => $this->lang['form_thead_date_add'],
            "zagolovok" => $this->lang['form_thead_zagolovok'], 
            "is_visible" => $this->lang['form_thead_is_visible'], 
            "is_rss" => $this->lang['form_thead_is_rss'],
            "url" => $this->lang['form_thead_url'],
            "main_theme" => $this->lang['form_thead_main_theme'],
        );
        
        $columns = Array();                
        for($i=0; $i<=count($this->news->column)+1; $i++){
           if ( ($i==0) || ($i==count($this->news->column)+1)) $setting.=$i.":{ sorter: false }, ";
        }    
        
        $setting.="4:{ sorter: 'checkbox' }, 5:{ sorter: 'checkbox' }, ";
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        
        foreach($this->news->column as $key => $val) { 
            
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            
            if ($i==7) {$th.="<th$class>$val</th><th>".$this->lang['news_redactor']."</th>";}
            else if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;  
        $columns['head_act_width'] = 70;  
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arrayNews = $this->news->getNews();
        
        $addAct='/admin/addNews/';
        $updateAct='/admin/updateNews/';
        $formShow='/admin/formShowNews/';
        $deleteAct='/admin/deleteNews/';
        //$table="news";
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        //$tbody['fheight']= "594";
        //$tbody['fwidth']= "1110";
        
        $tbody['creditor_width']= "760";
        $tbody['creditor_height']= "220"; 
        $tbody['creditor_height2']= "120"; 
        $tbody['creditor_big']= "1";  //применяем текстовый редактор
        $tbody['creditor_small']= "1";  //применяем текстовый редактор
        $tbody['date_area']="1"; //применяем datapicker
                
        $tr="";
        for ($j=0; $j<count($arrayNews); $j++){
         
        $arrayNews[$j] = Common::removeStipsSlashes($arrayNews[$j]);    
        $id_author=$arrayNews[$j]['id_users'];
        
        unset($arrayNews[$j]['text'], $arrayNews[$j]['is_main'], $arrayNews[$j]['annotaciya'],  $arrayNews[$j]['comment'], $arrayNews[$j]['details'], $arrayNews[$j]['id_users'], $arrayNews[$j]['title'], $arrayNews[$j]['keywords'], $arrayNews[$j]['description']);
        //print_r($arrayNews[$j]);
        
        $user = new userEdit();
        $authorInfo = $user->getUser($id_author);
        $rol = $user->getRoles($id_author);
        $author=$authorInfo['login']; //." (".$authorInfo['name']." ".$authorInfo['otch'].")";
       
            
            $classTr = ( Common::is_predprintResurs($arrayNews[$j]['id_news'], 'news') ) ? ' class="preprint" title="'.$this->lang['predprint_resurs'].'"' : '' ;
        
            $i=0; 
            $tr.="<tr{$classTr}>";
                foreach($arrayNews[$j] as $key => $val ){
                   if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                        $tr.='<td class="center">'.$val.'</td>'; 
                   
                   }elseif (strrpos($key, "is_") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }                     
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="news" nameidrec="id_news"/></td>';                      
                        
                   }else if ($key=="main_theme"){
                       
                       $results=$this->news->getMainTheme($arrayNews[$j]['main_theme']);
                     
                       if ($results) {
                            $tr.="<td>".$results[0]['name']."</td>";
                        }else{
                            $tr.="<td>".$this->lang['no_main_theme']."</td>";
                        }  
             
                   }else if ($key=="date"){
                       $tr.="<td>".Common::getTimeConversion($arrayNews[$j]['date'], $this->lang)."</td>"; 
                   }else{                    
                        $tr.="<td>$val</td>";                                          
                    }                                                    
                    $i++; 
                }            
            $tr.='
             <td class="cnt">'.$author.'</td>   
             <td style="padding-left: 6px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        } 
         $tbody['tr']=$tr;
       
    return $tbody;
    }   
}

?>