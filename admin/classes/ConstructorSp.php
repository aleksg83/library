<?php
class ConstructorSp extends ConstructorElements {
    
    private $sp;
    private $lang;
        
    public function __construct(spEdit $sp, $lang) {
        $this->sp = $sp;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            name: "required",
                        },
                        messages: {
                            name: "'.$this->lang['requiredField'].'",
                        }
                      });';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['spname'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['spname'].'</span>';        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        
        $this->sp->column=Array(
            "id" => "",
            "id-show" => "ID",
            "name" => $this->lang['form_thead_name'],
            "is_visible" => $this->lang['form_thead_is_visible'],
            "position" => $this->lang['menu_position'], 
        );
        #-----------------------------------------------------------------------
        if ( ($this->sp->name == "vid_news") || ($this->sp->name == "theme_news") ){
             $this->sp->column=Array(
                "id" => "",
                "id-show" => "ID",
                "name" => $this->lang['form_thead_name'],
                "is_visible" => $this->lang['form_thead_is_visible'],
                "url" => $this->lang['lib_url'],   
                "position" => $this->lang['menu_position'],    
            );
            
        }
        #-----------------------------------------------------------------------
        if ($this->sp->name == "organasation"){
             $this->sp->column=Array(
                 "id" => "",
                 "id-show" => "ID",
                 "name" => $this->lang['form_thead_sp_org_name'],
                 "is_visible" => $this->lang['form_thead_is_visible'],
                 "is_view_in_main" => $this->lang['form_thead_sp_org_is_main'],
                 "url" => $this->lang['form_thead_url'], 
                 "is_in_encyclopedia" => $this->lang['form_thead_is_in_encyclopedia'],
                 "position" => $this->lang['menu_position'],
                 "id_users" => $this->lang['news_redactor'],
                
            );
        }
        #-----------------------------------------------------------------------
        
        $columns = Array();   
        
       /* 
        if ($this->sp->name=="theme_news") {
                unset($this->sp->column['is_main'], $this->sp->column['url']);
                        
        }
        */
        
        for($i=0; $i<=count($this->sp->column); $i++){
           if ( ($i==0) || ($i=count($this->sp->column))) $setting.=$i.":{ sorter: false }, ";
             
        }  
        
        if ($this->sp->name == "organasation"){
            $setting.="3:{ sorter: 'checkbox' },
                       4:{ sorter: 'checkbox' },
                       6:{ sorter: 'checkbox' },";
        }else{
            $setting.="3:{ sorter: 'checkbox' },";
        }       
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->sp->column as $key => $val) { 
           
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            
            if ($i>0) $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;    
        $columns['numb'] = count($this->sp->column)+1;
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arraySp = $this->sp->select();
        
        $addAct='/admin/addSp/'.$this->sp->name;
        $updateAct='/admin/updateSp/'.$this->sp->name;
        $formShow='/admin/formShowSp/'.$this->sp->name;
        $deleteAct='/admin/deleteSp/'.$this->sp->name;
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        if ($this->sp->name == "organasation"){
              //$tbody['fheight']= "594";
              //$tbody['fwidth']= "1110";
        
              $tbody['creditor_width']= "760";
              $tbody['creditor_height']= "220";   
              $tbody['creditor_big']= "1";   
        }else{
              $tbody['fheight']= "330";
              $tbody['fwidth']= "320";
        }
        
        $rubricBut="";
        $style='style="padding-left: 40px;"';
        $tr="";
        for ($j=0; $j<count($arraySp); $j++){
            $arraySp[$j] = Common::removeStipsSlashes($arraySp[$j]);
            $i=0; 
            $tr.='<tr>';
            
            #-------------------------------------------------------------------
            if ($this->sp->name=="theme_news") {
                unset($arraySp[$j]['is_main']);
                        
            }
            #-------------------------------------------------------------------
            if ($this->sp->name == "organasation"){
                 $user=new userEdit();
                 $authorInfo = $user->getUser($arraySp[$j]['id_users']);
                 $author=$authorInfo['login']; 
                 unset($arraySp[$j]['day'], $arraySp[$j]['month'], $arraySp[$j]['year'], $arraySp[$j]['text'], 
                       $arraySp[$j]['short_text'], $arraySp[$j]['definition'], $arraySp[$j]['keywords'], $arraySp[$j]['description'], 
                       $arraySp[$j]['title'], $arraySp[$j]['photo'], $arraySp[$j]['add_data'], 
                       $arraySp[$j]['update_data'], $arraySp[$j]['id_users'], $arraySp[$j]['is_view_news']);
                 
                 $rubricBut='
                     <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['sp_org_in_rubric'].'" href="/admin/showSpOrgRubric/'.$this->sp->name.'/?id='.$arraySp[$j]['id_sp_organasation'].'&name='.urlencode($arraySp[$j]['name']).'" id_record="'.$arraySp[$j]['id_sp_organasation'].'">
                        <span class="ui-icon ui-icon-shuffle"></span>
                    </a> ';
                  $style='style="padding-left: 24px;"';
            }
            #-------------------------------------------------------------------
        
            
            
            
                foreach($arraySp[$j] as $key => $val ){
                   if ($i==0){
                        $id_record=$val;  
                        $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
                        $tr.='<td class="center">'.$id_record.'</td>';
                   }elseif(strrpos($key, "is_") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }                    
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="sp_'.$this->sp->name.'" nameidrec="id_sp_'.$this->sp->name.'"/></td>';                      
                    }else if ($key=="position"){                    
                        $tr.="<td class='cnt'>$val</td>";                                          
                    } else {                    
                        $tr.="<td>$val</td>";                                          
                    }                                                    
                    $i++; 
                    
                    if ($this->sp->name == "organasation"){
                        if ($i==count($arraySp[$j])) {
                             $tr.="<td  class=\"cnt\">$author</td>"; 
                        }
                    }
                    
                }            
            $tr.='<td '.$style.'>  
             '.$rubricBut.'   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil"></span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
               <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
        }  
        $tbody['tr']=$tr;
          
     return $tbody;
    }   
}

?>