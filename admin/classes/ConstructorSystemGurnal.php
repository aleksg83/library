<?php
class ConstructorSystemGurnal extends ConstructorElements {
    
    private $sistem, $lang, $column;
        
    public function __construct( systemGurnalEdit $sistem, $lang) {
        $this->sistem = $sistem;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['system_gurnal_name'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['system_gurnal_name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        
        $this->column = Array(
            "id" => "",
            "date" => $this->lang['system_gurnal_date'],
            "id_user" => $this->lang['system_gurnal_id_user'],
            "login" => $this->lang['system_gurnal_login'],
            "action" => $this->lang['system_gurnal_action'], 
            "ip_addres" => $this->lang['system_gurnal_ip'], 
        );
        
        $columns = Array();                
        
        for($i=0; $i<=count($this->column)+1; $i++){
           if ( $i==count($this->column)) $setting.=$i.":{ sorter: false }, ";
        }
        
        $columns['setting'] = "";//$setting;        
        $th="";
        $i=0;
        
        foreach($this->column as $key => $val) { 
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
         
        $columns['hidden_check_var2'] = true;
        //$columns['hidden_action'] = true;
        //$columns['numb'] = count($this->column)+1;
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
       $tbody = Array();
       
       if (!isset($_POST['date_from'])){
            $_POST['date_from'] = $_SESSION["PharusAdminPhilterGurnalDateStart"];
            $_POST['date_to'] = $_SESSION["PharusAdminPhilterGurnalDateEnd"];
       }  
       
       if ( ($_POST['date_from'] != "") || ($_POST['date_to'] != "")){ //если в фильтре установлены данные
           
            $_POST['date_from']=($_POST['date_from']=="")? date("d.m.Y"): $_POST['date_from']; 
            if ($_POST['date_to']==""){
                $day=new DateTime($_POST['date_from']);
                $day->modify('+1 year'); 
                $_POST['date_to']=$day->format('d.m.Y');
            }
            
            //формируем 
            $date_from=Common::getTimeConversionInsertDB($_POST['date_from']);
            $date_to=Common::getTimeConversionInsertDB($_POST['date_to']);
            
            $start = new DateTime($date_from);
            $d1=$start->format("Y-m-d H:i:s");
            $end = new DateTime($date_to); 
            $d2=$end->format("Y-m-d H:i:s");
            
            if ($d2 >= $d1){ 
                $array = $this->sistem->showSystemGurnalRangeValue($d1, $d2);
            }
           
       }else{
            $array = $this->sistem->showSystemGurnalValue();
            
       }    
        //Common::pre($array);
       
       $emptyAct='/admin/systemGurnalEmpty/';
        $formShow='/admin/formShowSystemGurnal/';
        
        $panel = new servicePanel();
        $tbody['delete'] = $panel->getEmptyButton($emptyAct, $this->lang['system_gurnal_del'], $urlPage);
        $tbody['philter_date'] = $panel->getPhilterDateGurnal($this->lang, $urlPage);
        
        //$tbody['fheight']= "600";
        $tbody['fwidth']= "620";
        $tbody['view']="view";
         
       $tr="";
       for ($j=0; $j<count($array); $j++){
            
            $id_record=$array[$j]['id_system_gurnal'];
                        
            $tr.='<tr>';
            
            $tr.='<td class="cnt">'.$array[$j]['date'].'</td>'; 
            $tr.='<td class="cnt">'.$array[$j]['id_users'].'</td>'; 
            $tr.='<td class="cnt">'.$array[$j]['login'].'</td>'; 
            $tr.='<td class="cnt">'.$array[$j]['action'].'</td>'; 
            $tr.='<td class="cnt">'.$array[$j]['ip_address'].'</td>'; 
            
          
           $tr.='<td style="padding-left:65px; width:40px !important;"> 
                     <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['system_gurnal_view'].'" href="javascript:void(0);" action="'.$updateAct.'" act="view" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
                     <span class="ui-icon ui-icon-search"></span>
                     </a>
                 </td>
                </tr>';
         
            
        }
       
       $tbody['tr']=$tr;
       
    return $tbody;
    }   
}

?>