<?php
class feedbackEdit {
    private $db,  $error;
    public $column;
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();
        Common::ChecksAccessRedactor();
    }
    
    public function getMessage($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_feedback WHERE id_feedback=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_feedback ORDER BY date_add desc");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        
        return $result;
    }
    
    public function getBanEmail($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_ban_email WHERE id_ban_email=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_ban_email ORDER BY email");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        
        return $result;
    }
   
    public function update($data){
        if ($data['status']=="2")  {
            $sql=', id_users=:id_users';
        }      
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_feedback SET status=:status {$sql} WHERE id_feedback=:id_feedback");        
        $sth->bindParam(":status", $data['status'], PDO::PARAM_INT);
        if ($data['status']=="2") $sth->bindParam(":id_users", $data['id_user'], PDO::PARAM_INT);
        $sth->bindParam(":id_feedback", $data['id'], PDO::PARAM_INT);
        $sth->execute();        
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
     
    }
    
    public function delete($id){
       $arrayDel = explode(",", $id);
       $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
       $sth = $this->db->prepare("DELETE FROM ".PREFIX."_feedback WHERE id_feedback IN ($place_holders)");
       $sth->execute($arrayDel);
       $err = $sth->errorInfo();
       return ($err[0] != '00000')?false:true;
        
    }
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id, $act, $action, $url, $lang){
         $feed = $this->getMessage($id);
         $feed = Common::removeStipsSlashes($feed);
         $id_user = $_SESSION['user']['user_id']; //id редактора сообщения
         
         $size=68; 
         $class='sm0'; //класс списка
         
         
         $options = array();
         if ($feed['status']=="1") {
             $options[1]=$lang['server_feedback_status_s1'];
             $options[2]=$lang['server_feedback_status_s2'];
         }else if ($feed['status']=="2") $options[2]=$lang['server_feedback_status_s2'];
          
         if ($act=="edit") $dateAdd =  Common::getTimeConversion($feed['date_add'], $lang);
                          
       
         $date =          new field_text("date_add", $lang['form_thead_date_add'], false, $dateAdd, "", $size);         
         $name =          new field_text("name", $lang['server_feedback_name_user'], false, $feed['name'], "", $size);         
         $email =         new field_text("email", $lang['form_thead_user_send_email'], false, $feed['email'], "", $size);         
         $message =       new field_textarea("message", $lang['server_feedback_message'], false, $feed['message'], 53, 6, "", "", ""); 
         $status =    new field_select("status", $lang['server_feedback_status_mess'], $options, $feed['status'], false, "", "", "", "", $class); 
       
         $id_user =       new field_hidden_int("id_user", false, $id_user);
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
          
        $form = new form(array("date" => $date,
                               "name" => $name,
                               "email" => $email,
                               "message" => $message,
                               "status" => $status,
                               "id" => $id_rec,
                               "url" => $url,
                               "id_user" => $id_user,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
        
     }
     //form
     #---------------------------------------------------------------------------------------------
     
     
     #--------------------------------------------------------------------------
     #banEmail
     
      public function addBan($data){
        if ($this->getCounBanEmail($data['email'])==0){
            $sth = $this->db->prepare("INSERT INTO ".PREFIX."_ban_email (email) VALUES (:email)");
            $sth->bindParam(":email", $data['email'], PDO::PARAM_INT);
            $sth->execute();
            $err = $sth->errorInfo();
            $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
            return ($err[0] != '00000')?false:true;
        }else{
            return true;
        }    
       
     }
     public function updateBan($data){
        $sth = $this->db->prepare("UPDATE ".PREFIX."_ban_email SET email=:email WHERE id_ban_email=:id_ban_email");        
        $sth->bindParam(":email", $data['email'], PDO::PARAM_INT);
        $sth->bindParam(":id_ban_email", $data['id'], PDO::PARAM_INT);
        $sth->execute();        
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
      }
    
    public function deleteBan($id){
       $arrayDel = explode(",", $id);
       $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
       $sth = $this->db->prepare("DELETE FROM ".PREFIX."_ban_email WHERE id_ban_email IN ($place_holders)");
       $sth->execute($arrayDel);
       $err = $sth->errorInfo();
       return ($err[0] != '00000')?false:true;  
    }
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showFormBan($id, $act, $action, $url, $lang){
         $ban = $this->getBanEmail($id);
         
         $size=41; 
        
         $email =         new field_text("email", $lang['form_thead_user_send_email'], false, $ban['email'], "", $size);         
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
          
        $form = new form(array("email" => $email,
                               "id" => $id_rec,
                               "url" => $url,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
        
     }
     //form
     #---------------------------------------------------------------------------------------------
      public function getCounBanEmail($email){
        if($email){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_ban_email WHERE email=?");
            $sth->bindParam(1, $email, PDO::PARAM_INT);
            $sth->execute();
            $count=$sth->rowCount();
            //$result = $sth->fetch(PDO::FETCH_ASSOC);
            return $count;
        }
        
    }
     
     
     
     
}

?>