<?php
class ConstructorNewsSendUser extends ConstructorElements {
    
    private $user;
    private $lang;
        
    public function __construct(newsSendUserEdit $user, $lang) {
        $this->user = $user;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return  '$("#validateForm").validate({
                        rules: {
                            name: "required",
                            email: {
                                required: true,
                                email: true
                            },    
                            
                           
                        },
                        messages: {
                            name: "'.$this->lang['requiredField'].'",
                            email: {
                                   required: "'.$this->lang['requiredField'].'",
                                   email: "'.$this->lang['errorEmailRules'].'",    
                           }             
                        }
                      });';
       
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['news_send_user_page'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$this->lang['news_send_user_page'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        $this->user->column = Array(
            "id" => "",
            "name" => $this->lang['form_thead_user_send_name'],
            "sex" => $this->lang['form_thead_user_send_sex'],
            "email" => $this->lang['form_thead_user_send_email'], 
            "is_active" => $this->lang['form_thead_user_is_active'], 
            "is_black_list" => $this->lang['form_thead_is_black_list']
        );
        
        $columns = Array();                
        for($i=0; $i<=count($this->user->column)+1; $i++){
           if ( ($i==0) || ($i==count($this->user->column)+1)) $setting.=$i.":{ sorter: false }, ";
        }    
        
        $setting.="4:{ sorter: 'checkbox' }, 5:{ sorter: 'checkbox' }, ";
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        
        foreach($this->user->column as $key => $val) { 
            
            if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
            else $class='';
            
            if ($i>0)  $th.="<th$class>$val</th>";
            $i++;
        }        
        $columns['th'] = $th;           
        
        return  $columns;
      
    }
    
    public function getTableTbody($urlPage){
        
        $tbody = Array();
        $array = $this->user->getUsers();
        
           
        $addAct='/admin/addSendUser/';
        $updateAct='/admin/updateSendUser/';
        $formShow='/admin/formShowSendUser/';
        $deleteAct='/admin/deleteSendUser/';
        //$table="news";
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
        $tbody['fheight']= "400";
        $tbody['fwidth']= "380";
       
        $tr="";
        for ($j=0; $j<count($array); $j++){
         
        $array[$j] = Common::removeStipsSlashes($array[$j]);    
        $sex = ($array[$j]['sex']=='1') ? $this->lang['men'] : $this->lang['women']; 
        unset($array[$j]['sex']);
         $i=0; 
         $tr.='<tr>';
         foreach($array[$j] as $key => $val ){
             
           if ($i==0){
                $id_record=$val;  
                $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
           }else if ($i==1){
                $tr.="<td>{$val}</td><td>{$sex}</td>"; 
                
           }elseif (strrpos($key, "is_") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }                     
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="user_email" nameidrec="id_user_email"/></td>';                      
                        
           }else{                    
                 $tr.="<td>{$val}</td>";                                          
           }                                                    
            $i++; 
    }//foreach  
           
            $tr.='
             <td style="padding-left: 40px;"> 
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
              <span class="ui-icon ui-icon-pencil">ss</span>
             </a>                                      
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-circle-close"></span>
            </a> 
            </td>
        </tr>';
      
  }//for 
         $tbody['tr']=$tr;
       
         return $tbody;
        
    }  
}

?>