<?php

class servicePanel {
    #---------------------------------------------------------------------------
    public function getAddButton($addAct, $title, $urlPage, $formShow){
        
       $addButton='<a class="btn ui-state-default ui-corner-all tooltip action" href="javascript:void(0);" title="'.$title.'" action="'.$addAct.'" act="add" url="'.$urlPage.'" form="'.$formShow.'" id_record="">
                    <span class="ui-icon ui-icon-plusthick"></span>
                    '.$title.'
                    </a>';
        
        return $addButton;
    }
    #---------------------------------------------------------------------------
    public function getAddMassButton($title, $urlPage, $formShow){
        
       $addButton='<a class="btn ui-state-default ui-corner-all tooltip multy-load close-multi" href="javascript:void(0);" title="'.$title.'"  url="'.$urlPage.'">
                    <span class="ui-icon ui-icon-transferthick-e-w"></span>
                    '.$title.'
                    </a>';
        
       return $addButton;
    }
    #---------------------------------------------------------------------------
    
    public function getDelButton($deleteAct, $title, $urlPage){
       
      $delButton='<a class="btn ui-state-default ui-corner-all tooltip  action" href="javascript:void(0);" title="'.$title.'" action="'.$deleteAct.'" act="delall" url="'.$urlPage.'">
                    <span class="ui-icon ui-icon-trash"></span>
                    '.$title.'
                  </a>';  
      return $delButton;
    }
    #---------------------------------------------------------------------------
    
    public function getRightButton($title, $urlPage, $class="ui-icon-pencil"){
       
      $rightButton='<a class="btn ui-state-default ui-corner-all tooltip  action right" href="'.$urlPage.'" title="'.$title.'">
                    <span class="ui-icon '.$class.'"></span>
                    '.$title.'
                  </a>';  
      return $rightButton;
    }
    //--------------------------------------------------------------------------
    
     public function getEmptyButton($deleteAct, $title, $urlPage){
       
       $emptyButton='<a class="btn ui-state-default ui-corner-all tooltip empty" href="javascript:void(0);" title="'.$title.'" action="'.$deleteAct.'" url="'.$urlPage.'">
                    <span class="ui-icon ui-icon-trash"></span>
                    '.$title.'
                  </a>';  
      return $emptyButton;
    }
    
    
    #---------------------------------------------------------------------------
    public function getPersonButton($addAct, $title, $hover, $class){
        
       $addButton='<a class="btn ui-state-default ui-corner-all tooltip action '.$class.'" href="'.$addAct.'" title="'.$hover.'">
                    <span class="ui-icon ui-icon-person"></span>
                    '.$title.'
                    </a>';
        
        return $addButton;
    }
    #---------------------------------------------------------------------------
    public function getPhilterCalButton($lang, $urlCal, $type){
       $addButton='<p class="philter">
           '.$lang['philter_calendar'].':&nbsp; 
           <select class="philterCalc" url="'.$urlCal.'">';
       
            for ($j=1; $j<4; $j++){
              $sel= ($type==$j)? ' selected="selected"' : "";
              $addButton.='<option value="'.$j.'"'.$sel.'>'.$lang['philter_calendar_type'.$j].'</option>';
            }       
       $addButton.='</select> </p>';
       
       return $addButton;
    }
    #---------------------------------------------------------------------------
    public function getPhilterDateButton($lang, $urlCal, $type){
        
        if (isset($_POST['date_from'])){
            $_SESSION["PharusAdminPhilterDateStart"]=($_POST['date_from']!="")? $_POST['date_from'] : "";
            $_SESSION["PharusAdminPhilterDateEnd"]=($_POST['date_to']!="")? $_POST['date_to'] : "";
        }
        
         $philterDate='<p class="philterForm"><form action="/'.$urlCal.''.$type.'/" method="POST" class="philterForm"> 
                <span> 
                '.$lang['cal_philter_date'].'&nbsp;'.$lang['cal_philter_from'].'&nbsp;<input type="text" name="date_from" class="philter_from" value="'.$_SESSION["PharusAdminPhilterDateStart"].'"/>&nbsp;'.$lang['cal_philter_to'].'&nbsp;<input type="text" name="date_to" class="philter_to" value="'.$_SESSION["PharusAdminPhilterDateEnd"].'"/> 
                 <input type="submit" name="subPhilter" value="OK" class="philterBut"/>
                </span>
              </form>
              </p>
                ';
         return $philterDate;
    }
    #---------------------------------------------------------------------------
      public function getPhilterDateGurnal($lang, $url){
        
        if (isset($_POST['date_from'])){
            $_SESSION["PharusAdminPhilterGurnalDateStart"]=($_POST['date_from']!="")? $_POST['date_from'] : "";
            $_SESSION["PharusAdminPhilterGurnalDateEnd"]=($_POST['date_to']!="")? $_POST['date_to'] : "";
        }
        
         $philterDate='<p class="philterForm"><form action="'.$url.'" method="POST" class="philterForm"> 
                <span> 
                '.$lang['system_gurnal_range_date'].'&nbsp;'.$lang['cal_philter_from'].'&nbsp;<input type="text" name="date_from" class="philter_from" value="'.$_SESSION["PharusAdminPhilterGurnalDateStart"].'"/>&nbsp;'.$lang['cal_philter_to'].'&nbsp;<input type="text" name="date_to" class="philter_to" value="'.$_SESSION["PharusAdminPhilterGurnalDateEnd"].'"/> 
                 <input type="submit" name="subPhilter" value="OK" class="philterBut"/>
                </span>
              </form>
              </p>
                ';
         return $philterDate;
    }
    #---------------------------------------------------------------------------
    public function getUpdateRSSSetting($updateAct, $title, $urlPage, $formShow){
       
      $rightButton='<a class="btn ui-state-default ui-corner-all tooltip  action right" href="javascript:void(0);" title="'.$title.'" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" form="'.$formShow.'" id_record="1">
                    <span class="ui-icon ui-icon-pencil"></span>
                    '.$title.'
                  </a>';        
      
      
      return $rightButton;
    }
    #---------------------------------------------------------------------------
    public function getUpdateRSS($updateAct, $title, $lang, $time){
       
      $rightButton='<a class="btn ui-state-default ui-corner-all tooltip  action" href="'.$updateAct.'" title="'.$title.'">
                    <span class="ui-icon ui-icon-refresh"></span>
                    '.$title.'
                  </a>
                  <p class="left">'.$lang['rss_last_update'].' - '.$time.'</p>';        
      
      
      return $rightButton;
    }
    #---------------------------------------------------------------------------
    public function filterLibVariables($lngD, $nameFile, $lang){
      $directory=DROOT."/lang/{$lngD}/";
      $linkDirectory=opendir($directory);
      //chdir($linkDirectory);
      $selectFile='<select class="right philterLang">';
     
      while ( $d=readdir($linkDirectory)){
          
          if(is_file($directory.$d)){
                               
                if ($d==$nameFile) $check=' selected="selected"';
                else $check="";
              
                $name=$d;
                if ($d=="portal.php") $name=$lang['portal_panel'];
                else if ($d=="admin.php") $name=$lang['admin_panel'];
                
                $selectFile.='<option value="'.$d.'"'.$check.' >'.$name.'</option>';
               
           }   
        }
      $selectFile.="</select>";    
      
      return $selectFile;
     
    }
    #---------------------------------------------------------------------------
    
    public function getAddArhivsButton($addAct, $title, $urlPage, $action, $type, $dopClass="ui-icon-plusthick"){
        
       $addButton='<a class="btn ui-state-default ui-corner-all tooltip '.$action.'" href="javascript:void(0);" title="'.$title.'" action="'.$addAct.'" type="'.$type.'" url="'.$urlPage.'">
                    <span class="ui-icon '.$dopClass.'"></span>
                    '.$title.' <span class="act close"><img src="/admin/templates/default/images/ajax-loader.gif"/></span>
                    </a>';
        
        return $addButton;
    }
    
}

?>