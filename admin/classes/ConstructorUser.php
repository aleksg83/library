<?php
class ConstructorUser extends ConstructorElements {
    
    private $user;
    private $lang;
  
    public function __construct( userEdit $user, $lang) {
        $this->user = $user;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        
            return  '$("#validateForm").validate({
                        rules: {
                             login: {
                                    required: true,
                                    loginFormSimbols : true,
                                    loginFormUniq: true
                             },
                             password:{
                                     required: true,
                                     minlength: 6,
                             }, 
                             password_redact:{
                                     minlength: 6,
                             }   
                             
                        },
                        messages: {
                             //login: "'.$this->lang['requiredField'].'",
                             password: "'.$this->lang['requiredFieldPassword'].'",    
                             password_redact: "'.$this->lang['requiredFieldPassword'].'",        
                        }
                      });';
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['user_page'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
      
          $this->lib->column = Array(
             'id' => "",
             'id-show' => 'ID',
             'login' => $this->lang['login'],
             'fam' => $this->lang['surname'],  
             'name' => $this->lang['user_name'], 
             'is_active' => $this->lang['user_is_active'],  
             'roles' => $this->lang['user_roles'],   
             
          );
    
        $columns = Array();  
        
        for($i=0; $i<=count($this->lib->column); $i++){
           if ( ($i==0) || ($i=count($this->lib->column))) $setting.=$i.":{ sorter: false }, ";
        }
        
        $setting.="5:{ sorter: 'checkbox' },";
        
        $columns['setting'] = $setting;        
        $th="";
        $i=0;
        foreach($this->lib->column as $key => $val) { 
         if (strrpos($key, "is_") === 0) $class=' class="filter-false"';
           else $class='';   
          if ($i>0) $th.="<th$class>$val</th>";
          $i++;
        }        
        $columns['th'] = $th;    
        
        $columns['numb'] = count($this->lib->column);
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $arrayResurs= $this->user->getUser();
            
        
        $dopParam='';
        $addAct='/admin/addUser/'.$dopParam;
        $updateAct='/admin/updateUser/'.$dopParam;
        $formShow='/admin/formShowUser/'.$dopParam;
        $deleteAct='/admin/deleteUser/'.$dopParam;
        
        $urlPage=$urlPage.$dopParam;
        
        $panel = new servicePanel();
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        
            
        $tbody['fheight']= "430";
        $tbody['fwidth']= "620";
        
         
        $tr="";
                       
        for ($j=0; $j<count($arrayResurs); $j++){
            
            $id_record=$arrayResurs[$j]['id_users'];
            
            $disable='';
            if ($arrayResurs[$j]['login']=="admin")  $disable='disabled="disabled"';
            
            $tr.='<tr>';
            
            $tr.='<td class="center"><input type="checkbox" value="'.$id_record.'" name="list" class="checkbox" '.$disable.'/></td>'; 
            $tr.='<td class="center">'.$id_record.'</td>'; 
            
            $tr.='<td class="cnt">'.$arrayResurs[$j]['login'].'</td>'; 
            $tr.='<td class="cnt">'.$arrayResurs[$j]['fam'].'</td>'; 
            $tr.='<td class="cnt">'.$arrayResurs[$j]['name'].'</td>'; 
            
            
           $checked = ($arrayResurs[$j]['is_active']=="1") ? 'checked="cheked"': "";
           $tr.='<td class="cnt"><input type="checkbox" value="'.$arrayResurs[$j]['is_active'].'" name="is_active" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="users" nameidrec="id_users" '.$disable.'/></td>';                      
           #-----------------------------------------
           $rolUser="";
           $roles=$this->user->getRoles($id_record);
           for ($i=0; $i<count($roles); $i++ ){
              $rolUser.=$roles[$i]['name']."<br/>";
           }
           #----------------------------------------- 
           $tr.='<td class="cnt">'.$rolUser.'</td>'; 
           
           $del="";
           if ($arrayResurs[$j]['login'] != 'admin'){
           $del=' <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
                        <span class="ui-icon ui-icon-circle-close"></span>
                    </a> ';
           }
           
           $tr.='<td style="padding-left:30px;"> 
                     <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['user_in_roles'].'" href="/admin/showUserInRoles/?user='.$id_record.'&login='.$arrayResurs[$j]['login'].'" id_record="'.$id_record.'">
                      <span class="ui-icon ui-icon-shuffle"></span>
                    </a>                   
                    <a class="btn_no_text btn ui-state-default ui-corner-all tooltips action action2" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'">
                     <span class="ui-icon ui-icon-pencil"></span>
                     </a>
                     '.$del.'
                 </td>
                </tr>';
         
            
        }
       
       
       $tbody['tr']=$tr;
               
     return $tbody;
      
    }  
            
}

?>