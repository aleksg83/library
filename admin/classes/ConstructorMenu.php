<?php
class ConstructorMenu extends ConstructorElements {
    private $menu;
    private $lang;
    private $db;
    
    public function __construct(menuEdit $menu, $lang) {
        $this->menu = $menu;
        $this->lang = $lang;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return ' $("#validateForm").validate({
                        rules: {
                            name: "required",
                            urlpunct:{
                                required: true,
                                menuURLUniq:true,
                            } 
                        },
                        messages: {
                            name: "'.$this->lang['requiredField'].'",
                            //urlpunct: "'.$this->lang['requiredField'].'",     
                        }
                      });';
    }
    
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = ($this->menu->isRubr)?$this->lang['rubrics']:$this->lang['menu'];        
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
        
         //$this->menu->isRubr;
        
       
        
        $this->menu->column = Array ( "name" => $this->lang['form_thead_name_punct'], 
                                      "id-show" => "ID",
                                      "is_visible" => $this->lang['form_thead_is_visible'],
                                      "url" => $this->lang['form_thead_url'],
                                      "position"  => $this->lang['form_thead_position'],
                                     );
        
         if ($this->menu->isRubr) $this->menu->column['id'] = $this->lang['form_thead_id_record'];
        
        $columns = Array();          
        for($i=0; $i<=count($this->menu->column)+2; $i++){
            $setting.=$i.":{ sorter: false }, ";
        }
        $columns['setting'] = $setting;
        
        $th="";
        $i=0;
        foreach($this->menu->column as $key => $val)  { 
            if ( ($this->menu->isRubr) && ($key=="name")) $val=$this->lang['rubric'];
            if ($key=="name") { $th.="<th>$val</th> <th>&nbsp;</th><th>&nbsp;</th>"; }    
            else  if ($i>0) { $th.="<th>$val</th>";}
            $i++;
        }
       
        $columns['th'] = $th; 
        
        $columns['hidden_check'] = true;
        
        $columns['numb'] = count($this->menu->column);
        
        return  $columns;
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();        
        $arraySp = $this->menu->showMenu(); 
                
        #var_dump($this->menu); exit;
        $idOpenPage = explode(",", $_COOKIE['pharus_tree_showMenu']);
        $tbody['tree'] = true;
        
        $dopParam=($this->menu->isRubr)? "?rubric=ok" : "";
        $addAct='/admin/addMenu/'.$dopParam;
        $updateAct='/admin/updateMenu/'.$dopParam;
        $formShow='/admin/formShowMenu/'.$dopParam;
        $deleteAct='/admin/deleteMenu/'.$dopParam;
        
        $panel = new servicePanel();
        
        $tbody['add'] = $panel->getAddButton($addAct, $this->lang['add'], $urlPage, $formShow);
        
        if (!$this->menu->isRubr) {
            $siteMapAct='/adminajax/updateSiteMap/';
            $tbody['addMass'] = $panel->getAddArhivsButton($siteMapAct, $this->lang['menu_site_map'], $urlPage, " sitemap", "xml", "ui-icon-shuffle");
        }    
        
       // $tbody['delete'] = $panel->getDelButton($deleteAct, $this->lang['deleteAll'], $urlPage);
        $tbody['fheight']=($this->menu->isRubr)? "380" : "604";
        $tbody['fwidth']=($this->menu->isRubr)? "300" : "400";
        $tbody['draggable']=true;
        
        $tr="";
        for ($j=0; $j<count($arraySp); $j++){
            $i=0; 
            
            $level= $arraySp[$j]['level'];
            $id_menu_top= $arraySp[$j]['id_menu_top'];
            $position=$arraySp[$j]['position'];
            $id_rec=$arraySp[$j]['id_menu'];
            $namePunct= urlencode($arraySp[$j]['name']);
            $nameButRubricInMenu=($this->menu->isRubr)? $this->lang['menu_in_rubric'] : $this->lang['rubric_in_menu'];
            /*------------------------------------------------------*/
             $dopParam2=($this->menu->isRubr)? "?rubric=ok&menu=$id_rec&name=$namePunct" : "?menu=$id_rec&name=$namePunct";
             $rubricMenuShow='/admin/rubricMenu/'.$dopParam2;
            /*------------------------------------------------------*/
            
             if ($level>1) {
                 if ( in_array($id_menu_top, $idOpenPage)) $classTr='';
                 else $classTr=' close';
             } else{
                  $classTr='';
             }
             
             
             $tr.='<tr class="draggable'.$classTr.'" id="row-'.$id_rec.'-node" level="'.$level.'" parent="'.$id_menu_top.'" position="'.$position.'" rubric="'.$this->menu->isRubr.'">';
           
            //-------------------------------------
                      $positionList='<select class="row-weight" name="node-'.$id_rec.'-weight">';
                     for ($i0=1; $i0<count($arraySp); $i0++){
                          if ( $i0==$position)  $sel1="selected"; else $sel1="";                        
                          $positionList.='<option '.$sel1.' value="'.$i0.'">'.$i0.'</option>'; 
                      }
                      $positionList.="</select>";
                      
                      
                      $parentNodeList='<select class="row-parent" name="node-'.$id_rec.'-parent">';
                      $parentNodeList.='<option value="0">Root</option>';
                      
                      for ($i1=0; $i1<count($arraySp); $i1++){
                          if ( $arraySp[$i1]['id_menu']==$id_menu_top)  $sel2="selected"; else $sel2="";                        
                          $parentNodeList.='<option '.$sel2.' value="'.$arraySp[$i1]['id_menu'].'" style="padding-left:'.($arraySp[$i1]['level']*10).'px;">'.$arraySp[$i1]['name'].'</option>'; 
                       
                      }
                     
                      $parentNodeList.="</select>";
            //-------------------------------------
            
            unset($arraySp[$j]['is_rubricator'], $arraySp[$j]['url_name'], $arraySp[$j]['id_menu_top'], $arraySp[$j]['level'], $arraySp[$j]['text']);
           
                           
            if ($this->menu->isRubr)  unset($arraySp[$j]['text']);
            
                foreach($arraySp[$j] as $key => $val ){
                 if ($key!="rubs"){
                   if ($i==0){
                        $id_record=$val;  
                       // $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>';                
                   }elseif (strrpos($key, "is_visible") === 0){                  
                          if ($val == 1) {
                             $checked = 'checked="cheked"';
                          } else {
                              $checked="";
                          }                     
                          $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="/adminajax/setIsVisible/" table="menu" nameidrec="id_menu"/></td>';                      
                    }elseif ($key=="name"){
                          $left=( ($level-1)*20)."px";
                        
                            /*if ($level>1) $class='class="tree"';
                            $tr.="<td><p $class style='margin-left:$left;'>$val</p></td>";*/
                        
                            //проверка вложенности
                            #---------------------------
                            $sth1=$this->db->prepare("SELECT * FROM ".PREFIX."_menu WHERE id_menu_top=?");
                            $sth1->bindParam(1, $id_record, PDO::PARAM_INT);
                            $sth1->execute();
                            $nChild=$sth1->rowCount();
                            
                            $dopClassOpenClose = ( in_array($id_rec, $idOpenPage)) ?'ui-icon-minusthick':'ui-icon-plusthick';
                            $plus=($nChild>0) ? '<span class="ui-icon treerubric menuplus '.$dopClassOpenClose.'"></span>' : '' ;
                            #--------------------------
                        
                        /*$tr.="<td style='padding-left:10px;'><p>$val</p>*/
                           
                         $tr.="<td class=\"rubrics menuplus\"><p style='padding-left:10px;' idrec=\"".$id_record."\" idtop=\"".$id_menu_top."\" >".$plus." ".$val."</p>   
                            
                         <input class=\"node-id\" name=\"node-$id_record-id\" value=\"$id_record\" type=\"hidden\" />
                         <input class=\"row-depth\" name=\"node-$id_record-depth\" value=\"$level\" type=\"hidden\" />
                        
                        </td>
                        <td>$positionList</td>
                        <td>$parentNodeList</td>
                        <td class='cnt'>$id_record</td>
                         ";
                       
                    }else{                    
                        $class= ( $key=="position" )? " class='cnt pos'": ""; 
                        if ($key=="url") $class=" class='url'"; 
                        $tr.="<td$class>$val</td>";     
                        if (($key=="position") && $this->menu->isRubr)  $tr.="<td class='cnt'>".$id_record."</td>"; 
                    }                                                    
                    $i++; 
                 }  
                }            
            $tr.='<td style="padding-left: 24px;">   
            <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$nameButRubricInMenu.'" href="'.$rubricMenuShow.'" id_record="'.$id_record.'">
              <span class="ui-icon ui-icon-shuffle"></span>
             </a>                 

             <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$this->lang['edit'].'" href="javascript:void(0);" action="'.$updateAct.'" act="edit" url="'.$urlPage.'" id_record="'.$id_record.'" form="'.$formShow.'" root="'.$id_menu_top.'" level="'.$level.'">
              <span class="ui-icon ui-icon-pencil" ></span>
             </a>                   
             <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$this->lang['delete'].'" href="javascript:void(0);" action="'.$deleteAct.'" act="delete" url="'.$urlPage.'" id_record="'.$id_record.'">
               <span class="ui-icon ui-icon-circle-close"></span>
            </a>   
            </td>
        </tr>';
        }  
       
        $tbody['tr']=$tr;
        return $tbody;
    }        
}

?>