<?php
class newsEdit {
    private $db, $idNews, $error;
    public $column;
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();   
    }
    
    public function getNews($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_news WHERE id_news=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $sth = $this->db->query("SELECT * FROM ".PREFIX."_news ORDER BY date desc");
            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
        }
        if($id){
         return $this->getNewsDetails($result, 1);   
        }else{ 
         return $this->getNewsDetails($result, 0);
        }
    }
    
      public function getMainTheme($id=0){
          if ($id!=0){
             $sth=$this->db->query("SELECT `name` FROM ".PREFIX."_sp_theme_news  WHERE id_sp_theme_news={$id}");
             $results = $sth->fetchAll(PDO::FETCH_ASSOC);
             return $results;
          }else{
              return "";
          }  
      }
    
    public function add($data){
       
        $data=$this->prepareInsertData($data);
        
        $this->db->beginTransaction();
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_news (id_users, date, zagolovok, annotaciya, text, comment, is_main, is_visible, is_rss, url, main_theme, keywords, description, title) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $sth->bindParam(1, $_SESSION['user']['user_id'], PDO::PARAM_INT);
        $sth->bindParam(2, $data['date'], PDO::PARAM_STR);
        $sth->bindParam(3, $data['zagolovok'], PDO::PARAM_STR);
        $sth->bindParam(4, $data['annotaciya'], PDO::PARAM_STR);
        $sth->bindParam(5, $data['text'], PDO::PARAM_STR);
        $sth->bindParam(6, $data['comment'], PDO::PARAM_STR);   
        $sth->bindParam(7, $data['is_main'], PDO::PARAM_INT);   
        $sth->bindParam(8, $data['is_visible'], PDO::PARAM_INT); 
        $sth->bindParam(9, $data['is_rss'], PDO::PARAM_INT); 
        $sth->bindParam(10, $data['url_page'], PDO::PARAM_STR); 
        $sth->bindParam(11, $data['main_theme'], PDO::PARAM_INT); 
        $sth->bindParam(12, $data['keywords'], PDO::PARAM_STR); 
        $sth->bindParam(13, $data['description'], PDO::PARAM_STR); 
        $sth->bindParam(14, $data['title'], PDO::PARAM_STR); 
        $sth->execute();        
        $this->idNews = $this->db->lastInsertId();
         $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        $err = $sth->errorInfo();
        
        $this->error = ($err[0] != '00000')?0:1;
      
        if (count($data['details'])>0) { 
            $this->addNewsDetails($data['details']); 
        }
        
       // if(!in_array(0, $this->error)){
       if ($this->error == 1){
            $this->db->commit();
            
            #-----------------
                  #predprint
                   if (Common::is_predprint("news")) Common::InsertPredPrint($this->idNews, "news", $this->db);
            
            return true;
        }else{
            $this->db->rollBack();
            return false;
        }
    }
    
    public function update($data){
        
        $data=$this->prepareInsertData($data, "edit"); 
         
        $this->db->beginTransaction();
        $sth = $this->db->prepare("UPDATE ".PREFIX."_news SET date=?, zagolovok=?, annotaciya=?, text=?, comment=?, is_main=?, is_visible=?, is_rss=?, url=?, main_theme=?, keywords=?, description=?, title=? WHERE id_news=?");        
        $sth->bindParam(1, $data['date'], PDO::PARAM_STR);
        $sth->bindParam(2, $data['zagolovok'], PDO::PARAM_STR);
        $sth->bindParam(3, $data['annotaciya'], PDO::PARAM_STR);
        $sth->bindParam(4, $data['text'], PDO::PARAM_STR);
        $sth->bindParam(5, $data['comment'], PDO::PARAM_STR);    
        $sth->bindParam(6, $data['is_main'], PDO::PARAM_INT);   
        $sth->bindParam(7, $data['is_visible'], PDO::PARAM_INT); 
        $sth->bindParam(8, $data['is_rss'], PDO::PARAM_INT); 
        $sth->bindParam(9, $data['url_page'], PDO::PARAM_STR); 
        $sth->bindParam(10, $data['main_theme'], PDO::PARAM_INT); 
        $sth->bindParam(11, $data['keywords'], PDO::PARAM_STR); 
        $sth->bindParam(12, $data['description'], PDO::PARAM_STR); 
        $sth->bindParam(13, $data['title'], PDO::PARAM_STR); 
        $sth->bindParam(14, $data['id'], PDO::PARAM_INT);
        $sth->execute();        
        $this->idNews = $data['id'];
        $err = $sth->errorInfo();
        $this->error = ($err[0] != '00000')?0:1;
        $this->addNewsDetails($data['details']);       
        
        //if(!in_array(0, $this->error)){
         if ($this->error == 1){
            $this->db->commit();
            return true;
        }else{
            $this->db->rollBack();
            #return $this->error;
            return false;
        }
    }
    
    public function delete($id){
       $arrayDel = explode(",", $id);
        
        foreach($arrayDel as $key=>$val){
            $this->idNews=$val;
            $this->clearRelNewsSp("sp_vid_news");
            $this->clearRelNewsSp("sp_theme_news");
            $this->clearRelNewsSp("sp_education");
            $this->clearRelNewsSp("sp_organasation");
            $this->clearRelNewsSp("person");
            
            //-------------------------------------------------------------------
             $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_users_pred_print WHERE id_news=?");
             $sth->bindParam(1, $this->idNews, PDO::PARAM_INT);
             $sth->execute();
                
        } 
        
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_news WHERE id_news IN ($place_holders)");
        $sth->execute($arrayDel);
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
        
    }
#---------------------------------------------------------------------------------------------------------------------------------------------------    
    private function getNewsDetails($news, $mode=0){        
       
       //-----------------------------------------------------------------------------------------
       if ($mode==0){           
           foreach($news as $key=>$value){
            $details = array();
            $sth=$this->db->query("
                SELECT vn.`name`, vn.`id_sp_vid_news` as 'id', 'sp_vid_news' as 'vid' FROM ".PREFIX."_rel_news_sp_vid_news rvn
                        left join ".PREFIX."_sp_vid_news vn using (id_sp_vid_news)
                WHERE rvn.id_news={$value['id_news']}
                union all
                SELECT tn.`name`, tn.`id_sp_theme_news` as 'id', 'sp_theme_news' as 'vid' FROM ".PREFIX."_rel_news_sp_theme_news rtn
                        left join ".PREFIX."_sp_theme_news tn using (id_sp_theme_news)
                WHERE rtn.id_news={$value['id_news']}
                union all
                SELECT e.`name`, e.`id_sp_education` as 'id', 'sp_education' as 'vid' FROM ".PREFIX."_rel_news_sp_education re
                        left join ".PREFIX."_sp_education e using (id_sp_education)
                WHERE re.id_news={$value['id_news']}
                    
                union all
                SELECT p.`fio`, p.`id_person` as 'id', 'person' as 'vid' FROM ".PREFIX."_rel_news_person rp
                        left join ".PREFIX."_person p using (id_person)
                WHERE rp.id_news={$value['id_news']}    
                    
                union all
                SELECT o.`name`, o.`id_sp_organasation` as 'id', 'sp_organasation' as 'vid' FROM ".PREFIX."_rel_news_sp_organasation ro
                        left join ".PREFIX."_sp_organasation o using (id_sp_organasation)
                WHERE ro.id_news={$value['id_news']}");
            $results = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach($results as $k=>$result){
                $details[$result['vid']][$result['id']] = $result['name'];
            }
            $news[$key]['details'] = $details;
        } 
       //----------------------------------------------------------------------------------------
      }else if ($mode==1){
          $details = array();
          $id=$news['id_news'];
          $sth=$this->db->query("
                SELECT vn.`name`, vn.`id_sp_vid_news` as 'id', 'sp_vid_news' as 'vid' FROM ".PREFIX."_rel_news_sp_vid_news rvn
                        left join ".PREFIX."_sp_vid_news vn using (id_sp_vid_news)
                WHERE rvn.id_news={$id}
                union all
                SELECT tn.`name`, tn.`id_sp_theme_news` as 'id', 'sp_theme_news' as 'vid' FROM ".PREFIX."_rel_news_sp_theme_news rtn
                        left join ".PREFIX."_sp_theme_news tn using (id_sp_theme_news)
                WHERE rtn.id_news={$id}
                union all
                SELECT e.`name`, e.`id_sp_education` as 'id', 'sp_education' as 'vid' FROM ".PREFIX."_rel_news_sp_education re
                        left join ".PREFIX."_sp_education e using (id_sp_education)
                WHERE re.id_news={$id}
                    
                union all
                SELECT p.`fio`, p.`id_person` as 'id', 'person' as 'vid' FROM ".PREFIX."_rel_news_person rp
                        left join ".PREFIX."_person p using (id_person)
                WHERE rp.id_news={$id}    
                    
                union all
                SELECT o.`name`, o.`id_sp_organasation` as 'id', 'sp_organasation' as 'vid' FROM ".PREFIX."_rel_news_sp_organasation ro
                        left join ".PREFIX."_sp_organasation o using (id_sp_organasation)
                WHERE ro.id_news={$id}");
            $results = $sth->fetchAll(PDO::FETCH_ASSOC);
            foreach($results as $k=>$result){
                $details[$result['vid']][$result['id']] = $result['name'];
            }
            $news['details'] = $details;
      }
      //----------------------------------------------------------------------------------------
        return $news;
    }
    
    private function addNewsDetails($details){
         foreach($details as $sp=>$detail){
            $this->clearRelNewsSp($sp);
            foreach($detail as $key=>$val){
                $sth = $this->db->prepare("INSERT INTO ".PREFIX."_rel_news_{$sp} (id_news, id_{$sp}) VALUES (?, ?)");
                $sth->bindParam(1, $this->idNews, PDO::PARAM_INT);
                $sth->bindParam(2, $val, PDO::PARAM_INT);
                $sth->execute();
                $err = $sth->errorInfo();
                $this->error = ($err[0] != '00000')?0:1;
           }
        }
    }
    
    private function clearRelNewsSp($sp){
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_rel_news_{$sp} WHERE id_news=?");
        $sth->bindParam(1, $this->idNews, PDO::PARAM_INT);
        $sth->execute();
    }
    #----------------------------------------------------------------------------------------------  
     public function prepareInsertData($data, $type="add"){
        
        $data['date'] = Common::getTimeConversionInsertDB($data['date']);
        $data['url_page'] = ($data['url_page']=="")? Common::createURL($data['zagolovok']) : $data['url_page'];
        #------------------------------------------------------------------------
        //уникальность url 
        if ($type=="add"){
            $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_news  WHERE url=?");
            $sth->bindParam(1, $data['url_page'], PDO::PARAM_STR);
        }else if ($type=="edit"){    
             $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_news  WHERE id_news!=? and url=?");
             $sth->bindParam(1, $data['id'], PDO::PARAM_STR);
             $sth->bindParam(2, $data['url_page'], PDO::PARAM_STR); 
        }    
            $sth->execute();
            //$err = $sth->errorInfo();
            $count = $sth->rowCount();
           
            if ( $count > 0){
              $i=$count+1;   
              if (strrpos($data['url_page'], ".html")){
                  $length =  strlen($data['url_page'])-5;
                  $temp   =  substr($data['url_page'], 0, $length);
                  $data['url_page']=$temp."-".$i.".html";
                  
              }else{
                  $data['url_page']=$data['url_page']."-".$i;
              }
            }
         #-----------------------------------------------------------------------  
                
        $data['text']= ($data['text']!="")? $data['text']:$data['annotaciya'];
        $data['title']= ($data['title']!="")? $data['title']:$data['zagolovok'];
        
        
        
        //----------------------------------------------------------------------
        $data['details'] = array();
       
        #------- 
        if ( count($data['sp_vid_news'])>0 ) {
            if ( in_array("0", $data['sp_vid_news']) ){
                unset($data['sp_vid_news'][0]);
             }
            $data['details']['sp_vid_news']=$data['sp_vid_news'];
        }
        #-------
        if ( count($data['sp_theme_news'])>0 ) {
            if ( in_array("0", $data['sp_theme_news']) ){
            unset($data['sp_theme_news'][0]);
            }
            $data['details']['sp_theme_news']=$data['sp_theme_news'];
        }
        #------
        if ( count($data['sp_education'])>0 ) {
            if ( in_array("0", $data['sp_education']) ){
            unset($data['sp_education'][0]);
            }
            $data['details']['sp_education']=$data['sp_education'];
        }
        #-----
        if ( count($data['sp_organasation'])>0 ) {
            if ( in_array("0", $data['sp_organasation']) ){
            unset($data['sp_organasation'][0]);
            }
            $data['details']['sp_organasation']=$data['sp_organasation'];
        }
        #----
        if ( count($data['person'])>0 ) {
            if ( in_array("0", $data['person']) ){
            unset($data['person'][0]);
            }
            $data['details']['person']=$data['person'];
        }
        #----
        //-----------------------------------------------------------------------
       
        if ( count($data['sp_theme_news'])==1 ){
           $data['main_theme'] = ($data['main_theme']=="0")? $data['sp_theme_news'][0] : $data['main_theme'];
        }
        
        
        //clear \r\n

        $data['text']=  Common::removingCharacter($data['text']);
        $data['comment']=  Common::removingCharacter($data['comment']);
        
        
        return $data;
    }
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id, $act, $action, $url, $lang){
       
         if ($act=="edit")  {
             $news = $this->getNews($id);
             $news = Common::removeStipsSlashes($news);
         }    
        
         $id_user = $_SESSION['user']['user_id']; //id редактора новости
         
         //print_r($news[0]['details']);
        
         //---------------------------------------------------------------------
         $theme= new spEdit("theme_news");
         $themeList=$theme->select();
         $options = array();
         $options_t = array();
         $id_theme = array();
         $options[0]=$lang['no_main_theme'];
         $options_t[0]="";//$lang['news_no_theme'];
         for($j=0; $j<count($themeList); $j++) {
            $value=$themeList[$j]['name'];
            $key=$themeList[$j]['id_sp_theme_news'];
                       
            if ( ($act=="edit") && count($news['details']['sp_theme_news'])>0 ) {
                if ( in_array($value, $news['details']['sp_theme_news']) ) array_push($id_theme, $key);
            }    
           
            $options[$key]=$value; 
            $options_t[$key]=$value; 
         }
         //---------------------------------------------------------------------
         
         //---------------------------------------------------------------------
         $vid=new spEdit("vid_news");
         $vidList=$vid->select();
         $options_vid = array();
         $options_vid[0] = ""; //$lang['news_no_vid'];
         $id_vid = array();
         for($j=0; $j<count($vidList); $j++) {
            $value=$vidList[$j]['name'];
            $key=$vidList[$j]['id_sp_vid_news'];
            if ( ($act=="edit") && count($news['details']['sp_vid_news'])>0 ) {
                if ( in_array($value, $news['details']['sp_vid_news']) ) array_push($id_vid, $key);
            }    
             $options_vid[$key]=$value;   
         }
         //---------------------------------------------------------------------
         
         //---------------------------------------------------------------------
         $educationCl = new spEdit("education");
         $edList=$educationCl->select();
         $options_ed = array();
         $options_ed[0] = ""; //$lang['news_no_vid'];
         $id_ed = array();
         for($j=0; $j<count($edList); $j++) {
            $value=$edList[$j]['name'];
            $key=$edList[$j]['id_sp_education'];
           if ( ($act=="edit") && count($news['details']['sp_education'])>0 ) {
               if ( in_array($value, $news['details']['sp_education']) ) array_push($id_ed, $key);
           }    
            $options_ed[$key]=$value;   
         }
         //---------------------------------------------------------------------
         
         //---------------------------------------------------------------------
         $organization=new spEdit("organasation");
         $orgList=$organization->selectNews();
         $options_org = array();
         $options_org[0] = ""; //$lang['news_no_vid'];
         $id_org = array();
         for($j=0; $j<count($orgList); $j++) {
            $value=$orgList[$j]['name'];
            $key=$orgList[$j]['id_sp_organasation'];
           if ( ($act=="edit") && count($news['details']['sp_organasation'])>0) { 
               if ( in_array($value, $news['details']['sp_organasation']) ) array_push($id_org, $key);
               
           }
            $options_org[$key]=$value;   
         }
         //---------------------------------------------------------------------
         
         //---------------------------------------------------------------------
         $persona=new PersonEdit();
         $personList=$persona->getPersonsNews();
         $options_per = array();
         $options_per[0] = ""; //$lang['news_no_vid'];
         $id_person = array();
         for($j=0; $j<count($personList); $j++) {
            $value=$personList[$j]['fio'];
            $key=$personList[$j]['id_person'];
            if ( ($act=="edit") && count($news['details']['person'])>0 ) {
                if ( in_array($value, $news['details']['person']) ) array_push($id_person, $key);
            }
            $options_per[$key]=$value;   
         }
         //---------------------------------------------------------------------
         
         
         
         $size=41; 
         $size2=123; 
         $class='sm0'; //класс списка
         $classForwars='sm0'; //класс списка
         $paramTextField['class']="creditor";
         $paramTextField['id']="creditor_id";
         $paramTextField['column']="columns-2"; //определяем поле в правую колонку формы, форма получиться двух-колоночная
         
         $paramTextField2['class']="creditor";
         $paramTextField2['id']="creditor_id2";
         $paramTextField2['column']="columns-2";
         
         $paramTextFieldDefinition['column']="columns-2";
         $parametersDate['id']="datepicker";        
         
         $flg = (($act=="add") || ($news['is_main']=="1"))?true:false;     
         $flg2 = (($act=="add") || ($news['is_visible']=="1"))?true:false; 
         $flg3 = (($act=="add") || ($news['is_rss']=="1"))?true:false; 
          
         if ($act=="edit") $dateAdd =  Common::getTimeConversionForm($news['date']);
         
         
         if ($act=="add"){
             $news['url']="";
             $news['main_theme'] = "";
             $news['zagolovok'] = "";
             $news['annotaciya'] = "";
             $news['text'] = "";
             $news['definition'] = "";
             $news['comment'] = "";
             $news['title'] = "";
             $news['description'] = "";        
             $news['keywords'] = "";
             
         }
       
         $date =          new field_text("date", $lang['date_add'], true, $dateAdd, "", 39, $parametersDate);         
         $url_page =      new field_text("url_page", $lang['news_url_page'], false, $news['url'], "", $size);         
         $is_main =       new field_checkbox("is_main", $lang['news_is_main'], $flg, $paramTextFieldDefinition1);   
         $is_visible =    new field_checkbox("is_visible", $lang['is_visible'], $flg2, $paramTextFieldDefinition1);
         $is_rss =    new field_checkbox("is_rss", $lang['is_rss'], $flg3, $paramTextFieldDefinition1);
                
         $main_theme =    new field_select("main_theme", $lang['news_main_theme'], $options, $news['main_theme'], false, "", "", "", "", $class); 
         $theme =         new field_select("sp_theme_news", $lang['news_theme'], $options_t, $id_theme, true, "5", "", "", "", $class); 
         $vid =           new field_select("sp_vid_news", $lang['news_vid'], $options_vid, $id_vid, true, "5", "", "", "", $class); 
         $education =     new field_select("sp_education", $lang['news_education'], $options_ed, $id_ed, true, "5", "", "", "", $class); 
         $organasation =  new field_select("sp_organasation", $lang['news_organisation'], $options_org, $id_org, true, "5", "", "", "", $class); 
         $person =        new field_select("person", $lang['news_person'], $options_per, $id_person, true, "5", "", "", "", $class); 
         
         $zagolovok =     new field_text("zagolovok", $lang['news_zagolovok'], true, $news['zagolovok'], "", $size2, $paramTextFieldDefinition);         
         $annotaciya =    new field_textarea("annotaciya", $lang['news_annotaciya'], true, $news['annotaciya'], 93, 6, "", "", "", $paramTextFieldDefinition); 
         $text =          new field_textarea("text", $lang['news_text'], false, $news['text'], 38, 10, "", "", "", $paramTextField); 
         $comment =        new field_textarea("comment", $lang['news_comment'], false, $news['comment'], 38, 10, "", "", "", $paramTextField2); 
         
         $title =         new field_text("title", $lang['title'], false, $news['title'], "", $size2, $paramTextFieldDefinition);         
         $description =   new field_textarea("description", $lang['meta_description'], false, $news['description'], 93, 2, "", "", "", $paramTextFieldDefinition); 
         $keywords =      new field_text("keywords", $lang['meta_keywords'], false, $news['keywords'], "", $size2, $paramTextFieldDefinition);         
        
         $id_user =       new field_hidden_int("id_user", false, $id_user);
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
          
        $form = new form(array("date" => $date,
                               "url_page" => $url_page,
                               "sp_theme_news" => $theme,
                               "main_theme" => $main_theme,
                               "sp_vid_news" => $vid,
                               "sp_education" => $education,
                               "sp_organasation" => $organasation, 
                               "person" => $person, 
                               "is_visible" => $is_visible,
                               "is_main" => $is_main, 
                               "is_rss" => $is_rss, 
                               "zagolovok"=>$zagolovok,
                               "annotaciya" => $annotaciya,    
                               "text"   => $text,
                               "comment" => $comment,
                                "title" => $title,
                               "description" => $description,
                               "keywords" => $keywords,
                               "id" => $id_rec,
                               "url" => $url,
                               "id_user" => $id_user,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
        
     }
     //form
     #---------------------------------------------------------------------------------------------
     public function getCountNewsFromTo($from, $to){  
         
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_news WHERE date > ? and date < ?");
            $sth->bindParam(1, $from, PDO::PARAM_STR);
            $sth->bindParam(2, $to, PDO::PARAM_STR);
            $sth->execute();
          
            
           return $sth->rowCount();
         #   print_r($sth->fetchAll(PDO::FETCH_ASSOC));
           
    }
}

?>