<?php
class ConstructorConfigServer extends ConstructorElements {
    private $lang;
  
    public function __construct($lang) {
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
        return '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['server_config'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        return  '';
     
    }
    
    public function getTableTbody($urlPage){
       return '';
    }  
            
}

?>