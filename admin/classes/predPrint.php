<?php
class predPrint {
    private $db, $user, $error;
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();   
        $this->user=$_SESSION['user']['user_id'];
    }
    
    public function getPrintResurs($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_rel_users_pred_print WHERE id_rel_users_pred_print=?");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
        }else{
            $result=array();
            $sth=$this->db->prepare("SELECT 
                                      *
                                        FROM ".PREFIX."_rel_users_pred_print
                                        WHERE id_users=? and is_done='0'");
      
            $sth->bindParam(1, $this->user, PDO::PARAM_INT);
            $sth->execute();
            $results_0 = $sth->fetchAll(PDO::FETCH_ASSOC);
            $count = $sth->rowCount();
            $i=0;
     
            for ($j=0; $j<$count; $j++){
                $pr=$results_0[$j]['prioritet']+1;
                                
                $sth1=$this->db->prepare("SELECT 
                                                *
                                                 FROM ".PREFIX."_rel_users_pred_print
                                                 WHERE id_data=? and id_news=? and prioritet=? and is_done='0'");
                $sth1->bindParam(1, $results_0[$j]['id_data'], PDO::PARAM_INT);
                $sth1->bindParam(2, $results_0[$j]['id_news'], PDO::PARAM_INT);
                $sth1->bindParam(3, $pr, PDO::PARAM_INT);
                $sth1->execute();
                //$sth1->fetchAll(PDO::FETCH_ASSOC)
                $countTemp = $sth1->rowCount();
                if ($countTemp==0){
                        array_push($result, $results_0[$j]);
                }        
            }
     
        }
        
        
        return $result;
    }
    
    
     public function getPrintResursAdmin(){
            $result=array();
            $sth=$this->db->prepare("SELECT 
                                      *
                                        FROM ".PREFIX."_rel_users_pred_print
                                        WHERE id_users!=? and is_done='0'");
            $sth->bindParam(1, $this->user, PDO::PARAM_INT);
            $sth->execute();
            $results_0 = $sth->fetchAll(PDO::FETCH_ASSOC);
            $count = $sth->rowCount();
            $i=0;
            for ($j=0; $j<$count; $j++){
                $pr=$results_0[$j]['prioritet']+1;
                                
                $sth1=$this->db->prepare("SELECT 
                                                *
                                                 FROM ".PREFIX."_rel_users_pred_print
                                                 WHERE id_data=? and id_news=? and prioritet=? and is_done='0'");
                $sth1->bindParam(1, $results_0[$j]['id_data'], PDO::PARAM_INT);
                $sth1->bindParam(2, $results_0[$j]['id_news'], PDO::PARAM_INT);
                $sth1->bindParam(3, $pr, PDO::PARAM_INT);
                $sth1->execute();
                //$sth1->fetchAll(PDO::FETCH_ASSOC)
                $countTemp = $sth1->rowCount();
                if ($countTemp==0){
                        array_push($result, $results_0[$j]);
                }        
            }
             
        return $result;
    }
    
    
    
    
    public function update($data){
        
        if ( $data['act_type']['0']=="0"){ //только сохранить комментарий
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_rel_users_pred_print SET comment=?, date_update=? WHERE id_rel_users_pred_print=?");        
            $sth->bindParam(1, $data['comment'], PDO::PARAM_STR);
            $sth->bindParam(2, date("Y-m-d H:i:s"), PDO::PARAM_STR);
            $sth->bindParam(3, $data['id'], PDO::PARAM_INT);
            
        }else if ( $data['act_type']['0']=="1"){ //отменить и вернуть предыдущему редактору
            $is_done=0;
            $prioritet=$data['prioritet']+1;
            
            #-------------------------------------------------------------------
            $sth1 = $this->db->prepare("UPDATE ".PREFIX."_rel_users_pred_print SET is_done=? WHERE id_data=? and id_news=? and prioritet=?");        
            $sth1->bindParam(1, $is_done, PDO::PARAM_INT);
            $sth1->bindParam(2, $data['id_data'], PDO::PARAM_INT);
            $sth1->bindParam(3, $data['id_news'], PDO::PARAM_INT);
            $sth1->bindParam(4, $prioritet, PDO::PARAM_INT);
            $sth1->execute();  
            #-------------------------------------------------------------------
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_rel_users_pred_print SET comment=?, date_update=? WHERE id_rel_users_pred_print=?");        
            $sth->bindParam(1, $data['comment'], PDO::PARAM_STR);
            $sth->bindParam(2, date("Y-m-d H:i:s"), PDO::PARAM_STR);
            $sth->bindParam(3, $data['id'], PDO::PARAM_INT);
            
            
        }else if ( $data['act_type']['0']=="2"){ //утвердить
            $is_done=1;
            
            $sth = $this->db->prepare("UPDATE ".PREFIX."_rel_users_pred_print SET comment=?, date_update=?, is_done=? WHERE id_rel_users_pred_print=?");        
            $sth->bindParam(1, $data['comment'], PDO::PARAM_STR);
            $sth->bindParam(2, date("Y-m-d H:i:s"), PDO::PARAM_STR);
            $sth->bindParam(3, $is_done, PDO::PARAM_INT);
            $sth->bindParam(4, $data['id'], PDO::PARAM_INT);
            
        }
        
        
        $sth->execute();        
        $err = $sth->errorInfo();
        return ($err[0] != '00000')?false:true;
       
    }
    
    public function emptyResursAdmin($id){
       if ($id){ 
             
            $info=$this->getPrintResurs($id);
            
            $isDn=1;
            $sth = $this->db->prepare("UPDATE ".PREFIX."_rel_users_pred_print SET is_done=? WHERE id_data=? and id_news=?");        
            $sth->bindParam(1, $isDn, PDO::PARAM_INT);
            $sth->bindParam(2, $info['id_data'], PDO::PARAM_INT);
            $sth->bindParam(3, $info['id_news'], PDO::PARAM_INT);
            $sth->execute();        
            $err = $sth->errorInfo();
            return ($err[0] != '00000')?false:true;
       }else{
           return false;
       }
    }
   
    #-----------------------------------------------------------------------------------------------
    //form
    /*
     * $id - id-записи
     * $act - действие удалить, добавить, редактировать
     * $action - url действия
     * $url - url страницы возврата после выполнения действия 
     * $lang - языковый массив 
     */
    public function showForm($id, $act, $action, $url, $lang){
      
        if ($act=="edit")  {  
         $print = $this->getPrintResurs($id);
         $print = Common::removeStipsSlashes($print);
        } 
        
        $id_user = $_SESSION['user']['user_id']; //id пользователя
        
        $typeAction= array();
        $typeAction[0]=$lang['print_act_type1'];
         
        if ( $this->getCountUserPrintNextandPrev($id_user, "prev")!="0"){  //есть предыдущие пользователи
           
             $typeAction[1]=$lang['print_act_type2'];
             $typeAction[2]=$lang['print_act_type3_end'];
        }else if ( $this->getCountUserPrintNextandPrev($id_user, "next")!="0"){ 
             $typeAction[2]=$lang['print_act_type3'];
        }else{
             $typeAction[1]=$lang['print_act_type2'];
             $typeAction[2]=$lang['print_act_type3'];   
        }
        #-----------------------------------------------------------------------         
        
        //сбор комментов всех пользователей
        $commetRedactor=$this->getCommentUserPrintPrev($print['id_data'], $print['id_news'], $print['prioritet'], $lang) ;
        print_r($commetRedactor);
        
        
         $typeActParametr = array();
         $typeActParametr['class']="art_type";
         
         $act_type =  new field_radio("act_type", $lang['print_act_type'], $typeAction,  "0", $typeActParametr); 
         
         
         $comment =    new field_textarea("comment", $lang['print_comment'], true, $print['comment'], 70, 8, "", "", "", ""); 
         
         $id_users =      new field_hidden_int("id_user", false, $id_user);
         $id_data =       new field_hidden_int("id_data", false, $print['id_data']);
         $id_news =       new field_hidden_int("id_news", false, $print['id_news']);
         $prioritet =     new field_hidden_int("prioritet", false, $print['prioritet']);
         $id_rec =        new field_hidden_int("id", false, $id);  
         $url =           new field_hidden_int("url", false, $url);
          
        $form = new form(array("comment" => $comment,
                               "act_type" => $act_type, 
                               "id_data" => $id_data,
                               "id_news" => $id_news,
                               "id" => $id_rec,
                               "url" => $url,
                               "id_user" => $id_users,
                               "prioritet" => $prioritet,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
       
     }
     //form
     #---------------------------------------------------------------------------------------------
     public function showFormAdmin($id, $lang){
      
         $print = $this->getPrintResurs($id);
         $print = Common::removeStipsSlashes($print);
        
        //сбор комментов всех пользователей
         $commetRedactor=$this->getCommentUserPrintPrev($print['id_data'], $print['id_news'], $print['prioritet'], $lang) ;
         print_r($commetRedactor);
     }
     
     
     
     
     #------------------------------------------------------------------------------  
     public function getCountUserPrintNextandPrev($id_user, $go) {
        $sth=$this->db->prepare("SELECT 
                                *
                                    FROM ".PREFIX."_rel_users_pred_print
                                    WHERE id_users=?");
      
        $sth->bindParam(1, $id_user, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        $prioritet = $result['prioritet'];
        
        $prioritetNew =  ($go == "next") ? ($prioritet-1) : ($prioritet+1);
        $is_done = ($go == "next") ? 0 : 1;
        
        $sth1=$this->db->prepare("SELECT 
                                *
                                    FROM ".PREFIX."_rel_users_pred_print
                                    WHERE id_data=? and id_news=? and prioritet=? and is_done=?");
     
         $sth1->bindParam(1, $result['id_data'], PDO::PARAM_INT);
         $sth1->bindParam(2, $result['id_news'], PDO::PARAM_INT);
         $sth1->bindParam(3, $prioritetNew, PDO::PARAM_INT);
         $sth1->bindParam(4, $is_done, PDO::PARAM_INT);
         $sth1->execute();
         $rowCountTemp = $sth1->rowCount();
    
      
    return $rowCountTemp;
   
 }
 #------------------------------------------------------------------------------ 
 
 #------------------------------------------------------------------------------  
     public function getCommentUserPrintPrev($id_data, $id_news, $prioritet, $lang) {
         $prioritetStart=$prioritet+1;
                 
         $sth=$this->db->prepare("SELECT 
                                    *
                                    FROM ".PREFIX."_rel_users_pred_print
                                    WHERE id_data=? and id_news=? and prioritet !=? and comment!=''  ORDER BY prioritet");
         //and prioritet >= ? and is_done='1' 
      
         $sth->bindParam(1, $id_data, PDO::PARAM_INT);
         $sth->bindParam(2, $id_news, PDO::PARAM_INT);
         $sth->bindParam(3, $prioritet, PDO::PARAM_INT);
         $sth->execute();
         $results = $sth->fetchAll(PDO::FETCH_ASSOC);
         $rowCount = $sth->rowCount();
         $return = '<b>'.$lang['print_comment_redactor'].'</b>';
         if ($rowCount==0){
             $return.='<p>'.$lang['print_no_comment'].'</p>';
         }else{
           for($i=0; $i<$rowCount; $i++){
                $id=$results[$i]['id_users'];
                $user=new userEdit();
                $info = $user->getUser($id); 
                $comment = $results[$i]['comment'];
                $date_up = Common::getTimeConversion($results[$i]['date_update'], $lang);
             
                if ( ($info['fam']!="") || ($info['name']!="")){
                    $fam= '('.$info['fam'].' '.$info['name'].' '.$info['otch'].')';
                }   
             
             $return.='<p>'.$lang['login'].' <b>'.$info['login'].'</b>'.$fam.',  '.$lang['print_comment_update'].' '.$date_up.'<br/>
             <u><i>'.$lang['text_comment'].'</i></u>: '.$comment.'<br/>
             -----------------------------------------------------------------------------------------------------------------------------
            </p>';
             
            }
         }    
         
    
      
    return $return;
   
 }
 #------------------------------------------------------------------------------ 

 #------------------------------------------------------------------------------  
  public function  getCountPredPrintAdmin() {
     $pr=1;
     
     $sth=$this->db->prepare("SELECT 
                                *
                                    FROM ".PREFIX."_rel_users_pred_print
                                    WHERE prioritet=? and is_done='0'");
      
     $sth->bindParam(1, $pr, PDO::PARAM_INT);
     $sth->execute();
     $count = $sth->rowCount();
     return $count;
 }
 #------------------------------------------------------------------------------ 
 
 
     
}

?>