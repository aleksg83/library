<?php
class ConstructorRassilkaSendAction extends ConstructorElements {
    private $ras;
    private $lang;
    private $id;
    
    public function __construct(rassilkaSendEdit $ras, $id, $lang) {
        $this->ras = $ras;
        $this->lang = $lang;
        $this->id = $id;
        $sql = new Sql();
        $this->db = $sql->connect();
    }
    
    public function getValidateForm(){
        return '';
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $names = $this->lang['rassilka_action'];        
        $path =  '<a href="/admin/rassilkaSend/" title="'.$this->lang['rassilka_send_page'].'">'.$this->lang['rassilka_send_page'].'</a>';   
        
        $breadcrumbs['name']=  $names;
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > '.$path.' >'.$names.'</span>';        
        
        return $breadcrumbs;
    }
    
    public function getTableHead(){
         return  '';
    }
    
    public function getTableTbody($urlPage){
        
        $tbody = Array();
        $setting=$this->ras->getRassulkaSetting();
        $rassilka=$this->ras->getRassilka($this->id);
        $user=new newsSendUserEdit();
        $listSender=$user->getUsersSender();
        $n=count($listSender);
        
        $list.='<div class="blockSendSet">';
        $list.='<p>'.$this->lang['rassilka_send_tema'].' - <span class="bold">"'.$rassilka['tema'].'</span>".</p>';
        $list.='<p>'.$this->lang['rassilka_setting_kolvo_adress'].' - <span class="bold">'.$n.'</span>.</p>';
        $list.='<p><u>'.$this->lang['rassilka_setting'].'</u>:</p>';
        $list.='<p>'.$this->lang['rassilka_setting_name_author'].' - <span class="bold">'.$setting['name_author'].'</span>, '.$this->lang['rassilka_setting_adress_author'].' - <span class="bold">'.$setting['adress_author'].'</span>. </p>';
        
        $_SESSION['send_user_ok']=0;
        $_SESSION['send_user_error']=0;
        
        if ($setting['type']=="1"){
            $list.='<p>'.$this->lang['rassilka_setting_text'].' - <span class="bold">'.$this->lang['rassilka_setting_type0'].'</span>';
        }elseif ($setting['type']=="2"){
            
            if ($setting['kolvo_pisem']>=$setting['kolvo_addr']){
                $limit=$setting['kolvo_addr'];
                $countUserPacet=$setting['kolvo_addr'];
            }elseif ($setting['kolvo_pisem']<$setting['kolvo_addr']){
                $limit=$setting['kolvo_pisem'];
                $countUserPacet=$setting['kolvo_pisem'];
            }  
           
            $countPacet= ceil($n / $limit);
            //$countUserPacet= floor($n / $countPacet);
            
            $list.='<p>'.$this->lang['rassilka_setting_text'].' - <span class="bold">'.$this->lang['rassilka_setting_type1'].'</span> ('.$this->lang['rassilka_setting_time_paket'].' - <span class="bold">'.$setting['time_paket'].'</span>; '.$this->lang['rassilka_setting_kolvo_pisem'].' - <span class="bold">'.$setting['kolvo_pisem'].'</span>; '.$this->lang['rassilka_setting_kolvo_adress'].' -  <span class="bold">'.$setting['kolvo_addr'].'</span>)';
            $list.='<p>'.$this->lang['rassilka_setting_kolvo_paket'].' - <span class="bold">'.$countPacet.'</span>';
        }
        
        $list.='<p>
            
           <a class="btn ui-state-default but1" href="javascript:history.back();">
            <span class="ui-icon ui-icon-arrowstop-1-w"></span>
            '.$this->lang['cancel'].'
            </a>

            <a class="btn ui-state-default but1 senderRassilka" id_record="'.$this->id.'" type="'.$setting['type'].'" time_packet="'.$setting['time_paket'].'" kolvo_pisem="'.$setting['kolvo_pisem'].'" kolvo_addr="'.$setting['kolvo_addr'].'" countPacet="'.$countPacet.'" countUserPacet="'.$countUserPacet.'" href="javascript:void(0);">
            <span class="ui-icon ui-icon-arrowstop-1-e"></span>
             '.$this->lang['rassilka_action2'].'
            </a>
            </p>';
        
         $list.='</div>
             <div class="listInformSender" style="display:none;">
             <p>'.$this->lang['rassilka_action3'].'</p> 
            </div>
            <p style="margin-top:10px; display:none;" class="button_next"><a class="btn ui-state-default but1 senderRassilka" href="'.SITE_URL.'admin/rassilkaSend/">
            <span class="ui-icon ui-icon-arrowstop-1-e"></span>
             Продолжить
            </a>
            </p>


             ';
             
        $tbody['text-div']=$list;
        
        return $tbody;
    }    
}
?>