<?php
class staticEdit {
    private $db, $page=array(), $idNextNode, $level = 0, $err;
        
    public function __construct() {
        $sql = new Sql();
        $this->db = $sql->connect();
        $this->nextNode=true;
    }
    
    public function showStatic($id=null){
        if($id){
            $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE id_static_page=? ORDER BY position");
            $sth->bindParam(1, $id, PDO::PARAM_INT);
            $sth->execute();
            $this->page = $sth->fetch(PDO::FETCH_ASSOC);
            //$this->menu['rubr'] = $this->getMenuRubr($id);
            #echo "<pre>"; print_r($this->page); echo "</pre>"; exit;
        }else{
              $this->getNextPage();
        }
        #echo "<pre>"; print_r($this->page); echo "</pre>"; exit;
        return $this->page;
    }
     
   
    public function add($data){
        $data=$this->prepareInsertData($data);

        $n=0;
        $sth = $this->db->prepare("INSERT INTO ".PREFIX."_static_page (id_static_page_top, id_menu, name, url, annotaciya, text, keywords, description, title, id_users, is_visible, is_menu, date_add, is_folders, position, level, link_out, id_page_forwars) VALUES (:id_static_page_top, :id_menu, :name, :url, :annotaciya, :text, :keywords, :description, :title, :id_users, :is_visible, :is_menu, :date_add, :is_folders, :position, :level, :link_out, :id_page_forwars)");
        $sth->bindParam(":id_static_page_top", $data['id_page_top'], PDO::PARAM_INT);
        $sth->bindParam(":id_menu", $data['id_menu'], PDO::PARAM_INT);
        $sth->bindParam(":name", $data['name'], PDO::PARAM_STR);
        $sth->bindParam(":url", $data['url_page'], PDO::PARAM_STR);
        $sth->bindParam(":annotaciya", $data['annotaciya'], PDO::PARAM_STR);
        $sth->bindParam(":text", $data['text'], PDO::PARAM_STR);
        $sth->bindParam(":keywords", $data['keywords'], PDO::PARAM_STR);
        $sth->bindParam(":description", $data['description'], PDO::PARAM_STR);
        $sth->bindParam(":title", $data['title'], PDO::PARAM_STR);
        $sth->bindParam(":id_users", $_SESSION['user']['user_id'], PDO::PARAM_INT);
        $sth->bindParam(":is_visible", $data['is_visible'], PDO::PARAM_INT);
        $sth->bindParam(":is_menu", $data['is_menu'], PDO::PARAM_INT);
        $sth->bindParam(":date_add", $data['date_add'], PDO::PARAM_STR);
        $sth->bindParam(":is_folders", $n, PDO::PARAM_INT);
        $sth->bindParam(":position", $data['position'], PDO::PARAM_INT);
        $sth->bindParam(":level", $data['level'], PDO::PARAM_INT);
        $sth->bindParam(":link_out", $data['link_out'], PDO::PARAM_STR);
        $sth->bindParam(":id_page_forwars", $data['id_page_forwars'], PDO::PARAM_INT);
      
        $sth->execute();
        $err = $sth->errorInfo();
         $_SESSION['setting_gurnal_last_insert_id']=$this->db->lastInsertId();
        
        if ($err[0] == '00000') { //запись добавлена, надо обработать родительский ресурс
            
            if (intval($data['id_page_top']) > 0) { //находим родительский ресурс
                
                $parent = $this->showStatic($data['id_page_top']);
            
                if ( ($parent['is_folders']=="0") ||  (strrpos($parent['url'], ".html")>0) ) {
                 
                 $length =  strlen($parent['url'])-5;
                 $temp   =  substr($parent['url'], 0, $length);
               
                               
                 $n=1;
                 $sth = $this->db->prepare("UPDATE ".PREFIX."_static_page SET url=?, is_folders=? WHERE id_static_page=?");        
                 $sth->bindParam(1, $temp, PDO::PARAM_STR);
                 $sth->bindParam(2, $n, PDO::PARAM_INT);
                 $sth->bindParam(3, $parent['id_static_page'], PDO::PARAM_INT);
                 $sth->execute(); 
                 
                 //$err = $sth->errorInfo();
                }
                
            }
            
        }
        
     
        return ($err[0] != '00000')?false:true;
       
    }
    
    public function update($data){
       $data=$this->prepareInsertData($data, "edit");

       //проверка изменился ли родитель
        $tmp=$this->showStatic($data['id']);
        if ($tmp['id_static_page_top']!=$data['id_page_top']){
         //у всех потомкков надо будет изменить уровень level
         $change=true;
         $id_top_star_page=$tmp['id_static_page_top']; //id прежнего родителя
        }
        
        $sth = $this->db->prepare("UPDATE ".PREFIX."_static_page SET id_static_page_top=?, id_menu=?, name=?, url=?, annotaciya=?, text=?, keywords=?, description=?, title=?, is_visible=?, is_menu=?, date_add=?, date_update=?, position=?, level=?, link_out=?, id_page_forwars=? WHERE id_static_page=?");
        $sth->bindParam(1, $data['id_page_top'], PDO::PARAM_INT);
        $sth->bindParam(2, $data['id_menu'], PDO::PARAM_INT);
        $sth->bindParam(3, $data['name'], PDO::PARAM_STR);
        $sth->bindParam(4, $data['url_page'], PDO::PARAM_STR);
        $sth->bindParam(5, $data['annotaciya'], PDO::PARAM_STR);
        $sth->bindParam(6, $data['text'], PDO::PARAM_STR);
        $sth->bindParam(7, $data['keywords'], PDO::PARAM_STR);
        $sth->bindParam(8, $data['description'], PDO::PARAM_STR);
        $sth->bindParam(9, $data['title'], PDO::PARAM_STR);
        $sth->bindParam(10, $data['is_visible'], PDO::PARAM_INT);
        $sth->bindParam(11, $data['is_menu'], PDO::PARAM_INT);
        $sth->bindParam(12, $data['date_add'], PDO::PARAM_STR);
        $sth->bindParam(13, date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $sth->bindParam(14, $data['position'], PDO::PARAM_INT);
        $sth->bindParam(15, $data['level'], PDO::PARAM_INT);
        $sth->bindParam(16, $data['link_out'], PDO::PARAM_STR);
        $sth->bindParam(17, $data['id_page_forwars'], PDO::PARAM_INT);
        $sth->bindParam(18, $data['id'], PDO::PARAM_INT);
      
        $sth->execute();
        $err = $sth->errorInfo();
        
        
        if ($err[0] == '00000'){
              $this->setNodeUrl($data['id']);
        }
        
        if ( ($err[0] == '00000') && $change) {
              $this->updateLevel($data['id'], $data['level']);
              
             //родитель узла изменен, надо проверить прежнего родителя
             if ($id_top_star_page!="0")  $this->setNodeUrl($id_top_star_page); 
             //затем проверим нового родителя,если он не 0
             if ($data['id_page_top']!="0")  $this->setNodeUrl($data['id_page_top']); 
           
        }
        
        return ($err[0] != '00000')?false:true;
        
    }
    
    public function delete($id){
        $arrayDel = explode(",", $id);
        
        $place_holders = implode(',', array_fill(0, count($arrayDel), '?'));
        $sth = $this->db->prepare("DELETE FROM ".PREFIX."_static_page WHERE id_static_page IN ($place_holders)");
        $sth->execute($arrayDel);
        
        for ($i=0; $i<count($arrayDel); $i++){
            $this->deleteNode($arrayDel[$i]);    
        }
        $err = $sth->errorInfo();
        
        if ($err[0] == '00000') {
            //обновление url  и folders всех разделов
            $resursAll = $this->showStatic();
            for ($i=0; $i<count($resursAll); $i++){
              $this->setNodeUrl($resursAll[$i]['id_static_page']);
            }
        }
        
        return ($err[0] != '00000')?false:true;
        
    }
    
     public function prepareInsertData($data, $type="add"){
        
        $data['date_add'] = Common::getTimeConversionInsertDB($data['date_add']);
        $data['url_page'] = ($data['url_page']=="")? Common::createURL($data['name']) : $data['url_page'];
        #------------------------------------------------------------------------
        //уникальность url
        if (strrpos($data['url_page'], ".html")){
            $length =  strlen($data['url_page'])-5;
            $url_page2 =  substr($data['url_page'], 0, $length);
        }else{
            $url_page2 = $data['url_page'].".html";
        }
        
        if ($type=="add"){
            $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_static_page WHERE url=? or url=?");
            $sth->bindParam(1, $data['url_page'], PDO::PARAM_STR);
            $sth->bindParam(2, $url_page2, PDO::PARAM_STR);
        }else if ($type=="edit"){    
             $sth=$this->db->prepare("SELECT `url` FROM ".PREFIX."_static_page WHERE id_static_page!=? and (url=? or url=?)");
             $sth->bindParam(1, $data['id'], PDO::PARAM_STR);
             $sth->bindParam(2, $data['url_page'], PDO::PARAM_STR); 
             $sth->bindParam(3, $url_page2, PDO::PARAM_STR); 
        }    
            $sth->execute();
            //$err = $sth->errorInfo();
            $count = $sth->rowCount();
           
            if ( $count > 0){
              $i=$count+1;   
              if (strrpos($data['url_page'], ".html")){
                  $length =  strlen($data['url_page'])-5;
                  $temp   =  substr($data['url_page'], 0, $length);
                  $data['url_page']=$temp."-".$i.".html";
                  
              }else{
                  $data['url_page']=$data['url_page']."-".$i;
              }
            }
        #-----------------------------------------------------------------------  
       
        $data['title']= ($data['title']!="")? $data['title']:$data['name'];
        //----------------------------------------------------------------------
        
        $data['position']=intval($data['position']); 
        if ($data['position']==""){
            $sth=$this->db->prepare("SELECT `id_static_page` FROM ".PREFIX."_static_page WHERE id_static_page_top=?");
            $sth->bindParam(1, $data['id_page_top'], PDO::PARAM_STR);
            $sth->execute();
            $data['position'] = $sth->rowCount()+1;
        }
        if (!isset($data['is_menu'])) $data['is_menu']=0;
        
         $data['text']=  Common::removingCharacter($data['text']);
       
        return $data;
    }
    
    
    public function showForm($id, $act, $action, $url, $lang){
         $this->idNextNode=$id; //при выводе списка прерываем поиск потомков текущего элемента
         $resultAll = $this->showStatic();
         $result = $this->showStatic($id);
         $result = Common::removeStipsSlashes($result);
         
         $level = $result['level'];
         $id_top= $result['id_static_page_top'];
         $id_forwars= $result['id_page_forwars'];
         
         $menu=new menuEdit(0);
         $menuList = $menu->showMenu();
                  
         $options = array();
         $options[0]=$lang['root_node'];
         $optionsOts = array();
         $optionsOts[0]="-------";
         $parametr[]="";
         $levels[]=" level=\"0\"";
         for($j=0; $j<count($resultAll); $j++) {
            $value="";
            $keigen="";
            $flag=false;            
            if ($resultAll[$j]['id_static_page'] != $id){            
                foreach($resultAll[$j]  as $key => $val ){
                    if ($key=="id_static_page") $keigen = $val;                     
                    if ($key=="name") $value = $val; 
                    if ($key=="level") $level = $val; 
                 }  
                 
                $tire = ($level>0)?"-":"";  
                
                for ($i=0; $i<$level-1; $i++){
                    $tire.="-";
                }
                
                $options[$keigen]=$tire.$value; 
                $optionsOts[$keigen]=$tire.$value; 
                $parametr[]=' style="padding-left:'.($level*10).'px;"';
                $levels[]=" level=\"$level\"";
            }    
         }
        
         $optionsMenu = array();
         $optionsMenu[0]=$lang['no_menu_node'];
         $parametrMenu[]="";
       
         for($j=0; $j<count($menuList); $j++) {
            $value="";
            $keigen="";
            $flag=false;            
                foreach($menuList[$j] as $key => $val ){
                    if ($key=="id_menu") $keigen = $val;                     
                    if ($key=="name") $value = $val; 
                    if ($key=="level") $level = $val; 
                 }                    
                $tire = ($level>0)?"- ":"";  
                $optionsMenu[$keigen]=$tire.$value; 
                $parametrMenu[]=' style="padding-left:'.($level*10).'px;"';               
         }
         
                        
         $flg = ( ($act=="add") || ($result['is_visible']=="1") ) ? true:false;  
         $flg2 =( ($act=="add") || ($result['is_menu']=="1") ) ? true:false;  
         if ($act=="add") {
             $result['position']="";
             $result['level']=1;
         }    
         
         $namePunct = $lang['form_thead_name_page'];
         $size=41; 
         $size2=123; 
         $class='level sm0'; //класс списка
         $classMenu='sm0';
         //$class= 'level big'; //класс списка
         
         $paramTextField['class']="creditor";
         $paramTextField['id']="creditor_id";
         $paramTextField['column']="columns-2"; //определяем поле в правую колонку формы, форма получиться двух-колоночная
         
         $paramTextFieldDefinition['column']="columns-2";
         $parametersDate['id']="datepicker";     
         
         if ($act=="edit") $dateAdd =  Common::getTimeConversionForm($result['date_add']);
             
         $date =          new field_text("date_add", $lang['date_add'], true, $dateAdd, "", 39, $parametersDate);         
         $name =          new field_text("name", $namePunct, true, $result['name'], "", $size);         
         $urlPage =      new field_text("url_page", $lang['menu_url'], false, $result['url'], "", $size);         
         //$annotaciya =    new field_textarea("annotaciya", $lang['page_annotaciya'], false, $result['annotaciya'], 93, 6, "", "", "", $paramTextFieldDefinition); 
         $annotaciya =        new field_hidden("annotaciya", false, $lang['page_annotaciya']);
         $text =          new field_textarea("text", $lang['page_text'], false, $result['text'], 38, 10, "", "", "", $paramTextField); 
         $root =          new field_select( "id_page_top", $lang['root_pege'], $options, $id_top, false, "", "level", $parametr, $levels, $class); 
         $id_menu =       new field_select( "id_menu", $lang['menu_node'], $optionsMenu, $result['id_menu'], false, "", "", $parametrMenu, "", $classMenu); 
         $id_page_forwars = new field_select( "id_page_forwars", $lang['page_forwars'], $optionsOts, $id_forwars, false, "", "", $parametr, "", $classMenu); 
         $position =      new field_text_int("position", $lang['menu_position'], false, $result['position'],"","","", $size);
         $link_out =       new field_text("link_out", $lang['page_link_out'], false, $result['link_out'], "", $size);         
         $is_visible =    new field_checkbox("is_visible", $lang['is_visible'], $flg);  
         $is_menu =       new field_checkbox("is_menu", $lang['form_thead_punct_is_menu'], $flg2);  
         
         $title =         new field_text("title", $lang['title'], false, $result['title'], "", $size2, $paramTextFieldDefinition);         
         $description =   new field_textarea("description", $lang['meta_description'], false, $result['description'], 93, 2, "", "", "", $paramTextFieldDefinition); 
         $keywords =      new field_text("keywords", $lang['meta_keywords'], false, $result['keywords'], "", $size2, $paramTextFieldDefinition);         
      
         $id_rec =        new field_hidden_int("id", false, $id);        
         $url =           new field_hidden_int("url", false, $url);
         $levelform =     new field_hidden_int("level", false, $result['level']);
         
        
        $form = new form(array("date_add" => $date,
                                "name"   => $name,
                                "root" => $root,
                                "id_menu" => $id_menu,
                                "id_page_forwars" => $id_page_forwars,
                                "url_page" => $urlPage,    
                                "position"   => $position, 
                                "link_out" => $link_out,
                                "is_visible"   => $is_visible,
                                "is_menu"   => $is_menu,
                                "text" => $text,
                                "title" => $title,
                                "description" => $description,
                                "keywords" => $keywords,
                                "annotaciya" => $annotaciya,
                                
                                "id" => $id_rec,
                                "url" => $url,
                                "level" => $levelform,
                                ),
                             "",
                             $action);        
        return  $form->print_form();
         
     }

#----------------------------------------------------------------------------------------------------------------------------------------------------------    
    private function getNextPage($idPageTop=0){  
        //++$this->level; 
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE id_static_page_top=? order by position");
        $sth->bindParam(1, $idPageTop, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        if(is_array($result)){  
            $i=0;
            foreach($result as $val){
                 array_push( $this->page, $result[$i]);
                
                 
                 if ($this->idNextNode!=$val['id_static_page']) {
                      $this->getNextPage($val['id_static_page']);
                     // --$this->level;
                 }else{
                      $this->idNextNode="";
                 } 
                    
                 $i++;
            }    
        }
    }
     
     public function updateLevelChild($id, $level){
         if ($id && $level) $this->updateLevel ($id, $level);
     }
    
     private function updateLevel($id, $level){
        if (!$id)  $id=0;  
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE id_static_page_top=?");
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        
        if(is_array($result)){  
             $level++;
            
             foreach($result as $val){
                 //echo $val['id_menu']."v".$level; exit();
                 $sthUp = $this->db->prepare("UPDATE ".PREFIX."_static_page SET level=? WHERE id_static_page=?");
                 $sthUp->bindParam(1, $level, PDO::PARAM_INT);
                 $sthUp->bindParam(2, $val['id_static_page'], PDO::PARAM_INT);
                 $sthUp->execute();
                 //$result = $sthUp->fetchAll(PDO::FETCH_ASSOC); 
                 
                 $this->updateLevel($val['id_static_page'], $level);
            } 
        }
         
     }   
   
     private function deleteNode($id){        
        $sth = $this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE id_static_page_top=?");
        $sth->bindParam(1, $id, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC); 
        
        if(is_array($result)){
             foreach($result as $val){
                 $this->deleteNode($val['id_static_page']);
             }
        }   
           $sthDel = $this->db->prepare("DELETE FROM ".PREFIX."_static_page WHERE id_static_page_top=?");
           $sthDel->bindParam(1, $id, PDO::PARAM_INT);
           $sthDel->execute();
       }   
       
       private function setNodeUrl($id){ 
           $sth=$this->db->prepare("SELECT * FROM ".PREFIX."_static_page WHERE id_static_page_top=?");
           $sth->bindParam(1, $id, PDO::PARAM_INT);
           $sth->execute();
           $countRow=$sth->rowCount();
           $update=false; 
           
           if ($countRow==0){ //нет детей
               $isfolder=0;
               $resurs=$this->showStatic($id);
               
                if ( ($resurs['is_folders']=="1") ||  (strpos($resurs['url'], ".html")===false) ) {
                 $resurs['url']= (strpos($resurs['url'], ".html")===false) ? $resurs['url'].".html" : $resurs['url'];   
                 $update=true;   
               }
              
           }else{ //есть дети
               $isfolder=1;
               $resurs=$this->showStatic($id);
               if ( ($resurs['is_folders']=="0") ||  (strpos($resurs['url'], ".html")>0) ) {
                  //$resurs['url']= (strpos($resurs['url'], ".html")>0) ? $resurs['url'].".html" : $resurs['url'];   
                  
                  if (strrpos($resurs['url'], ".html")>0){
                    $length =  strlen($resurs['url'])-5;
                    $resurs['url'] = substr($resurs['url'], 0, $length);
                  }
                  
                  $update=true;
               }
                             
           }
                     
           if ($update){
             $sth = $this->db->prepare("UPDATE ".PREFIX."_static_page SET is_folders=?, url=? WHERE id_static_page=?");
             $sth->bindParam(1, $isfolder, PDO::PARAM_INT);
             $sth->bindParam(2, $resurs['url'], PDO::PARAM_INT);
             $sth->bindParam(3, $id, PDO::PARAM_INT);
             $sth->execute();
           }  
          
           
           
       }
      
}

?>