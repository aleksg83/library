<?php
class ConstructorRassilkaSetiing extends ConstructorElements {
    
    private $ras;
    private $lang;
  
    public function __construct( rassilkaSendEdit $ras, $lang) {
        $this->ras = $ras;
        $this->lang = $lang;
    }
    
    public function getValidateForm(){
       
        return '';
        
    }
    
    public function getBreadCrumbs(){
        $breadcrumbs = Array();        
        $breadcrumbs['name']= $this->lang['rassilka_setting'];
        $breadcrumbs['puth']= '<span><a href="/admin/" title="'.$this->lang['dashboard'].'">'.$this->lang['dashboard'].'</a> > <span><a href="/admin/rassilkaSend/" title="'.$this->lang['rassilka_send_page'].'">'.$this->lang['rassilka_send_page'].'</a> > '.$breadcrumbs['name'].'</span>';        
        return $breadcrumbs;
    }
 
    public function getTableHead(){
        return  '';
     
    }
    
    public function getTableTbody($urlPage){
        $tbody = Array();
        $setting=$this->ras->getRassulkaSetting();
        
        $dopParam='';
        $updateAct='/admin/updateSettingRassilkaSave/'.$dopParam;
        $urlPage=$urlPage.$dopParam;
        
        
       // $setting = Common::removeStipsSlashes($setting);    
        
        if ($setting['type']=="1"){
            $ch1=' checked="checked"';
            $ch2="";
            $div1=' style="display:block"';
            $div2=' style="display:none"';
        }elseif ($setting['type']=="2"){
            $ch2=' checked="checked"';
            $ch1="";
            $div2=' style="display:block"';
            $div1=' style="display:none"';
        }
        
        if ($setting['is_HTML']=="1"){
            $chkh=' checked="checked"';
        }else{
            $chkh='';
        }
       
        //редактор
        
        $cred="
            var ckeditor4 = CKEDITOR.replace( 'creditor_id3',
					{
                                          	skin : 'kama',
						language: 'ru',
                                                //width: 900,
                                                height: 200,
                                                toolbar:[
                                                    ['Source','-','Link','Unlink','Anchor','HorizontalRule'],
                                                    ['Bold','Italic','Strike','-','SpecialChar','-','NumberedList','BulletedList','-','Blockquote','Outdent','Indent','CreateDiv','-','ShowBlocks'],                                            
                                                    ['Undo','Redo','-','RemoveFormat'],    
                                                    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
                                                    
                                                
                                                ]
                                                    
			});
            ";
        
        $tbody['text-div']='
           
	<script type="text/javascript">
        $().ready(function() {
        
                '.$cred.'
                    
            $("#validateForm").validate({
            	rules: {
                            name: "required",
                            email_a: {
                                email: true
                            }, 
                            email_s: {
                                email: true
                            }, 
                           
                        },
                        messages: {
                            name: "'.$this->lang['requiredField'].'",
                            email_a: {
                                   email: "'.$this->lang['errorEmailRules'].'",    
                           },
                           email_s: {
                                   email: "'.$this->lang['errorEmailRules'].'",    
                           }  
                        }
            });
            
          });  
	</script>

        <form class="forms" id="validateForm" method="post" action="'.$updateAct.'"> 
            <fieldset>
                <ul>
                    <li>
			<label class="desc" for="name">'.$this->lang['rassilka_setting_name_author'].'</label>
			<div><input id="name" class="field text full" name="name" value="'.$setting['name_author'].'" /></div>
                    </li>
                    
                    <li>
			<label class="desc" for="email_a">'.$this->lang['rassilka_setting_adress_author'].'</label>
			<div><input id="email_a" class="field text full" name="email_a" value="'.$setting['adress_author'].'" /></div>
                    </li>
                    
                    <li>
			<label class="desc" for="email_s">'.$this->lang['rassilka_setting_adress_server'].'</label>
			<div><input id="email_s" class="field text full" name="email_s" value="'.$setting['adress_server'].'" /></div>
                    </li>
                    

                    <li>
			<label class="desc" for="type">'.$this->lang['rassilka_setting_type'].'</label>
			<div class="text"><input type="radio" id="type" class="art_type" name="type" value="0" '.$ch1.'/>'.$this->lang['rassilka_setting_type0'].'</div>
                        <div class="text"><input type="radio" id="type" class="art_type" name="type" value="1" '.$ch2.'/>'.$this->lang['rassilka_setting_type1'].'</div>
                    </li>
                    
                    <div class="type0 text" '.$div1.'></div>
                    
                    <div class="type1 text" '.$div2.'>
                      
                       <li>
			<label class="desc" for="time">'.$this->lang['rassilka_setting_time_paket'].'</label>
			<div><input id="time" class="field text full" name="time" value="'.$setting['time_paket'].'" /></div>
                      </li>   

                      <li>
			<label class="desc" for="k_p">'.$this->lang['rassilka_setting_kolvo_pisem'].'</label>
			<div><input id="k_p" class="field text full" name="k_p" value="'.$setting['kolvo_pisem'].'" /></div>
                      </li>
                      
                      <li>
			<label class="desc" for="k_a">'.$this->lang['rassilka_setting_kolvo_adress'].'</label>
			<div><input id="k_a" class="field text full" name="k_a" value="'.$setting['kolvo_addr'].'" /></div>
                      </li>

                    </div>
                    

                    <li>
			<label class="desc" for="creditor_id">'.$this->lang['rassilka_setting_end_letter'].'</label>
			<div class="text"><textarea id="creditor_id3"  name="text">'.$setting['end_letter'].'</textarea></div>                        
                    </li>
           
                     <li>
			<label class="desc" for="html">'.$this->lang['rassilka_setting_html'].'</label>
			<div class="text"><input type="checkbox" id="html" class="check"  name="is_html" value="'.$setting['is_HTML'].'" '.$chkh.'/></div>                        
                    </li>
                    
                    <li>
			<label class="desc" for="about">'.$this->lang['rassilka_setting_about'].'</label>
			<div class="text"><textarea id="about"  name="about" rows="4" style="width:100%">'.$setting['about'].'</textarea></div>                        
                    </li>
                    
                    <li>
			<label class="desc" for="info_after_sender">'.$this->lang['rassilka_setting_info_after_sender'].'</label>
			<div class="text"><textarea id="info_after_sender"  name="info_after_sender" rows="4" style="width:100%">'.$setting['info_after_sender'].'</textarea></div>                        
                    </li>

                    
                    <li>
			<label class="desc" for="appeal_to_men">'.$this->lang['rassilka_setting_appeal_to_men'].'</label>
			<div><input id="appeal_to_men" class="field text full" name="appeal_to_men" value="'.$setting['appeal_to_men'].'" /></div>
                    </li>
                    
                    <li>
			<label class="desc" for="appeal_to_women">'.$this->lang['rassilka_setting_appeal_to_women'].'</label>
			<div><input id="appeal_to_women" class="field text full" name="appeal_to_women" value="'.$setting['appeal_to_women'].'" /></div>
                    </li>


		<li>
                        <input  type="hidden" name="url" value="'.$urlPage.'"/>
                        <input  type="hidden" name="id" value="1"/>    
			<input class="update" type="submit" value="'.$this->lang['rassilka_setting_save'].'"/>
                </li>
		</ul>
		</fieldset>
            </form>    
        ';
               
     return $tbody;
    
    }  
            
}

?>