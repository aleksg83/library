<?
//cookie
  function getCookieArray( $cookieValue ){
    $massTemp = explode(';', $cookieValue);
    $valuesMass = "";
    for($i=0; $i<count($massTemp); $i++){
        $k = strpos($massTemp[$i], "="); 
        $j = substr($massTemp[$i],0, $k);
        $valTemp = substr($massTemp[$i], $k+1);
        $valuesMass[$j] = $valTemp;
    }
   return $valuesMass;
}
$urlMass= explode("/", $_SERVER['REQUEST_URI']);
$cookieName = "pharus_window_".$urlMass[2];
$massiv = getCookieArray( $_COOKIE[$cookieName] );

//преобразование текстовой формы в редактор
if ($_COOKIE[$cookieName] == ""){
    $creditorWidth= ($this->tbody['creditor_width']!="")? $this->tbody['creditor_width']:600;
} else {
    $creditorWidth = $massiv['creditorWidth']; 
}    

$creditorHeight= ($this->tbody['creditor_height']!="")? $this->tbody['creditor_height']:500;
$creditorHeight2= ($this->tbody['creditor_height2']!="")? $this->tbody['creditor_height2']:200;


?>

<script type="text/javascript" src="<?=CSSJS?>/default/js/validate.js"></script>
<script type="text/javascript">
$(document).ready(function() {
          
          //функция получения  ассоциативного массива из куки
          function getAssociativeArray( cookieValue ){
           if ( (cookieValue != "") && (cookieValue != null)){     
              var massTemp = cookieValue.split(';');
               var valuesMass = new Array;
                for(var i=0; i<massTemp.length; i++){
                     var k =  massTemp[i].indexOf ("="); 
                     var j = massTemp[i].substring(0, k);
                     var valTemp = massTemp[i].substring(k+1);
                     valuesMass[j] = valTemp;
                }
                return valuesMass;
            }else{
              return false;   
            }     
          }    
         
         
            var areaWidth = $(window).width();
            areaWidth = (parseInt(areaWidth) > 1440 ) ? 1440 : areaWidth;
            
            var actionControlPanelWin = "pharus_window_"+actionControlPanel;
            var cookieMass = $.cookie(actionControlPanelWin);
            var CookieValuesAss = getAssociativeArray(cookieMass);
            var areaCreditor = ($.cookie(actionControlPanelWin) != null)? CookieValuesAss['creditorWidth']-6 :(areaWidth - 58 - 0.3*areaWidth) ;
            
            var areaHeight = $(window).height();
            areaHeight = (parseInt(areaHeight) > 900 ) ? 900 : areaHeight;
            
            
            setTimeout(function () { 
			$('#infooperation').fadeOut('slow');
             }, 3000);
             
             /*****************************************************************/
             
             function offScroll()
             {
                 var w = $("#page_wrapper").width(); 
            
                 $("body").css({"overflow":"hidden"});
                 $("#page_wrapper").width(w); 
            
             
                    /*
                    var  left = $(document).scrollLeft();
                    var  top = $(document).scrollTop();
                    $(document.body).css({
                        overflowX: "hidden"
                    });
	 
                    function stop_scroll(event) {
                        event.preventDefault();
                       $(document).scrollLeft(left);
                       $(document).scrollTop(top);
                    }
                    
                    $(document).bind("mousewheel", stop_scroll);
                    $(window).bind("scroll mousewheel", stop_scroll);
                    */
               
            }

            function onScroll()
            {
               $("body").css({
                overflow: ""
                });
               /*
               $(document).unbind('mousewheel');
                $(window).unbind("scroll mousewheel");
                $(document.body).css({
                overflow: ""
                });
               */
              
            }
            
            /**********************************************************************************/
              
                   
                   <? if ($this->tbody['draggable']) {?> 
                      $('#sort-table').tableDrag(); 
                   <?} ?> 
                          
         /*************************************************************************************/
         //----------------------------------------------------------------------------------------//
          $.validator.addMethod(
                "formRuDate",
                    function(value, element) {
       
                            return value.match(/^\d\d?\.\d\d?\.\d\d\d\d? \d\d?\:\d\d?:\d\d$/);
                  },
                 "<?=$this->lang['error_format_date']?>"
);
    
        $.validator.addMethod(
                "formRuDateNoTime",
                    function(value, element) {
       
                            return value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
                  },
                 "<?=$this->lang['error_format_date_no_time']?>"
);
    
    
      $.validator.addMethod(
                "formRuDateToDate",
                    function(value, element) {
                        var val1=$("#datepicker_1").val();
                        var val2=$("#datepicker_2").val();
                        
                        var date_1 = new Date(val1.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
                	var date_2 = new Date(val2.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
                        
                        var date  = new Date();
 
                        date.setTime(Date.parse(date_1));
                        var year1 = date.getFullYear();
                        var month1 = date.getMonth();
                        var day1 = date.getDate();
	  
                        date.setTime(Date.parse(date_2));
                        var year2 = date.getFullYear();
                        var month2 = date.getMonth();
                        var day2 = date.getDate();

                        var age = year2 - year1;
                        var month = ( month2 + 1 )- ( month1 + 1);  //+1 так как нумерация от 0 до 11
                        var day = day2 - day1;
                        
                        if ( age <= 0 && month <= 0 && day <0){
                           return false;
                        }else{
                           return true;
                        }
                      
                  },
                 "<?=$this->lang['error_format_date_to_date']?>"
);
         
      $.validator.addMethod(
                "loginFormSimbols",
                    function(value, element) {
                       
                        if((new RegExp("^[0-9a-zA-Z\_\-]{5,}$", "").test(value)))
                         {
                            return true;
                        }
                  },
                 "<?=$this->lang['error_format_login_symbols']?>"
      );
            
       $.validator.addMethod(
                "loginFormUniq",
                    function(value, element) {
                        
                        actForm = $("input[name=type_act]").val()
                        check=false;
                        ret = false;
                 
                        $.ajax({
     			  url: "/admin/getUniqLogin/",
			  async : false,
      			  type: "POST",
                          data:{ login: value, action: actForm},
      			  timeout: 6000000,
                          beforeSend: function(){
                             	
		          },
      			  success: function(data){
                                if (data == "yes") {
                                    check=false;
                                } else {
                                    check=true; 
                                }
                                ret=true;
			  }, 
	  		  error: function(xhr, status){
                             
                          }	
   			 });
                      
                   if (ret) return check;
                  
                  },
                 "<?=$this->lang['error_format_login_count']?>"
        );     
        //---------------------------------------------------------------------- 
        
        $.validator.addMethod(
                "menuURLUniq",
                    function(value, element) {
                        
                        actForm = $("form#validateForm  input[name=type_act]").val();
                        idRec = $("form#validateForm input[name=id]").val();
                        idTop = $("form#validateForm  select[name=id_menu_top]").val();
                        isRubric=$("form#validateForm  input[name=is_rubricator]").val();
                        check=false;
                        ret = false;
                 
                        $.ajax({
     			  url: "/admin/getUniqMenu/",
			  async : false,
      			  type: "POST",
                          data:{ url_name: value, action: actForm, id_menu_top:idTop, id_menu:idRec, isrubric:isRubric},
      			  timeout: 6000000,
                          beforeSend: function(){
                             	
		          },
      			  success: function(data){
                                if (data == "yes") {
                                    check=false;
                                } else {
                                    check=true; 
                                }
                                ret=true;
			  }, 
	  		  error: function(xhr, status){
                             
                          }	
   			 });
                      
                   if (ret) return check;
                  
                  },
                 "<?=$this->lang['error_format_menu_url_count']?>"
        );     
         
        $("select[name=id_menu_top]").live( "change", function() {
            $("input[name=urlpunct]").trigger("focusout"); 
        });
         
         //---------------------------------------------------------------------------------------//
         
         function setDataPicker(){
              $('#datepicker').datetimepicker({
                    changeMonth: true,
                    changeYear: true,
                    showOn: 'both',
                    buttonImage:'<?=CSSJS?>/default/images/calendarj.png',
                    showSecond: true,
                    timeFormat: 'HH:mm:ss',
                    stepHour: 1,
                    stepMinute: 1,
                    stepSecond: 1
                });
       
         }
         
          function removeDataPicker(){
            $('#datepicker').removeClass('hasDatepicker');
          }
          
         //--------------------------------------------------------------------------------------
          function setDataPicker2(){
              $('#datepicker_1, #datepicker_2').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showOn: 'both',
                    buttonImage:'<?=CSSJS?>/default/images/calendarj.png',
                    timeFormat: 'HH:mm:ss',
                });
       
         }
         
         $('.philter_from, .philter_to').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    //showOn: 'both',
                    //buttonImage:'<?=CSSJS?>/default/images/calendarj.png',
                    timeFormat: 'HH:mm:ss',
                });
      

         //------------------------------------------------------------------------------------//
          var ckeditor1, ckeditor3, html = '';
          function createEditor() {
                    if ( ckeditor1 )
                    return;

                    var config = {};
                  //  ckeditor1 = CKEDITOR.appendTo( 'creditor_id', config, html );
                   
                    var ckeditor1 = CKEDITOR.replace( 'creditor_id',
					{
                                          	skin : 'kama',
						language: 'ru',
                                                //width: <?=$creditorWidth?>,
                                                //width: areaWidth - 58 - 0.3*areaWidth,  
                                                width: areaCreditor,
                                                //height: <?=$creditorHeight?>,
                                                height: areaHeight - 160,
                                                //uiColor: '#94b4f9',
                                                //fullPage: true,
                                                allowedContent: true,
                                                toolbar:[
                                                    ['Source','-','Maximize','-','Preview','-','Templates','-','Image','Flash','Table','Link','Unlink','Anchor','HorizontalRule'],
                                                    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],    
                                                    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
                                                    ['TextColor','BGColor'],
                                                    '/',

                                                    ['Bold','Italic','Strike','-','Subscript','Superscript','-','SpecialChar','-','NumberedList','BulletedList','-','Blockquote','Outdent','Indent','CreateDiv','-','ShowBlocks'],
                                                    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                                                    [/*'Styles',*/'Format','Font','FontSize','Classes1', 'Classes2'],

                                                ]
                                                    
			});
                                          
                        AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor1});
                      
          }
         
          function removeEditor() {
                    if ( !ckeditor1 )
                    return; 
                    // Retrieve the editor contents. In an Ajax application, this data would be
                    // sent to the server or used in any other way.
                    //document.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();
                    //document.getElementById( 'contents' ).style.display = '';
                    // Destroy the editor.
                    ckeditor1.destroy();
                    ckeditor1 = null;
           }
         //----------------------------------------------------------------------------------//
         function createEditor3() {
                    if ( ckeditor3 )
                    return;

                    var config = {};
                  //  ckeditor1 = CKEDITOR.appendTo( 'creditor_id', config, html );
                   
                    var ckeditor3 = CKEDITOR.replace( 'creditor_id3',
					{
                                          	skin : 'kama',
						language: 'ru',
                                                width: <?=$creditorWidth?>,
                                                height: <?=$creditorHeight?>,
                                                //uiColor: '#94b4f9',
                                                //fullPage: true,
                                                allowedContent: true,
                                                toolbar:[
                                                    ['Source','-','Maximize','-','Preview','-','Templates','-','Image','Flash','Table','Link','Unlink','Anchor','HorizontalRule'],
                                                    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],    
                                                    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
                                                    ['TextColor','BGColor'],
                                                    '/',

                                                    ['Bold','Italic','Strike','-','Subscript','Superscript','-','SpecialChar','-','NumberedList','BulletedList','-','Blockquote','Outdent','Indent','CreateDiv','-','ShowBlocks'],
                                                    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
                                                    [/*'Styles',*/'Format','Font','FontSize','Classes1', 'Classes2'],

                                                ]
                                                    
			});
                   
                        AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor3});
                      
          }
         
         
         
         
         //-----------------------------------------------------------------------------------//
          
          
          var ckeditor2;
          function createEditorSmall() {
                    if ( ckeditor2 )
                    return;

                    var config = {};
                                   
                    var ckeditor2 = CKEDITOR.replace( 'creditor_id2',
					{
                                          	skin : 'kama',
						language: 'ru',
                                                //width: <?=$creditorWidth?>,
                                                //width: areaWidth - 58 - 0.3*areaWidth, 
                                                width: areaCreditor,
                                                height: <?=$creditorHeight2?>,
                                                //uiColor: '#94b4f9',
                                                //fullPage: true,
                                                allowedContent: true,
                                                toolbar:[
                                                    ['Source','-','Link','Unlink','Anchor','HorizontalRule'],
                                                    ['Bold','Italic','Strike','-','SpecialChar','-','NumberedList','BulletedList','-','Blockquote','Outdent','Indent','CreateDiv','-','ShowBlocks'],                                            
                                                    ['Undo','Redo','-','RemoveFormat'],    
                                                    ['Cut','Copy','Paste','PasteText','PasteFromWord'],
                                                    
                                                
                                                ]
                                                    
			});
                   
          }
         
         
         
         
         //-------------------------------------------------------------------------------------//
                     $(".check").live( "click", function() {
                          var pole = $(this).attr("name");
                          var tableName = $(this).attr("table");
                          var idRecName = $(this).attr("nameidrec");
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                         
                          //выключение
                          /*
                          if (value == 1){
                              value=0;
                          } else {
                              value=1;
                          }
                          
                          //if (pole=="is_visible"){ 
                                
                            /* $.post( action, 
                                       {id: id_rec, is_visible:value}
                              );
                             */
                            
                            $.post( action, 
                                       {table:tableName, nameColumn: pole, id: id_rec, valueRec:value, id_rec_name: idRecName}
                            );         
                          //}          
                          
                      });    
                    
        
                     $("a.action").live( "click", function() {
                      var action=$(this).attr("action"); //url действия
                      var showForm=$(this).attr("form"); //form действия
                      var act=$(this).attr("act"); // действие - удалить, добавить, редактировать
                      var url=$(this).attr("url"); // url страницы возврата после выполнения действия 
                      var id_record=$(this).attr("id_record"); // id записи
            
            
                      // удаление записи
                      if (act == "delete"){  
                            
                            var title="<?=$this->lang['delete']?>?";
                            var text="<?=$this->lang['del_question']?>";
                            
                            if ($(this).hasClass("removeprint")){
                                text="<?=$this->lang['predprint_remove']?>?";
                            }
                            
                             $("#modal").attr("title", title);
                             $("#modal p").html('<span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>'+text);
                               
                             var $dialogDel = $( "#modal" ).dialog({
                                               modal: true,
                                               resizable:false,
                                               buttons: {
                                                     "<?=$this->lang['cancel']?>": function() {
                                                        $(this).dialog( "close" );
                                                     },
                                                      "<?=$this->lang['continue']?>": function() {
                                                           $("form.pager-form input[name=url]").val(url);
                                                           $("form.pager-form input[name=id]").val(id_record);
                                                           $("form.pager-form").attr("action", action);
                                                           $("form.pager-form").submit();  
                                                      },
				
                                                }                             		
                                            });             
                                  $dialogDel.dialog( "open" );
                                  $( ".ui-dialog-title" ).attr("title", title);
                                  $( ".ui-dialog-title" ).html(title);
                                     
                       // удаление всех записей
                       } else if (act == "delall"){  
                             var id_rec="";
                            
                              $("input[name=list]:checkbox:checked").each(function(){
                              var n=$(this).val(); 
                           
                              if (n!="checkall") id_rec += n + ",";
                              }); 
                         
                             id_rec=id_rec.substr(0, id_rec.length-1);
                         
                         
                          if (id_rec.length > 0) { 
                             
                             var title="<?=$this->lang['deleteAll']?>?";
                            
                             
                             $("#modal").attr("title", "<?=$this->lang['deleteAll']?>?");
                             $("#modal p").html('<span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span><?=$this->lang['del_question_all']?>');
                               
                             var $dialogDel = $( "#modal" ).dialog({
                                                 modal: true,
                                                 resizable:false,
                                                 buttons: {
                                                     "<?=$this->lang['cancel']?>": function() {
                                                        $("#form").html("");
                                                        $(this).dialog( "close" );
                                                     },
                                                      "<?=$this->lang['continue']?>": function() {
                                                           $("form.pager-form input[name=url]").val(url);
                                                           $("form.pager-form input[name=id]").val(id_rec);
                                                           $("form.pager-form").attr("action", action);
                                                           $("form.pager-form").submit(); 
                                                     },
				
                                                }                             		
                                            });             
                                  $dialogDel.dialog( "open" );
                                  $( ".ui-dialog-title" ).attr("title", title);
                                  $( ".ui-dialog-title" ).html(title);
                         }
            
                     //добавление и редактирование 
                     } else  if ( (act == "edit") || (act == "add") || (act == "addmass") || (act == "view")){
                     
                             if (act == "edit") {
                                 var title="<?=$this->lang['edit']?>"; 
                             } else if (act == "addmass") {
                                 var title="<?=$this->lang['addMass']?>"; 
                             } else if (act == "view") {
                                 var title="<?=$this->lang['view_record']?>";
                             }else { 
                               var title="<?=$this->lang['add']?>"; 
                             }
                   
                     //$("#form").attr("title", title);
                     
                       //**************************************** Ajax получение формы *****************************************
			$.ajax({
     			  url: showForm,
			  //async : false,
      			  type: "POST",
                         // type: "GET",
     			  data:{ id: id_record, act: act, action: action, url: url},
      			  timeout: 6000000,
                          beforeSend: function(){
                              $("#form").html();	
		          },
      			  success: function(data){
				$("#form").html(data);	 
                                <?=$this->validate?>
                                <? if($this->tbody['creditor_big']=="1") {?> createEditor(); <?}?>
                                <? if($this->tbody['creditor_no_resize']=="1") {?> createEditor3(); <?}?>    
                                <? if($this->tbody['creditor_small']=="1") {?> createEditorSmall(); <?}?>    
                                <? if($this->tbody['date_area']=="1") {?>  setDataPicker();<?}?>  
                                <? if($this->tbody['date_area2']=="1") {?>  setDataPicker2();<?}?>      
			  }, 
	  		  error: function(xhr, status){
                              
                          }	
   			 });
                        
 		    //------------------------ Ajax получение формы -------------------------------------------------
                    var $dialog = $( "#form" ).dialog({
                                  modal: true,
                                  resizable:true,
                                  title: title,
                                  <? 
                                    
                                   if ($_COOKIE[$cookieName] == ""){
                                       
                                      if ( isset($this->tbody['fwidth']) ){
                                         echo "width: ".$this->tbody['fwidth'].",
                                              ";
                                      }else{
                                         echo "width: areaWidth-18,";
                                      }
                                     
                                      if ( isset($this->tbody['fheight']) ){
                                         echo "height: ".$this->tbody['fheight'].",
                                              ";
                                      }else{
                                         echo "height: areaHeight-18,";
                                      }
                                    
                                        
                                   }else{
                                        echo "width:  ".$massiv['width']."+6,";  
                                        echo "height:  ".$massiv['height']." + 100,";  
                                        
                                    }
                                                               
                                    
                                   
                                  ?>
                                  //width:  areaWidth-18,                 
                                  //height: areaHeight-18,                  
                                   buttons: {
                                    "<?=$this->lang['cancel']?>": function() {
                                        $("#form").html("");
                                        <? if($this->tbody['creditor_big']=="1") {?> removeEditor();<?}?>
                                        <? if($this->tbody['date_area']=="1") {?>  removeDataPicker();<?}?>      
					$(this).dialog( "close" );
                                        onScroll();
                                    },
                                   
                                   
                                   <? if ($this->tbody['view']!="view") { ?>         
                                   
                                    "<?=$this->lang['save']?>": function() {
                                       $("form.editAction").submit().validate();
                                       onScroll();
                                       //$(this).dialog( "close" );
				    }	
                                            
                                   <?}?>  
                                       
                                 },
                                 open: function (event, ui) {
                                    
                                 }, 
                                 close: function (event, ui) {
                                     $("#form").html("");
                                      onScroll();
                                 },
                                 resize: function (event, ui) {        
                                     //if ( ckeditor1 )  ckeditor1.width('200');
                                     //console.log( $(this).height() + ' ' + $(this).width());
                                     var crWidth = $(this).width() -18 - 0.3*$(this).width();
                                     $(".cke_editor_creditor_id, .cke_editor_creditor_id2").css("width", crWidth + 'px');
                                     $.cookie("pharus_window_"+actionControlPanel, "width="+ parseInt($(this).width()) +";height="+parseInt($(this).height())+";creditorWidth="+parseInt(crWidth)+";", {path: "/"} );
                                     
                                 },
                                         
                                         
                                
                    });
		   
                    $dialog.dialog( "open" );
                    offScroll();
                    $( ".ui-dialog-title" ).attr("title", title);
                    $( ".ui-dialog-title" ).html(title);
                   
                 } 
            
              });
            
            
            
              $("select").live("change", function() {
                              
                if ( $(this).hasClass("level")) {
                    var level = $(this).children("option:selected").attr("level");
                        level = parseInt(level, 10) + 1;
                    $("form.editAction input[name=level]").val(level);
                }
                
              });
              
              
              $("td.pos").dblclick(function(){
                  
                  if ($(this).closest("tr").attr("rubric")) {
                       var val=$(this).text();
                       $(this).html('<input type="text" name="position" class="updateMenuPosition" value="'+ val +'" code="int" size="1"/>');
                  }
              });
              
              
              $("input.updateMenuPosition").live("keypress focusout", function(eventObject) {
                   if ( (eventObject.which == 13) || (eventObject.type =="focusout") ){
                    
                     var value=parseInt( $(".updateMenuPosition").val(), 10);
                     var id=$(this).closest("tr").find(".node-id").val();
                     $(this).closest("tr").attr("position", value);
                     $(this).closest("tr").find(".row-weight").val(value);
                     $(this).closest("td").html(value);
                    
                    //DOM обновлен, изменим базу данных  
                       $.ajax({
                         url: '/adminajax/updateMenuPosition/',
                         type: "POST",
                        // dataType: "json",
                         data:{ id: id, position : value  },
                         timeout: 6000000,
                         beforeSend: function(){
                             	
                         },
                        success: function(data){
                        
                        }, 
                         error: function(xhr, status){
                              
                         }	
                        }); 
                 }
                  
               });
              
               $("input[code=int]").live("keypress",function(e) {
                 if($.browser.msie)
                  return isNum(e.keyCode)
                else
                 return (e.keyCode) ? true : isNum(e.charCode)

                });


                function isNum(cCode){
                  //return /[0-9\(\)\ \+\-\.]/.test(String.fromCharCode(cCode))
                 return /[0-9]/.test(String.fromCharCode(cCode))
                }
                
                
                //-------------------------InRubricMenu------------------------------------
                  $(".checkInRubric").click(function(){
                          var id_main = parseInt($(this).attr("id_main"), 10);
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                          var rubric = parseInt($(this).attr("rubric"), 10);
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          
                          //выключение
                          if (value == 1){
                              value=0;
                          } else {
                              value=1;
                          }
                          
                           $.post( action, 
                                       {id: id_rec, id_main:id_main, rubric: rubric, value: value}
                            );         
                          //}          
                          
                      });    
                //-------------------------------------------------------------------------
                  
                  
                //-------------------------InRubricPerson------------------------------------
                  $(".checkInRubricPerson").click(function(){
                          var id_person = parseInt($(this).attr("id_person"), 10);
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          
                          //выключение
                          if (value == 1){
                              value=0;
                          } else {
                              value=1;
                          }
                          
                           $.post( action, 
                                       {id: id_rec, id_person:id_person, value: value}
                            );         
                          //}          
                          
                      });    
                //-------------------------------------------------------------------------
                
                
                //-------------------------InRubricPerson------------------------------------
                  $(".checkInRubricLib").click(function(){
                          var id_data = parseInt($(this).attr("id_data"), 10);
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          
                          //выключение
                          if (value == 1){
                              value=0;
                          } else {
                              value=1;
                          }
                          
                           $.post( action, 
                                       {id: id_rec, id_data:id_data, value: value}
                            );         
          
                          //}          
                          
                      });    
                //-------------------------------------------------------------------------
                 
                 //-------------------------InRubricSpOrg------------------------------------
                  $(".checkInRubricSpOrg").click(function(){
                          var id_sp_org = parseInt($(this).attr("id_sp_org"), 10);
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          
                          //alert(id_sp_org);
                          //alert(action);
                          
                          //выключение
                          if (value == 1){
                              value=0;
                          } else {
                              value=1;
                          }
                          
                           $.post( action, 
                                       {id: id_rec, id_sp_org:id_sp_org, value: value}
                            );       
                          //}          
                          
                      });    
                //-------------------------------------------------------------------------
                
                
                //-------------------------InPersonLib------------------------------------
                  $(".checkInPersonLib").live("click", function() {
                          var id_sp_type_person = parseInt($(this).attr("id_sp_type_person"), 10);
                          var id_data = parseInt($(this).attr("id_data"), 10);
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          
                          //alert("xxxxx");
                          
                          //выключение
                         /* if (value == 1){
                              value=0;
                          } else {
                              value=1;
                          }*/
                          
                           $.post( action, 
                                       {id: id_rec, id_sp_type_person: id_sp_type_person, id_data: id_data, value: value}
                            );       
                          //}          
                          
                      });    
                //--------------------------------------------------------------
                
                
                //-------------------------InRubricPerson-----------------------
                  $(".checkUserInRoles").live("click", function() {
                          var id_users = parseInt($(this).attr("id_users"), 10);
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          
                           $.post( action, 
                                       {id: id_rec, id_users:id_users, value: value}
                            );         
                                 
                          
                      });    
                //--------------------------------------------------------------
                 
                 
                 
                 
                //----------- upload -------------------------------------------
             if($("#fileUpload2").length>0) {  
                $("#fileUpload2").fileUpload({
                
                'uploader': '<?=CSSJS?>/default/uploadify/uploader.swf',
		'cancelImg': '<?=CSSJS?>/default/uploadify/cancel.png',
		'script': '<?=CSSJS?>/default/uploadify/upload.php',
		'folder': '<?=$this->tbody['folders'];?>',
		'multi': true,
		'buttonText': 'Select Files',
		'checkScript': '<?=CSSJS?>/default/uploadify/check.php',
		'displayData': 'speed',
		'simUploadLimit': 2,
		'auto'        : true,
                
                'onAllComplete' : function(event,data) {
                  window.location.href="<?=$this->tbody['url-upload']?>";
		  //alert(data.filesUploaded + ' files uploaded successfully!');
                 }
                });
              }  
                //--------------------------------------------------------------
               
               //---------------------------------------------------------------
               $(".philterCalc").change(function(){
                  var select = $(this).val();
                  var url = $(this).attr("url");
                  var act = urlServer + url + select+"/"
                  window.location.href=act;
                  //window.open( act );
               });
              
              //----------------------------------------------------------------
               $("a.empty").live( "click", function() {
                      var action=$(this).attr("action"); //url действия
                      var url=$(this).attr("url"); // url страницы возврата после выполнения действия 
                      var id_record=$(this).attr("id_record"); // id записи
            
                        var title="<?=$this->lang['empty']?>?";
                            
                        $("#modal").attr("title", title);
                        $("#modal p").html('<span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span><?=$this->lang['empty_question']?>');
                               
                         var $dialogDel = $( "#modal" ).dialog({
                                             modal: true,
                                             resizable:false,
                                             buttons: {
                                                       "<?=$this->lang['cancel']?>": function() {
                                                        $(this).dialog( "close" );
                                                       },
                                                      "<?=$this->lang['continue']?>": function() {
                                                           $("form.pager-form input[name=url]").val(url);
                                                           $("form.pager-form").attr("action", action);
                                                           $("form.pager-form").submit();  
                                                       },
				
                                                     }                             		
                                           });             
                         $dialogDel.dialog( "open" );
                         $( ".ui-dialog-title" ).attr("title", title);
                         $( ".ui-dialog-title" ).html(title);
                                     
                     });
              //----------------------------------------------------------------
               
});

 </script>