<script type="text/javascript" src="<?=CSSJS?>/default/js/tablesorter.js"></script>
<script type="text/javascript" src="<?=CSSJS?>/default/js/tablesorter-pager.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	/* Table Sorter */
	$("#sort-table")
	.tablesorter({
		widgets: ['zebra'],
		headers: { 
		           // assign the secound column (we start counting zero) 
		            0: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            }, 
		            // assign the third column (we start counting zero) 
		            <?=count($this->column);?>: { 
		                // disable it by setting the property sorter to false 
		                sorter: false 
		            } 
		        }, 
	})
	
	.tablesorterPager({container: $("#pager")}); 

	$(".header").append('<span class="ui-icon ui-icon-carat-2-n-s"></span>');

	
});

/* Check all table rows */

var checkflag = "false";
function check(field) {
if (checkflag == "false") {
for (i = 0; i < field.length; i++) {
field[i].checked = true;}
checkflag = "true";
return "check_all"; }
else {
for (i = 0; i < field.length; i++) {
field[i].checked = false; }
checkflag = "false";
return "check_none"; }
}


</script>



 <?
          $i=0; 
           //$url=$_SERVER['REQUEST_URI'];
           
           $url=str_replace ("?".$_SERVER['QUERY_STRING'], "", $_SERVER['REQUEST_URI']);
           $columnName="";
           $th="";
           
           //----------------------------------------------------------------------------------------------
           // формирование столбцов таблицы и пустых скрытых информационных полей для формы
           foreach($this->column as $key => $val) 
           { 
              
               if ($i==0) {
                   $hidden='<input type="hidden" name="id" value=""/>';
                   $columsNull="id="; 
               }    
               if ($i>0)  
               {
                   $th.="<th>$val</th>";
                   $hidden.='<input type="hidden" name="'.$key.'" value=""/>';
                   
                   
                   if (strrpos($key, "is_") === 0){
                       
                         if ($key=="is_visible"){
                            
                          $columsNull.="&$key=1";
                         
                          } else {
                         
                             $columsNull.="&$key=0";
                            }
                   } else {
                       
                      $columsNull.="&$key=";
                   }   
                   
               }
               
               //названия столбцов для дальнейшего использования в формах
               $columnName.="&".$val; 
               
               $i++; 
           }
            
            $columnName=trim($columnName, "&");
            $hidden.='<input type="hidden" name="url" value=""/>'; //адрес страницы после выполнения всех действий
           
            
            
            //----------------------------------------------------------------------------------------------
            //формирование строк таблицы
            $tr="";
            for ($j=0; $j<count($this->data); $j++)
            {
               
               $i=0; 
               $columns=""; //данные полей текущей строки
               $tr.='<tr>';
            
            foreach($this->data[$j] as $key => $val )
            {
          
               
                if ($i==0){
                     $id_record=$val;  
                     $columns="id=".$id_record;
                     $tr.='<td class="center"><input type="checkbox" value="'.$val.'" name="list" class="checkbox"/></td>'; 
               
                } else if (strrpos($key, "is_") === 0){
                  
                     if ($val == 1) $checked = 'checked="cheked"';
                     else $checked="";
                     
                     $tr.='<td class="cnt"><input type="checkbox" value="'.$val.'" name="'.$key.'" class="check" '.$checked.' id_record="'.$id_record.'" action="'.$this->uptadecolumn.'"/></td>'; 
                     
                } else {
                    
                     $tr.="<td>$val</td>"; 
                                         
                }
                if ($i>0) $columns.="&$key=$val";
                                                
                $i++; 
            }
            
            
            $tr.='<td'.$styleTDAction.'>
                    
                   <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$lng['edit'].'" href="javascript:void(0);" action="'.$this->edit.'" columns="'.$columns.'" columnname="'.$columnName.'" act="edit" url="'.$url.'">
		     <span class="ui-icon ui-icon-pencil" ></span>
		   </a>
                                      
                   <a class="btn_no_text btn ui-state-default ui-corner-all tooltip action" title="'.$lng['delete'].'" href="javascript:void(0);" action="'.$this->delete.'" columns="'.$columns.'" columnname="'.$columnName.'" act="delete" url="'.$url.'">
		    <span class="ui-icon ui-icon-circle-close"></span>
		   </a>
                   
                   
                 </td>
            
            </tr>';
        }  
 
?> 
 
<div class="toolbar">
<a class="btn ui-state-default ui-corner-all action" href="javascript:void(0);" title="<?=$lng['add']?>" action="<?=$this->add?>" act="add" url="<?=$url?>" columns="<?=$columsNull?>" columnname="<?=$columnName?>">
<span class="ui-icon ui-icon-plusthick"></span>
<?=$lng['add']?>
</a>
   
<a class="btn ui-state-default ui-corner-all action" href="javascript:void(0);" title="<?=$lng['deleteAll']?>" action="<?=$this->delete?>" act="delall" url="<?=$url?>" columns="" columnname="">
<span class="ui-icon ui-icon-trash"></span>
<?=$lng['deleteAll']?>
</a>    
</div>
<div class="clear"></div>


<? if (!empty($this->operation)){
     $infoOperation="";
     foreach ($this->operation as $key => $value) {
           
         if ( ($key=="delete") & ($value=="ok") ) {
               
                $infoOperation.=$lng['deleteInfo']."<br/>";
                
         } else if ( ($key=="edit") & ($value=="ok") ) {
               
                $infoOperation.=$lng['editInfo']."<br/>";
         
                
         }  else if ( ($key=="add") & ($value=="ok") ) {
               
                $infoOperation.=$lng['addInfo']."<br/>";
                
         }        
                 
     }
    
?>   
 <div class="response-msg inf ui-corner-all" id="infooperation">
<span><?=$infoOperation?></span>
</div>
<?}?>




<div class="hastable">
<? if (count($this->data) > 0) { ?>    
 <form name="myform" class="pager-form" method="post" action="">
 <table id="sort-table"> 
    <thead> 
     <tr>
    	<th><input type="checkbox" value="check_none" onclick="this.value=check(this.form.list)" class="submit"/></th>
	 <?=$th?>
         <th style="width:128px"><?=$lng['options']?></th> 
    </tr> 
  </thead> 
  <tbody>
       <?=$tr?>						
  </tbody>
</table>
       
        <?=$hidden?>
          
<div id="pager">
					
<a class="btn_no_text btn ui-state-default ui-corner-all first" title="<?=$lng['firstPage']?>" href="#">
<span class="ui-icon ui-icon-arrowthickstop-1-w"></span>
</a>
<a class="btn_no_text btn ui-state-default ui-corner-all prev" title="<?=$lng['previosPage']?>" href="#">
<span class="ui-icon ui-icon-circle-arrow-w"></span>
</a>
							
<input type="text" class="pagedisplay"/>
<a class="btn_no_text btn ui-state-default ui-corner-all next" title="<?=$lng['nextPage']?>" href="#">
<span class="ui-icon ui-icon-circle-arrow-e"></span>
</a>
<a class="btn_no_text btn ui-state-default ui-corner-all last" title="<?=$lng['lastPage']?>" href="#">
<span class="ui-icon ui-icon-arrowthickstop-1-e"></span>
</a>
<select class="pagesize">
<option value="10" selected="selected">10 <?=$lng['records']?></option>
<option value="20">20 <?=$lng['records']?></option>
<option value="30">30 <?=$lng['records']?></option>
<option value="40">40 <?=$lng['records']?></option>
</select>								
</div>
    
    
</form>
<? }else{ ?>    

<div class="response-msg inf ui-corner-all">
<span><?=$lng['noRecord']?></span>
</div>
    
<? }?>