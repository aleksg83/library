<?
   if ($page == "spedit"){
       
       $validate=' $("#validateForm").validate({
                        rules: {
                            name: "required",
                        },
                        messages: {
                            name: "'.$lng['requiredField'].'",
                        }
                    });  
                   '; 
   }
?>

<script type="text/javascript" src="<?=CSSJS?>/default/js/validate.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
                     setTimeout(function () { 
				$('#infooperation').fadeOut('slow');
		     }, 3000);
                     
                     
                      $(".check").click(function(){
                          var pole = $(this).attr("name");
                          var value = parseInt($(this).val(), 10);
                          var action = $(this).attr("action");
                          var id_rec = parseInt($(this).attr("id_record"), 10);
                          
                          //выключение
                          if (value == 1){
                              value=0;
                          } else {
                              value=1;
                          }
                          
                            if (pole=="is_visible"){ 
                                
                                 $.post( action, 
                                       {id: id_rec, is_visible:value}
                                 );
                                     
                            }          
                          
                      })    
                      
                       function formSubmit(keyAndVal, action, columns, url)
                      {
                         for (var key in keyAndVal) 
                        {
                            var val = keyAndVal[key].split('=');
               
                            if ( val[0].indexOf("is_")>-1)
                            {
                               var mode=$(this).closest("tr").children("td").children("input[name="+ val[0] +"][type=checkbox]").attr("checked");
                  
                               if (mode) val[1]=1; 
                               else val[1]=0;
                   
                             }
               
                            $("form.pager-form input[name="+val[0]+"]").val(val[1]);
             
                         }    
          
                        $("form.pager-form input[name=url]").val(url);
                        $("form.pager-form").attr("action", action);
                        $("form.pager-form").submit(); 
            
                     }
        
        
        
                     $("a.action").click(function(){
            
                      var action=$(this).attr("action"); //url действия
                      var columns=$(this).attr("columns"); //значения полей
                      var columnName=$(this).attr("columnname").split('&'); //названия столбцов
            
                      var keyAndVal = columns.split('&'); //массив пар значений имя поля = значение  
                      var act=$(this).attr("act"); // действие - удалить, добавить, редактировать
                      var url=$(this).attr("url"); // url страницы возврата после выполнения действия 
            
            
                      // удаление записи
                      if (act == "delete"){  
              
                             $("#modal").attr("title", "<?=$lng['delete']?>?");
                             $("#modal p").html('<span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span><?=$lng['del_question']?>');
                               
                             var $dialogDel = $( "#modal" ).dialog({
                                               modal: true,
                                               resizable:false,
                                               buttons: {
                                                     "<?=$lng['cancel']?>": function() {
                                                        $(this).dialog( "close" );
                                                     },
                                                      "<?=$lng['continue']?>": function() {
                                                          formSubmit(keyAndVal, action, columns, url); 
                                                      },
				
                                                }                             		
                                            });             
                                  $dialogDel.dialog( "open" );
            
                       // удаление всех записей
                      } else if (act == "delall"){  
                             var id_rec="";
                           
                              $("input[name=list]:checkbox:checked").each(function(){
                              var n=$(this).val(); 
                           
                              if (n!="checkall") id_rec += n + ",";
                              }); 
                         
                             id_rec=id_rec.substr(0, id_rec.length-1);
                         
                         
                          if (id_rec.length > 0) { 
                             
                             $("#modal").attr("title", "<?=$lng['deleteAll']?>?");
                             $("#modal p").html('<span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span><?=$lng['del_question_all']?>');
                               
                             var $dialogDel = $( "#modal" ).dialog({
                                                 modal: true,
                                                 resizable:false,
                                                 buttons: {
                                                     "<?=$lng['cancel']?>": function() {
                                                        $(this).dialog( "close" );
                                                     },
                                                      "<?=$lng['continue']?>": function() {
                                                           $("form.pager-form input[name=url]").val(url);
                                                           $("form.pager-form input[name=id]").val(id_rec);
                                                           $("form.pager-form").attr("action", action);
                                                           $("form.pager-form").submit(); 
                                                     },
				
                                                }                             		
                                            });             
                                  $dialogDel.dialog( "open" );
                         }
            
                      
                     } else  if ( (act == "edit") || (act == "add")){
               
                            var formAction = '<form method="POST" action="'+ action +'" class="editAction" id="validateForm">';
                   
                            formAction = formAction + '<fieldset><ul>';
                   
                            var i=0;
                            for (var key in keyAndVal) 
                            {
                               var val = keyAndVal [key].split('=');
               
                              if ( val[0] == "id") {
                            
                                    var control = '<input type="hidden" name="'+val[0]+'" value="'+val[1]+'"/>';
                             
                              } else if ( val[0].indexOf("is_")>-1){
                           
                                if  (act == "add"){
                                     
                                     if (val[1]==1){ 
                                          var checkedMode = ' checked="checked"';
                                    
                                     } else {
                                          var checkedMode ='';
                                     }     
                                     
                                
                                } else {
                                    
                                    var mode=$(this).closest("tr").children("td").children("input[name="+ val[0] +"][type=checkbox]").attr("checked");
                                
                                    if (mode) {
                                    
                                          val[1]=1; 
                                          var checkedMode = ' checked="checked"';
                                    
                                     } else {
                                    
                                          val[1]=0;
                                          var checkedMode ='';
                                    }    
                                }
                                                            
                                var control ='<li><label class="desc" for="'+columnName[i]+'">'+columnName[i]+'</label>';
                                    control = control +'<input type="checkbox" name="'+val[0]+'" value="'+val[1]+'" '+ checkedMode +'/><li>';
                            
                            } else {
                          
                                var control ='<li><label class="desc" for="'+columnName[i]+'">'+columnName[i]+'</label>';
                                    control = control +'<input id="name" type="text" name="'+val[0]+'" value="'+val[1]+'" class="field text full"/><li>';
                                                      
                            }
               
                            formAction = formAction + control;
                            i++;    
                            
                      }  
                    
                    formAction = formAction + '</ul></fieldset>';
                    formAction = formAction +'<input type="hidden" name="url" value="'+url+'"/>';
                  
                    formAction = formAction +'</form>';
                              
                    $("#form").attr("title", "<?=$lng['edit']?>");
                    $("#form").html(formAction);
                    
                    <?=$validate?>
               
                    var $dialog = $( "#form" ).dialog({
                                  modal: true,
                                  resizable:false,
                                  buttons: {
                                    "<?=$lng['cancel']?>": function() {
					$(this).dialog( "close" );
                                    },
                                    "<?=$lng['save']?>": function() {
                                        
                                       $("form.editAction").submit().validate();
                                       //$(this).dialog( "close" );
				    }				
                                 }
                    });
		
                    $dialog.dialog( "open" );
                            
                 } 
            
              });
                     
                    
});

 </script>