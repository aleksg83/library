<?if ($this->thead['hidden_check'] || $this->thead['var_sort_old']) {?>

<script type="text/javascript" src="<?=CSSJS?>/default/js/tablesorter.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $("#sort-table")
	.tablesorter({
                widgets: ['zebra'],
		headers: { <?=$this->thead['setting'];?> },
    }).tablesorterPager({container: $("#pager"), size:pageSizeValue});
   
}); 
</script>
 
<?}else{?>
 
<link href="<?=CSSJS?>/default/css/filter.formatter.css" rel="stylesheet" media="all" />
<link href="<?=CSSJS?>/default/css/theme.blue.css" rel="stylesheet" media="all" /> 

<script type="text/javascript" src="<?=CSSJS?>/default/js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?=CSSJS?>/default/js/jquery.tablesorter.widgets.js"></script>
<script type="text/javascript" src="<?=CSSJS?>/default/js/jquery.tablesorter.widgets-filter-formatter.js"></script>
<link href="<?=CSSJS?>/default/css/tiptip.css" rel="stylesheet" media="all" /> 
<script type="text/javascript" src="<?=CSSJS?>/default/js/tiptip/jquery.tiptip.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	/* Table Sorter */
        
   $.tablesorter.addParser({
    id: 'checkbox',
    is: function(s) {
        return false;
    },
    format: function(s, table, cell, cellIndex) {
        var $t = $(table), $c = $(cell), c,

        // resort the table after the checkbox status has changed
        resort = false;

        if (!$t.hasClass('hasCheckbox')) {
            $t
            .addClass('hasCheckbox')
            // make checkbox in header set all others
            .find('thead th:eq(' + cellIndex + ') input[type=checkbox]')
            .bind('change', function(){
                c = this.checked;
                $t.find('tbody tr td:nth-child(' + (cellIndex + 1) + ') input').each(function(){
                  this.checked = c;
                  $(this).trigger('change');
                });
            })
            .bind('mouseup', function(){
                return false;
            });
            $t.find('tbody tr').each(function(){
                $(this).find('td:eq(' + cellIndex + ')').find('input[type=checkbox]').bind('change', function(){
                    $t.trigger('updateCell', [$(this).closest('td')[0], resort]);
                });
            });
        }
        // return 1 for true, 2 for false, so true sorts before false
        c = ($c.find('input[type=checkbox]')[0].checked) ? 1 : 2;
        $c.closest('tr')[ c === 1 ? 'addClass' : 'removeClass' ]('checked');
        return c;
    },
    type: 'numeric'
});
         
	$("#sort-table")
	.tablesorter({
                theme: 'blue',
                widthFixed : true,
		widgets: ['zebra', 'filter'],
		headers: { <?=$this->thead['setting'];?> },
                widgetOptions : {
                // jQuery selector string of an element used to reset the filters
                 filter_reset : 'button.reset',
                 filter_hideFilters : true,
                        
                }        
       }).tablesorterPager({container: $("#pager"), size:pageSizeValue});
     
       $(".tooltips").tipTip();
      
}); 
</script> 

<?}?>

  <script type="text/javascript" src="<?=CSSJS?>/default/js/tablesorter-pager.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	/* Table Sorter 
	$("#sort-table")
	.tablesorter({
                theme: 'blue',
                widthFixed : true,
		widgets: ['zebra', 'filter'],
		headers: { <?=$this->thead['setting'];?> },
                widgetOptions : {
                // jQuery selector string of an element used to reset the filters
                 filter_reset : 'button.reset',
                 filter_hideFilters : true,
                        
                }        
       }).tablesorterPager({container: $("#pager"), size:150}); 
        */
	$(".header").append('<span class="ui-icon ui-icon-carat-2-n-s"></span>');

        });

/* Check all table rows */

var checkflag = "false";
function check(field) {
    if (checkflag == "false") {
        for (i = 0; i < field.length; i++) { field[i].checked = true; }
            checkflag = "true";
            return "check_all"; 
        } else {
            for (i = 0; i < field.length; i++) { field[i].checked = false; }
                checkflag = "false";
        return "check_none"; 
    }
}

</script>
<div class="toolbar">
 <?=$this->tbody['philter_calendar'];?>
 <?=$this->tbody['add'];?>
 <?=$this->tbody['addMass'];?>
 <?=$this->tbody['delete'];?>
 <?=$this->tbody['updateRSS'];?>   
    
 <? for ($i=1; $i<=5; $i++){
    echo $this->tbody['person_'.$i];
    }
    
 ?>   
 </div>

<div class="toolbar right">
    
<?=$this->tbody['philter_date'];?>     
    
<?if (!$this->thead['hidden_check']) {?>
    <button class="btn ui-state-default ui-corner-all tooltip  reset" title="<?=$this->lang['filter_reset']?>"><span class="ui-icon ui-icon-refresh"></span><?=$this->lang['filter_reset_name']?></button> 
<?}?>
    
<?=$this->tbody['right_but'];?>  

<?=$this->tbody['updateRSSSettting'];?>       
    
<?=$this->tbody['philterLang'];?>           
</div> 

<div class="clear"></div>

<? include ("info.php");?>

<?=$this->tbody['text-div']?>

<div class="hastable">
<? if ( (count($this->tbody['tr']) > 0) && ($this->tbody['tr']!="") ) { ?>    
 
 <form name="myform" class="pager-form" method="post" action="">
 <table id="sort-table"> 
    
     <colgroup>
        <?
        if ($this->thead['numb'] == "") $this->thead['numb']=1;
        
         for ($j=0;$j<$this->thead['numb']; $j++){
             echo "<col>";
        }
        ?> 
         
     </colgroup>
     
    <thead> 
     <tr>
    	<?if ( !$this->thead['hidden_check'] && !$this->thead['hidden_check_var2'] ) {?>
          <th class="filter-false"><input type="checkbox" value="check_none" onclick="this.value=check(this.form.list)" class="submit"/></th>
        <?}?>        
	 <?=$this->thead['th']?>
          
         <?if (!$this->thead['hidden_action']) {?>
          <? $act_width = ($this->thead['head_act_width']!="") ? $this->thead['head_act_width'] : '138'?>
          <th style="width:<?=$act_width?>px"  class="filter-false"><?=$this->lang['options']?></th> 
          <?}?>    
    </tr> 
  </thead> 
  <tbody>
       <?=$this->tbody['tr']?>						
  </tbody>
</table>
       
  <input type="hidden" name="id" value=""/>
  <input type="hidden" name="url" value=""/>
          
<div id="pager">					
    <a class="btn_no_text btn ui-state-default ui-corner-all first tooltip" title="<?=$this->lang['firstPage']?>" href="#">
        <span class="ui-icon ui-icon-arrowthickstop-1-w"></span>
    </a>
    <a class="btn_no_text btn ui-state-default ui-corner-all prev tooltip" title="<?=$this->lang['previosPage']?>" href="#">
        <span class="ui-icon ui-icon-circle-arrow-w"></span>
    </a>

    <input type="text" class="pagedisplay"/>

    <a class="btn_no_text btn ui-state-default ui-corner-all next tooltip" title="<?=$this->lang['nextPage']?>" href="#">
        <span class="ui-icon ui-icon-circle-arrow-e"></span>
    </a>
    <a class="btn_no_text btn ui-state-default ui-corner-all last tooltip" title="<?=$this->lang['lastPage']?>" href="#">
        <span class="ui-icon ui-icon-arrowthickstop-1-e"></span>
    </a>

    <select class="pagesize">
        <option value="10">10 <?=$this->lang['records']?></option>
        <option value="25">25 <?=$this->lang['records']?></option>
        <option value="50">50 <?=$this->lang['records']?></option>
        <option value="1000">1000 <?=$this->lang['records']?></option>
    </select>								
</div>
    
    
</form>
<? }else{ ?>    

<div class="response-msg inf ui-corner-all">
<span><?=$this->lang['noRecord']?></span>
</div>
    
<? }?>