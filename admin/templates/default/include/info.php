<!-- вывод сообщений о результатах операций -->
<? 
 //--------------------------------------------------------------------------------------------
  if (!empty($this->operation)){
     $infoOperation="";
     $show=false;
     $showError=false;
     foreach ($this->operation as $key => $value) {
           
         if ( ($key=="delete") & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['deleteInfo']."<br/>";
                $show=true;
                
         } else if ( ($key=="deletes") & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['deleteSinfo']."<br/>";
                 $show=true;
                
         } else if ( (($key=="edit")|| ($key=="update")) & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['editInfo']."<br/>";
                 $show=true;
         
         } else if ( (($key=="edit")|| ($key=="update")) & ($value=="false") ) {
               
                $infoOperation.=$this->lang['editInfoFalse']."<br/>";
                $show=false;
                $showError=true;
                
         } else if ( (($key=="edit")|| ($key=="update")) & ($value=="falsepass") ) {
               
                $infoOperation.=$this->lang['editInfoFalsePass']."<br/>";
                $show=false;
                $showError=true;
         
                     
         }  else if ( ($key=="add") & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['addInfo']."<br/>";
                $show=true;
                
         }  else if ( ($key=="import") & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['importOk']."<br/>";
                $show=true;
                
         }  else if ( ($key=="upload") & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['uploadOk']."<br/>";
                $show=true;
                
         
         } else if ( ($key=="updateSetRss") & ($value=="false") ) {
                $infoOperation.=$this->lang['updateSetRSSFalse']."<br/>";
                $show=false;
                $showError=true;
         } else if ( ($key=="updateSetRss") & ($value=="ok") ) {
                $infoOperation.=$this->lang['updateSetRSSTrue']."<br/>";
                $show=true;
                $showError=false;
        
                
         }  else if ( ($key=="creatFile") & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['creatFile_ok']."<br/>";
                $show=true;
       
         }  else if ( ($key=="creatFile") & ($value=="no") ) {
               
                $infoOperation.=$this->lang['creatFile_no']."<br/>";
                $show=true;
         }  else if ( ($key=="emptyGurnal") & ($value=="ok") ) {
               
                $infoOperation.=$this->lang['emptyGurnal_ok']."<br/>";
                $show=true;
       
         }  else if ( ($key=="emptyGurnal") & ($value=="no") ) {
               
                $infoOperation.=$this->lang['emptyGurnal_no']."<br/>";
                $show=true;
         }   
         
         
         
         
                 
     }

    if ($show && !$showError){ 
?>   

    <div class="response-msg inf ui-corner-all" id="infooperation">
     <span><?=$infoOperation?></span>
    </div>
<?
    }
    
 if (!$show && $showError){ 
?>   

    <div class="response-msg error ui-corner-all" id="infooperation">
     <span><?=$infoOperation?></span>
    </div>
<?
    }   
    
  }
  //---------------------------------------------------------------------------------------------------
?>