		<div id="sidebar">
			<div class="sidebar-content">
				<a id="close_sidebar" class="btn ui-state-default full-link ui-corner-all" href="#drill">
					<span class="ui-icon ui-icon-circle-arrow-e"></span>
					<?=$this->lang['closeBar']?>
				</a>
				<a id="open_sidebar" class="btn tooltip ui-state-default full-link icon-only ui-corner-all" title="<?=$this->lang['openBar']?>" href="#"><span class="ui-icon ui-icon-circle-arrow-w"></span></a>
				<div class="hide_sidebar">
					
                                      <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div class="portlet-header ui-widget-header"><?=$this->lang['site']?><span class="ui-icon ui-icon-circle-arrow-s"></span></div>
						<div class="portlet-content">
							<ul id="style-switcher" class="side-menu">
                                                             
                                                             <li><a href="<?=SITE_URL?>" target="_blank"><?=$this->lang['goSite']?></a></li>
                                                         
                                                         <?
                                                           $this->auth = auth::instance(); 
                                                           if ($this->auth->isAdmin) {
                                                         ?>    
                                                              <? if (Common::isBlock()) { ?> 
                                                                    <li><a href="javascript:void(0)" class="BlockSite" mode="0"><?=$this->lang['blockSiteVk']?></a></li>
                                                              <? }else{ ?>
                                                                    <li><a href="javascript:void(0)" class="BlockSite" mode="1"><?=$this->lang['blockSite']?></a></li>
                                                              <? } ?>
                                                           <?}?>	
							</ul>
						</div>
					</div>
					
				    		<div class="box ui-widget ui-widget-content ui-corner-all">
						<h3><?=$this->lang['fast_enter']?></h3>
						<div class="content">
                                                  
                                                  <? if ( in_array("PRINTNEWS", $_SESSION['role']) || in_array("PRINTRESURS", $_SESSION['role']) || in_array("ADMIN", $_SESSION['role'])){?>   
                                                    <a class="btn ui-state-default full-link ui-corner-all" href="/admin/showPredPrint/">
								<span class="ui-icon ui-icon-print"></span>
								<?=$this->lang['predprint']?> (<?=Common::CountPredPrint($_SESSION['user']['user_id']);?>)
							</a>
                                                  <?}?>   
							<a class="btn ui-state-default full-link ui-corner-all" href="/admin/showNews/">
								<span class="ui-icon ui-icon-signal-diag"></span>
								<?=$this->lang['newsname']?>
							</a>
							<a class="btn ui-state-default full-link ui-corner-all" href="/admin/showPerson/">
								<span class="ui-icon ui-icon-person"></span>
								<?=$this->lang['persons']?>
							</a>
							<a class="btn ui-state-default full-link ui-corner-all" href="/admin/showLib/book/">
								<span class="ui-icon ui-icon-bookmark"></span>
								<?=$this->lang['lib_book']?>
							</a>
							<a class="btn ui-state-default full-link ui-corner-all" href="/admin/showLib/article/">
								<span class="ui-icon ui-icon-clipboard"></span>
								<?=$this->lang['lib_article']?>
							</a>
                                                    <? if ($this->auth->isAdmin) { ?>
							<a class="btn ui-state-default full-link ui-corner-all" href="/admin/showEvents/1/">
								<span class="ui-icon ui-icon-calendar"></span>
								<?=$this->lang['lib_calendarj_short']?>
							</a>
                                                    <?}?> 
						</div>
					</div>
                                    
                                    
					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div class="portlet-header ui-widget-header"><?=$this->lang['layoutWidth']?><span class="ui-icon ui-icon-circle-arrow-s"></span></div>
						<div class="portlet-content">
							<ul class="side-menu layout-options">
								<li>
									<a href="javascript:void(0);" id="" title="Switch to 100% width layout"><b>100%</b> width</a>
								</li>
								<li>
									<a href="javascript:void(0);" id="layout90" title="Switch to 90% width layout"><b>90%</b> width</a>
								</li>
								<li>
									<a href="javascript:void(0);" id="layout75" title="Switch to 75% width layout"><b>75%</b> width</a>
								</li>
								<li>
									<a href="javascript:void(0);" id="layout980" title="Switch to 980px layout"><b>980px</b> width</a>
								</li>
								<li>
									<a href="javascript:void(0);" id="layout1280" title="Switch to 1280px layout"><b>1280px</b> width</a>
								</li>
								<li>
									<a href="javascript:void(0);" id="layout1400" title="Switch to 1400px layout"><b>1400px</b> width</a>
								</li>
								<li>
									<a href="javascript:void(0);" id="layout1600" title="Switch to 1600px layout"><b>1600px</b> width</a>
								</li>
							</ul>
						</div>
					</div>
                                    
					<div class="clear"></div>
					
					
				</div>
			</div>
		</div>
		<div class="clear"></div>