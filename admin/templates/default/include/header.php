<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?=$this->lang['cmsName'].": ".$this->namePage?></title>
       
               
        <script type="text/javascript" src="<?=CSSJS?>/default/js/jquery-1.4.4.min.js"></script>
        
        
 	<script type="text/javascript" src="<?=CSSJS?>/default/js/ui/ui.core.js"></script>
	<script type="text/javascript" src="<?=CSSJS?>/default/js/superfish.js"></script>
	<script type="text/javascript" src="<?=CSSJS?>/default/js/live_search.js"></script>
	<!--<script type="text/javascript" src="<?=CSSJS?>/default/js/sidebar_menu.js"></script>-->
	<script type="text/javascript" src="<?=CSSJS?>/default/js/tooltip.js"></script>
	<script type="text/javascript" src="<?=CSSJS?>/default/js/cookie.js"></script>
	<script type="text/javascript" src="<?=CSSJS?>/default/js/ui/ui.sortable.js"></script> 
        
        
	<script type="text/javascript" src="<?=CSSJS?>/default/js/ui/ui.draggable.js"></script>
       <!--<script type="text/javascript" src="<?=CSSJS?>/default/js/ui/ui.droppable.js"></script>-->
        
        
        <script type="text/javascript" src="<?=CSSJS?>/default/js/ui/ui.datepicker.js"></script>
                
       	<script type="text/javascript" src="<?=CSSJS?>/default/js/ui/ui.resizable.js"></script>
	<script type="text/javascript" src="<?=CSSJS?>/default/js/ui/ui.dialog.js"></script> 
       
       <script type="text/javascript" src="<?=CSSJS?>/default/js/jquery.colorbox.js"></script>
       <!--<script type="text/javascript" src="<?=CSSJS?>/default/js/jquery.tablednd.js"></script>-->
              
       <script type="text/javascript" src="<?=CSSJS?>/default/js/ui/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="<?=CSSJS?>/default/js/jquery-ui-sliderAccess.js"></script>
              
       
        <? 
          echo '<script type="text/javascript">';
               $tree = ($this->tbody['tree'])?'var treeStructure = true;':'var treeStructure = false;';                    
           echo $tree.'</script>'; 
       ?>
        
        <script type="text/javascript" src="<?=CSSJS?>/default/js/custom.js"></script> 
       
        <script type="text/javascript" src="<?=CSSJS?>/default/js/drogtable/jquery.tabledrag.js"></script>
        <link href="<?=CSSJS?>/default/js/drogtable/assets/jquery.tabledrag.css" rel="stylesheet" media="all" />
        
        <link type="text/css" href="<?=CSSJS?>/default/uploadify/uploadify.css" rel="stylesheet">
        <script type="text/javascript" src="<?=CSSJS?>/default/js/jquery.uploadify.js"></script>
        
        
        <script src="<?=CSSJS?>/default/js/alerts/jquery.alerts.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?=CSSJS?>/default/js/alerts/jquery.alerts.css" type="text/css" />
        
        
        <link href="<?=CSSJS?>/default/css/colorbox.css" rel="stylesheet" media="all" />
               
	<link href="<?=CSSJS?>/default/css/ui/ui.base.css" rel="stylesheet" media="all" />
        <link href="<?=CSSJS?>/default/css/ui/jquery-ui-timepicker-addon.css" rel="stylesheet" media="all" />
        <link href="<?=CSSJS?>/default/css/themes/black_rose/ui.css" rel="stylesheet" media="all" />
        
      	<!--[if IE 6]>
	<link href="<?=CSSJS?>/default/css/ie6.css" rel="stylesheet" media="all" />
	
	<script src="<?=CSSJS?>/default/js/pngfix.js"></script>
	<script>
	  /* Fix IE6 Transparent PNG */
	  DD_belatedPNG.fix('.logo, .other ul#dashboard-buttons li a');

	</script>
	<![endif]-->
	<!--[if IE 7]>
	<link href="<?=CSSJS?>/default/css/ie7.css" rel="stylesheet" media="all" />
	<![endif]-->
        
         
      <script type="text/javascript" src="/modules/ckeditor4.1/ckeditor.js"></script>
      <script type="text/javascript" src="/modules/ckeditor4.1/dialog-patch.js"></script>
      <script type="text/javascript" src="/modules/AjexFileManager/ajex.js"></script>
      
      <link href="<?=SITE_URL?>favicon.ico" rel="shortcut icon">
      <link href="<?=SITE_URL?>favicon.ico" rel="shortcut icon" type="image/x-icon">
   
</head>
<body>

    <div id="page_wrapper">
   		<div id="page-header">
			<div id="page-header-wrapper">
				<div id="top">
					<a href="/admin/" class="logo" title="<?=$this->lang['cmsName']." ".version?>"><?=$this->lang['cmsName']?></a>
					
                    <? if(!$this->isLogged) echo "</div>";?>                  
                    <? if($this->isLogged) { ?>

                    
                                    <div class="welcome">
						<span class="note"><?=$this->lang['welcome']?> <a href="/admin/profile/" title="<?=$_SESSION['user']['name']?>"><?=$_SESSION['user']['fam']." ".$_SESSION['user']['name']." (".$_SESSION['user']['login'].")";?></a></span>
						<!--<a class="btn ui-state-default ui-corner-all" href="#">
							<span class="ui-icon ui-icon-wrench"></span>
							Настройки
						</a>-->
						<a class="btn ui-state-default ui-corner-all" href="/admin/profile/">
							<span class="ui-icon ui-icon-person"></span>
                                                        <?=$this->lang['profile']?>
							
                                                        
						</a>
						<a class="btn ui-state-default ui-corner-all" href="/admin/logout">
							<span class="ui-icon ui-icon-power"></span>
							<?=$this->lang['exit'];?>
						</a>						
					</div>
                     
                      
				</div>
                            
				<?=$this->mainMenuList;?>
				                        
                             <?}?>
                                
                             
                            
			</div>
		</div>
        
 