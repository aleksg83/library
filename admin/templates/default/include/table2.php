<script type="text/javascript" src="<?=CSSJS?>/default/js/tablesorter.js"></script>
<script type="text/javascript" src="<?=CSSJS?>/default/js/tablesorter-pager.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	/* Table Sorter */
        $("#sort-table")
	.tablesorter({
		widgets: ['zebra'],
		headers: { <?=$this->thead['setting'];?> } 
	}).tablesorterPager({container: $("#pager"), size:pageSizeValue}); 
        
	$(".header").append('<span class="ui-icon ui-icon-carat-2-n-s"></span>');
});

/* Check all table rows */

var checkflag = "false";
function check(field) {
    if (checkflag == "false") {
        for (i = 0; i < field.length; i++) { field[i].checked = true; }
            checkflag = "true";
            return "check_all"; 
        } else {
            for (i = 0; i < field.length; i++) { field[i].checked = false; }
                checkflag = "false";
        return "check_none"; 
    }
}


</script>
<div class="toolbar">
 <?=$this->tbody['add'];?>
 <?=$this->tbody['addMass'];?>
 <?=$this->tbody['delete'];?>
</div>
<div class="clear"></div>

<? include ("info.php");?>

<div class="hastable">
<? if ( (count($this->tbody['tr']) > 0) && ($this->tbody['tr']!="") ) { ?>    
 <form name="myform" class="pager-form" method="post" action="">
 <table id="sort-table" <?=$this->thead['style']?>> 
    <thead> 
     <tr>
    	 <?=$this->thead['th']?>
    </tr> 
  </thead> 
  <tbody>
       <?=$this->tbody['tr']?>						
  </tbody>
</table>
       
  <input type="hidden" name="id" value=""/>
  <input type="hidden" name="url" value=""/>
          
<div id="pager">					
    <a class="btn_no_text btn ui-state-default ui-corner-all first" title="<?=$lng['firstPage']?>" href="#">
        <span class="ui-icon ui-icon-arrowthickstop-1-w"></span>
    </a>
    <a class="btn_no_text btn ui-state-default ui-corner-all prev" title="<?=$lng['previosPage']?>" href="#">
        <span class="ui-icon ui-icon-circle-arrow-w"></span>
    </a>

    <input type="text" class="pagedisplay"/>

    <a class="btn_no_text btn ui-state-default ui-corner-all next" title="<?=$lng['nextPage']?>" href="#">
        <span class="ui-icon ui-icon-circle-arrow-e"></span>
    </a>
    <a class="btn_no_text btn ui-state-default ui-corner-all last" title="<?=$lng['lastPage']?>" href="#">
        <span class="ui-icon ui-icon-arrowthickstop-1-e"></span>
    </a>

    <select class="pagesize">
        <option value="10">10 <?=$lng['records']?></option>
        <option value="25" selected="selected">25 <?=$lng['records']?></option>
        <option value="50">50 <?=$lng['records']?></option>
        <option value="1000">1000 <?=$lng['records']?></option>
    </select>								
</div>
    
    
</form>
<? }else{ ?>    

<div class="response-msg inf ui-corner-all">
<span><?=$this->lang['noRecord']?></span>
</div>
    
<? }?>