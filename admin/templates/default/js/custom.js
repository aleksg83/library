var actionControlPanel = "";

$(document).ready(function() {

    // Navigation menu
    urlServer = window.location.protocol+ '//' +window.location.hostname+"/";
    
	$('ul#navigation').superfish({ 
		delay:       1000,
		animation:   {opacity:'show',height:'show'},
		speed:       'fast',
		autoArrows:  true,
		dropShadows: false
	});

	$('ul#navigation li').hover(function(){
		$(this).addClass('sfHover2');
	},
	function(){
		$(this).removeClass('sfHover2');
	});
	
	// Live Search
	
	jQuery('#search-bar input[name="q"]').liveSearch({url: 'live_search.php?q='});
	
	//Hover states on the static widgets

	$('.ui-state-default').hover(
		function() { $(this).addClass('ui-state-hover'); }, 
		function() { $(this).removeClass('ui-state-hover'); }
	);

	//Sortable portlets

	$('.sortable .column').sortable({
		cursor: "move",
		connectWith: '.sortable .column',
		dropOnEmpty: false
	});

	$(".column").disableSelection();

	//Sidebar only sortable boxes
	$(".side_sort").sortable({
		axis: 'y',
		cursor: "move",
		connectWith: '.side_sort'
	});

	
	//Close/Open portlets
	$(".portlet-header").hover(function() {
		$(this).addClass("ui-portlet-hover");
	},
	function(){
		$(this).removeClass("ui-portlet-hover");
	});

	$(".portlet-header .ui-icon").click(function() {
		$(this).toggleClass("ui-icon-circle-arrow-n");
		$(this).parents(".portlet:first").find(".portlet-content").toggle();
	});


	// Sidebar close/open (with cookies)

	function close_sidebar() {
		
		$("#sidebar").addClass('closed-sidebar');
		$("#page_wrapper #page-content #page-content-wrapper").addClass("no-bg-image wrapper-full");
		$("#open_sidebar").show();
		$("#close_sidebar, .hide_sidebar").hide();
	}

	function open_sidebar() {
		$("#sidebar").removeClass('closed-sidebar');
		$("#page_wrapper #page-content #page-content-wrapper").removeClass("no-bg-image wrapper-full");
		$("#open_sidebar").hide();
		$("#close_sidebar, .hide_sidebar").show();
                
	}

	$('#close_sidebar').click(function(){
		close_sidebar();
		if($.browser.safari) {
		    location.reload();
		}
                
              $.cookie('sidebarPharus', 'closed', {path: "/"} );
     	      $(this).addClass("active");
	});
	
	$('#open_sidebar').click(function(){
            	open_sidebar();
		if($.browser.safari) {
		    location.reload();
		}
		$.cookie('sidebarPharus', 'open', {path: "/"} );
	});
	
	var sidebar = $.cookie('sidebarPharus');

	if (sidebar == 'closed') {
              close_sidebar();
	}else if (sidebar == 'open') {
              open_sidebar();
	 }
        

	/* Tooltip */

	$(function() {
		$('.tooltip').tooltip({
			track: true,
			delay: 0,
			showURL: false,
			showBody: " - ",
			fade: 250
			});
		});
		
	/* Theme changer - set cookie */

    $(function() {

$(".group").colorbox({rel:'group', transition:"fade"});   

        $('a.set_theme').click(function() {
           	var theme_name = $(this).attr("id");
			$('body').append('<div id="theme_switcher" />');
			$('#theme_switcher').fadeIn('fast');

			setTimeout(function () { 
				$('#theme_switcher').fadeOut('fast');
			}, 2000);

			setTimeout(function () { 
			$("link[title='style']").attr("href","css/themes/" + theme_name + "/ui.css");
			}, 500);

			$.cookie('theme', theme_name );

			$('a.set_theme').removeClass("active");
			$(this).addClass("active");
			
        });
		
		var theme = $.cookie('theme');

		$("a.set_theme[id="+ theme +"]").addClass("active");
	    
		if (theme == 'black_rose') {
	        $("link[title='style']").attr("href","css/themes/black_rose/ui.css");
	        
	    };

		if (theme == 'gray_standard') {
	        $("link[title='style']").attr("href","css/themes/gray_standard/ui.css");
	    };

		if (theme == 'gray_lightness') {
	        $("link[title='style']").attr("href","css/themes/gray_lightness/ui.css");
	    };
	    
		if (theme == 'blueberry') {
	        $("link[title='style']").attr("href","css/themes/blueberry/ui.css");
	    };
	    
		if (theme == 'apple_pie') {
	        $("link[title='style']").attr("href","css/themes/apple_pie/ui.css");
	    };

    });
    
	/* Layout option - Change layout from fluid to fixed with set cookie */

    $(function() {

		$('.layout-options a').click(function(){
			var lay_id = $(this).attr("id");
			$('body').attr("class",lay_id);
			$("#page-layout, #page-header-wrapper, #sub-nav").addClass("fixed");
			//$.cookie('layout', lay_id );
                        $.cookie('layoutPharus', lay_id, {path: "/"} );
			$('.layout-options a').removeClass("active");
			$(this).addClass("active");
		})
			
	    var lay_cookie = $.cookie('layoutPharus');

		$(".layout-options a[id="+ lay_cookie +"]").addClass("active");

		if (lay_cookie == 'layout100') {
			$('body').attr("class","");
			$("#page-layout, #page-header-wrapper, #sub-nav").removeClass("fixed");
	    };

		if (lay_cookie == 'layout90') {
			$('body').attr("class","layout90");
			$("#page-layout, #page-header-wrapper, #sub-nav").addClass("fixed");
	    };
	    
		if (lay_cookie == 'layout75') {
			$('body').attr("class","layout75");
			$("#page-layout, #page-header-wrapper, #sub-nav").addClass("fixed");
	    };
	    
		if (lay_cookie == 'layout980') {
			$('body').attr("class","layout980");
			$("#page-layout, #page-header-wrapper, #sub-nav").addClass("fixed");
	    };
	    
		if (lay_cookie == 'layout1280') {
			$('body').attr("class","layout1280");
			$("#page-layout, #page-header-wrapper, #sub-nav").addClass("fixed");
	    };
	    
		if (lay_cookie == 'layout1400') {
			$('body').attr("class","layout1400");
			$("#page-layout, #page-header-wrapper, #sub-nav").addClass("fixed");
	    };
	    
		if (lay_cookie == 'layout1600') {
			$('body').attr("class","layout1600");
			$("#page-layout, #page-header-wrapper, #sub-nav").addClass("fixed");
	    };

    });

	// Dialog			

	$('#dialog').dialog({
		autoOpen: false,
		width: 600,
		bgiframe: false,
		modal: false,
		buttons: {
			"Ok": function() { 
				$(this).dialog("close"); 
			}, 
			"Cancel": function() { 
				$(this).dialog("close"); 
			} 
		}
	});

	// Modal Confirmation		

		$("#modal_confirmation").dialog({
			autoOpen: false,
			bgiframe: true,
			resizable: false,
			width:500,
			modal: true,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			},
			buttons: {
				'Delete all items in recycle bin': function() {
					$(this).dialog('close');
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			}
		});
	
	// Dialog Link

	$('#dialog_link').click(function(){
		$('#dialog').dialog('open');
		return false;
	});
	
	// Modal Confirmation Link

	$('#modal_confirmation_link').click(function(){
		$('#modal_confirmation').dialog('open');
		return false;
	});
	
	// Same height

	var sidebarHeight = $("#sidebar").height();
	$("#page-content-wrapper").css({"minHeight" : sidebarHeight });

	// Simple drop down menu

	var myIndex, myMenu, position, space=20;
	
	$("div.sub").each(function(){
		$(this).css('left', $(this).parent().offset().left);
		$(this).slideUp('fast');
	});
	
	$(".drop-down li").hover(function(){
		$("ul",this).slideDown('fast');
		
		//get the index, set the selector, add class
		myIndex = $(".main1").index(this);
		myMenu = $(".drop-down a.btn:eq("+myIndex+")");
	}, function(){
		$("ul",this).slideUp('fast');
	});

              
        //------------------------------------------------------------------------------------------------
        $( "input[type=checkbox][name!=list]" ).live( "click", function() {
                    
           if ($(this).attr("checked")) $(this).val("1");
           else $(this).val("0");
           
        });
        //------------------------------------------------------------------------------------------------- 
       
        //------------------------------------------------------------------------------------------------
        $.datepicker.regional['ru'] = {
            closeText: 'Применить',
            prevText: '<Пред',
            nextText: 'След>',
            currentText: 'Сегодня',
            monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
            dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
            dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            weekHeader: 'Не',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['ru']);


        $.timepicker.regional['ru'] = {
            timeOnlyTitle: 'Выберите время',
            timeText: 'Время',
            hourText: 'Часы',
            minuteText: 'Минуты',
            secondText: 'Секунды',
            millisecText: 'Миллисекунды',
            timezoneText: 'Часовой пояс',
            currentText: 'Сейчас',
            closeText: 'Выбор',
            timeFormat: 'HH:mm',
            amNames: ['AM', 'A'],
            pmNames: ['PM', 'P'],
            isRTL: false
        };
        $.timepicker.setDefaults($.timepicker.regional['ru']);
      
       //-------------------------------------------------------------------------------------------------
       $(".art_type").live( "click", function() {
           var type=$(this).val();
           //type == 0 //часть издания
           //type == 1 //книга
           
           if (type==0){
               $(".type0").css("display", "block");
               $(".type1").css("display", "none");
           } else {
               $(".type1").css("display", "block");
               $(".type0").css("display", "none");
           }
           
       });
       
       $(".dis_type").live( "click", function() {
           var type=$(this).val();
           //type == 0 //диссертация
           //type == 1 //автореферат
           
           if (type=="0"){
               $(".type0").css("display", "block");
               $(".type1").css("display", "none");
           } else {
               $(".type1").css("display", "block");
               $(".type0").css("display", "none");
           }
           
       });
       
       $(".multy-load").live( "click", function() {
           
           if ($(this).hasClass('close-multi')){
               $(this).removeClass("close-multi");
               $(this).addClass("open");
               
               $(".container-upload").addClass("open-div");
           }else{
               $(this).removeClass("open");
               $(this).addClass("close-multi");
               
               $(".container-upload").removeClass("open-div");
           }
           
       });
       
       
       
       //-------------------------------------------------------------------------
               pageSizeValue = (treeStructure)? 1000 : $.cookie("pharusLibraryPage"); 
               
               if (pageSizeValue === null) {
                   //pageSizeValue = setCookie("pharusLibraryPage", "25");
                   $.cookie("pharusLibraryPage", "25", {
                    path: "/"
                    });
                   pageSizeValue="25";
               } 
                
              $(".pagesize  [value='"+ pageSizeValue +"']").attr("selected", "selected");
              
               $(".pagesize").change(function(){
                   var select = $(this).val(); 
                   $.cookie("pharusLibraryPage", select, {
                    path: "/"
                    });
                   pageSizeValue=select;
                 
               });
             
              //-------------------------------------------------------------------------
               $(".senderRassilka").click(function(){
                
                   var id = $(this).attr("id_record"); //id rassulka
                   var type = $(this).attr("type"); //type sender
                   var time_packet = parseInt($(this).attr("time_packet"))*60; //время на отправку пакета в секундах
                  // var kolvo_pisem = $(this).attr("kolvo_pisem"); //максимальное кол-во писем в пакете
                  // var kolvo_addr = $(this).attr("kolvo_addr"); //максимальное кол-во адресатов в пакете
                   var countPacet = $(this).attr("countPacet"); //кол-во пакетов
                   var countUserPacet = $(this).attr("countUserPacet"); //кол-во пользователей в пакете
                   
                   $(".blockSendSet").css("display","none");
                   $(".listInformSender").css("display","block");
                   var user = [];
                   
                  
                  var timeStartAction = new Date; //старт скрипта отправки 
                  
                  var $container = $(".listInformSender");
                  //-----------------------------------------------------------
                  //list user
                   $.ajax({
     			  url: "/Adminajax/getListUserSend/",
			  async : false,
      			  type: "POST",
                          data:{ id: id},
                          dataType: "json",
      			  timeout: 6000000,
                          beforeSend: function(){
                             	
		          },
      			  success: function(data){
                               $.each(data, function(key, val) {
                                     user.push(val);
				});
                                
			  }, 
	  		  error: function(xhr, status){
                             
                          }	
   			 });
                  
                  
                  //************! функция ajax-отправки !***********************
                  function sendUserMessageAjax(i){
                      
                      $.ajax({
     			  url: "/Adminajax/sendActionPacets/",
			  async : false,
      			  type: "POST",
                          data:{ id: id, idUser:user[i]['id_user_email'], name:user[i]['name'], email:user[i]['email'], sex:user[i]['sex']},
      			  timeout: 6000000,
                          beforeSend: function(){
                             	
		          },
      			  success: function(data){
                              /* if(i%1000 == 0)
                               {
                                   $container.html(data);
                               }else{ */
                                    $container.append(data);
                                    $container[0].scrollTop = $container[0].scrollHeight;
                                  
                               //}
                               
                               if (i==(user.length-1)) {
                                   
                                   $.ajax({
                                    url: "/Adminajax/setSendStatus/",
                                    async : false,
                                    type: "POST",
                                    data:{ id: id, status:"SEND"},
                                    timeout: 6000000,
                                    beforeSend: function(){
                             	
                                    },
                                    success: function(data){
                                        //перевести рассылку в архив!!!
                                        $container.append(data);
                                        $container[0].scrollTop = $container[0].scrollHeight;
                                        $(".button_next").css("display", "block");
                                        
                                        
                                        var IntervalAction = new Date - timeStartAction;
                                        var RetunIntervalSec=(IntervalAction / 1000); //возращаем в секундах
                                        var RetunIntervalMin=(IntervalAction / 1000) / 60; //возращаем в минутах
                    
                                        $container.append("<p><b>Рассылка выполнена за "+ RetunIntervalMin.toFixed(2) +" мин. ("+RetunIntervalSec.toFixed(2)+" сек.)</b></p>");
                                        $container[0].scrollTop = $container[0].scrollHeight;
                                        
                                  },
                                  error: function(xhr, status){
                                  
                                   }	
                                 });        
                                          
                               }//if 
			  }, 
	  		  error: function(xhr, status){
                                   //в случае ошибки в перевести в статус "Ошибка"
                                   $.ajax({
                                    url: "/Adminajax/setSendStatus/",
                                    async : false,
                                    type: "POST",
                                    data:{ id: id, status:"ERROR"},
                                    timeout: 6000000,
                                    beforeSend: function(){
                             	
                                    },
                                    success: function(data){
                                        $container.append(data);
                                  },
                                  error: function(xhr, status){
                                  
                                   }	
                                 });        
                                   
                          }	
   			 });
                      
                  }
                  //************! функция ajax-отправки !***********************
               
                  
                    
                  //без ограничений
                 //--------------------------1------------------------------------
                 if (type=="1") {
                   for (var i=0; i<user.length; i++){
                       sendUserMessageAjax(i);
                   }
                 } //if type==1 ------------------------------------------------ 
                 
               
                //---------------- пакетная отправка ---------------------------
                if (type=="2") {
                  
                  //countPacet
                  var nPacet=1; //начинаем отправку с первого пакета
                  
                  function sendPakets(){
                   if (nPacet<=countPacet){  
                     var start=(nPacet-1)*countUserPacet;//index пользователя, с кого начинаем отправку
                     var end=nPacet*countUserPacet; //index пользователя, перед кем заканчиваем
                     if (end>user.length) end=user.length;
                     
                     $("p.removeTime_"+ (nPacet-1) +"").remove(); //очистка работы таймера прошлого пакета
                     $container.append("<p>Отправка пакета № "+ nPacet +"....</p>");
                     $container[0].scrollTop = $container[0].scrollHeight;
                     
                     var timeStartPaket = new Date; //старт отправки пакета
                     //================================
                     for (var i=start; i<end; i++){
                        sendUserMessageAjax(i);
                     }//for user
                     //================================== 
                      var IntervalSender = new Date - timeStartPaket; //время отправки текущего пакета
                      var IntervalPaket= time_packet - (IntervalSender / 1000) //оставшееся время  на отправку пакета в секундах
                      var countSecond= Math.ceil(IntervalPaket);
                      $container.append("<p class=\"removeTime_"+nPacet+"\">До отправки следущего пакета осталось <b><span class=\"TimerPaket\">"+ countSecond +"</span></b> сек.</p>"); 
                      $container[0].scrollTop = $container[0].scrollHeight;
                       
                      var timer2 = setInterval(function() { 
                            $(".TimerPaket").html(countSecond); 
                            countSecond--; 
                            if (countSecond<1) {
                               clearInterval(timer2); 
                            }
                      }, 1000);
                      
                      nPacet++; //счетчик номера пакета увеличиваем
                      
                   }
                     
                     //все пакеты отправлены
                     if (nPacet>countPacet) {
                       clearInterval(timer); 
                       $("p.removeTime_"+ (nPacet-1) +"").remove(); //очистка работы таймера последнего пакета
                     }  
                  }
                  
                  sendPakets();//отправляем первый пакет
                  var timer = setInterval(sendPakets, time_packet*1000);
                
                } //if type==2 
                //---------------- пакетная отправка --------------------------- 
                                   
               });
               //!!!!!!!!!!!!!!!!!!!!!!!! функция отправки сообщений !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               
              
              //-------------------------------------------------------------------------
               $(".BlockSite").click(function(){
                   var mode=$(this).attr("mode");
                   var text = (mode=="0")?  "Вы действительно хотите включить сайт?" : "Вы действительно хотите отключить сайт?" ;
                   
                   jConfirm(text, "Внимание!", function(r) {
 			
			if (r)
			{
				$.ajax({
                                     url: "/Adminajax/setBlockSite/",
                                     //async : false,
                                     type: "POST",
                                     data:{ value: mode},
                                     //dataType: "json",
                                     timeout: 6000000,
                                     beforeSend: function(){
                             	
                                     },
                                     success: function(data){
                                         if (data=="1"){    
                                            var title = (mode=="0")? "Отключить сайт" : "Включить сайт" ;
                                            var message = (mode=="0")? "Сайт включен" : "Сайт отключен" ;
                                            var modeNew = (mode=="0")?1:0;
                                            
                                             $(".BlockSite").attr("mode", modeNew);
                                             $(".BlockSite").text(title);
                                             
                                             jAlert(message, 'Отключение/вjAlerключение сайта');
                                             //alert(data);
                                         } else {
                                              var messageError = (mode=="0")? " не включен" : " не отключен" ;
                                              jAlert('Ошибка операции. Портал '+ messageError +'', 'Отключение/включение сайта');
                                         }  
                                
                                     }, 
                                     error: function(xhr, status){
                             
                                     }	
                                });
                      	     
			}
	
 			});
                   
                   
                   
               });  
               
           //-------------------------------------------------------------------    
           
           $("select.philterLang").live("change", function() {
                var value = $(this).val();
                window.location.href=urlServer+"admin/redactSlovar/?file="+value;
                //var level = $(this).children("option:selected").attr("level");
                                
              });
          
          //-------------------------------------------------------------------- 
           $("input.settingSetCheck").live("click", function() {
               var idRec=parseInt($(this).attr("id_record"), 10);
               var action=$(this).attr("action");
               var value = parseInt($(this).val(), 10);
               
               $.post( action, 
                             {id: idRec, value:value}
               );     
               
           });
           
           $("input.settingSetInp").live("keypress focusout",function(eventObject) {
  	
                if ( (eventObject.which == 13) || (eventObject.type =="focusout") ){
                      var idRec=parseInt($(this).attr("id_record"), 10);
                      var action=$(this).attr("action");
                      var value = $(this).val();
                       $.post( action, 
                             {id: idRec, value:value}
               );  
                }
           
          });   
         
          //-------------------------------------------------------------------
           $(".arhiv").click(function(){
                   var type=$(this).attr("type");
                   var action=$(this).attr("action");
                   var url=$(this).attr("url");
                       url=url.substring(1); 
                   var text = (type=="zip")?  "Вы действительно хотите создать архив сайта?" : "Вы действительно хотите создать дамп базы данных сайта?" ;
                   var $obj = $(this);   
                             
                  jConfirm(text, "Внимание!", function(r) {
 			
			if (r)
			{
                           $obj.find("span.act").removeClass("close");   
                           
                           $.ajax({
                                     url: action,
                                     //async : false,
                                     type: "POST",
                                     data:{type: type},
                                     timeout: 6000000,
                                     beforeSend: function(){
                             	
                                     },
                                     success: function(data){
                                           $obj.find("span.act").addClass("close"); 
                                           if (data=="1"){
                                                window.location.href=urlServer+url+'?creatFile=ok';
                                           }else{
                                                window.location.href=urlServer+url+'?creatFile=no';
                                           }     
                                     }, 
                                     error: function(xhr, status){
                                           window.location.href=urlServer+url+'?creatFile=no'; 
                                     }	
                                });
                          
                        }
                  }); 
          });
          //--------------------------------------------------------------------
          function getUrlAction(){
                if (window.location.port.toString().length > 0){
			var dop_count = window.location.port.toString().length + 1;
		} else{
			var dop_count = 0;
		}
                 var len = window.location.protocol.toString().length+2 + window.location.hostname.toString().length + 1 + dop_count;
                 var query = window.location.href.substring(len);
	  
                var vars = query.split("/");
	
               if ( (vars.length > 0) && (query.length>0) ) {
                            return vars[1];
               }else{
		   return "no_action";   
               }
	   
            }
          
          actionControlPanel = getUrlAction();
          //--------------------------------------------------------------------
           $(".treerubric").live("click", function() {
               var idTop=$(this).parent().attr("idtop");
               var idRec=$(this).parent().attr("idrec");
               
               var nTR=$("td.rubrics p[idtop="+ idRec +"]").size();
               var flag="";
                                                        
               if ($(this).hasClass('ui-icon-plusthick')){//plus
                   flag = 'open';
                   $(this).removeClass('ui-icon-plusthick');
                   $(this).addClass('ui-icon-minusthick');
                   
                   for(var i=0;i<nTR; i++){
                       var $temp=$("td.rubrics p.[idtop="+ idRec +"]:eq("+ i +")");
                       
                       $temp.closest("tr").removeClass("close");
                   }
                   
               }else{                                      //minus 
                   flag = 'close';
                   $(this).removeClass('ui-icon-minusthick');
                   $(this).addClass('ui-icon-plusthick');
                   recursiveCloseTr(idTop, idRec);
               }
               
               removeAndAddCookieTable(flag, idRec); //удаление узла из куки
          
           });
           
           function recursiveCloseTr(id_top, id_rec){
               //набор строк текущего уровня
               var n=$("td.rubrics p[idtop="+ id_rec +"]").size();
               
                for(var i=0;i<n; i++){
                  var $target=$("td.rubrics p.[idtop="+ id_rec +"]:eq("+ i +")");
                  var idT=$target.attr("idtop");
                  var idR=$target.attr("idrec");
                  
                  if ($target.find("span").hasClass("ui-icon-minusthick")){
                    removeAndAddCookieTable('close', id_rec); //удаление узла из куки
                  }
                  
                   if ($target.has("span.treerubric")) {
                       $("td.rubrics p.[idtop="+ id_rec +"][idrec="+ idR +"] span").removeClass('ui-icon-minusthick');
                       $("td.rubrics p.[idtop="+ id_rec +"][idrec="+ idR +"] span").addClass('ui-icon-plusthick'); 
                       recursiveCloseTr(idT, idR);
                  }
                  
                  $target.closest("tr").addClass("close");
                }       
             
           }
           
           function removeAndAddCookieTable(flag, idRec){
               var actionControlPanelTree = "pharus_tree_"+actionControlPanel;
               /*обработка куки*/
               var treeCookie = ($.cookie(actionControlPanelTree) != null) ? $.cookie(actionControlPanelTree) : "";
               var massTemp = treeCookie.split(',');
               
               
               if ( flag == 'close') {
                    var indexDel = -1;
                    for(var i=0;i<massTemp.length; i++){
                       if ( parseInt(massTemp[i]) == parseInt(idRec) ) indexDel = i;
                   }
                   massTemp.splice(indexDel,1);
                   
               }else if ( flag == 'open') {
                   massTemp.push(idRec);
               }   
               
               $.cookie(actionControlPanelTree, massTemp.join(','), {path: "/"} );
               
               
           }
          //-------------------------------------------------------------------
         /* function getAssociativeArray( cookieValue ){
               var massTemp = cookieValue.split(';');
               var valuesMass = new Array;
                for(var i=0; i<massTemp.length; i++){
                     var k =  massTemp[i].indexOf ("="); 
                     var j = massTemp[i].substring(0, k);
                     var valTemp = massTemp[i].substring(k+1);
                     valuesMass[j] = valTemp;
                }
                return valuesMass;
          }
           
          var testObj = "width=123;height=455";
           $.cookie('testSel', testObj, {path: "/"} );
           var rk = $.cookie('testSel');
           valTest = getAssociativeArray(rk)
           console.log(valTest['height'] );
          
        */
           
          //-------------------------------------------------------------------
           $(".sitemap").click(function(){
                   var type=$(this).attr("type");
                   var action=$(this).attr("action");
                   var $obj = $(this);   
                   $obj.find("span.act").removeClass("close");   
                           
                           $.ajax({
                                     url: action,
                                     //async : false,
                                     type: "POST",
                                     //data:{type: type},
                                     timeout: 6000000,
                                     beforeSend: function(){
                             	
                                     },
                                     success: function(data){
                                           $obj.find("span.act").addClass("close");  
                                           jAlert("Файл sitemap.xml успешно обновлен", 'Обновление SiteMap.xml');
                                     }, 
                                     error: function(xhr, status){
                                            jAlert("Ошибка операции, файл sitemap.xml не обновлен", 'Обновление SiteMap.xml');
                                     }	
                                });
                      
                 
          });  
          //--------------------------------------------------------------------
         /* 
         .cke_show_blocks h6 {
    background-repeat: no-repeat;
    border: 1px dotted #808080;
    padding-top: 8px;
}          */
                             
        var iFrameDOM = $("iframe").contents();                    
        iFrameDOM.find('body').addClass("sdsd");
       // iFrameDOM.find("p").hide();
});

