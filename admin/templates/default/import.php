<?php include('include/header.php');?>

<div id="sub-nav">
    <div class="page-title">
	<h1><?=$this->breadcrumbs['name']?></h1>
	    <?=$this->breadcrumbs['puth']?>
    </div>
 </div>
<div id="page-layout">
    <div id="page-content">
	<div id="page-content-wrapper">
						
            <div class="inner-page-title">
		<h2><?=$this->namePage;?></h2><br/>
            </div>
                        
             <div class="content-box">
		<div class="two-column">
                    
                    <? include ("include/info.php");?>
                    
                    <p><?=$this->info?></p>
                    
                    <? if (isset($_GET['add'])) : ?>
                    <p>
                          <?if ($_GET['add']=="1") :
                           echo $this->lang['importActionOk'];
                          endif;?>
                        
                          <?if ($_GET['add']=="0") :
                           echo $this->lang['importActionNo'];
                          endif;?>
                    </p>
                    <? endif;?>
                    <div class="form import"> 
                        <form enctype='multipart/form-data' action='<?=$this->action?>' method='post'>
                        <input type='file' name='book'> 
                        <input type='submit' value="<?=$this->lang['importSend'];?>">
                        <span class="import close"><img src="/admin/templates/default/images/ajax-loader.gif"/></span>
                        </form>
                        
                    </div>
                    
                    <div id="modal" title="">
                    <p></p>
                    </div>
                    
                    <div id="form" title="" style="width:300px; height: 300px;">
                       
                    </div>
                        
                        
                </div>
	        <div class="clear"></div>
					
	    </div>
							
	     <div class="clear"></div>

				
				
	    <?php include('include/sidebar.php'); ?>
	</div>
	<div class="clear"></div>
   </div>
</div>
<?php include('include/footer.php'); ?></div>
</body>
</html>