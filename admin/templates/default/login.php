<?php include('include/header.php'); ?>
<link href="<?=CSSJS?>/default/css/ui/ui.login.css" rel="stylesheet" media="all" />

		<div id="sub-nav">
			<div class="page-title">
				<h1><?=$lng['auth']?></h1>
				<span><?=$lng['authDescription']?></span>
			</div>
		</div>
		<div class="clear"></div>
		<div id="page-layout">
			<div id="page-content">
				<div id="page-content-wrapper">

				<div id="tabs">
					
					<div id="login">
                                            <?php if ($_SESSION['alert_error']) {?>
                                                
						<div class="response-msg error">
							
                                                         <span><?=$this->lang['errorLoginForm']?></span>
							 <?=$_SESSION['alert_error'];?>
                                                        
						</div>
                                            <?}?>
						<form action="/admin/" method="POST">
							<ul>
								<li>
									<label for="login" class="desc">
				
										<?=$this->lang['login']; ?>:
									</label>
									<div>
										<input type="text" tabindex="1" maxlength="255" value="" class="field text full" name="login" id="email" />
									</div>
								</li>
								<li>
									<label for="password" class="desc">
										<?=$this->lang['password']; ?>:
									</label>
				
									<div>
										<input type="password" tabindex="1" maxlength="255" value="" class="field text full" name="password" id="password" />
									</div>
								</li>
								<li class="buttons">
									<div>
										<button class="ui-state-default ui-corner-all float-right ui-button" type="submit"><?=$this->lang['enter']?></button>
									</div>
								</li>
							</ul>
						</form>
					</div>
					
				      </div>



				</div>
				<div class="clear"></div>
			</div>
		</div>
	
        <?php include('include/footer.php'); ?></div>
</body>
</html>