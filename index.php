<?
session_start();
#ini_set('display_errors', 1);
ini_set('display_errors', 0);
error_reporting(E_ALL ^E_NOTICE);

require_once 'config.php';

function auload($class){
    $dirs = array(DROOT.'/controllers/', DROOT.'/classes/', DROOT.'/admin/classes/');
    foreach($dirs as $dir){
        foreach (new DirectoryIterator($dir) as $fileInfo){   
            if($fileInfo->isDot()) continue;            
            if(is_file($fileInfo->getPath().'/'.$class.'.php')) require_once $fileInfo->getPath().'/'.$class.'.php'; 
            if(is_dir($fileInfo->getPath().'/'.$fileInfo->getFilename())){    
                foreach (new DirectoryIterator($fileInfo->getPath().'/'.$fileInfo->getFilename()) as $fi){
                    if($fileInfo->isDot()) continue;
                    if(is_file($fi->getPath().'/'.$class.'.php')) require_once $fi->getPath().'/'.$class.'.php'; 
                }
            }            
        }
        
    }
}
spl_autoload_register('auload');

$front = FrontController::getInstance();
$front->route();
echo $front->getBody();
?>