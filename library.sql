﻿# SQL Manager 2010 for MySQL 4.5.0.9
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : library


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `library`;

CREATE DATABASE `library`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `library`;

#
# Structure for the `pharus_ban_email` table : 
#

CREATE TABLE `pharus_ban_email` (
  `id_ban_email` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_ban_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_users` table : 
#

CREATE TABLE `pharus_users` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) NOT NULL,
  `password` varchar(256) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `fam` varchar(40) DEFAULT NULL,
  `otch` varchar(40) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `is_blocked` tinyint(4) DEFAULT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `prioritet` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_calendar` table : 
#

CREATE TABLE `pharus_calendar` (
  `id_calendar` int(11) NOT NULL AUTO_INCREMENT,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `title` varchar(128) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `repeat_type` tinyint(4) DEFAULT NULL COMMENT '0-никогда\r\n1-день\r\n2-неделя\r\n3-месяц\r\n4-год',
  `repeat_value` int(10) DEFAULT NULL,
  `is_bc` tinyint(4) DEFAULT NULL COMMENT 'признак "До нашей эры"',
  `is_visible` tinyint(4) DEFAULT NULL,
  `is_main` tinyint(4) DEFAULT NULL,
  `id_users` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_calendar`),
  KEY `id_users` (`id_users`),
  CONSTRAINT `pharus_calendar_fk_userd` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_type_data` table : 
#

CREATE TABLE `pharus_sp_type_data` (
  `id_sp_type_data` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `code` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_sp_type_data`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_data` table : 
#

CREATE TABLE `pharus_data` (
  `id_data` int(11) NOT NULL AUTO_INCREMENT,
  `id_sp_type_data` int(11) NOT NULL COMMENT 'Ссылка на тип материала',
  `short_author` varchar(128) DEFAULT NULL COMMENT 'Короткое ФИО автора',
  `name` varchar(128) NOT NULL COMMENT 'Заглавие',
  `short_name` varchar(256) DEFAULT NULL COMMENT 'Короткое заглавие',
  `is_dot` tinyint(4) NOT NULL COMMENT 'Ставить ли точку после заглавия',
  `annotaciya` text COMMENT 'Аннотация',
  `bo` text COMMENT 'БО',
  `year` smallint(6) DEFAULT NULL COMMENT 'Год издания',
  `id_sp_polnota` int(11) DEFAULT NULL COMMENT 'Ссылка на полноту предоставления материала',
  `is_view_in_rubrik` tinyint(4) NOT NULL COMMENT 'Показывать ли в рубриках',
  `is_view_author` tinyint(4) NOT NULL COMMENT 'Показывать ли в указателе авторов',
  `is_view_zaglavie` tinyint(4) NOT NULL COMMENT 'Показывать ли в указателе заглавий',
  `is_search` tinyint(4) DEFAULT NULL COMMENT 'Искать ли по заглавию и содержанию',
  `is_add_year` tinyint(4) DEFAULT NULL COMMENT 'Добавлять ли в короткое имя год',
  `id_users` int(11) NOT NULL COMMENT 'Добавил пользователь',
  `url` varchar(256) DEFAULT NULL COMMENT 'Ссылка на материал',
  `file_name` varchar(256) DEFAULT NULL COMMENT 'Имя основного файла',
  `folder_name` varchar(256) DEFAULT NULL COMMENT 'Название папки',
  `start_page` varchar(16) DEFAULT NULL COMMENT 'Номер первой страницы',
  `vid_dis` varchar(64) DEFAULT NULL COMMENT 'Вид дисертации',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время добавления',
  `date_update` timestamp NULL DEFAULT NULL COMMENT 'Время последнего обновления',
  `is_periodical` tinyint(4) DEFAULT NULL COMMENT 'Является ли периодическим ',
  `is_recomended` tinyint(4) DEFAULT NULL COMMENT 'Признак рекомендованно к прочтению',
  `place_of_work` varchar(512) DEFAULT NULL COMMENT 'Место выполнения работы',
  `is_meta_book` tinyint(4) DEFAULT NULL COMMENT 'Признак метакниги',
  `is_in_stock` tinyint(4) DEFAULT NULL COMMENT 'Признак "В наличии"',
  `article_source` tinyint(4) DEFAULT '0' COMMENT 'Ссылка на источник статьи',
  `article_source_text` longtext COMMENT 'Ссылка на источник статьи текстом',
  `article_group_bo` text COMMENT 'Библиографическое описание групп статей в продолжениях. Заполняется у статьи начала',
  `title` varchar(256) DEFAULT NULL COMMENT 'Служебный',
  `description` varchar(256) DEFAULT NULL COMMENT 'Служебный',
  `keywords` varchar(256) DEFAULT NULL COMMENT 'Служебный',
  `materials` text COMMENT 'Материалы, связанные с издание для книги и \r\nтекстовое описание для метакниги',
  `id_next` int(11) DEFAULT '0' COMMENT 'id продолжения статьи',
  `id_prev` int(11) DEFAULT '0' COMMENT 'id предыдущей части статьи',
  PRIMARY KEY (`id_data`),
  KEY `users_data` (`id_users`),
  KEY `sp_type_data_data` (`id_sp_type_data`),
  KEY `id_next` (`id_next`),
  KEY `id_prev` (`id_prev`),
  CONSTRAINT `sp_type_data_data` FOREIGN KEY (`id_sp_type_data`) REFERENCES `pharus_sp_type_data` (`id_sp_type_data`),
  CONSTRAINT `users_data` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_feedback` table : 
#

CREATE TABLE `pharus_feedback` (
  `id_feedback` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `message` text,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Статус сообщения 1-новое, 2-обработанное',
  `id_users` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_feedback`),
  KEY `users_feedback` (`id_users`),
  CONSTRAINT `users_feedback` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_main_setting` table : 
#

CREATE TABLE `pharus_main_setting` (
  `id_main_setting` int(11) NOT NULL AUTO_INCREMENT,
  `id_sp_type_data` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `value` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_main_setting`),
  KEY `sp_type_data_main_setting` (`id_sp_type_data`),
  CONSTRAINT `sp_type_data_main_setting` FOREIGN KEY (`id_sp_type_data`) REFERENCES `pharus_sp_type_data` (`id_sp_type_data`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_mapping` table : 
#

CREATE TABLE `pharus_mapping` (
  `id_mapping` int(11) NOT NULL AUTO_INCREMENT,
  `id_data` int(11) NOT NULL,
  `num_page` int(11) DEFAULT NULL COMMENT 'номер страницы в просмотрщике',
  `real_page` varchar(128) DEFAULT NULL COMMENT 'Реальная страница в издании',
  `name` varchar(256) DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '1-полное описание\r\n0-краткое описание',
  `file` varchar(256) DEFAULT NULL COMMENT 'имя файла',
  PRIMARY KEY (`id_mapping`),
  KEY `data_mapping` (`id_data`),
  CONSTRAINT `data_mapping` FOREIGN KEY (`id_data`) REFERENCES `pharus_data` (`id_data`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=497 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_menu` table : 
#

CREATE TABLE `pharus_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu_top` int(11) DEFAULT '0',
  `name` varchar(128) DEFAULT NULL,
  `is_visible` tinyint(4) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `text` longtext,
  `is_rubricator` tinyint(4) DEFAULT NULL,
  `url_name` varchar(64) DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_news` table : 
#

CREATE TABLE `pharus_news` (
  `id_news` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `zagolovok` varchar(512) DEFAULT NULL,
  `annotaciya` varchar(256) DEFAULT NULL,
  `text` text,
  `comment` text,
  `is_main` tinyint(4) DEFAULT NULL COMMENT 'Признак отображения на главной странице',
  `is_visible` int(4) DEFAULT NULL,
  `is_rss` int(4) DEFAULT NULL COMMENT 'Доступность новости в RSS-потоке',
  `url` varchar(256) DEFAULT NULL COMMENT 'url новости',
  `main_theme` int(11) DEFAULT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_news`),
  KEY `users_news` (`id_users`),
  CONSTRAINT `users_news` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_person` table : 
#

CREATE TABLE `pharus_person` (
  `id_person` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `fam_dp` varchar(32) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `surname` varchar(64) DEFAULT NULL,
  `patronymic` varchar(64) DEFAULT NULL,
  `fio` varchar(192) NOT NULL,
  `other_name` varchar(128) DEFAULT NULL,
  `years` varchar(32) DEFAULT NULL,
  `day` smallint(6) NOT NULL,
  `month` smallint(6) NOT NULL,
  `year` smallint(6) NOT NULL,
  `is_bc` tinyint(4) DEFAULT '0' COMMENT 'Дата рождения до Н.Э?',
  `is_view_pam` tinyint(4) NOT NULL DEFAULT '0',
  `is_view_izb` tinyint(4) NOT NULL DEFAULT '0',
  `is_view_in_main` tinyint(4) DEFAULT NULL COMMENT 'Показывать на главной',
  `is_view_news` tinyint(4) DEFAULT NULL,
  `definition` text,
  `short_definition` text,
  `text` text,
  `short_text` text,
  `fio_full_enc` varchar(256) DEFAULT NULL COMMENT 'Полное ФИО, для энциклопедии',
  `date_full_enc` text COMMENT 'Даты и места рождения и смерти,для энциклопедии ',
  `url` varchar(128) DEFAULT NULL,
  `is_in_encyclopedia` tinyint(4) DEFAULT NULL,
  `is_portret` tinyint(4) DEFAULT NULL COMMENT 'Отображать портрет?',
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `photo` varchar(128) DEFAULT NULL,
  `add_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_data` datetime DEFAULT NULL,
  `id_person_forwars` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_person`),
  KEY `users_person` (`id_users`),
  CONSTRAINT `users_person` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_status` table : 
#

CREATE TABLE `pharus_sp_status` (
  `id_sp_status` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `value` varchar(64) NOT NULL,
  PRIMARY KEY (`id_sp_status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rassilka` table : 
#

CREATE TABLE `pharus_rassilka` (
  `id_rassilka` int(11) NOT NULL AUTO_INCREMENT,
  `tema` varchar(256) DEFAULT NULL,
  `body` text,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_send` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `file` varchar(256) DEFAULT NULL,
  `id_sp_status` int(11) NOT NULL,
  PRIMARY KEY (`id_rassilka`),
  KEY `id_sp_status` (`id_sp_status`),
  CONSTRAINT `sp_status_rassilka` FOREIGN KEY (`id_sp_status`) REFERENCES `pharus_sp_status` (`id_sp_status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rassilka_setting` table : 
#

CREATE TABLE `pharus_rassilka_setting` (
  `id_rassilka_setting` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT NULL COMMENT 'Тип рассылки 1 - без ограничений, 2 - с ограничениями (пакетами)',
  `time_paket` int(11) DEFAULT NULL,
  `kolvo_pisem` int(11) DEFAULT NULL,
  `kolvo_addr` int(11) DEFAULT NULL,
  `name_author` varchar(256) DEFAULT NULL COMMENT 'Имя автора сообщений',
  `adress_author` varchar(256) DEFAULT NULL COMMENT 'Email адрес отправителя',
  `adress_server` varchar(256) DEFAULT NULL COMMENT 'Email адрес сервера отправки',
  `is_HTML` tinyint(4) DEFAULT NULL COMMENT 'Отправка писмьа в формате HTML или нет',
  `end_letter` text COMMENT 'Окончание писем рассылки',
  `about` text COMMENT 'О рассылкке',
  `info_after_sender` text COMMENT 'Иноформацию подписчику о результате подписки',
  `appeal_to_men` varchar(256) DEFAULT NULL COMMENT 'Обращение к мужчине в сообщении',
  `appeal_to_women` varchar(256) DEFAULT NULL COMMENT 'Обращение к женщине в сообщении',
  PRIMARY KEY (`id_rassilka_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_data_data` table : 
#

CREATE TABLE `pharus_rel_data_data` (
  `id_rel_data_data` int(11) NOT NULL AUTO_INCREMENT,
  `id_data` int(11) NOT NULL,
  `id_data_used` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_data_data`),
  KEY `data_rel_data_data1` (`id_data_used`),
  KEY `data_rel_data_data` (`id_data`),
  CONSTRAINT `data_rel_data_data` FOREIGN KEY (`id_data`) REFERENCES `pharus_data` (`id_data`),
  CONSTRAINT `data_rel_data_data1` FOREIGN KEY (`id_data_used`) REFERENCES `pharus_data` (`id_data`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_data_menu` table : 
#

CREATE TABLE `pharus_rel_data_menu` (
  `id_rel_data_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_data` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_data_menu`),
  KEY `menu_rel_data_menu` (`id_menu`),
  KEY `data_rel_data_menu` (`id_data`),
  CONSTRAINT `data_rel_data_menu` FOREIGN KEY (`id_data`) REFERENCES `pharus_data` (`id_data`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_rel_data_menu` FOREIGN KEY (`id_menu`) REFERENCES `pharus_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_type_person` table : 
#

CREATE TABLE `pharus_sp_type_person` (
  `id_sp_type_person` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_sp_type_person`,`name`),
  KEY `id_sp_type_person` (`id_sp_type_person`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_data_person` table : 
#

CREATE TABLE `pharus_rel_data_person` (
  `id_rel_data_person` int(11) NOT NULL AUTO_INCREMENT,
  `id_data` int(11) NOT NULL,
  `id_person` int(11) NOT NULL,
  `id_sp_type_person` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_data_person`),
  KEY `data_rel_data_person` (`id_data`),
  KEY `person_rel_data_person` (`id_person`),
  KEY `id_sp_type_person` (`id_sp_type_person`),
  CONSTRAINT `data_rel_data_person` FOREIGN KEY (`id_data`) REFERENCES `pharus_data` (`id_data`),
  CONSTRAINT `person_rel_data_person` FOREIGN KEY (`id_person`) REFERENCES `pharus_person` (`id_person`),
  CONSTRAINT `pharus_rel_data_person_fk` FOREIGN KEY (`id_sp_type_person`) REFERENCES `pharus_sp_type_person` (`id_sp_type_person`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_menu_rub` table : 
#

CREATE TABLE `pharus_rel_menu_rub` (
  `id_rel_menu_rub` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) DEFAULT NULL,
  `id_rub` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_menu_rub`),
  UNIQUE KEY `id_menu_rub_index` (`id_menu`,`id_rub`),
  KEY `id_menu` (`id_menu`),
  KEY `id_rub` (`id_rub`),
  CONSTRAINT `pharus_rel_menu_rub_fk` FOREIGN KEY (`id_menu`) REFERENCES `pharus_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pharus_rel_menu_rub_fk1` FOREIGN KEY (`id_rub`) REFERENCES `pharus_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_news_person` table : 
#

CREATE TABLE `pharus_rel_news_person` (
  `id_rel_news_person` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) DEFAULT NULL,
  `id_person` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_news_person`),
  KEY `id_news` (`id_news`),
  KEY `id_person` (`id_person`),
  CONSTRAINT `rel_news_person_fk` FOREIGN KEY (`id_news`) REFERENCES `pharus_news` (`id_news`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rel_news_person_fk1` FOREIGN KEY (`id_person`) REFERENCES `pharus_person` (`id_person`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_education` table : 
#

CREATE TABLE `pharus_sp_education` (
  `id_sp_education` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `is_visible` tinyint(4) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_sp_education`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Образование';

#
# Structure for the `pharus_rel_news_sp_education` table : 
#

CREATE TABLE `pharus_rel_news_sp_education` (
  `id_rel_news_sp_education` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `id_sp_education` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_news_sp_education`),
  KEY `news_rel_news_sp_education` (`id_news`),
  KEY `sp_education_rel_news_sp_education` (`id_sp_education`),
  CONSTRAINT `news_rel_news_sp_education` FOREIGN KEY (`id_news`) REFERENCES `pharus_news` (`id_news`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_education_rel_news_sp_education` FOREIGN KEY (`id_sp_education`) REFERENCES `pharus_sp_education` (`id_sp_education`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_organasation` table : 
#

CREATE TABLE `pharus_sp_organasation` (
  `id_sp_organasation` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `is_visible` tinyint(4) DEFAULT NULL,
  `id_users` int(11) DEFAULT NULL,
  `day` smallint(6) DEFAULT NULL,
  `month` smallint(6) DEFAULT NULL,
  `year` smallint(6) DEFAULT NULL,
  `is_view_in_main` tinyint(4) DEFAULT NULL,
  `is_view_news` tinyint(4) DEFAULT NULL,
  `short_text` text,
  `definition` text,
  `text` text,
  `url` varchar(256) DEFAULT NULL,
  `is_in_encyclopedia` tinyint(4) DEFAULT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `photo` varchar(256) DEFAULT NULL,
  `add_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_data` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `position` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_sp_organasation`),
  KEY `id_users` (`id_users`),
  CONSTRAINT `users_sp_organasation_fk` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Организации';

#
# Structure for the `pharus_rel_news_sp_organasation` table : 
#

CREATE TABLE `pharus_rel_news_sp_organasation` (
  `id_rel_news_sp_organasation` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `id_sp_organasation` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_news_sp_organasation`),
  KEY `news_rel_news_sp_organasation` (`id_news`),
  KEY `sp_organasation_rel_news_sp_organasation` (`id_sp_organasation`),
  CONSTRAINT `news_rel_news_sp_organasation` FOREIGN KEY (`id_news`) REFERENCES `pharus_news` (`id_news`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_organasation_rel_news_sp_organasation` FOREIGN KEY (`id_sp_organasation`) REFERENCES `pharus_sp_organasation` (`id_sp_organasation`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_theme_news` table : 
#

CREATE TABLE `pharus_sp_theme_news` (
  `id_sp_theme_news` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `is_visible` tinyint(4) DEFAULT NULL,
  `is_main` tinyint(4) NOT NULL DEFAULT '1',
  `url` varchar(128) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_sp_theme_news`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Темы новостей';

#
# Structure for the `pharus_rel_news_sp_theme_news` table : 
#

CREATE TABLE `pharus_rel_news_sp_theme_news` (
  `id_rel_news_sp_theme_news` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `id_sp_theme_news` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_news_sp_theme_news`),
  KEY `news_rel_news_sp_theme_news` (`id_news`),
  KEY `sp_theme_news_rel_news_sp_theme_news` (`id_sp_theme_news`),
  CONSTRAINT `news_rel_news_sp_theme_news` FOREIGN KEY (`id_news`) REFERENCES `pharus_news` (`id_news`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_theme_news_rel_news_sp_theme_news` FOREIGN KEY (`id_sp_theme_news`) REFERENCES `pharus_sp_theme_news` (`id_sp_theme_news`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_vid_news` table : 
#

CREATE TABLE `pharus_sp_vid_news` (
  `id_sp_vid_news` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `is_visible` tinyint(4) DEFAULT '1',
  `url` varchar(256) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_sp_vid_news`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Виды новостей';

#
# Structure for the `pharus_rel_news_sp_vid_news` table : 
#

CREATE TABLE `pharus_rel_news_sp_vid_news` (
  `id_rel_news_sp_vid_news` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `id_sp_vid_news` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_news_sp_vid_news`),
  KEY `news_rel_news_sp_vid_news` (`id_news`),
  KEY `sp_vid_news_rel_news_sp_vid_news` (`id_sp_vid_news`),
  CONSTRAINT `pharus_rel_news_sp_vid_news_fk` FOREIGN KEY (`id_news`) REFERENCES `pharus_news` (`id_news`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_vid_news_rel_news_sp_vid_news` FOREIGN KEY (`id_sp_vid_news`) REFERENCES `pharus_sp_vid_news` (`id_sp_vid_news`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_person_menu` table : 
#

CREATE TABLE `pharus_rel_person_menu` (
  `id_rel_person_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_person` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_person_menu`),
  KEY `id_person` (`id_person`),
  KEY `id_menu` (`id_menu`),
  CONSTRAINT `pharus_rel_person_menu_fk` FOREIGN KEY (`id_person`) REFERENCES `pharus_person` (`id_person`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pharus_rel_person_menu_fk1` FOREIGN KEY (`id_menu`) REFERENCES `pharus_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_user_email` table : 
#

CREATE TABLE `pharus_user_email` (
  `id_user_email` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `email` varchar(256) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `is_black_list` tinyint(4) DEFAULT NULL COMMENT 'Признак занесения в черный список (0-нет, 1-да)',
  `sex` tinyint(4) DEFAULT '1' COMMENT '1-мужчина,0-женщина',
  PRIMARY KEY (`id_user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_rassilka_user_email` table : 
#

CREATE TABLE `pharus_rel_rassilka_user_email` (
  `id_rel_rassilka_user_email` int(11) NOT NULL AUTO_INCREMENT,
  `id_rassilka` int(11) NOT NULL,
  `id_user_email` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_rassilka_user_email`),
  KEY `rassilka_rel_rassilka_user_email` (`id_rassilka`),
  KEY `user_email_rel_rassilka_user_email` (`id_user_email`),
  CONSTRAINT `rassilka_rel_rassilka_user_email` FOREIGN KEY (`id_rassilka`) REFERENCES `pharus_rassilka` (`id_rassilka`),
  CONSTRAINT `user_email_rel_rassilka_user_email` FOREIGN KEY (`id_user_email`) REFERENCES `pharus_user_email` (`id_user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_sp_organasation_menu` table : 
#

CREATE TABLE `pharus_rel_sp_organasation_menu` (
  `id_rel_sp_organasation_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_sp_organasation` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_sp_organasation_menu`),
  KEY `menu_rel_sp_organasation_menu` (`id_menu`),
  KEY `sp_organasation_rel_sp_organasation_menu` (`id_sp_organasation`),
  CONSTRAINT `menu_rel_sp_organasation_menu` FOREIGN KEY (`id_menu`) REFERENCES `pharus_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sp_organasation_rel_sp_organasation_menu` FOREIGN KEY (`id_sp_organasation`) REFERENCES `pharus_sp_organasation` (`id_sp_organasation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_users_pred_print` table : 
#

CREATE TABLE `pharus_rel_users_pred_print` (
  `id_rel_users_pred_print` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `id_data` int(11) DEFAULT NULL,
  `id_news` int(11) DEFAULT NULL,
  `prioritet` smallint(6) DEFAULT NULL,
  `is_done` tinyint(4) DEFAULT '0',
  `comment` varchar(256) DEFAULT NULL,
  `is_comment_done` tinyint(4) DEFAULT '0',
  `date_update` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_rel_users_pred_print`),
  KEY `users_rel_users_pred_print` (`id_users`),
  CONSTRAINT `users_rel_users_pred_print` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_roles` table : 
#

CREATE TABLE `pharus_roles` (
  `id_roles` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id_roles`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rel_users_roles` table : 
#

CREATE TABLE `pharus_rel_users_roles` (
  `id_rel_users_roles` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `id_roles` int(11) NOT NULL,
  PRIMARY KEY (`id_rel_users_roles`),
  KEY `users_rel_users_roles` (`id_users`),
  KEY `roles_rel_users_roles` (`id_roles`),
  CONSTRAINT `roles_rel_users_roles` FOREIGN KEY (`id_roles`) REFERENCES `pharus_roles` (`id_roles`),
  CONSTRAINT `users_rel_users_roles` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_rss` table : 
#

CREATE TABLE `pharus_rss` (
  `id_rss` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `link` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `language` varchar(40) NOT NULL,
  `copyright` varchar(128) DEFAULT NULL,
  `managingEditor` varchar(128) DEFAULT NULL,
  `webMaster` varchar(128) DEFAULT NULL,
  `pubDate` datetime NOT NULL,
  `lastBuildDate` datetime DEFAULT NULL,
  `category` varchar(256) DEFAULT NULL,
  `author` varchar(256) DEFAULT NULL,
  `number` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_rss`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_citata` table : 
#

CREATE TABLE `pharus_sp_citata` (
  `id_sp_citata` int(11) NOT NULL AUTO_INCREMENT,
  `id_person` int(11) NOT NULL,
  `value` text,
  PRIMARY KEY (`id_sp_citata`),
  KEY `person_sp_citata` (`id_person`),
  CONSTRAINT `person_sp_citata` FOREIGN KEY (`id_person`) REFERENCES `pharus_person` (`id_person`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_polnota` table : 
#

CREATE TABLE `pharus_sp_polnota` (
  `id_sp_polnota` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_sp_polnota`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_sp_table_names` table : 
#

CREATE TABLE `pharus_sp_table_names` (
  `id_sp_table_names` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(64) DEFAULT NULL,
  `column` varchar(64) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id_sp_table_names`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_static_page` table : 
#

CREATE TABLE `pharus_static_page` (
  `id_static_page` int(11) NOT NULL AUTO_INCREMENT,
  `id_static_page_top` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT '0',
  `name` varchar(256) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `annotaciya` varchar(256) DEFAULT NULL,
  `text` longtext NOT NULL,
  `keywords` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `id_users` int(11) DEFAULT NULL,
  `is_visible` tinyint(4) DEFAULT NULL,
  `is_menu` tinyint(4) DEFAULT NULL COMMENT 'Выводить в меню?',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `is_folders` tinyint(4) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `link_out` varchar(256) DEFAULT NULL,
  `id_page_forwars` int(11) DEFAULT '0',
  PRIMARY KEY (`id_static_page`),
  KEY `id_menu` (`id_menu`),
  KEY `id_users` (`id_users`),
  CONSTRAINT `users_page` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

#
# Structure for the `pharus_system_gurnal` table : 
#

CREATE TABLE `pharus_system_gurnal` (
  `id_system_gurnal` int(11) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) NOT NULL,
  `login` varchar(256) NOT NULL,
  `action` varchar(256) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` varchar(40) DEFAULT NULL,
  `db_old` text,
  `db_new` text,
  PRIMARY KEY (`id_system_gurnal`),
  KEY `users_system_gurnal` (`id_users`),
  CONSTRAINT `users_system_gurnal` FOREIGN KEY (`id_users`) REFERENCES `pharus_users` (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=7527 DEFAULT CHARSET=utf8;

#
# Data for the `pharus_ban_email` table  (LIMIT 0,500)
#

INSERT INTO `pharus_ban_email` (`id_ban_email`, `email`) VALUES 
  (1,'qwerty@mail.ry');
COMMIT;

#
# Data for the `pharus_users` table  (LIMIT 0,500)
#

INSERT INTO `pharus_users` (`id_users`, `login`, `password`, `name`, `fam`, `otch`, `email`, `is_active`, `is_blocked`, `phone`, `address`, `prioritet`) VALUES 
  (1,'admin','21232f297a57a5a743894a0e4a801fc3','Администратор','','','',1,0,'',NULL,NULL),
  (2,'redactor','e10adc3949ba59abbe56e057f20f883e','Редактор','Петров','Петрович','',1,0,'',NULL,1),
  (3,'redactor2','e10adc3949ba59abbe56e057f20f883e','','','','',1,0,'',NULL,2),
  (4,'redactor3','e10adc3949ba59abbe56e057f20f883e','','','','',1,0,'',NULL,2),
  (7,'tester124','e10adc3949ba59abbe56e057f20f883e','','фамилия','1212','',1,0,'',NULL,0);
COMMIT;

#
# Data for the `pharus_calendar` table  (LIMIT 0,500)
#

INSERT INTO `pharus_calendar` (`id_calendar`, `date_from`, `date_to`, `title`, `text`, `url`, `repeat_type`, `repeat_value`, `is_bc`, `is_visible`, `is_main`, `id_users`) VALUES 
  (3,'2012-07-23','2012-07-24','Событие в календаре','Описание события для слайдера',NULL,4,1,0,1,1,1),
  (4,'2013-07-25','2013-08-25','Тестовое месяц','Текстовое сообщение месяц','/link/1',0,0,0,1,1,1),
  (8,'2013-07-17','2013-07-19','Месяц, повторение 2','Месяц!','',3,2,0,1,1,1),
  (10,'2013-07-18','2013-07-28','Неделя','Описание для недели','',4,1,0,1,1,1),
  (11,'2013-07-29','2013-07-31','событие июля','','/encyclopedia/index/organization/',4,5,0,1,1,1),
  (12,'2013-01-01','2013-01-02','Годовое событие - повторять неделю через месяц','Поторяем, описываем','',2,4,0,1,0,1);
COMMIT;

#
# Data for the `pharus_sp_type_data` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_type_data` (`id_sp_type_data`, `name`, `url`, `code`) VALUES 
  (1,'Книга','lib/book','book'),
  (2,'Статья','lib/article','article'),
  (3,'Диссертация','lib/thesis_material','thesis'),
  (4,'Диафильм','lib/filmstrip','filmstrip'),
  (5,'Новости','news','news'),
  (6,'Метакнига',NULL,'metabook');
COMMIT;

#
# Data for the `pharus_data` table  (LIMIT 0,500)
#

INSERT INTO `pharus_data` (`id_data`, `id_sp_type_data`, `short_author`, `name`, `short_name`, `is_dot`, `annotaciya`, `bo`, `year`, `id_sp_polnota`, `is_view_in_rubrik`, `is_view_author`, `is_view_zaglavie`, `is_search`, `is_add_year`, `id_users`, `url`, `file_name`, `folder_name`, `start_page`, `vid_dis`, `date_add`, `date_update`, `is_periodical`, `is_recomended`, `place_of_work`, `is_meta_book`, `is_in_stock`, `article_source`, `article_source_text`, `article_group_bo`, `title`, `description`, `keywords`, `materials`, `id_next`, `id_prev`) VALUES 
  (4,1,'Герлинг А.Б.','Основное начертание  плоской и сферической тригонометрии','Основное начертание  плоской и сферической тригонометрии',1,'Книга посвящена занимательной математике. В красивой и доступной форме автор рассказывает об открытиях ученых, занимательных парадоксах и софизмах, применениях математики.','описание (БО)',1828,1,1,1,1,1,1,1,'gerling_osnovnoe_nachertanie_ploskoj_i_sfericheskoj_trigonometrii_1828.html','gerling_osnovnoe_nachertanie_ploskoj_i_sfericheskoj_trigonometrii_1828.djvu','gerling_osnovnoe_nachertanie_ploskoj_i_sfericheskoj_trigonometrii_1828','1',NULL,'2013-04-25 21:19:29','2013-08-17 19:21:26',1,1,NULL,NULL,1,NULL,NULL,NULL,'Короткий автор : Полное название, 1828','','','<p>hjgj&nbsp;ывыывыв</p>\r\n\r\n<p>fdsfdf</p>\r\n\r\n<p>sdfsdf</p>\r\n',0,0),
  (7,1,'Бобынин В. В.','Полное заглавие','Короткое заглавие',0,'Аннтотация','БО',1966,1,1,1,1,1,0,1,'polnoe-zaglavie.html','aleksandrova_r_a_1966.djvu','aleksandrova_r_a_1966','1',NULL,'2013-06-24 21:10:11','2013-08-17 19:19:55',0,0,NULL,NULL,0,NULL,NULL,NULL,'Бобынин В. В. : Полное заглавие, 1966','','','',0,0),
  (8,2,'Бобынин В. В.','Руководство прямолинейной тригонометрии','Тригонометрия',0,'Аннтотация','БО',1966,3,1,1,1,1,0,1,'rukovodstvo-pryamolineynoy-trigonometrii.html','malinin_rukovodstvo_pryamolinejnoj_trigonometrii_1870_(fragment).djvu','malinin_rukovodstvo_pryamolinejnoj_trigonometrii_1870_(fragment)','45',NULL,'2013-04-24 18:05:42','2013-08-01 23:16:35',NULL,NULL,NULL,NULL,NULL,7,'Полное заглавие',NULL,'Бобынин В. В. : Руководство прямолинейной тригонометрии, 1966','','','',0,0),
  (9,6,'уцукуцк','Метабук','цукуц',1,NULL,NULL,NULL,1,1,0,1,1,1,1,'metabuk.html',NULL,NULL,'1',NULL,'2013-04-24 10:55:20','2013-07-10 19:15:35',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,'Метабук','','','<p>ываваываываываываыва</p>\r\n\r\n<p>werewr</p>\r\n\r\n<p>werwer</p>\r\n',0,0),
  (21,3,'Васильев И.П.','Подготовка вычислительных выражений','Подготовка вычислительных выражений',0,NULL,'библиографическое опсание выаываываываываыва',2010,3,1,1,1,1,1,1,'vasil-ev-i-p-_podgotovka-vychislitel-nyh-vyrazheniy_2010.html',NULL,'45345_kandidatskaya','1','1','2013-04-30 16:21:16','2013-08-07 17:58:41',NULL,NULL,'',NULL,NULL,0,NULL,NULL,'45345 : Кандидатская,','','',NULL,0,0),
  (22,3,'Сергеев Н.П.','Авторефрерат Методика обучения математике студентов гуманитарных специальностей вузов в контексте интенсификации обучения','Авторефрерат  Методика обучения математике студентов гуманитарных специальностей вузов в контексте интенсификации обучения',0,NULL,'Библиографическое описание',1999,1,1,1,1,1,1,1,'avtor-dissertacii_avtorefrerat_1999.html',NULL,'prprpr_papraprapr','1','3','2013-04-30 16:37:46','2013-08-07 16:55:21',NULL,NULL,'',NULL,NULL,21,NULL,NULL,'прпрпр : папрапрапр,','','',NULL,0,0),
  (25,4,'Короткое имя автора','Заглавие','Короткое заглавие',1,'Аннотация к диафильму','описание',1980,1,1,1,1,1,1,1,'korotkoe-imya-avtora_zaglavie_god-izdaniya.html',NULL,'korotkoe_imya_avtora_zaglavie_god_izdaniya','1',NULL,'2013-05-20 16:25:47','2013-08-06 19:27:09',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Короткое имя автора : Заглавие, Год издания','','',NULL,0,0),
  (26,2,'Малинин В.М.','Руководство прямолинейной тригонометрии','Руководство тригонометрия',1,'','',1934,3,1,1,1,1,1,1,'malinin_rukovodstvo-pryamolineynoy-trigonometrii_1934.html',NULL,'malinin_rukovodstvo_pryamolineynoy_trigonometrii_1934','1',NULL,'2013-05-26 14:02:59','2013-08-02 04:05:13',NULL,NULL,NULL,NULL,NULL,0,'Книга тригонометрия',NULL,'Малинин : Руководство прямолинейной тригонометрии, 1934','','','',0,0),
  (47,1,'Бобынин В. В.1','Полное заглавие1','Короткое заглавие1',1,'Аннтотация1','Выготский Л. С. Собрание сочинений: в 6 т. / Гл. ред. А. В. Запорожец. - М.: Педагогика, 1982-1984. Т. 1 : Вопросы теории и истории психологии / Под ред. А. Р. Лурия, М. Г. Ярошевского. - 1982. - 488 с.',1966,1,1,1,1,1,0,1,'polnoe-zaglavie1.html','a_1966.djvu','a_1966','1','1','2013-08-01 23:07:51','2013-08-06 02:07:19',1,1,'place_of_work1',NULL,1,0,'article_source_text1',NULL,'Бобынин В. В.1 : Полное заглавие1, 1966','','','<p>materials1</p>\r\n',0,0),
  (48,1,'Бобынин В. В.','Полное заглавие','Короткое заглавие',0,'Аннтотация','Векторы в пространстве : диафильм по математике для 9 класса / [автор А. Литвинова, консультант Г. Левитас, ред. Н. Морозова]. — [М. : студия «Диафильм», 1977]. — 41 кадр.',1966,2,1,1,1,1,0,1,'polnoe-zaglavie-2.html','r_a_1966.djvu','r_a_1966','1','1','2013-08-01 23:07:52','2013-08-17 19:18:16',1,1,'place_of_work',NULL,1,0,'article_source_text',NULL,'Бобынин В. В. : Полное заглавие, 1966','','','<p>materials</p>\r\n',0,0),
  (49,2,'Автор статьи','Начало статьи','Начало статьи',0,'','',2011,3,1,1,0,1,1,1,'avtor-stat-i_nachalo-stat-i_.html',NULL,NULL,'23',NULL,'2013-08-02 00:37:25','2013-08-07 00:27:08',NULL,NULL,NULL,NULL,NULL,4,'Основное начертание  плоской и сферической тригонометрии','бо статьи в продолжении','Автор статьи : Начало статьи,','','','',50,0),
  (50,2,'Автор статьи','Продолжение статьи','Продолжение статьи',0,'','',2012,1,1,1,0,1,1,1,'avtor-stat-i_prodolzhenie-stat-i_2012.html',NULL,NULL,'10',NULL,'2013-08-02 00:38:31','2013-08-02 00:39:38',NULL,NULL,NULL,NULL,NULL,4,'Основное начертание  плоской и сферической тригонометрии',NULL,'Автор статьи : Продолжение статьи, 2012','','','',51,49),
  (51,2,'Автор статьи','Окончание статьи','Окончание статьи',0,'','',2013,0,1,1,0,1,1,1,'avtor-stat-i_okonchanie-stat-i_.html',NULL,NULL,'1',NULL,'2013-08-02 00:39:21','2013-08-02 01:04:20',NULL,NULL,NULL,NULL,NULL,4,'Основное начертание  плоской и сферической тригонометрии',NULL,'Автор статьи : Окончание статьи,','','','',0,50),
  (52,2,'Ивановский Р.И.','Вычитание больших чисел','Вычитание больших чисел',1,'','',1923,1,1,1,0,1,1,1,'ivanovskiy-r-i-_vychitanie-bol-shih-chisel_1923.html',NULL,'ivanovskiy_r_i__vychitanie_bol_shih_chisel_1923_999','1',NULL,'2013-08-07 01:41:25',NULL,NULL,NULL,NULL,NULL,NULL,0,'Источник самостоятельной статьи','','Ивановский Р.И. : Вычитание больших чисел, 1923','','','',0,0),
  (53,3,'Петров И.В.','Методика подготовки будущих учителей математики к использованию моделирования в обучении школьников','Методика подготовки будущих учителей математики к использованию моделирования в обучении школьников',0,NULL,'описание',2010,1,1,1,0,1,1,1,'sadykova-a-a-_metodika-podgotovki-buduschih-uchiteley-matematiki-k-ispol-zovaniyu-modelirovaniya-v-obuchenii-shkol-nikov_2010.html',NULL,'sadykova_a_a__metodika_podgotovki_buduschih_uchiteley_matematiki_k_ispol_zovaniyu_modelirovaniya_v_obuchenii_shkol_nikov_2010','1','2','2013-08-07 12:34:03','2013-08-07 17:12:42',NULL,NULL,'',NULL,NULL,0,NULL,NULL,'Садыкова А. А. : Методика подготовки будущих учителей математики к использованию моделирования в обучении школьников, 2010','','',NULL,0,0),
  (54,3,'Петров И.В.','Автореферат Методика подготовки будущих учителей','Автореферат Методика подготовки будущих учителей',0,NULL,'',2011,2,1,1,0,1,1,1,'petrov-v-i-_avtoreferat-metodika-podgotovki-buduschih-uchiteley_2011.html',NULL,'petrov_v_i__avtoreferat_metodika_podgotovki_buduschih_uchiteley_2011','1','3','2013-08-07 16:05:18','2013-08-07 17:13:02',NULL,NULL,'',NULL,NULL,53,NULL,NULL,'Петров В.И. : Автореферат Методика подготовки будущих учителей, 2011','','',NULL,0,0),
  (55,3,'Иванесес Н.А.','Нет автореферата и что с этим делать','Нет автореферата и что с этим делать',0,NULL,'',1934,2,1,1,0,1,1,1,'ivaneses-n-a-_net-avtoreferata-i-chto-s-etim-delat-_1934.html',NULL,'ivaneses_n_a__net_avtoreferata_i_chto_s_etim_delat__1934','1','2','2013-08-07 17:22:19',NULL,NULL,NULL,'',NULL,NULL,0,NULL,NULL,'Иванесес Н.А. : Нет автореферата и что с этим делать, 1934','','',NULL,0,0);
COMMIT;

#
# Data for the `pharus_feedback` table  (LIMIT 0,500)
#

INSERT INTO `pharus_feedback` (`id_feedback`, `name`, `email`, `message`, `date_add`, `status`, `id_users`) VALUES 
  (5,'имя','qwerty@dddd.rt',NULL,'2013-07-27 00:52:38',1,NULL),
  (6,'имя отправителя','qwerty@mail.ry',NULL,'2013-08-10 00:43:43',2,1);
COMMIT;

#
# Data for the `pharus_main_setting` table  (LIMIT 0,500)
#

INSERT INTO `pharus_main_setting` (`id_main_setting`, `id_sp_type_data`, `name`, `value`) VALUES 
  (2,5,'countOnPage','7'),
  (3,1,'isPredPrint','1'),
  (5,5,'isPredPrint','1'),
  (6,NULL,'BlockSaite','0'),
  (10,5,'countNewsMain','5'),
  (11,NULL,'showCountMonthSystemGurnalDefault','1'),
  (12,5,'CountVidNewsJournal','7'),
  (13,5,'CountVidNewsAdvert','7'),
  (14,1,'CountDataBookReader','3'),
  (15,NULL,'StaticMainPage','42, 47, 50, 56'),
  (16,NULL,'emailAdmin','aleksg83@mail.ru'),
  (17,NULL,'ListMenuMain','45, 1, 46'),
  (18,NULL,'IdVidNewsMain','4'),
  (19,NULL,'IdVidNewsMainRight','2'),
  (20,NULL,'ListMenuMainLimit','5, 11, 11'),
  (21,NULL,'EncyclopediaMenuType1','59'),
  (22,NULL,'EncyclopediaMenuType2','60'),
  (23,NULL,'EncyclopediaMenuCalendarj','69'),
  (24,NULL,'EncyclopediaMenuPerson','61'),
  (25,NULL,'EncyclopediaMenuOrg','62'),
  (26,NULL,'EncyclopediaMenuType3','68'),
  (27,NULL,'EncyclopediaMenuType4','125'),
  (28,NULL,'countOnPagePersonOrganisation','15'),
  (29,NULL,'countOnListInEnciclopediaStaticRazdel','12'),
  (30,NULL,'countOnPageAuthor','25');
COMMIT;

#
# Data for the `pharus_mapping` table  (LIMIT 0,500)
#

INSERT INTO `pharus_mapping` (`id_mapping`, `id_data`, `num_page`, `real_page`, `name`, `level`, `type`, `file`) VALUES 
  (1,4,1,'1','Обложка',1,1,'0001.jpg'),
  (2,4,2,'2','Титульный лист',1,1,'0002.jpg'),
  (3,4,3,'3','Цензурное разрешение',1,1,'0003.jpg'),
  (4,4,4,'4','Содержание',1,1,'0004.jpg'),
  (5,4,6,'6',NULL,NULL,1,'0005.jpg'),
  (6,4,5,'5',NULL,NULL,1,'0006.jpg'),
  (7,4,6,'7',NULL,NULL,1,'0007.jpg'),
  (8,4,8,'8','Отделение первое. Основные понятия и приготовления',1,1,'0008.jpg'),
  (9,4,9,'9','Глава первая. Изъяснение тригонометрических вспомогательных величин',2,1,'0009.jpg'),
  (10,4,10,'10','1. Синус и косинус',3,1,'0010.jpg'),
  (11,4,11,'11',NULL,NULL,1,'0011.jpg'),
  (12,4,12,'12',NULL,NULL,1,'0012.jpg'),
  (13,4,13,'13',NULL,NULL,1,'0013.jpg'),
  (14,4,14,'14',NULL,NULL,1,'0014.jpg'),
  (15,4,15,'15',NULL,NULL,1,'0015.jpg'),
  (16,4,16,'16',NULL,NULL,1,'0016.jpg'),
  (17,4,17,'17','2. Тангенс и котангенс',3,1,'0017.jpg'),
  (18,4,18,'18',NULL,NULL,1,'0018.jpg'),
  (19,4,19,'19',NULL,NULL,1,'0019.jpg'),
  (20,4,20,'20','3. Секанс и косеканс',3,1,'0020.jpg'),
  (21,4,21,'21',NULL,NULL,1,'0021.jpg'),
  (22,4,22,'22','4. Синус и косинус обращенный',3,1,'0022.jpg'),
  (23,4,23,'23',NULL,NULL,1,'0023.jpg'),
  (24,4,24,'24','Глава вторая. Взаимные отношения тригонометрических вспомогательных величин (тригонометрические уравнения)',2,1,'0024.jpg'),
  (25,4,25,'25','I. Уравнения между тригонометрическими линиями той же дуги',3,1,'0025.jpg'),
  (26,4,26,'26',NULL,NULL,1,'0026.jpg'),
  (27,4,27,'27',NULL,NULL,1,'0027.jpg'),
  (28,4,28,'28','II. Между тригонометрическими линиями простых и сложных дуг',3,1,'0028.jpg'),
  (29,4,29,'29',NULL,NULL,1,'0029.jpg'),
  (30,4,30,'30',NULL,NULL,1,'0030.jpg'),
  (31,4,31,'31',NULL,NULL,1,'0031.jpg'),
  (32,4,32,'32',NULL,NULL,1,'0032.jpg'),
  (33,4,33,'33',NULL,NULL,1,'0033.jpg'),
  (34,4,34,'34',NULL,NULL,1,'0034.jpg'),
  (35,4,35,'35',NULL,NULL,1,'0035.jpg'),
  (36,4,36,'36',NULL,NULL,1,'0036.jpg'),
  (37,4,37,'37','Глава третья. Сочинение и употребление тригонометрических таблиц',2,1,'0037.jpg'),
  (38,4,38,'38',NULL,NULL,1,'0038.jpg'),
  (39,4,39,'39',NULL,NULL,1,'0039.jpg'),
  (40,4,40,'40',NULL,NULL,1,'0040.jpg'),
  (41,4,41,'41',NULL,NULL,1,'0041.jpg'),
  (42,4,42,'42',NULL,NULL,1,'0042.jpg'),
  (43,4,43,'43',NULL,NULL,1,'0043.jpg'),
  (44,4,44,'44',NULL,NULL,1,'0044.jpg'),
  (45,4,45,'45',NULL,NULL,1,'0045.jpg'),
  (46,4,46,'46',NULL,NULL,1,'0046.jpg'),
  (47,4,47,'47',NULL,NULL,1,'0047.jpg'),
  (48,4,48,'48',NULL,NULL,1,'0048.jpg'),
  (49,4,49,'49',NULL,NULL,1,'0049.jpg'),
  (50,4,50,'50',NULL,NULL,1,'0050.jpg'),
  (51,4,51,'51',NULL,NULL,1,'0051.jpg'),
  (52,4,52,'52',NULL,NULL,1,'0052.jpg'),
  (53,4,53,'53','Глава четверая. Приложение тригонометрических вспомогательных величин и таблиц к разрешению геометрических и арифметических задач',2,1,'0053.jpg'),
  (54,4,54,'54','I. Измерение и строение углов',3,1,'0054.jpg'),
  (55,4,55,'55','II. Приложение к алгебраическим уравнениям',3,1,'0055.jpg'),
  (56,4,56,'56',NULL,NULL,1,'0056.jpg'),
  (57,4,57,'57',NULL,NULL,1,'0057.jpg'),
  (58,4,58,'58',NULL,NULL,1,'0058.jpg'),
  (59,4,59,'59',NULL,NULL,1,'0059.jpg'),
  (60,4,60,'60',NULL,NULL,1,'0069.jpg'),
  (61,4,61,'61','III. Сокращение тригонометрических уравнений',3,1,'0061.jpg'),
  (62,4,62,'62','Отделение второе. Плоские треугольники.',1,1,'0062.jpg'),
  (63,4,63,'63','Глава первая. Разрешение прямоугольных и равнобедренных треугольников',2,1,'0063.jpg'),
  (64,4,64,'64',NULL,NULL,1,'0064.jpg'),
  (65,4,65,'65','Глава вторая. Общее разрешение плоских треугольников',2,1,'0065.jpg'),
  (66,4,66,'66','I. Даны два угла и одна сторона',3,1,'0066.jpg'),
  (67,4,67,'67','II. Даны две стороны и угол между ними заключенный',3,1,'0067.jpg'),
  (68,4,68,'68',NULL,NULL,1,'0068.jpg'),
  (69,4,69,'69',NULL,NULL,1,'0069.jpg'),
  (70,4,70,'70',NULL,NULL,1,'0070.jpg'),
  (71,4,71,'71',NULL,NULL,1,'0071.jpg'),
  (72,4,72,'72','III. Даны две стороны и один противулежащий угол',3,1,'0072.jpg'),
  (73,4,73,'73','IV. Даны три стороны ',3,1,'0073.jpg'),
  (74,4,74,'74',NULL,NULL,1,'0074.jpg'),
  (75,4,75,'75',NULL,NULL,1,'0075.jpg'),
  (76,4,76,'76',NULL,NULL,1,'0076.jpg'),
  (77,4,77,'77','Вычисление площади плоского треугольника (п. 90)',3,1,'0077.jpg'),
  (78,4,78,'78','Прибавление к (84)',3,1,'0078.jpg'),
  (79,4,79,'79',NULL,NULL,1,'0079.jpg'),
  (80,4,80,'80','Отделение третье. Сферические треугольники',1,1,'0080.jpg'),
  (81,4,81,'81','Глава первая. Общие геометрические рассуждения о сферическом треугольнике',2,1,'0081.jpg'),
  (82,4,82,'82',NULL,NULL,1,'0082.jpg'),
  (83,4,83,'83',NULL,NULL,1,'0083.jpg'),
  (84,4,84,'84',NULL,NULL,1,'0084.jpg'),
  (85,4,85,'85',NULL,NULL,1,'0085.jpg'),
  (86,4,86,'86',NULL,NULL,1,'0086.jpg'),
  (87,4,87,'87',NULL,NULL,1,'0087.jpg'),
  (88,4,88,'88',NULL,NULL,1,'0088.jpg'),
  (89,4,89,'89',NULL,NULL,1,'0089.jpg'),
  (90,4,90,'90',NULL,NULL,1,'0090.jpg'),
  (91,4,91,'91',NULL,NULL,1,'0091.jpg'),
  (92,4,92,'92',NULL,NULL,1,'0092.jpg'),
  (93,4,93,'93','Глава вторая. Разрешение прямоугольных и равнобедренных сферических треугольников',2,1,'0093.jpg'),
  (94,4,94,'94',NULL,NULL,1,'0094.jpg'),
  (95,4,95,'95',NULL,NULL,1,'0095.jpg'),
  (96,4,96,'96',NULL,NULL,1,'0096.jpg'),
  (97,4,97,'97',NULL,NULL,1,'0097.jpg'),
  (98,4,98,'98','Глава третья. Разрешение косвенноугольных сферических треугольников',2,1,'0098.jpg'),
  (99,4,99,'99',NULL,NULL,1,'0099.jpg'),
  (100,4,100,'100',NULL,NULL,1,'0100.jpg'),
  (101,4,101,'101',NULL,NULL,1,'0101.jpg'),
  (102,4,102,'102',NULL,NULL,1,'0102.jpg'),
  (103,4,103,'103',NULL,NULL,1,'0103.jpg'),
  (104,4,104,'104','I. Даны все три стороны',3,1,'0104.jpg'),
  (105,4,105,'105','II. Даны две стороны и включенный угол',3,1,'0105.jpg'),
  (106,4,106,'106',NULL,NULL,1,'0106.jpg'),
  (107,4,107,'107',NULL,NULL,1,'0107.jpg'),
  (108,4,108,'108',NULL,NULL,1,'0108.jpg'),
  (109,4,109,'109','III. Даны две стороны и противулежащий угол',3,1,'0109.jpg'),
  (110,4,110,'110',NULL,NULL,1,'0110.jpg'),
  (111,4,111,'111',NULL,NULL,1,'0111.jpg'),
  (112,4,112,'112',NULL,NULL,1,'0112.jpg'),
  (113,4,113,'113',NULL,NULL,1,'0113.jpg'),
  (114,4,114,'114',NULL,NULL,1,'0114.jpg'),
  (115,4,115,'115',NULL,NULL,1,'0115.jpg'),
  (116,4,116,'116',NULL,NULL,1,'0116.jpg'),
  (117,4,117,'117',NULL,NULL,1,'0117.jpg'),
  (118,4,118,'118','IV. Даны два угла и одна противулежащая сторона',3,1,'0118.jpg'),
  (119,4,119,'119','V. Даны два угла и к ним прилежащая сторона',3,1,'0119.jpg'),
  (120,4,120,'120',NULL,NULL,1,'0120.jpg'),
  (121,4,121,'121',NULL,NULL,1,'0121.jpg'),
  (122,4,122,'122','VI. Даны три угла',3,1,'0122.jpg'),
  (123,4,123,'123','Вычисление площади сферического треугольника (п. 133)',3,1,'0123.jpg'),
  (124,4,124,'124','Чертежи',1,1,'0124.jpg'),
  (125,4,125,'125',NULL,NULL,1,'0125.jpg'),
  (126,4,126,'126',NULL,NULL,1,'0126.jpg'),
  (127,4,127,'127',NULL,NULL,1,'0127.jpg'),
  (171,7,1,'1','Обложка',1,1,'0001.jpg'),
  (172,7,2,'2','Титульный лист',1,1,'0002.jpg'),
  (173,7,3,'3','Сведения об оппонентах и месте защиты',1,1,'0003.jpg'),
  (174,7,4,'4','[Текст автореферата]',1,1,'0004.jpg'),
  (175,7,5,'5',NULL,NULL,1,'0005.jpg'),
  (176,7,6,'6',NULL,NULL,1,'0006.jpg'),
  (177,7,7,'7',NULL,NULL,1,'0007.jpg'),
  (178,7,8,'8',NULL,NULL,1,'0008.jpg'),
  (179,7,9,'9',NULL,NULL,1,'0009.jpg'),
  (180,7,10,'10',NULL,NULL,1,'0010.jpg'),
  (181,7,11,'11',NULL,NULL,1,'0011.jpg'),
  (182,7,12,'12',NULL,NULL,1,'0012.jpg'),
  (183,7,13,'13',NULL,NULL,1,'0013.jpg'),
  (184,7,14,'14',NULL,NULL,1,'0014.jpg'),
  (185,7,15,'15',NULL,NULL,1,'0015.jpg'),
  (186,7,16,'16',NULL,NULL,1,'0016.jpg'),
  (187,7,17,'17',NULL,NULL,1,'0017.jpg'),
  (188,7,18,'18',NULL,NULL,1,'0018.jpg'),
  (189,7,19,'19',NULL,NULL,1,'0019.jpg'),
  (190,7,20,'20',NULL,NULL,1,'0020.jpg'),
  (191,7,21,'21','Концевая страница',1,1,'0021.jpg'),
  (213,26,1,'1','Оглавление',1,1,'0001.jpg'),
  (214,26,2,'2','',0,1,'0002.jpg'),
  (215,26,3,'3','Предисловие',1,1,'0003.jpg'),
  (216,26,4,'4','',0,1,'0004.jpg'),
  (217,26,5,'5','Введение',1,1,'0005.jpg'),
  (218,26,6,'6','',0,1,'0006.jpg'),
  (219,26,7,'7','Тригонометрические величины',1,1,'0007.jpg'),
  (220,26,8,'32','4. Приведение формул к удобному виду',1,1,'0008.jpg'),
  (221,26,9,'33','',0,1,'0009.jpg'),
  (222,26,10,'34','',0,1,'0010.jpg'),
  (223,26,11,'38','Оглавление',1,1,'0011.jpg'),
  (440,47,1,'-','Обложка',1,1,'0001.jpg'),
  (441,47,2,'1','Титульный лист',1,1,'0002.jpg'),
  (442,47,3,'2','Сведения об оппонентах и месте защиты',1,1,'0003.jpg'),
  (443,47,4,'3','[Текст автореферата]',1,1,'0004.jpg'),
  (444,47,5,'4',NULL,NULL,1,'0005.jpg'),
  (445,47,6,'5',NULL,NULL,1,'0006.jpg'),
  (446,47,7,'6',NULL,NULL,1,'0007.jpg'),
  (447,47,8,'7',NULL,NULL,1,'0008.jpg'),
  (448,47,9,'8',NULL,NULL,1,'0009.jpg'),
  (449,47,10,'9',NULL,NULL,1,'0010.jpg'),
  (450,47,11,'10',NULL,NULL,1,'0011.jpg'),
  (451,47,12,'11',NULL,NULL,1,'0012.jpg'),
  (452,47,13,'12',NULL,NULL,1,'0013.jpg'),
  (453,47,14,'13',NULL,NULL,1,'0014.jpg'),
  (454,47,15,'14',NULL,NULL,1,'0015.jpg'),
  (455,47,16,'15',NULL,NULL,1,'0016.jpg'),
  (456,47,17,'16',NULL,NULL,1,'0017.jpg'),
  (457,47,18,'17',NULL,NULL,1,'0018.jpg'),
  (458,47,19,'18',NULL,NULL,1,'0019.jpg'),
  (459,47,20,'19',NULL,NULL,1,'0020.jpg'),
  (460,47,21,'20','Концевая страница',1,1,'0021.jpg'),
  (461,48,1,'-','Обложка',1,1,'0001.jpg'),
  (462,48,2,'1','Титульный лист',1,1,'0002.jpg'),
  (463,48,3,'2','Сведения об оппонентах и месте защиты',1,1,'0003.jpg'),
  (464,48,4,'3','[Текст автореферата]',1,1,'0004.jpg'),
  (465,48,5,'4',NULL,NULL,1,'0005.jpg'),
  (466,48,6,'5',NULL,NULL,1,'0006.jpg'),
  (467,48,7,'6',NULL,NULL,1,'0007.jpg'),
  (468,48,8,'7',NULL,NULL,1,'0008.jpg'),
  (469,48,9,'8',NULL,NULL,1,'0009.jpg'),
  (470,48,10,'9',NULL,NULL,1,'0010.jpg'),
  (471,48,11,'10',NULL,NULL,1,'0011.jpg'),
  (472,48,12,'11',NULL,NULL,1,'0012.jpg'),
  (473,48,13,'12',NULL,NULL,1,'0013.jpg'),
  (474,48,14,'13',NULL,NULL,1,'0014.jpg'),
  (475,48,15,'14',NULL,NULL,1,'0015.jpg'),
  (476,48,16,'15',NULL,NULL,1,'0016.jpg'),
  (477,48,17,'16',NULL,NULL,1,'0017.jpg'),
  (478,48,18,'17',NULL,NULL,1,'0018.jpg'),
  (479,48,19,'18',NULL,NULL,1,'0019.jpg'),
  (480,48,20,'19',NULL,NULL,1,'0020.jpg'),
  (481,48,21,'20','Концевая страница',1,1,'0021.jpg'),
  (482,25,0,'-','Обложка',1,0,''),
  (483,25,0,'1','Содержание',1,0,''),
  (484,25,0,'2','Глава 1',1,0,''),
  (485,25,0,'2','Подраздел 1',2,0,''),
  (486,54,1,'1','Оглавление',1,0,'0001.jpg'),
  (487,54,2,'4','Глава 1',1,0,'0002.jpg'),
  (488,54,3,'5','Подраздел 1',2,0,'0003.jpg'),
  (489,53,1,'1','Оглавление',1,1,'0001.jpg'),
  (490,53,2,'2','Глава 1',1,1,'0002.jpg'),
  (491,53,3,'4','Раздел 1.1.',2,1,'0003.jpg'),
  (492,53,5,'IV','Раздел 1.2',2,1,'0005.jpg'),
  (493,54,4,'12','Разде 2.3',2,0,'0004.jpg'),
  (494,22,1,'1','Оглавление',1,1,'0001.jpg'),
  (495,22,2,'3','Глава 1',1,1,'0002.jpg'),
  (496,22,3,'7','Глава 2',1,1,'0003.jpg');
COMMIT;

#
# Data for the `pharus_menu` table  (LIMIT 0,500)
#

INSERT INTO `pharus_menu` (`id_menu`, `id_menu_top`, `name`, `is_visible`, `url`, `text`, `is_rubricator`, `url_name`, `level`, `position`) VALUES 
  (1,0,'Библиотека',1,'lib/','<p>Текст о библиотеке</p> \r\n',0,'lib',1,1),
  (2,1,'Поступления',1,'lib/revenues','Это поступления\r\n',0,'revenues',2,1),
  (3,1,'Книги',1,'lib/book/','<p>Текстовое описание раздела, книги, находится в меню</p>',0,'book',2,2),
  (4,1,'Статьи',1,'lib/article/','<p>Описание раздела</p>',0,'article',2,3),
  (5,3,'Алгебра',1,'lib/book/algebra','',0,'algebra',3,4),
  (6,3,'Геометрия',1,'lib/book/geometry','Это геометрия',0,'geometry',3,5),
  (7,3,'Тригонометрия',1,'lib/book/trigonometry','Это тригонометрия',0,'trigonometry',3,6),
  (8,114,'Начальное образование',1,'biblish/primary_education/','',1,'primary_education',2,1),
  (9,8,'Воспитание детей',1,'biblish/primary_education/bringing_up_children','',1,'bringing_up_children',3,1),
  (10,8,'Обучение детей',1,'biblish/primary_education/education_of_children/','',1,'education_of_children',3,2),
  (11,114,'Учебники',1,'biblish/textbooks/',NULL,1,'textbooks',2,4),
  (12,11,'XVIII',1,'biblish/textbooks/XVIII','',1,'XVIII',3,1),
  (13,11,'XIX- начало XX века',1,'biblish/textbooks/XIX-XX','',1,'XIX-XX',3,2),
  (14,1,'Диафильмы',1,'lib/filmstrip/','',0,'filmstrip',2,4),
  (15,1,'Диссертационные материалы',1,'lib/thesis/','Диссматериалы',0,'thesis',2,5),
  (16,1,'Периодика',1,'lib/periodicals','Это периодика',0,'periodicals',2,6),
  (17,1,'Авторы',1,'lib/authors','Это авторы',0,'authors',2,7),
  (18,1,'Заглавия',0,'lib/titles','Это заглавия',0,'titles',2,8),
  (19,3,'Общая методика',1,'lib/book/general_procedure','Это общая методика',0,'general_procedure',3,1),
  (20,3,'Математика для малышей',1,'lib/book/math_for_kids','Это математика для малышей',0,'math_for_kids',3,2),
  (21,3,'Арифметика (математика)',1,'lib/book/arithmetic_mathematics','Это арифметика (математика)',0,'arithmetic_mathematics',3,3),
  (22,3,'Начала анализа',1,'lib/book/beginning_of_the_analysis','Это начала анализа',0,'beginning_of_the_analysis',3,7),
  (23,3,'История образования',1,'lib/book/history_of_education','Это история образования',0,'history_of_education',3,8),
  (24,3,'История науки',1,'lib/book/history_of_science','Это история науки',0,'history_of_science',3,9),
  (25,3,'Энциклопедии и справочники',1,'lib/book/reference','Это энциклопедии и справочники',0,'reference',3,10),
  (26,3,'Съезды и конференции',1,'lib/book/congresses_and_conferences','Это съезды и конференции',0,'congresses_and_conferences',3,12),
  (27,3,'Библиография',1,'lib/book/bibliography','Это библиография',0,'bibliography',3,11),
  (28,114,'Общее среднее образование',1,'biblish/secondary_education/','',1,'secondary_education',2,2),
  (29,28,'Педагогика и общая методика',1,'biblish/secondary_education/pedagogy_and_general_technique','',1,'pedagogy_and_general_technique',3,1),
  (30,28,'Математика и частная методика',1,'biblish/secondary_education/mathematics_and_the_private_method','',1,'mathematics_and_the_private_method',3,2),
  (31,28,'Проблемы математического образования',1,'biblish/secondary_education/problems_of_mathematical_education','',1,'problems_of_mathematical_education',3,3),
  (32,114,'Дополнительное и вузовское образование',1,'biblish/additional_and_university_education/','',1,'additional_and_university_education',2,3),
  (33,32,'Факультативные занятия',1,'biblish/additional_and_university_education/extracurricular_activities','',1,'extracurricular_activities',3,1),
  (34,32,'Олимпиады',1,'biblish/additional_and_university_education/olympics','',1,'olympics',3,2),
  (35,32,'Заочные школы',1,'biblish/additional_and_university_education/correspondence_schools','',1,'correspondence_schools',3,3),
  (36,32,'Вступительные экзамены в вузы',1,'biblish/additional_and_university_education/entrance_exams','',1,'entrance_exams',3,4),
  (37,32,'Подготовка учителя математики',1,'biblish/additional_and_university_education/mathematics_teacher_preparation','',1,'mathematics_teacher_preparation',3,5),
  (38,114,'Учебники и задачники по математике',1,'biblish/textbooks_and_problem_books_in_mathematics/','',1,'textbooks_and_problem_books_in_mathematics',2,5),
  (39,38,'Школьные учебники',1,'biblish/textbooks_and_problem_books_in_mathematics/school_textbooks','',1,'school_textbooks',3,1),
  (40,38,'Школьные задачники',1,'biblish/textbooks_and_problem_books_in_mathematics/school_problem_books','',1,'school_problem_books',3,2),
  (41,38,'Учебники и задачники для высшей школы',1,'biblish/textbooks_and_problem_books_in_mathematics/textbooks_and_problem_books_for_high_school','',1,'textbooks_and_problem_books_for_high_school',3,3),
  (42,11,'1918 - 1930',1,'biblish/textbooks/1918_1930','',1,'1918_1930',3,3),
  (43,11,'1930 - 1932',1,'biblish/textbooks/1930_1932','',1,'1930_1932',3,4),
  (44,11,'После 1932 года',1,'biblish/textbooks/1932','',1,'1932',3,5),
  (45,0,'Спутник учителя',1,'sputnik/','',0,'sputnik',1,0),
  (46,0,'Энциклопедия',1,'encyclopedia/','',0,'encyclopedia',1,2),
  (47,0,'Новости',1,'news','Это новости',0,'news',1,6),
  (48,0,'Книжный вестник',1,'bookJournal/','Это книжный вестник',0,'bookJournal',1,4),
  (49,0,'Ищу читателя',1,'reader/','Это ищу читателя',0,'reader',1,5),
  (59,46,'Математика',1,'encyclopedia/mathematics','Математика',0,'mathematics',2,1),
  (60,46,'Педагогика',1,'encyclopedia/pedagogy','Педагогика',0,'pedagogy',2,2),
  (61,46,'Персоны',1,'encyclopedia/person','Персоны',0,'person',2,3),
  (62,46,'Организации',1,'encyclopedia/organization','Организация',0,'organization',2,3),
  (68,46,'Хронология',1,'encyclopedia/chronology','Хронология',0,'chronology',2,8),
  (69,46,'Календарь',1,'encyclopedia/almanac','Календарь',0,'almanac',2,7),
  (70,48,'Общая методика',1,'bookJournal/general_rocedure',NULL,0,'general_rocedure',2,2),
  (71,48,'Математика для малышей',1,'bookJournal/kv_math_for_kids',NULL,0,'kv_math_for_kids',2,3),
  (72,48,'Арифметика (математика)',1,'bookJournal/kv_arithmetic_mathematics',NULL,0,'kv_arithmetic_mathematics',2,4),
  (73,48,'Алгебра',1,'bookJournal/kv_algebra',NULL,0,'kv_algebra',2,5),
  (74,48,'Геометрия',1,'bookJournal/kv_geometry',NULL,0,'kv_geometry',2,6),
  (75,48,'Тригонометрия',1,'bookJournal/kv_trigonometry',NULL,0,'kv_trigonometry',2,7),
  (76,48,'Начала анализа',1,'bookJournal/kv_beginning_of_the_analysis',NULL,0,'kv_beginning_of_the_analysis',2,8),
  (77,48,'История образования',1,'bookJournal/kv_history_of_education',NULL,0,'kv_history_of_education',2,9),
  (78,48,'История науки',1,'bookJournal/kv_history_of_science',NULL,0,'kv_history_of_science',2,10),
  (79,48,'Энциклопедии и справочники',1,'bookJournal/kv_reference',NULL,0,'kv_reference',2,11),
  (80,48,'Съезды и конференции',1,'bookJournal/conferences_and_conventions',NULL,0,'conferences_and_conventions',2,12),
  (81,48,'Библиография',1,'bookJournal/kv_bibliography',NULL,0,'kv_bibliography',2,13),
  (83,114,'Пособия',1,'biblish/kv_aid',NULL,1,'kv_aid',2,6),
  (89,49,'Алгебра',1,'reader/index/reader_algebra','Это алгебра',0,'reader_algebra',2,2),
  (90,49,'Геометрия',1,'reader/index/reader_geometry','Это геометрия',0,'reader_geometry',2,4),
  (91,49,'Тригонометрия',1,'reader/index/reader_trigonometry','Это тригонометрия',0,'reader_trigonometry',2,3),
  (92,49,'Общая методика',1,'reader/index/reader_general_procedure','Это общая методика',0,'reader_general_procedure',2,5),
  (93,49,'Математика для малышей',1,'reader/index/reader_math_for_kids','Это математика для малышей',0,'reader_math_for_kids',2,6),
  (94,49,'Арифметика (математика)',1,'reader/index/reader_arithmetic_mathematics','Это арифметика (математика)',0,'reader_arithmetic_mathematics',2,7),
  (95,49,'Начала анализа',1,'reader/index/reader_beginning_of_the_analysis','Это начала анализа',0,'reader_beginning_of_the_analysis',2,8),
  (96,49,'История образования',1,'reader/index/reader_history_of_education','Это история образования',0,'reader_history_of_education',2,9),
  (97,49,'История науки',1,'reader/index/reader_history_of_science','Это история науки',0,'reader_history_of_science',2,10),
  (98,49,'Энциклопедии и справочники',1,'reader/index/reader_reference','Это энциклопедии и справочники',0,'reader_reference',2,11),
  (99,49,'Съезды и конференции',1,'reader/index/reader_congresses_and_conferences','Это съезды и конференции',0,'reader_congresses_and_conferences',2,12),
  (100,49,'Библиография',1,'reader/index/reader_bibliography','Это библиография',0,'reader_bibliography',2,13),
  (112,10,'ТЕМП',1,'biblish/primary_education/education_of_children/dd','',1,'dd',4,1),
  (114,0,'Библиотека',1,'biblish/','',1,'biblish',1,1),
  (115,0,'Энциклопедия',1,'encyc/','',1,'encyc',1,2),
  (116,115,'Организации',1,'encyc/org/','',1,'org',2,1),
  (117,115,'Персоны',1,'encyc/person/','',1,'person',2,2),
  (118,117,'Ученые',1,'encyc/person/uc','',1,'uc',3,1),
  (119,117,'Деятели образования',1,'encyc/person/dobr','',1,'dobr',3,2),
  (120,117,'Педагоги-математики',1,'encyc/person/pedagog','',1,'pedagog',3,3),
  (121,116,'Университеты',1,'encyc/org/univer','',1,'univer',3,1),
  (122,116,'Педагогические вузы',1,'encyc/org/pvuz','',1,'pvuz',3,2),
  (123,116,'Институты',1,'encyc/org/institut','',1,'institut',3,3),
  (124,116,'Гимназии и училища',1,'encyc/org/gimnazii','',1,'gimnazii',3,4),
  (125,46,'События',1,'encyclopedia/event','',0,'event',2,6),
  (126,0,'Периодика',1,'periodika/','',1,'periodika',1,3),
  (127,126,'Учебники',1,'periodika/tutorials','',1,'tutorials',2,1),
  (128,126,'Пособия',1,'periodika/aid','',1,'aid',2,2),
  (129,126,'Задачники',1,'periodika/task','',1,'task',2,3),
  (130,4,'Алгебра',1,'lib/article/algebra','',0,'algebra',3,1),
  (131,4,'Геометрия',1,'lib/article/geometriya','',0,'geometriya',3,2),
  (132,14,'Алгебра',1,'lib/filmstrip/algebra','',0,'algebra',3,1),
  (133,14,'Геометрия',1,'lib/filmstrip/geometriya','',0,'geometriya',3,2),
  (134,15,'Алгебра',1,'lib/thesis/algebra','',0,'algebra',3,1),
  (135,15,'Геометрия',1,'lib/thesis/geometriya','',0,'geometriya',3,2),
  (136,0,'Книжный вестник',1,'book_vestnik/','',1,'book_vestnik',1,4),
  (137,136,'Учебники',1,'book_vestnik/uchebnik_kv','',1,'uchebnik_kv',2,1),
  (138,136,'Пособия',1,'book_vestnik/posibiya','',1,'posibiya',2,2),
  (139,136,'Задачники',1,'book_vestnik/zadachniki','',1,'zadachniki',2,3),
  (140,48,'Все книги',1,'bookJournal/index/kv_all_book',NULL,0,'kv_all_book',2,1),
  (141,0,'О проекте',1,'static/index/o-proekte','',0,'static/index/o-proekte',1,7);
COMMIT;

#
# Data for the `pharus_news` table  (LIMIT 0,500)
#

INSERT INTO `pharus_news` (`id_news`, `id_users`, `date`, `zagolovok`, `annotaciya`, `text`, `comment`, `is_main`, `is_visible`, `is_rss`, `url`, `main_theme`, `keywords`, `description`, `title`) VALUES 
  (13,1,'2013-07-17 03:11:36','Электронная библиотека «Математическое образование: прошлое и настоящее» новое слово как нам жить','Электронная библиотека «Математическое образование: прошлое и настоящее»','<p>sdfsdf</p>\r\n','',1,1,1,'afdsdaf.html',2,'','','Электронная библиотека «Математическое образование: прошлое и настоящее»'),
  (14,1,'2013-07-19 23:24:10','Электронная библиотека «Математическое образование: прошлое и настоящее»','Электронная библиотека «Математическое образование: прошлое и настоящее»','<p>текст</p>\r\n','',1,1,1,'novst.html',4,'','','Электронная библиотека «Математическое образование: прошлое и настоящее»'),
  (15,1,'2013-07-22 13:04:26','Бюджетные расходы на образование возрастут на 7%','Бюджеты растут, а образование всё хуже','<p>аннотация новости</p>\r\n','',1,1,1,'segodnya.html',1,'','','Сегодня'),
  (16,1,'2013-07-22 13:22:58','Новые потрясения в системе образования','Российское образование катится а пропасть!','<p>Российское образование катится а пропасть!</p>\r\n','',1,1,1,'novye-potryaseniya-v-sisteme-obrazovaniya.html',1,'','','Новые потрясения в системе образования'),
  (17,1,'2013-07-22 13:24:46','Что будет с академией наук?','Вновь задались вопросом - что будет с акадеемий наук','<p>Всё более странными становятся эти &laquo;учителя года&raquo;: математичка Анна Мехед публично врала о том, почему стала учителем; ее коллеги Дмитрий Гущин и Михаил Случ призывали ввести систему стукачества на экзаменах, а теперь учитель биологии за два месяца до выборов в Госдуму демонстрирует партбилет &laquo;Единой России&raquo;.</p>\r\n\r\n<p>Вот такие размышления по теме</p>\r\n','<p>Всё более странными становятся эти &laquo;учителя года&raquo;: математичка Анна Мехед публично врала о том, почему стала учителем; ее коллеги Дмитрий Гущин и Михаил Случ призывали ввести систему стукачества на экзаменах, а теперь учитель биологии за два месяца до выборов в Госдуму демонстрирует партбилет &laquo;Единой России&raquo;.</p>\r\n',1,1,1,'chto-budet-s-akademiey-nauk.html',0,'','','Что будет с академией наук?'),
  (18,1,'2013-07-22 13:25:45','Как жить в этой стране или что такое госраспил?','Итак - как же нам выживать при такой \\\"коруппции\\\"','<p>Итак - как же нам выживать при такой коруппции</p>\r\n','',1,1,1,'kak-zhit-v-etoy-strane-ili-chto-takoe-gosraspil.html',0,'','','Как жить в этой стране или что такое госраспил?'),
  (19,1,'2013-07-22 18:24:55','Преображенский суд Москвы арестовал ректора Государственного университета','Преображенский суд \"Москвы\" арестовал ректора Государственного университета управления Виктора Козбаненкоrn','<p><a class=\"openimg\" href=\"/upload/images/fototelegramma.jpg\">Нововея мерзопасконое действиеПреображенский</a> суд <a class=\"openimg\" href=\"/upload/images/galstuk.jpg\">Москвы</a> <a class=\"openimg\" href=\"/upload/images/galstuk.jpg\">Москвы</a> &quot;арестовал&quot; ректора Государственного университета управления Виктора Козбаненко</p>\r\n\r\n<p class=\"math\">a^2 + b^2 = c^2</p>\r\n','<p>Так &quot;бывает&quot;</p>\r\n',NULL,1,1,'preobrazhenskiy-sud-moskvy-arestoval-rektora-gosudarstvennogo-universiteta-upravleniya-viktora-kozbanenko.html',1,'','','Преображенский суд Москвы арестовал ректора Государственного университета управления Виктора Козбаненко');
COMMIT;

#
# Data for the `pharus_person` table  (LIMIT 0,500)
#

INSERT INTO `pharus_person` (`id_person`, `id_users`, `fam_dp`, `name`, `surname`, `patronymic`, `fio`, `other_name`, `years`, `day`, `month`, `year`, `is_bc`, `is_view_pam`, `is_view_izb`, `is_view_in_main`, `is_view_news`, `definition`, `short_definition`, `text`, `short_text`, `fio_full_enc`, `date_full_enc`, `url`, `is_in_encyclopedia`, `is_portret`, `keywords`, `description`, `title`, `photo`, `add_data`, `update_data`, `id_person_forwars`) VALUES 
  (1,1,'Бобынин B.B.','','','','Бобынин Виктор Викторович','','1849-1919',0,7,1848,0,0,0,1,0,'В 1872 окончил Моск. ун-т, где с 1882 начал читать курс истории математики. Магистерская дисс. Б. \"Математика древних египтян\" (1882) представляла собой исследование, выполненное на основе опубликованного незадолго до того математич. папируса из собраний Ринда. Более 40 лет Б. изучал и популяризировал историю математич. знаний.','В 1872 окончил Моск. ун-т, где с 1882 начал читать курс истории математики. Магистерская дисс. Б. \"Математика древних египтян\" (1882) представляла собой исследование, выполненное на основе опубликованного незадолго до того математич. папируса из собраний Ринда. Более 40 лет Б. изучал и популяризировал историю математич. знаний.','<p>dgdfgdfg</p>\r\n\r\n<p>dfgdfgdfg</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>dfgdfgd</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>dfg</p>','Выдающийся математик','Полное ФИО','Родился там-то, жил там то','bobynin-viktor-viktorovich.html',1,NULL,'','','','data/person/pifagor.foto.jpg','0000-00-00 00:00:00','2013-08-09 16:14:08',2),
  (2,1,'Извольский Н.А.','','','','Извольский Николай Александрович','','1870-1938',1,8,1883,0,0,0,1,0,'Выдающийся математик современности - дефиниция','Выдающийся математик современности - дефиниция','<p>Педагог и общественный деятель, Заслуженный учитель РФ, доктор педагогических наук, , больше известного как &laquo;Школа Ямбурга&raquo;.</p>\r\n\r\n<p class=\"zag2\">Биография</p>\r\n\r\n<p>Разработчик и автор адаптивной модели школы &mdash; новой модели разноуровневой и многопрофильной общеобразовательной массовой школы с набором классов различной направленности, образовательных услуг, открытой, для детей самых разных возможностей и способностей, вне зависимости от их индивидуальных психологических особенностей, здоровья, склонностей, материальной обеспечен-ности семьи. Самый главный посыл такого образовательного учреждения &mdash; не ребенок приспосабливается к школе, а школа адаптируется под возможности, по-требности и способности ребенка. Е. А. Ямбург &mdash; участник многих телевизионных и радиопередач по вопросам воспитания и обучения детей, развития культуры и общества. Главный редактор и автор проекта &laquo;Антология выстаивания и преображения. Век XX&raquo;.</p>\r\n\r\n<p class=\"zag2\">Основные публикации</p>\r\n\r\n<ul class=\"book\">\r\n\t<li class=\"color1\"><a href=\"#\" style=\"-moz-user-select: none;\" unselectable=\"on\">Всеобщее начальное обучение. &mdash; 1894.</a></li>\r\n\t<li>Всеобщее начальное обучение. &mdash; 1894.</li>\r\n\t<li>Всеобщее начальное обучение. &mdash; 1894.</li>\r\n</ul>\r\n\r\n<p class=\"zag2\">Контакты</p>\r\n\r\n<p>E-mail: <a href=\"#\">licey@samara.ru</a><br />\r\nWeb-site: <a href=\"#\">http://www.licey-samara.ru</a></p>','Выдающийся математик современности',NULL,NULL,'izvolskiy-nikolay-aleksandrovich.html',1,NULL,'','','Извольский Николай Александрович','data/person/373pifagor.foto.jpg','0000-00-00 00:00:00','2013-08-08 02:42:53',0),
  (4,1,'Фамилия','','','','Фамилия Имя Отчество','family','',10,10,1853,0,0,0,1,1,'','','<p>ываыва</p>','','[b]ааа[/b]','ааа','family.html',1,0,'','','Фамилия','data/person/family.jpg','2013-05-31 16:04:56','2013-10-09 11:56:38',0),
  (5,1,'Тестовая персона','','','','Тестовая персона Фамилия','','1983-2012',0,7,1983,0,0,0,NULL,0,'дефиниция с маленькой','дефиниция с маленькой','','текст для календаря',NULL,NULL,'testovaya-persona-familiya.html',1,NULL,'','','','','2013-07-29 20:00:22','2013-08-08 03:13:25',0),
  (6,1,'Барыкин И.С.','','','','Барыкин Иван Сергеевич','barykin_ivan_sergeevich','1823-1978',0,0,0,0,0,0,NULL,0,'Педагог, деятель народного образования, автор учебников для начальной школы','Педагог, деятель народного образования, автор учебников для начальной школы','','',NULL,NULL,'barykin-ivan-sergeevich.html',NULL,NULL,'','','Барыкин Иван Сергеевич','','2013-07-30 19:46:31','2013-08-01 23:55:08',0),
  (7,1,'Быков И.В.','','','','Быков Иван Сергеевич','bykov_ivan_sergeevich','',0,0,0,0,0,0,NULL,0,'Дефениция','Дефениция','','',NULL,NULL,'bykov-ivan-sergeevich.html',1,NULL,'','','','','2013-07-30 21:16:05','2013-07-30 21:16:40',0),
  (8,1,'Баруков С.А.','','','','Баруков Сергей Андреевич','barukov_s_a','1823-1978',7,2,1823,0,0,0,NULL,0,'Дефиниция','Дефиниция','<p>Текст энциклопедия</p>','','[b]Полное[/b] Фио','Дата рождения','barukov_s_a.html',1,0,'','','Баруков С.А.','data/person/barukov_s_a.jpg','2013-08-09 16:16:04','2013-09-30 00:59:03',0),
  (9,1,'Фам Д.П.','','','','Фимолионов В.П.','fimolionov_v_p','',0,0,0,0,0,0,NULL,0,'','','','','','','fimolionov-v-p.html',NULL,NULL,'','','Фам Д.П.','','2013-09-27 23:20:52',NULL,0),
  (10,1,'Тестовый перосонаж','','','','','test_x_x','',0,0,0,0,0,0,NULL,0,'','','','','','','test_x_x.html',1,1,'','','Тестовый перосонаж','data/person/test_x_x.jpg','2013-09-30 00:36:05',NULL,0);
COMMIT;

#
# Data for the `pharus_sp_status` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_status` (`id_sp_status`, `name`, `value`) VALUES 
  (1,'NEW','Новая рассылка'),
  (2,'ERROR','Ошибка отправки'),
  (3,'SEND','Отправлено и переведено в архив');
COMMIT;

#
# Data for the `pharus_rassilka` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rassilka` (`id_rassilka`, `tema`, `body`, `date_add`, `date_send`, `file`, `id_sp_status`) VALUES 
  (1,'Рассылка','<p>ячсячсячсясччсячс</p>\r\n','2013-08-10 00:47:59','0000-00-00 00:00:00','library.zip',1);
COMMIT;

#
# Data for the `pharus_rassilka_setting` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rassilka_setting` (`id_rassilka_setting`, `type`, `time_paket`, `kolvo_pisem`, `kolvo_addr`, `name_author`, `adress_author`, `adress_server`, `is_HTML`, `end_letter`, `about`, `info_after_sender`, `appeal_to_men`, `appeal_to_women`) VALUES 
  (1,1,15,50,20,'Имя отправителя','admin@mathedu.rt','servrt@fff.rt',1,'<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n','Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...','{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкцию по активации подписки Вы найдете в письме, которое отправленно на  электронный адрес {%email%}.','Уважаемый','Уважаемая');
COMMIT;

#
# Data for the `pharus_rel_data_data` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_data_data` (`id_rel_data_data`, `id_data`, `id_data_used`) VALUES 
  (17,9,4);
COMMIT;

#
# Data for the `pharus_rel_data_menu` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_data_menu` (`id_rel_data_menu`, `id_data`, `id_menu`) VALUES 
  (2,4,9),
  (5,7,10),
  (6,7,9),
  (10,7,83),
  (16,8,9),
  (17,8,11),
  (19,4,8),
  (20,21,8),
  (21,21,9),
  (22,21,10),
  (23,22,8),
  (24,22,9),
  (25,22,10),
  (56,26,8),
  (57,26,9),
  (58,26,10),
  (60,7,8),
  (61,4,10),
  (95,47,8),
  (96,47,9),
  (97,47,10),
  (98,48,8),
  (99,48,9),
  (100,48,10),
  (101,51,8),
  (102,51,9),
  (103,26,114),
  (104,26,28),
  (105,26,32),
  (106,26,33),
  (107,26,34),
  (108,26,35),
  (109,26,36),
  (110,25,8),
  (111,25,9),
  (112,25,10),
  (113,25,112),
  (114,8,8),
  (115,8,10),
  (116,22,28),
  (117,22,29),
  (118,22,30),
  (119,49,8),
  (120,49,9),
  (121,49,10),
  (122,50,8),
  (123,50,9),
  (124,50,10),
  (125,52,8),
  (126,52,9),
  (127,52,10),
  (128,25,114),
  (129,53,8),
  (130,53,9),
  (131,53,114),
  (132,53,10),
  (133,54,114),
  (134,54,8),
  (135,54,9),
  (136,22,114),
  (137,55,114),
  (138,55,8),
  (139,55,28),
  (140,55,10),
  (141,55,9),
  (142,4,126),
  (143,4,127),
  (144,4,128),
  (145,4,129),
  (146,7,126),
  (147,7,127),
  (148,7,128),
  (149,7,129),
  (150,48,114),
  (152,48,137),
  (153,48,138),
  (154,48,139);
COMMIT;

#
# Data for the `pharus_sp_type_person` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_type_person` (`id_sp_type_person`, `name`) VALUES 
  (1,'Автор'),
  (2,'Редактор'),
  (3,'Юбиляры'),
  (4,'Научный руководитель (консультант)'),
  (5,'Оппонент');
COMMIT;

#
# Data for the `pharus_rel_data_person` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_data_person` (`id_rel_data_person`, `id_data`, `id_person`, `id_sp_type_person`) VALUES 
  (149,4,2,2),
  (150,4,1,3),
  (151,25,1,1),
  (152,25,1,2),
  (153,25,2,3),
  (154,21,2,4),
  (155,21,4,5),
  (244,47,4,3),
  (245,47,1,4),
  (246,47,2,4),
  (247,47,4,5),
  (248,47,1,1),
  (249,47,2,1),
  (250,47,1,2),
  (251,48,4,3),
  (252,48,1,4),
  (253,48,2,4),
  (254,48,4,5),
  (255,48,1,1),
  (256,48,2,1),
  (257,48,1,2),
  (258,48,6,1),
  (260,47,6,1),
  (261,8,6,1),
  (262,25,6,1),
  (264,22,6,4),
  (265,22,1,1),
  (266,51,6,1),
  (267,50,6,1),
  (268,49,6,1),
  (269,4,6,2),
  (270,26,6,2),
  (271,22,6,2),
  (272,55,2,1);
COMMIT;

#
# Data for the `pharus_rel_menu_rub` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_menu_rub` (`id_rel_menu_rub`, `id_menu`, `id_rub`) VALUES 
  (62,61,118),
  (63,61,119),
  (64,61,120),
  (65,62,121),
  (66,62,122),
  (67,62,123),
  (68,62,124),
  (69,5,8),
  (70,5,9),
  (71,5,10),
  (72,5,114),
  (76,5,28),
  (77,5,29),
  (78,3,114),
  (79,3,8),
  (80,3,28),
  (81,3,9),
  (82,3,10),
  (83,3,30),
  (84,3,29),
  (85,130,8),
  (86,130,9),
  (87,130,28),
  (88,130,29),
  (89,130,30),
  (90,130,31),
  (92,4,8),
  (93,4,9),
  (94,4,10),
  (95,4,28),
  (96,4,32),
  (97,4,11),
  (98,4,29),
  (99,4,30),
  (100,4,31),
  (101,4,33),
  (102,4,34),
  (103,4,35),
  (104,4,36),
  (105,4,12),
  (106,4,13),
  (107,4,42),
  (108,4,43),
  (109,4,44),
  (110,4,38),
  (111,4,39),
  (112,4,40),
  (113,4,41),
  (114,4,83),
  (115,130,114),
  (116,130,32),
  (117,130,34),
  (118,130,35),
  (119,130,36),
  (120,130,37),
  (121,130,33),
  (122,132,8),
  (123,132,9),
  (124,132,10),
  (125,14,8),
  (126,14,10),
  (127,14,9),
  (128,15,8),
  (129,15,9),
  (130,15,10),
  (131,15,28),
  (134,15,29),
  (135,15,30),
  (136,15,31),
  (141,15,37),
  (147,134,8),
  (148,134,9),
  (149,134,10),
  (150,135,8),
  (151,135,9),
  (152,135,10),
  (153,135,112),
  (154,135,28),
  (155,135,29),
  (156,135,30),
  (157,135,31),
  (158,131,8),
  (159,131,9),
  (160,131,10),
  (161,131,28),
  (162,131,29),
  (163,131,31),
  (164,4,114),
  (165,131,114),
  (166,133,114),
  (167,133,8),
  (168,133,9),
  (169,133,10),
  (170,133,28),
  (171,133,29),
  (172,133,30),
  (173,14,114),
  (174,15,114),
  (183,70,136),
  (184,70,137),
  (185,70,138),
  (186,70,139),
  (187,71,136),
  (188,71,137),
  (189,71,138),
  (190,71,139),
  (195,72,136),
  (196,72,137),
  (197,72,138),
  (198,72,139);
COMMIT;

#
# Data for the `pharus_sp_education` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_education` (`id_sp_education`, `name`, `is_visible`, `position`) VALUES 
  (1,'Высшее',1,1),
  (2,'Среднее',1,2),
  (3,'Начальное',1,3);
COMMIT;

#
# Data for the `pharus_rel_news_sp_education` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_news_sp_education` (`id_rel_news_sp_education`, `id_news`, `id_sp_education`) VALUES 
  (6,17,1);
COMMIT;

#
# Data for the `pharus_sp_organasation` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_organasation` (`id_sp_organasation`, `name`, `is_visible`, `id_users`, `day`, `month`, `year`, `is_view_in_main`, `is_view_news`, `short_text`, `definition`, `text`, `url`, `is_in_encyclopedia`, `keywords`, `description`, `title`, `photo`, `add_data`, `update_data`, `position`) VALUES 
  (1,'Министерство образование',1,1,24,7,1983,1,1,'текст для главной','Дефиниция','<p class=\"zag2\">Описание</p>\r\n\r\n<p>Основан на базе лицея &laquo;Медицинского&raquo; в 2000 г.<br />\r\nОбучение ведется по следующим направлениям: информационные технологии, инженерное дело, естественные науки, иностранные языки, история.</p>\r\n\r\n<p class=\"zag2\">Преподавание математики</p>\r\n\r\n<p>В лицее математика преподается по наглядно-лабораторному методу, суть которого заключается в том, что дети не только считают, но и про-изводят различные действия над предметами.<br />\r\nУчителя математики: <a href=\"#\" style=\"-moz-user-select: none;\" unselectable=\"on\">О. А. Иванова</a> , В. Б. Петрова, А. П. Сидорова.</p>\r\n\r\n<p class=\"zag2\">Контакты</p>\r\n\r\n<p>E-mail: <a href=\"#\" style=\"-moz-user-select: none;\" unselectable=\"on\">licey@samara.ru</a><br />\r\nWeb-site: <a href=\"#\" style=\"-moz-user-select: none;\" unselectable=\"on\">http://www.licey-samara.ru</a></p>','org1.html',1,'','','Министерство образование','data/organisation/pifagor.foto.jpg','0000-00-00 00:00:00','2013-08-08 02:57:40',1),
  (2,'Школа №2',0,1,0,7,1993,1,0,'',NULL,'','shkola-2.html',1,'','','',NULL,'0000-00-00 00:00:00','2013-07-30 18:43:54',2),
  (3,'тестовая организация без месяца',1,1,0,0,2003,1,0,'описание этой организации',NULL,'','testovaya-organizaciya-bez-mesyaca.html',1,'','','тестовая организация без месяца','','2013-07-29 19:03:06','0000-00-00 00:00:00',0),
  (4,'Министерство для тестов',1,1,0,1,1983,0,0,'Обоснование создания',NULL,'<p class=\"zag2\">Описание</p>\r\n<p>\r\nОснован на базе лицея «Медицинского» в 2000 г.\r\n<br>\r\nОбучение ведется по следующим направлениям: информационные технологии, инженерное дело, естественные науки, иностранные языки, история.\r\n</p>\r\n<p class=\"zag2\">Преподавание математики</p>\r\n<p>\r\nВ лицее математика преподается по наглядно-лабораторному методу, суть которого заключается в том, что дети не только считают, но и про-изводят различные действия над предметами.\r\n<br>\r\nУчителя математики:\r\n<a href=\"#\" unselectable=\"on\" style=\"-moz-user-select: none;\">О. А. Иванова</a>\r\n, В. Б. Петрова, А. П. Сидорова.\r\n</p>\r\n<p class=\"zag2\">Контакты</p>\r\n<p>\r\nE-mail:\r\n<a href=\"#\" unselectable=\"on\" style=\"-moz-user-select: none;\">licey@samara.ru</a>\r\n<br>\r\nWeb-site:\r\n<a href=\"#\" unselectable=\"on\" style=\"-moz-user-select: none;\">http://www.licey-samara.ru</a>\r\n</p>','ministerstvo-dlya-testov.html',1,'','','Министерство для тестов','','2013-07-31 01:26:52','2013-07-31 03:31:58',0);
COMMIT;

#
# Data for the `pharus_rel_news_sp_organasation` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_news_sp_organasation` (`id_rel_news_sp_organasation`, `id_news`, `id_sp_organasation`) VALUES 
  (10,17,1),
  (11,16,1);
COMMIT;

#
# Data for the `pharus_sp_theme_news` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_theme_news` (`id_sp_theme_news`, `name`, `is_visible`, `is_main`, `url`, `position`) VALUES 
  (1,'Cтандарты образования',1,0,'standarts_education',10),
  (2,'Единый госэкзамен',1,0,'unified_state',2),
  (3,'Церковь и школа',1,0,'church_and_school',3),
  (4,'Реформы образования',1,0,'education_reform',4),
  (5,'Содержание образования',1,0,'content_of_education',5),
  (6,'Информатизация',1,0,'computerization',6),
  (7,'Профильное обучение',1,0,'specialized_education',7),
  (8,'Школьный учебник',1,0,'school_textbook',8);
COMMIT;

#
# Data for the `pharus_rel_news_sp_theme_news` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_news_sp_theme_news` (`id_rel_news_sp_theme_news`, `id_news`, `id_sp_theme_news`) VALUES 
  (19,13,6),
  (23,15,1),
  (24,14,1),
  (25,14,4),
  (32,17,1),
  (33,17,7),
  (35,16,1),
  (36,16,4),
  (50,19,1),
  (51,19,4);
COMMIT;

#
# Data for the `pharus_sp_vid_news` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_vid_news` (`id_sp_vid_news`, `name`, `is_visible`, `url`, `position`) VALUES 
  (1,'Статьи, интервью',1,'article',1),
  (2,'Объявления',1,'objyavleniya',2),
  (4,'Дневник проекта',1,'dnevnik_proekta',3),
  (5,'События и мнения',1,'sobytiya_i_mneniya',4);
COMMIT;

#
# Data for the `pharus_rel_news_sp_vid_news` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_news_sp_vid_news` (`id_rel_news_sp_vid_news`, `id_news`, `id_sp_vid_news`) VALUES 
  (24,13,4),
  (25,18,1),
  (26,18,4),
  (53,15,1),
  (54,15,2),
  (55,14,1),
  (56,14,2),
  (57,14,4),
  (61,17,1),
  (64,16,1),
  (91,19,1),
  (92,19,2);
COMMIT;

#
# Data for the `pharus_rel_person_menu` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_person_menu` (`id_rel_person_menu`, `id_person`, `id_menu`) VALUES 
  (1,1,118),
  (2,2,118),
  (3,2,119),
  (4,4,119),
  (5,5,119),
  (6,6,120),
  (7,7,120),
  (8,6,119),
  (9,1,119),
  (12,5,118),
  (13,4,118);
COMMIT;

#
# Data for the `pharus_user_email` table  (LIMIT 0,500)
#

INSERT INTO `pharus_user_email` (`id_user_email`, `name`, `email`, `is_active`, `is_black_list`, `sex`) VALUES 
  (1,'Пользователь','aleksg83@mail.ru',1,0,0),
  (2,'Алексей','qwerty@mail.ry',1,0,1);
COMMIT;

#
# Data for the `pharus_rel_sp_organasation_menu` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_sp_organasation_menu` (`id_rel_sp_organasation_menu`, `id_sp_organasation`, `id_menu`) VALUES 
  (1,1,8),
  (4,1,11),
  (5,1,9),
  (6,1,10),
  (7,1,121),
  (8,2,119),
  (9,3,123),
  (12,2,123),
  (13,3,122),
  (14,3,124),
  (15,1,122),
  (16,3,121),
  (18,4,121),
  (19,4,122);
COMMIT;

#
# Data for the `pharus_rel_users_pred_print` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_users_pred_print` (`id_rel_users_pred_print`, `id_users`, `id_data`, `id_news`, `prioritet`, `is_done`, `comment`, `is_comment_done`, `date_update`) VALUES 
  (4,2,0,15,1,1,NULL,0,'0000-00-00 00:00:00'),
  (5,4,0,15,2,1,NULL,0,'0000-00-00 00:00:00'),
  (6,2,0,16,1,1,NULL,0,'0000-00-00 00:00:00'),
  (7,4,0,16,2,1,NULL,0,'0000-00-00 00:00:00'),
  (8,2,0,17,1,1,NULL,0,'0000-00-00 00:00:00'),
  (9,4,0,17,2,1,NULL,0,'0000-00-00 00:00:00'),
  (10,2,0,18,1,0,'тествое комментарий _+++о',0,'2013-08-10 00:25:30'),
  (11,4,0,18,2,1,'Комментарий первого автора',0,'2013-07-25 03:40:51'),
  (12,2,0,19,1,1,NULL,0,'0000-00-00 00:00:00'),
  (13,4,0,19,2,1,NULL,0,'0000-00-00 00:00:00'),
  (14,2,52,0,1,0,NULL,0,'0000-00-00 00:00:00'),
  (15,4,52,0,2,0,NULL,0,'0000-00-00 00:00:00'),
  (16,3,52,0,3,0,NULL,0,'0000-00-00 00:00:00'),
  (17,2,53,0,1,0,NULL,0,'0000-00-00 00:00:00'),
  (18,4,53,0,2,0,NULL,0,'0000-00-00 00:00:00'),
  (19,3,53,0,3,0,NULL,0,'0000-00-00 00:00:00'),
  (20,2,54,0,1,1,NULL,0,'0000-00-00 00:00:00'),
  (21,4,54,0,2,0,NULL,0,'0000-00-00 00:00:00'),
  (22,3,54,0,3,0,NULL,0,'0000-00-00 00:00:00'),
  (23,2,55,0,1,0,NULL,0,'0000-00-00 00:00:00'),
  (24,4,55,0,2,0,NULL,0,'0000-00-00 00:00:00'),
  (25,3,55,0,3,0,NULL,0,'0000-00-00 00:00:00');
COMMIT;

#
# Data for the `pharus_roles` table  (LIMIT 0,500)
#

INSERT INTO `pharus_roles` (`id_roles`, `code`, `name`) VALUES 
  (1,'ADMIN','Администратор'),
  (2,'REDACTOR','Редактор'),
  (4,'PRINTRESURS','Предпринт ресурсов библиотеки'),
  (5,'PRINTNEWS','Предпринт новостей');
COMMIT;

#
# Data for the `pharus_rel_users_roles` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rel_users_roles` (`id_rel_users_roles`, `id_users`, `id_roles`) VALUES 
  (1,1,1),
  (14,2,2),
  (15,2,4),
  (16,2,5),
  (17,3,2),
  (18,3,4),
  (20,4,2),
  (21,4,5),
  (22,4,4),
  (25,7,2);
COMMIT;

#
# Data for the `pharus_rss` table  (LIMIT 0,500)
#

INSERT INTO `pharus_rss` (`id_rss`, `title`, `link`, `description`, `language`, `copyright`, `managingEditor`, `webMaster`, `pubDate`, `lastBuildDate`, `category`, `author`, `number`) VALUES 
  (1,'Математическое образование','http://mathedu.ru','Освещение вопросов математического образования в Россси','ru-ru','','','','2013-07-05 00:40:04','2013-08-10 00:57:23','Образование',NULL,20);
COMMIT;

#
# Data for the `pharus_sp_citata` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_citata` (`id_sp_citata`, `id_person`, `value`) VALUES 
  (1,1,'цитата');
COMMIT;

#
# Data for the `pharus_sp_polnota` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_polnota` (`id_sp_polnota`, `name`) VALUES 
  (1,'Полный'),
  (2,'Фрагмент'),
  (3,'Описание');
COMMIT;

#
# Data for the `pharus_sp_table_names` table  (LIMIT 0,500)
#

INSERT INTO `pharus_sp_table_names` (`id_sp_table_names`, `table`, `column`, `name`) VALUES 
  (1,NULL,'name','Имя'),
  (2,'sp_education','name','Образование'),
  (3,NULL,'value','Значение'),
  (4,NULL,'is_visible','Видимость'),
  (5,NULL,'position','Позиция'),
  (6,NULL,'url','Адрес'),
  (7,'menu','text','Описание раздела'),
  (8,NULL,'fam_dp','Фамилия в имен. падеже'),
  (9,NULL,'fio','ФИО'),
  (10,NULL,'is_view_in_main','Отображать на главной'),
  (11,NULL,'is_in_encyclopedia','Объект энциклопедии'),
  (12,NULL,'id_person_forwars','Отсылка'),
  (13,'news','date','Дата создания'),
  (14,'news','zagolovok','Заголовок'),
  (15,'news','annotaciya','Аннотация'),
  (16,'news','main_theme','Главная тема'),
  (17,'sp_type_person','name','Тип'),
  (18,'news','is_rss','Включать в RSS-поток'),
  (19,'data','name','Заглавие'),
  (20,'data','date_add','Дата добавления'),
  (21,'data','year','Год издания'),
  (22,'data','id_sp_polnota','Полнота материала'),
  (23,'data','is_view_in_rubrik','Доступность'),
  (24,'data','is_view_author','Отображать в разделе \"Авторы\"'),
  (25,'data','is_periodical','Переодическое'),
  (26,'data','is_recomended','Рекомендовано к прочтению'),
  (27,'data','is_in_stock','В наличии (для рубрики \"Ищу читателя\")'),
  (28,'data','vid_dis','Вид диссертации'),
  (29,'data','id_scientific_director','Научный руководитель'),
  (30,NULL,'is_main','Главная тема');
COMMIT;

#
# Data for the `pharus_static_page` table  (LIMIT 0,500)
#

INSERT INTO `pharus_static_page` (`id_static_page`, `id_static_page_top`, `id_menu`, `name`, `url`, `annotaciya`, `text`, `keywords`, `description`, `title`, `id_users`, `is_visible`, `is_menu`, `date_add`, `date_update`, `is_folders`, `position`, `level`, `link_out`, `id_page_forwars`) VALUES 
  (40,0,45,'Спутник учителя','stutnik-uchitelya','','<p>Текст страницы Спутник учителя</p>\r\n','','','Спутник учителя, много интересного для преподавателей',1,1,1,'2013-07-26 13:31:34','2013-07-27 17:12:24',1,1,1,NULL,NULL),
  (41,0,46,'Энциклопедия','enciklopediya','','<p>Текст раздела энциклопедия</p>\r\n','','','Энциклопедия',1,1,1,'2013-07-26 13:32:05','2013-07-28 21:09:41',1,2,1,NULL,NULL),
  (42,0,0,'Система координат','sistema-koordinat','','','','','Система координат',1,1,1,'2013-07-26 13:32:39','0000-00-00 00:00:00',1,3,1,NULL,NULL),
  (43,42,0,'Власть и общество','vlast-i-obschestvo','','<p>Текст раздела &quot;<a href=\"[[~67~]]\">Власть</a> и общество&quot;</p>\r\n','','','Власть и общество',1,1,1,'2013-07-26 13:33:02','2013-07-26 21:11:12',1,1,2,NULL,NULL),
  (44,42,0,'Времена образования','vremena-obrazovaniya.html','','','','','Времена образования',1,1,1,'2013-07-26 13:33:35','2013-07-26 13:34:02',0,2,2,NULL,NULL),
  (45,42,0,'Дети XXI века','deti-xxi-veka.html','','','','','Дети XXI века',1,1,1,'2013-07-26 13:34:23','2013-07-26 13:34:46',0,3,2,NULL,NULL),
  (46,42,0,'Герои нашего времени','geroi-nashego-vremeni.html','','','','','Герои нашего времени',1,1,1,'2013-07-26 13:34:56','0000-00-00 00:00:00',0,4,2,NULL,NULL),
  (47,0,0,'Человек и Текст','chelovek-i-tekst','','','','','Человек и текст',1,1,1,'2013-07-26 14:22:26','2013-07-26 14:22:49',1,4,1,NULL,NULL),
  (48,47,0,'Написание','napisanie.html','','','','','Написание',1,1,1,'2013-07-26 14:22:59','0000-00-00 00:00:00',0,1,2,NULL,NULL),
  (49,47,0,'Редактирование','redaktirovanie.html','','','','','Редактирование',1,1,1,'2013-07-26 14:23:25','2013-07-26 14:23:43',0,5,2,NULL,NULL),
  (50,0,0,'Исследователям','issledovatelyam','','','','','Исследователям',1,1,1,'2013-07-26 15:33:24','0000-00-00 00:00:00',1,5,1,NULL,NULL),
  (52,50,0,'Законодательство','zakonodatel-stvo.html','','','','','Законодательство',1,1,1,'2013-07-26 15:47:52','2013-07-26 15:50:40',0,1,2,NULL,NULL),
  (53,50,0,'Периодика','periodika-i.html','','','','','Периодика',1,1,1,'2013-07-26 15:48:57','2013-08-02 13:08:27',0,2,2,NULL,0),
  (54,50,0,'Библиография','bibliografiya.html','','','','','Библиография',1,1,1,'2013-07-26 15:49:08','2013-07-26 15:51:48',0,3,2,NULL,NULL),
  (55,50,0,'Архивы','arhivy.html','','','','','Архивы',1,1,1,'2013-07-26 15:49:20','2013-07-26 15:54:18',0,4,2,NULL,NULL),
  (56,0,0,'Библиотекa','biblioteka','','','','','Библиотеки',1,1,1,'2013-07-26 15:49:37','2013-07-26 15:55:53',1,6,1,NULL,NULL),
  (57,50,0,'Библиотеки','biblioteki.html','','','','','Библиотеки',1,1,1,'2013-07-26 15:54:34','0000-00-00 00:00:00',0,5,2,NULL,NULL),
  (58,56,0,'Цели и задачи','celi-i-zadachi.html','','','','','Цели и задачи',1,1,1,'2013-07-26 15:55:24','0000-00-00 00:00:00',0,1,2,NULL,NULL),
  (59,56,0,'Структура','struktura.html','','','','','Структура',1,1,1,'2013-07-26 15:56:08','2013-07-26 15:56:49',0,2,2,NULL,NULL),
  (60,56,0,'Содержание','soderzhanie.html','','','','','Содержание',1,1,1,'2013-07-26 15:56:19','0000-00-00 00:00:00',0,3,2,NULL,NULL),
  (61,56,0,'Технологии','tehnologii.html','','','','','Технологии',1,1,1,'2013-07-26 15:57:02','2013-07-26 15:57:22',0,4,2,NULL,NULL),
  (62,56,0,'Сотрудничество','sotrudnichestvo.html','','','','','Сотрудничество',1,1,1,'2013-07-26 15:57:34','0000-00-00 00:00:00',0,5,2,NULL,NULL),
  (63,56,0,'Обратная связь','feedback.html','','<p>Для связи с администратором сайта заполните форму:</p>\r\n\r\n<div class=\"form-feedback\">\r\n<p class=\"info\">&nbsp;</p>\r\n\r\n<form action=\"\" method=\"post\"><label>Ваше имя *</label>\r\n\r\n<p><input name=\"name\" type=\"text\" value=\"\" /></p>\r\n<label>Email *</label>\r\n\r\n<p><input name=\"email\" type=\"text\" value=\"\" /></p>\r\n<label>Текст сообщения</label>\r\n\r\n<p><textarea name=\"text\"></textarea></p>\r\n<label>Код проверки *</label>\r\n<p class=\"code\">&nbsp;</p>\r\n<p><a class=\"sender\" hreh=\"javascript:void(0)\">Отправить</a></p>\r\n</form>\r\n</div>\r\n','','','Обратная связь',1,1,1,'2013-07-26 15:58:30','2013-07-27 01:36:10',0,6,2,NULL,NULL),
  (64,56,0,'Помощь','pomosch.html','','','','','Помощь',1,1,1,'2013-07-26 15:58:47','0000-00-00 00:00:00',0,7,2,NULL,NULL),
  (65,47,0,'Цитаты, ссылки, сноски','citaty-ssylki-snoski.html','','','','','Цитаты, ссылки, сноски',1,1,1,'2013-07-26 16:09:37','0000-00-00 00:00:00',0,3,2,NULL,NULL),
  (66,47,0,'Указатели','ukazateli.html','','','','','Указатели',1,1,1,'2013-07-26 16:10:00','0000-00-00 00:00:00',0,4,2,NULL,NULL),
  (67,43,0,'Тестовая запись власть и общество','testovaya-zapis-vlast-i-obschestvo.html','','<p>Содержимое ресурса &quot;подчиненной области&quot;</p>\r\n','','','Тестовая запись власть и общество',1,1,1,'2013-07-17 00:00:00','0000-00-00 00:00:00',0,1,3,NULL,NULL),
  (68,40,0,'Стандарты','standarty','','','','','Стандарты',1,1,1,'2013-07-15 00:00:00','2013-07-27 13:39:36',1,1,2,NULL,NULL),
  (69,40,0,'Учебники и пособия','uchebniki-i-posobiya.html','Аннотация','<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\r\n<p style=\"margin-left: 40px;\">zxdsafsdfsdf</p>\r\n\r\n<p style=\"text-align: justify;\">k;lkl;kkl;lkasdasdasdasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd sxasdfsad</p>\r\n\r\n<p style=\"text-align: center;\">&nbsp;asdsadasd</p>\r\n\r\n<p style=\"text-align: justify;\">ada</p>\r\n\r\n<ul>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ul>\r\n','','','Учебники и пособия',1,1,1,'2013-07-27 13:39:58','2013-12-29 22:12:16',0,2,2,'',0),
  (70,40,0,'Единый государственный экзамен','edinyy-gosudarstvennyy-ekzamen.html','','','','','Единый государственный экзамен',1,1,1,'2013-07-27 13:40:42','0000-00-00 00:00:00',0,3,2,NULL,NULL),
  (71,40,0,'Государственная итоговая аттестация','gosudarstvennaya-itogovaya-attestaciya-edinyy-gosudarstvennyy-ekzamen.html','','','','','Государственная итоговая аттестация Единый государственный экзамен',1,1,1,'2013-07-27 13:41:04','2013-07-27 15:22:45',0,4,2,NULL,NULL),
  (72,40,0,'Мероприятия','meropriyatiya','','<p>Текст <a href=\"[[~77~]]\">раздела</a> раздела <a href=\"[[~68~]]\">ссылка</a> и другая<a href=\"[[~40~]]\"> ссылка</a> на главную </p>\r\n','','','Мероприятия',1,1,1,'2013-07-27 13:41:45','2013-07-28 14:43:17',1,5,2,NULL,NULL),
  (73,40,0,'Аттестация и повышение квалификации','attestaciya-i-povyshenie-kvalifikacii.html','','','','','Аттестация и повышение квалификации',1,1,1,'2013-07-27 13:42:50','0000-00-00 00:00:00',0,6,2,NULL,NULL),
  (74,40,0,'Интересные школы','interesnye-shkoly.html','','','','','Интересные школы',1,1,1,'2013-07-27 13:43:11','2013-07-27 13:43:30',0,7,2,NULL,NULL),
  (75,40,0,'Еженедельник новых книг','ezhenedel-nik-novyh-knig.html','','','','','Еженедельник новых книг',1,1,1,'2013-07-27 13:44:01','2013-07-27 13:44:54',0,8,2,NULL,NULL),
  (76,40,0,' Web-ресурсы','web-resursy.html','','','','',' Web-ресурсы',1,1,1,'2013-07-27 13:45:02','0000-00-00 00:00:00',0,9,2,NULL,NULL),
  (77,72,0,'Мероприятия тест','meropriyatiya-test.html','','<p>текст раздела</p>\r\n','','','Мероприятия тест',1,1,1,'2013-07-27 17:34:01','0000-00-00 00:00:00',0,1,3,NULL,NULL),
  (78,68,0,'Правила','pravila.html','','','','','Правила',1,1,1,'2013-07-28 00:03:35','0000-00-00 00:00:00',0,1,3,NULL,NULL),
  (79,68,0,'Госты','gosty.html','','','','','Госты',1,1,1,'2013-07-28 01:14:23','2013-07-28 01:15:02',0,2,3,NULL,NULL),
  (80,41,59,'Математика','matematika','','<p>Текст Математика - энциклопедия</p>\r\n','','','Математика',1,1,1,'2013-07-28 19:38:25','2013-07-28 19:40:29',1,1,2,NULL,NULL),
  (81,41,60,'Педагогика','pedagogika','','<p>Текстовое описание Педагогики</p>\r\n','','','Педагогика',1,1,1,'2013-07-28 20:59:35','2013-07-31 15:27:44',1,2,2,NULL,NULL),
  (82,41,68,'Хронология','hronologiya.html','','<p>Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. &laquo;Что со мной случилось? &raquo; &ndash; подумал он. Это не было сном. Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах. Над столом, где были разложены распакованные образцы сукон &ndash; Замза был коммивояжером, &ndash; висел портрет, который он недавно вырезал из иллюстрированного журнала и вставил в красивую золоченую рамку.</p>\r\n\r\n<div class=\"dopmaterials\">\r\n<table>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td class=\"zag\">История<br />\r\n\t\t\tматематики</td>\r\n\t\t\t<td class=\"zag\">История<br />\r\n\t\t\tпедагогики</td>\r\n\t\t\t<td class=\"zag\">История математического<br />\r\n\t\t\tобразования</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">50000 лет до н.э. &mdash; 20 г. до н.э.</a></td>\r\n\t\t\t<td><a href=\"#\">V в. до н. э.</a></td>\r\n\t\t\t<td><a href=\"#\">XVIII век</a></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">I&mdash;XII века</a></td>\r\n\t\t\t<td><a href=\"#\">IV в. до. н. э. &mdash; I в.</a></td>\r\n\t\t\t<td><a href=\"#\">XIX век</a></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">XIII&mdash;XV века</a></td>\r\n\t\t\t<td><a href=\"#\">I&mdash;X века</a></td>\r\n\t\t\t<td><a href=\"#\">XX век</a></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"/encyclopedia/xvi.html\">XVI век</a></td>\r\n\t\t\t<td><a href=\"#\">XI в.</a></td>\r\n\t\t\t<td><a href=\"#\">XXI век</a></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">XVII век</a></td>\r\n\t\t\t<td><a href=\"#\">XII&mdash;XIV века</a></td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">XVIII век</a></td>\r\n\t\t\t<td><a href=\"#\">&hellip;</a></td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">XII&mdash;XIV века</a></td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">XIX век</a></td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><a href=\"#\">XX век</a></td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t\t<td>&nbsp;</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n</div>\r\n','','','Хронология',1,1,1,'2013-07-28 21:01:54','2013-07-31 13:28:36',0,4,2,NULL,NULL),
  (83,80,0,'Арифметика','arifmetika','','<p><a href=\"/encyclopedia/index/mathematics/arifmetika/chislo.html\">ссылка</a></p>\r\n','','','Арифметика',1,1,1,'2013-07-28 21:03:08','2013-07-31 22:27:09',1,1,3,NULL,0),
  (84,80,0,'Алгебра','algebra','','','','','Алгебра',1,1,1,'2013-07-28 21:03:41','0000-00-00 00:00:00',1,2,3,NULL,NULL),
  (85,80,0,'Геометрия','geometriya.html','','','','','Геометрия',1,1,1,'2013-07-28 21:04:46','0000-00-00 00:00:00',0,3,3,NULL,NULL),
  (86,83,0,'Число','chislo.html','','<p>Определние числа в арифметике</p>\r\n','','','Число',1,1,1,'2013-07-28 21:57:51','2013-09-22 23:49:17',0,1,4,'',87),
  (87,83,0,'Многочлены','mnogochleny.html','','<p>Текст многочлены</p>\r\n','','','Многочлены',1,1,1,'2013-07-28 21:59:00','0000-00-00 00:00:00',0,2,4,NULL,NULL),
  (88,41,125,'События','events','','<p>Текст &quot;События&quot;</p>\r\n','','','События',1,1,1,'2013-07-29 20:51:04','2013-07-31 13:29:13',1,5,2,NULL,NULL),
  (89,88,0,'Исторические','istoricheskie','Аннотация','<p>Текст &quot;Исторические&quot;, а также <a href=\"[[~95~]]\">искусство</a></p>\r\n','','','Исторические',1,1,1,'2013-07-29 20:51:56','2013-12-28 01:08:28',1,1,3,'',0),
  (90,88,0,'Наше время','nashe-vremya.html','','','','','Наше время',1,1,1,'2013-07-29 20:58:43','0000-00-00 00:00:00',0,2,3,NULL,NULL),
  (91,41,61,'Персоны','persony.html','','<p>Это главная страницах о персонах, текстовоы описание</p>\r\n','','','Персоны',1,1,1,'2013-07-30 15:34:22','2013-07-30 15:45:13',0,3,2,NULL,NULL),
  (92,41,62,'Организации','organizacii.html','','<p>Главная страница раздела Организации</p>\r\n','','','Организации',1,1,1,'2013-07-30 16:13:54','0000-00-00 00:00:00',0,6,2,NULL,NULL),
  (93,81,0,'Педагогика, 1 статья','pedagogika-1-stat-ya.html','','','','','Педагогика, 1 статья',1,1,1,'2013-07-31 12:58:03','2013-09-22 23:48:38',0,1,3,'',0),
  (94,81,0,'Абстракция','abstrakciya.html','','','','','Абстракция',1,1,1,'2013-07-31 12:58:57','2013-07-31 22:53:00',0,2,3,NULL,98),
  (95,89,0,'Искусство','iskusstvo.html','','<p>Искусство, привязка к историческим событиям</p>\r\n','','','Искусство',1,1,1,'2013-07-31 13:45:41','2013-07-31 13:48:26',0,1,4,NULL,NULL),
  (96,81,0,'Абсолютная величина','absolyutnaya-velichina.html','','<p>Абсолютная величина</p>\r\n','','','Абсолютная величина',1,1,1,'2013-07-31 15:54:36','0000-00-00 00:00:00',0,3,3,NULL,NULL),
  (97,81,0,'Авторская школа','avtorskaya-shkola.html','','','','','Авторская школа',1,1,1,'2013-07-31 16:31:26','0000-00-00 00:00:00',0,4,3,NULL,NULL),
  (98,81,0,'Авторитет','avtoritet.html','','<p>Авторитет</p>\r\n','','','Авторитет',1,1,1,'2013-07-31 16:57:53','0000-00-00 00:00:00',0,5,3,NULL,NULL),
  (99,83,0,'Анализ','analiz.html','','','','','Анализ',1,1,1,'2013-07-31 22:51:06','2013-07-31 22:52:09',0,3,4,NULL,86),
  (100,84,0,'Уравнения','urovneniya.html','','','','','Уровнения',1,1,1,'2013-07-31 23:25:30','2013-07-31 23:26:47',0,1,4,NULL,0),
  (101,84,0,'Линейное уравнение','lineynoe-uravnenie.html','','','','','Линейное уравнение',1,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00',0,2,4,NULL,100),
  (102,83,0,'Арифметика определение','arifmetika-opredelenie.html','','','','','Арифметика определение',1,1,1,'2013-07-31 23:46:52','0000-00-00 00:00:00',0,4,4,NULL,0),
  (103,84,0,'Аппробация','approbaciya.html','','','','','Аппробация',1,1,1,'2013-07-31 23:48:06','0000-00-00 00:00:00',0,3,4,NULL,0),
  (104,0,16,'Периодика','periodika','','<p>Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. &laquo;Что со мной случилось? &raquo; &ndash; подумал он.</p>\r\n\r\n<p>Это не было сном. Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах.</p>\r\n\r\n<p>Над столом, где были разложены распакованные образцы сукон &ndash; Замза был коммивояжером, &ndash; висел портрет, который он недавно вырезал из иллюстрированного журнала и вставил в красивую золоченую рамку.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"dopmaterials\">\r\n<p class=\"zag\">Журналы для учителей и преподавателей</p>\r\n\r\n<p><a href=\"[[~105~]]\">Вестник опытной физики и элементарной математики (1886&mdash;1917)</a></p>\r\n\r\n<p><a href=\"#\">Квант (1970 &mdash; настоящее время)</a></p>\r\n\r\n<p><a href=\"#\">Математика в школе (1934 &mdash; настоящее время)</a></p>\r\n\r\n<p><a href=\"#\">Математический вестник (1914&mdash;1917)</a></p>\r\n\r\n<p><a href=\"#\">Математическое образование (1912&mdash;1917, 1928&mdash;1930)</a></p>\r\n</div>\r\n\r\n<div class=\"dopmaterials\">\r\n<p class=\"zag\">Издания высших учебных заведений</p>\r\n\r\n<p><a href=\"#\">Известия Ростовского педагогического института (1938 &mdash; настоящее время)</a></p>\r\n\r\n<p><a href=\"#\">Ученые записки Московского университета. Серия &laquo;Математика&raquo; (1925&mdash;1933)</a></p>\r\n</div>\r\n\r\n<div class=\"dopmaterials\">\r\n<p class=\"zag\">Издания по истории науки</p>\r\n\r\n<p><a href=\"#\">Вопросы истории естествознания и техники (1948 &mdash; настоящее время)</a></p>\r\n\r\n<p><a href=\"#\">Историко-математические исследования (1948 &mdash; настоящее время) </a></p>\r\n</div>\r\n','','','Периодика',1,1,1,'2013-08-02 00:17:48','2013-08-02 13:12:00',1,7,1,NULL,0),
  (105,104,0,'Вестник опытной физики и элементарной математики','vestnik-opytnoy-fiziki-i-elementarnoy-matematiki','','<p>Текст Вестника, подробное<a href=\"[[~106~]]\"> расписание</a></p>\r\n','','','Вестник опытной физики и элементарной математики',1,1,1,'2013-08-02 13:02:51','2013-08-02 14:15:15',1,1,2,NULL,0),
  (106,105,0,'Вестник 1','vestnik-1.html','','<p>Раздел подвестника</p>\r\n','','','Вестник 1',1,1,1,'2013-08-02 13:48:42','2013-08-02 13:49:44',0,8,3,NULL,0),
  (107,0,0,'О проекте','o-proekte','','','','','О проекте',1,1,1,'2013-09-22 19:12:02','0000-00-00 00:00:00',1,8,1,NULL,0),
  (108,107,0,'Библиотека','biblioteka-2.html','','','','','Библиотека',1,1,1,'2013-09-22 19:12:53','2013-09-22 23:14:06',0,1,2,'',0);
COMMIT;

#
# Data for the `pharus_system_gurnal` table  (LIMIT 0,500)
#

INSERT INTO `pharus_system_gurnal` (`id_system_gurnal`, `id_users`, `login`, `action`, `date`, `ip_address`, `db_old`, `db_new`) VALUES 
  (6542,1,'admin','/admin/systemGurnal/','2013-10-09 23:23:37','127.0.0.1',NULL,NULL),
  (6543,1,'admin','/admin/formShowSendUser/','2013-10-09 23:23:55','127.0.0.1',NULL,NULL),
  (6544,1,'admin','/admin/addSendUser/','2013-10-09 23:24:26','127.0.0.1',NULL,'a:5:{s:13:\"id_user_email\";s:1:\"1\";s:4:\"name\";s:24:\"Пользователь\";s:5:\"email\";s:16:\"aleksg83@mail.ru\";s:9:\"is_active\";s:1:\"1\";s:13:\"is_black_list\";s:1:\"0\";}'),
  (6545,1,'admin','/admin/newsSendUser/','2013-10-09 23:24:27','127.0.0.1',NULL,NULL),
  (6546,1,'admin','/admin/rassilkaSend/','2013-10-10 01:26:46','127.0.0.1',NULL,NULL),
  (6547,1,'admin','/admin/updateSettingRassilka/','2013-10-10 01:26:51','127.0.0.1',NULL,NULL),
  (6548,1,'admin','/admin/','2013-10-10 16:12:15','127.0.0.1',NULL,NULL),
  (6549,1,'admin','/admin/showLib/book/','2013-10-10 16:12:21','127.0.0.1',NULL,NULL),
  (6550,1,'admin','/admin/showPerson/','2013-10-10 16:12:25','127.0.0.1',NULL,NULL),
  (6551,1,'admin','/admin/formShowPerson/','2013-10-10 16:12:28','127.0.0.1',NULL,NULL),
  (6552,1,'admin','/admin/showPerson/','2013-10-10 16:13:47','127.0.0.1',NULL,NULL),
  (6553,1,'admin','/admin/formShowPerson/','2013-10-10 16:13:52','127.0.0.1',NULL,NULL),
  (6554,1,'admin','/admin/showPerson/','2013-10-10 16:16:24','127.0.0.1',NULL,NULL),
  (6555,1,'admin','/admin/formShowPerson/','2013-10-10 16:16:29','127.0.0.1',NULL,NULL),
  (6556,1,'admin','/admin/','2013-10-10 16:19:27','127.0.0.1',NULL,NULL),
  (6557,1,'admin','/admin/showPerson/','2013-10-10 16:19:34','127.0.0.1',NULL,NULL),
  (6558,1,'admin','/admin/formShowPerson/','2013-10-10 16:19:37','127.0.0.1',NULL,NULL),
  (6559,1,'admin','/admin/showPerson/','2013-10-10 16:20:18','127.0.0.1',NULL,NULL),
  (6560,1,'admin','/admin/formShowPerson/','2013-10-10 16:20:22','127.0.0.1',NULL,NULL),
  (6561,1,'admin','/admin/showPerson/','2013-10-10 16:20:33','127.0.0.1',NULL,NULL),
  (6562,1,'admin','/admin/formShowPerson/','2013-10-10 16:20:42','127.0.0.1',NULL,NULL),
  (6563,1,'admin','/admin/showPerson/','2013-10-10 16:21:47','127.0.0.1',NULL,NULL),
  (6564,1,'admin','/admin/formShowPerson/','2013-10-10 16:21:52','127.0.0.1',NULL,NULL),
  (6565,1,'admin','/admin/showPerson/','2013-10-10 16:21:58','127.0.0.1',NULL,NULL),
  (6566,1,'admin','/admin/showPerson/','2013-10-10 16:22:00','127.0.0.1',NULL,NULL),
  (6567,1,'admin','/admin/formShowPerson/','2013-10-10 16:22:05','127.0.0.1',NULL,NULL),
  (6568,1,'admin','/admin/showPerson/','2013-10-10 16:22:51','127.0.0.1',NULL,NULL),
  (6569,1,'admin','/admin/formShowPerson/','2013-10-10 16:22:55','127.0.0.1',NULL,NULL),
  (6570,1,'admin','/admin/showPerson/','2013-10-10 16:23:00','127.0.0.1',NULL,NULL),
  (6571,1,'admin','/admin/formShowPerson/','2013-10-10 16:23:03','127.0.0.1',NULL,NULL),
  (6572,1,'admin','/admin/showPerson/','2013-10-10 16:23:49','127.0.0.1',NULL,NULL),
  (6573,1,'admin','/admin/formShowPerson/','2013-10-10 16:23:52','127.0.0.1',NULL,NULL),
  (6574,1,'admin','/admin/showPerson/','2013-10-10 16:24:21','127.0.0.1',NULL,NULL),
  (6575,1,'admin','/admin/formShowPerson/','2013-10-10 16:24:25','127.0.0.1',NULL,NULL),
  (6576,1,'admin','/admin/formShowPerson/','2013-10-10 16:26:15','127.0.0.1',NULL,NULL),
  (6577,1,'admin','/admin/formShowPerson/','2013-10-10 16:27:12','127.0.0.1',NULL,NULL),
  (6578,1,'admin','/admin/showPerson/','2013-10-10 16:28:15','127.0.0.1',NULL,NULL),
  (6579,1,'admin','/admin/formShowPerson/','2013-10-10 16:28:25','127.0.0.1',NULL,NULL),
  (6580,1,'admin','/admin/showPerson/','2013-10-10 17:53:37','127.0.0.1',NULL,NULL),
  (6581,1,'admin','/admin/formShowPerson/','2013-10-10 17:53:44','127.0.0.1',NULL,NULL),
  (6582,1,'admin','/admin/showPerson/','2013-10-10 17:54:43','127.0.0.1',NULL,NULL),
  (6583,1,'admin','/admin/formShowPerson/','2013-10-10 17:54:50','127.0.0.1',NULL,NULL),
  (6584,1,'admin','/admin/formShowPerson/','2013-10-10 17:54:57','127.0.0.1',NULL,NULL),
  (6585,1,'admin','/admin/formShowPerson/','2013-10-10 17:55:26','127.0.0.1',NULL,NULL),
  (6586,1,'admin','/admin/formShowPerson/','2013-10-10 17:56:46','127.0.0.1',NULL,NULL),
  (6587,1,'admin','/admin/','2013-10-10 18:00:07','127.0.0.1',NULL,NULL),
  (6588,1,'admin','/admin/showPerson/','2013-10-10 18:00:12','127.0.0.1',NULL,NULL),
  (6589,1,'admin','/admin/formShowPerson/','2013-10-10 18:00:19','127.0.0.1',NULL,NULL),
  (6590,1,'admin','/admin/formShowPerson/','2013-10-10 18:00:46','127.0.0.1',NULL,NULL),
  (6591,1,'admin','/admin/formShowPerson/','2013-10-10 19:23:18','127.0.0.1',NULL,NULL),
  (6592,1,'admin','/admin/showPerson/','2013-10-11 12:18:01','127.0.0.1',NULL,NULL),
  (6593,1,'admin','/admin/updateSettingRassilka/','2013-10-12 00:16:38','127.0.0.1',NULL,NULL),
  (6594,1,'admin','/admin/showPerson/','2013-10-12 01:10:29','127.0.0.1',NULL,NULL),
  (6595,1,'admin','/admin/updateSettingRassilka/','2013-10-12 01:52:19','127.0.0.1',NULL,NULL),
  (6596,1,'admin','/admin/updateSettingRassilka/','2013-10-12 01:53:02','127.0.0.1',NULL,NULL),
  (6597,1,'admin','/admin/updateSettingRassilka/','2013-10-12 01:55:34','127.0.0.1',NULL,NULL),
  (6598,1,'admin','/admin/updateSettingRassilka/','2013-10-12 01:57:14','127.0.0.1',NULL,NULL),
  (6599,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 01:59:05','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";N;s:17:\"info_after_sender\";N;}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}'),
  (6600,1,'admin','/admin/updateSettingRassilka/','2013-10-12 01:59:05','127.0.0.1',NULL,NULL),
  (6601,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 01:59:58','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:6:\"ввв\";}'),
  (6602,1,'admin','/admin/updateSettingRassilka/','2013-10-12 01:59:58','127.0.0.1',NULL,NULL),
  (6603,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 02:00:37','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:6:\"ввв\";}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}'),
  (6604,1,'admin','/admin/updateSettingRassilka/','2013-10-12 02:00:38','127.0.0.1',NULL,NULL),
  (6605,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 02:00:38','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}'),
  (6606,1,'admin','/admin/updateSettingRassilka/','2013-10-12 02:00:39','127.0.0.1',NULL,NULL),
  (6607,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 02:01:37','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:207:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}'),
  (6608,1,'admin','/admin/updateSettingRassilka/','2013-10-12 02:01:38','127.0.0.1',NULL,NULL),
  (6609,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 02:28:14','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:64:\"Благодарим за подписку, и всё такое\";}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:517:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкция по активации подписки в письме, отправленном на  электронный адрес {%email%}.\";}'),
  (6610,1,'admin','/admin/updateSettingRassilka/','2013-10-12 02:28:14','127.0.0.1',NULL,NULL),
  (6611,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 02:30:58','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:517:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкция по активации подписки в письме, отправленном на  электронный адрес {%email%}.\";}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:530:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкция по активации подписки в письме, которое отправленно на  электронный адрес {%email%}.\";}'),
  (6612,1,'admin','/admin/updateSettingRassilka/','2013-10-12 02:30:59','127.0.0.1',NULL,NULL),
  (6613,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 02:59:01','127.0.0.1','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:530:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкция по активации подписки в письме, которое отправленно на  электронный адрес {%email%}.\";}','a:12:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:550:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкцию по активации подписки Вы найдете в письме, которое отправленно на  электронный адрес {%email%}.\";}'),
  (6614,1,'admin','/admin/updateSettingRassilka/','2013-10-12 02:59:01','127.0.0.1',NULL,NULL),
  (6615,1,'admin','/admin/updateSettingRassilka/','2013-10-12 03:11:53','127.0.0.1',NULL,NULL),
  (6616,1,'admin','/admin/showUser/','2013-10-12 03:16:25','127.0.0.1',NULL,NULL),
  (6617,1,'admin','/admin/newsSendUser/','2013-10-12 03:16:32','127.0.0.1',NULL,NULL),
  (6618,1,'admin','/admin/rassilkaSend/','2013-10-12 03:16:42','127.0.0.1',NULL,NULL),
  (6619,1,'admin','/admin/updateSettingRassilka/','2013-10-12 03:17:05','127.0.0.1',NULL,NULL),
  (6620,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 03:17:46','127.0.0.1','a:14:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:550:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкцию по активации подписки Вы найдете в письме, которое отправленно на  электронный адрес {%email%}.\";s:13:\"appeal_to_men\";N;s:15:\"appeal_to_women\";N;}','a:14:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:550:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкцию по активации подписки Вы найдете в письме, которое отправленно на  электронный адрес {%email%}.\";s:13:\"appeal_to_men\";s:18:\"Уважаемый\";s:15:\"appeal_to_women\";s:0:\"\";}'),
  (6621,1,'admin','/admin/updateSettingRassilka/','2013-10-12 03:17:46','127.0.0.1',NULL,NULL),
  (6622,1,'admin','/admin/updateSettingRassilkaSave/','2013-10-12 03:17:54','127.0.0.1','a:14:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:550:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкцию по активации подписки Вы найдете в письме, которое отправленно на  электронный адрес {%email%}.\";s:13:\"appeal_to_men\";s:18:\"Уважаемый\";s:15:\"appeal_to_women\";s:0:\"\";}','a:14:{s:19:\"id_rassilka_setting\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:10:\"time_paket\";s:2:\"15\";s:11:\"kolvo_pisem\";s:2:\"50\";s:10:\"kolvo_addr\";s:2:\"20\";s:11:\"name_author\";s:29:\"Имя отправителя\";s:13:\"adress_author\";s:16:\"admin@mathedu.rt\";s:13:\"adress_server\";s:13:\"servrt@fff.rt\";s:7:\"is_HTML\";s:1:\"1\";s:10:\"end_letter\";s:75:\"<p>окончание</p>\r\n\r\n<p>ывавыа</p>\r\n\r\n<p>ыываыва</p>\r\n\";s:5:\"about\";s:273:\"Рассылка, в нее включается всё, при этом она бесплатна, а адреса почты никому \"не передаются\". Вообщем полная круть, и куды дальше бечь ни кто не знает...\";s:17:\"info_after_sender\";s:550:\"{%user_name%}! Благодарим Вас за подписку. Указанный Вами при регистрации электронный адрес {%email%} добавлен в базу подписчиков. Для получения рассылки с нашего сайта, Вам необходимо активировать подписку. Инструкцию по активации подписки Вы найдете в письме, которое отправленно на  электронный адрес {%email%}.\";s:13:\"appeal_to_men\";s:18:\"Уважаемый\";s:15:\"appeal_to_women\";s:18:\"Уважаемая\";}'),
  (6623,1,'admin','/admin/updateSettingRassilka/','2013-10-12 03:17:55','127.0.0.1',NULL,NULL),
  (6624,1,'admin','/admin/newsSendUser/','2013-10-12 03:18:45','127.0.0.1',NULL,NULL),
  (6625,1,'admin','/admin/newsSendUser/','2013-10-12 03:21:08','127.0.0.1',NULL,NULL),
  (6626,1,'admin','/admin/newsSendUser/','2013-10-12 03:24:58','127.0.0.1',NULL,NULL),
  (6627,1,'admin','/admin/newsSendUser/','2013-10-12 03:25:50','127.0.0.1',NULL,NULL),
  (6628,1,'admin','/admin/newsSendUser/','2013-10-12 03:26:37','127.0.0.1',NULL,NULL),
  (6629,1,'admin','/admin/showPerson/','2013-10-12 03:27:33','127.0.0.1',NULL,NULL),
  (6630,1,'admin','/admin/newsSendUser/','2013-10-12 03:27:36','127.0.0.1',NULL,NULL),
  (6631,1,'admin','/admin/newsSendUser/','2013-10-12 03:28:14','127.0.0.1',NULL,NULL),
  (6632,1,'admin','/admin/newsSendUser/','2013-10-12 03:28:46','127.0.0.1',NULL,NULL),
  (6633,1,'admin','/admin/redactSlovar/','2013-10-12 03:29:05','127.0.0.1',NULL,NULL),
  (6634,1,'admin','/admin/redactSlovar/','2013-10-12 03:29:32','127.0.0.1',NULL,NULL),
  (6635,1,'admin','/admin/formShowLangVariables/','2013-10-12 03:29:44','127.0.0.1',NULL,NULL),
  (6636,1,'admin','/admin/rassilkaSend/','2013-10-12 12:21:31','127.0.0.1',NULL,NULL),
  (6637,1,'admin','/admin/sendRassilka/1/','2013-10-12 12:21:37','127.0.0.1',NULL,NULL),
  (6638,1,'admin','/admin/rassilkaSend/','2013-10-12 12:38:32','127.0.0.1',NULL,NULL),
  (6639,1,'admin','/admin/formShowRassilka/','2013-10-12 12:38:37','127.0.0.1',NULL,NULL),
  (6640,1,'admin','/admin/formShowRassilka/','2013-10-12 12:39:02','127.0.0.1',NULL,NULL),
  (6641,1,'admin','/admin/newsSendUser/','2013-10-13 02:26:20','127.0.0.1',NULL,NULL),
  (6642,1,'admin','/admin/showPerson/','2013-10-14 19:14:37','127.0.0.1',NULL,NULL),
  (6643,1,'admin','/admin/showPerson/','2013-10-14 19:14:39','127.0.0.1',NULL,NULL),
  (6644,1,'admin','/admin/newsSendUser/','2013-10-14 19:14:42','127.0.0.1',NULL,NULL),
  (6645,1,'admin','/admin/newsSendUser/','2013-10-14 19:14:44','127.0.0.1',NULL,NULL),
  (6646,1,'admin','/admin/showStaticPage/','2013-10-14 20:51:34','127.0.0.1',NULL,NULL),
  (6647,1,'admin','/admin/newsSendUser/','2013-10-14 23:06:33','127.0.0.1',NULL,NULL),
  (6648,1,'admin','/adminajax/setIsVisible/','2013-10-14 23:06:37','127.0.0.1',NULL,'a:5:{s:5:\"table\";s:10:\"user_email\";s:2:\"id\";s:1:\"2\";s:6:\"column\";s:9:\"is_active\";s:6:\"id_rec\";s:13:\"id_user_email\";s:5:\"value\";s:1:\"0\";}'),
  (6649,1,'admin','/admin/newsSendUser/','2013-10-14 23:11:15','127.0.0.1',NULL,NULL),
  (6650,1,'admin','/admin/newsSendUser/','2013-10-14 23:11:29','127.0.0.1',NULL,NULL),
  (6651,1,'admin','/admin/formShowSendUser/','2013-10-14 23:11:32','127.0.0.1',NULL,NULL),
  (6652,1,'admin','/admin/showMenu/','2013-10-14 23:13:48','127.0.0.1',NULL,NULL),
  (6653,1,'admin','/admin/newsSendUser/','2013-10-14 23:19:59','127.0.0.1',NULL,NULL),
  (6654,1,'admin','/admin/formShowSendUser/','2013-10-14 23:20:05','127.0.0.1',NULL,NULL),
  (6655,1,'admin','/admin/newsSendUser/','2013-10-14 23:20:34','127.0.0.1',NULL,NULL),
  (6656,1,'admin','/admin/formShowSendUser/','2013-10-14 23:20:38','127.0.0.1',NULL,NULL),
  (6657,1,'admin','/admin/formShowSendUser/','2013-10-14 23:20:47','127.0.0.1',NULL,NULL),
  (6658,1,'admin','/admin/newsSendUser/','2013-10-14 23:22:25','127.0.0.1',NULL,NULL),
  (6659,1,'admin','/admin/formShowSendUser/','2013-10-14 23:22:40','127.0.0.1',NULL,NULL),
  (6660,1,'admin','/admin/newsSendUser/','2013-10-14 23:22:59','127.0.0.1',NULL,NULL),
  (6661,1,'admin','/admin/formShowSendUser/','2013-10-14 23:23:03','127.0.0.1',NULL,NULL),
  (6662,1,'admin','/admin/newsSendUser/','2013-10-14 23:23:11','127.0.0.1',NULL,NULL),
  (6663,1,'admin','/admin/formShowSendUser/','2013-10-14 23:23:14','127.0.0.1',NULL,NULL),
  (6664,1,'admin','/admin/formShowSendUser/','2013-10-14 23:23:23','127.0.0.1',NULL,NULL),
  (6665,1,'admin','/admin/formShowSendUser/','2013-10-14 23:23:36','127.0.0.1',NULL,NULL),
  (6666,1,'admin','/admin/newsSendUser/','2013-10-14 23:25:13','127.0.0.1',NULL,NULL),
  (6667,1,'admin','/admin/formShowSendUser/','2013-10-14 23:25:17','127.0.0.1',NULL,NULL),
  (6668,1,'admin','/admin/newsSendUser/','2013-10-14 23:25:20','127.0.0.1',NULL,NULL),
  (6669,1,'admin','/admin/formShowSendUser/','2013-10-14 23:25:23','127.0.0.1',NULL,NULL),
  (6670,1,'admin','/admin/formShowSendUser/','2013-10-14 23:25:32','127.0.0.1',NULL,NULL),
  (6671,1,'admin','/admin/formShowSendUser/','2013-10-14 23:25:39','127.0.0.1',NULL,NULL),
  (6672,1,'admin','/admin/formShowSendUser/','2013-10-14 23:25:50','127.0.0.1',NULL,NULL),
  (6673,1,'admin','/admin/newsSendUser/','2013-10-14 23:26:18','127.0.0.1',NULL,NULL),
  (6674,1,'admin','/admin/formShowSendUser/','2013-10-14 23:26:23','127.0.0.1',NULL,NULL),
  (6675,1,'admin','/admin/formShowSendUser/','2013-10-14 23:26:29','127.0.0.1',NULL,NULL),
  (6676,1,'admin','/admin/formShowSendUser/','2013-10-14 23:26:41','127.0.0.1',NULL,NULL),
  (6677,1,'admin','/admin/formShowSendUser/','2013-10-14 23:26:44','127.0.0.1',NULL,NULL),
  (6678,1,'admin','/admin/newsSendUser/','2013-10-14 23:27:34','127.0.0.1',NULL,NULL),
  (6679,1,'admin','/admin/formShowSendUser/','2013-10-14 23:27:39','127.0.0.1',NULL,NULL),
  (6680,1,'admin','/admin/formShowSendUser/','2013-10-14 23:27:49','127.0.0.1',NULL,NULL),
  (6681,1,'admin','/admin/formShowSendUser/','2013-10-14 23:27:52','127.0.0.1',NULL,NULL),
  (6682,1,'admin','/admin/formShowSendUser/','2013-10-14 23:28:10','127.0.0.1',NULL,NULL),
  (6683,1,'admin','/admin/newsSendUser/','2013-10-14 23:28:52','127.0.0.1',NULL,NULL),
  (6684,1,'admin','/admin/formShowSendUser/','2013-10-14 23:28:55','127.0.0.1',NULL,NULL),
  (6685,1,'admin','/admin/newsSendUser/','2013-10-14 23:30:09','127.0.0.1',NULL,NULL),
  (6686,1,'admin','/admin/formShowSendUser/','2013-10-14 23:30:15','127.0.0.1',NULL,NULL),
  (6687,1,'admin','/admin/formShowSendUser/','2013-10-14 23:30:24','127.0.0.1',NULL,NULL),
  (6688,1,'admin','/admin/newsSendUser/','2013-10-14 23:30:51','127.0.0.1',NULL,NULL),
  (6689,1,'admin','/admin/formShowSendUser/','2013-10-14 23:30:56','127.0.0.1',NULL,NULL),
  (6690,1,'admin','/admin/newsSendUser/','2013-10-14 23:31:19','127.0.0.1',NULL,NULL),
  (6691,1,'admin','/admin/formShowSendUser/','2013-10-14 23:31:22','127.0.0.1',NULL,NULL),
  (6692,1,'admin','/admin/formShowSendUser/','2013-10-14 23:31:27','127.0.0.1',NULL,NULL),
  (6693,1,'admin','/admin/newsSendUser/','2013-10-14 23:31:34','127.0.0.1',NULL,NULL),
  (6694,1,'admin','/admin/formShowSendUser/','2013-10-14 23:31:38','127.0.0.1',NULL,NULL),
  (6695,1,'admin','/admin/newsSendUser/','2013-10-14 23:32:03','127.0.0.1',NULL,NULL),
  (6696,1,'admin','/admin/formShowSendUser/','2013-10-14 23:32:07','127.0.0.1',NULL,NULL),
  (6697,1,'admin','/admin/newsSendUser/','2013-10-14 23:32:20','127.0.0.1',NULL,NULL),
  (6698,1,'admin','/admin/formShowSendUser/','2013-10-14 23:32:24','127.0.0.1',NULL,NULL),
  (6699,1,'admin','/admin/newsSendUser/','2013-10-14 23:33:03','127.0.0.1',NULL,NULL),
  (6700,1,'admin','/admin/formShowSendUser/','2013-10-14 23:33:08','127.0.0.1',NULL,NULL),
  (6701,1,'admin','/admin/formShowSendUser/','2013-10-14 23:33:30','127.0.0.1',NULL,NULL),
  (6702,1,'admin','/admin/newsSendUser/','2013-10-14 23:33:43','127.0.0.1',NULL,NULL),
  (6703,1,'admin','/admin/formShowSendUser/','2013-10-14 23:33:48','127.0.0.1',NULL,NULL),
  (6704,1,'admin','/admin/formShowSendUser/','2013-10-14 23:33:54','127.0.0.1',NULL,NULL),
  (6705,1,'admin','/admin/newsSendUser/','2013-10-14 23:34:07','127.0.0.1',NULL,NULL),
  (6706,1,'admin','/admin/formShowSendUser/','2013-10-14 23:34:11','127.0.0.1',NULL,NULL),
  (6707,1,'admin','/admin/','2013-10-14 23:35:09','127.0.0.1',NULL,NULL),
  (6708,1,'admin','/admin/newsSendUser/','2013-10-14 23:35:18','127.0.0.1',NULL,NULL),
  (6709,1,'admin','/admin/formShowSendUser/','2013-10-14 23:35:22','127.0.0.1',NULL,NULL),
  (6710,1,'admin','/admin/newsSendUser/','2013-10-14 23:35:25','127.0.0.1',NULL,NULL),
  (6711,1,'admin','/admin/newsSendUser/','2013-10-14 23:35:28','127.0.0.1',NULL,NULL),
  (6712,1,'admin','/admin/newsSendUser/','2013-10-14 23:35:29','127.0.0.1',NULL,NULL),
  (6713,1,'admin','/admin/formShowSendUser/','2013-10-14 23:35:32','127.0.0.1',NULL,NULL),
  (6714,1,'admin','/admin/formShowSendUser/','2013-10-14 23:35:35','127.0.0.1',NULL,NULL),
  (6715,1,'admin','/admin/formShowSendUser/','2013-10-14 23:35:43','127.0.0.1',NULL,NULL),
  (6716,1,'admin','/admin/formShowSendUser/','2013-10-14 23:35:48','127.0.0.1',NULL,NULL),
  (6717,1,'admin','/admin/formShowSendUser/','2013-10-14 23:35:51','127.0.0.1',NULL,NULL),
  (6718,1,'admin','/admin/formShowSendUser/','2013-10-14 23:35:59','127.0.0.1',NULL,NULL),
  (6719,1,'admin','/admin/formShowSendUser/','2013-10-14 23:36:04','127.0.0.1',NULL,NULL),
  (6720,1,'admin','/admin/newsSendUser/','2013-10-14 23:36:09','127.0.0.1',NULL,NULL),
  (6721,1,'admin','/admin/newsSendUser/','2013-10-14 23:36:11','127.0.0.1',NULL,NULL),
  (6722,1,'admin','/admin/newsSendUser/','2013-10-14 23:36:13','127.0.0.1',NULL,NULL),
  (6723,1,'admin','/admin/newsSendUser/','2013-10-14 23:36:14','127.0.0.1',NULL,NULL),
  (6724,1,'admin','/admin/formShowSendUser/','2013-10-14 23:36:19','127.0.0.1',NULL,NULL),
  (6725,1,'admin','/admin/newsSendUser/','2013-10-14 23:36:37','127.0.0.1',NULL,NULL),
  (6726,1,'admin','/admin/formShowSendUser/','2013-10-14 23:36:41','127.0.0.1',NULL,NULL),
  (6727,1,'admin','/admin/newsSendUser/','2013-10-14 23:37:35','127.0.0.1',NULL,NULL),
  (6728,1,'admin','/admin/formShowSendUser/','2013-10-14 23:37:39','127.0.0.1',NULL,NULL),
  (6729,1,'admin','/admin/newsSendUser/','2013-10-14 23:38:33','127.0.0.1',NULL,NULL),
  (6730,1,'admin','/admin/formShowSendUser/','2013-10-14 23:38:37','127.0.0.1',NULL,NULL),
  (6731,1,'admin','/admin/newsSendUser/','2013-10-14 23:39:01','127.0.0.1',NULL,NULL),
  (6732,1,'admin','/admin/formShowSendUser/','2013-10-14 23:39:05','127.0.0.1',NULL,NULL),
  (6733,1,'admin','/admin/formShowSendUser/','2013-10-14 23:39:08','127.0.0.1',NULL,NULL),
  (6734,1,'admin','/admin/formShowSendUser/','2013-10-14 23:39:15','127.0.0.1',NULL,NULL),
  (6735,1,'admin','/admin/newsSendUser/','2013-10-14 23:39:24','127.0.0.1',NULL,NULL),
  (6736,1,'admin','/admin/formShowSendUser/','2013-10-14 23:39:27','127.0.0.1',NULL,NULL),
  (6737,1,'admin','/admin/formShowSendUser/','2013-10-14 23:39:30','127.0.0.1',NULL,NULL),
  (6738,1,'admin','/admin/updateSendUser/','2013-10-14 23:39:35','127.0.0.1','a:6:{s:13:\"id_user_email\";s:1:\"2\";s:4:\"name\";s:14:\"Алексей\";s:5:\"email\";s:14:\"qwerty@mail.ry\";s:9:\"is_active\";s:1:\"1\";s:13:\"is_black_list\";s:1:\"0\";s:3:\"sex\";s:1:\"1\";}','a:6:{s:13:\"id_user_email\";s:1:\"2\";s:4:\"name\";s:14:\"Алексей\";s:5:\"email\";s:14:\"qwerty@mail.ry\";s:9:\"is_active\";s:1:\"1\";s:13:\"is_black_list\";s:1:\"0\";s:3:\"sex\";s:1:\"1\";}'),
  (6739,1,'admin','/admin/newsSendUser/','2013-10-14 23:39:35','127.0.0.1',NULL,NULL),
  (6740,1,'admin','/admin/formShowSendUser/','2013-10-14 23:39:41','127.0.0.1',NULL,NULL),
  (6741,1,'admin','/admin/formShowSendUser/','2013-10-14 23:39:44','127.0.0.1',NULL,NULL),
  (6742,1,'admin','/admin/formShowSendUser/','2013-10-14 23:41:22','127.0.0.1',NULL,NULL),
  (6743,1,'admin','/admin/rassilkaSend/','2013-10-15 00:37:23','127.0.0.1',NULL,NULL),
  (6744,1,'admin','/admin/updateSettingRassilka/','2013-10-15 00:37:27','127.0.0.1',NULL,NULL),
  (6745,1,'admin','/admin/newsSendUser/','2013-10-15 11:27:32','127.0.0.1',NULL,NULL),
  (6746,1,'admin','/admin/showNews/','2013-10-15 12:53:49','127.0.0.1',NULL,NULL),
  (6747,1,'admin','/admin/showNews/','2013-10-15 12:55:28','127.0.0.1',NULL,NULL),
  (6748,1,'admin','/admin/showNews/','2013-10-15 12:55:43','127.0.0.1',NULL,NULL),
  (6749,1,'admin','/admin/showNews/','2013-10-15 12:55:57','127.0.0.1',NULL,NULL),
  (6750,1,'admin','/admin/showNews/','2013-10-15 12:56:06','127.0.0.1',NULL,NULL),
  (6751,1,'admin','/admin/showNews/','2013-10-15 12:56:19','127.0.0.1',NULL,NULL),
  (6752,1,'admin','/admin/showNews/','2013-10-15 12:56:53','127.0.0.1',NULL,NULL),
  (6753,1,'admin','/admin/showNews/','2013-10-15 12:57:06','127.0.0.1',NULL,NULL),
  (6754,1,'admin','/admin/showNews/','2013-10-15 12:57:28','127.0.0.1',NULL,NULL),
  (6755,1,'admin','/admin/showNews/','2013-10-15 12:58:09','127.0.0.1',NULL,NULL),
  (6756,1,'admin','/admin/showNews/','2013-10-15 12:58:41','127.0.0.1',NULL,NULL),
  (6757,1,'admin','/admin/showNews/','2013-10-15 13:00:41','127.0.0.1',NULL,NULL),
  (6758,1,'admin','/admin/showNews/','2013-10-15 13:01:53','127.0.0.1',NULL,NULL),
  (6759,1,'admin','/admin/showNews/','2013-10-15 13:02:21','127.0.0.1',NULL,NULL),
  (6760,1,'admin','/admin/showNews/','2013-10-15 13:02:51','127.0.0.1',NULL,NULL),
  (6761,1,'admin','/admin/showNews/','2013-10-15 13:03:47','127.0.0.1',NULL,NULL),
  (6762,1,'admin','/admin/showNews/','2013-10-15 13:04:55','127.0.0.1',NULL,NULL),
  (6763,1,'admin','/admin/showNews/','2013-10-15 13:05:12','127.0.0.1',NULL,NULL),
  (6764,1,'admin','/admin/showNews/','2013-10-15 13:05:42','127.0.0.1',NULL,NULL),
  (6765,1,'admin','/admin/showNews/','2013-10-15 13:09:24','127.0.0.1',NULL,NULL),
  (6766,1,'admin','/admin/showNews/','2013-10-15 13:13:48','127.0.0.1',NULL,NULL),
  (6767,1,'admin','/admin/formShowNews/','2013-10-15 13:13:54','127.0.0.1',NULL,NULL),
  (6768,1,'admin','/admin/showNews/','2013-10-15 13:14:34','127.0.0.1',NULL,NULL),
  (6769,1,'admin','/admin/formShowNews/','2013-10-15 13:14:38','127.0.0.1',NULL,NULL),
  (6770,1,'admin','/admin/showNews/','2013-10-15 13:14:51','127.0.0.1',NULL,NULL),
  (6771,1,'admin','/admin/formShowNews/','2013-10-15 13:14:56','127.0.0.1',NULL,NULL),
  (6772,1,'admin','/admin/showLib/book/','2013-10-15 13:19:20','127.0.0.1',NULL,NULL),
  (6773,1,'admin','/admin/showPredPrint/','2013-10-15 13:19:37','127.0.0.1',NULL,NULL),
  (6774,1,'admin','/admin/showLib/article/','2013-10-15 13:19:50','127.0.0.1',NULL,NULL),
  (6775,1,'admin','/admin/showPredPrint/','2013-10-15 13:20:58','127.0.0.1',NULL,NULL),
  (6776,1,'admin','/admin/showNews/','2013-10-15 13:21:40','127.0.0.1',NULL,NULL),
  (6777,1,'admin','/admin/showLib/article/','2013-10-15 13:23:02','127.0.0.1',NULL,NULL),
  (6778,1,'admin','/admin/showLib/article/','2013-10-15 13:24:00','127.0.0.1',NULL,NULL),
  (6779,1,'admin','/admin/showLib/article/','2013-10-15 13:24:26','127.0.0.1',NULL,NULL),
  (6780,1,'admin','/admin/showLib/thesis/','2013-10-15 13:24:47','127.0.0.1',NULL,NULL),
  (6781,1,'admin','/admin/showLib/thesis/','2013-10-15 13:28:07','127.0.0.1',NULL,NULL),
  (6782,1,'admin','/admin/showLib/thesis/','2013-10-15 13:29:00','127.0.0.1',NULL,NULL),
  (6783,1,'admin','/admin/showLib/thesis/','2013-10-15 13:29:42','127.0.0.1',NULL,NULL),
  (6784,1,'admin','/admin/showLib/thesis/','2013-10-15 13:30:22','127.0.0.1',NULL,NULL),
  (6785,1,'admin','/admin/showLib/thesis/','2013-10-15 13:32:39','127.0.0.1',NULL,NULL),
  (6786,1,'admin','/admin/showLib/thesis/','2013-10-15 13:33:18','127.0.0.1',NULL,NULL),
  (6787,1,'admin','/admin/showLib/thesis/','2013-10-15 13:34:42','127.0.0.1',NULL,NULL),
  (6788,1,'admin','/admin/showLib/thesis/','2013-10-15 13:35:08','127.0.0.1',NULL,NULL),
  (6789,1,'admin','/admin/showLib/thesis/','2013-10-15 13:36:31','127.0.0.1',NULL,NULL),
  (6790,1,'admin','/admin/showLib/thesis/','2013-10-15 13:36:51','127.0.0.1',NULL,NULL),
  (6791,1,'admin','/admin/showLib/thesis/','2013-10-15 13:37:38','127.0.0.1',NULL,NULL),
  (6792,1,'admin','/admin/showLib/thesis/','2013-10-15 13:37:44','127.0.0.1',NULL,NULL),
  (6793,1,'admin','/admin/showLib/thesis/','2013-10-15 13:38:13','127.0.0.1',NULL,NULL),
  (6794,1,'admin','/admin/showLib/thesis/','2013-10-15 13:38:28','127.0.0.1',NULL,NULL),
  (6795,1,'admin','/admin/showSp/education/','2013-10-15 15:13:57','127.0.0.1',NULL,NULL),
  (6796,1,'admin','/admin/formShowSp/education','2013-10-15 15:14:05','127.0.0.1',NULL,NULL),
  (6797,1,'admin','/admin/showSp/organasation/','2013-10-15 15:14:12','127.0.0.1',NULL,NULL),
  (6798,1,'admin','/admin/formShowSp/organasation','2013-10-15 15:14:16','127.0.0.1',NULL,NULL),
  (6799,1,'admin','/admin/showSp/organasation/','2013-10-15 15:15:58','127.0.0.1',NULL,NULL),
  (6800,1,'admin','/admin/formShowSp/organasation','2013-10-15 15:16:04','127.0.0.1',NULL,NULL),
  (6801,1,'admin','/admin/showSp/theme_news/','2013-10-15 15:16:25','127.0.0.1',NULL,NULL),
  (6802,1,'admin','/admin/formShowSp/theme_news','2013-10-15 15:16:36','127.0.0.1',NULL,NULL),
  (6803,1,'admin','/admin/showSp/theme_news/','2013-10-15 15:17:28','127.0.0.1',NULL,NULL),
  (6804,1,'admin','/admin/formShowSp/theme_news','2013-10-15 15:17:37','127.0.0.1',NULL,NULL),
  (6805,1,'admin','/admin/showSp/vid_news/','2013-10-15 15:17:43','127.0.0.1',NULL,NULL),
  (6806,1,'admin','/admin/formShowSp/vid_news','2013-10-15 15:17:47','127.0.0.1',NULL,NULL),
  (6807,1,'admin','/admin/showSp/education/','2013-10-15 15:17:55','127.0.0.1',NULL,NULL),
  (6808,1,'admin','/admin/formShowSp/education','2013-10-15 15:17:59','127.0.0.1',NULL,NULL),
  (6809,1,'admin','/admin/showMenu/','2013-10-15 15:18:04','127.0.0.1',NULL,NULL),
  (6810,1,'admin','/admin/formShowMenu/','2013-10-15 15:18:09','127.0.0.1',NULL,NULL),
  (6811,1,'admin','/admin/showMenu/','2013-10-15 15:18:15','127.0.0.1',NULL,NULL),
  (6812,1,'admin','/admin/formShowMenu/','2013-10-15 15:18:32','127.0.0.1',NULL,NULL),
  (6813,1,'admin','/admin/formShowMenu/','2013-10-15 15:18:46','127.0.0.1',NULL,NULL),
  (6814,1,'admin','/admin/showMenu/','2013-10-15 15:19:25','127.0.0.1',NULL,NULL),
  (6815,1,'admin','/admin/formShowMenu/','2013-10-15 15:19:29','127.0.0.1',NULL,NULL),
  (6816,1,'admin','/admin/showMenu/','2013-10-15 15:19:38','127.0.0.1',NULL,NULL),
  (6817,1,'admin','/admin/formShowMenu/','2013-10-15 15:19:42','127.0.0.1',NULL,NULL),
  (6818,1,'admin','/admin/showLib/book/','2013-10-15 15:19:55','127.0.0.1',NULL,NULL),
  (6819,1,'admin','/admin/formShowLib/book/','2013-10-15 15:19:59','127.0.0.1',NULL,NULL),
  (6820,1,'admin','/admin/showLib/book/','2013-10-15 15:21:45','127.0.0.1',NULL,NULL),
  (6821,1,'admin','/admin/formShowLib/book/','2013-10-15 15:21:50','127.0.0.1',NULL,NULL),
  (6822,1,'admin','/admin/showLib/book/','2013-10-15 15:22:27','127.0.0.1',NULL,NULL),
  (6823,1,'admin','/admin/formShowLib/book/','2013-10-15 15:22:32','127.0.0.1',NULL,NULL),
  (6824,1,'admin','/admin/showLib/book/','2013-10-15 15:23:28','127.0.0.1',NULL,NULL),
  (6825,1,'admin','/admin/formShowLib/book/','2013-10-15 15:23:33','127.0.0.1',NULL,NULL),
  (6826,1,'admin','/admin/showLib/book/','2013-10-15 15:25:29','127.0.0.1',NULL,NULL),
  (6827,1,'admin','/admin/formShowLib/book/','2013-10-15 15:25:33','127.0.0.1',NULL,NULL),
  (6828,1,'admin','/admin/showLib/article/','2013-10-15 15:25:48','127.0.0.1',NULL,NULL),
  (6829,1,'admin','/admin/formShowLib/article/','2013-10-15 15:25:58','127.0.0.1',NULL,NULL),
  (6830,1,'admin','/admin/showLib/book/','2013-10-15 15:26:38','127.0.0.1',NULL,NULL),
  (6831,1,'admin','/admin/showLib/metabook/','2013-10-15 15:26:41','127.0.0.1',NULL,NULL),
  (6832,1,'admin','/admin/formShowLib/metabook/','2013-10-15 15:26:45','127.0.0.1',NULL,NULL),
  (6833,1,'admin','/admin/showLib/metabook/','2013-10-15 15:27:48','127.0.0.1',NULL,NULL),
  (6834,1,'admin','/admin/formShowLib/metabook/','2013-10-15 15:27:52','127.0.0.1',NULL,NULL),
  (6835,1,'admin','/admin/showLib/article/','2013-10-15 15:28:27','127.0.0.1',NULL,NULL),
  (6836,1,'admin','/admin/formShowLib/article/','2013-10-15 15:28:31','127.0.0.1',NULL,NULL),
  (6837,1,'admin','/admin/showLib/article/','2013-10-15 15:30:00','127.0.0.1',NULL,NULL),
  (6838,1,'admin','/admin/formShowLib/article/','2013-10-15 15:30:05','127.0.0.1',NULL,NULL),
  (6839,1,'admin','/admin/showLib/filmstrip/','2013-10-15 15:30:37','127.0.0.1',NULL,NULL),
  (6840,1,'admin','/admin/formShowLib/filmstrip/','2013-10-15 15:30:40','127.0.0.1',NULL,NULL),
  (6841,1,'admin','/admin/showLib/filmstrip/','2013-10-15 15:31:54','127.0.0.1',NULL,NULL),
  (6842,1,'admin','/admin/formShowLib/filmstrip/','2013-10-15 15:31:59','127.0.0.1',NULL,NULL),
  (6843,1,'admin','/admin/showLib/thesis/','2013-10-15 15:32:12','127.0.0.1',NULL,NULL),
  (6844,1,'admin','/admin/formShowLib/thesis/','2013-10-15 15:32:17','127.0.0.1',NULL,NULL),
  (6845,1,'admin','/admin/showLib/thesis/','2013-10-15 15:33:07','127.0.0.1',NULL,NULL),
  (6846,1,'admin','/admin/formShowLib/thesis/','2013-10-15 15:33:11','127.0.0.1',NULL,NULL),
  (6847,1,'admin','/admin/showNews/','2013-10-15 15:33:58','127.0.0.1',NULL,NULL),
  (6848,1,'admin','/admin/formShowNews/','2013-10-15 15:34:07','127.0.0.1',NULL,NULL),
  (6849,1,'admin','/admin/showNews/','2013-10-15 15:36:14','127.0.0.1',NULL,NULL),
  (6850,1,'admin','/admin/formShowNews/','2013-10-15 15:36:19','127.0.0.1',NULL,NULL),
  (6851,1,'admin','/admin/showNews/','2013-10-15 15:37:20','127.0.0.1',NULL,NULL),
  (6852,1,'admin','/admin/formShowNews/','2013-10-15 15:37:37','127.0.0.1',NULL,NULL),
  (6853,1,'admin','/admin/showPerson/','2013-10-15 15:37:47','127.0.0.1',NULL,NULL),
  (6854,1,'admin','/admin/formShowPerson/','2013-10-15 15:37:51','127.0.0.1',NULL,NULL),
  (6855,1,'admin','/admin/showPerson/','2013-10-15 15:38:48','127.0.0.1',NULL,NULL),
  (6856,1,'admin','/admin/formShowPerson/','2013-10-15 15:38:53','127.0.0.1',NULL,NULL),
  (6857,1,'admin','/admin/showStaticPage/','2013-10-15 15:39:06','127.0.0.1',NULL,NULL),
  (6858,1,'admin','/admin/formShowStaticPage/','2013-10-15 15:39:12','127.0.0.1',NULL,NULL),
  (6859,1,'admin','/admin/showStaticPage/','2013-10-15 15:40:08','127.0.0.1',NULL,NULL),
  (6860,1,'admin','/admin/formShowStaticPage/','2013-10-15 15:40:13','127.0.0.1',NULL,NULL),
  (6861,1,'admin','/admin/formShowStaticPage/','2013-10-15 15:43:18','127.0.0.1',NULL,NULL),
  (6862,1,'admin','/admin/showEvents/1/','2013-10-15 15:44:24','127.0.0.1',NULL,NULL),
  (6863,1,'admin','/admin/formShowEvent/1/','2013-10-15 15:44:29','127.0.0.1',NULL,NULL),
  (6864,1,'admin','/admin/showEvents/1/','2013-10-15 15:47:15','127.0.0.1',NULL,NULL),
  (6865,1,'admin','/admin/formShowEvent/1/','2013-10-15 15:47:20','127.0.0.1',NULL,NULL),
  (6866,1,'admin','/admin/showEvents/1/','2013-10-15 15:47:34','127.0.0.1',NULL,NULL),
  (6867,1,'admin','/admin/formShowEvent/1/','2013-10-15 15:47:38','127.0.0.1',NULL,NULL),
  (6868,1,'admin','/admin/showEvents/1/','2013-10-15 15:47:44','127.0.0.1',NULL,NULL),
  (6869,1,'admin','/admin/formShowEvent/1/','2013-10-15 15:47:49','127.0.0.1',NULL,NULL),
  (6870,1,'admin','/admin/showEvents/1/','2013-10-15 15:48:00','127.0.0.1',NULL,NULL),
  (6871,1,'admin','/admin/formShowEvent/1/','2013-10-15 15:48:05','127.0.0.1',NULL,NULL),
  (6872,1,'admin','/admin/formShowNews/','2013-10-15 15:53:54','127.0.0.1',NULL,NULL),
  (6873,1,'admin','/admin/showNews/','2013-10-15 15:53:58','127.0.0.1',NULL,NULL),
  (6874,1,'admin','/admin/formShowNews/','2013-10-15 15:54:03','127.0.0.1',NULL,NULL),
  (6875,1,'admin','/admin/formShowNews/','2013-10-15 15:54:17','127.0.0.1',NULL,NULL),
  (6876,1,'admin','/admin/formShowNews/','2013-10-15 15:54:29','127.0.0.1',NULL,NULL),
  (6877,1,'admin','/admin/formShowNews/','2013-10-15 15:54:38','127.0.0.1',NULL,NULL),
  (6878,1,'admin','/admin/formShowNews/','2013-10-15 15:55:08','127.0.0.1',NULL,NULL),
  (6879,1,'admin','/admin/formShowNews/','2013-10-15 15:55:18','127.0.0.1',NULL,NULL),
  (6880,1,'admin','/admin/formShowNews/','2013-10-15 15:55:27','127.0.0.1',NULL,NULL),
  (6881,1,'admin','/admin/formShowNews/','2013-10-15 15:55:44','127.0.0.1',NULL,NULL),
  (6882,1,'admin','/admin/formShowNews/','2013-10-15 15:55:50','127.0.0.1',NULL,NULL),
  (6883,1,'admin','/admin/showEvents/1/','2013-10-15 15:56:05','127.0.0.1',NULL,NULL),
  (6884,1,'admin','/admin/showEvents/2/','2013-10-15 15:56:08','127.0.0.1',NULL,NULL),
  (6885,1,'admin','/admin/feedback/','2013-10-15 15:57:53','127.0.0.1',NULL,NULL),
  (6886,1,'admin','/admin/formShowFeedback/','2013-10-15 15:57:56','127.0.0.1',NULL,NULL),
  (6887,1,'admin','/admin/formShowFeedback/','2013-10-15 16:00:34','127.0.0.1',NULL,NULL),
  (6888,1,'admin','/admin/formShowFeedback/','2013-10-15 16:00:40','127.0.0.1',NULL,NULL),
  (6889,1,'admin','/admin/feedback/','2013-10-15 16:00:41','127.0.0.1',NULL,NULL),
  (6890,1,'admin','/admin/formShowFeedback/','2013-10-15 16:00:46','127.0.0.1',NULL,NULL),
  (6891,1,'admin','/admin/feedback/','2013-10-15 16:01:18','127.0.0.1',NULL,NULL),
  (6892,1,'admin','/admin/formShowFeedback/','2013-10-15 16:01:22','127.0.0.1',NULL,NULL),
  (6893,1,'admin','/admin/feedback/','2013-10-15 16:01:30','127.0.0.1',NULL,NULL),
  (6894,1,'admin','/admin/formShowFeedback/','2013-10-15 16:01:34','127.0.0.1',NULL,NULL),
  (6895,1,'admin','/admin/feedback/','2013-10-15 16:01:45','127.0.0.1',NULL,NULL),
  (6896,1,'admin','/admin/formShowFeedback/','2013-10-15 16:01:52','127.0.0.1',NULL,NULL),
  (6897,1,'admin','/admin/formShowFeedback/','2013-10-15 16:01:59','127.0.0.1',NULL,NULL),
  (6898,1,'admin','/admin/rassilkaSend/','2013-10-15 16:02:07','127.0.0.1',NULL,NULL),
  (6899,1,'admin','/admin/formShowRassilka/','2013-10-15 16:02:11','127.0.0.1',NULL,NULL),
  (6900,1,'admin','/admin/rassilkaSend/','2013-10-15 16:02:59','127.0.0.1',NULL,NULL),
  (6901,1,'admin','/admin/formShowRassilka/','2013-10-15 16:03:04','127.0.0.1',NULL,NULL),
  (6902,1,'admin','/admin/rassilkaSend/','2013-10-15 16:03:32','127.0.0.1',NULL,NULL),
  (6903,1,'admin','/admin/formShowRassilka/','2013-10-15 16:03:36','127.0.0.1',NULL,NULL),
  (6904,1,'admin','/admin/rassilkaSend/','2013-10-15 16:07:47','127.0.0.1',NULL,NULL),
  (6905,1,'admin','/admin/formShowRassilka/','2013-10-15 16:07:54','127.0.0.1',NULL,NULL),
  (6906,1,'admin','/admin/rassilkaSend/','2013-10-15 16:12:25','127.0.0.1',NULL,NULL),
  (6907,1,'admin','/admin/formShowRassilka/','2013-10-15 16:12:30','127.0.0.1',NULL,NULL),
  (6908,1,'admin','/admin/rassilkaSend/','2013-10-15 16:14:18','127.0.0.1',NULL,NULL),
  (6909,1,'admin','/admin/formShowRassilka/','2013-10-15 16:14:25','127.0.0.1',NULL,NULL),
  (6910,1,'admin','/admin/rassilkaSend/','2013-10-15 16:21:01','127.0.0.1',NULL,NULL),
  (6911,1,'admin','/admin/formShowRassilka/','2013-10-15 16:21:07','127.0.0.1',NULL,NULL),
  (6912,1,'admin','/admin/rassilkaSend/','2013-10-15 16:21:14','127.0.0.1',NULL,NULL),
  (6913,1,'admin','/admin/formShowRassilka/','2013-10-15 16:21:21','127.0.0.1',NULL,NULL),
  (6914,1,'admin','/admin/rassilkaSend/','2013-10-15 16:21:43','127.0.0.1',NULL,NULL),
  (6915,1,'admin','/admin/formShowRassilka/','2013-10-15 16:21:47','127.0.0.1',NULL,NULL),
  (6916,1,'admin','/admin/rassilkaSend/','2013-10-15 16:22:03','127.0.0.1',NULL,NULL),
  (6917,1,'admin','/admin/formShowRassilka/','2013-10-15 16:22:07','127.0.0.1',NULL,NULL),
  (6918,1,'admin','/admin/updateSettingRassilka/','2013-10-15 16:22:15','127.0.0.1',NULL,NULL),
  (6919,1,'admin','/admin/newsSendUser/','2013-10-15 16:22:24','127.0.0.1',NULL,NULL),
  (6920,1,'admin','/admin/formShowSendUser/','2013-10-15 16:22:27','127.0.0.1',NULL,NULL),
  (6921,1,'admin','/admin/rss/','2013-10-15 16:22:35','127.0.0.1',NULL,NULL),
  (6922,1,'admin','/admin/formShowRSS/','2013-10-15 16:22:41','127.0.0.1',NULL,NULL),
  (6923,1,'admin','/admin/rss/','2013-10-15 16:23:29','127.0.0.1',NULL,NULL),
  (6924,1,'admin','/admin/formShowRSS/','2013-10-15 16:23:35','127.0.0.1',NULL,NULL),
  (6925,1,'admin','/admin/rss/','2013-10-15 16:24:39','127.0.0.1',NULL,NULL),
  (6926,1,'admin','/admin/formShowRSS/','2013-10-15 16:24:46','127.0.0.1',NULL,NULL),
  (6927,1,'admin','/admin/rss/','2013-10-15 16:25:13','127.0.0.1',NULL,NULL),
  (6928,1,'admin','/admin/formShowRSS/','2013-10-15 16:25:18','127.0.0.1',NULL,NULL),
  (6929,1,'admin','/admin/rss/','2013-10-15 16:25:25','127.0.0.1',NULL,NULL),
  (6930,1,'admin','/admin/formShowRSS/','2013-10-15 16:25:29','127.0.0.1',NULL,NULL),
  (6931,1,'admin','/admin/rss/','2013-10-15 16:25:40','127.0.0.1',NULL,NULL),
  (6932,1,'admin','/admin/formShowRSS/','2013-10-15 16:25:48','127.0.0.1',NULL,NULL),
  (6933,1,'admin','/admin/rss/','2013-10-15 16:26:21','127.0.0.1',NULL,NULL),
  (6934,1,'admin','/admin/formShowRSS/','2013-10-15 16:26:27','127.0.0.1',NULL,NULL),
  (6935,1,'admin','/admin/showArhivs/','2013-10-15 16:26:50','127.0.0.1',NULL,NULL),
  (6936,1,'admin','/admin/showUser/','2013-10-15 16:27:18','127.0.0.1',NULL,NULL),
  (6937,1,'admin','/admin/formShowUser/','2013-10-15 16:27:23','127.0.0.1',NULL,NULL),
  (6938,1,'admin','/admin/formShowUser/','2013-10-15 16:27:34','127.0.0.1',NULL,NULL),
  (6939,1,'admin','/admin/formShowUser/','2013-10-15 16:27:44','127.0.0.1',NULL,NULL),
  (6940,1,'admin','/admin/showUser/','2013-10-15 16:27:46','127.0.0.1',NULL,NULL),
  (6941,1,'admin','/admin/formShowUser/','2013-10-15 16:27:55','127.0.0.1',NULL,NULL),
  (6942,1,'admin','/admin/showRoles/','2013-10-15 16:28:00','127.0.0.1',NULL,NULL),
  (6943,1,'admin','/admin/formShowRoles/','2013-10-15 16:28:06','127.0.0.1',NULL,NULL),
  (6944,1,'admin','/admin/formShowRoles/','2013-10-15 16:28:09','127.0.0.1',NULL,NULL),
  (6945,1,'admin','/admin/formShowRoles/','2013-10-15 16:28:14','127.0.0.1',NULL,NULL),
  (6946,1,'admin','/admin/showFormImport/','2013-10-15 16:28:20','127.0.0.1',NULL,NULL),
  (6947,1,'admin','/admin/configSystem/','2013-10-15 16:28:30','127.0.0.1',NULL,NULL),
  (6948,1,'admin','/admin/configServer/','2013-10-15 16:28:37','127.0.0.1',NULL,NULL),
  (6949,1,'admin','/admin/redactSlovar/','2013-10-15 16:28:59','127.0.0.1',NULL,NULL),
  (6950,1,'admin','/admin/formShowLangVariables/','2013-10-15 16:29:04','127.0.0.1',NULL,NULL),
  (6951,1,'admin','/admin/formShowLangVariables/','2013-10-15 16:29:09','127.0.0.1',NULL,NULL),
  (6952,1,'admin','/admin/systemGurnal/','2013-10-15 16:29:14','127.0.0.1',NULL,NULL),
  (6953,1,'admin','/admin/formShowSystemGurnal/','2013-10-15 16:29:22','127.0.0.1',NULL,NULL),
  (6954,1,'admin','/admin/systemGurnal/','2013-10-15 16:30:16','127.0.0.1',NULL,NULL),
  (6955,1,'admin','/admin/formShowSystemGurnal/','2013-10-15 16:30:25','127.0.0.1',NULL,NULL),
  (6956,1,'admin','/admin/systemGurnal/','2013-10-15 16:30:37','127.0.0.1',NULL,NULL),
  (6957,1,'admin','/admin/formShowSystemGurnal/','2013-10-15 16:30:43','127.0.0.1',NULL,NULL),
  (6958,1,'admin','/admin/systemGurnal/','2013-10-15 16:31:00','127.0.0.1',NULL,NULL),
  (6959,1,'admin','/admin/formShowSystemGurnal/','2013-10-15 16:31:05','127.0.0.1',NULL,NULL),
  (6960,1,'admin','/admin/systemGurnal/','2013-10-15 16:31:12','127.0.0.1',NULL,NULL),
  (6961,1,'admin','/admin/formShowSystemGurnal/','2013-10-15 16:31:16','127.0.0.1',NULL,NULL),
  (6962,1,'admin','/admin/','2013-10-15 16:31:31','127.0.0.1',NULL,NULL),
  (6963,1,'admin','/admin/showPredPrint/','2013-10-15 16:31:45','127.0.0.1',NULL,NULL),
  (6964,1,'admin','/admin/formShowPredPrintAdmin/','2013-10-15 16:31:50','127.0.0.1',NULL,NULL),
  (6965,1,'admin','/admin/showPredPrint/','2013-10-15 16:32:18','127.0.0.1',NULL,NULL),
  (6966,1,'admin','/admin/formShowPredPrintAdmin/','2013-10-15 16:32:22','127.0.0.1',NULL,NULL),
  (6967,1,'admin','/admin/showPredPrint/','2013-10-15 16:32:32','127.0.0.1',NULL,NULL),
  (6968,1,'admin','/admin/formShowPredPrintAdmin/','2013-10-15 16:32:36','127.0.0.1',NULL,NULL),
  (6969,1,'admin','/admin/formShowPredPrintAdmin/','2013-10-15 16:32:43','127.0.0.1',NULL,NULL),
  (6970,1,'admin','/admin/formShowPredPrintAdmin/','2013-10-15 16:32:49','127.0.0.1',NULL,NULL),
  (6971,1,'admin','/admin/formShowPredPrintAdmin/','2013-10-15 16:32:53','127.0.0.1',NULL,NULL),
  (6972,1,'admin','/admin/showPredPrint/','2013-10-15 16:33:03','127.0.0.1',NULL,NULL),
  (6973,1,'admin','/admin/formShowPredPrintAdmin/','2013-10-15 16:33:07','127.0.0.1',NULL,NULL),
  (6974,1,'admin','/admin/showPredPrint/','2013-10-15 18:11:34','127.0.0.1',NULL,NULL),
  (6975,1,'admin','/admin/showNews/','2013-10-15 18:11:38','127.0.0.1',NULL,NULL),
  (6976,1,'admin','/admin/showNews/','2013-10-15 18:12:45','127.0.0.1',NULL,NULL),
  (6977,1,'admin','/admin/showNews/','2013-10-15 18:13:43','127.0.0.1',NULL,NULL),
  (6978,1,'admin','/admin/showNews/','2013-10-15 18:15:26','127.0.0.1',NULL,NULL),
  (6979,1,'admin','/admin/showNews/','2013-10-15 18:17:38','127.0.0.1',NULL,NULL),
  (6980,1,'admin','/admin/showNews/','2013-10-15 18:18:32','127.0.0.1',NULL,NULL),
  (6981,1,'admin','/admin/showNews/','2013-10-15 18:21:34','127.0.0.1',NULL,NULL),
  (6982,1,'admin','/admin/showNews/','2013-10-15 18:21:58','127.0.0.1',NULL,NULL),
  (6983,1,'admin','/admin/showNews/','2013-10-15 18:22:11','127.0.0.1',NULL,NULL),
  (6984,1,'admin','/admin/showNews/','2013-10-15 18:22:17','127.0.0.1',NULL,NULL),
  (6985,1,'admin','/admin/showNews/','2013-10-15 18:22:31','127.0.0.1',NULL,NULL),
  (6986,1,'admin','/admin/showNews/','2013-10-15 18:22:46','127.0.0.1',NULL,NULL),
  (6987,1,'admin','/admin/showNews/','2013-10-15 18:22:58','127.0.0.1',NULL,NULL),
  (6988,1,'admin','/admin/showNews/','2013-10-15 18:24:22','127.0.0.1',NULL,NULL),
  (6989,1,'admin','/admin/showNews/','2013-10-15 20:50:10','127.0.0.1',NULL,NULL),
  (6990,1,'admin','/admin/showNews/','2013-10-15 20:54:36','127.0.0.1',NULL,NULL),
  (6991,1,'admin','/admin/formShowNews/','2013-10-15 20:54:49','127.0.0.1',NULL,NULL),
  (6992,1,'admin','/admin/showNews/','2013-10-15 20:55:08','127.0.0.1',NULL,NULL),
  (6993,1,'admin','/admin/formShowNews/','2013-10-15 20:55:13','127.0.0.1',NULL,NULL),
  (6994,1,'admin','/admin/showNews/','2013-10-15 20:56:39','127.0.0.1',NULL,NULL),
  (6995,1,'admin','/admin/formShowNews/','2013-10-15 20:56:47','127.0.0.1',NULL,NULL),
  (6996,1,'admin','/admin/showNews/','2013-10-15 20:57:17','127.0.0.1',NULL,NULL),
  (6997,1,'admin','/admin/formShowNews/','2013-10-15 20:57:27','127.0.0.1',NULL,NULL),
  (6998,1,'admin','/admin/showNews/','2013-10-15 20:57:51','127.0.0.1',NULL,NULL),
  (6999,1,'admin','/admin/formShowNews/','2013-10-15 20:57:55','127.0.0.1',NULL,NULL),
  (7000,1,'admin','/admin/showNews/','2013-10-15 20:58:16','127.0.0.1',NULL,NULL),
  (7001,1,'admin','/admin/formShowNews/','2013-10-15 20:58:21','127.0.0.1',NULL,NULL),
  (7002,1,'admin','/admin/showNews/','2013-10-15 20:58:52','127.0.0.1',NULL,NULL),
  (7003,1,'admin','/admin/formShowNews/','2013-10-15 20:58:56','127.0.0.1',NULL,NULL),
  (7004,1,'admin','/admin/showNews/','2013-10-15 20:59:37','127.0.0.1',NULL,NULL),
  (7005,1,'admin','/admin/formShowNews/','2013-10-15 20:59:41','127.0.0.1',NULL,NULL),
  (7006,1,'admin','/admin/showNews/','2013-10-15 20:59:59','127.0.0.1',NULL,NULL),
  (7007,1,'admin','/admin/formShowNews/','2013-10-15 21:00:03','127.0.0.1',NULL,NULL),
  (7008,1,'admin','/admin/showNews/','2013-10-15 21:00:32','127.0.0.1',NULL,NULL),
  (7009,1,'admin','/admin/formShowNews/','2013-10-15 21:00:46','127.0.0.1',NULL,NULL),
  (7010,1,'admin','/admin/showNews/','2013-10-15 21:00:54','127.0.0.1',NULL,NULL),
  (7011,1,'admin','/admin/formShowNews/','2013-10-15 21:00:57','127.0.0.1',NULL,NULL),
  (7012,1,'admin','/admin/showNews/','2013-10-16 00:03:10','127.0.0.1',NULL,NULL),
  (7013,1,'admin','/admin/formShowNews/','2013-10-16 00:03:15','127.0.0.1',NULL,NULL),
  (7014,1,'admin','/admin/showNews/','2013-10-16 00:04:49','127.0.0.1',NULL,NULL),
  (7015,1,'admin','/admin/formShowNews/','2013-10-16 00:04:53','127.0.0.1',NULL,NULL),
  (7016,1,'admin','/admin/showNews/','2013-10-16 00:05:05','127.0.0.1',NULL,NULL),
  (7017,1,'admin','/admin/formShowNews/','2013-10-16 00:05:09','127.0.0.1',NULL,NULL),
  (7018,1,'admin','/admin/showNews/','2013-10-16 00:08:41','127.0.0.1',NULL,NULL),
  (7019,1,'admin','/admin/formShowNews/','2013-10-16 00:08:45','127.0.0.1',NULL,NULL),
  (7020,1,'admin','/admin/showNews/','2013-10-16 00:09:52','127.0.0.1',NULL,NULL),
  (7021,1,'admin','/admin/formShowNews/','2013-10-16 00:10:04','127.0.0.1',NULL,NULL),
  (7022,1,'admin','/admin/showNews/','2013-10-16 00:10:17','127.0.0.1',NULL,NULL),
  (7023,1,'admin','/admin/formShowNews/','2013-10-16 00:10:21','127.0.0.1',NULL,NULL),
  (7024,1,'admin','/admin/showNews/','2013-10-16 00:14:26','127.0.0.1',NULL,NULL),
  (7025,1,'admin','/admin/formShowNews/','2013-10-16 00:14:37','127.0.0.1',NULL,NULL),
  (7026,1,'admin','/admin/showNews/','2013-10-16 00:15:02','127.0.0.1',NULL,NULL),
  (7027,1,'admin','/admin/formShowNews/','2013-10-16 00:15:06','127.0.0.1',NULL,NULL),
  (7028,1,'admin','/admin/showNews/','2013-10-16 00:17:27','127.0.0.1',NULL,NULL),
  (7029,1,'admin','/admin/formShowNews/','2013-10-16 00:17:32','127.0.0.1',NULL,NULL),
  (7030,1,'admin','/admin/showNews/','2013-10-16 00:18:20','127.0.0.1',NULL,NULL),
  (7031,1,'admin','/admin/formShowNews/','2013-10-16 00:18:28','127.0.0.1',NULL,NULL),
  (7032,1,'admin','/admin/showNews/','2013-10-16 00:23:13','127.0.0.1',NULL,NULL),
  (7033,1,'admin','/admin/formShowNews/','2013-10-16 00:23:17','127.0.0.1',NULL,NULL),
  (7034,1,'admin','/admin/showNews/','2013-10-16 00:31:22','127.0.0.1',NULL,NULL),
  (7035,1,'admin','/admin/formShowNews/','2013-10-16 00:31:26','127.0.0.1',NULL,NULL),
  (7036,1,'admin','/admin/showNews/','2013-10-16 00:40:40','127.0.0.1',NULL,NULL),
  (7037,1,'admin','/admin/formShowNews/','2013-10-16 00:40:50','127.0.0.1',NULL,NULL),
  (7038,1,'admin','/admin/showNews/','2013-10-16 00:41:42','127.0.0.1',NULL,NULL),
  (7039,1,'admin','/admin/formShowNews/','2013-10-16 00:41:46','127.0.0.1',NULL,NULL),
  (7040,1,'admin','/admin/showNews/','2013-10-16 00:59:19','127.0.0.1',NULL,NULL),
  (7041,1,'admin','/admin/formShowNews/','2013-10-16 00:59:23','127.0.0.1',NULL,NULL);
COMMIT;

#
# Data for the `pharus_system_gurnal` table  (LIMIT 500,500)
#

INSERT INTO `pharus_system_gurnal` (`id_system_gurnal`, `id_users`, `login`, `action`, `date`, `ip_address`, `db_old`, `db_new`) VALUES 
  (7042,1,'admin','/admin/showNews/','2013-10-16 00:59:36','127.0.0.1',NULL,NULL),
  (7043,1,'admin','/admin/formShowNews/','2013-10-16 00:59:40','127.0.0.1',NULL,NULL),
  (7044,1,'admin','/admin/showNews/','2013-10-16 01:00:04','127.0.0.1',NULL,NULL),
  (7045,1,'admin','/admin/formShowNews/','2013-10-16 01:00:09','127.0.0.1',NULL,NULL),
  (7046,1,'admin','/admin/showNews/','2013-10-16 01:23:21','127.0.0.1',NULL,NULL),
  (7047,1,'admin','/admin/formShowNews/','2013-10-16 01:23:31','127.0.0.1',NULL,NULL),
  (7048,1,'admin','/admin/showNews/','2013-10-16 01:23:46','127.0.0.1',NULL,NULL),
  (7049,1,'admin','/admin/formShowNews/','2013-10-16 01:23:51','127.0.0.1',NULL,NULL),
  (7050,1,'admin','/admin/showNews/','2013-10-16 01:24:00','127.0.0.1',NULL,NULL),
  (7051,1,'admin','/admin/formShowNews/','2013-10-16 01:24:04','127.0.0.1',NULL,NULL),
  (7052,1,'admin','/admin/formShowNews/','2013-10-16 01:24:11','127.0.0.1',NULL,NULL),
  (7053,1,'admin','/admin/showNews/','2013-10-16 01:25:10','127.0.0.1',NULL,NULL),
  (7054,1,'admin','/admin/formShowNews/','2013-10-16 01:25:15','127.0.0.1',NULL,NULL),
  (7055,1,'admin','/admin/showNews/','2013-10-16 01:26:11','127.0.0.1',NULL,NULL),
  (7056,1,'admin','/admin/formShowNews/','2013-10-16 01:26:16','127.0.0.1',NULL,NULL),
  (7057,1,'admin','/admin/showNews/','2013-10-16 01:26:42','127.0.0.1',NULL,NULL),
  (7058,1,'admin','/admin/formShowNews/','2013-10-16 01:26:46','127.0.0.1',NULL,NULL),
  (7059,1,'admin','/admin/showNews/','2013-10-16 01:27:02','127.0.0.1',NULL,NULL),
  (7060,1,'admin','/admin/formShowNews/','2013-10-16 01:27:06','127.0.0.1',NULL,NULL),
  (7061,1,'admin','/admin/showNews/','2013-10-16 01:28:35','127.0.0.1',NULL,NULL),
  (7062,1,'admin','/admin/formShowNews/','2013-10-16 01:28:41','127.0.0.1',NULL,NULL),
  (7063,1,'admin','/admin/showNews/','2013-10-16 01:29:03','127.0.0.1',NULL,NULL),
  (7064,1,'admin','/admin/formShowNews/','2013-10-16 01:29:08','127.0.0.1',NULL,NULL),
  (7065,1,'admin','/admin/showNews/','2013-10-16 01:31:04','127.0.0.1',NULL,NULL),
  (7066,1,'admin','/admin/formShowNews/','2013-10-16 01:31:09','127.0.0.1',NULL,NULL),
  (7067,1,'admin','/admin/showNews/','2013-10-16 01:31:11','127.0.0.1',NULL,NULL),
  (7068,1,'admin','/admin/formShowNews/','2013-10-16 01:31:16','127.0.0.1',NULL,NULL),
  (7069,1,'admin','/admin/showNews/','2013-10-16 01:31:29','127.0.0.1',NULL,NULL),
  (7070,1,'admin','/admin/formShowNews/','2013-10-16 01:31:33','127.0.0.1',NULL,NULL),
  (7071,1,'admin','/admin/formShowNews/','2013-10-16 01:31:40','127.0.0.1',NULL,NULL),
  (7072,1,'admin','/admin/formShowNews/','2013-10-16 01:32:04','127.0.0.1',NULL,NULL),
  (7073,1,'admin','/admin/showNews/','2013-10-16 01:36:16','127.0.0.1',NULL,NULL),
  (7074,1,'admin','/admin/formShowNews/','2013-10-16 01:36:20','127.0.0.1',NULL,NULL),
  (7075,1,'admin','/admin/formShowNews/','2013-10-16 01:36:27','127.0.0.1',NULL,NULL),
  (7076,1,'admin','/admin/showNews/','2013-10-16 01:47:56','127.0.0.1',NULL,NULL),
  (7077,1,'admin','/admin/formShowNews/','2013-10-16 01:48:01','127.0.0.1',NULL,NULL),
  (7078,1,'admin','/admin/showNews/','2013-10-16 01:48:22','127.0.0.1',NULL,NULL),
  (7079,1,'admin','/admin/formShowNews/','2013-10-16 01:48:29','127.0.0.1',NULL,NULL),
  (7080,1,'admin','/admin/showNews/','2013-10-16 01:48:44','127.0.0.1',NULL,NULL),
  (7081,1,'admin','/admin/formShowNews/','2013-10-16 01:48:48','127.0.0.1',NULL,NULL),
  (7082,1,'admin','/admin/showNews/','2013-10-16 02:00:01','127.0.0.1',NULL,NULL),
  (7083,1,'admin','/admin/formShowNews/','2013-10-16 02:00:08','127.0.0.1',NULL,NULL),
  (7084,1,'admin','/admin/showNews/','2013-10-16 02:01:07','127.0.0.1',NULL,NULL),
  (7085,1,'admin','/admin/formShowNews/','2013-10-16 02:01:13','127.0.0.1',NULL,NULL),
  (7086,1,'admin','/admin/showNews/','2013-10-16 02:02:16','127.0.0.1',NULL,NULL),
  (7087,1,'admin','/admin/formShowNews/','2013-10-16 02:02:21','127.0.0.1',NULL,NULL),
  (7088,1,'admin','/admin/showNews/','2013-10-16 02:17:15','127.0.0.1',NULL,NULL),
  (7089,1,'admin','/admin/formShowNews/','2013-10-16 02:17:22','127.0.0.1',NULL,NULL),
  (7090,1,'admin','/admin/showNews/','2013-10-16 02:18:34','127.0.0.1',NULL,NULL),
  (7091,1,'admin','/admin/formShowNews/','2013-10-16 02:18:39','127.0.0.1',NULL,NULL),
  (7092,1,'admin','/admin/showNews/','2013-10-16 02:20:30','127.0.0.1',NULL,NULL),
  (7093,1,'admin','/admin/formShowNews/','2013-10-16 02:20:35','127.0.0.1',NULL,NULL),
  (7094,1,'admin','/admin/showNews/','2013-10-16 02:21:27','127.0.0.1',NULL,NULL),
  (7095,1,'admin','/admin/formShowNews/','2013-10-16 02:21:33','127.0.0.1',NULL,NULL),
  (7096,1,'admin','/admin/showNews/','2013-10-16 02:36:53','127.0.0.1',NULL,NULL),
  (7097,1,'admin','/admin/formShowNews/','2013-10-16 02:36:58','127.0.0.1',NULL,NULL),
  (7098,1,'admin','/admin/showNews/','2013-10-16 02:37:17','127.0.0.1',NULL,NULL),
  (7099,1,'admin','/admin/formShowNews/','2013-10-16 02:37:22','127.0.0.1',NULL,NULL),
  (7100,1,'admin','/admin/showNews/','2013-10-16 02:37:57','127.0.0.1',NULL,NULL),
  (7101,1,'admin','/admin/formShowNews/','2013-10-16 02:38:01','127.0.0.1',NULL,NULL),
  (7102,1,'admin','/admin/showNews/','2013-10-16 02:39:38','127.0.0.1',NULL,NULL),
  (7103,1,'admin','/admin/formShowNews/','2013-10-16 02:39:43','127.0.0.1',NULL,NULL),
  (7104,1,'admin','/admin/showNews/','2013-10-16 02:40:02','127.0.0.1',NULL,NULL),
  (7105,1,'admin','/admin/formShowNews/','2013-10-16 02:40:05','127.0.0.1',NULL,NULL),
  (7106,1,'admin','/admin/showNews/','2013-10-16 02:42:31','127.0.0.1',NULL,NULL),
  (7107,1,'admin','/admin/formShowNews/','2013-10-16 02:42:41','127.0.0.1',NULL,NULL),
  (7108,1,'admin','/admin/formShowNews/','2013-10-16 02:42:48','127.0.0.1',NULL,NULL),
  (7109,1,'admin','/admin/formShowNews/','2013-10-16 02:43:37','127.0.0.1',NULL,NULL),
  (7110,1,'admin','/admin/showNews/','2013-10-16 02:43:54','127.0.0.1',NULL,NULL),
  (7111,1,'admin','/admin/formShowNews/','2013-10-16 02:43:59','127.0.0.1',NULL,NULL),
  (7112,1,'admin','/admin/showNews/','2013-10-16 02:44:14','127.0.0.1',NULL,NULL),
  (7113,1,'admin','/admin/formShowNews/','2013-10-16 02:44:18','127.0.0.1',NULL,NULL),
  (7114,1,'admin','/admin/showNews/','2013-10-16 02:48:35','127.0.0.1',NULL,NULL),
  (7115,1,'admin','/admin/formShowNews/','2013-10-16 02:48:39','127.0.0.1',NULL,NULL),
  (7116,1,'admin','/admin/showNews/','2013-10-16 02:48:51','127.0.0.1',NULL,NULL),
  (7117,1,'admin','/admin/formShowNews/','2013-10-16 02:48:56','127.0.0.1',NULL,NULL),
  (7118,1,'admin','/admin/showNews/','2013-10-16 02:49:10','127.0.0.1',NULL,NULL),
  (7119,1,'admin','/admin/formShowNews/','2013-10-16 02:49:14','127.0.0.1',NULL,NULL),
  (7120,1,'admin','/admin/showNews/','2013-10-16 02:50:09','127.0.0.1',NULL,NULL),
  (7121,1,'admin','/admin/formShowNews/','2013-10-16 02:50:13','127.0.0.1',NULL,NULL),
  (7122,1,'admin','/admin/showNews/','2013-10-16 02:51:13','127.0.0.1',NULL,NULL),
  (7123,1,'admin','/admin/formShowNews/','2013-10-16 02:51:17','127.0.0.1',NULL,NULL),
  (7124,1,'admin','/admin/showNews/','2013-10-16 02:52:09','127.0.0.1',NULL,NULL),
  (7125,1,'admin','/admin/formShowNews/','2013-10-16 02:52:13','127.0.0.1',NULL,NULL),
  (7126,1,'admin','/admin/showNews/','2013-10-16 02:52:33','127.0.0.1',NULL,NULL),
  (7127,1,'admin','/admin/formShowNews/','2013-10-16 02:52:41','127.0.0.1',NULL,NULL),
  (7128,1,'admin','/admin/showNews/','2013-10-16 02:53:13','127.0.0.1',NULL,NULL),
  (7129,1,'admin','/admin/formShowNews/','2013-10-16 02:53:19','127.0.0.1',NULL,NULL),
  (7130,1,'admin','/admin/showNews/','2013-10-16 02:57:29','127.0.0.1',NULL,NULL),
  (7131,1,'admin','/admin/formShowNews/','2013-10-16 02:57:34','127.0.0.1',NULL,NULL),
  (7132,1,'admin','/admin/showNews/','2013-10-16 02:59:54','127.0.0.1',NULL,NULL),
  (7133,1,'admin','/admin/formShowNews/','2013-10-16 02:59:58','127.0.0.1',NULL,NULL),
  (7134,1,'admin','/admin/showNews/','2013-10-16 03:02:06','127.0.0.1',NULL,NULL),
  (7135,1,'admin','/admin/formShowNews/','2013-10-16 03:02:11','127.0.0.1',NULL,NULL),
  (7136,1,'admin','/admin/showNews/','2013-10-16 10:48:20','127.0.0.1',NULL,NULL),
  (7137,1,'admin','/admin/showNews/','2013-10-16 10:48:21','127.0.0.1',NULL,NULL),
  (7138,1,'admin','/admin/formShowNews/','2013-10-16 10:48:27','127.0.0.1',NULL,NULL),
  (7139,1,'admin','/admin/showNews/','2013-10-16 10:49:12','127.0.0.1',NULL,NULL),
  (7140,1,'admin','/admin/formShowNews/','2013-10-16 10:49:16','127.0.0.1',NULL,NULL),
  (7141,1,'admin','/admin/showNews/','2013-10-16 13:08:19','127.0.0.1',NULL,NULL),
  (7142,1,'admin','/admin/formShowNews/','2013-10-16 13:08:52','127.0.0.1',NULL,NULL),
  (7143,1,'admin','/admin/showNews/','2013-10-16 13:14:08','127.0.0.1',NULL,NULL),
  (7144,1,'admin','/admin/formShowNews/','2013-10-16 13:14:11','127.0.0.1',NULL,NULL),
  (7145,1,'admin','/admin/showNews/','2013-10-16 13:18:53','127.0.0.1',NULL,NULL),
  (7146,1,'admin','/admin/formShowNews/','2013-10-16 13:18:59','127.0.0.1',NULL,NULL),
  (7147,1,'admin','/admin/showNews/','2013-10-16 13:21:27','127.0.0.1',NULL,NULL),
  (7148,1,'admin','/admin/formShowNews/','2013-10-16 13:21:39','127.0.0.1',NULL,NULL),
  (7149,1,'admin','/admin/showNews/','2013-10-16 13:25:43','127.0.0.1',NULL,NULL),
  (7150,1,'admin','/admin/formShowNews/','2013-10-16 13:25:48','127.0.0.1',NULL,NULL),
  (7151,1,'admin','/admin/showNews/','2013-10-16 13:26:25','127.0.0.1',NULL,NULL),
  (7152,1,'admin','/admin/formShowNews/','2013-10-16 13:26:30','127.0.0.1',NULL,NULL),
  (7153,1,'admin','/admin/showNews/','2013-10-16 13:27:39','127.0.0.1',NULL,NULL),
  (7154,1,'admin','/admin/formShowNews/','2013-10-16 13:27:44','127.0.0.1',NULL,NULL),
  (7155,1,'admin','/admin/showNews/','2013-10-16 13:28:50','127.0.0.1',NULL,NULL),
  (7156,1,'admin','/admin/showNews/','2013-10-16 13:28:54','127.0.0.1',NULL,NULL),
  (7157,1,'admin','/admin/showNews/','2013-10-16 13:28:59','127.0.0.1',NULL,NULL),
  (7158,1,'admin','/admin/formShowNews/','2013-10-16 13:29:13','127.0.0.1',NULL,NULL),
  (7159,1,'admin','/admin/showNews/','2013-10-16 13:31:54','127.0.0.1',NULL,NULL),
  (7160,1,'admin','/admin/formShowNews/','2013-10-16 13:32:00','127.0.0.1',NULL,NULL),
  (7161,1,'admin','/admin/showNews/','2013-10-16 13:33:14','127.0.0.1',NULL,NULL),
  (7162,1,'admin','/admin/formShowNews/','2013-10-16 13:33:38','127.0.0.1',NULL,NULL),
  (7163,1,'admin','/admin/showNews/','2013-10-16 13:34:12','127.0.0.1',NULL,NULL),
  (7164,1,'admin','/admin/formShowNews/','2013-10-16 13:34:17','127.0.0.1',NULL,NULL),
  (7165,1,'admin','/admin/','2013-10-16 13:38:07','127.0.0.1',NULL,NULL),
  (7166,1,'admin','/admin/showNews/','2013-10-16 13:38:14','127.0.0.1',NULL,NULL),
  (7167,1,'admin','/admin/formShowNews/','2013-10-16 13:38:18','127.0.0.1',NULL,NULL),
  (7168,1,'admin','/admin/formShowNews/','2013-10-17 00:20:09','127.0.0.1',NULL,NULL),
  (7169,1,'admin','/admin/showNews/','2013-10-17 00:20:50','127.0.0.1',NULL,NULL),
  (7170,1,'admin','/admin/formShowNews/','2013-10-17 00:20:55','127.0.0.1',NULL,NULL),
  (7171,1,'admin','/admin/','2013-10-23 00:53:27','127.0.0.1',NULL,NULL),
  (7172,1,'admin','/admin/showMenu/','2013-10-23 00:53:36','127.0.0.1',NULL,NULL),
  (7173,1,'admin','/admin/showMenu/','2013-10-24 22:33:02','127.0.0.1',NULL,NULL),
  (7174,1,'admin','/admin/showMenu/','2013-10-27 22:49:21','127.0.0.1',NULL,NULL),
  (7175,1,'admin','/admin/','2013-11-05 21:13:40','127.0.0.1',NULL,NULL),
  (7176,1,'admin','/admin/','2013-11-06 12:07:39','127.0.0.1',NULL,NULL),
  (7177,1,'admin','/admin/','2013-11-06 12:07:40','127.0.0.1',NULL,NULL),
  (7178,1,'admin','/admin/showNews/','2013-11-06 14:16:14','127.0.0.1',NULL,NULL),
  (7179,1,'admin','/admin/showSp/vid_news/','2013-11-06 14:16:21','127.0.0.1',NULL,NULL),
  (7180,1,'admin','/admin/formShowSp/vid_news','2013-11-06 14:16:48','127.0.0.1',NULL,NULL),
  (7181,1,'admin','/admin/updateSp/vid_news','2013-11-06 14:16:55','127.0.0.1','a:5:{s:14:\"id_sp_vid_news\";s:1:\"2\";s:4:\"name\";s:20:\"Объявления\";s:10:\"is_visible\";s:1:\"1\";s:3:\"url\";s:12:\"objyavleniya\";s:8:\"position\";s:2:\"22\";}','a:5:{s:14:\"id_sp_vid_news\";s:1:\"2\";s:4:\"name\";s:20:\"Объявления\";s:10:\"is_visible\";s:1:\"1\";s:3:\"url\";s:12:\"objyavleniya\";s:8:\"position\";s:1:\"2\";}'),
  (7182,1,'admin','/admin/showSp/vid_news/','2013-11-06 14:16:56','127.0.0.1',NULL,NULL),
  (7183,1,'admin','/admin/showNews/','2013-11-06 14:19:19','127.0.0.1',NULL,NULL),
  (7184,1,'admin','/admin/formShowNews/','2013-11-06 14:19:38','127.0.0.1',NULL,NULL),
  (7185,1,'admin','/admin/updateNews/','2013-11-06 14:19:54','127.0.0.1','a:16:{s:7:\"id_news\";s:2:\"19\";s:8:\"id_users\";s:1:\"1\";s:4:\"date\";s:19:\"2013-07-22 18:24:55\";s:9:\"zagolovok\";s:140:\"Преображенский суд Москвы арестовал ректора Государственного университета\";s:10:\"annotaciya\";s:201:\"Преображенский суд \"Москвы\" арестовал ректора Государственного университета управления Виктора Козбаненкоrn\";s:4:\"text\";s:507:\"<p><a class=\"openimg\" href=\"/upload/images/fototelegramma.jpg\">Нововея мерзопасконое действиеПреображенский</a> суд <a class=\"openimg\" href=\"/upload/images/galstuk.jpg\" >Москвы</a> <a class=\"openimg\" href=\"/upload/images/galstuk.jpg\">Москвы</a> &quot;арестовал&quot; ректора Государственного университета управления Виктора Козбаненко</p>\r\n\r\n<p class=\"math\">a^2 + b^2 = c^2</p>\r\n\";s:7:\"comment\";s:40:\"<p>Так &quot;бывает&quot;</p>\r\n\";s:7:\"is_main\";N;s:10:\"is_visible\";s:1:\"1\";s:6:\"is_rss\";s:1:\"1\";s:3:\"url\";s:110:\"preobrazhenskiy-sud-moskvy-arestoval-rektora-gosudarstvennogo-universiteta-upravleniya-viktora-kozbanenko.html\";s:10:\"main_theme\";s:1:\"1\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:197:\"Преображенский суд Москвы арестовал ректора Государственного университета управления Виктора Козбаненко\";s:7:\"details\";a:2:{s:11:\"sp_vid_news\";a:2:{i:1;s:30:\"Статьи, интервью\";i:2;s:20:\"Объявления\";}s:13:\"sp_theme_news\";a:1:{i:1;s:40:\"Cтандарты образования\";}}}','a:16:{s:7:\"id_news\";s:2:\"19\";s:8:\"id_users\";s:1:\"1\";s:4:\"date\";s:19:\"2013-07-22 18:24:55\";s:9:\"zagolovok\";s:140:\"Преображенский суд Москвы арестовал ректора Государственного университета\";s:10:\"annotaciya\";s:201:\"Преображенский суд \"Москвы\" арестовал ректора Государственного университета управления Виктора Козбаненкоrn\";s:4:\"text\";s:506:\"<p><a class=\"openimg\" href=\"/upload/images/fototelegramma.jpg\">Нововея мерзопасконое действиеПреображенский</a> суд <a class=\"openimg\" href=\"/upload/images/galstuk.jpg\">Москвы</a> <a class=\"openimg\" href=\"/upload/images/galstuk.jpg\">Москвы</a> &quot;арестовал&quot; ректора Государственного университета управления Виктора Козбаненко</p>\r\n\r\n<p class=\"math\">a^2 + b^2 = c^2</p>\r\n\";s:7:\"comment\";s:40:\"<p>Так &quot;бывает&quot;</p>\r\n\";s:7:\"is_main\";N;s:10:\"is_visible\";s:1:\"1\";s:6:\"is_rss\";s:1:\"1\";s:3:\"url\";s:110:\"preobrazhenskiy-sud-moskvy-arestoval-rektora-gosudarstvennogo-universiteta-upravleniya-viktora-kozbanenko.html\";s:10:\"main_theme\";s:1:\"1\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:197:\"Преображенский суд Москвы арестовал ректора Государственного университета управления Виктора Козбаненко\";s:7:\"details\";a:2:{s:11:\"sp_vid_news\";a:2:{i:1;s:30:\"Статьи, интервью\";i:2;s:20:\"Объявления\";}s:13:\"sp_theme_news\";a:2:{i:1;s:40:\"Cтандарты образования\";i:4;s:37:\"Реформы образования\";}}}'),
  (7186,1,'admin','/admin/showNews/','2013-11-06 14:19:55','127.0.0.1',NULL,NULL),
  (7187,1,'admin','/admin/formShowNews/','2013-11-06 14:20:38','127.0.0.1',NULL,NULL),
  (7188,1,'admin','/admin/showNews/','2013-11-08 10:23:19','127.0.0.1',NULL,NULL),
  (7189,1,'admin','/admin/showNews/','2013-11-09 22:11:04','127.0.0.1',NULL,NULL),
  (7190,1,'admin','/admin/showNews/','2013-11-12 22:21:17','127.0.0.1',NULL,NULL),
  (7191,1,'admin','/admin/','2013-12-18 10:50:06','127.0.0.1',NULL,NULL),
  (7192,1,'admin','/admin/showLib/book/','2013-12-18 10:50:18','127.0.0.1',NULL,NULL),
  (7193,1,'admin','/admin/showStaticPage/','2013-12-18 10:50:26','127.0.0.1',NULL,NULL),
  (7194,1,'admin','/admin/','2013-12-27 23:48:04','127.0.0.1',NULL,NULL),
  (7195,1,'admin','/admin/','2013-12-27 23:48:05','127.0.0.1',NULL,NULL),
  (7196,1,'admin','/admin/showLib/book/','2013-12-27 23:48:21','127.0.0.1',NULL,NULL),
  (7197,1,'admin','/admin/showStaticPage/','2013-12-27 23:48:47','127.0.0.1',NULL,NULL),
  (7198,1,'admin','/admin/showStaticPage/','2013-12-28 00:10:11','127.0.0.1',NULL,NULL),
  (7199,1,'admin','/admin/showStaticPage/','2013-12-28 00:11:35','127.0.0.1',NULL,NULL),
  (7200,1,'admin','/admin/showStaticPage/','2013-12-28 00:14:06','127.0.0.1',NULL,NULL),
  (7201,1,'admin','/admin/showStaticPage/','2013-12-28 00:15:03','127.0.0.1',NULL,NULL),
  (7202,1,'admin','/admin/showStaticPage/','2013-12-28 00:15:46','127.0.0.1',NULL,NULL),
  (7203,1,'admin','/admin/showStaticPage/','2013-12-28 00:22:06','127.0.0.1',NULL,NULL),
  (7204,1,'admin','/admin/showStaticPage/','2013-12-28 00:22:30','127.0.0.1',NULL,NULL),
  (7205,1,'admin','/admin/showStaticPage/','2013-12-28 00:23:11','127.0.0.1',NULL,NULL),
  (7206,1,'admin','/admin/showStaticPage/','2013-12-28 00:23:22','127.0.0.1',NULL,NULL),
  (7207,1,'admin','/admin/showStaticPage/','2013-12-28 00:24:17','127.0.0.1',NULL,NULL),
  (7208,1,'admin','/admin/showStaticPage/','2013-12-28 00:24:56','127.0.0.1',NULL,NULL),
  (7209,1,'admin','/admin/showStaticPage/','2013-12-28 00:25:40','127.0.0.1',NULL,NULL),
  (7210,1,'admin','/admin/showStaticPage/','2013-12-28 00:25:57','127.0.0.1',NULL,NULL),
  (7211,1,'admin','/admin/showStaticPage/','2013-12-28 00:26:53','127.0.0.1',NULL,NULL),
  (7212,1,'admin','/admin/showStaticPage/','2013-12-28 00:27:06','127.0.0.1',NULL,NULL),
  (7213,1,'admin','/admin/showStaticPage/','2013-12-28 00:30:50','127.0.0.1',NULL,NULL),
  (7214,1,'admin','/admin/showStaticPage/','2013-12-28 00:31:08','127.0.0.1',NULL,NULL),
  (7215,1,'admin','/admin/showStaticPage/','2013-12-28 00:34:16','127.0.0.1',NULL,NULL),
  (7216,1,'admin','/admin/showStaticPage/','2013-12-28 00:34:41','127.0.0.1',NULL,NULL),
  (7217,1,'admin','/admin/showStaticPage/','2013-12-28 00:35:43','127.0.0.1',NULL,NULL),
  (7218,1,'admin','/admin/showStaticPage/','2013-12-28 00:36:39','127.0.0.1',NULL,NULL),
  (7219,1,'admin','/admin/showStaticPage/','2013-12-28 00:37:59','127.0.0.1',NULL,NULL),
  (7220,1,'admin','/admin/showStaticPage/','2013-12-28 00:39:15','127.0.0.1',NULL,NULL),
  (7221,1,'admin','/admin/showStaticPage/','2013-12-28 00:39:40','127.0.0.1',NULL,NULL),
  (7222,1,'admin','/admin/showStaticPage/','2013-12-28 00:39:48','127.0.0.1',NULL,NULL),
  (7223,1,'admin','/admin/showStaticPage/','2013-12-28 00:40:44','127.0.0.1',NULL,NULL),
  (7224,1,'admin','/admin/showStaticPage/','2013-12-28 00:45:42','127.0.0.1',NULL,NULL),
  (7225,1,'admin','/admin/showStaticPage/','2013-12-28 00:45:56','127.0.0.1',NULL,NULL),
  (7226,1,'admin','/admin/showStaticPage/','2013-12-28 00:57:31','127.0.0.1',NULL,NULL),
  (7227,1,'admin','/admin/showStaticPage/','2013-12-28 00:59:31','127.0.0.1',NULL,NULL),
  (7228,1,'admin','/admin/showStaticPage/','2013-12-28 01:03:13','127.0.0.1',NULL,NULL),
  (7229,1,'admin','/admin/showStaticPage/','2013-12-28 01:04:39','127.0.0.1',NULL,NULL),
  (7230,1,'admin','/admin/showStaticPage/','2013-12-28 01:05:10','127.0.0.1',NULL,NULL),
  (7231,1,'admin','/admin/showStaticPage/','2013-12-28 01:07:08','127.0.0.1',NULL,NULL),
  (7232,1,'admin','/admin/showStaticPage/','2013-12-28 01:07:31','127.0.0.1',NULL,NULL),
  (7233,1,'admin','/admin/showStaticPage/','2013-12-28 01:08:14','127.0.0.1',NULL,NULL),
  (7234,1,'admin','/admin/formShowStaticPage/','2013-12-28 01:08:23','127.0.0.1',NULL,NULL),
  (7235,1,'admin','/admin/updateStaticPage/','2013-12-28 01:08:27','127.0.0.1','a:20:{s:14:\"id_static_page\";s:2:\"89\";s:18:\"id_static_page_top\";s:2:\"88\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:24:\"Исторические\";s:3:\"url\";s:13:\"istoricheskie\";s:10:\"annotaciya\";s:0:\"\";s:4:\"text\";s:113:\"<p>Текст &quot;Исторические&quot;, а также <a href=\"[[~95~]]\">искусство</a></p>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:24:\"Исторические\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-29 20:51:56\";s:11:\"date_update\";s:19:\"2013-07-31 15:11:18\";s:10:\"is_folders\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:5:\"level\";s:1:\"3\";s:8:\"link_out\";N;s:15:\"id_page_forwars\";N;}','a:20:{s:14:\"id_static_page\";s:2:\"89\";s:18:\"id_static_page_top\";s:2:\"88\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:24:\"Исторические\";s:3:\"url\";s:13:\"istoricheskie\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:111:\"<p>Текст &quot;Исторические&quot;, а также <a href=\"[[~95~]]\">искусство</a></p>\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:24:\"Исторические\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-29 20:51:56\";s:11:\"date_update\";s:19:\"2013-12-28 01:08:27\";s:10:\"is_folders\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:5:\"level\";s:1:\"3\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}'),
  (7236,1,'admin','/admin/updateStaticPage/','2013-12-28 01:08:28','127.0.0.1','a:20:{s:14:\"id_static_page\";s:2:\"89\";s:18:\"id_static_page_top\";s:2:\"88\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:24:\"Исторические\";s:3:\"url\";s:13:\"istoricheskie\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:111:\"<p>Текст &quot;Исторические&quot;, а также <a href=\"[[~95~]]\">искусство</a></p>\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:24:\"Исторические\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-29 20:51:56\";s:11:\"date_update\";s:19:\"2013-12-28 01:08:27\";s:10:\"is_folders\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:5:\"level\";s:1:\"3\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}','a:20:{s:14:\"id_static_page\";s:2:\"89\";s:18:\"id_static_page_top\";s:2:\"88\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:24:\"Исторические\";s:3:\"url\";s:13:\"istoricheskie\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:113:\"<p>Текст &quot;Исторические&quot;, а также <a href=\"[[~95~]]\">искусство</a></p>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:24:\"Исторические\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-29 20:51:56\";s:11:\"date_update\";s:19:\"2013-12-28 01:08:28\";s:10:\"is_folders\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:5:\"level\";s:1:\"3\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}'),
  (7237,1,'admin','/admin/showStaticPage/','2013-12-28 01:08:28','127.0.0.1',NULL,NULL),
  (7238,1,'admin','/admin/showSp/education/','2013-12-28 01:09:27','127.0.0.1',NULL,NULL),
  (7239,1,'admin','/admin/showSp/organasation/','2013-12-28 01:09:33','127.0.0.1',NULL,NULL),
  (7240,1,'admin','/admin/showSp/theme_news/','2013-12-28 01:09:38','127.0.0.1',NULL,NULL),
  (7241,1,'admin','/admin/showSp/vid_news/','2013-12-28 01:09:47','127.0.0.1',NULL,NULL),
  (7242,1,'admin','/admin/showMenu/','2013-12-28 01:09:51','127.0.0.1',NULL,NULL),
  (7243,1,'admin','/admin/showMenu/','2013-12-28 01:13:49','127.0.0.1',NULL,NULL),
  (7244,1,'admin','/admin/showMenu/','2013-12-28 01:15:15','127.0.0.1',NULL,NULL),
  (7245,1,'admin','/admin/showMenu/','2013-12-28 01:15:33','127.0.0.1',NULL,NULL),
  (7246,1,'admin','/admin/showMenu/','2013-12-28 01:16:14','127.0.0.1',NULL,NULL),
  (7247,1,'admin','/admin/showMenu/','2013-12-28 01:16:58','127.0.0.1',NULL,NULL),
  (7248,1,'admin','/admin/showMenu/','2013-12-28 01:17:15','127.0.0.1',NULL,NULL),
  (7249,1,'admin','/admin/showMenu/','2013-12-28 01:19:14','127.0.0.1',NULL,NULL),
  (7250,1,'admin','/admin/showMenu/','2013-12-28 01:20:01','127.0.0.1',NULL,NULL),
  (7251,1,'admin','/admin/showMenu/','2013-12-28 01:20:44','127.0.0.1',NULL,NULL),
  (7252,1,'admin','/admin/showMenu/','2013-12-28 01:23:10','127.0.0.1',NULL,NULL),
  (7253,1,'admin','/admin/showMenu/','2013-12-28 01:23:49','127.0.0.1',NULL,NULL),
  (7254,1,'admin','/admin/showMenu/','2013-12-28 01:24:47','127.0.0.1',NULL,NULL),
  (7255,1,'admin','/admin/showMenu/','2013-12-28 01:26:55','127.0.0.1',NULL,NULL),
  (7256,1,'admin','/admin/showMenu/','2013-12-28 01:28:35','127.0.0.1',NULL,NULL),
  (7257,1,'admin','/admin/showMenu/','2013-12-28 01:29:29','127.0.0.1',NULL,NULL),
  (7258,1,'admin','/admin/showMenu/','2013-12-28 01:30:57','127.0.0.1',NULL,NULL),
  (7259,1,'admin','/admin/showMenu/','2013-12-28 01:31:04','127.0.0.1',NULL,NULL),
  (7260,1,'admin','/admin/showMenu/','2013-12-28 01:31:32','127.0.0.1',NULL,NULL),
  (7261,1,'admin','/admin/showMenu/','2013-12-28 01:31:49','127.0.0.1',NULL,NULL),
  (7262,1,'admin','/admin/showMenu/','2013-12-28 01:32:08','127.0.0.1',NULL,NULL),
  (7263,1,'admin','/admin/showMenu/','2013-12-28 01:32:21','127.0.0.1',NULL,NULL),
  (7264,1,'admin','/admin/showMenu/','2013-12-28 01:33:01','127.0.0.1',NULL,NULL),
  (7265,1,'admin','/admin/showMenu/','2013-12-28 01:33:54','127.0.0.1',NULL,NULL),
  (7266,1,'admin','/admin/showMenu/','2013-12-28 01:34:03','127.0.0.1',NULL,NULL),
  (7267,1,'admin','/admin/showMenu/','2013-12-28 01:37:34','127.0.0.1',NULL,NULL),
  (7268,1,'admin','/admin/showStaticPage/','2013-12-28 01:38:31','127.0.0.1',NULL,NULL),
  (7269,1,'admin','/admin/showStaticPage/','2013-12-28 01:40:10','127.0.0.1',NULL,NULL),
  (7270,1,'admin','/admin/showStaticPage/','2013-12-28 01:42:32','127.0.0.1',NULL,NULL),
  (7271,1,'admin','/admin/showStaticPage/','2013-12-28 01:44:28','127.0.0.1',NULL,NULL),
  (7272,1,'admin','/admin/showStaticPage/','2013-12-28 01:45:11','127.0.0.1',NULL,NULL),
  (7273,1,'admin','/admin/showStaticPage/','2013-12-28 01:46:32','127.0.0.1',NULL,NULL),
  (7274,1,'admin','/admin/showStaticPage/','2013-12-28 01:49:51','127.0.0.1',NULL,NULL),
  (7275,1,'admin','/admin/showStaticPage/','2013-12-28 01:50:46','127.0.0.1',NULL,NULL),
  (7276,1,'admin','/admin/showPerson/','2013-12-28 01:51:26','127.0.0.1',NULL,NULL),
  (7277,1,'admin','/admin/showEvents/1/','2013-12-28 01:51:39','127.0.0.1',NULL,NULL),
  (7278,1,'admin','/admin/showLib/book/','2013-12-28 01:51:47','127.0.0.1',NULL,NULL),
  (7279,1,'admin','/admin/showLib/article/','2013-12-28 01:51:53','127.0.0.1',NULL,NULL),
  (7280,1,'admin','/admin/showStaticPage/','2013-12-28 01:52:24','127.0.0.1',NULL,NULL),
  (7281,1,'admin','/admin/showStaticPage/','2013-12-28 01:53:14','127.0.0.1',NULL,NULL),
  (7282,1,'admin','/admin/showSp/organasation/','2013-12-28 09:05:39','127.0.0.1',NULL,NULL),
  (7283,1,'admin','/admin/showLib/thesis/','2013-12-28 09:05:54','127.0.0.1',NULL,NULL),
  (7284,1,'admin','/admin/showLib/thesis/','2013-12-28 09:09:10','127.0.0.1',NULL,NULL),
  (7285,1,'admin','/admin/showLib/thesis/','2013-12-28 09:10:30','127.0.0.1',NULL,NULL),
  (7286,1,'admin','/admin/showLib/thesis/','2013-12-28 09:11:45','127.0.0.1',NULL,NULL),
  (7287,1,'admin','/admin/showLib/thesis/','2013-12-28 09:12:13','127.0.0.1',NULL,NULL),
  (7288,1,'admin','/admin/showLib/thesis/','2013-12-28 09:12:32','127.0.0.1',NULL,NULL),
  (7289,1,'admin','/admin/showLib/thesis/','2013-12-28 09:15:11','127.0.0.1',NULL,NULL),
  (7290,1,'admin','/admin/showLib/thesis/','2013-12-28 09:17:50','127.0.0.1',NULL,NULL),
  (7291,1,'admin','/admin/showLib/thesis/','2013-12-28 09:21:36','127.0.0.1',NULL,NULL),
  (7292,1,'admin','/admin/showLib/thesis/','2013-12-28 09:22:18','127.0.0.1',NULL,NULL),
  (7293,1,'admin','/admin/showLib/thesis/','2013-12-28 09:22:50','127.0.0.1',NULL,NULL),
  (7294,1,'admin','/admin/showLib/thesis/','2013-12-28 09:23:31','127.0.0.1',NULL,NULL),
  (7295,1,'admin','/admin/showLib/thesis/','2013-12-28 09:24:45','127.0.0.1',NULL,NULL),
  (7296,1,'admin','/admin/showLib/thesis/','2013-12-28 09:25:12','127.0.0.1',NULL,NULL),
  (7297,1,'admin','/admin/showLib/thesis/','2013-12-28 09:31:50','127.0.0.1',NULL,NULL),
  (7298,1,'admin','/admin/showLib/thesis/','2013-12-28 09:34:58','127.0.0.1',NULL,NULL),
  (7299,1,'admin','/admin/showLib/thesis/','2013-12-28 09:35:21','127.0.0.1',NULL,NULL),
  (7300,1,'admin','/admin/showLib/thesis/','2013-12-28 09:36:29','127.0.0.1',NULL,NULL),
  (7301,1,'admin','/admin/showLib/thesis/','2013-12-28 09:36:49','127.0.0.1',NULL,NULL),
  (7302,1,'admin','/admin/showLib/thesis/','2013-12-28 09:37:18','127.0.0.1',NULL,NULL),
  (7303,1,'admin','/admin/showLib/thesis/','2013-12-28 09:37:51','127.0.0.1',NULL,NULL),
  (7304,1,'admin','/admin/showLib/thesis/','2013-12-28 09:41:51','127.0.0.1',NULL,NULL),
  (7305,1,'admin','/admin/showLib/thesis/','2013-12-28 09:42:19','127.0.0.1',NULL,NULL),
  (7306,1,'admin','/admin/showLib/thesis/','2013-12-28 09:42:51','127.0.0.1',NULL,NULL),
  (7307,1,'admin','/admin/showLib/thesis/','2013-12-28 09:43:06','127.0.0.1',NULL,NULL),
  (7308,1,'admin','/admin/showStaticPage/','2013-12-28 09:44:28','127.0.0.1',NULL,NULL),
  (7309,1,'admin','/admin/showStaticPage/','2013-12-28 09:54:19','127.0.0.1',NULL,NULL),
  (7310,1,'admin','/admin/showStaticPage/','2013-12-28 09:54:58','127.0.0.1',NULL,NULL),
  (7311,1,'admin','/admin/showStaticPage/','2013-12-28 10:09:33','127.0.0.1',NULL,NULL),
  (7312,1,'admin','/admin/showStaticPage/','2013-12-28 10:09:49','127.0.0.1',NULL,NULL),
  (7313,1,'admin','/admin/showStaticPage/','2013-12-28 10:10:31','127.0.0.1',NULL,NULL),
  (7314,1,'admin','/admin/showStaticPage/','2013-12-28 10:11:11','127.0.0.1',NULL,NULL),
  (7315,1,'admin','/admin/showStaticPage/','2013-12-28 10:11:43','127.0.0.1',NULL,NULL),
  (7316,1,'admin','/admin/showStaticPage/','2013-12-29 00:23:49','127.0.0.1',NULL,NULL),
  (7317,1,'admin','/admin/showStaticPage/','2013-12-29 00:27:28','127.0.0.1',NULL,NULL),
  (7318,1,'admin','/admin/showStaticPage/','2013-12-29 00:29:05','127.0.0.1',NULL,NULL),
  (7319,1,'admin','/admin/showStaticPage/','2013-12-29 00:29:09','127.0.0.1',NULL,NULL),
  (7320,1,'admin','/admin/showStaticPage/','2013-12-29 00:29:36','127.0.0.1',NULL,NULL),
  (7321,1,'admin','/admin/showStaticPage/','2013-12-29 00:29:48','127.0.0.1',NULL,NULL),
  (7322,1,'admin','/admin/showMenu/','2013-12-29 00:34:46','127.0.0.1',NULL,NULL),
  (7323,1,'admin','/admin/showMenu/','2013-12-29 00:34:53','127.0.0.1',NULL,NULL),
  (7324,1,'admin','/admin/showMenu/','2013-12-29 00:34:58','127.0.0.1',NULL,NULL),
  (7325,1,'admin','/admin/showMenu/','2013-12-29 00:35:11','127.0.0.1',NULL,NULL),
  (7326,1,'admin','/admin/showMenu/','2013-12-29 00:39:34','127.0.0.1',NULL,NULL),
  (7327,1,'admin','/admin/showMenu/','2013-12-29 00:40:56','127.0.0.1',NULL,NULL),
  (7328,1,'admin','/admin/showMenu/','2013-12-29 00:42:35','127.0.0.1',NULL,NULL),
  (7329,1,'admin','/admin/showMenu/','2013-12-29 00:44:37','127.0.0.1',NULL,NULL),
  (7330,1,'admin','/admin/showMenu/','2013-12-29 00:50:41','127.0.0.1',NULL,NULL),
  (7331,1,'admin','/admin/formShowMenu/','2013-12-29 00:50:57','127.0.0.1',NULL,NULL),
  (7332,1,'admin','/admin/showMenu/','2013-12-29 00:51:46','127.0.0.1',NULL,NULL),
  (7333,1,'admin','/admin/showMenu/','2013-12-29 00:52:26','127.0.0.1',NULL,NULL),
  (7334,1,'admin','/admin/showMenu/','2013-12-29 00:52:43','127.0.0.1',NULL,NULL),
  (7335,1,'admin','/admin/showMenu/','2013-12-29 00:53:09','127.0.0.1',NULL,NULL),
  (7336,1,'admin','/admin/showMenu/','2013-12-29 01:03:57','127.0.0.1',NULL,NULL),
  (7337,1,'admin','/admin/showMenu/','2013-12-29 01:04:13','127.0.0.1',NULL,NULL),
  (7338,1,'admin','/admin/showMenu/','2013-12-29 01:05:06','127.0.0.1',NULL,NULL),
  (7339,1,'admin','/admin/showMenu/','2013-12-29 01:05:16','127.0.0.1',NULL,NULL),
  (7340,1,'admin','/admin/showMenu/','2013-12-29 01:05:59','127.0.0.1',NULL,NULL),
  (7341,1,'admin','/admin/showMenu/','2013-12-29 01:07:06','127.0.0.1',NULL,NULL),
  (7342,1,'admin','/admin/showMenu/','2013-12-29 01:08:07','127.0.0.1',NULL,NULL),
  (7343,1,'admin','/admin/showMenu/','2013-12-29 01:08:22','127.0.0.1',NULL,NULL),
  (7344,1,'admin','/admin/showMenu/','2013-12-29 01:08:45','127.0.0.1',NULL,NULL),
  (7345,1,'admin','/admin/showMenu/','2013-12-29 01:08:57','127.0.0.1',NULL,NULL),
  (7346,1,'admin','/admin/showMenu/','2013-12-29 01:10:03','127.0.0.1',NULL,NULL),
  (7347,1,'admin','/admin/showMenu/','2013-12-29 01:10:21','127.0.0.1',NULL,NULL),
  (7348,1,'admin','/admin/showMenu/','2013-12-29 01:13:37','127.0.0.1',NULL,NULL),
  (7349,1,'admin','/admin/showMenu/','2013-12-29 01:13:46','127.0.0.1',NULL,NULL),
  (7350,1,'admin','/admin/showMenu/','2013-12-29 01:14:03','127.0.0.1',NULL,NULL),
  (7351,1,'admin','/admin/showMenu/','2013-12-29 01:20:19','127.0.0.1',NULL,NULL),
  (7352,1,'admin','/admin/formShowMenu/','2013-12-29 01:20:28','127.0.0.1',NULL,NULL),
  (7353,1,'admin','/admin/getUniqMenu/','2013-12-29 01:20:46','127.0.0.1',NULL,NULL),
  (7354,1,'admin','/admin/updateMenu/','2013-12-29 01:20:48','127.0.0.1','a:11:{s:7:\"id_menu\";s:3:\"114\";s:11:\"id_menu_top\";s:1:\"0\";s:4:\"name\";s:20:\"Библиотека\";s:10:\"is_visible\";s:1:\"1\";s:3:\"url\";s:8:\"biblish/\";s:4:\"text\";s:0:\"\";s:13:\"is_rubricator\";s:1:\"1\";s:8:\"url_name\";s:7:\"biblish\";s:5:\"level\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:4:\"rubr\";a:0:{}}','a:11:{s:7:\"id_menu\";s:3:\"114\";s:11:\"id_menu_top\";s:1:\"0\";s:4:\"name\";s:20:\"Библиотека\";s:10:\"is_visible\";s:1:\"1\";s:3:\"url\";s:8:\"biblish/\";s:4:\"text\";s:0:\"\";s:13:\"is_rubricator\";s:1:\"1\";s:8:\"url_name\";s:7:\"biblish\";s:5:\"level\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:4:\"rubr\";a:0:{}}'),
  (7355,1,'admin','/admin/showMenu/','2013-12-29 01:20:48','127.0.0.1',NULL,NULL),
  (7356,1,'admin','/admin/formShowMenu/','2013-12-29 01:20:52','127.0.0.1',NULL,NULL),
  (7357,1,'admin','/admin/showMenu/','2013-12-29 01:23:37','127.0.0.1',NULL,NULL),
  (7358,1,'admin','/admin/showMenu/','2013-12-29 01:25:32','127.0.0.1',NULL,NULL),
  (7359,1,'admin','/admin/formShowMenu/','2013-12-29 01:25:37','127.0.0.1',NULL,NULL),
  (7360,1,'admin','/admin/formShowMenu/','2013-12-29 01:25:49','127.0.0.1',NULL,NULL),
  (7361,1,'admin','/admin/showMenu/','2013-12-29 01:25:51','127.0.0.1',NULL,NULL),
  (7362,1,'admin','/admin/formShowMenu/','2013-12-29 01:25:55','127.0.0.1',NULL,NULL),
  (7363,1,'admin','/admin/showMenu/','2013-12-29 01:29:47','127.0.0.1',NULL,NULL),
  (7364,1,'admin','/admin/formShowMenu/','2013-12-29 01:29:51','127.0.0.1',NULL,NULL),
  (7365,1,'admin','/admin/showMenu/','2013-12-29 01:30:43','127.0.0.1',NULL,NULL),
  (7366,1,'admin','/admin/showMenu/','2013-12-29 01:38:05','127.0.0.1',NULL,NULL),
  (7367,1,'admin','/admin/formShowMenu/','2013-12-29 01:38:10','127.0.0.1',NULL,NULL),
  (7368,1,'admin','/admin/showMenu/','2013-12-29 01:38:49','127.0.0.1',NULL,NULL),
  (7369,1,'admin','/admin/formShowMenu/','2013-12-29 01:38:54','127.0.0.1',NULL,NULL),
  (7370,1,'admin','/admin/showMenu/','2013-12-29 01:44:07','127.0.0.1',NULL,NULL),
  (7371,1,'admin','/admin/formShowMenu/','2013-12-29 01:44:13','127.0.0.1',NULL,NULL),
  (7372,1,'admin','/admin/showMenu/','2013-12-29 01:46:39','127.0.0.1',NULL,NULL),
  (7373,1,'admin','/admin/formShowMenu/','2013-12-29 01:46:45','127.0.0.1',NULL,NULL),
  (7374,1,'admin','/admin/showMenu/','2013-12-29 01:49:13','127.0.0.1',NULL,NULL),
  (7375,1,'admin','/admin/formShowMenu/','2013-12-29 01:49:19','127.0.0.1',NULL,NULL),
  (7376,1,'admin','/admin/showMenu/','2013-12-29 01:51:51','127.0.0.1',NULL,NULL),
  (7377,1,'admin','/admin/showMenu/','2013-12-29 01:52:42','127.0.0.1',NULL,NULL),
  (7378,1,'admin','/admin/showMenu/','2013-12-29 01:52:51','127.0.0.1',NULL,NULL),
  (7379,1,'admin','/admin/showMenu/','2013-12-29 01:53:56','127.0.0.1',NULL,NULL),
  (7380,1,'admin','/admin/formShowMenu/','2013-12-29 01:54:03','127.0.0.1',NULL,NULL),
  (7381,1,'admin','/admin/showMenu/','2013-12-29 01:54:52','127.0.0.1',NULL,NULL),
  (7382,1,'admin','/admin/showMenu/','2013-12-29 01:55:39','127.0.0.1',NULL,NULL),
  (7383,1,'admin','/admin/showMenu/','2013-12-29 01:56:09','127.0.0.1',NULL,NULL),
  (7384,1,'admin','/admin/showMenu/','2013-12-29 01:56:26','127.0.0.1',NULL,NULL),
  (7385,1,'admin','/admin/showMenu/','2013-12-29 01:56:37','127.0.0.1',NULL,NULL),
  (7386,1,'admin','/admin/showMenu/','2013-12-29 01:56:43','127.0.0.1',NULL,NULL),
  (7387,1,'admin','/admin/showMenu/','2013-12-29 01:56:59','127.0.0.1',NULL,NULL),
  (7388,1,'admin','/admin/formShowMenu/','2013-12-29 01:57:04','127.0.0.1',NULL,NULL),
  (7389,1,'admin','/admin/showMenu/','2013-12-29 01:57:21','127.0.0.1',NULL,NULL),
  (7390,1,'admin','/admin/formShowMenu/','2013-12-29 01:57:27','127.0.0.1',NULL,NULL),
  (7391,1,'admin','/admin/showMenu/','2013-12-29 01:57:52','127.0.0.1',NULL,NULL),
  (7392,1,'admin','/admin/formShowMenu/','2013-12-29 01:57:59','127.0.0.1',NULL,NULL),
  (7393,1,'admin','/admin/showMenu/','2013-12-29 01:58:07','127.0.0.1',NULL,NULL),
  (7394,1,'admin','/admin/formShowMenu/','2013-12-29 01:58:13','127.0.0.1',NULL,NULL),
  (7395,1,'admin','/admin/showMenu/','2013-12-29 01:58:54','127.0.0.1',NULL,NULL),
  (7396,1,'admin','/admin/formShowMenu/','2013-12-29 01:58:58','127.0.0.1',NULL,NULL),
  (7397,1,'admin','/admin/getUniqMenu/','2013-12-29 01:59:29','127.0.0.1',NULL,NULL),
  (7398,1,'admin','/admin/updateMenu/','2013-12-29 01:59:30','127.0.0.1','a:11:{s:7:\"id_menu\";s:3:\"114\";s:11:\"id_menu_top\";s:1:\"0\";s:4:\"name\";s:20:\"Библиотека\";s:10:\"is_visible\";s:1:\"1\";s:3:\"url\";s:8:\"biblish/\";s:4:\"text\";s:0:\"\";s:13:\"is_rubricator\";s:1:\"1\";s:8:\"url_name\";s:7:\"biblish\";s:5:\"level\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:4:\"rubr\";a:0:{}}','a:11:{s:7:\"id_menu\";s:3:\"114\";s:11:\"id_menu_top\";s:1:\"0\";s:4:\"name\";s:20:\"Библиотека\";s:10:\"is_visible\";s:1:\"1\";s:3:\"url\";s:8:\"biblish/\";s:4:\"text\";s:0:\"\";s:13:\"is_rubricator\";s:1:\"1\";s:8:\"url_name\";s:7:\"biblish\";s:5:\"level\";s:1:\"1\";s:8:\"position\";s:1:\"1\";s:4:\"rubr\";a:0:{}}'),
  (7399,1,'admin','/admin/showMenu/','2013-12-29 01:59:31','127.0.0.1',NULL,NULL),
  (7400,1,'admin','/admin/formShowMenu/','2013-12-29 01:59:37','127.0.0.1',NULL,NULL),
  (7401,1,'admin','/admin/formShowMenu/','2013-12-29 01:59:43','127.0.0.1',NULL,NULL),
  (7402,1,'admin','/admin/showMenu/','2013-12-29 02:00:13','127.0.0.1',NULL,NULL),
  (7403,1,'admin','/admin/formShowMenu/','2013-12-29 02:00:18','127.0.0.1',NULL,NULL),
  (7404,1,'admin','/admin/showMenu/','2013-12-29 02:00:25','127.0.0.1',NULL,NULL),
  (7405,1,'admin','/admin/formShowMenu/','2013-12-29 02:00:30','127.0.0.1',NULL,NULL),
  (7406,1,'admin','/admin/formShowMenu/','2013-12-29 02:00:48','127.0.0.1',NULL,NULL),
  (7407,1,'admin','/admin/showMenu/','2013-12-29 02:00:50','127.0.0.1',NULL,NULL),
  (7408,1,'admin','/admin/formShowMenu/','2013-12-29 02:00:56','127.0.0.1',NULL,NULL),
  (7409,1,'admin','/admin/showMenu/','2013-12-29 02:02:52','127.0.0.1',NULL,NULL),
  (7410,1,'admin','/admin/formShowMenu/','2013-12-29 02:03:00','127.0.0.1',NULL,NULL),
  (7411,1,'admin','/admin/showMenu/','2013-12-29 02:03:09','127.0.0.1',NULL,NULL),
  (7412,1,'admin','/admin/formShowMenu/','2013-12-29 02:03:15','127.0.0.1',NULL,NULL),
  (7413,1,'admin','/admin/showMenu/','2013-12-29 02:03:35','127.0.0.1',NULL,NULL),
  (7414,1,'admin','/admin/formShowMenu/','2013-12-29 02:03:41','127.0.0.1',NULL,NULL),
  (7415,1,'admin','/admin/showMenu/','2013-12-29 02:04:08','127.0.0.1',NULL,NULL),
  (7416,1,'admin','/admin/formShowMenu/','2013-12-29 02:04:15','127.0.0.1',NULL,NULL),
  (7417,1,'admin','/admin/showMenu/','2013-12-29 02:04:28','127.0.0.1',NULL,NULL),
  (7418,1,'admin','/admin/formShowMenu/','2013-12-29 02:04:34','127.0.0.1',NULL,NULL),
  (7419,1,'admin','/admin/showMenu/','2013-12-29 02:04:50','127.0.0.1',NULL,NULL),
  (7420,1,'admin','/admin/formShowMenu/','2013-12-29 02:04:56','127.0.0.1',NULL,NULL),
  (7421,1,'admin','/admin/showMenu/','2013-12-29 02:05:51','127.0.0.1',NULL,NULL),
  (7422,1,'admin','/admin/formShowMenu/','2013-12-29 02:05:56','127.0.0.1',NULL,NULL),
  (7423,1,'admin','/admin/showMenu/','2013-12-29 02:07:46','127.0.0.1',NULL,NULL),
  (7424,1,'admin','/admin/formShowMenu/','2013-12-29 02:07:51','127.0.0.1',NULL,NULL),
  (7425,1,'admin','/admin/showMenu/','2013-12-29 02:07:56','127.0.0.1',NULL,NULL),
  (7426,1,'admin','/admin/formShowMenu/','2013-12-29 02:08:00','127.0.0.1',NULL,NULL),
  (7427,1,'admin','/admin/showMenu/','2013-12-29 02:08:08','127.0.0.1',NULL,NULL),
  (7428,1,'admin','/admin/formShowMenu/','2013-12-29 02:08:12','127.0.0.1',NULL,NULL),
  (7429,1,'admin','/admin/showMenu/','2013-12-29 02:08:20','127.0.0.1',NULL,NULL),
  (7430,1,'admin','/admin/formShowMenu/','2013-12-29 02:08:24','127.0.0.1',NULL,NULL),
  (7431,1,'admin','/admin/showMenu/','2013-12-29 02:08:30','127.0.0.1',NULL,NULL),
  (7432,1,'admin','/admin/formShowMenu/','2013-12-29 02:08:34','127.0.0.1',NULL,NULL),
  (7433,1,'admin','/admin/showMenu/','2013-12-29 02:08:42','127.0.0.1',NULL,NULL),
  (7434,1,'admin','/admin/formShowMenu/','2013-12-29 02:08:47','127.0.0.1',NULL,NULL),
  (7435,1,'admin','/admin/formShowMenu/','2013-12-29 02:08:57','127.0.0.1',NULL,NULL),
  (7436,1,'admin','/admin/showMenu/','2013-12-29 02:09:08','127.0.0.1',NULL,NULL),
  (7437,1,'admin','/admin/formShowMenu/','2013-12-29 02:09:14','127.0.0.1',NULL,NULL),
  (7438,1,'admin','/admin/showMenu/','2013-12-29 02:09:21','127.0.0.1',NULL,NULL),
  (7439,1,'admin','/admin/formShowMenu/','2013-12-29 02:09:26','127.0.0.1',NULL,NULL),
  (7440,1,'admin','/admin/showMenu/','2013-12-29 02:09:43','127.0.0.1',NULL,NULL),
  (7441,1,'admin','/admin/formShowMenu/','2013-12-29 02:09:49','127.0.0.1',NULL,NULL),
  (7442,1,'admin','/admin/showMenu/','2013-12-29 02:09:53','127.0.0.1',NULL,NULL),
  (7443,1,'admin','/admin/formShowMenu/','2013-12-29 02:09:59','127.0.0.1',NULL,NULL),
  (7444,1,'admin','/admin/formShowMenu/','2013-12-29 02:10:12','127.0.0.1',NULL,NULL),
  (7445,1,'admin','/admin/showMenu/','2013-12-29 02:10:18','127.0.0.1',NULL,NULL),
  (7446,1,'admin','/admin/formShowMenu/','2013-12-29 02:10:22','127.0.0.1',NULL,NULL),
  (7447,1,'admin','/admin/showStaticPage/','2013-12-29 02:11:02','127.0.0.1',NULL,NULL),
  (7448,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:11:06','127.0.0.1',NULL,NULL),
  (7449,1,'admin','/admin/showStaticPage/','2013-12-29 02:11:16','127.0.0.1',NULL,NULL),
  (7450,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:11:21','127.0.0.1',NULL,NULL),
  (7451,1,'admin','/admin/showStaticPage/','2013-12-29 02:12:48','127.0.0.1',NULL,NULL),
  (7452,1,'admin','/admin/showStaticPage/','2013-12-29 02:13:02','127.0.0.1',NULL,NULL),
  (7453,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:13:10','127.0.0.1',NULL,NULL),
  (7454,1,'admin','/admin/showStaticPage/','2013-12-29 02:15:06','127.0.0.1',NULL,NULL),
  (7455,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:15:13','127.0.0.1',NULL,NULL),
  (7456,1,'admin','/admin/showStaticPage/','2013-12-29 02:15:42','127.0.0.1',NULL,NULL),
  (7457,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:15:48','127.0.0.1',NULL,NULL),
  (7458,1,'admin','/admin/showStaticPage/','2013-12-29 02:18:02','127.0.0.1',NULL,NULL),
  (7459,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:18:06','127.0.0.1',NULL,NULL),
  (7460,1,'admin','/admin/showStaticPage/','2013-12-29 02:18:10','127.0.0.1',NULL,NULL),
  (7461,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:18:14','127.0.0.1',NULL,NULL),
  (7462,1,'admin','/admin/showStaticPage/','2013-12-29 02:21:22','127.0.0.1',NULL,NULL),
  (7463,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:21:26','127.0.0.1',NULL,NULL),
  (7464,1,'admin','/admin/showStaticPage/','2013-12-29 02:22:34','127.0.0.1',NULL,NULL),
  (7465,1,'admin','/admin/showStaticPage/','2013-12-29 02:22:36','127.0.0.1',NULL,NULL),
  (7466,1,'admin','/admin/showStaticPage/','2013-12-29 02:22:39','127.0.0.1',NULL,NULL),
  (7467,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:22:41','127.0.0.1',NULL,NULL),
  (7468,1,'admin','/admin/showStaticPage/','2013-12-29 02:23:28','127.0.0.1',NULL,NULL),
  (7469,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:23:34','127.0.0.1',NULL,NULL),
  (7470,1,'admin','/admin/showStaticPage/','2013-12-29 02:23:39','127.0.0.1',NULL,NULL),
  (7471,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:23:44','127.0.0.1',NULL,NULL),
  (7472,1,'admin','/admin/showStaticPage/','2013-12-29 02:24:15','127.0.0.1',NULL,NULL),
  (7473,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:24:19','127.0.0.1',NULL,NULL),
  (7474,1,'admin','/admin/showStaticPage/','2013-12-29 02:24:58','127.0.0.1',NULL,NULL),
  (7475,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:25:05','127.0.0.1',NULL,NULL),
  (7476,1,'admin','/admin/showStaticPage/','2013-12-29 02:26:48','127.0.0.1',NULL,NULL),
  (7477,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:26:52','127.0.0.1',NULL,NULL),
  (7478,1,'admin','/admin/showStaticPage/','2013-12-29 02:26:59','127.0.0.1',NULL,NULL),
  (7479,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:27:03','127.0.0.1',NULL,NULL),
  (7480,1,'admin','/admin/showStaticPage/','2013-12-29 02:31:16','127.0.0.1',NULL,NULL),
  (7481,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:31:20','127.0.0.1',NULL,NULL),
  (7482,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:31:30','127.0.0.1',NULL,NULL),
  (7483,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:31:36','127.0.0.1',NULL,NULL),
  (7484,1,'admin','/admin/showStaticPage/','2013-12-29 02:36:38','127.0.0.1',NULL,NULL),
  (7485,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:36:44','127.0.0.1',NULL,NULL),
  (7486,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:36:52','127.0.0.1',NULL,NULL),
  (7487,1,'admin','/admin/showStaticPage/','2013-12-29 02:37:02','127.0.0.1',NULL,NULL),
  (7488,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:37:07','127.0.0.1',NULL,NULL),
  (7489,1,'admin','/admin/showStaticPage/','2013-12-29 02:37:22','127.0.0.1',NULL,NULL),
  (7490,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:37:29','127.0.0.1',NULL,NULL),
  (7491,1,'admin','/admin/showStaticPage/','2013-12-29 02:37:38','127.0.0.1',NULL,NULL),
  (7492,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:37:43','127.0.0.1',NULL,NULL),
  (7493,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:37:59','127.0.0.1',NULL,NULL),
  (7494,1,'admin','/admin/showStaticPage/','2013-12-29 02:38:01','127.0.0.1',NULL,NULL),
  (7495,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:38:06','127.0.0.1',NULL,NULL),
  (7496,1,'admin','/admin/showStaticPage/','2013-12-29 02:38:14','127.0.0.1',NULL,NULL),
  (7497,1,'admin','/admin/formShowStaticPage/','2013-12-29 02:38:19','127.0.0.1',NULL,NULL),
  (7498,1,'admin','/admin/showStaticPage/','2013-12-29 21:47:29','127.0.0.1',NULL,NULL),
  (7499,1,'admin','/admin/formShowStaticPage/','2013-12-29 21:47:52','127.0.0.1',NULL,NULL),
  (7500,1,'admin','/admin/updateStaticPage/','2013-12-29 21:48:21','127.0.0.1','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:0:\"\";s:4:\"text\";s:0:\"\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-07-27 13:40:27\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";N;s:15:\"id_page_forwars\";N;}','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:56:\"<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-12-29 21:48:21\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}'),
  (7501,1,'admin','/admin/showStaticPage/','2013-12-29 21:48:22','127.0.0.1',NULL,NULL),
  (7502,1,'admin','/admin/formShowStaticPage/','2013-12-29 21:48:27','127.0.0.1',NULL,NULL),
  (7503,1,'admin','/admin/formShowStaticPage/','2013-12-29 21:56:40','127.0.0.1',NULL,NULL),
  (7504,1,'admin','/admin/updateStaticPage/','2013-12-29 21:57:07','127.0.0.1','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:56:\"<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-12-29 21:48:21\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:131:\"<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ul>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-12-29 21:57:07\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}'),
  (7505,1,'admin','/admin/showStaticPage/','2013-12-29 21:57:07','127.0.0.1',NULL,NULL),
  (7506,1,'admin','/admin/formShowStaticPage/','2013-12-29 22:10:20','127.0.0.1',NULL,NULL),
  (7507,1,'admin','/admin/updateStaticPage/','2013-12-29 22:10:33','127.0.0.1','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:131:\"<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ul>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-12-29 21:57:07\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:164:\"<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\r\n<p style=\"margin-left: 40px;\">zxdsafsdfsdf</p>\r\n\r\n<ul>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ul>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-12-29 22:10:33\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}'),
  (7508,1,'admin','/admin/showStaticPage/','2013-12-29 22:10:34','127.0.0.1',NULL,NULL),
  (7509,1,'admin','/admin/formShowStaticPage/','2013-12-29 22:10:51','127.0.0.1',NULL,NULL),
  (7510,1,'admin','/admin/updateStaticPage/','2013-12-29 22:12:17','127.0.0.1','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:164:\"<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\r\n<p style=\"margin-left: 40px;\">zxdsafsdfsdf</p>\r\n\r\n<ul>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ul>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-12-29 22:10:33\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}','a:20:{s:14:\"id_static_page\";s:2:\"69\";s:18:\"id_static_page_top\";s:2:\"40\";s:7:\"id_menu\";s:1:\"0\";s:4:\"name\";s:34:\"Учебники и пособия\";s:3:\"url\";s:25:\"uchebniki-i-posobiya.html\";s:10:\"annotaciya\";s:18:\"Аннотация\";s:4:\"text\";s:392:\"<ol>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ol>\r\n\r\n<p style=\"margin-left: 40px;\">zxdsafsdfsdf</p>\r\n\r\n<p style=\"text-align: justify;\">k;lkl;kkl;lkasdasdasdasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddd sxasdfsad</p>\r\n\r\n<p style=\"text-align: center;\">&nbsp;asdsadasd</p>\r\n\r\n<p style=\"text-align: justify;\">ada</p>\r\n\r\n<ul>\r\n\t<li>лист1</li>\r\n\t<li>лист 2</li>\r\n</ul>\r\n\";s:8:\"keywords\";s:0:\"\";s:11:\"description\";s:0:\"\";s:5:\"title\";s:34:\"Учебники и пособия\";s:8:\"id_users\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:7:\"is_menu\";s:1:\"1\";s:8:\"date_add\";s:19:\"2013-07-27 13:39:58\";s:11:\"date_update\";s:19:\"2013-12-29 22:12:16\";s:10:\"is_folders\";s:1:\"0\";s:8:\"position\";s:1:\"2\";s:5:\"level\";s:1:\"2\";s:8:\"link_out\";s:0:\"\";s:15:\"id_page_forwars\";s:1:\"0\";}'),
  (7511,1,'admin','/admin/showStaticPage/','2013-12-29 22:12:17','127.0.0.1',NULL,NULL),
  (7512,1,'admin','/admin/showStaticPage/','2014-01-03 13:47:09','127.0.0.1',NULL,NULL),
  (7513,1,'admin','/admin/showStaticPage/','2014-01-15 17:13:13','127.0.0.1',NULL,NULL),
  (7514,1,'admin','/admin/showPerson/','2014-01-16 12:11:12','127.0.0.1',NULL,NULL),
  (7515,1,'admin','/admin/showPerson/','2014-01-16 12:13:33','127.0.0.1',NULL,NULL),
  (7516,1,'admin','/admin/showPerson/','2014-01-16 12:18:49','127.0.0.1',NULL,NULL),
  (7517,1,'admin','/admin/showPerson/','2014-01-16 12:19:05','127.0.0.1',NULL,NULL),
  (7518,1,'admin','/admin/showPerson/','2014-01-16 12:19:15','127.0.0.1',NULL,NULL),
  (7519,1,'admin','/admin/showPerson/','2014-01-16 12:23:59','127.0.0.1',NULL,NULL),
  (7520,1,'admin','/admin/formShowPerson/','2014-01-16 12:24:05','127.0.0.1',NULL,NULL),
  (7521,1,'admin','/admin/showPerson/','2014-01-17 01:48:38','127.0.0.1',NULL,NULL),
  (7522,1,'admin','/admin/showPerson/','2014-01-21 00:04:34','127.0.0.1',NULL,NULL),
  (7523,1,'admin','/admin/showPerson/','2014-01-28 20:40:51','127.0.0.1',NULL,NULL),
  (7524,1,'admin','/admin/showPerson/','2014-02-07 15:07:47','127.0.0.1',NULL,NULL),
  (7525,1,'admin','/admin/showPerson/','2014-03-04 11:45:09','127.0.0.1',NULL,NULL),
  (7526,1,'admin','/admin/showPerson/','2014-03-06 16:18:00','127.0.0.1',NULL,NULL);
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;